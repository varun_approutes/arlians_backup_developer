// dashboard.js
    // var arlians = angular.module('arliansApp', ['ngSanitize', 'textAngular','ngAnimate','hm.readmore']);

 var arlians = angular.module('arliansApp', ['ngSanitize', 'textAngular']);

arlians.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist([
        // Allow same origin resource loads.
        'self',
        // Allow loading from our assets domain.  Notice the difference between * and **.
        'http://docs.google.com/gview?url=**',
        'https://www.youtube.com/vi/**']);
})
arlians.directive('ngFiles', ['$parse', function ($parse) {
        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, {$files: event.target.files});
            });
        }
        ;
        return {
            link: fn_link
        }
    }]);
arlians.filter('capitalize', function() {
    return function(input, all) {
        var count = 0;
      var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
      //console.log();
      return (!!input) ? input.replace(reg,function(txt){
        if(count<2){
            count++;return txt.charAt(0).toUpperCase();
        }else{
            return '';
        }
        }) : '';
    }
  });
arlians.filter('strLimit', ['$filter', function ($filter) {
        return function (input, limit) {
            if (!input)
                return;
            if (input.length <= limit) {
                return input;
            }
            return $filter('limitTo')(input, limit) + '...';
        };
    }]);

arlians.config(function($provide) {
                // this demonstrates how to register a new tool and add it to the default toolbar
                $provide.decorator('taOptions', ['taRegisterTool', '$delegate', function(taRegisterTool, taOptions) { // $delegate is the taOptions we are decorating
                    taOptions.toolbar = [
                                            ['h1', 'quote'],
                                            ['bold', 'italics',  'ul', 'ol'],
                                            ['justifyLeft', 'justifyCenter', 'justifyRight', 'indent']
                                            //['html', 'insertImage','insertLink', 'insertVideo', 'wordcount', 'charcount']
                                        ];
                    taRegisterTool('colourRed', {
                        iconclass: "fa fa-square red",
                        action: function() {
                            this.$editor().wrapSelection('forecolor', 'red');
                        }
                    });
                    return taOptions;
                }]);

});
arlians.controller('DashboardCtrl', ['$scope','$rootScope','$location', function ($scope,$rootScope,$location) {
        $scope.user_email = UserProxy.getAccessToken();
        $scope.user_details = {};
        $scope.front_img_name = '';
        $scope.bussinessname = '';
        $scope.count_unread_message = 0;
        $scope.show_read_mail_notification = [];
		$rootScope.show_talk_notification_count = 0;
		$rootScope.show_talk_notification = [];
        $scope.show_unread_mail_notification = [];
        $scope.notification_list = [];
        $scope.notification_list_count = 0;
        $scope.notification_unseen = '';
        $scope.connection_request_list = [];
        $scope.notification_unseen_count = 0;
        $scope.path = '';
        $scope.user_buisnessname = '';
        $scope.user_firstname = '';
        $scope.user_lastname = '';
        $scope.is_professional = false;
        $scope.user_buisnessname_disable = false;
        this.updateUserDetail = function () {
            UserProxy.getCurrentUser(function (result) {
               // console.log("#Result of user data");
               // consloe.log(result);
                $scope.path = result.user_info.path;
                $scope.front_img_name = result.user_info.fornt_image;
                $scope.bussinessname = result.bussinessname;
                $scope.sign_image = result.user_info.sign_image; 
                $scope.tagline = result.tagline;  
                $rootScope.tagline2 =result.user_info.tagline;  

            });
        };
        $scope.toggleIsProfrssional = function () {
            $scope.is_professional = $scope.is_professional == true ? false : true;
            $timeout(function () {
                if ($scope.is_professional == true) {
                    debugger;
                    $scope.user_buisnessname = '';
                    $scope.user_buisnessname_disable = true;
                } else {
                    $scope.user_buisnessname_disable = false;
                }
            }, 100);
        };
        UserProxy.isBuisnessNameExists(function (result) {
            if (result.success == 'true') {
                $scope.buisname_exit = 'true';
            } else {
                $scope.buisname_exit = 'false';
                $('#wiz_one_last_modal').modal('show');
            }
        });
		UserProxy.CommunityNotification(function (result) {
			//console.log("n_comunity");
			//console.log(result);
			//console.log("n_comunity");
			$rootScope.show_talk_notification_count=result.notificationcount;
            $rootScope.show_talk_notification=result.details; 
        });

       //$rootScope.matchMob = true;
       // $rootScope.profMob = true;
        //$rootScope.hubMob =true;
        localStorage.setItem("MOBILEFLAG", '');
        localStorage.removeItem('MOBILEFLAG');

        $scope.activeHub = function(){
          //  window.location.href ="";
        localStorage.setItem("MOBILEFLAG", '');
        localStorage.removeItem('MOBILEFLAG');
        angular.element(document.querySelector("#mobmenu")).removeClass("dl-active");
        angular.element(document.querySelector("#menuul")).removeClass("dl-menuopen");
        //$location.path('/').search({FLAG: 'HUB'});
        localStorage.setItem("MOBILEFLAG", 'HUB');
        //window.location.href ="dashboard
       
        window.location.href ="http://arlians.com/developer/member/dashboard";
        }
        $scope.activeProfile = function(){
           // window.location.href ="";
       // localStorage.setItem("MOBILEFLAG", 'HUB');
        window.location.href ="http://arlians.com/developer/memberprofile/viewMemberProfileApi";

        }
          $scope.activeSettings = function(){
           // window.location.href ="";
       // localStorage.setItem("MOBILEFLAG", 'HUB');
        window.location.href ="http://arlians.com/developer/memberprofile/userSsettings";

        }

//http://arlians.com/developer/memberprofile/userSsettings

  $scope.activeProf =function(){

        /*$rootScope.matchMob = false;hboad_pge
        $rootScope.profMob = true;
        $rootScope.hubMob = false;*/
        window.location.href ="";
        localStorage.setItem("MOBILEFLAG", '');
        localStorage.removeItem('MOBILEFLAG');
        angular.element(document.querySelector("#mobmenu")).removeClass("dl-active");
        angular.element(document.querySelector("#menuul")).removeClass("dl-menuopen");
        localStorage.setItem("MOBILEFLAG", 'PROF');
       //.location.href ="dashboard";
       window.location.href ="http://arlians.com/developer/member/dashboard";
        
        
  }

$scope.activeMatch = function(){

       // $rootScope.matchMob = true;
       // $rootScope.profMob = false;
       // $rootScope.hubMob = false;
    //window.location.href ="";

        localStorage.setItem("MOBILEFLAG", '');
        localStorage.removeItem('MOBILEFLAG');
        angular.element(document.querySelector("#mobmenu")).removeClass("dl-active");
        angular.element(document.querySelector("#menuul")).removeClass("dl-menuopen");
        localStorage.setItem("MOBILEFLAG", 'MATCH');
        //window.location.href ="dashboard";
        window.location.href ="http://arlians.com/developer/member/dashboard";
}

     $(window).resize(function(){
   //console.log("Width"+window.innerWidth);


    $scope.$apply(function(){
 localStorage.setItem("PAGEWIDTH", window.innerWidth);
 if(window.innerWidth>992){
 localStorage.setItem("MOBILEFLAG", '');
localStorage.removeItem('MOBILEFLAG');
}

    });

});


        $scope.welcomeWizard = function () {
            if ($scope.is_professional == false) {
                if ($scope.user_buisnessname == '') {
                   // alertify.error('Please enter your business name');
                    return false;
                }
            }
            if ($scope.user_firstname == '') {
                //alertify.error('Please enter your first name');
                return false;
            }
            if ($scope.user_lastname == '') {
               // alertify.error('Please enter your last name');
                return false;
            }

            if ($scope.is_professional == false) {
                //UserProxy.isBusinessNameuniqe($scope.user_buisnessname, function (result) {
                    //if (result.success == 'true') {
                        UserProxy.welcomeWizard($scope.is_professional, $scope.user_buisnessname, $scope.user_firstname, $scope.user_lastname, function (result) {
                            if (result.response == 'success') {
                                $('#wiz_one_last_modal').modal('hide');
                                $scope.getCurrentUserDetails();
                            }
                        });
                    //} else {
                        //alertify.error('Business name exit');
                    //}
                //});
            } else {
                UserProxy.welcomeWizard($scope.is_professional, $scope.user_buisnessname, $scope.user_firstname, $scope.user_lastname, function (result) {
                    if (result.response == 'success') {
                        $('#wiz_one_last_modal').modal('hide');
                        $scope.getCurrentUserDetails();
                    }
                });
            }

        };
        $scope.getCurrentUserDetails = function () {
            UserProxy.getCurrentUser(function (result) {

               // console.log("Nilanjan setting page");
               // console.log(result);
               // alert(result.country_name);
               // alert(result.user_info.city);
              //  console.log("********************");
                $scope.path = result.user_info.path;
                $scope.front_img_name = result.user_info.fornt_image;
                $scope.bussinessname = result.bussinessname;
                $scope.reg_date = result.user_info.reg_date;
                $scope.connection_count = result.user_info.connection_count;
                $scope.user_initial = result.logobussinessname;
                $scope.sign_image = result.user_info.sign_image;
                $scope.profile_first_name = result.user_info.fname;
                $scope.profile_last_name = result.user_info.lname;
                $scope.profile_bussinessname = result.user_info.bussinessname;
                $scope.tagline = result.tagline;
                $scope.cityuser = result.user_info.city;
                $scope.country_name = result.country_name;
                $scope.back_image = result.user_info.back_image;
                $rootScope.tagline2 = result.user_info.tag_line;
                $rootScope.USERIDCURRENT = result.user_info.mid;
            });
        }
        $scope.connection_request_count = 0;
        if ($scope.user_email === '' || $scope.user_email === 0 || $scope.user_email === null || $scope.user_email === '0') {
            window.location = "logout";
        } else {
            $scope.getCurrentUserDetails();
            UserProxy.getUserNotificationDetails(function (result_notification) {
               // console.log("result_notification");
               // console.log(result_notification);
               // console.log("result_notification");
                $scope.notification_list = result_notification.notification_list;
                $scope.notification_list_count = $scope.notification_list.length;
                $scope.notification_unseen = result_notification.notification_unseen;
                $scope.notification_unseen_count = $scope.notification_unseen.length;
                //alert($scope.notification_unseen_count);
            });
            UserProxy.getUserRequestDetails(function (result_request) {
                //console.log(result_request);
                var count = 0;
                for (var i = 0; i < result_request.length; i++) {
                    if (result_request[i].is_seen == 0) {
                        count++;
                    }
                }
                $scope.connection_request_count = count;
                $scope.connection_request_list = result_request;
            });
            UserProxy.getUserUnreadMailDetails(function (result) {
               // console.log("===========================");
               // console.log(result);
               // console.log("***********************");


                $scope.count_unread_message = result;
                $scope.show_unread_mail_notification = result;
               /* var count_read_message_display_from_4 = 4 - $scope.count_unread_message.length;
                if (count_read_message_display_from_4 > 0) {
                    UserProxy.getUserReadMailDetails(count_read_message_display_from_4, function (result_read) {
                        $scope.show_read_mail_notification = result_read;
                    });
                }*/
            });
        }
        $scope.updateNotification = function(){
            $scope.notification_list_count = 0;
           // console.log($scope.notification_list);
            UserProxy.updateNotification(angular.toJson($scope.notification_list), function (result) {
              //  console.log(result);
            });
        }
        $scope.friendRequestAccepted = function (member_obj) {
            UserProxy.friendRequestAccepted(member_obj.cid, function (result) {
                if (result.success == 1) {
					$scope.connection_request_count -= 1;
                    member_obj.is_accepted = '1';
                } else {
                    //alertify.error('Some error occured');
                }
            });
        };
        $scope.talkRequestAccepted = function (talk_obj) {
			for(var i=0;i<$rootScope.show_talk_notification.length;i++){
				//alert($scope.show_talk_notification.length);
				//console.log($rootScope.show_talk_notification);	
					if($rootScope.show_talk_notification[i].talk_access_id==talk_obj.talk_access_id){
					//alert($rootScope.show_talk_notification[i].talk_access_id);
					
					var value2=$rootScope.show_talk_notification[i].talk_access_id;
					//alert(value1);
					
					
					$('#'+value2).remove();
					$rootScope.show_talk_notification_count--;
					UserProxy.talkRequestAccepted(talk_obj.mid,talk_obj.community_id,talk_obj.talk_owner_id, function (result) {
					if (result.success == 1) {
						//alertify.success('Talk Request Accepted');
					} else {
						//alertify.error('Some error occured');
					}
				});
			}
		  }
        };
        $scope.talkRequestDecline = function (talk_obj) {
			console.log(talk_obj);
		for(var i=0;i<$rootScope.show_talk_notification.length;i++){
				//alert($scope.show_talk_notification.length);
				console.log($rootScope.show_talk_notification);	
					if($rootScope.show_talk_notification[i].talk_access_id==talk_obj.talk_access_id){
					//alert($rootScope.show_talk_notification[i].talk_access_id);
					
					var value1=$rootScope.show_talk_notification[i].talk_access_id;
					//alert(value1);
					
					
					$('#'+value1).remove();
					$rootScope.show_talk_notification_count--;
					//return false;
					UserProxy.talkRequestDecline(talk_obj.mid,talk_obj.community_id,talk_obj.talk_owner_id, function (result) {
						
						if (result.success == 1) {
							//alertify.success('Decline Successfully');
						} else {
							//alertify.error('Some error occured');
						}
					});
					
				}
			}
        };
	


        $scope.friendRequestDecline = function (member_obj) {
            UserProxy.friendRequestDecline(member_obj.cid, function (result) {
                if (result.success == 1) {
                    $scope.connection_request_count -= 1;
                    var index = $scope.connection_request_list.indexOf(member_obj);
                    $scope.connection_request_list.splice(index, 1);
                } else {
                    //alertify.error('Some error occured');
                }
            });
        };
        $scope.signOut = function () {
            UserProxy.unsetAccessToken();
            setTimeout(function () {
                window.location = "logout";
            }, 200);
        };
        $scope.openModalForSignatureImg = function () {
            $('#crop_signature_image').modal('show');
        }
        $scope.uploadSignatureImage = function () {
            try {
                var img = cropperc.getDataURL();
                //console.log(img);
                UserProxy.signatureImage(img, function (result) {
                    if (result.response == 'success') {
                        $scope.sign_image = result.sign_img;
                        $('#crop_signature_image').modal('hide');
                    }
                });
            } catch (e) {
               // alertify.error('Please select a image');
            }
        };
        /*$scope.editProfile = function(){
         window.location = "logout";
         };*/
    }]);
var options_signature =
        {
            imageBox: '.imageBox_signature',
            thumbBox: '.thumbBox_signature',
            spinner: '.spinner',
            imgSrc: 'avatar.png'
        }
var cropperc;
document.querySelector('#filesignature').addEventListener('change', function () {
    var reader = new FileReader();
    reader.onload = function (e) {
        options_signature.imgSrc = e.target.result;
        cropperc = new cropbox(options_signature);
    }
    reader.readAsDataURL(this.files[0]);
    //this.files = [];
});
;
document.querySelector('#btnZoomInc').addEventListener('click', function () {
    cropperc.zoomIn();
});

document.querySelector('#btnZoomOutc').addEventListener('click', function () {
    cropperc.zoomOut();
});

