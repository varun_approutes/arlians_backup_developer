﻿arlians.controller('viewOtherMemberProfileApiCtrl', function ($scope, $timeout) {
	//debugger;
    $scope.business_name = '';
    $scope.detail_suggestion_list = [];
    $scope.details_suggestion = [];
    $scope.show_detail = false;
    $scope.search_module = true;
	$scope.profile_image = '';
	$scope.list_latest_message_viewable = [];
	$scope.suggested_connected_lists = [];
	$scope.sectorname = [];
	$scope.city='';
	$scope.countryName='';
	$scope.publishList=[];
	$scope.doc_uploaded=[];
    UserProxy.getProfileDetails(function (result) {
		$scope.business_name=result.bussinessname;
		$scope.business_tagline=result.tag_line;
		$scope.business_desc=result.bussinessdesc;
		$scope.profileComplition=result.profileComplition;
		$scope.connection_no=result.connection_no;
		$scope.post_no=result.post_no;
		$scope.publish_no=result.publish_no;
		$scope.opentalks_no=result.opentalks_no;
		$scope.profile_image=result.profile_image;
		$scope.background_image=result.background_image;
		$scope.list_latest_message_viewable = result.latest_msg;
		$scope.sectorname = result.sectorname;
		$scope.suggested_connected_lists = result.connection_with;
		$scope.city = result.city;
		$scope.publishList = result.publishList;
		$scope.countryName = result.countryName;
		$scope.doc_uploaded = result.doc_uploaded;
		$timeout(function () {	
			$("#connect_business").owlCarousel({
				navigation : true,
				slideSpeed : 300,
				paginationSpeed : 400,
				singleItem : true,
				autoPlay : true,
				pagination : false,
				navigationText : ["&#xf053;", "&#xf054;"]
			});
		},500);
		$timeout(function () {	
			$("#summary").owlCarousel({
				itemsCustom : [
           [0, 1],
          [450, 1],
          [600, 2],
          [700, 3],
          [1000, 4],
          [1200, 4],
          [1400, 4],
          [1600, 5]
        ],
        navigation : true,
        pagination : false,
        navigationText : ["&#xf053;", "&#xf054;"]
			});
		},500);
		$timeout(function () {	
			$("#publish").owlCarousel({
				itemsCustom : [
          [0, 1],
          [450, 1],
          [600, 2],
          [700, 3],
          [1000, 3],
          [1200, 3],
          [1400, 3],
          [1600, 4]
        ],
        navigation : true,
        pagination : false,
        navigationText : ["&#xf053;", "&#xf054;"]
			});
		},500);
		//alert($scope.profile_image);
		//alert($scope.business_name);
         //console.log(result);
    });
	//alert('hii');
});

	
arlians.directive('ellipsis', [function () {
    return {
        required: 'ngBindHtml',
        restrict: 'A',
        priority: 100,
        link: function ($scope, element, attrs, ctrl) {
            $scope.hasEllipsis = false;
            $scope.$watch(element.html(), function(value) {
               if (!$scope.hasEllipsis) {
                   // apply ellipsis only one
                   $scope.hasEllipsis = true;
                   element.ellipsis();
               }
            });
        }
    };
}]);