"use strict";
arlians.controller('settingsCtrl', function ($scope, $timeout,$window) {

   // window.localStorage.setItem("Matching_setting", '');
   // window.localStorage.removeItem('Matching_setting');

   var matchflag = window.localStorage.getItem("Matching_setting");
  // alert(matchflag);
   if(matchflag ==='100'){
    $scope.MatchFlag='YES';
   // alert($scope.MatchFlag);
    window.localStorage.setItem("Matching_setting", '');
    window.localStorage.removeItem('Matching_setting');
    
   }

    $scope.user_setting_overview = true;
    $scope.user_setting_account = false;
    $scope.user_setting_plans = false;
	//$scope.user_subscription_status=false;
    $scope.buisness_list = [];
	
	$scope.FnCheckSubscription_email=function($event){
		var event=$event;
		var title='Wait...';
		var msg='unsubscribing will mean ' +$scope.bussinessname+ ' will not feature in these updates and miss out on reaching out other businesses. Are you sure you wish to unsubscribe?';
		var checkboxvalue=$scope.weekly_business_email_sub_status;
		var fieldname='weekly_business_email_sub_status';
		if($scope.bussinessname!='' || $scope.bussinessname!=null){
			if(checkboxvalue==false){
			$timeout(function(){	
			$scope.$apply(function(){
				
			alertify.confirm(title,msg,function(e){
					if(e){
						
							UserProxy.FnSaveSubscription(fieldname,checkboxvalue,function(res){
								if(res.response=='success')
								{
								  //alertify.success("Subscription Status Changed Successfully");
								}
							});
						}
				},function(ev){
					if(ev){
				
						if($scope.weekly_business_email_sub_status ==false){
							$scope.weekly_business_email_sub_status=true;
							$scope.$digest();
						}
					}
				});
			  });
			  },50);
			}else{
				UserProxy.FnSaveSubscription(fieldname,checkboxvalue,function(res){
					if(res.response=='success')
					{
					 // alertify.success("Subscription Status Changed Successfully");
					}
		
				});
			}
		}else{
			
			UserProxy.FnSaveSubscription(fieldname,checkboxvalue,function(res){
				if(res.response=='success')
				{
				 // alertify.success("Subscription Status Changed Successfully");
				}
			
			});
		}
		
 }
	$scope.FnCheckSubscription_news=function(){
		var checkboxvalue=$scope.weekly_bing_news_sub_status;
		var fieldname='weekly_bing_news_sub_status';
		
		
		UserProxy.FnSaveSubscription(fieldname,checkboxvalue,function(res){
			if(res.response=='success')
			{
			 //alertify.success("Subscription Status Changed Successfully");
			}
			
		});
		//alert(subscriptionvalue);
		
	}
	$scope.FnCheckSubscription_new_msg=function(){
		var checkboxvalue=$scope.connection_message_sub_status;
		var fieldname='connection_message_sub_status';
		
		
		UserProxy.FnSaveSubscription(fieldname,checkboxvalue,function(res){
			if(res.response=='success')
			{
			  //alertify.success("Subscription Status Changed Successfully");
			}
			
		});
		//alert(subscriptionvalue);
		
	}
	
    $scope.showUserSetting = function () {
        $scope.user_setting_overview = true;
        $scope.user_setting_account = false;
        $scope.user_setting_plans = false;
    };
    $scope.showAccountSetting = function () {
        $scope.user_setting_overview = false;
        $scope.user_setting_account = true;
        $scope.user_setting_plans = false;
    };
    $scope.showPlansSetting = function () {
        $scope.user_setting_overview = false;
        $scope.user_setting_account = false;
        $scope.user_setting_plans = true;
    };
    $scope.wizardOne = {
        show_buisness_list: true,
        show_top_list: false,
        show_sub_sector_list: false
    }
    UserProxy.isCodeExist(function (result) {
        if (result == 'true') {
            $scope.type = 'insert_old_customer_type';
        } else {
            $scope.type = 'insert_new_customer_type';
        }
    });
    $scope.descriptionSetting = function () {
        $('#wizoneModal').modal('show');
    };
    $scope.industry_list_for_wizard_two = [];
    UserProxy.getIndustryList(function (result) {
        //console.log(result);
        $scope.industry_list_for_wizard_two = result.industry_list;
    });
    $scope.matchigSetting = function () {
        if ($scope.type == 'insert_old_customer_type') {
            $('#myModal').modal('show');
        } else {
            $scope.show_wizard_notice = true;
            $scope.show_industry_list = false;
            $scope.show_target_sector = false;
            $scope.show_target_list = false;
            $scope.show_looking_for = false;
            $('#myWizTwoFirstModal').modal('show');
        }
    };
    $scope.openWizardTwoForNewUser = function () {
        $scope.show_wizard_notice = false;
        $scope.show_industry_list = true;
    };
    $scope.show_name_form = false;
    $scope.show_email_form = false;
    $scope.showEditName = function () {
        $scope.show_name_form = $scope.show_name_form == false ? true : false;
    }
    $scope.showEditMail = function () {
        $scope.show_email_form = $scope.show_email_form == false ? true : false;
    }
    $scope.show_industry_list = true;
    $scope.show_target_sector = false;
    $scope.selectedIndustryCode = [];
    $scope.selectedSubsector = [];
    $scope.user_selected_buisness = [];
    $scope.user_email = '';
    $scope.bussinessname = '';
    $scope.buisness_top_lists = [];
    $scope.user_selected_toplines = [];
    $scope.sub_sector_list = [];
    $scope.user_selected_sub_sector = [];
    $scope.user_target_sub_sector = [];
    $scope.user_own_sub_sector = [];
    $scope.user_password_validate = '';
    $scope.selectedTarget = [];
    $scope.selectedLookingFor = [];
    $scope.formData = {
        email: ''
    }
    $scope.form_cred_data = {
        // cur_pass: '',
        password: '',
        password_c: ''
    }
    $scope.form_name_data = {
        first_name: '',
        last_name: ''
    }
    UserProxy.getCurrentUser(function (result) {
		//console.log("getCurrentUser");
		//console.log(result);
        $scope.user_email = result.user_info.email;
        $scope.bussinessname = result.user_info.bussinessname;
        $scope.form_name_data.first_name = result.user_info.fname;
        $scope.form_name_data.last_name = result.user_info.lname;
        $scope.formData.email = result.user_info.email;
		$scope.weekly_business_email_sub_status = result.user_info.weekly_business_email_sub_status;
		$scope.weekly_bing_news_sub_status = result.user_info.weekly_bing_news_sub_status;
		//$scope.messages_email_sub_status = result.user_info.messages_email_sub_status;
		$scope.connection_message_sub_status = result.user_info.connection_message_sub_status;
		if($scope.weekly_business_email_sub_status==1){
			$scope.weekly_business_email_sub_status=true;
		}
		else{
			$scope.weekly_business_email_sub_status=false;
		}
		if($scope.weekly_bing_news_sub_status==1){
			$scope.weekly_bing_news_sub_status=true;
		}
		else{
			$scope.weekly_bing_news_sub_status=false;
		}
		if($scope.connection_message_sub_status==1){
			$scope.connection_message_sub_status=true;
		}
		else{
			$scope.connection_message_sub_status=false;
		}
		
    });
    UserProxy.getIndustryList(function (result) {
        $scope.industry_list_for_wizard_two = result.industry_list;
        // console.log($scope.industry_list_for_wizard_two);
    });
    UserProxy.getBuisnessList(function (result) {
        //console.log(result);
        $scope.buisness_list = result;
    });
    UserProxy.getTopList(function (result) {
        //console.log(result);
        $scope.buisness_top_lists = result.top_list;
    });
    UserProxy.getUserTargetSubSector(function (result) {
        $scope.user_target_sub_sector = result;
    });
    UserProxy.getUserOwnSubSector(function (result) {
        $scope.user_own_sub_sector = result;
    });
    $scope.openTopList = function () {
        if ($scope.user_selected_buisness.length == 0) {
            //alertify.error('Please select your buisness');
            return false;
        } else {
            $scope.wizardOne.show_buisness_list = false;
            $scope.wizardOne.show_top_list = true;
            $scope.wizardOne.show_sub_sector_list = false;
        }
    }
    $scope.showBuisnessList = function () {
        $scope.wizardOne.show_buisness_list = true;
        $scope.wizardOne.show_top_list = false;
        $scope.wizardOne.show_sub_sector_list = false;
    }
    $scope.setTopList = function (toplist) {
        var is_removed = 0;
        if ($scope.user_selected_toplines.length > 0) {
            for (var i = 0; i < $scope.user_selected_toplines.length; i++) {
                if ($scope.user_selected_toplines[i].top_code == toplist.top_code) {
                    var index = $scope.user_selected_toplines.indexOf(toplist);
                    var val = $scope.user_selected_toplines.splice(index, 1);
                    //console.log(val);
                    if (val.length > 0) {
                        is_removed++;
                    }
                }
            }
            if (is_removed == 0)
                $scope.user_selected_toplines.push(toplist);
        } else {
            $scope.user_selected_toplines.push(toplist);
        }
        //console.log($scope.user_selected_toplines);
    }
    $scope.setUserSelectedSubSector = function (sel_sub_sector) {
        var is_removed = 0;
        if ($scope.user_selected_sub_sector.length > 0) {
            for (var i = 0; i < $scope.user_selected_sub_sector.length; i++) {
                if ($scope.user_selected_sub_sector[i].code == sel_sub_sector.code) {
                    var index = $scope.user_selected_sub_sector.indexOf(sel_sub_sector);
                    var val = $scope.user_selected_sub_sector.splice(index, 1);
                    if (val.length > 0) {
                        is_removed++;
                    }
                }
            }
            if (is_removed == 0)
                $scope.user_selected_sub_sector.push(sel_sub_sector);
        } else {
            $scope.user_selected_sub_sector.push(sel_sub_sector);
        }
        //console.log($scope.user_selected_sub_sector);
    }
    $scope.sendCombinationCode = function () {
        if ($scope.user_selected_sub_sector.length == 0) {
           // alertify.error("Please select sub sector");
        } else {
            UserProxy.sendCombinationCodeObj(angular.toJson($scope.user_selected_sub_sector), angular.toJson($scope.user_selected_buisness), '', function (result) {
                if (result.success == 'true') {
                    //alertify.success("inserted successfully");
                    $scope.wizardOne.show_buisness_list = true;
                    $scope.wizardOne.show_top_list = false;
                    $scope.wizardOne.show_sub_sector_list = false;
                    $('#wizoneModal').modal('hide');
                   // debugger;
                    $scope.user_selected_sub_sector = [];
                    $scope.user_selected_buisness = [];
                    $scope.user_selected_toplines = [];
                    $('.effect__click').removeClass('flipped');
                    $('.sub_sector').removeClass('selectedsub');
                    UserProxy.getUserOwnSubSector(function (result) {
                        $scope.user_own_sub_sector = result;
                    });
                } else {
                   // alertify.error('Buisness code already exist');
                }
            });
        }
    }
    $scope.showSubSectorSection = function () {
        if ($scope.user_selected_toplines.length == 0) {
            //alertify.error('Please select atleast one top lines');
            return false;
        }
        UserProxy.subsectorlistToplineWiseObj(angular.toJson($scope.user_selected_toplines), function (result) {
            $scope.sub_sector_list = result;
        });
        $scope.wizardOne.show_buisness_list = false;
        $scope.wizardOne.show_top_list = false;
        $scope.wizardOne.show_sub_sector_list = true;
    }
    $scope.setBuisness = function (buisness) {
        var is_removed = 0;
        if ($scope.user_selected_buisness.length > 0) {
            for (var i = 0; i < $scope.user_selected_buisness.length; i++) {
                if ($scope.user_selected_buisness[i].bussiness_code == buisness.bussiness_code) {
                    var index = $scope.user_selected_buisness.indexOf(buisness);
                    var val = $scope.user_selected_buisness.splice(index, 1);
                    //console.log(val);
                    if (val.length > 0) {
                        is_removed++;
                    }
                }
            }
            if (is_removed == 0)
                $scope.user_selected_buisness.push(buisness);
        } else {
            $scope.user_selected_buisness.push(buisness);
        }
       // console.log($scope.user_selected_buisness);
    }
    $scope.setWizardBackTo = function (seclection) {
        if (seclection === 'one') {
            $scope.show_industry_list = true;
            $scope.show_target_sector = false;
        }
    }
    $scope.setIndustryCode = function (industry_code) {
        var is_removed = 0;
        if ($scope.selectedIndustryCode.length > 0) {
            for (var i = 0; i < $scope.selectedIndustryCode.length; i++) {
                if ($scope.selectedIndustryCode[i].industry_code == industry_code.industry_code) {
                    var index = $scope.selectedIndustryCode.indexOf(industry_code);
                    var val = $scope.selectedIndustryCode.splice(index, 1);
                    //console.log(val);
                    if (val.length > 0) {
                        is_removed++;
                    }
                }
            }
            if (is_removed == 0)
                $scope.selectedIndustryCode.push(industry_code);
        } else {
            $scope.selectedIndustryCode.push(industry_code);
        }
        //console.log($scope.selectedIndustryCode);
    };
    $scope.setSubSectorCode = function (subsector_code) {
        var is_removed = 0;
        if ($scope.selectedSubsector.length > 0) {
            for (var i = 0; i < $scope.selectedSubsector.length; i++) {
                if ($scope.selectedSubsector[i].tar_sector_code == subsector_code.tar_sector_code) {
                    var index = $scope.selectedSubsector.indexOf(subsector_code);
                    var val = $scope.selectedSubsector.splice(index, 1);
                    //console.log(val);
                    if (val.length > 0) {
                        is_removed++;
                    }
                }
            }
            if (is_removed == 0)
                $scope.selectedSubsector.push(subsector_code);
        } else {
            $scope.selectedSubsector.push(subsector_code);
        }
       // console.log($scope.selectedSubsector);

    };
    $scope.sub_sector_list_wiz_2 = [];
    $scope.getTargetSectorByIndustry = function () {
        if ($scope.selectedIndustryCode.length == 0) {
            //alertify.error('Please make a selection');
            return false;
        } else {
            UserProxy.getTargetSectorList(angular.toJson($scope.selectedIndustryCode), function (result) {
                $scope.sub_sector_list_wiz_2 = result;
               // console.log($scope.sub_sector_list_wiz_2);
                $scope.show_industry_list = false;
                $scope.show_target_sector = true;
            });
        }
    };
    $scope.saveUserPasswordSetting = function (settings_pass_object) {
        if (settings_pass_object.password == '') {
           // alertify.error("Please enter new password");
            return false;
        }
        if ($('#password_c').val() == '') {
           // alertify.error("Please enter confirm password");
            return false;
        }
        $('#wiz_password_modal').modal('show');
        $scope.validatePassPassword = function () {
            UserProxy.matchUserPassword($scope.user_password_validate, function (result_validate) {
                if (result_validate.response == 'success') {
                    UserProxy.resetPassword(settings_pass_object.password, function (result) {
                        if (result.response == 'success') {
                            $scope.form_cred_data.password = '';
                            $scope.form_cred_data.password_c = '';
                            $('#wiz_password_modal').modal('hide');
                            $scope.user_password_validate = '';
                        } else {
                            //alertify.error("Please try again");
                        }
                    });
                } else {
                    //alertify.error('Please enter your correct password');
                }
            });
        }
    }
    $scope.saveUserNameSetting = function (settings_object) {
        //console.log(settings_object);
        if (settings_object.first_name == '') {
            //alertify.error("Please enter your first name");
            return false;
        }
        if (settings_object.last_name == '') {
            //alertify.error("Please enter youe last name");
            return false;
        }
        $('#wiz_name_modal').modal('show');
        $scope.validatePassword = function () {
            UserProxy.matchUserPassword($scope.user_password_validate, function (result) {
                if (result.response == 'success') {
                    UserProxy.editName(settings_object, function (result) {
                        if (result.response == 'success') {
                           // alertify.success('Name changed');
                            $scope.showEditName();
                            $('#wiz_name_modal').modal('hide');
                            $scope.user_password_validate = '';
                        } else {
                           // alertify.error("Please try again");
                        }
                    });
                } else {
                    //alertify.error('Please enter your correct password');
                }
            });
        }
    }

    $scope.saveMailSettingd = function (settings_object) {
        if (settings_object.email == '') {
            //alertify.error("Please enter new email id");
            return false;
        }

        if (settings_object.email !== undefined) {
            $('#wiz_email_modal').modal('show');
            $scope.validateEmailPassword = function () {
                UserProxy.matchUserPassword($scope.user_password_validate, function (result_validate) {
                    if (result_validate.response == 'success') {
                        UserProxy.userEmailValidate(settings_object.email, function (result) {
                            if (result.success == false) {
                                UserProxy.updatProfile(settings_object.email, function (result_data) {
                                    if (result_data.response == 'success') {
                                        //alertify.success("Email updated");
                                        $scope.formData = {
                                            email: ''
                                        }
                                        $('#wiz_email_modal').modal('hide');
                                        $scope.user_password_validate = '';
                                        UserProxy.setAccessToken(settings_object.email);
                                    } else {
                                        $scope.user_password_validate = '';
                                       // alertify.error("Please try again");
                                        $('#wiz_email_modal').modal('hide');
                                    }
                                });
                            } else {
                                $scope.user_password_validate = '';
                               // alertify.error('This email address is already registered');
                                $('#wiz_email_modal').modal('hide');
                            }
                        });
                    } else {
                        $scope.user_password_validate = '';
                       // alertify.error('Please enter your correct password');
                        $('#wiz_email_modal').modal('hide');
                    }
                });
            }
        }
    }
    $scope.checkBusinessCodeExitForUser = function () {
        if ($scope.selectedSubsector.length == 0) {
           // alertify.error('Please make a selection');
            return false;
        } else {
            // UserProxy.checkBusinessCodeExitForUser($scope.selectedIndustryCode[0].industry_code, $scope.selectedSubsector[0].tar_sector_code, function (result) {
            UserProxy.checkBusinessCodeExitForUser(angular.toJson($scope.selectedIndustryCode), angular.toJson($scope.selectedSubsector), function (result) {
                if (result == 'true') {
                    alertify.alert('This code is already exits.Please change it.');
                } else {
                    if ($scope.type != 'insert_old_customer_type') {
                        UserProxy.lookingServicesList(function (result) {
                           // console.log(result);
                            $scope.target_sector_list = result.target_sector_list;
                            $scope.show_industry_list = false;
                            $scope.show_target_sector = false;
                            $scope.show_looking_for = false;
                            $scope.show_locality = false;
                            $scope.show_services = false;
                            $scope.show_target_list = true;
                        });
                    } else {
                        UserProxy.setupWizTwoApi('insert_old_customer_type', angular.toJson($scope.selectedIndustryCode), angular.toJson($scope.selectedSubsector), '', '', '', '', function (result) {
                            if (result.response = 'success') {
                                $('#myModal').modal('hide');
                               // alertify.success("Successfullt added");
                                $scope.show_industry_list = true;
                                $scope.show_target_sector = false;
                                $('.effect__click').removeClass('flipped');
                                $('.sub_sector').removeClass('selectedsub');
                                $scope.selectedIndustryCode = [];
                                $scope.selectedSubsector = [];
                                UserProxy.getUserTargetSubSector(function (result) {
                                    $scope.user_target_sub_sector = result;
                                });
                            }
                        });
                    }
                }
            });
        }
    }



    $scope.shareHub2 = function () {

       console.log("when shared modal open");
             

       // $('#publish_details').modal('hide');

        $('#myShareModal2').modal('show');

    }




$( "#statusbtn" ).on('click', function(){
   // alert("I want this to appear after the modal has opened!");

   var statusval=$("#status").val();
  // alert(statusval);

   if(statusval ==='DELETE'){
var statusval1=3;

   UserProxy.settingAccountDelete(statusval1, function (result) {

            if (result.response == 'success') {

              // alertify.success("Report posted successfully");
               $('#myShareModal2').modal('hide');

               UserProxy.unsetAccessToken();
                 setTimeout(function () {
                window.location = "logout";
            }, 200);

            }
            else{


              //alertify.success("Report not posted successfully");
               $('#myShareModal2').modal('hide');
            }

        });
} else{

            //alertify.error("Please Insert DELETE In InputBox");
               $('#myShareModal2').modal('show');

}




  // alert(statusval);
});





    $scope.setTarget = function (target) {
        var is_removed = 0;
        if ($scope.selectedTarget.length > 0) {
            for (var i = 0; i < $scope.selectedTarget.length; i++) {
                if ($scope.selectedTarget[i].look_service_id == target.look_service_id) {
                    var index = $scope.selectedTarget.indexOf(target);
                    var val = $scope.selectedTarget.splice(index, 1);
                    //console.log(val);
                    if (val.length > 0) {
                        is_removed++;
                    }
                }
            }
            if (is_removed == 0)
                $scope.selectedTarget.push(target);
        } else {
            $scope.selectedTarget.push(target);
        }
        //console.log($scope.selectedTarget);
    };
    $scope.fetchServices = function () {
        if ($scope.selectedTarget.length > 0) {
            UserProxy.getLookingForList(function (result) {
                //console.log(result);
                $scope.looking_for_list = result;
                $scope.show_industry_list = false;
                $scope.show_target_sector = false;
                $scope.show_target_list = false;
                $scope.show_locality = false;
                $scope.show_services = false;
                $scope.show_looking_for = true;
            });

        } else {
            //alertify.error('Please make a selection');
            return false;
        }
    };
    $scope.setLookingFor = function (looking_for) {
        var is_removed = 0;
        if ($scope.selectedLookingFor.length > 0) {
            for (var i = 0; i < $scope.selectedLookingFor.length; i++) {
                if ($scope.selectedLookingFor[i].looking_for_id == looking_for.looking_for_id) {
                    var index = $scope.selectedLookingFor.indexOf(looking_for);
                    var val = $scope.selectedLookingFor.splice(index, 1);
                    //console.log(val);
                    if (val.length > 0) {
                        is_removed++;
                    }
                }
            }
            if (is_removed == 0)
                $scope.selectedLookingFor.push(looking_for);
        } else {
            $scope.selectedLookingFor.push(looking_for);
        }
       // console.log($scope.selectedLookingFor);
    };
    $scope.saveDataForWizard = function () {
        //alert($scope.selectedTarget.length); return false;
        if ($scope.selectedLookingFor.length > 0) {
            UserProxy.setupWizTwoApi($scope.type, angular.toJson($scope.selectedIndustryCode), angular.toJson($scope.selectedSubsector), angular.toJson($scope.selectedLookingFor), angular.toJson($scope.selectedLocality), $scope.is_consumer == true ? 1 : 2, angular.toJson($scope.selectedTarget), function (result) {
                if (result.response = 'success') {
                    $('#myWizTwoFirstModal').modal('hide');

                    $scope.selectedIndustryCode = [];
                    $scope.show_industry_list = true;
                    $scope.show_target_sector = false;
                    $scope.show_looking_for = false;
                    $scope.show_locality = false;
                    $scope.show_services = false;
                    $scope.show_target_list = false;
                    UserProxy.getUserTargetSubSector(function (result) {
                        $scope.user_target_sub_sector = result;
                    });
                    alertify.alert('You have now set up your matching. See who Arlians suggests you connect with today, using the my suggestions section.');

                    $scope.type = 'insert_old_customer_type';
                } else {
                    //alertify.error('Some error occured.Please try again');
                }

            });
        } else {
            //alertify.error('Please make a selection');
        }
    };
    $scope.deleteTargetSector = function (target_sector, $event) {
        //console.log(target_sector);
        var event = $event;
        alertify.confirm("Are you want to delete " + target_sector.targetsubname, function (e) {
            if (e) {
                UserProxy.deleteTargetSector(target_sector, function (result) {
                    if (result.response == "success") {
                        $(event.target).parent().remove();
                    }
                });
            }
        });
    }
    $scope.selected_sub_sector = [];
    $scope.openModalforsubsector = function (sector) {
       // console.log(sector);
        UserProxy.getSelectedSubsector(sector, function (result) {
          //  console.log(result);
            $scope.selected_sub_sector = result;
        });
        $('#selected_sub_sector').modal('show');
    }
    $scope.deleteSubSector = function (buis_code, $event) {
        var event = $event;
        alertify.confirm("Are you want to delete " + buis_code.bussiness_name, function (e) {
            if (e) {
                UserProxy.removeUserDetailFromWizOne(buis_code, function (result) {
                    if (result.response == "success") {
                        $(event.target).parent().remove();
                        UserProxy.getUserOwnSubSector(function (result) {
                            //debugger;
                            $scope.user_own_sub_sector = [];
                            $scope.user_own_sub_sector = result;
                        });
                    }
                });
            }
        });

    }
}
).directive('validPasswordC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.myForm.password.$viewValue
                ctrl.$setValidity('noMatch', !noMatch)
            })
        }
    }
});
function animationCard(cur_obj, class_name) {
    if ($(cur_obj).hasClass('flipped')) {
        $(cur_obj).removeClass('flipped');
    } else {
        $(class_name).each(function () {
            $(this).removeClass('flipped');
        });
        $(cur_obj).addClass('flipped');
    }
}
function animatedSubSector(cur_obj) {
    if ($(cur_obj).hasClass('selectedsub')) {
        $(cur_obj).removeClass('selectedsub');
    } else {
        $(cur_obj).addClass('selectedsub');
    }
}
function animationBuisnessTopLines(cur_obj) {
    if ($(cur_obj).hasClass('flipped')) {
        $(cur_obj).removeClass('flipped');
    } else {
        $(cur_obj).addClass('flipped');
    }
}

