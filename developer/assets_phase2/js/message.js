arlians.controller('messageCtrl', function ($scope, $timeout, $http) {
    $scope.message = {
        message_sub: '',
        message_content: ''
	};
	
    $scope.inbox_list = [];
    $scope.inbox_list_by_id = [];
	$scope.inbox_list_by_id_on_sent_recv=[];
    var message_send_to = [];
    $scope.sent_msg_list = [];
    $scope.connection_list = [];
    $scope.combinemsg = false;
    UserProxy.getCurrentUser(function (result) {
        $scope.user_mid = result.user_info.mid;
    });
	$scope.refresh_message_page = function(){
		$scope.message = {
        message_sub: '',
        message_content: ''
		
    };
	
	$(".token-input-list").children("li.token-input-token").children("p").remove();
	$(".token-input-list").children("li.token-input-token").children("span").remove();
	$scope.show_chain = false;
	};
	
    $scope.sendMessage = function (message) {
	
        if ($scope.message.message_sub == '') {
            //alertify.error('Please enter the subject');
            return false;
        }
		
        if ($scope.message.message_content == '') {
            //alertify.error('Please enter message');
            return false;
        }
        $(".token-input-token").each(function () {
			var b_name=$(this).find('p').text();
			var res_b_name=b_name.split("-");
			console.log(res_b_name[1]);
            message_send_to.push(res_b_name[1]);
			//message_send_to.push($(this).find('p').text());
        });
		//alert(message_send_to);
        if (message_send_to.length == 0) {
           // alertify.error('Please select the sender');
            return false;
        }
		
        if ($scope.combinemsg == true) {
            message.message_content = message.message_content + '<br/>' + $scope.chain_msg;
			
        }
		$("#circularG").css("display","block");
		
        UserProxy.sendMessageFromMessage(message_send_to, message.message_sub, message.message_content, function (result) {
			
		
            if (result.response == 'success') {
					
				$('.token-input-token').remove();
				message_send_to=[];
                $scope.message.message_content = '';
				
                $scope.message.message_sub = '';
                $scope.chain_msg = '';
                $scope.expand = false;
                $scope.show_chain = true;
                $scope.combinemsg = false;
                $scope.getInboxMessage();
				UserProxy.getSentApi(function (result) {
				  $scope.sent_msg_list = result;
				 
                });
				$("#circularG").css("display","none");
            }
			
        });
    };
    $scope.show_chain = false;
    $scope.expand = false;
    $scope.chain_msg = '';
    $scope.getInboxMessage = function () {
        UserProxy.getInboxApi(function (result) {
			console.log("inbox_list");
			console.log(result);
            $scope.inbox_list = result;
        });
    }
    $scope.getInboxMessage();
    $scope.showPreviousMail = function () {
        $scope.show_chain = true;
        $scope.expand = false;
    }
    UserProxy.getNetworkConnectionList(function (result) {
        $scope.connection_list = result.connection;
    });

    UserProxy.getSentApi(function (result) {
         console.log("result");
        console.log(result);

        $scope.sent_msg_list = result;
    });
    $scope.select_inbox_msg = [];
    $scope.selectlist = function (inbox_obj) {
		
		$scope.select_inbox_msg.push(inbox_obj.msg_id);
       /*var is_removed = 0;
        if ($scope.select_inbox_msg.length > 0) {
            for (var i = 0; i < $scope.select_inbox_msg.length; i++) {
                if ($scope.select_inbox_msg[i].msg_id == inbox_obj.msg_id) {
                    var index = $scope.select_inbox_msg.indexOf(inbox_obj);
                    var val = $scope.select_inbox_msg.splice(index, 1);
                    //console.log(val);
                    if (val.length > 0) {
                        is_removed++;
                    }
                }
            }
            if (is_removed == 0)
                //$scope.select_inbox_msg.push(inbox_obj);
				$scope.select_inbox_msg.push(inbox_obj.msg_id);
        } else {
            //$scope.select_inbox_msg.push(inbox_obj);
			  $scope.select_inbox_msg.push(inbox_obj.msg_id);	
        }*/
		
        //console.log($scope.select_inbox_msg);
    }
    $scope.deleteSelect = function () {
        if ($scope.select_inbox_msg.length == 0) {
            //alertify.error("Please select a message");
            return false;
        }
		
	
        //console.log($scope.select_inbox_msg);
        UserProxy.deleteSelectMessage($scope.select_inbox_msg, function (result) {
			console.log("delete message");
			console.log(result);
			//return false;
				 //alert($scope.select_inbox_msg[0].msg_id);
            if (result.response = 'success') {
					
                for (var i = 0; i < $scope.select_inbox_msg.length; i++) {
                    var index = $scope.inbox_list.indexOf($scope.select_inbox_msg[i].msg_id);
                    $scope.inbox_list.splice(index, 1);
					$('#m_'+$scope.select_inbox_msg[i].msg_id).remove();

                }
                
            }
			//location.reload(true);
        });

    };

    $scope.replyMessage = function (msg_obj) {
		
        console.log('replyMessage');
        console.log(msg_obj);
	
		$("#circularG").delay(10000).show();
		UserProxy.getMessageByMsgId(msg_obj.msg_sub,function(result){
			alert("hii");
			
			$("#circularG").delay(4000).hide();
			
			
				console.log("text");
				console.log(result);
				$scope.inbox_list_by_id=result;
				
			 });
			$('.token-input-token').remove();
			$scope.message.message_sub = msg_obj.msg_sub;
				if($scope.message.message_sub!='' || $scope.message.message_sub!=null){
					$("#msgg_sub").prop("disabled","true");	
				}
			$scope.chain_msg = '<span class="msg_send">'+msg_obj.msgdate + '</span>'+'<span class="msg_send">'+msg_obj.msg_sub+'</span>';
			
			$scope.expand = true;
			$scope.show_chain = false;
			$scope.combinemsg = true;
			if(msg_obj.email!=''){
				//var html = '<li class="token-input-token"><p value="' + msg_obj.mt_id + '">' + msg_obj.bussinessname + '</p><span  class="token-input-delete-token">×</span></li>'
				var html = '<li class="token-input-token"><p value="' + msg_obj.mt_id + '">' + msg_obj.bussinessname + '</p><p style="display:none"; value=" ' + msg_obj.mt_id + '">-' + msg_obj.email + '</p><span  class="token-input-delete-token">×</span></li>'
				
				
			}else{
				
				var html = '<li class="token-input-token"><p value="' + msg_obj.mt_id + '">' + msg_obj.fname + ' '+ msg_obj.lname+'</p><span class="token-input-delete-token">×</span></li>'
				
			}
			$('.token-input-list').prepend(html);
			 UserProxy.msgStatusChange(msg_obj.mt_id, function (result) {
				 console.log("tesing")
				 console.log(result);
				
			 });	
			
			
			
			
			
			
			/*$('.token-input-token').remove();
			$scope.message.message_sub = result.subject;
			$scope.chain_msg = '<span class="msg_send">'+msg_obj.msgdate + '</span>'+'<span class="msg_send">'+ result.subject+'</span>';
			
			$scope.expand = true;
			$scope.show_chain = false;
			$scope.combinemsg = true;
			if(result.replyemailaddress!=''){
				var html = '<li class="token-input-token"><p value="' + msg_obj.mt_id + '">' + result.replybusinessname + '</p><span  class="token-input-delete-token">×</span></li>'
		   
				
			}else{
				
				var html = '<li class="token-input-token"><p value="' + msg_obj.mt_id + '">' + msg_obj.fname + ' '+ msg_obj.lname+'</p><span class="token-input-delete-token">×</span></li>'
			}
			$('.token-input-list').prepend(html);
			 UserProxy.msgStatusChange(msg_obj.mt_id, function (result) {
				 console.log("tesing")
				 console.log(result);
			 });	*/

			
    }
	$scope.Sent_Receive_msg=function(msg_object){
		console.log(msg_object);
		UserProxy.getMessageByMsgId(msg_object.msg_id,function(res){
			$scope.inbox_list_by_id_on_sent_recv=res;
			$("#sentReceiveModal").modal('show');
		});
	}
	
$('#RefreshBtn').on("click",function(){

    location.reload(true); 
    
})


	
});




