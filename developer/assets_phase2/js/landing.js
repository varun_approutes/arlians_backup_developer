﻿// loading.js
        var arlians = angular.module('arliansApp', ['directive.g+signin', 'UserEmailCnfValidation']);/*'directive.g+signin'  'UserEmailCnfValidation'*/
arlians.controller('LandingCtrl', function ($timeout, $scope) {
    $scope.third_page_signin_section = true;
    $scope.third_page_login_section = false;
    $scope.third_page_validate_sign_section = false;
    $scope.showThirdPageSignIn = function () {
        $scope.third_page_signin_section = true;
        $scope.third_page_login_section = false;
        $scope.third_page_validate_sign_section = false;
    }
    $scope.showThirdPageLogin = function () {
        $scope.third_page_signin_section = false;
        $scope.third_page_login_section = true;
        $scope.third_page_validate_sign_section = false;
    }

    $scope.user_login = {};
    $scope.user_email = {};
    $scope.user_forget_credentials = {};
    $scope.google_user = {};
    $scope.sign_in_section = true;
    $scope.login_section = false;
    $scope.user_login_section = true;
    $scope.user_forget_credentials_section = false;
    $scope.user_email = UserProxy.getAccessToken();
    var filters = [
        {
            'filterId': 0,
            'name': 'Sign up'
        },
        {
            'filterId': 1,
            'name': 'Login'
        }
    ];
    $scope.filters = filters;
    $scope.selected = 0;
    $scope.select = function (index) {
        if (index == 0) {
            $scope.sign_in_section = true;
            $scope.login_section = false;
        } else {
            $scope.sign_in_section = false;
            $scope.login_section = true;
        }
        $scope.selected = index;
    };
    $scope.showPgeOneSignUp = function(){    
        $scope.selected = 0;  
        $scope.sign_in_section = true;
        $scope.login_section = false;
    };
    $scope.showPgeOneLogin = function(){  
        $scope.selected = 1;      
        $scope.sign_in_section = false;
        $scope.login_section = true;
    };
    if ($scope.user_email == '' || $scope.user_email == 0 || $scope.user_email == null) {
        $scope.user_email_section = true;
        $scope.user_validat_email_and_password_section = false;
        $scope.user_validat_email_and_password_section_1 = false;
        $scope.user_email = {};
        $scope.sign_in_user = {};
        $scope.user_details = {};
        $scope.showForgetCrendentialsSection = function () {
            $scope.user_login_section = false;
            $scope.user_forget_credentials_section = true;
        };
        $scope.showLoginSection = function () {
            $scope.user_login_section = true;
            $scope.user_forget_credentials_section = false;
        };
        $scope.forgetCrendentials = function (user_forget_credentials) {
            if (user_forget_credentials.user_email === undefined) {
                alertify.error("Please enter a valid email");
                return false;
            }
            UserProxy.forgetCredential(user_forget_credentials, function (result) {
                if (result.success == 1) {
                    alertify.success("An email has beem forwarded to your eamil address for reset your password");
                    user_forget_credentials.user_email = '';
                    $scope.user_login_section = true;
                    $scope.user_forget_credentials_section = false;
                } else {
                    alertify.error('Your email address has not been registered!');
                }
            });
        };
        $scope.validateUserEmail = function (user_email) {
            //debugger;
            if (user_email.user_email === undefined) {
                alertify.error("Please enter a valid email");
                return false;
            }

            UserProxy.userEmailValidate(user_email.user_email, function (result) {
                if (result.success === false) {
                    $('.sign_up_animation').removeClass('fadeInUp').addClass('element-animation');
                    $timeout(function () {
                        $scope.user_email_section = false;
                        $scope.user_validat_email_and_password_section = true;
                        $scope.third_page_signin_section = false;
                        $scope.third_page_validate_sign_section = true;
                    }, 700);
                } else {
                    //debugger;
                    alertify.error('This email address is already registered');
                }
            });
        };
        $scope.googleLogin = function () {
            alert('hiii');
            $('#gConnect').trigger('click');
        };
        $scope.registeredUser = function (sign_in_user, user_email) {
            if (sign_in_user.confirm_user_email == undefined) {
                alertify.error("Please enter previous email to validate");
                return false;
            }
            if (sign_in_user.confirm_user_email != user_email.user_email) {
                alertify.error("Please enter previous email id to validate");
                return false;
            } 
            if (sign_in_user.user_password  == undefined || sign_in_user.user_password == '') {
                alertify.error("Please enter password");
                return false;
            }else {
                UserProxy.signUpMember(sign_in_user, function (result_data) {
                    if (result_data.success == true) {
                        UserProxy.setAccessToken(sign_in_user.confirm_user_email);
                        window.location.href = 'member/setup_wiz_one';
                    } else {
                        alertify.error("This is embrassing.Some error has occured !");
                    }
                });
            }
        };
        $scope.validateUser = function (user_login) {
            //console.log(user_login);
            if (user_login.user_email === undefined || user_login.user_email == '') {
                alertify.error("Please enter a valid email");
                return false;
            }
            if (user_login.user_password === undefined || user_login.user_password == '') {
                alertify.error("Please enter password");
                return false;
            }
            UserProxy.authenticate(user_login, function (result_data) {
                //console.log(result_data);
                //return false;
                if (result_data.validate == false) {
                    alertify.error("Invalid login credentials");
                } else {
                    if (result_data.status == 1) {
                        UserProxy.setAccessToken(user_login.user_email);
                        window.location = result_data.redirect;
                    } else if (result_data.status == 2) {
                        UserProxy.setAccessToken(user_login.user_email);
                        window.location = result_data.redirect;
                    } else if (result_data.status == 3) {
                        result_data.user_email = '';
                        result_data.user_password = '';
                        alertify.error(result_data.error_message);
                    } else if (result_data.status == 0) {
                        alertify.error(result_data.error_message);
                        //alertify.alert(result_data.error_message);
                    } else {                        
                        UserProxy.setAccessToken(user_login.user_email);
                        window.location.href = 'member/setup_wiz_one';
                    }
                }
            });
        };
        $scope.$on('event:google-plus-signin-success', function (event, authResult) {
            gapi.client.plus.people.get({
                'userId': 'me'
            }).then(function (res) {
                var profile = res.result;
                console.log(profile);
                $scope.google_user.email = profile.emails[0].value;
                $scope.google_user.fname = profile.name.givenName;
                $scope.google_user.lname = profile.name.familyName;
                $scope.google_user.type = 'google_plus';
                $scope.google_user.social_id = profile.id;
                console.log($scope.google_user);
                UserProxy.socialUserValidation($scope.google_user, function (response) {
                    if (response.process == 'success') {
                        UserProxy.setAccessToken($scope.google_user.email);
                        window.location = "member/" + response.landing_page;
                    } else {
                        alertify.error("Error occured please try again.");
                    }
                });
            }, function (err) {
                var error = err.result;
                alertify.error(error.message);
            });
        });
        $scope.$on('event:google-plus-signin-failure', function (event, authResult) {
            console.log('Not signed into Google Plus.');
        });
        $scope.finalSubmission = function (sign_in_user) {
            console.log(sign_in_user);
        };
    } else {
        window.location = "member/dashboard";
    }
});
var auth2 = {};
var helper = (function () {
    return {
        /**
         * Hides the sign in button and starts the post-authorization operations.
         *
         * @param {Object} authResult An Object which contains the access token and
         *   other authentication information.
         */
        onSignInCallback: function (authResult) {
            //debugger;
            $('#authResult').html('Auth Result:<br/>');
            for (var field in authResult) {
                $('#authResult').append(' ' + field + ': ' +
                        authResult[field] + '<br/>');
            }
            console.log(authResult.isSignedIn.get());
            if (authResult.isSignedIn.get()) {
                $('#authOps').show('slow');
                $('#gConnect').hide();
                helper.profile();
                //helper.people();
            } else {
                if (authResult['error'] || authResult.currentUser.get().getAuthResponse() == null) {
                    // There was an error, which means the user is not signed in.
                    // As an example, you can handle by writing to the console:
                    console.log('There was an error: ' + authResult['error']);
                }
                $('#authResult').append('Logged out');
                $('#authOps').hide('slow');
                $('#gConnect').show();
            }
            console.log('authResult', authResult);
        },
        /**
         * Calls the OAuth2 endpoint to disconnect the app for the user.
         */
        disconnect: function () {
            // Revoke the access token.
            auth2.disconnect();
        },
        /**
         * Gets and renders the list of people visible to this app.
         */
        people: function () {
            gapi.client.plus.people.list({
                'userId': 'me',
                'collection': 'visible'
            }).then(function (res) {
                var people = res.result;
                $('#visiblePeople').empty();
                $('#visiblePeople').append('Number of people visible to this app: ' +
                        people.totalItems + '<br/>');
                for (var personIndex in people.items) {
                    person = people.items[personIndex];
                    $('#visiblePeople').append('<img src="' + person.image.url + '">');
                }
            });
        },
        /**
         * Gets and renders the currently signed in user's profile data.
         */
        profile: function () {
            gapi.client.plus.people.get({
                'userId': 'me'
            }).then(function (res) {
                var profile = res.result;
                console.log(profile);
                $('#profile').empty();
                $('#profile').append(
                        $('<p><img src=\"' + profile.image.url + '\"></p>'));
                $('#profile').append(
                        $('<p>Hello ' + profile.displayName + '!<br />Tagline: ' +
                                profile.tagline + '<br />About: ' + profile.aboutMe + '</p>'));
                if (profile.emails) {
                    $('#profile').append('<br/>Emails: ');
                    for (var i = 0; i < profile.emails.length; i++) {
                        $('#profile').append(profile.emails[i].value).append(' ');
                    }
                    $('#profile').append('<br/>');
                }
                if (profile.cover && profile.coverPhoto) {
                    $('#profile').append(
                            $('<p><img src=\"' + profile.cover.coverPhoto.url + '\"></p>'));
                }
            }, function (err) {
                var error = err.result;
                $('#profile').empty();
                $('#profile').append(error.message);
            });
        }
    };
})();
function startApp() {
    gapi.load('auth2', function () {
        gapi.client.load('plus', 'v1').then(function () {
            gapi.signin2.render('signin-button', {
                scope: 'https://www.googleapis.com/auth/plus.login',
                fetch_basic_profile: false});
            gapi.auth2.init({fetch_basic_profile: false,
                scope: 'https://www.googleapis.com/auth/plus.login'}).then(
                    function () {
                        console.log('init');
                        auth2 = gapi.auth2.getAuthInstance();
                        auth2.isSignedIn.listen(updateSignIn);
                        auth2.then(updateSignIn);
                    });
        });
    });
}
function googleLogin() {
    $('#clientid').trigger('click');
}
;
var updateSignIn = function () {
    console.log('update sign in state');
    if (auth2.isSignedIn.get()) {
        console.log('signed in');
        helper.onSignInCallback(gapi.auth2.getAuthInstance());
    } else {
        console.log('signed out');
        helper.onSignInCallback(gapi.auth2.getAuthInstance());
    }
}
/************** custom directive for email check ******************/
angular.module('UserEmailCnfValidation', []).directive('validEmailC', function () {
    return {
        require: 'ngModel',
        link: function (scope, elm, attrs, ctrl) {
            ctrl.$parsers.unshift(function (viewValue, $scope) {
                var noMatch = viewValue != scope.userMail.user_email.$viewValue;
                ctrl.$setValidity('noMatch', !noMatch);
            });
        }
    };
});
