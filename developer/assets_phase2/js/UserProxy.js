var UserProxy = {

    //serviceURL: "http://arlians.online-seo-service.co.uk", 

    // serviceURL: "http://localhost/arlians/social-network",

    //serviceURL: baseUrls,
   serviceURL: "http://arlians.com/developer",
 
   // serviceURL: CORE_URL,

    errorHandler: null, // function reference

    getHeaders: function () {

        var token = this.getAccessToken();

        //return token != 0 ? token : ''

        /*return {         

         "USER_TOKEN": token != 0 ? token : ''        

         };*/

    },

    ajaxCall: function (actionUrl, data, callback) {
		console.log(actionUrl);
        if (actionUrl == "hub/moreHubApi" || actionUrl == "member/updateNotificationData" || actionUrl=="community/suggestTag" || 
				actionUrl=="memberprofile/convertUrlToHtml" || actionUrl=="hub/hubDelete" || actionUrl=="hub/hub_post_api"
				|| actionUrl=="message/updateMessageStatus" || actionUrl=="message/FnGetMessagebyMsgId" || 
				actionUrl=="message/msglist_api" || actionUrl=="message/getInboxApi" || actionUrl=="message/getSentApi" 
				|| actionUrl=="message/deleteSelectMessage" || actionUrl=="memberprofile/getNetworkConnectionList" 
				|| actionUrl=="community/perticepant" || actionUrl=="community/access_check" || actionUrl=="community/add_community_comment"
				|| actionUrl=="community/get_community_details" || actionUrl=="community/deleteComment" 
				||  actionUrl=="community/insertSubComment" ||  actionUrl=="community/add_community" || actionUrl=="community/get_community_details"
				||  actionUrl=="community/fetchLatestTalk" ||  actionUrl=="community/delete_community" 
				||  actionUrl=="community/dislike" ||  actionUrl=="community/add_community_like" ||  actionUrl=="hub/userPostLike" 
				||  actionUrl=="hub/userDeleteLike" ||  actionUrl=="hub/postCommentApi" ||  actionUrl=="hub/FnCommentDisLike" ||  actionUrl=="hub/FnCommentLike"
				|| actionUrl=="hub/deleteComment" || actionUrl=="hub/shareWithComment" || actionUrl=="memberprofile/FnUpdateDetsOnce" 
				|| actionUrl=="memberprofile/matchUserPassword" || actionUrl=="memberprofile/editName"  
				|| actionUrl=="member/validateMemberEmail" || actionUrl=="memberprofile/resetPassword" 
				|| actionUrl=="memberprofile/deleteAccount") {   
	
			
			//$("#circularG").css("display", "block");
			//$("#circularG").css("backgroundColor", "red");
            $('#loader-wrapper').hide();
            	
        }else{
			
            $('#loader-wrapper').show();
           
        }

        $.ajax({

            //url:actionUrl,

            url: this.serviceURL + "/" + actionUrl,

            type: "POST",

            //headers: JSON.stringify(this.getAccessToken()),

            //contentType: "application/json; charset=utf-8",

            dataType: "json",

            async: false,

            data: data,

            //data: JSON.stringify(data)

        }).success(function (result) {

		//alert(result);

            $('#loader-wrapper').hide();
           

            if (result.error) {
	//alert("00000000000000000");
			
			alert(result.error, 'error');

                alertify.log(result.error, 'error');

            }

            if (callback)

                callback(result);

        }).fail(function (jqXHR, textStatus, errorThrown) {

            $('#loader-wrapper').hide();
          

            if (callback)

                callback({error: textStatus + ":" + errorThrown});

        });
	  
    }, //-ajaxCall



    authenticate: function (dataObjectUserValidate, callback) {

        this.ajaxCall("member/validateSignUpMember",

                /*{data:dataObjectUserValidate},*/

                        {email: dataObjectUserValidate.user_email, password: dataObjectUserValidate.user_password},

                callback);

            }, //-authenticate

    getAccessToken: function () {

        return CommonHelper.getCookie("FRONT_ACCESS_ARLIANS_TOKEN");

    }, //setAccessToken



    setAccessToken: function (accessToken) {

        //alert(accessToken);

        CommonHelper.setCookie("FRONT_ACCESS_ARLIANS_TOKEN", accessToken);

    }, //setAccessToken



    unsetAccessToken: function () {

        CommonHelper.unsetCookie("FRONT_ACCESS_ARLIANS_TOKEN", 0);

    }, //setAccessToken



    logout: function (callback) {

        this.ajaxCall("User/logout", {user_token: this.getAccessToken()}, callback);

    }, //logout 



    activateUser: function (token, callback) {

        this.ajaxCall("user/activateUser",

                {user_token: token},

        callback);

    }, //activate User	



    forgetCredential: function (forgetCredential, callback) {

        this.ajaxCall("member/forgetCredential",

                {data: forgetCredential.user_email},

        callback);

    }, // fotrget Password



    resetPassword: function (token, password, callback) {

        this.ajaxCall("user/resetPassword",

                {user_token: token, new_password: password},

        callback);

    }, //-resetPassword



    changePassword: function (user_id, password, callback) {

        this.ajaxCall("user/changePassword",

                {user_id: user_id, new_password: password},

        callback);

    }, //changed password



    getCurrentUser: function (callback) {

        this.ajaxCall("member/getUserDetails",

                {user_token: this.getAccessToken},

        //{user_token: encodeURIComponent(DAT.getAccessToken())},

        callback);

    }, // get Current User



    chkEmail: function (chk_email, callback) {

        this.ajaxCall("user/checkEmailValidation",

                {email: chk_email},

        callback);

    }, //chkEmail   

    Unsubscribe: function (email, callback) {

        this.ajaxCall("member/FnUnsubscribe",

                {email: email},

        callback);

    }, //chkEmail  	



    checkPasswordValidation: function (user_id, password, callback) {

        this.ajaxCall("user/checkPasswordValidation",

                {user_id: user_id, password: password},

        callback);

    }, //checkPasswordValidation



    getUserUnreadMailDetails: function (callback) {

        this.ajaxCall("member/getUserUnreadMessage",

                {user_email: this.getAccessToken},

        callback);

    }, //checkPasswordValidation    



    getUserReadMailDetails: function (count, callback) {

        this.ajaxCall("member/getUserReadMessage",

                {count: count, user_email: this.getAccessToken},

        callback);

    }, //checkPasswordValidation    



    getUserNotificationDetails: function (callback) {

        this.ajaxCall("member/fetchWhoViewedMyProfile",

                {user_email: this.getAccessToken},

        callback);

    }, //getUserNotificationDetails



    getUserRequestDetails: function (callback) {

        this.ajaxCall("member/userRequestConnectList",

                {user_email: this.getAccessToken},

        callback);

    }, //getUserRequestDetails



    friendRequestAccepted: function (cid, callback) {

        this.ajaxCall("member/userAcceptRequestForConnection",

                {cid: cid},

        callback);

    }, //friendRequestAccepted



    friendRequestDecline: function (cid, callback) {

        this.ajaxCall("member/userRequestConnectionSeen",

                {cid: cid},

        callback);

    }, //friendRequestDecline



    getUserSuggestion: function (callback) {

        this.ajaxCall("member/dashboardold",

                {user_email: this.getAccessToken},

        callback);

    },

    userEmailValidate: function (user_email, callback) {

        this.ajaxCall("member/validateMemberEmail",

                {user_email: user_email},

        callback);

    },

    signUpMember: function (sign_in_user, callback) {



        this.ajaxCall("explore/userSignUpMember",

                {user_email: sign_in_user.confirm_user_email, password: sign_in_user.user_password},

        callback);



    },

    socialUserValidation: function (social_user, callback) {



        this.ajaxCall("explore/socialUserValidation",

                {email: social_user.email, fname: social_user.fname, lname: social_user.lname, social_id: social_user.social_id, type: social_user.type},

        callback);



    },

    getBuisnessList: function (callback) {



       // this.ajaxCall("category/get_bussiness_list",
       this.ajaxCall("category/get_all_bussiness_list",

                {},

                callback);



    },

    getTopList: function (callback) {



        this.ajaxCall("category/get_top_list",

                {},

                callback);



    },

    subsectorlistToplineWise: function (selected_buisness_top_lines, callback) {



        this.ajaxCall("category/subsectorlistToplineWise",

                {selected_buisness_top_lines: selected_buisness_top_lines},

        callback);



    },

    subsectorlistToplineWiseObj: function (selected_buisness_top_lines, callback) {



        this.ajaxCall("category/subsectorlistToplineWiseObj",

                {selected_buisness_top_lines: selected_buisness_top_lines},

        callback);



    },

    sendCombinationCode: function (selected_user_toplines_and_sub_sectors, selected_user_business, user_unique_buisness, callback) {



        this.ajaxCall("category/sendCombinationCode",

                {user_email: this.getAccessToken, selected_user_business: selected_user_business, selected_user_toplines_and_sub_sectors: selected_user_toplines_and_sub_sectors, user_unique_buisness: user_unique_buisness},

        callback);



    },

    sendCombinationCodeObj: function (selected_user_toplines_and_sub_sectors, selected_user_business, user_unique_buisness, callback) {



        this.ajaxCall("category/sendCombinationCodeObj",

                {user_email: this.getAccessToken, selected_user_business: selected_user_business, selected_user_toplines_and_sub_sectors: selected_user_toplines_and_sub_sectors, user_unique_buisness: user_unique_buisness},

        callback);



    },

    getHubNews: function (track_load, callback) {



        this.ajaxCall("hub/moreHubApi",

                {user_emai: this.getAccessToken, start: track_load},

        callback);



    },


 getScrollFlag: function () {



        this.ajaxCall("hub/moreHubApi",

                {scroll_flag: 'scrollflag'},

        callback);



    },






    sharePost: function (post_message, select_scope_to_share, callback) {



        this.ajaxCall("member/hub_post_api",

                {user_email: this.getAccessToken, post_message: post_message, shared_for: select_scope_to_share.shareId, tag_arr: ''},

        callback);



    },

    getDetailSuggestByKey: function (suggested_type, callback) {



        this.ajaxCall("suggestion/makeSuggestedListForloginuserMod",

                {user_email: this.getAccessToken, key: suggested_type.type, type_key: suggested_type.type_key},

        callback);



    },

    hubPost: function (img, about_the_img, select_scope_to_share, title, callback) {



        this.ajaxCall("hub/hub_post_api",

                {user_email: this.getAccessToken, imgFile: img, publish_text: about_the_img, share: 4, title: title},

        callback);



    },

    getProfileDetails: function (callback) {

        this.ajaxCall("memberprofile/viewProfileApi",

                {user_email: this.getAccessToken},

        callback);

    },

    getOtherProfileDetails: function (member_id, callback) {

        this.ajaxCall("viewprofile/otherProfileApi",

                {user_email: this.getAccessToken, member_id: member_id},

        callback);

    },

    userSuggestionFeedback: function (suggestion_obj, feedback, callback) {



        this.ajaxCall("memberprofile/userSuggestionFeedback",

                {user_email: this.getAccessToken},

        callback);



    },

    postLike: function (hub_id, callback) {



        this.ajaxCall("hub/userPostLike",

                {hub_id: hub_id, user_email: this.getAccessToken},

        callback);



    },

    deleteLike: function (hub_id, callback) {



        this.ajaxCall("hub/userDeleteLike",

                {user_email: this.getAccessToken, hub_id: hub_id},

        callback);



    },

    postLikeCom: function (comm_id,mid,hub_id,news_id, callback) {



        this.ajaxCall("hub/FnCommentLike",

                {comm_id:comm_id,mid:mid,hub_id:hub_id,news_id:news_id,user_email: this.getAccessToken},

        callback);



    },

    deleteLikeCom: function (comm_id,mid,hub_id,news_id, callback) {



        this.ajaxCall("hub/FnCommentDisLike",

                {comm_id:comm_id,mid:mid,hub_id:hub_id,news_id:news_id,user_email: this.getAccessToken},

        callback);



    },









    postComments: function (comment, hub_id, callback) {

       

        this.ajaxCall("hub/postCommentApi",

                {user_email: this.getAccessToken, hub_id: hub_id, comment: comment},

        callback);



    },

    uploadImage: function (img, callback) {



        this.ajaxCall("memberprofile/uploadImage",

                {user_email: this.getAccessToken, imgFile: img},

        callback);



    },

    uploadBanner: function (img, callback) {



        this.ajaxCall("memberprofile/uploadBackImage",

                {user_email: this.getAccessToken, imgFile: img},

        callback);



    },

    editBusinessName: function (edit_business_name, mid, callback) {



        this.ajaxCall("memberprofile/editBussinessName",

                {bussinessname: edit_business_name, mid: mid},

        callback);



    },

    editTagName: function (edit_business_name, mid, callback) {



        this.ajaxCall("memberprofile/editTagLine",

                {business_tagline: edit_business_name, mid: mid},

        callback);



    },

    editLocation: function (country_id, edit_city, mid, callback) {



        this.ajaxCall("memberprofile/editMemberLocation",

                {city: edit_city, mid: mid, country_id: country_id},

        callback);



    },

    editBizSumary: function (business_desc_edit, mid, callback) {



        this.ajaxCall("memberprofile/editBussinessSummery",

                {bussinessdesc: business_desc_edit, mid: mid},

        callback);



    },

    getCountryList: function (callback) {



        this.ajaxCall("member/get_country",

                {},

                callback);



    },

    getIndustryList: function (callback) {



        this.ajaxCall("category/get_industry_list",

                {},

                callback);



    },

    checkBusinessCodeExitForUser: function (code1, code2, callback) {



        this.ajaxCall("category/isThisCodeAlredyexistinwiztwo",

                {user_email: this.getAccessToken, code1: code1, code2: code2},

        callback);



    },

//    checkBusinessCodeExitForUserApi:function(code1,code2,callback){



//        this.ajaxCall("category/isThisCodeAlredyexistinwiztwo",



//            {user_email: this.getAccessToken,code1:code1,code2:code2},



//        callback);



//    },



    getTargetSectorList: function (selected_industry_code, callback) {

        //alert(selected_industry_code);

        this.ajaxCall("category/get_target_sector_list_api",

                {selected_industry_code: selected_industry_code},

        callback);

    },

    isCodeExist: function (callback) {

        this.ajaxCall("category/isCodeExist",

                {user_email: this.getAccessToken},

        callback);

    },
            
	
	talkRequestAccepted: function (mid,community_id,talk_owner_id,callback) {

        this.ajaxCall("community/accepttalkrequest",

                {user_email: this.getAccessToken,mid:mid,community_id:community_id,talk_owner_id:talk_owner_id},

        callback);

    },
	
	talkRequestDecline: function (mid,community_id,talk_owner_id,callback) {

        this.ajaxCall("community/declinetalkrequest",

                {user_email: this.getAccessToken,mid:mid,community_id:community_id,talk_owner_id:talk_owner_id},

        callback);

    },
	
	

    lookingServicesList: function (callback) {

        this.ajaxCall("category/looking_services_list",

                {},

                callback);

    },

    getLookingForList: function (callback) {



        this.ajaxCall("category/get_looking_for_list",

                {},

                callback);



    },

    getLocalityList: function (callback) {

        this.ajaxCall("category/get_locality_list",

                {},

                callback);

    },

    getIsServiceList: function (callback) {

        this.ajaxCall("category/get_is_service_list",

                {},

                callback);

    },

    setupWizTwo: function (type, industry_you_serve, target_sector, looking_for_biz, locality_to_explore, services, looking_service, callback) {

        this.ajaxCall("category/setup_wiz_two",

                {user_email: this.getAccessToken, type: type, industry_you_serve: industry_you_serve, target_sector: target_sector, looking_for_biz: looking_for_biz, locality_to_explore: locality_to_explore, services: services, looking_service: looking_service},

        callback);

    },

    setupWizTwoApi: function (type, industry_you_serve, target_sector, looking_for_biz, locality_to_explore, services, looking_service, callback) {

        this.ajaxCall("category/setup_wiz_two_api",

                {user_email: this.getAccessToken, type: type, industry_you_serve: industry_you_serve, target_sector: target_sector, looking_for_biz: looking_for_biz, locality_to_explore: locality_to_explore, services: services, looking_service: looking_service},

        callback);

    },

    welcomeWizard: function (is_professional, user_buisnessname, user_firstname, user_lastname, callback) {

        this.ajaxCall("category/set_user_type",

                {user_email: this.getAccessToken, businesstype: is_professional, businessname: user_buisnessname, fname: user_firstname, lname: user_lastname},

        callback);

    },

    isBuisnessNameExists: function (callback) {

        this.ajaxCall("member/isBuisnessNameExist",

                {user_email: this.getAccessToken},

        callback);

    },

    viewDocApi: function (callback) {

        this.ajaxCall("memberprofile/viewDocApi",

                {user_email: this.getAccessToken},

        callback);

    },

    deleteDocument: function (doc_id, callback) {

        this.ajaxCall("memberprofile/deleteDocument",

                {doc_id: doc_id},

        callback);

    },

    uploadDocCoverPic: function (cover_pic, doc_id, callback) {

        this.ajaxCall("memberprofile/coverUpload",

                {imgFile: cover_pic, doc_id: doc_id},

        callback);

    },

    shareHub: function (hub_id, hbcomment, scope,url_provider,POSTFLAG, callback) {

        //alert(url_provider);
		console.log("REQUEST FROM SHARE TYPE IS="+POSTFLAG);
        this.ajaxCall("hub/shareWithComment",

                {user_email: this.getAccessToken, hub_id: hub_id, hbcomment: hbcomment, scope: scope,url_provider:url_provider,POSTFLAG:POSTFLAG},

        callback);

    },

    reportPost: function (hub_id, callback) {

        this.ajaxCall("hub/hubReport",

                {user_email: this.getAccessToken, hub_id: hub_id},

        callback);

    },

    deletePost: function (hub_id, callback) {

        this.ajaxCall("hub/hubDelete",

                {user_email: this.getAccessToken, hub_id: hub_id},

        callback);

    },

    newsLike: function (news_id, callback) {

        this.ajaxCall("hub/hubNewsLike",

                {newsId: news_id, user_email: this.getAccessToken},

        callback);

    },

    newsDislike: function (news_id, callback) {

        this.ajaxCall("hub/dislikeNews",

                {newsId: news_id, user_email: this.getAccessToken},

        callback);

    },

    postNewsComments: function (comment, news_id, callback) {

        this.ajaxCall("hub/hubNewsComments",

                {comment: comment, newsId: news_id, user_email: this.getAccessToken},

        callback);
	//console.log(callback);

    },

    shareNewsHub: function (new_obj, scope, callback) {

        this.ajaxCall("hub/saveNewsAsHub",

                {news_id: new_obj.news_id, hub_title: new_obj.hub_title, content: new_obj.content, link: new_obj.link, shared_for: new_obj.shared_for, user_email: this.getAccessToken, hbcomment: new_obj.hbComment, news_link: new_obj.news_link, scope: scope, source:new_obj.sourceNews,type_of_post:3,POSTFLAG:'NEWS'},

        callback);

    },

    getUserTargetSubSector: function (callback) {

        this.ajaxCall("member/targetSubSector",

                {user_email: this.getAccessToken},

        callback);

    },

    getUserOwnSubSector: function (callback) {



        this.ajaxCall("member/ownSubSector",

                {user_email: this.getAccessToken},

        callback);



    },

    updatProfile: function (email, callback) {

        this.ajaxCall("memberprofile/editEmail",

                {user_email: this.getAccessToken, email: email},

        callback);

    },

    deleteHubComment: function (comm_id, callback) {

        this.ajaxCall("hub/deleteComment",

                {user_email: this.getAccessToken, comment_id: comm_id},

        callback);

    },

    deleteHubComment2: function (comm_id, callback) {

        this.ajaxCall("hub/FnDeleteComment",

                {comm_id: comm_id},

        callback);

    },









    connectionLogic: function (connected_id, callback) {

        this.ajaxCall("memberprofile/connectionRequest",

                {user_email: this.getAccessToken, connected_id: connected_id},

        callback);

    },

    disconnectionLogic: function (connected_id, callback) {

        this.ajaxCall("member/deleteSuggestion",

                {user_email: this.getAccessToken, connected_id: connected_id},

        callback);

    },

    sendMessage: function (reciever_id, message, callback) {

        this.ajaxCall("message/sendMessage",

                {user_email: this.getAccessToken, reciever_id: reciever_id, message: message, subject: ''},

        callback);

    },

    sendMessageFromMessage: function (message_send_to, message_sub, message_content, callback) {

        this.ajaxCall("message/msglist_api",

                {user_email: this.getAccessToken, message_send_to: message_send_to, message_sub: message_sub, message_content: message_content},

        callback);

    },

    getInboxApi: function (callback) {

        this.ajaxCall("message/getInboxApi",

                {user_email: this.getAccessToken},

        callback);

    },

    getNetworkConnectionList: function (callback) {

        this.ajaxCall("memberprofile/getNetworkConnectionList",

                {user_email: this.getAccessToken},

        callback);

    },

    deleteSelectMessage: function (delete_msg_obj, callback) {

        this.ajaxCall("message/deleteSelectMessage",

                {user_email: this.getAccessToken, delete_msg_obj: delete_msg_obj},

        callback);

    },

    getSentApi: function (callback) {

        this.ajaxCall("message/getSentApi",

                {user_email: this.getAccessToken},

        callback);

    },

    deleteTargetSector: function (target_sector, callback) {

        this.ajaxCall("memberprofile/removeUserDetail",

                {user_email: this.getAccessToken, code: target_sector.targetsubcode},

        callback);

    },

    getSelectedSubsector: function (sector, callback) {

        this.ajaxCall("memberprofile/getSubsectorListByUser",

                {user_email: this.getAccessToken, sector_id: sector.subcode},

        callback);

    },

    removeUserDetailFromWizOne: function (buis_code, callback) {

        this.ajaxCall("memberprofile/removeUserDetailFromWizOne",

                {wiz_one_id: buis_code.wiz_one_id},

        callback);

    },

    signatureImage: function (img, callback) {

        this.ajaxCall("memberprofile/uploadSignImage",

                {user_email: this.getAccessToken, imgFile: img},

        callback);

    },

    validateCurrentPassword: function (password, callback) {

        this.ajaxCall("memberprofile/validateCurrentPassword",

                {user_email: this.getAccessToken, password: password},

        callback);

    },

    resetPassword:function (password, callback) {

        this.ajaxCall("memberprofile/resetPassword",

                {user_email: this.getAccessToken, password: password},

        callback);

    },

            editName: function (settings_object, callback) {

                this.ajaxCall("memberprofile/editName",

                        {user_email: this.getAccessToken, last_name: settings_object.last_name, first_name: settings_object.first_name},

                callback);

            },

    matchUserPassword: function (pwd, callback) {

        this.ajaxCall("memberprofile/matchUserPassword",

                {user_email: this.getAccessToken, pwd: pwd},

        callback);

    },

    /* * ***** Samarjit Debnath ****** */



    suggestTag: function (tags, callback) {

        this.ajaxCall("community/suggestTag",

                {user_email: this.getAccessToken, str: tags},

        callback);

    },

    addCommunity: function (title, content, tags, talk_access, callback) {



        this.ajaxCall("community/add_community",

                {user_email: this.getAccessToken, title: title, content: content, tags: tags, talk_access: talk_access},

        callback);

    },

    PostUrl: function (url, callback) {
			
        this.ajaxCall("memberprofile/convertUrlToHtml",

                {url: url},

        callback);

    },

    getCommunityDetails: function (callback) {

        this.ajaxCall("community/get_community_details",

                {user_email: this.getAccessToken},

        callback);

    },

    addCommunityComment: function (com_id, myTalk_comment, mid, callback) {

        this.ajaxCall("community/add_community_comment",

                {user_email: this.getAccessToken, com_id: com_id, comment: myTalk_comment, mid: mid},

        callback);

    },

    getLikeCount: function (com_id, callback) {



        this.ajaxCall("community/get_like_count",

                {user_email: this.getAccessToken, com_id: com_id},

        callback);

    },

    addCommunityLike: function (com_id, callback) {

        this.ajaxCall("community/add_community_like",

                {user_email: this.getAccessToken, com_id: com_id},

        callback);

    },

    disLike: function (com_id, callback) {

        this.ajaxCall("community/dislike",

                {user_email: this.getAccessToken, com_id: com_id},

        callback);

    },

    deleteParticipation: function (com_id, callback) {

        this.ajaxCall("community/delete_community",

                {user_email: this.getAccessToken, com_id: com_id},

        callback);

    },

    deleteCommentLatestTalk: function (com_id, callback) {

        this.ajaxCall("community/deleteComment",

                {user_email: this.getAccessToken, com_id: com_id},

        callback);

    },

    topTalk: function (callback) {

        this.ajaxCall("community/topTalk",

                {user_email: this.getAccessToken},

        callback);

    },

    fetchLatestTalk: function (callback) {

        this.ajaxCall("community/fetchLatestTalk",

                {user_email: this.getAccessToken},

        callback);

    },

    accessCheck: function (com_id, mid, callback) {

        this.ajaxCall("community/access_check",

                {user_email: this.getAccessToken, com_id: com_id, talk_owner_id: mid},

        callback);

    },

    allowAccess: function (com_id, callback) {

        this.ajaxCall("community/allow_access",

                {user_email: this.getAccessToken, com_id: com_id},

        callback);

    },

    requestAccess: function (com_id, mid, callback) {

        this.ajaxCall("community/request_access",

                {user_email: this.getAccessToken, com_id: com_id, talk_owner_id: mid},

        callback);

    },

    sendMessageLatestTalk: function (to, msg, sub, callback) {

        this.ajaxCall("message/msglist_api",

                {user_emai: this.getAccessToken, message_send_to: to, message_content: msg, message_sub: sub},

        callback);

    },

    recommendSendMessage: function (toemail, message,com_id, callback) {

        this.ajaxCall("community/FnRecommendTalk",

                {user_email: this.getAccessToken, toemail: toemail, comm_id: com_id, message: message},

        callback);

    },

    autocomplete: function (q, callback) {

        this.ajaxCall("member/autocomplete",

                {user_email: this.getAccessToken, q: q},

        callback);

    },

    insertSubComment: function (com_comm_id, latestTalk_sub_comment, mid, callback) {

        this.ajaxCall("community/insertSubComment",

                {user_email: this.getAccessToken, com_comm_id: com_comm_id, comment: latestTalk_sub_comment, mid: mid},

        callback);

    },

    perticipant: function (com_id, callback) {

        this.ajaxCall("community/perticepant",

                {user_email: this.getAccessToken, com_id: com_id},

        callback);

    },

    isBusinessNameuniqe: function (user_buisnessname, callback) {

        this.ajaxCall("member/isBusinessNameuniqe",

                {user_buisnessname: user_buisnessname},

        callback);

    },

    rePublish: function (imgprof,comments, hub_title, hid, callback) {

        this.ajaxCall("hub/hub_edit_post",

                {title: hub_title,imgFile:imgprof,comments: comments, hid: hid},

        callback);

    },

    getConnection: function (connected_id, mid, callback) {

        this.ajaxCall("hub/isConnected",

                {connected_id: connected_id, mid: mid},

        callback);

    },
	CommunityNotification: function (callback) {

        this.ajaxCall("community/FnCommunityNotification",

                {user_email: this.getAccessToken},

        callback);

    },
	getMessageByMsgId:function(message_id,callback){
		 this.ajaxCall("message/FnGetMessagebyMsgId",
				{user_email: this.getAccessToken,message_id:message_id},
		 callback);
	},

    /* * ***** Samarjit Debnath ****** */

    hideNews: function (new_obj, callback) {

        this.ajaxCall("hub/newsBlockFromUser",

                {user_email: this.getAccessToken, news_id: new_obj.id},

        callback);

    },

    blockUser: function (mid, callback) {

        this.ajaxCall("hub/FnBlockHubUser",

                {user_email: this.getAccessToken, hub_owner_id: mid},

        callback);

    },
	FnSaveSubscription: function (fieldname,checkboxvalue, callback) {
      
		
		if(checkboxvalue===true)
		{
			var subscriptionvalue=1;
		}
		else
		{
			var subscriptionvalue=0;
		}
		this.ajaxCall("memberprofile/FnSaveSubscription",
                 
                {database_field_name: fieldname,user_email: this.getAccessToken, subscriptionvalue: subscriptionvalue},

        callback);

    },
    updateNotification: function(list,callback){
        this.ajaxCall("member/updateNotificationData",
            {user_email: this.getAccessToken,list: list},
        callback);
    },
	msgStatusChange:function(mt_id,callback){
		 this.ajaxCall("message/updateMessageStatus",
            {user_email: this.getAccessToken,mt_id: mt_id},
        callback);
	},
    settingAccountDelete:function(status,callback){
         this.ajaxCall("memberprofile/deleteAccount",
            {user_email: this.getAccessToken,status: status},
        callback);
    },

    editProfile: function (edit_business_name,fname,lname,edit_tagline,edit_city,selected_country,mid, callback) {

        console.log(edit_business_name);
        console.log(fname);
        console.log(lname);
        console.log(edit_tagline);
        console.log(edit_city);
        console.log(selected_country);
        console.log(mid);
        this.ajaxCall("memberprofile/FnUpdateDetsOnce",

                {bussinessname:edit_business_name,fname:fname,lname:lname,tag_line:edit_tagline,city:edit_city,country:selected_country,mid: mid},

        callback);

    },

};



