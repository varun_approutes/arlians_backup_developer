arlians.controller('DashboardSuggestionCtrl', function ($scope, $timeout, $filter) {
    arlians.filter('strLimit', ['$filter', function ($filter) {
            return function (input, limit) {
                if (!input)
                    return;
                if (input.length <= limit) {
                    return input;
                }
                return $filter('limitTo')(input, limit) + '...';
            };
        }]);
    $scope.total_suggestion_count = 0;
    $scope.detail_suggestion_list = [];
    $scope.details_suggestion = [];
    $scope.show_detail = false;
    $scope.search_module = true;
    $scope.search_for_suggestion = '';
    $scope.user_email = UserProxy.getAccessToken();
    UserProxy.getUserSuggestion(function (result) {
        console.log(result);
        $scope.total_suggestion_count = result.suggestion_count != '' ? result.suggestion_count : 0;
        $scope.detail_suggestion_list = result.suggestionCountTypeWise;
        $timeout(function () {
            var owl = $("#result-circle");
            owl.owlCarousel({
                itemsCustom: [
                    [0, 2],
                    [450, 2],
                    [600, 2],
                    [700, 2],
                    [1000, 2],
                    [1200, 2],
                    [1400, 2],
                    [1600, 2]
                ],
                navigation: true,
                pagination: false,
                autoPlay: true,
                navigationText: ["&#xf104;", "&#xf105;"]
            });
        }, 1000);
    });
    $scope.getDetailSuggestion = function (current_item) {        
        var owl = $("#detail_suggestion");
        if (typeof owl.data('owlCarousel') != 'undefined') {
            owl.data('owlCarousel').destroy();
            owl.removeClass('owl-carousel');
        }
        $scope.search_for_suggestion = current_item.type;
        UserProxy.getDetailSuggestByKey(current_item, function (result) {
            //console.log(result);            
            $scope.details_suggestion = result;
            $scope.total_count = current_item.type_count;
            //utils.setupcarousel();
            $scope.show_detail = true;
            $scope.search_module = false;
            $timeout(function () {
                //debugger;
                var owl = $("#detail_suggestion");
                if (typeof owl.data('owlCarousel') != 'undefined') {
                    owl.data('owlCarousel').destroy();
                    owl.removeClass('owl-carousel');
                }
                owl.owlCarousel({
                    itemsCustom: [
                        [0, 1],
                        [450, 1],
                        [600, 1],
                        [700, 1],
                        [1000, 1],
                        [1200, 1],
                        [1400, 1],
                        [1600, 1]
                    ],
                    navigation: true,
                    pagination: false,
                    autoPlay: true,
                    navigationText: ["&#xf104;", "&#xf105;"]
                });
            }, 1500);
        });
    };
    $scope.backToSuggestionList = function (current_item) {
        //console.log(current_item);
        $scope.show_detail = false;
        $scope.search_module = true;
        //var owl = $("#detail_suggestion");
        //owl.trigger( 'remove.owl.carousel', [1] );
    };
    $scope.individual_suggestion_user_feedback = function (suggestion_obj, feedback) {

        UserProxy.userSuggestionFeedback(suggestion_obj, feedback, function (result) {
            if (feedback == 1) {
                alertify.success('Your feedback is succesfully saved');
            } else {
                alertify.success('Your feedback is succesfully saved');
            }
        });
    };
});
arlians.controller('DashboardHub', function ($scope) {
    //$scope.share_with_list = [];
    var share_list = [
        {
            'shareId': 1,
            'name': 'Private: your network only'
        },
        {
            'shareId': 2,
            'name': 'Public: all businesses you match with '
        },
        {
            'shareId': 3,
            'name': 'Public: all relevant businesses'
        }
    ];
    UserProxy.getCurrentUser(function (result) {
        console.log(result);
        $scope.path = result.user_info.path;
        $scope.front_img_name = result.user_info.fornt_image;
        $scope.bussinessname = result.user_info.bussinessname;
        //alert($scope.bussinessname);
    });
    $scope.whatLikeClassIsIt = function (value) {
        if (value == true)
            return "fa-heart";      
        else
            return "fa-heart-o";
    }
    $scope.current_post_publish_list = [];
    $scope.select_post = function (index) {
        $scope.select_scope_to_post = index;
        $scope.show_post_selected = index.name;
    };
    $scope.publish_header = '';
    $scope.savePublish = function (text) {
        var img = $('#hub_publish_preview_img').attr('src');
        //alert(img); return false;
        //alert(img); return false;
        var about_the_img = CKEDITOR.instances.txtEditor.getData();
//        if (img == '' || img == undefined) {
//            alertify.error('Please select an image');
//            return false;
//        };
        if (about_the_img == '' || about_the_img == undefined) {
            alertify.error('Please insert some text');
            return false;
        }
//        if ($scope.select_scope_to_post == '' || $scope.select_scope_to_post == undefined) {
//            alertify.error('Please choose scope to share');
//            return false;
//        }
//        ;
        if ($scope.publish_header == '' || $scope.publish_header == undefined) {
            alertify.error('Please insert header');
            return false;
        }
        about_the_img = '<h4>' + $scope.publish_header + '</h4>' + about_the_img;
        UserProxy.hubPost(img, about_the_img, $scope.select_scope_to_post, function (result) {
            if (result.response == 'success') {
                var limit = 150;
                alertify.success('Sucessfully posted');
                $('#hub_publish_preview_img').attr('src', '');
                $('#hub_publish_default_img').show();
                $('#hub_publish_preview_img').hide();
                $('#upload_img_for_post').val('');
                //var changedString = String(about_the_img).replace(/<[^>]+>/gm, '');
                //var length = about_the_img.length;
                //about_the_img = about_the_img.length > limit ? about_the_img.substr(0, limit - 1) : about_the_img;
                CKEDITOR.instances.txtEditor.setData('');
                var current_post_detail = {
                    buisname: $scope.bussinessname,
                    html_text: about_the_img,
                    image: $scope.front_img_name,
                    image_post: img,
                    path: $scope.path,
                    post: false,
                    hub_id: result.hub_id,
                    like_count: 0,
                    comments_count: 0,
                    like: false,
                    show_comments:false,
                    comment:'',
                    comments:[]
                };
                $scope.current_post_publish_list.push(current_post_detail);
                $scope.text_in_mind = '';
                $scope.select_scope_to_share = '';
                $scope.show_selected = '';
                $scope.show_post_selected = '';
                $scope.show_default_hub = true;
                $scope.show_publish = false;
                $('.imageBox').css('background', '');
                $('#file').val('');
            }
        });
    };
    $scope.user_details = {};
    $scope.share_with_list = share_list;
    $scope.hub_list = [];
    $scope.text_in_mind = '';
    $scope.select_scope_to_share = '';
    $scope.show_selected = '';
    $scope.select = function (index) {
        $scope.select_scope_to_share = index;
        $scope.show_selected = index.name;
    };

    $scope.post_and_publish_listing = [];
    $scope.sharePost = function () {
        if ($scope.text_in_mind == '' || $scope.text_in_mind == undefined) {
            alertify.error('Please insert some text');
            return false;
        }
        if ($scope.select_scope_to_share == '' || $scope.select_scope_to_share == undefined) {
            alertify.error('Please choose scope to share');
            return false;
        }
        UserProxy.sharePost($scope.text_in_mind, $scope.select_scope_to_share, function (result) { 
            debugger;
            if (result.responce == 'success') {
                alertify.success('Sucessfully posted');
                //debugger;
                var current_post_detail = {
                    buisname: $scope.bussinessname,
                    text: $scope.text_in_mind,
                    image: $scope.front_img_name,
                    path: $scope.path,
                    post: true,
                    hub_id: result.hub_id,
                    like_count: 0,
                    comments_count: 0,
                    like: false,
                    show_comments:false,
                    comment:'',
                    comments:[]
                };
                $scope.current_post_publish_list.unshift(current_post_detail);
                $scope.text_in_mind = '';
                $scope.select_scope_to_share = '';
                $scope.show_selected = '';
            }
        });
    };
    $scope.postComments = function(publish_object){
        if(publish_object.comment == ''){
            alertify.error('Please enter some text');
        }else{
            UserProxy.postComments(publish_object.comment, publish_object.hub_id, function (result) {
                debugger;
                if(result.response == 'success'){
                    publish_object.comment = '';
                    var cur_comment = {
                        cid: result.id,
                        comment_view: result.comment,
                        buisnesname: $scope.bussinessname,
                        image: $scope.front_img_name,
                        path:$scope.path
                    };
                    console.log(cur_comment);
                    publish_object.comments_count+=1;
                    publish_object.comments.push(cur_comment);
                    publish_object.show_comments=true;
                }
           });
        }
    };
    $scope.commentsAnimation = function(cur_obj){
        alert(cur_obj.show_comments);
        cur_obj.show_comments = cur_obj.show_comments == true ? false : true;
    };
    $scope.feedbackHub = function(cur_object){
        //alert(cur_object.hub_id);
        if (cur_object.like == false) {
            UserProxy.postLike(cur_object.hub_id, function (result) {
                if (result.response = 'success') {
                    cur_object.like = true;
                    cur_object.like_count += 1;
                } else {
                    alertify.error = 'Some error occured';
                }
            });
        } else {
            UserProxy.deleteLike(cur_object.hub_id, function (result) {
                if (result.response = 'success') {
                    cur_object.like = false;
                    cur_object.like_count -= 1;
                } else {
                    alertify.error = 'Some error occured';
                }
            });
        }
        console.log(cur_object);
    };
    $scope.feedback = function (cur_object) {
        if (cur_object.like == 0) {            
            UserProxy.postLike(cur_object.hid, function (result) {
                if (result.response = 'success') {
                    cur_object.like = true;
                    cur_object.like_count += 1;
                } else {
                    alertify.error = 'Some error occured';
                }
            });
        } else {
            UserProxy.deleteLike(cur_object.hid, function (result) {
                if (result.response = 'success') {
                    cur_object.like = false;
                    cur_object.like_count -= 1;
                } else {
                    alertify.error = 'Some error occured';
                }
            });
        }
        //console.log(cur_object);
    };
    $scope.show_publish = false;
    $scope.show_default_hub = true;
    UserProxy.getHubNews(function (result) {
        //debugger;
        console.log(result);
        $scope.hub_list = result;
        //console.log($scope.watson_news);
        //debugger;
//        $('.sticky-scroll').theiaStickySidebar({
//            additionalMarginTop: 50
//        });
    });
    $scope.showPublishSection = function () {
        $scope.show_default_hub = false;
        $scope.show_publish = true;
    };
    $scope.showDefault = function () {
        $scope.show_default_hub = true;
        $scope.show_publish = false;
    };
});
(function (module) {

    var fileReader = function ($q, $log) {

        var onLoad = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
        var onProgress = function (reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress",
                        {
                            total: event.total,
                            loaded: event.loaded
                        });
            };
        };
        var getReader = function (deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();
            var reader = getReader(deferred, scope);
            reader.readAsDataURL(file);
            return deferred.promise;
        };
        return {
            readAsDataUrl: readAsDataURL
        };
    };
    module.factory("fileReader",
            ["$q", "$log", fileReader]);
});
function readURLPostImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#hub_publish_default_img').hide();
            $('#hub_publish_preview_img').show();
            $('#hub_publish_preview_img').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function open_publish_crop_img_modal() {
    $('#crop_publish_image').modal('show');
}
var options =
        {
            imageBox: '.imageBox',
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: 'avatar.png'
        }
var cropper;
document.querySelector('#file').addEventListener('change', function () {
    var reader = new FileReader();
    reader.onload = function (e) {
        options.imgSrc = e.target.result;
        cropper = new cropbox(options);
    }
    reader.readAsDataURL(this.files[0]);
    this.files = [];
});
document.querySelector('#btnZoomIn').addEventListener('click', function () {
    cropper.zoomIn();
})
document.querySelector('#btnZoomOut').addEventListener('click', function () {
    cropper.zoomOut();
});
document.querySelector('#btnCrop').addEventListener('click', function () {
    try {
        var img = cropper.getDataURL();
        $('#hub_publish_default_img').hide();
        $('#hub_publish_preview_img').show();
        $('#hub_publish_preview_img').attr('src', img);
        $('#crop_publish_image').modal('hide');
    } catch (e) {
        alertify.error('Please select a image');
    }

    //document.querySelector('.cropped').innerHTML += '<img src="'+img+'">';

})

