//var service_url = "http://localhost/arlians/social-network/";

var service_url = "http://arlians.com/developer/";

//var service_url = "http://arlians.com/";



arlians.controller('DashboardSuggestionCtrl', function ($scope, $timeout, $filter) {

    arlians.filter('strLimit', ['$filter', function ($filter) {

            return function (input, limit) {

                if (!input)

                    return;

                if (input.length <= limit) {

                    return input;

                }

                return $filter('limitTo')(input, limit) + '...';

            };

        }]);

    $scope.total_suggestion_count = 0;

    $scope.detail_suggestion_list = [];

    $scope.details_suggestion = [];

    $scope.show_detail = false;

    $scope.search_module = true;

    $scope.search_for_suggestion = '';

    $scope.htmlContent = '';

    $scope.user_email = UserProxy.getAccessToken();

//    UserProxy.isThisCodeAlredyexistinwiztwo(function(){

//        

//    });

    $scope.industry_list_for_wizard_two = [];

    $scope.sub_sector_list = [];

    $scope.looking_for_list = [];

    $scope.locality_list = [];

    $scope.services_list = [];

    $scope.target_sector_list = [];

    UserProxy.getIndustryList(function (result) {

        //console.log(result);

        $scope.industry_list_for_wizard_two = result.industry_list;

    });

    UserProxy.isCodeExist(function (result) {

        if (result == 'true') {

            $scope.type = 'insert_old_customer_type';

        } else {

            //alertify.alert('For best match result fill improve your match.')

            $scope.type = 'insert_new_customer_type';

        }

    });



    $scope.show_wizard_notice = false;

    $scope.show_industry_list = true;

    $scope.show_target_sector = false;

    $scope.show_target_list = false;

    $scope.show_looking_for = false;

    $scope.show_locality = false;

    $scope.show_services = false;

    $scope.openWizardTwoModal = function () {

        if ($scope.type == 'insert_old_customer_type') {

            $scope.show_wizard_notice = false;

            $scope.show_industry_list = true;

            $scope.show_target_sector = false;

            $scope.show_target_list = false;

            $scope.show_looking_for = false;

        } else {

            $scope.show_wizard_notice = true;

            $scope.show_industry_list = false;

            $scope.show_target_sector = false;

            $scope.show_target_list = false;

            $scope.show_looking_for = false;

        }

        $('#myModal').modal('show');

    };

    $scope.openWizardTwoForNewUser = function () {

        $scope.show_wizard_notice = false;

        $scope.show_industry_list = true;

    };

    $scope.setWizardBackTo = function (seclection) {

        if (seclection === 'one') {

            //$scope.show_wizard_notice = false;

            $scope.show_industry_list = true;

            $scope.show_target_sector = false;

            $scope.show_target_list = false;

            $scope.show_looking_for = false;

        }

        if (seclection == 'two') {

            $scope.show_industry_list = false;

            $scope.show_target_sector = true;

            $scope.show_target_list = false;

            $scope.show_looking_for = false;

        }

        if (seclection == 'three') {

            $scope.show_industry_list = false;

            $scope.show_target_sector = false;

            $scope.show_target_list = true;

            $scope.show_looking_for = false;

        }

        if (seclection == 'four') {

            $scope.show_industry_list = false;

            $scope.show_target_sector = false;

            $scope.show_looking_for = false;

            $scope.show_locality = true;

            $scope.show_services = false;

            $scope.show_target_list = false;

        }

        if (seclection == 'five') {

            $scope.show_industry_list = false;

            $scope.show_target_sector = false;

            $scope.show_looking_for = false;

            $scope.show_locality = false;

            $scope.show_services = true;

            $scope.show_target_list = false;

        }

    };

    $scope.selectedIndustryCode = [];

    $scope.selectedSubsector = [];

    $scope.selectedLookingFor = [];

    $scope.selectedLocality = [];

    $scope.selectedService = [];

    $scope.selectedTarget = [];

    $scope.setIndustryCode = function (industry_code) {

        $scope.selectedIndustryCode = [];

        $scope.selectedIndustryCode.push(industry_code);

        //console.log($scope.selectedIndustryCode);

    };

    $scope.setSubSectorCode = function (subsector_code) {

        //alert($scope.selectedSubsector);

        $scope.selectedSubsector = [];

        $scope.selectedSubsector.push(subsector_code);

        //console.log($scope.selectedSubsector);

    };

    $scope.is_consumer = false;

    $scope.setConsumer = function (consumer) {

        $scope.is_consumer = $scope.is_consumer == true ? false : true;

        ///alert($scope.is_consumer);

    };

    $scope.getTargetSectorByIndustry = function () {

        //   console.log($scope.selectedIndustryCode);

        if ($scope.selectedIndustryCode.length == 0) {

            alertify.error('Please make a selection');

            return false;

        } else {

            //alert($scope.selectedIndustryCode[0].industry_code);

            UserProxy.getTargetSectorList($scope.selectedIndustryCode[0].industry_code, function (result) {

                // console.log(result.target_sector_list);

                //$scope.sub_sector_list

                $scope.sub_sector_list = result.target_sector_list;

                $scope.show_industry_list = false;

                $scope.show_target_sector = true;

                $scope.show_looking_for = false;

                $scope.show_locality = false;

                $scope.show_services = false;

                $scope.show_target_list = false;

            });

        }

    };

    $scope.fetchLocalityList = function () {

        if ($scope.selectedLookingFor.length > 0) {

            UserProxy.getLocalityList(function (result) {

                //  console.log(result);

                $scope.locality_list = result;

                $scope.show_industry_list = false;

                $scope.show_target_sector = false;

                $scope.show_looking_for = false;

                $scope.show_locality = true;

                $scope.show_services = false;

                $scope.show_target_list = false;

            });

        } else {

            alertify.error('Please select looking for');

            return false;

        }

    };

    $scope.fetchServices = function () {

        //if ($scope.selectedLocality.length > 0) {

        //if ($scope.selectedLookingFor.length > 0) {

        if ($scope.selectedTarget.length > 0) {

            UserProxy.getLookingForList(function (result) {

                //console.log(result);

                $scope.looking_for_list = result;

                $scope.show_industry_list = false;

                $scope.show_target_sector = false;

                $scope.show_target_list = false;

                $scope.show_locality = false;

                $scope.show_services = false;

                $scope.show_looking_for = true;

            });

//            UserProxy.getIsServiceList(function (result) {

//                console.log(result.is_service);

//                $scope.services_list = result.is_service;

//                $scope.show_industry_list = false;

//                $scope.show_target_sector = false;

//                $scope.show_looking_for = false;

//                $scope.show_locality = false;

//                $scope.show_services = true;

//                $scope.show_target_list = false;

//            });

        } else {

            alertify.error('Please make a selection');

            return false;

        }

    };

    $scope.checkServices = function () {

        if ($scope.selectedService.length > 0) {

            if ($scope.selectedService.length == 1) {

                if ($scope.selectedService[0].service_id == 1) {

                    //alert('Need to work');

                    UserProxy.setupWizTwo($scope.type, $scope.selectedIndustryCode[0].industry_code, $scope.selectedSubsector[0].tar_sector_code, angular.toJson($scope.selectedLookingFor), angular.toJson($scope.selectedLocality), angular.toJson($scope.selectedService), angular.toJson($scope.selectedTarget), function (result) {

                        if (result.response = 'success') {

                            $('#myModal').modal('hide');

                            $scope.show_industry_list = true;

                            $scope.show_target_sector = false;

                            $scope.show_looking_for = false;

                            $scope.show_locality = false;

                            $scope.show_services = false;

                            $scope.show_target_list = false;

                            $scope.selectedIndustryCode = [];

                            UserProxy.getUserSuggestion(function (result) {



                                var owl = $("#result-circle");

                                if (typeof owl.data('owlCarousel') != 'undefined') {

                                    owl.data('owlCarousel').destroy();

                                    owl.removeClass('owl-carousel');

                                }

                                $scope.total_suggestion_count = result.suggestion_count != '' ? result.suggestion_count : 0;

                                $scope.detail_suggestion_list = result.suggestionCountTypeWise;

                                // console.log($scope.detail_suggestion_list);

                                $timeout(function () {

                                    //debugger;

                                    var owl = $("#result-circle");

                                    owl.owlCarousel({

                                        itemsCustom: [

                                            [0, 2],

                                            [450, 2],

                                            [600, 2],

                                            [700, 2],

                                            [1000, 2],

                                            [1200, 2],

                                            [1400, 2],

                                            [1600, 2]

                                        ],

                                        navigation: true,

                                        pagination: false,

                                        autoPlay: true,

                                        navigationText: ["&#xf104;", "&#xf105;"]

                                    });

                                }, 1000);

                            });

                            alertify.alert('You have now set up your matching. See who Arlians suggests you connect with today, using the my suggestions section.');

                        } else {

                            alertify.error('Some error occured.Please try again');

                        }

                    });

                } else {

                    //debugger;

                    UserProxy.getLookingForList(function (result) {

                        //console.log(result);

                        $scope.looking_for_list = result;

                        $scope.show_industry_list = false;

                        $scope.show_target_sector = false;

                        $scope.show_target_list = false;

                        $scope.show_locality = false;

                        $scope.show_services = false;

                        $scope.show_looking_for = true;

                    });

//                    UserProxy.lookingServicesList(function (result) {

//                        console.log(result);

//                        $scope.target_sector_list = result.target_sector_list;

//                        $scope.show_industry_list = false;

//                        $scope.show_target_sector = false;

//                        $scope.show_looking_for = false;

//                        $scope.show_locality = false;

//                        $scope.show_services = false;

//                        $scope.show_target_list = true;

//                    });

                }

            } else {

                UserProxy.getLookingForList(function (result) {

                    //console.log(result);                    

                    $scope.looking_for_list = result;

                    $scope.show_industry_list = false;

                    $scope.show_services = false;

                    $scope.show_target_sector = false;

                    $scope.show_target_list = false;

                    $scope.show_locality = false;

                    $scope.show_looking_for = true;

                });

//                UserProxy.lookingServicesList(function (result) {

//                    console.log(result);

//                    $scope.target_sector_list = result.target_sector_list;

//                    $scope.show_industry_list = false;

//                    $scope.show_target_sector = false;

//                    $scope.show_looking_for = false;

//                    $scope.show_locality = false;

//                    $scope.show_services = false;

//                    $scope.show_target_list = true;

//

//                });

            }

        } else {

            alertify.error('Please select your services');

        }

    };

    $scope.saveDataForWizard = function () {

        //alert($scope.selectedTarget.length); return false;

        if ($scope.selectedLookingFor.length > 0) {

            UserProxy.setupWizTwo($scope.type, $scope.selectedIndustryCode[0].industry_code, $scope.selectedSubsector[0].tar_sector_code, angular.toJson($scope.selectedLookingFor), angular.toJson($scope.selectedLocality), $scope.is_consumer == true ? 1 : 2, angular.toJson($scope.selectedTarget), function (result) {

                if (result.response = 'success') {

                    $('#myModal').modal('hide');

                    $scope.selectedIndustryCode = [];

                    $scope.show_industry_list = true;

                    $scope.show_target_sector = false;

                    $scope.show_looking_for = false;

                    $scope.show_locality = false;

                    $scope.show_services = false;

                    $scope.show_target_list = false;

                    alertify.alert('You have now set up your matching. See who Arlians suggests you connect with today, using the my suggestions section.');

                    UserProxy.getUserSuggestion(function (result) {



                        $scope.total_suggestion_count = result.suggestion_count != '' ? result.suggestion_count : 0;

                        $scope.detail_suggestion_list = result.suggestionCountTypeWise;

                        $timeout(function () {

                            //debugger;

                            var owl = $("#result-circle");

                            owl.owlCarousel({

                                itemsCustom: [

                                    [0, 2],

                                    [450, 2],

                                    [600, 2],

                                    [700, 2],

                                    [1000, 2],

                                    [1200, 2],

                                    [1400, 2],

                                    [1600, 2]

                                ],

                                navigation: true,

                                pagination: false,

                                autoPlay: true,

                                navigationText: ["&#xf104;", "&#xf105;"]

                            });

                        }, 1500);

                    });

                    $scope.type = 'insert_old_customer_type';

                } else {

                    alertify.error('Some error occured.Please try again');

                }



            });

        } else {

            alertify.error('Please make a selection');

        }

    };

    $scope.setTarget = function (target) {

        var is_removed = 0;

        if ($scope.selectedTarget.length > 0) {

            for (var i = 0; i < $scope.selectedTarget.length; i++) {

                if ($scope.selectedTarget[i].look_service_id == target.look_service_id) {

                    var index = $scope.selectedTarget.indexOf(target);

                    var val = $scope.selectedTarget.splice(index, 1);

                    //console.log(val);

                    if (val.length > 0) {

                        is_removed++;

                    }

                }

            }

            if (is_removed == 0)

                $scope.selectedTarget.push(target);

        } else {

            $scope.selectedTarget.push(target);

        }

        //console.log($scope.selectedTarget);

    };

    $scope.setService = function (service) {

        var is_removed = 0;

        if ($scope.selectedService.length > 0) {

            for (var i = 0; i < $scope.selectedService.length; i++) {

                if ($scope.selectedService[i].service_id == service.service_id) {

                    var index = $scope.selectedService.indexOf(service);

                    var val = $scope.selectedService.splice(index, 1);

                    //console.log(val);

                    if (val.length > 0) {

                        is_removed++;

                    }

                }

            }

            if (is_removed == 0)

                $scope.selectedService.push(service);

        } else {

            $scope.selectedService.push(service);

        }

        //console.log($scope.selectedService);

    }

    $scope.setLocality = function (locality) {

        var is_removed = 0;

        if ($scope.selectedLocality.length > 0) {

            for (var i = 0; i < $scope.selectedLocality.length; i++) {

                if ($scope.selectedLocality[i].locality_id == locality.locality_id) {

                    var index = $scope.selectedLocality.indexOf(locality);

                    var val = $scope.selectedLocality.splice(index, 1);

                    //console.log(val);

                    if (val.length > 0) {

                        is_removed++;

                    }

                }

            }

            if (is_removed == 0)

                $scope.selectedLocality.push(locality);

        } else {

            $scope.selectedLocality.push(locality);

        }

        //console.log($scope.selectedLocality);

    }

    $scope.setLookingFor = function (looking_for) {

        var is_removed = 0;

        if ($scope.selectedLookingFor.length > 0) {

            for (var i = 0; i < $scope.selectedLookingFor.length; i++) {

                if ($scope.selectedLookingFor[i].looking_for_id == looking_for.looking_for_id) {

                    var index = $scope.selectedLookingFor.indexOf(looking_for);

                    var val = $scope.selectedLookingFor.splice(index, 1);

                    //console.log(val);

                    if (val.length > 0) {

                        is_removed++;

                    }

                }

            }

            if (is_removed == 0)

                $scope.selectedLookingFor.push(looking_for);

        } else {

            $scope.selectedLookingFor.push(looking_for);

        }

        //console.log($scope.selectedLookingFor);

    };

    $scope.checkBusinessCodeExitForUser = function () {

        if ($scope.selectedSubsector.length == 0) {

            alertify.error('Please make a selection');

            return false;

        } else {

            UserProxy.checkBusinessCodeExitForUser($scope.selectedIndustryCode[0].industry_code, $scope.selectedSubsector[0].tar_sector_code, function (result) {

                if (result == 'true') {

                    alertify.alert('This code is already exits.Please change it.');

                } else {

                    //debugger;

                    if ($scope.type == 'insert_new_customer_type') {

                        UserProxy.lookingServicesList(function (result) {

                            // console.log(result);

                            $scope.target_sector_list = result.target_sector_list;

                            $scope.show_industry_list = false;

                            $scope.show_target_sector = false;

                            $scope.show_looking_for = false;

                            $scope.show_locality = false;

                            $scope.show_services = false;

                            $scope.show_target_list = true;

                        });

//                        UserProxy.getLookingForList(function (result) {

//                            //console.log(result);

//                            $scope.looking_for_list = result;

//                            $scope.show_industry_list = false;

//                            $scope.show_target_sector = false;

//                            $scope.show_looking_for = true;

//                            $scope.show_locality = false;

//                        });

                    } else {

                        UserProxy.setupWizTwo($scope.type, $scope.selectedIndustryCode[0].industry_code, $scope.selectedSubsector[0].tar_sector_code, angular.toJson($scope.selectedLookingFor), angular.toJson($scope.selectedLocality), angular.toJson($scope.selectedService), angular.toJson($scope.selectedTarget), function (result) {

                            if (result.response = 'success') {

                                $('#myModal').modal('hide');

                                $scope.selectedIndustryCode = [];

                                $scope.show_industry_list = true;

                                $scope.show_target_sector = false;

                                $scope.show_looking_for = false;

                                $scope.show_locality = false;

                                $scope.show_services = false;

                                $scope.show_target_list = false;

                                UserProxy.getUserSuggestion(function (result) {

                                    //console.log(result);

                                    var owl = $("#result-circle");

                                    if (typeof owl.data('owlCarousel') != 'undefined') {

                                        owl.data('owlCarousel').destroy();

                                        owl.removeClass('owl-carousel');

                                    }

                                    $scope.total_suggestion_count = result.suggestion_count != '' ? result.suggestion_count : 0;

                                    $scope.detail_suggestion_list = result.suggestionCountTypeWise;

                                    $timeout(function () {

                                        var owl = $("#result-circle");

                                        owl.owlCarousel({

                                            itemsCustom: [

                                                [0, 2],

                                                [450, 2],

                                                [600, 2],

                                                [700, 2],

                                                [1000, 2],

                                                [1200, 2],

                                                [1400, 2],

                                                [1600, 2]

                                            ],

                                            navigation: true,

                                            pagination: false,

                                            autoPlay: true,

                                            navigationText: ["&#xf104;", "&#xf105;"]

                                        });

                                    }, 1000);

                                });

                                alertify.alert('You have now set up your matching. See who Arlians suggests you connect with today, using the my suggestions section.');

                            } else {

                                alertify.error('Some error occured.Please try again');

                            }

                        });

                    }

                }

            });

        }

    };

    UserProxy.getUserSuggestion(function (result) {

        //console.log(result);

        $scope.total_suggestion_count = result.suggestion_count != '' ? result.suggestion_count : 0;

        $scope.detail_suggestion_list = result.suggestionCountTypeWise;

        $timeout(function () {

            if ($scope.detail_suggestion_list.length > 0) {

                var owl = $("#result-circle");

                owl.owlCarousel({

                    itemsCustom: [

                        [0, 2],

                        [450, 2],

                        [600, 2],

                        [700, 2],

                        [1000, 2],

                        [1200, 2],

                        [1400, 2],

                        [1600, 2]

                    ],

                    navigation: true,

                    pagination: false,

                    autoPlay: true,

                    navigationText: ["&#xf104;", "&#xf105;"]

                });

            }

        }, 1000);

    });

    $scope.getDetailSuggestion = function (current_item) {

        $scope.search_for_suggestion = current_item.type;

        var owl = $("#detail_suggestion");

        if (typeof owl.data('owlCarousel') != 'undefined') {

            owl.data('owlCarousel').destroy();

            owl.removeClass('owl-carousel');

        }

        UserProxy.getDetailSuggestByKey(current_item, function (result) {

            if (result.result == 'fail') {

                alertify.alert("You can't have access to view freelancer and seek invester profile");

                return false;

            }

            $scope.details_suggestion = result;

            $scope.total_count = current_item.type_count;

            //utils.setupcarousel();



            $timeout(function () {

                owl.owlCarousel({

                    pagination: false,

                    navigation: true,

                    navigationText: ["&#xf104;", "&#xf105;"],

                    slideSpeed: 300,

                    paginationSpeed: 400,

                    singleItem: true,

                    autoPlay: true

                });

                //$('.owl-item').last().remove();

                $scope.show_detail = true;

                $scope.search_module = false;

            }, 1200);

        });

    };

    $scope.backToSuggestionList = function (current_item) {

        //console.log(current_item);

        $scope.show_detail = false;

        $scope.search_module = true;

        //var owl = $("#detail_suggestion");

        //owl.trigger( 'remove.owl.carousel', [1] );

    };

    $scope.individual_suggestion_user_feedback = function (suggestion_obj, feedback) {

        UserProxy.userSuggestionFeedback(suggestion_obj, feedback, function (result) {

            if (feedback == 1) {

                alertify.success('Your feedback is succesfully saved');

            } else {

                alertify.success('Your feedback is succesfully saved');

            }

        });

    };

}).directive("owlCarousel", function () {

    return {

        restrict: 'E',

        transclude: false,

        link: function (scope) {

            scope.initCarousel = function (element) {

                // provide any default options you want

                var defaultOptions = {

                };

                var customOptions = scope.$eval($(element).attr('data-options'));

                // combine the two options objects

                for (var key in customOptions) {

                    defaultOptions[key] = customOptions[key];

                }

                // init carousel

                $(element).owlCarousel(defaultOptions);

            };

        }

    };

})

        .directive('owlCarouselItem', [function () {

                return {

                    restrict: 'A',

                    transclude: false,

                    link: function (scope, element) {

                        // wait for the last item in the ng-repeat then call init

                        if (scope.$last) {

                            scope.initCarousel(element.parent());

                        }

                    }

                };

            }]);

arlians.controller('DashboardHub', function ($scope, $timeout, $http) {


 // Controller function for split string
    
$scope.getstringsplitvalcom = function(attachmentstr) {


       // console.log("NILANJAN"+attachmentstr);
       if(attachmentstr){

        if(attachmentstr.indexOf('~') > -1)
        {

          var arr1 = attachmentstr.split('~');
          return arr1[0];

        }
        else{
        return attachmentstr;

        }
}


    };


$scope.getstringsplitvalcont = function(attachmentstr) {


       //console.log("NILANJAN"+attachmentstr);
        if(attachmentstr){

        if(attachmentstr.indexOf('~') > -1)
        {

          var arr1 = attachmentstr.split('~');
          return arr1[1];

        }
        else{
       // return attachmentstr;
        return '';

        }

}

    };









    arlians.filter('unsafe', function ($sce) {

        return function (val) {

            return $sce.trustAsHtml(val);

        };

    });

    //$scope.share_with_list = [];

    var share_list = [

        {

            'shareId': 1,

            'name': 'Private: your network only'

        },

        {

            'shareId': 2,

            'name': 'Public: all businesses you match with '

        },

        {

            'shareId': 3,

            'name': 'Public: all relevant businesses'

        }

    ];

    $scope.user_initial = '';

    $scope.user_mid = '';

    $scope.maxNumberOfChar = 350;

    //alert("hi");

    //alert("hi");

    UserProxy.getCurrentUser(function (result) {

        console.log("*********************************************");

        console.log(result);

        // debugger;

        if (result.chk_wiz_one != 'TRUE') {

            //UserProxy.unsetAccessToken();

            var user_email = UserProxy.getAccessToken();

            if (user_email == '' || user_email == 0 || user_email == null) {

                window.location = "logout";

            } else {

                setTimeout(function () {

                    window.location = "setup_wiz_one";

                }, 200);

            }



        } else {

            $scope.path = result.user_info.path;

            $scope.front_img_name = result.user_info.fornt_image;

            $scope.bussinessname = result.user_info.bussinessname;

            $scope.user_initial = result.logobussinessname;

            $scope.user_mid = result.user_info.mid;

        }

    });


   












    $scope.whatLikeClassIsIt = function (value) {

        if (value == true)

            return "fa-heart";

        else

            return "fa-heart-o";

    };

    $scope.current_post_publish_list = [];

    $scope.select_post = function (index) {

        $scope.select_scope_to_post = index;

        $scope.show_post_selected = index.name;

    };

    $scope.publish_header = '';



    $scope.content_about_publish = '';



    $scope.reportPost = function (hub) {

        UserProxy.reportPost(hub.hid, function (result) {

            if (result.response == 'success') {

                //alertify.success("Report posted successfully");

            }

        });

    }





    $scope.savePublish = function () {

        var img = $('#hub_publish_preview_img').attr('src');

        var about_the_img = $scope.content_about_publish;

        if (about_the_img == '' || about_the_img == undefined) {

            alertify.error('Please insert some text');

            return false;

        }

        if ($scope.publish_header == '' || $scope.publish_header == undefined) {

            alertify.error('Please insert header');

            return false;

        }

        var title = $scope.publish_header;

        UserProxy.hubPost(img, about_the_img, $scope.select_scope_to_post, title, function (result) {

            if (result.response == 'success') {



                var limit = 150;

                //alertify.success('Sucessfully posted');

                $('#hub_publish_preview_img').attr('src', '');

                $('#hub_publish_default_img').show();

                $('#hub_publish_preview_img').hide();

                $('#upload_img_for_post').val('');

                //CKEDITOR.instances.txtEditor.setData('');

                $scope.content_about_publish = '';

                var current_post_detail = {

                    //base_url: "http://arlians.online-seo-service.co.uk/uploads/hub_images/",

                    //base_url: "http://localhost/arlians/social-network/uploads/hub_images/",

                    base_url: service_url + "uploads/hub_images/",

                    bussinessname: $scope.bussinessname,

                    comment: "",

                    comment_count: 0,

                    comments: [],

                    content: about_the_img,

                    ext: result.ext,

                    hid: result.hub_id,

                    hub_post_time: "now",

                    hub_title: title,

                    is_connected: [],

                    like: 0,

                    likes: 0,

                    link: result.imagename,

                    mid: $scope.user_mid,

                    name: "",

                    news_link: null,

                    open_comment_box: false,

                    posted_on: "now",

                    front_img_name: $scope.front_img_name,

                    prof_img: $scope.path + $scope.front_img_name,

                    profile_initional: $scope.user_initial,

                    shared_for: '4',

                    show_comments: false,

                    status: "1",

                    tag_id: "",

                    tag_title: {undefined: null},

                    timestamp: 0,

                    total_report: "0",

                    total_share: 0,

                    type: "hub",

                    whose_share: '',

                    whose_share_id: null



                };

              
              

                //$scope.current_post_publish_list.unshift(current_post_detail);
                $scope.hub_list.unshift(current_post_detail);

                $scope.text_in_mind = '';

                $scope.select_scope_to_share = '';

                $scope.show_selected = '';

                $scope.publish_header = '';

                $scope.show_post_selected = '';

                $scope.show_default_hub = true;

                $scope.show_publish = false;

                $scope.show_publish_details = false;

                $scope.show_publish_details = false;

                $scope.show_default_community = true;

                //return false;

                $('#class_animate').removeClass('publish_show');

                $('.imageBox').css('background', '');

                $('#file').val('');

                $timeout(function () {

                    $("#content-1").mCustomScrollbar({

                        axis: "y", //set both axis scrollbars

                        advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                        // change mouse-wheel axis on-the-fly 

                        callbacks: {

                            onOverflowY: function () {

                                var opt = $(this).data("mCS").opt;

                                if (opt.mouseWheel.axis !== "y")

                                    opt.mouseWheel.axis = "y";

                            },

                            onOverflowX: function () {

                                var opt = $(this).data("mCS").opt;

                                if (opt.mouseWheel.axis !== "x")

                                    opt.mouseWheel.axis = "x";

                            },

                        }

                    });

                }, 2000);

            }

        });

    };



    /* ***************   Samarjit Debnath  *********** */

    $scope.sugessted_tags = [];

    $scope.savedtags = [];

    $scope.talkTags = function (tagData) {

        //console.log(tagData);

        if (tagData.length == 0 || tagData.length == 1 || tagData.length == 2) {

            $("#tag").hide();

        }

        if (tagData.length > 2) {

            $("#tag").show();

            UserProxy.suggestTag(tagData, function (result) {

                //console.log(result);

                $scope.sugessted_tags = result;

                //$scope.$watch();



            });

        }

    };





    /* ===================== recommend =========================== */



    //var message_send_to = [];

    $scope.recommend_talk = function (message) {

        console.log(message);

        if (message == '' || message == null) {

            alertify.error('Please enter message');

            return false;

        }

        if ($scope.savedBusinessName == 0) {

            alertify.error('Please select the sender');

            return false;

        }

        var message_send_to = $scope.savedBusinessName[0].bussinessname;



        UserProxy.recommendSendMessage(message_send_to, message, function (result) {

            //console.log(result);

            if (result.response == 'success') {

                alertify.alert("successfully sent");



                $scope.savedBusinessName = '';

                $("#demo-input-custom-limits_recommend").show();

                $("#demo-input-custom-limits_recommend").val('');

                $("#txt_msg").val('');

                /*UserProxy.getSentApi(function (result) {

                 $scope.sent_msg_list = result;

                 });*/

            }



        });



    };

    $scope.businessname = [];

    $scope.savedBusinessName = [];

    $scope.businessname_auto_search = function (auto_search_obj) {

        if (auto_search_obj.length == 0 || auto_search_obj.length == 1) {

            $("#business_name_recommend").hide();

            $(".loading").css("display", "none");

            result = '';

        }





        if (auto_search_obj.length > 1) {

            $(".loading").css("display", "block");

            $("#business_name_recommend").show();

            UserProxy.autocomplete(auto_search_obj, function (result) {

                //$(".loading").css("display","block");

                //console.log(result);

                if (result != '') {



                    $scope.businessname = result;

                    $(".loading").css("display", "none");

                    $(".no_result").css("display", "none");

                }

                if (result.length == 0) {



                    $(".loading").css("display", "none");

                    $(".no_result").css("display", "block");

                    $("#business_name_recommend").hide();

                }



            });

        }

    };

    $scope.saveBusiness_name = function (selected_business_name_obj) {

        $scope.savedBusinessName.unshift(selected_business_name_obj);

        //console.log($scope.savedBusinessName);

        $("#dropdown_list_recommend").hide();

        $scope.businessname = '';

        //$scope.business_name='';

        $("#demo-input-custom-limits_recommend").hide();

    }

    $scope.saveSuggestedTag = function (selected_tags_obj) {



        $scope.savedtags.unshift(selected_tags_obj);

        //console.log($scope.savedtags);

        $("#dropdown_list").hide();

        $scope.sugessted_tags = '';

        $(".no_result").css("display", "none");

        $(".loading").css("display", "none");



    }


    $scope.setPostUrlObj = function(){
        $scope.post_url_obj = {
            title: '',
            src: '',
            desc: '',
            news_url: '',
            title_show:'',
            src_show:'',
            desc_show:'',
            news_url_show:'',
            post_url_status:'false'
        }
        $scope.post_url_status = 'false';
    }    

    $scope.postUrl = function (data) {
        $scope.setPostUrlObj();
        UserProxy.PostUrl(data, function (result) {

            if (result.status != 'false') {

                $scope.html = result.html;

                $('#postUrlBlank').show();

                $scope.post_url_obj.title = result.title;

                $scope.post_url_obj.src = result.src;

                $scope.post_url_obj.desc = result.desc;

                $scope.post_url_obj.news_url = result.news_url;

                $scope.post_url_obj.title_show = result.title_show;

                $scope.post_url_obj.src_show = result.src_show;

                $scope.post_url_obj.desc_show = result.desc_show;

                $scope.post_url_obj.news_url_show = result.news_url_show;

                $scope.post_url_status = result.status;

                console.log(result);

            } else {
                $scope.setPostUrlObj();
                $('#postUrlBlank').hide();

            }

        });

    };

    var share_list_start_talk = [

        {

            'shareId': 0,

            'name': 'Private'

        },

        {

            'shareId': 1,

            'name': 'Public'

        }

    ];

    $scope.closeTags = function (tagObj) {

        var index = $scope.savedtags.indexOf(tagObj);

        $scope.savedtags.splice(index, 1);

    }

    $scope.closeBusinessname = function (businessObj) {

        var index = $scope.savedBusinessName.indexOf(businessObj);

        $scope.savedBusinessName.splice(index, 1);

        $("#demo-input-custom-limits_recommend").show();

    }



    $scope.share_list = share_list_start_talk;

    $scope.startTalkSubmit = function (content, title, list, tagsinput) {



        //console.log(list.shareId);

        //console.log($scope.savedtags);

        //console.log($scope.share_with_list.shareId);

        var tags = $scope.savedtags;

        var talk_access = list.shareId;

        //console.log(title);

        if (title == null || title == '') {

            alertify.error('fill the title box');

            return false;

        }

        if (content == null || content == '') {

            alertify.error('fill the content box');

            return false;

        }

        if ($scope.savedtags == 0) {

            alertify.error('please tag before post your talk');

            return false;

        }



        $("#titleBlank").val('');

        $("#contentBlank").val('');

        $("#tag_input").val('');

        $(".tag_list").val('');

        $scope.savedtags = [];



        UserProxy.addCommunity(title, content, tags, talk_access, function (result) {

            //console.log(result);



            if (result.responce == 'success') {

                $scope.myTalk = [];

                UserProxy.getCommunityDetails(function (response) {

                    // console.log(response.result);

                    $scope.myTalk = response.result;

                });

                $scope.fetch_Latest_Talk = [];

                UserProxy.fetchLatestTalk(function (result) {

                    console.log("...");

                    console.log("...");

                    console.log(result);

                    console.log("...");

                    console.log("...");

                    $scope.fetch_Latest_Talk = result;

                });

                var cur_my_talks = {

                    com_id: result.com_id,

                    comments: [],

                    content: content,

                    likes: [],

                    mid: "22",

                    tags: [],

                    title: title,

                    update_date: "now"

                };

                //console.log(cur_my_talks);

                //$scope.myTalk.unshift(cur_my_talks);

            }





        });

    }

    $scope.topTalk = [];

    UserProxy.topTalk(function (response) {

        //console.log(response);

        //console.log(response.toptalk);

        // console.log("...");

        $scope.topTalk = response.toptalk;



    });

    $scope.myTalk = [];

    UserProxy.getCommunityDetails(function (response) {

        /*console.log("mytalk");

         console.log(response.result);

         console.log("...");*/

        $scope.myTalk = response.result;



    });

    $scope.fetch_Latest_Talk = [];

    UserProxy.fetchLatestTalk(function (result) {

        $scope.fetch_Latest_Talk = result;

    });

    $scope.latest_talk_toggle_animation = function (cur_latest_talk) {

        for (var i = 0; i < $scope.fetch_Latest_Talk.length; i++) {

            if ($scope.fetch_Latest_Talk[i].com_id !== cur_latest_talk.com_id) {

                console.log(typeof ($scope.fetch_Latest_Talk[i].show_latest_talk_section));

                if ($scope.fetch_Latest_Talk[i].show_latest_talk_section == false) {

                    $scope.fetch_Latest_Talk[i].show_latest_talk_section = true;

                } else {

                    $scope.fetch_Latest_Talk[i].show_latest_talk_section = false;

                }

            }

        }

        if (cur_latest_talk.open_comment_box == false) {

            cur_latest_talk.open_comment_box = true;

        } else {

            cur_latest_talk.open_comment_box = false;

            for (var i = 0; i < $scope.fetch_Latest_Talk.length; i++) {

                if ($scope.fetch_Latest_Talk[i].com_id !== cur_latest_talk.com_id) {

                    $scope.fetch_Latest_Talk[i].open_comment_box = true;

                }

            }

        }

        for (var i = 0; i < cur_latest_talk.comments.length; i++) {

            if (cur_latest_talk.comments[i].show_sub_comment == false) {

                cur_latest_talk.comments[i].show_sub_comment = true;

            } else {

                cur_latest_talk.comments[i].show_sub_comment = false;

            }

        }

        //console.log(typeof(cur_latest_talk.show_sub_comment));

        /*if(cur_latest_talk.show_sub_comment == false){

         alert(cur_latest_talk.show_sub_comment);

         for(var i=0;i<cur_latest_talk.comments.length;i++){

         cur_latest_talk.comments[i].show_sub_comment = true;

         }

         cur_latest_talk.show_sub_comment= true;

         console.log(cur_latest_talk);

         }else{

         cur_latest_talk.show_sub_comment= false;

         for(var i=0;i<cur_latest_talk.comments.length;i++){

         //if($scope.fetch_Latest_Talk[i].com_id == cur_latest_talk.comments[i].com_id){  

         //alert($scope.fetch_Latest_Talk[i].com_id);

         cur_latest_talk.comments[i].show_sub_comment = true;

         // }else{

         // cur_latest_talk.comments[i].show_sub_comment = false;

         //}

         }

         }*/



        /*else{

         cur_latest_talk.comment[i].show_sub_comment = false;

         for(var i=0;i<$scope.fetch_Latest_Talk.length;i++){

         if($scope.fetch_Latest_Talk[i].comment[i].com_comm_id !== cur_latest_talk.comment[i].com_comm_id){                  

         $scope.fetch_Latest_Talk[i].comment[i].show_sub_comment=true;                   

         }

         }

         }

         }*/



    };

    $scope.latest_talk_toggle_animation_back = function (cur_latest_talk) {

        if (cur_latest_talk.open_comment_box == false) {

            cur_latest_talk.open_comment_box = true;

        } else {

            cur_latest_talk.open_comment_box = false;

            for (var i = 0; i < $scope.fetch_Latest_Talk.length; i++) {

                if ($scope.fetch_Latest_Talk[i].com_id !== cur_latest_talk.com_id) {

                    $scope.fetch_Latest_Talk[i].open_comment_box = true;

                }

            }

        }



    };

    $scope.feedback_top_talk = function (topTalk_like_obj) {

        //console.log(topTalk_like_obj);

        if (topTalk_like_obj.com_like == 0) {

            UserProxy.addCommunityLike(topTalk_like_obj.com_id, function (result) {

                //alert(topTalk_like_obj.com_like);

                if (result.msg = 'Your like has been placed successfully.') {

                    topTalk_like_obj.com_like = true;

                    topTalk_like_obj.total_like = parseInt(topTalk_like_obj.total_like) + 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        } else {

            //alert(topTalk_like_obj.com_like);

            UserProxy.disLike(topTalk_like_obj.com_id, function (result) {

                if (result.responce = 'success') {

                    topTalk_like_obj.com_like = false;

                    topTalk_like_obj.total_like = parseInt(topTalk_like_obj.total_like) - 1;

                }

            });

        }







    };

    $scope.feedback_latest_talk = function (latestTalk_like_obj) {

        //console.log(latestTalk_like_obj);

        if (latestTalk_like_obj.com_like == 0) {

            UserProxy.addCommunityLike(latestTalk_like_obj.com_id, function (result) {

                //alert(latestTalk_like_obj.com_like);

                if (result.msg = 'Your like has been placed successfully.') {

                    latestTalk_like_obj.com_like = true;

                    latestTalk_like_obj.total_like = parseInt(latestTalk_like_obj.total_like) + 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        } else {

            //alert(latestTalk_like_obj.com_like);

            UserProxy.disLike(latestTalk_like_obj.com_id, function (result) {

                if (result.responce = 'success') {

                    latestTalk_like_obj.com_like = false;

                    latestTalk_like_obj.total_like = parseInt(latestTalk_like_obj.total_like) - 1;

                }

            });

        }

    };

    $scope.feedback_latest_talk_comment_subComment = function (latestTalk_subComment) {

        console.log(latestTalk_subComment);

        //alert(latestTalk_subComment.comment_like_status);

        if (latestTalk_subComment.comment_like_status == false) {



            UserProxy.addCommunityLike(latestTalk_subComment.com_comm_id, function (result) {

                console.log(result);

                if (result.msg = 'Your like has  been placed successfully.') {

                    //alert("hi like");

                    latestTalk_subComment.comment_like_status = true;

                    latestTalk_subComment.comment_like_count = parseInt(latestTalk_subComment.comment_like_count) + 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        } else {

            //alert(latestTalk_like_obj.com_like);

            UserProxy.disLike(latestTalk_subComment.com_comm_id, function (result) {

                if (result.responce = 'success') {

                    latestTalk_subComment.comment_like_status = false;

                    latestTalk_subComment.comment_like_count = parseInt(latestTalk_subComment.comment_like_count) - 1;

                }

            });

        }

    };

    /* ***top_talk ** */







    $scope.deleteTalkComment = function (talk, comment, $event) {

        //console.log(talk);

        var event = $event;

        $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().parent().remove();

        UserProxy.deleteComment(comment.com_id, function (result) {

            //alert(comment.com_id);

            if (result.responce == 'success') {

                alertify.success("Post deleted successfully");

                talk.comments_count = parseInt(talk.comments_count) - 1;

                $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().parent().remove();

            }

        });

    }



    $scope.deleteMyTalk = function (talk, $event) {

        console.log(talk);

        var event = $event;

        $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().remove();

        debugger;

        UserProxy.deleteParticipation(talk.com_id, function (result) {

            alert(talk.com_id);

            console.log(result);

            if (result.responce == 'success') {

                alertify.success("Post deleted successfully");

                // talk.comments_count = parseInt(talk.comments_count) - 1;

                $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().remove();

            }

        });

    }



    /* =============  delete participate talk ========== */

    $scope.deleteParticipateTalk = function (talk, $event) {

        //console.log(talk);



        var event = $event;

        $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().remove();

        UserProxy.deleteParticipation(talk.com_id, function (result) {

            //alert(talk.com_id);

            //console.log(result);

            if (result.responce == 'success') {

                alertify.success("Post deleted successfully");

                //talk.total_comment = parseInt(talk.total_comment) - 1;

                $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().remove();

            }

        });

    }

    /* =============  delete comment latest talk ========== */

    $scope.deleteCommentLatestTalk = function (talk, comment, $event) {

        var event = $event;

        $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().parent().remove();





        UserProxy.deleteCommentLatestTalk(comment.com_id, function (result) {



            //alert(comment.com_id);

            //return false;

            console.log(result);

            if (result.responce == 'success') {

                alertify.success("Post deleted successfully");

                talk.total_comment = parseInt(talk.total_comment) - 1;

                $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().remove();

            }

        });

    }

    $scope.samr = false;

    $scope.postTalkComments = function (talk_comment_object) {





        // console.log(talk_comment_object);

        if (talk_comment_object.myTalk_comment == '') {

            alertify.error('Please enter some text');

        } else {

            UserProxy.addCommunityComment(talk_comment_object.com_id, talk_comment_object.myTalk_comment, talk_comment_object.mid, function (result) {



                if (result.msg == 'Your comment has been added successfully.') {

                    talk_comment_object.myTalk_comment = '';

                    var myTalk_Participate = {

                        com_id: result.com_id,

                        comment: result.comment,

                        mid: result.mid,

                        bussiness_name: result.bussiness_name,

                        profile_image: result.profile_image,

                        image_basepath: result.image_basepath,

                        comm_date: "now"

                    };

                    talk_comment_object.show_comments = true;

                    talk_comment_object.comments_count = parseInt(talk_comment_object.comments_count) + 1;

                    talk_comment_object.comments.push(myTalk_Participate);

                    console.log(talk_comment_object);

                    $timeout(function () {

                        $("#content-1").mCustomScrollbar({

                            axis: "y", //set both axis scrollbars

                            advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                            // change mouse-wheel axis on-the-fly 

                            callbacks: {

                                onOverflowY: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "y")

                                        opt.mouseWheel.axis = "y";

                                },

                                onOverflowX: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "x")

                                        opt.mouseWheel.axis = "x";

                                },

                            }

                        });

                    }, 2000);

                }

            });

            setInterval(function () {

                $scope.samr = true;

            }, 3000);

        }

    };

    $scope.topTalk_section = false;

    $scope.postTopTalkComments = function (topTalkObj) {

        console.log(topTalkObj);

        if (topTalkObj.topTalk_comment == null || topTalkObj.topTalk_comment == '') {

            alertify.error('Please enter some text in comment box before post');

        } else {

            UserProxy.addCommunityComment(topTalkObj.com_id, topTalkObj.topTalk_comment, topTalkObj.mid, function (result) {

                console.log(result);

                if (result.msg == 'Your comment has been added successfully.') {

                    topTalkObj.topTalk_comment = '';

                    var topTalk_Participate = {

                        com_id: result.com_id,

                        comment: result.comment,

                        mid: result.mid,

                        bussiness_name: result.bussiness_name,

                        profile_image: result.profile_image,

                        image_basepath: result.image_basepath,

                        comm_date: "now"

                    };

                    //alert(topTalkObj.bussiness_name);

                    topTalkObj.show_comments = true;

                    topTalkObj.total_comment = parseInt(topTalkObj.total_comment) + 1;

                    topTalkObj.comments.push(topTalk_Participate);

                    $timeout(function () {

                        $("#content-1").mCustomScrollbar({

                            axis: "y", //set both axis scrollbars

                            advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                            // change mouse-wheel axis on-the-fly 

                            callbacks: {

                                onOverflowY: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "y")

                                        opt.mouseWheel.axis = "y";

                                },

                                onOverflowX: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "x")

                                        opt.mouseWheel.axis = "x";

                                },

                            }

                        });

                    }, 2000);

                }

            });

            setInterval(function () {

                $scope.topTalk_section = true;

            }, 3000);

        }

    };





    $scope.commentsAnimation = function (cur_obj) {

        cur_obj.show_comments = cur_obj.show_comments == true ? false : true;

    };



    $scope.commentsAnimationMyTalk = function (myTalk_obj) {

        myTalk_obj.show_comments = myTalk_obj.show_comments == true ? false : true;

    };

    $scope.commentsAnimationTopTalk = function (topTalk_object) {

        topTalk_object.show_comments = topTalk_object.show_comments == true ? false : true;

    };

    $scope.open_animation_sub_comment = function (latestTalk_sub_comment) {



        console.log(latestTalk_sub_comment);

        latestTalk_sub_comment.show_sub_post_button = latestTalk_sub_comment.show_sub_post_button == false ? true : false;

    };



    $scope.comment_sub_comment = function (subComment_obj) {

        subComment_obj.show_sub_comment = subComment_obj.show_sub_comment == false ? true : false;

    };

    $scope.open_comment_Animation = function (cur_obj) {

        // alert(cur_obj.show_comments);

        cur_obj.open_comment_box = cur_obj.open_comment_box == true ? false : true;

    };

    $scope.open_comment_Animation_talk = function (myTalk_obj) {

        //alert(myTalk_obj.show_comments);

        myTalk_obj.open_comment_box = myTalk_obj.open_comment_box == true ? false : true;

    };

    $scope.open_comment_Animation_topTalk = function (topTalk_obj) {

        //alert(myTalk_obj.show_comments);

        topTalk_obj.open_comment_box = topTalk_obj.open_comment_box == true ? false : true;

    };



    $scope.Access_Allow_Participate = function () {

        console.log();

        UserProxy.allowAccess(com_id, function (result) {

            //console.log(result);

        });

    }

    $scope.open_recommend_Animation_latestTalk = function (recommend_obj) {

        // console.log(recommend_obj);

        $("#recommend").modal('show');

        recommend_obj.show_participate = recommend_obj.show_participate == true ? false : true;

    };

    $scope.toggleComment = function (latest_talk_toggle_comment_obj) {



        latest_talk_toggle_comment_obj.open_comment_box = latest_talk_toggle_comment_obj.open_comment_box == false ? true : false;

    };

    $scope.open_comment_Animation_latestTalk = function (latestTalk_obj) {

        //debugger;

        console.log(latestTalk_obj);



        UserProxy.perticipant(latestTalk_obj.com_id, function (response) {

            console.log(response);

            if (response.participate_stattus == false) {

                if (latestTalk_obj.talk_access == 1) {

                    latestTalk_obj.show_post_button = latestTalk_obj.show_post_button == false ? true : false;

                } else {



                    UserProxy.accessCheck(latestTalk_obj.com_id, latestTalk_obj.mid, function (result) {

                        //console.log("accessCheck");

                        //console.log(result);

                        if (result.status == 'notApplied') {

                            $("#participateModal").modal('show');

                            latestTalk_obj.show_participate = latestTalk_obj.show_participate == false ? true : false;

                            $('#participateModal').css('overflow', 'hidden');

                            $('#participateModal').css('position', 'fixed');

                        } else if (result.status == 0) {

                            alertify.alert("Your request is still not accepted please wait....");

                        } else if (result.status == 1) {

                            alertify.alert("congrats...your request is accepted");

                        } else {

                            alertify.alert("Oops!! something going wrong.");

                        }



                        $scope.sendRequest = function () {

                            //alert(latestTalk_obj.com_id);

                            UserProxy.requestAccess(latestTalk_obj.com_id, latestTalk_obj.mid, function (result) {

                                //console.log(result);

                                if (result.responce == "success") {

                                    var msg = "I am interested to participate in your talk.please accept";

                                    var sub = "access request";

                                    UserProxy.sendMessageLatestTalk(latestTalk_obj.bussiness_name, msg, sub, function (response) {

                                        //console.log("msg");

                                        //console.log(response);

                                        if (response.response == "success") {

                                            alertify.alert("your request is send to talk owner");

                                        } else {

                                            alertify.alert("Sorry request not sent.Try later");

                                        }

                                    });

                                } else if (result.responce == "success") {

                                    send_request_obj.open_comment_box = send_request_obj.open_comment_box == false ? true : false;

                                } else {

                                    alertify.alert("Sorry,your request is declined");

                                }



                            });

                            $("#participateModal").modal('hide');

                        };



                    });

                }

            } else {

                $("#participate_button").attr("disabled", "disabled");

                alertify.alert("You already participate in this talk");

            }

        })

    };

    $scope.latestTalk_section = false;

    $scope.Open_Animation_Comment_box_Latest_Talk = function (postLatestTalk_obj) {

        postLatestTalk_obj.open_comment_box = postLatestTalk_obj.open_comment_box == false ? true : false;

        console.log(postLatestTalk_obj);

        if (postLatestTalk_obj.latest_talkParticipation == null || postLatestTalk_obj.latest_talkParticipation == '') {

            alertify.error('Please enter some text in comment box before post');

        } else {

            UserProxy.addCommunityComment(postLatestTalk_obj.com_id, postLatestTalk_obj.latest_talkParticipation, postLatestTalk_obj.mid, function (result) {

                /*console.log("...");

                 console.log(result);

                 console.log("...");*/



                if (result.msg == 'Your comment has been added successfully.') {

                    //debugger;

                    postLatestTalk_obj.latest_talkParticipation = '';

                    var latestTalk_Participate = {

                        bussiness_name: postLatestTalk_obj.bussiness_name,

                        com_id: result.com_id,

                        com_comm_id: result.com_comm_id,

                        comm_date: "now",

                        comment: result.comment,

                        img_url: postLatestTalk_obj.img_url,

                        mid: result.mid,

                        profile_image: result.profile_image,

                        show_sub_comment: postLatestTalk_obj.show_sub_comment,

                        show_sub_post_button: postLatestTalk_obj.show_sub_post_button,

                        subcomment_count: 0,

                        comment_like_count: 0,

                        comment_like_status: false,

                        subcomment: []



                    };

                    var latestTalk_myTalk = {

                        bussiness_name: postLatestTalk_obj.bussiness_name,

                        com_id: postLatestTalk_obj.com_id,

                        com_like: postLatestTalk_obj.com_like,

                        comments: postLatestTalk_obj.comments,

                        comments_count: postLatestTalk_obj.total_comment,

                        content: postLatestTalk_obj.content,

                        img: postLatestTalk_obj.logomembername_url,

                        like_count: postLatestTalk_obj.total_like,

                        likes: "",

                        logomembername: postLatestTalk_obj.logomembername,

                        logomembername_url: postLatestTalk_obj.logomembername_url,

                        member_name: postLatestTalk_obj.member_name,

                        mid: postLatestTalk_obj.mid,

                        show_comments: "",

                        tags: "",

                        title: postLatestTalk_obj.title,

                        update_date: 'now'

                    };

                    console.log(latestTalk_myTalk); //return false;

                    postLatestTalk_obj.open_comment_box = false;

                    postLatestTalk_obj.total_comment = parseInt(postLatestTalk_obj.total_comment) + 1;

                    // console.log(latestTalk_Participate);

                    // console.log(postLatestTalk_obj);

                    postLatestTalk_obj.comments.push(latestTalk_Participate);

                    //postLatestTalk_obj.my_talk.push(latestTalk_myTalk);

                    $scope.myTalk.unshift(latestTalk_myTalk);

                    //console.log(postLatestTalk_obj.my_talk);



                    $timeout(function () {

                        $("#content-1").mCustomScrollbar({

                            axis: "y", //set both axis scrollbars

                            advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                            // change mouse-wheel axis on-the-fly 

                            callbacks: {

                                onOverflowY: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "y")

                                        opt.mouseWheel.axis = "y";

                                },

                                onOverflowX: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "x")

                                        opt.mouseWheel.axis = "x";

                                },

                            }

                        });

                    }, 2000);

                }

            });

        }

        setInterval(function () {

            $scope.latestTalk_section = true;

        }, 3000);

        postLatestTalk_obj.show_post_button = true;

    };

    $scope.latestTalk_sub_comment_section = false;

    $scope.postLatestTalkSubComments = function (postSubComment_obj) {

        //console.log(postSubComment_obj);

        postSubComment_obj.show_sub_comment = postSubComment_obj.show_sub_comment == false ? true : false;



        if (postSubComment_obj.latestTalk_sub_comment == null || postSubComment_obj.latestTalk_sub_comment == '') {

            alertify.error('Please enter some text in comment box before post');

        } else {

            UserProxy.insertSubComment(postSubComment_obj.com_comm_id, postSubComment_obj.latestTalk_sub_comment, postSubComment_obj.mid, function (result) {

                /*console.log("00000");

                 console.log(result);

                 console.log("00000");

                 alert(postSubComment_obj.com_comm_id);*/

                if (result.responce == 'success') {



                    var latestTalk_sub_comment = {

                        bussiness_name: result.bussiness_name,

                        profile_image: result.profile_image,

                        sub_comment_id: result.sub_comment_id,

                        comment_id: postSubComment_obj.com_comm_id,

                        sub_comment: postSubComment_obj.latestTalk_sub_comment,

                        sub_mid: postSubComment_obj.mid,

                        show_sub_comment: false,

                        sub_comm_date: "now"

                    };

                    postSubComment_obj.latestTalk_sub_comment = '';

                    console.log(latestTalk_sub_comment);

                    postSubComment_obj.show_sub_comment = false;

                    postSubComment_obj.subcomment_count = parseInt(postSubComment_obj.subcomment_count) + 1;

                    postSubComment_obj.subcomment.push(latestTalk_sub_comment);

                    $timeout(function () {

                        $("#content-1").mCustomScrollbar({

                            axis: "y", //set both axis scrollbars

                            advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                            // change mouse-wheel axis on-the-fly 

                            callbacks: {

                                onOverflowY: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "y")

                                        opt.mouseWheel.axis = "y";

                                },

                                onOverflowX: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "x")

                                        opt.mouseWheel.axis = "x";

                                },

                            }

                        });

                    }, 2000);

                }

            });

        }

        setInterval(function () {

            $scope.latestTalk_sub_comment_section = true;

        }, 3000);



    };



    /* ***************   Samarjit Debnath  *********** */



    $scope.show_default_community = true;

    $scope.show_hub_det = '';

    $scope.showHubDetails = function (hub) {

        //alert('hello');	

        hub.expand = hub.expand == false ? true : false;

    };

    /*$scope.showHubDetails = function (hub_contain_obj, type) {

     

     $scope.hub_det_type = type;

     $scope.show_hub_det = hub_contain_obj;

     $scope.show_publish = false;

     $scope.show_default_community = false;

     $scope.show_publish_details = true;

     

     };*/

    $scope.backToHub = function () {

        $scope.show_publish = false;

        $scope.show_publish_details = false;

        $scope.show_default_hub = true;

        $scope.show_default_community = true;

    };

    $scope.user_details = {};

    $scope.share_with_list = share_list;

    $scope.hub_list = [];

    $scope.text_in_mind = '';

    $scope.select_scope_to_share = '';

    $scope.show_selected = '';



//******************@@@@  Nilanjan This function for save a pos @@@@@ *********//
    $scope.select = function () {
         var current_post_detail ={};

       // alert("22222222222222222222");

        $scope.select_scope_to_share = 1;

        //$scope.show_selected = index.name;

        if ($scope.text_in_mind == '' || $scope.text_in_mind == undefined) {

            alertify.error('Please insert some text');

            return false;

        }

        if ($scope.select_scope_to_share == '' || $scope.select_scope_to_share == undefined) {

            alertify.error('Please choose scope to share');

            return false;

        }

        $scope.text_in_mind = $scope.text_in_mind.replace(/(?:https?|ftp):\/\/[\n\S]+/g, '');

     
        if ($scope.post_url_status == 'true') {

            var request = {

                method: 'POST',

                url: service_url + 'member/hub_post_api',

                data: post_formdata,

                headers: {

                    'Content-Type': undefined,

                    'auth': UserProxy.getAccessToken(),

                    'message': JSON.stringify({message: $scope.text_in_mind.replace(/(?:\r\n|\r|\n)/g, '<br />'),

                        title: $scope.post_url_obj.title,

                        desc: $scope.post_url_obj.desc,

                        src: $scope.post_url_obj.src,

                        news_url: $scope.post_url_obj.news_url

                    }),

                    //'message': {message:$scope.text_in_mind},

                    //'scope': $scope.select_scope_to_share.shareId,

                    'scope': 1,

                    'type': 1

                }

            };

        } else {

            var request = {

                method: 'POST',

                url: service_url + 'member/hub_post_api',

                data: post_formdata,

                headers: {

                    'Content-Type': undefined,

                    'auth': UserProxy.getAccessToken(),

                    'message': JSON.stringify({message: $scope.text_in_mind.replace(/(?:\r\n|\r|\n)/g, '<br />')}),

                    //'message': {message:$scope.text_in_mind},

                    //'scope': $scope.select_scope_to_share.shareId,

                    'scope': 1,

                    'type': 2

                }

            };

        }

        // SEND THE FILES.

        $('#loader-wrapper').show();

        $http(request)

                .success(function (result) {
                    $scope.post_url_status == 'false'
                    $('#loader-wrapper').hide();

                    if (result.response == 'success') {

                        //alertify.success('Sucessfully posted');

                        $('#file-3').val('');

                        $('#post_img').hide();

                        var post_img = $('#post_img>img').attr('src');

                        $('#post_img>img').attr('src', '');

                       

                       // var current_post_detail = {
                          current_post_detail = {   

                            //base_url: "http://localhost/arlians/social-network/uploads/hub_images/",


                            base_url: "",

                            bussinessname: $scope.bussinessname,

                            comment: "",

                            comment_count: 0,

                            comments: [],

                            content: $scope.post_url_status == 'true' ? $scope.post_url_obj.desc_show : $scope.text_in_mind.replace(/(?:\r\n|\r|\n)/g, '<br />'),

                            ext: result.ext,

                            hid: result.hub_id,

                            hub_post_time: "now",

                            hub_title: $scope.post_url_status == 'true' ? $scope.post_url_obj.title_show : null,

                            is_connected: [],

                            like: 0,

                            likes: 0,

                            link: $scope.post_url_status == 'true' ? $scope.post_url_obj.src_show : result.link,

                            mid: $scope.user_mid,

                            name: "",

                            news_link: $scope.post_url_status == 'true' ? $scope.post_url_obj.news_url_show : null,

                            open_comment_box: false,

                            posted_on: "",

                            front_img_name: $scope.front_img_name,

                            path: $scope.path,

                            prof_img: $scope.path + $scope.front_img_name,

                            profile_initional: $scope.user_initial,

                            //shared_for: "0",
                            shared_for: "0",

                            show_comments: false,

                            status: "1",

                            tag_id: null,

                            tag_title: {undefined: null},

                            timestamp: 0,

                            total_report: "0",

                            total_share: 0,

                           // hbcomment: $scope.post_url_status == 'true' ? $scope.text_in_mind.replace(/(?:\r\n|\r|\n)/g, '<br />') : '',
                          
                           hbcomment:  $scope.text_in_mind.replace(/(?:\r\n|\r|\n)/g, '<br />'),
                           
                            type: "hub",

                            whose_share: '',

                            whose_share_id: null,

                            post_msg: $scope.text_in_mind.replace(/(?:\r\n|\r|\n)/g, '<br />'), // nilanjan

                            flag:1,
                            type_of_post: $scope.post_url_status == 'true' ? '1' : '2'

                        };

                        console.log("Nilanjan testing post value fst");
                        console.log(current_post_detail);

                        //$scope.current_post_publish_list.unshift(current_post_detail);
                        $scope.hub_list.unshift(current_post_detail);
                        $scope.setPostUrlObj();
                        $scope.text_in_mind = '';

                        $scope.html = '';

                        $scope.select_scope_to_share = '';

                        $scope.show_selected = '';

                        $("#postUrlBlank").hide();

                        $timeout(function () {

                            $("#content-1").mCustomScrollbar({

                                axis: "y", //set both axis scrollbars

                                advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                                // change mouse-wheel axis on-the-fly 

                                callbacks: {

                                    onOverflowY: function () {

                                        var opt = $(this).data("mCS").opt;

                                        if (opt.mouseWheel.axis !== "y")

                                            opt.mouseWheel.axis = "y";

                                    },

                                    onOverflowX: function () {

                                        var opt = $(this).data("mCS").opt;

                                        if (opt.mouseWheel.axis !== "x")

                                            opt.mouseWheel.axis = "x";

                                    },

                                }

                            });

                        }, 2000);

                    }

                });

        //}

        return false;

    };

    $scope.removeDoc = function ($event) {

        $('#file-3').val('');

        $('#post_img').hide();

        $('#post_img>img').attr('src', '');

    }

    $scope.post_and_publish_listing = [];

    var post_formdata = new FormData();

    $scope.getTheFiles = function ($files) {

        //console.log($files);

        angular.forEach($files, function (value, key) {

            post_formdata.append(key, value);

        });

    };

    $scope.sharePost = function () {

        //debugger;

        UserProxy.sharePost($scope.text_in_mind, $scope.select_scope_to_share, function (result) {

            //debugger;



        });

        return false;

    };

    $scope.postComments = function (publish_object) {

        // console.log(publish_object);

        if (publish_object.comment == '') {

            alertify.error('Please enter some text');

        } else {

            UserProxy.postComments(publish_object.comment, publish_object.hub_id, function (result) {



                //  console.log("000");

                // console.log(result);

                // console.log("000");

                if (result.response == 'success') {

                    publish_object.comment = '';

                    var cur_comment = {

                        cid: result.id,

                        comment_view: result.comment,

                        buisnesname: $scope.bussinessname,

                        image: $scope.front_img_name,

                        path: $scope.path

                    };

                    //alert(result.id);

                    //console.log(cur_comment);

                    publish_object.comments_count += 1;

                    publish_object.comments.push(cur_comment);

                    publish_object.show_comments = true;

                    $timeout(function () {

                        $("#content-1").mCustomScrollbar({

                            axis: "y", //set both axis scrollbars

                            advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                            // change mouse-wheel axis on-the-fly 

                            callbacks: {

                                onOverflowY: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "y")

                                        opt.mouseWheel.axis = "y";

                                },

                                onOverflowX: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "x")

                                        opt.mouseWheel.axis = "x";

                                },

                            }

                        });

                    }, 2000);

                }

            });

        }

    };

    $scope.postNewsComments = function (publish_object) {

        //console.log(publish_object);

        if (publish_object.comment == '') {

            alertify.error('Please enter some tex');

        } else {

            UserProxy.postNewsComments(publish_object.comment, publish_object.id, function (result) {

                //alert(result.id);

                //console.log(result);

                if (result.response == 'success') {

                    var cur_comment = {

                        comm_id: result.id,

                        comment_view: result.comment,

                        buisnesname: $scope.bussinessname,

                        image: $scope.front_img_name,

                        image_path: $scope.path,

                        hub_post_time: "now",

                        mid: $scope.user_mid,

                        com_initial_name: $scope.user_initial

                    };

                    publish_object.comment = '';

                    publish_object.comment_count = parseInt(publish_object.comment_count) + 1;

                    publish_object.comments.push(cur_comment);

                    publish_object.show_comments = true;

                    $timeout(function () {

                        $("#content-1").mCustomScrollbar({

                            axis: "y", //set both axis scrollbars

                            advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                            // change mouse-wheel axis on-the-fly 

                            callbacks: {

                                onOverflowY: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "y")

                                        opt.mouseWheel.axis = "y";

                                },

                                onOverflowX: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "x")

                                        opt.mouseWheel.axis = "x";

                                },

                            }

                        });

                    }, 2000);

                }

            });

        }

    }











    $scope.postHubComments = function (publish_object) {

        //console.log(publish_object);

        if (publish_object.comment == '') {

            alertify.error('Please enter some text');

        } else {

            UserProxy.postComments(publish_object.comment, publish_object.hid, function (result) {

                //debugger;

                // console.log(result);

                if (result.response == 'success') {

                    publish_object.comment = '';

                    var cur_comment = {

                        comm_id: result.id,

                        comment_view: result.comment,

                        buisnesname: $scope.bussinessname,

                        image: $scope.front_img_name != '' ? $scope.front_img_name : null,

                        image_path: $scope.path,

                        hub_post_time: "now",

                        mid: $scope.user_mid,

                        com_initial_name: $scope.user_initial

                    };

                    //  console.log(cur_comment);

                    publish_object.comment_count += 1;

                    publish_object.comments.push(cur_comment);

                    publish_object.show_comments = true;

//                    $timeout(function () {

//                        $("#content-1").mCustomScrollbar({

//                            axis: "y", //set both axis scrollbars

//                            advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

//                            // change mouse-wheel axis on-the-fly 

//                            callbacks: {

//                                onOverflowY: function () {

//                                    var opt = $(this).data("mCS").opt;

//                                    if (opt.mouseWheel.axis !== "y")

//                                        opt.mouseWheel.axis = "y";

//                                },

//                                onOverflowX: function () {

//                                    var opt = $(this).data("mCS").opt;

//                                    if (opt.mouseWheel.axis !== "x")

//                                        opt.mouseWheel.axis = "x";

//                                },

//                            }

//                        });

//                    }, 2000);

                }

            });

        }

    };



    $scope.deleteComment = function (hub, comment, $event) {

        // console.log(hub);

        var event = $event;

        UserProxy.deleteHubComment(comment.comm_id, function (result) {

            if (result.response == 'success') {

                //alertify.success("Post deleted successfully");

                hub.comment_count = parseInt(hub.comment_count) - 1;

                $(event.target).parent().parent().parent().parent().parent().parent().parent().parent().parent().remove();

            }

        });

    }



    //$("#participateModal").hide();

    $scope.shareNewsHub = function (hub) {

        //console.log(hub);

        hub_new_obj = {

            hub_title: hub.title,

            content: hub.desc,

            link: hub.newsimg,

            news_id: hub.id,

            news_link: hub.url,

            shared_for: 'connection',

            sourceNews: hub.source,

            type_of_post:3,

            hub: hub

        }

        $scope.shareNewsObj = hub_new_obj;

        // console.log($scope.shareNewsObj);

        $('#myShareNewsModal').modal('show');

    }



    $scope.saveShareNewsComments = function (list, hub_new_obj) {

         console.log(hub_new_obj);

         console.log("nilanjan");

        UserProxy.shareNewsHub(hub_new_obj, list.shareId, function (result) {

            if (result.response = 'success') {

                //alertify.success("Successfully posted");

                hub_new_obj.hub.share_count = parseInt(hub_new_obj.hub.share_count) + 1;

                hub_share_news = {

                    //base_url: "http://arlians.online-seo-service.co.uk/uploads/hub_images/",

                    base_url: service_url + "uploads/hub_images/",

                    bussinessname: $scope.bussinessname,

                    comment: "",

                    comment_count: 0,

                    comments: [],

                    content: hub_new_obj.content,

                    ext: null,

                    hid: result.hid,

                    hub_post_time: "now",

                    hub_title: hub_new_obj.hub_title,

                    is_connected: [],

                    like: '0',

                    likes: '0',

                    link: hub_new_obj.link,

                    mid: $scope.user_mid,

                    name: "",

                    news_link: hub_new_obj.news_link,

                    open_comment_box: false,

                    posted_on: "now",

                    front_img_name: $scope.front_img_name,

                    prof_img: $scope.path + $scope.front_img_name,

                    profile_initional: $scope.user_initial,

                    shared_for: "4",

                    show_comments: false,

                    status: "1",

                    tag_id: null,

                    tag_title: {undefined: null},

                    timestamp: 0,

                    hbcomment: hub_new_obj.hbComment,

                    total_report: "0",

                    total_share: '0',

                    type: "hub",

                    whose_share: '',

                    whose_share_id: null,

                    sourceNews: hub_new_obj.sourceNews,

                    type_of_post:3,



                }

                $('#myShareNewsModal').modal('hide');

                $scope.hub_list.unshift(hub_share_news);

//                $timeout(function () {

//                    $("#content-1").mCustomScrollbar({

//                        axis: "y", //set both axis scrollbars

//                        advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

//                        // change mouse-wheel axis on-the-fly 

//                        callbacks: {

//                            onOverflowY: function () {

//                                var opt = $(this).data("mCS").opt;

//                                if (opt.mouseWheel.axis !== "y")

//                                    opt.mouseWheel.axis = "y";

//                            },

//                            onOverflowX: function () {

//                                var opt = $(this).data("mCS").opt;

//                                if (opt.mouseWheel.axis !== "x")

//                                    opt.mouseWheel.axis = "x";

//                            },

//                        }

//                    });

//                }, 500);

            } else {

                alertify.error("Some error occured please try again later");

            }

        });

    }

    $scope.saveShareComments = function (list, sharepost)

    {
        console.log("Nilanjan Shared data");

        console.log(sharepost);
      // return false;
        
         var com_cont = sharepost.hbComment + '~'+ sharepost.hbcomment;
         console.log(com_cont);

// return false;
      //  UserProxy.shareHub(sharepost.hid, sharepost.hbComment, list.shareId, function (result) {
        UserProxy.shareHub(sharepost.hid, com_cont, list.shareId, function (result) {

            if (result.response = 'success') {

                /// debugger;

                if (sharepost.shared_for == '4') {

                    var current_post_detail = {

                        base_url: service_url + "uploads/hub_images/",

                        bussinessname: $scope.bussinessname,

                        comment: "",

                        comment_count: 0,

                        comments: [],

                        content: sharepost.content,

                        ext: sharepost.ext,

                        hid: sharepost.hid,

                        hub_post_time: "now",

                        hub_title: sharepost.hub_title,

                        is_connected: [],

                        like: 0,

                        likes: 0,

                        link: sharepost.link,

                        mid: $scope.user_mid,

                        name: "",

                        news_link: sharepost.news_link,

                        open_comment_box: false,

                        posted_on: "now",

                        front_img_name: $scope.front_img_name,

                        prof_img: $scope.path + $scope.front_img_name,

                        profile_initional: $scope.user_initial,

                        shared_for: sharepost.shared_for,

                        path: $scope.path,

                        show_comments: false,

                        status: "1",

                        tag_id: "",

                        tag_title: {undefined: null},

                        timestamp: 0,

                        total_report: "0",

                        total_share: 0,

                        type: "hub",

                        flag:2,

                        hbcomment: sharepost.hbComment,

                        whose_share: sharepost.bussinessname,

                        whose_share_id: sharepost.mid,

                        whose_share_prof_img: sharepost.prof_img,

                        whose_share_logoname: sharepost.profile_initional

                    }



                } else {

                    //debugger;

                    var current_post_detail = {

                        //  base_url: "http://arlians.online-seo-service.co.uk/uploads/hub_images/",

                        //base_url: "http://localhost/arlians/social-network/uploads/hub_images/",

                        base_url: service_url + "uploads/hub_images/",

                        bussinessname: $scope.bussinessname,

                        comment: "",

                        comment_count: 0,

                        comments: [],

                        content: sharepost.content,
                        content:sharepost.content != null ?sharepost.content :sharepost.hbcomment, //nilanjan
                            
                        ext: sharepost.ext,

                        hid: sharepost.hid,

                        hub_post_time: "now",

                        hub_title: sharepost.hub_title,

                        is_connected: [],

                        like: 0,

                        likes: 0,

                        link: sharepost.link,

                        mid: $scope.user_mid,

                        name: "",

                        news_link: sharepost.news_link,

                        open_comment_box: false,

                        posted_on: "now",

                        front_img_name: $scope.front_img_name,

                        prof_img: $scope.path + $scope.front_img_name,

                        profile_initional: $scope.user_initial,

                       // shared_for: sharepost.shared_for,
                       shared_for: '1',

                        show_comments: false,

                        status: "1",

                        tag_id: null,

                        tag_title: {undefined: null},

                        timestamp: 0,

                        total_report: "0",

                        total_share: 0,

                        type: "hub",

                         flag:2,

                        hbcomment: sharepost.hbComment,

                        whose_share: sharepost.bussinessname,

                        whose_share_id: sharepost.mid,

                        whose_share_prof_img:sharepost.front_img_name!=""?sharepost.prof_img:"",

                        whose_share_logoname: sharepost.profile_initional

                    }



                }

                console.log("Current shared image 2nd");
                 console.log(current_post_detail);

                sharepost.total_share += sharepost.total_share;

                //$scope.current_post_publish_list.unshift(current_post_detail);
                $scope.hub_list.unshift(current_post_detail);
                //alertify.success('Successfully shared');



            } else {

                alertify.error = 'Some error occured';

            }

            $('#myShareModal').modal('hide');

        });

    }

    $scope.shareHub = function (hub) {

        //console.log(hub);

        $scope.shareObj = hub;

        $('#publish_details').modal('hide');

        $('#myShareModal').modal('show');

    }

    $scope.feedbackHub = function (cur_object) {

        //alert(cur_object.hub_id);

        if (cur_object.like == false) {

            UserProxy.postLike(cur_object.hub_id, function (result) {

                if (result.response = 'success') {

                    cur_object.like = true;

                    cur_object.like_count += 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        } else {

            UserProxy.deleteLike(cur_object.hub_id, function (result) {

                if (result.response = 'success') {

                    cur_object.like = false;

                    cur_object.like_count -= 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        }

        //  console.log(cur_object);

    };

    $scope.feedbackNews = function (cur_object) {

        //alert(cur_object.hub_id);

        if (cur_object.like == false) {

            UserProxy.newsLike(cur_object.id, function (result) {

                if (result.response = 'success') {

                    cur_object.like = true;

                    cur_object.like_count = parseInt(cur_object.like_count) + 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        } else {

            UserProxy.newsDislike(cur_object.id, function (result) {

                if (result.response = 'success') {

                    cur_object.like = false;

                    cur_object.like_count = parseInt(cur_object.like_count) - 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        }

        //  console.log(cur_object);

    };

    $scope.feedback = function (cur_object) {

        //console.log(cur_object);

        if (cur_object.like == 0) {

            //alert(cur_object.like);

            UserProxy.postLike(cur_object.hid, function (result) {

                if (result.response = 'success') {

                    cur_object.like = true;

                    cur_object.likes += 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        } else {

            UserProxy.deleteLike(cur_object.hid, function (result) {

                if (result.response = 'success') {

                    cur_object.like = false;

                    cur_object.likes -= 1;

                } else {

                    alertify.error = 'Some error occured';

                }

            });

        }

        //console.log(cur_object);        

    };

    $scope.readmore = function (hub_obj) {

        $('#publish_details').modal('show');

        //  console.log(hub_obj);

        $scope.hub = hub_obj;

    }

    $scope.show_publish = false;

    $scope.show_default_hub = true;

    var track_load = 0;

    var loading = false;

    var check = 0;

    var loading = false;

    $scope.hideNews = function (new_obj, $event) {

        var event = $event;

        console.log(new_obj);

        UserProxy.hideNews(new_obj, function (result) {

            if (result.response == 'success') {

                //alertify.success("Post deleted successfully"); 
                var nid = '#news-' + new_obj.id;  
               // alert(nid);


                //$(event.target).parent().parent().parent().parent().parent().parent().parent().parent().parent().parent().remove();

                $( nid ).remove();
            }

        });

    }

    $(window).scroll(function () {

         //$scope.hub_list =[];

        //if (($(window).scrollTop() + $(window).height()) == ($(document).height())) {

        if (($(window).scrollTop() > $(window).height() / 2)) {

            ///alert('hit');



            check++;

            if (loading == false) {

                loading = true;

                //UserProxy.getHubNews(track_load, function (result) {

                $.ajax({

                    //url:actionUrl,

                    //url: "http://arlians.online-seo-service.co.uk/hub/moreHubApi",

                    url: service_url + "hub/moreHubApi",

                    type: "POST",

                    dataType: "json",

                    async: true,

                    data: {user_emai: UserProxy.getAccessToken, start: track_load},

                    //data: JSON.stringify(data)

                }).success(function (result) {

                   

                    for (var i in result) {

                        //console.log(result[i]);

                        $scope.hub_list.push(result[i]);

                    }


                   // console.log("data from hub");
                 //   console.log( $scope.hub_list);

                    track_load++;

                    $timeout(function () {

                        //debugger;

                        function onScrollInit(items, trigger) {

                            items.each(function () {

                                var osElement = $(this),

                                        osAnimationClass = osElement.attr('data-os-animation'),

                                        osAnimationDelay = osElement.attr('data-os-animation-delay');

                                osElement.css({

                                    '-webkit-animation-delay': osAnimationDelay,

                                    '-moz-animation-delay': osAnimationDelay,

                                    'animation-delay': osAnimationDelay

                                });

                                var osTrigger = (trigger) ? trigger : osElement;

                                osTrigger.waypoint(function () {

                                    osElement.addClass('animated').addClass(osAnimationClass);

                                }, {

                                    triggerOnce: true,

                                    offset: '90%'

                                });

                            });

                        }

                        onScrollInit($('.os-animation'));

                        onScrollInit($('.staggered-animation'), $('.staggered-animation-container'));

                        $("#content-1").mCustomScrollbar({

                            axis: "y", //set both axis scrollbars

                            advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                            // change mouse-wheel axis on-the-fly 

                            callbacks: {

                                onOverflowY: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "y")

                                        opt.mouseWheel.axis = "y";

                                },

                                onOverflowX: function () {

                                    var opt = $(this).data("mCS").opt;

                                    if (opt.mouseWheel.axis !== "x")

                                        opt.mouseWheel.axis = "x";

                                },

                            }

                        });

                        $('#dashboard_display').show();

                    }, 2000);

                    loading = false;

                });

            }

        }

    });

    if (check === 0) {

        loading = true;

        UserProxy.getHubNews(track_load, function (result) {

            $scope.hub_list = result;

            track_load++;

            loading = false;

            $timeout(function () {

                //debugger;

                function onScrollInit(items, trigger) {

                    items.each(function () {

                        var osElement = $(this),

                                osAnimationClass = osElement.attr('data-os-animation'),

                                osAnimationDelay = osElement.attr('data-os-animation-delay');

                        osElement.css({

                            '-webkit-animation-delay': osAnimationDelay,

                            '-moz-animation-delay': osAnimationDelay,

                            'animation-delay': osAnimationDelay

                        });

                        var osTrigger = (trigger) ? trigger : osElement;

                        osTrigger.waypoint(function () {

                            osElement.addClass('animated').addClass(osAnimationClass);

                        }, {

                            triggerOnce: true,

                            offset: '90%'

                        });

                    });

                }

                onScrollInit($('.os-animation'));

                onScrollInit($('.staggered-animation'), $('.staggered-animation-container'));

                $("#content-1").mCustomScrollbar({

                    axis: "y", //set both axis scrollbars

                    advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements

                    // change mouse-wheel axis on-the-fly 

                    callbacks: {

                        onOverflowY: function () {

                            var opt = $(this).data("mCS").opt;

                            if (opt.mouseWheel.axis !== "y")

                                opt.mouseWheel.axis = "y";

                        },

                        onOverflowX: function () {

                            var opt = $(this).data("mCS").opt;

                            if (opt.mouseWheel.axis !== "x")

                                opt.mouseWheel.axis = "x";

                        },

                    }

                });

                $('#dashboard_display').show();

            }, 2000);

            $('#dashboard_display').show();

        });

    }



    $scope.deletePost = function (hub, $event) {

        //console.log($scope.hub_list);

        var event = $event;

        UserProxy.deletePost(hub.hid, function (result) {

            if (result.response == 'success') {

                //alertify.success("Post deleted successfully");

                $(event.target).parent().parent().parent().parent().parent().parent().parent().remove();

            }

        });

    };

    $scope.blockUser = function (hub) {

        console.log(hub);

        UserProxy.blockUser(hub.mid, function (result) {

            if (result.response == 'success') {

                alertify.success('User blocked')

            }

        });

    };

    $scope.showPublishSection = function () {

        $scope.show_default_hub = false;

        $scope.show_publish = true;

        $scope.show_publish_details = false;

        $('#class_animate').addClass('publish_show');

    };

    $scope.showDefault = function () {

        $scope.show_default_hub = true;

        $scope.show_publish = false;

        $scope.show_publish_details = false;

        $scope.show_publish_details = false;

        $scope.show_default_community = true;

        $('#class_animate').removeClass('publish_show');

    };

});

(function (module) {



    var fileReader = function ($q, $log) {



        var onLoad = function (reader, deferred, scope) {

            return function () {

                scope.$apply(function () {

                    deferred.resolve(reader.result);

                });

            };

        };

        var onError = function (reader, deferred, scope) {

            return function () {

                scope.$apply(function () {

                    deferred.reject(reader.result);

                });

            };

        };

        var onProgress = function (reader, scope) {

            return function (event) {

                scope.$broadcast("fileProgress",

                        {

                            total: event.total,

                            loaded: event.loaded

                        });

            };

        };

        var getReader = function (deferred, scope) {

            var reader = new FileReader();

            reader.onload = onLoad(reader, deferred, scope);

            reader.onerror = onError(reader, deferred, scope);

            reader.onprogress = onProgress(reader, scope);

            return reader;

        };

        var readAsDataURL = function (file, scope) {

            var deferred = $q.defer();

            var reader = getReader(deferred, scope);

            reader.readAsDataURL(file);

            return deferred.promise;

        };

        return {

            readAsDataUrl: readAsDataURL

        };

    };

    module.factory("fileReader",

            ["$q", "$log", fileReader]);

});

function readURLPostImage(input) {

    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {

            $('#hub_publish_default_img').hide();

            $('#hub_publish_preview_img').show();

            $('#hub_publish_preview_img').attr('src', e.target.result);

        };

        reader.readAsDataURL(input.files[0]);

    }

}

function open_publish_crop_img_modal() {

    $('#crop_publish_image').modal('show');

}

var options =

        {

            imageBox: '.imageBox',

            thumbBox: '.thumbBox',

            spinner: '.spinner',

            imgSrc: 'avatar.png'

        }

var cropper;

document.querySelector('#file').addEventListener('change', function () {

    var reader = new FileReader();

    reader.onload = function (e) {

        options.imgSrc = e.target.result;

        cropper = new cropbox(options);

    }

    reader.readAsDataURL(this.files[0]);

    this.files = [];

});

document.querySelector('#btnZoomIn').addEventListener('click', function () {

    cropper.zoomIn();

})

document.querySelector('#btnZoomOut').addEventListener('click', function () {

    cropper.zoomOut();

});

document.querySelector('#btnCrop').addEventListener('click', function () {

    try {

        var img = cropper.getDataURL();

        $('.pubimg_txt').remove();

        $('#hub_publish_default_img').hide();

        $('#hub_publish_preview_img').show();

        $('#hub_publish_preview_img').attr('src', img);

        $('#crop_publish_image').modal('hide');

        $('#publish_img_icon').removeClass('change_photo').addClass('change_photos');        

    } catch (e) {

        alertify.error('Please select a image');

    }



//document.querySelector('.cropped').innerHTML += '<img src="'+img+'">';



});

function animationCard(cur_obj, class_name) {

    if ($(cur_obj).hasClass('flipped')) {

        $(cur_obj).removeClass('flipped');

    } else {

        $(class_name).each(function () {

            $(this).removeClass('flipped');

        });

        $(cur_obj).addClass('flipped');

    }

}

function animationConsumers(cur_obj) {

    if ($(cur_obj).hasClass('flipped')) {

        $(cur_obj).removeClass('flipped');

    } else {

        $(cur_obj).addClass('flipped');

    }

}

function animatedSubSector(cur_obj) {

    if ($(cur_obj).hasClass('selectedsub')) {

        $(cur_obj).removeClass('selectedsub');

    } else {

        $(cur_obj).addClass('selectedsub');

    }

}





var limitHtmlFilter = function ($sce) {

    return function (input, limit) {

        var finalText = "";

        var counting = 1;

        var skipping = false;

        var addToText = false;

        for (var index = 0; index < input.length; index++) {

            addToText = false;

            if (skipping) {

                finalText += input[index];

                if (input[index] === ">") {

                    skipping = false;

                }

                continue;

            } else if (counting <= limit && input[index] !== "<") {

                counting++;

                addToText = true;

            } else if (input[index] === "<") {

                skipping = true;

                addToText = true;

            }



            if (addToText) {

                finalText += input[index];

            }



        }

        return $sce.trustAsHtml(finalText);

    };

};



//console.log(limitHtmlFilter()('<span><h1>Example</h1><p>Special Text</p></span>',10));



arlians.filter('limitHtml', limitHtmlFilter);



