﻿//CKEDITOR.replace('txtSummaryEditor');

        arlians.controller('profileCtrl', function ($scope, $timeout, $controller, $http) {
            $scope.business_name = '';
            $scope.own_id = '';
            $scope.detail_suggestion_list = [];
            $scope.details_suggestion = [];
            $scope.show_detail = false;
            $scope.search_module = true;
            $scope.doc_animate = true;
            $scope.profile_image = '';
            $scope.list_latest_message_viewable = [];
            $scope.suggested_connected_lists = [];
            $scope.sectorname = [];
            $scope.city = '';
            $scope.countryName = '';
            $scope.publishList = [];
            $scope.doc_uploaded = [];
            $scope.toggle_edit = true;
            $scope.mid = '';
            $scope.show_if_own_profile = true;
            $scope.show_if_other_profile = true;
            $scope.back_to_profile = false;
            $scope.showSummaryEditorAndUploadDoc = false;
            $scope.edit_business_name = '';
            $scope.edit_tagline = '';
            $scope.edit_city = '';
            $scope.selected_country = '';
            $scope.country_list = [];
            $scope.editSummary = function (current_ststus) {
                $scope.showSummaryEditorAndUploadDoc = current_ststus == false ? true : false;
            };
            $scope.business_desc_edit = '';
            $scope.message = '';
            $scope.connectionLogic = function (member_id, status) {
                if (status == 0) {
                    //$scope.isConnected = 2;
                    alertify.confirm("Sending invitation to connect", function (e) {
                        if (e) {
                            UserProxy.connectionLogic(member_id, function (result) {
                                if (result.response == 'success') {
                                    $scope.isConnected = 2;
                                    $scope.$apply(); //code by nilanjan
                                }
                            });
                        }
                    });
                } else {
                    //$scope.isConnected = 0;
                    alertify.confirm("Do you want to remove this business from your connections?", function (e) {
                        if (e) {
                            UserProxy.disconnectionLogic(member_id, function (result) {
                                if (result.response == 'success') {
                                    $scope.isConnected = 0;
                                }

                            });
                        }
                    });
                }

            }
			 $scope.openMessagePopup = function (member_id) {
        $('#message_modal').modal('show');
    }
    $scope.sendMessage = function (member_id) {
        UserProxy.sendMessage(member_id, $scope.message, function (result) {
            if (result.response == 'success') {
                $('#message_modal').modal('hide');
                $scope.message = '';
                //alertify.success('Message send');
            }
        });

    }
            UserProxy.getOtherProfileDetails(id, function (result) {
                $scope.show_if_own_profile = false;
                $scope.show_if_other_profile = true;
                $scope.back_to_profile = true;
                $scope.business_name = result.bussinessname;
                $scope.edit_business_name = result.bussinessname;
                $scope.fname = result.fname;
                $scope.lname = result.lname;
                $scope.business_tagline = result.tag_line;
                $scope.edit_tagline = result.tag_line;
                $scope.business_desc = result.bussinessdesc;
                $scope.business_desc_edit = result.bussinessdesc;
                $scope.logobussinessname = result.logobussinessname;
                // CKEDITOR.instances.txtSummaryEditor.setData(result.bussinessdesc);
                $scope.mid = result.mid;
                //$scope.profileComplition = result.profileComplition;
                $scope.connection_no = result.connection_no;
                $scope.post_no = result.post_no;
                $scope.publish_no = result.publish_no;
                $scope.opentalks_no = result.opentalks_no;
                $scope.profile_image = result.profile_image;
                $scope.background_image = result.background_image;
                //$scope.list_latest_message_viewable = result.latest_msg;
                $scope.sectorname = result.sectorname;
                //$scope.suggested_connected_lists = result.connection_with;
                $scope.city = result.city;
                $scope.edit_city = $scope.city;
                $scope.publishList = result.publishList;
                $scope.country_id = result.country;
                $scope.countryName = result.countryName;
                $scope.doc_uploaded = result.doc_uploaded;
                $scope.isConnected = result.isConnected;
                //alert($scope.isConnected);
                $timeout(function () {
                    var owl = $("#connect_business");
                    if (typeof owl.data('owlCarousel') != 'undefined') {
                        owl.data('owlCarousel').destroy();
                        owl.removeClass('owl-carousel');
                    }
                    $("#connect_business").owlCarousel({
                        navigation: true,
                        slideSpeed: 300,
                        paginationSpeed: 400,
                        singleItem: true,
                        autoPlay: true,
                        pagination: false,
                        navigationText: ["&#xf053;", "&#xf054;"]
                    });
                }, 1500);
                $timeout(function () {

                    var owl_summary = $("#summary");
                    if (typeof owl_summary.data('owlCarousel') != 'undefined') {
                        owl_summary.data('owlCarousel').destroy();
                        owl_summary.removeClass('owl-carousel');
                    }
                    $("#summary").owlCarousel({
                        itemsCustom: [
                            [0, 1],
                            [450, 1],
                            [600, 2],
                            [700, 3],
                            [1000, 4],
                            [1200, 4],
                            [1400, 4],
                            [1600, 5]
                        ],
                        navigation: true,
                        pagination: false,
                        navigationText: ["&#xf053;", "&#xf054;"]
                    });
                }, 1500);
                $timeout(function () {
                    var owl_publish = $("#publish");
                    if (typeof owl_publish.data('owlCarousel') != 'undefined') {
                        owl_publish.data('owlCarousel').destroy();
                        owl_publish.removeClass('owl-carousel');
                    }
                    $("#publish").owlCarousel({
                        itemsCustom: [
                            [0, 1],
                            [450, 1],
                            [600, 2],
                            [700, 3],
                            [1000, 3],
                            [1200, 3],
                            [1400, 3],
                            [1600, 4]
                        ],
                        navigation: true,
                        pagination: false,
                        navigationText: ["&#xf053;", "&#xf054;"]
                    });
                }, 1500);
            });


        });


(function (module) {

    var fileReader = function ($q, $log) {

        var onLoad = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
        var onProgress = function (reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress",
                        {
                            total: event.total,
                            loaded: event.loaded
                        });
            };
        };
        var getReader = function (deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();
            var reader = getReader(deferred, scope);
            reader.readAsDataURL(file);
            return deferred.promise;
        };
        return {
            readAsDataUrl: readAsDataURL
        };
    };
    module.factory("fileReader",
            ["$q", "$log", fileReader]);
});
function readURLPostImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#hub_profile_default_img').hide();
            $('#hub_profile_preview_img').show();
            $('#hub_profile_preview_img').attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}
function open_profile_crop_img_modal() {
    $('#crop_profile_image').modal('show');
}
function open_banner_crop_img_modal() {
    $('#crop_banner_image').modal('show');
}
var options =
        {
            imageBox: '.imageBox',
            thumbBox: '.thumbBox',
            spinner: '.spinner',
            imgSrc: 'avatar.png'
        }
var cropper;
document.querySelector('#file').addEventListener('change', function () {
    var reader = new FileReader();
    reader.onload = function (e) {
        options.imgSrc = e.target.result;
        cropper = new cropbox(options);
    }
    reader.readAsDataURL(this.files[0]);
    this.files = [];
});
var options_banner =
        {
            imageBox: '.imageBox_banner',
            thumbBox: '.thumbBox_banner',
            spinner: '.spinner',
            imgSrc: 'avatar.png'
        }
var cropperb;
document.querySelector('#filebanner').addEventListener('change', function () {
    var reader = new FileReader();
    reader.onload = function (e) {
        options_banner.imgSrc = e.target.result;
        cropperb = new cropbox(options_banner);
    }
    reader.readAsDataURL(this.files[0]);
    this.files = [];
});
document.querySelector('#btnZoomIn').addEventListener('click', function () {
    cropper.zoomIn();
});
document.querySelector('#btnZoomInb').addEventListener('click', function () {
    cropperb.zoomIn();
});
document.querySelector('#btnZoomOut').addEventListener('click', function () {
    cropper.zoomOut();
});
document.querySelector('#btnZoomOutb').addEventListener('click', function () {
    cropperb.zoomOut();
});
//document.querySelector('#btnCrop').addEventListener('click', function () {
//    try {
//        var img = cropper.getDataURL();      
//        UserProxy.uploadImage(img,function (result) {
//            if(result.response == true){
//                $('#hub_profile_preview_img').attr('src', img);
//                $('#crop_profile_image').modal('hide');
//            }
//        });        
//    } catch (e) {
//        alertify.error('Please select a image');
//    }
//    
//});
	

document.querySelector('#btnCropb').addEventListener('click', function () {
    try {
        var img = cropperb.getDataURL();
        $('#hub_banner_default_img').hide();
        $('#hub_banner_preview_img').show();
        $('#hub_banner_preview_img').attr('src', img);
        $('#crop_banner_image').modal('hide');
    } catch (e) {
       // alertify.error('Please select a image');
    }

    //document.querySelector('.cropped').innerHTML += '<img src="'+img+'">';

});
$("#cover_file").change(function () {
    readURL(this);
});
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#cover_img>img').attr('src', e.target.result);
        }
        $('#cover_img').show();
        reader.readAsDataURL(input.files[0]);
    }
}
arlians.directive('ellipsis', [function () {
        return {
            required: 'ngBindHtml',
            restrict: 'A',
            priority: 100,
            link: function ($scope, element, attrs, ctrl) {
                $scope.hasEllipsis = false;
                $scope.$watch(element.html(), function (value) {
                    if (!$scope.hasEllipsis) {
                        // apply ellipsis only one
                        $scope.hasEllipsis = true;
                        element.ellipsis();
                    }
                });
            }
        };
    }]);