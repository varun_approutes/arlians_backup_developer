var SelectedBuisness = function (code, name) {
    this.buisness_code = code;
    this.buisness_name = name;
}
var vmWizard = {
    step_one_selection: ko.observable(true),
    step_two_selection: ko.observable(false),
    step_three_selection: ko.observable(false),
    user_unique_buisness: ko.observable(''),
    user_selected_buisness: ko.observableArray(),
    sub_sector_list: ko.observableArray(),
    buisness_lists: ko.observableArray(),
    selected_looking_for_lists: ko.observableArray(),
    user_selected_sub_sector: ko.observableArray(),
    buisness_top_lists: ko.observableArray(),
    selected_Buisness: ko.observableArray(),
    selectionTopLines: function () {
        if (vmWizard.user_selected_buisness().length == 0) {
            alertify.error('Please select your business');
            return false;
        }
        vmWizard.step_one_selection(false);
        vmWizard.step_two_selection(true);
        vmWizard.step_three_selection(false);
    },
    selectionSectionOne: function () {
        vmWizard.step_one_selection(true);
        vmWizard.step_two_selection(false);
        vmWizard.step_three_selection(false);
    },
    showSubSectorSection: function () {
        if (vmWizard.selected_looking_for_lists().length == 0) {
            alertify.error('Please select atleast one top lines');
            return false;
        }
        UserProxy.subsectorlistToplineWise(vmWizard.selected_looking_for_lists(), function (result) {
            vmWizard.sub_sector_list(result);
        });
        vmWizard.step_one_selection(false);
        vmWizard.step_two_selection(false);
        vmWizard.step_three_selection(true);
    },
    selectionBuisnessTopLines: function () {
        //console.log(this);
        var sel_top_lines = this;
        var is_removed = 0;
        if (vmWizard.selected_looking_for_lists().length > 0) {
            for (var i = 0; i < vmWizard.selected_looking_for_lists().length; i++) {
                if (vmWizard.selected_looking_for_lists()[i].top_code == sel_top_lines.top_code) {
                    vmWizard.selected_looking_for_lists.remove(function (item) {
                        if (sel_top_lines.top_code == item.top_code) {
                            is_removed++;
                            return true;
                        }
                    });
                }
            }
            if (is_removed == 0)
                vmWizard.selected_looking_for_lists.push(sel_top_lines);
        } else {
            vmWizard.selected_looking_for_lists.push(sel_top_lines);
        }
    },
    setUserSelectedSubSector: function () {
        var sel_sub_sector = this;
        var is_removed = 0;
        if (vmWizard.user_selected_sub_sector().length > 0) {
            for (var i = 0; i < vmWizard.user_selected_sub_sector().length; i++) {
                if (vmWizard.user_selected_sub_sector()[i].code == sel_sub_sector.code) {
                    vmWizard.user_selected_sub_sector.remove(function (item) {
                        if (sel_sub_sector.code == item.code) {
                            is_removed++;
                            return true;
                        }
                    });
                }
            }
            if (is_removed == 0)
                vmWizard.user_selected_sub_sector.push(sel_sub_sector);
        } else {
            vmWizard.user_selected_sub_sector.push(sel_sub_sector);
        }
    },
    setTheFirstView: function () {
        if (vmWizard.user_selected_sub_sector().length == 0) {
            alertify.error('Please select atleast one sub sector');
            return false;
        } else {
            console.log(vmWizard.user_selected_sub_sector());
            vmWizard.selectionSectionOne();
        }
    },
    removeUserBuizness: function () {
        var select_buisness_to_remove = this;
        //alertify.confirm("Are you want to remove " + select_buisness_to_remove.buisness_name, function (e) {
            //if (e) {
                vmWizard.user_selected_buisness.remove(function (item) {
                    if (select_buisness_to_remove.buisness_code == item.buisness_code) {
                        return true;
                    }
                });
            //}
        //});
    },
    removeUserSelectedSubSector: function () {

        var select_sub_sector_to_remove = this;
        //alertify.confirm("Are you want to remove " + select_sub_sector_to_remove.name, function (e) {
            //if (e) {
                vmWizard.user_selected_sub_sector.remove(function (item) {
                    if (select_sub_sector_to_remove.code == item.code) {
                        return true;
                    }
                });
            //}
        //});
    },
    sendCombinationCode: function () {
        if (vmWizard.user_unique_buisness() == '') {
            alertify.error("it looks like you didn't complete everything");
            return false;
        }

        UserProxy.sendCombinationCode(vmWizard.user_selected_sub_sector(), vmWizard.user_selected_buisness(), vmWizard.user_unique_buisness(), function (result) {
            if (result.success == 'true') {
                window.location = "dashboard";
            } else {
                alertify.error('Business code already exit');
            }
        });
    }
}
$(function () {
    try {
        var user_email = UserProxy.getAccessToken();
        if (user_email == '' || user_email == 0 || user_email == null) {
            window.location ='';
        } 
    } catch (e) {
        alert(e);
    }
    try {
        UserProxy.getBuisnessList(function (result) {
            vmWizard.buisness_lists(result);
            setTimeout(function () {
                (function () {
                    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
                        new SelectFx(el);
                    });
                })();
                //$('.cs-options>ul>li').first().remove();
                $('#WizardCtrl').show();
            }, 1000);
        });
        UserProxy.getTopList(function (result) {
            //console.log(result);
            vmWizard.buisness_top_lists(result.top_list);
        });
    } catch (e) {
        alert(e);
    }
});
function animationBuisnessTopLines(cur_obj) {
    if ($(cur_obj).hasClass('flipped')) {
        $(cur_obj).removeClass('flipped');
    } else {
        $(cur_obj).addClass('flipped');
    }
}
function animatedSubSector(cur_obj) {
    if ($(cur_obj).hasClass('selectedsub')) {
        $(cur_obj).removeClass('selectedsub');
    } else {
        $(cur_obj).addClass('selectedsub');
    }
}
function selectedBuisness(obj) {
    var name = obj.textContent;
    var code = obj.getAttribute('data-value');
    vmWizard.user_selected_buisness.push(new SelectedBuisness(code, name));
    //console.log(vmWizard.user_selected_buisness());
}
;
ko.applyBindings(vmWizard, $("#WizardCtrl")[0]);

//var arlians = angular.module('arliansApp', []);
//arlians.controller('WizardCtrl',['$scope', function ($scope) {
//    $scope.user_email = UserProxy.getAccessToken();
//    if ($scope.user_email == '' || $scope.user_email == 0 || $scope.user_email == null) {
//        //window.location = "member/dashboard";
//    } else {
//        $scope.buisness_lists = '';
//        $scope.buisness_top_lists = '';
//        $scope.selected_buisness_list = [];
//        $scope.selected_buisness_list = [];
//        $scope.step_one_selection = true;
//        $scope.step_two_selection = false;
//        $scope.step_three_selection = false;
//        $scope.selectionTopLines = function () {
//            $scope.step_one_selection = false;
//            $scope.step_two_selection = true;
//            $scope.step_three_selection = false;
//        };
//        $scope.selectionSectionOne = function () {
//            $scope.step_one_selection = true;
//            $scope.step_two_selection = false;
//            $scope.step_three_selection = false;
//        };
//        $scope.showSubSectorSection = function () {
//            $scope.step_one_selection = false;
//            $scope.step_two_selection = false;
//            $scope.step_three_selection = true;
//        };
//        UserProxy.getBuisnessList(function (result) {
//            $scope.buisness_lists = result;
//            setTimeout(function(){
//                (function () {
//                    [].slice.call(document.querySelectorAll('select.cs-select')).forEach(function (el) {
//                        new SelectFx(el);
//                    });
//                })();
//            },1000);
//        });
//        UserProxy.getTopList(function (result) {
//            console.log(result);
//            $scope.buisness_top_lists = result;
//             
//        });
//        $scope.selectedBuisness = function(slected_buisness){  
//            //alert(slected_buisness);
//            console.log(slected_buisness);
//        };
//        //$scope.selected = $scope.getBuisnessList[0];
//        //console.log( $scope.selected);
//    }
//}]);
//
//
//function selectedBuisness(obj){  
//    var elem = angular.element(document.querySelector('#WizardCtrl'))
//    console.log(elem);
//}

