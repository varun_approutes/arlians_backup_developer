-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 08, 2015 at 10:45 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `arlians`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL,
  `type` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 = Super Admin,1 = Admin',
  `full_name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `user_image` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0' COMMENT '0 = Inactive,1 = Active'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `type`, `full_name`, `username`, `email`, `password`, `phone_no`, `description`, `user_image`, `status`) VALUES
(1, '0', 'Super Admin', 'super_admin', 'super_admin@digitalaptech.com', 'c33367701511b4f6020ec61ded352059', '9007177429', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean porttitor vestibulum imperdiet. Ut auctor accumsan erat, a vulputate metus tristique non. Aliquam aliquam vel orci quis sagittis.', '1ddf9130b33447e8294e9592b6473077.jpg', '1'),
(2, '1', 'Admin', 'admin', 'admin@digitalaptech.com', 'e10adc3949ba59abbe56e057f20f883e', '0987654321', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean porttitor vestibulum imperdiet. Ut auctor accumsan erat, a vulputate metus tristique non. Aliquam aliquam vel orci quis sagittis.', '', '1'),
(3, '1', 'BUIE ADMIN', 'admin_buie', 'admin@buie.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '1'),
(4, '1', 'God Church Admin', 'admin_god_church', 'admin_god_church@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '4324545545', 'test admin1', '1b03088493984c8581526a926b3eed37.jpeg', '1'),
(5, '1', 'Admin_bharti', 'bharti', 'admin@abhinavabharati.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '0'),
(6, '1', 'Admin_apeejay', 'apeejay', 'admin@apeejay.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `ar_bussiness`
--

CREATE TABLE IF NOT EXISTS `ar_bussiness` (
  `bussiness_id` int(11) NOT NULL,
  `bussiness_name` text,
  `bussiness_code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ar_cms`
--

CREATE TABLE IF NOT EXISTS `ar_cms` (
  `cms_id` int(11) NOT NULL,
  `cms_title` text,
  `cms_url` text,
  `cms_containt` text,
  `cms_status` enum('0','1') DEFAULT '1',
  `cms_updates` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_cms`
--

INSERT INTO `ar_cms` (`cms_id`, `cms_title`, `cms_url`, `cms_containt`, `cms_status`, `cms_updates`) VALUES
(1, 'Arlians cookie policy', 'arlians_cookie_policy', '&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;br /&gt;&lt;br /&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-top: 0.08in; font-variant: small-caps;&quot;&gt;&lt;span style=&quot;font-size: medium;&quot;&gt;&lt;strong&gt;Information about our use of cookies&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site. By continuing to browse the site, you are agreeing to our use of cookies.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer if you agree. Cookies contain information that is transferred to your computer''s hard drive.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;We use the following cookies:&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;strong&gt;Strictly necessary cookies.&lt;/strong&gt; These are cookies that are required for the operation of our website. They include, for example, cookies that enable you to log into secure areas of our website.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;strong&gt;Analytical/performance cookies.&lt;/strong&gt; They allow us to recognise and count the number of visitors and to see how visitors move around our website when they are using it. This helps us to improve the way our website works, for example, by ensuring that users are finding what they are looking for easily.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;strong&gt;Functionality cookies.&lt;/strong&gt; These are used to recognise you when you return to our website. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region).&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;strong&gt;Targeting cookies.&lt;/strong&gt; These cookies record your visit to our website, the pages you have visited and the links you have followed. We will use this information to make our website and the advertising displayed on it more relevant to your interests. We may also share this information with third parties for this purpose.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;You can find more information about the individual cookies we use and the purposes for which we use them in the table below:&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;table width=&quot;558&quot; cellspacing=&quot;0&quot; cellpadding=&quot;7&quot;&gt;&lt;colgroup&gt;&lt;col width=&quot;123&quot; /&gt; &lt;col width=&quot;124&quot; /&gt; &lt;col width=&quot;266&quot; /&gt; &lt;/colgroup&gt;\n&lt;tbody&gt;\n&lt;tr valign=&quot;TOP&quot;&gt;\n&lt;td style=&quot;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding: 0in 0in 0in 0.08in;&quot; width=&quot;123&quot;&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot;&gt;&lt;strong&gt;Cookie&lt;/strong&gt;&lt;/p&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding: 0in 0in 0in 0.08in;&quot; width=&quot;124&quot;&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot;&gt;&lt;strong&gt;Name&lt;/strong&gt;&lt;/p&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border: 1px solid #000000; padding: 0in 0.08in;&quot; width=&quot;266&quot;&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot;&gt;&lt;strong&gt;Purpose&lt;/strong&gt;&lt;/p&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr valign=&quot;TOP&quot;&gt;\n&lt;td style=&quot;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding: 0in 0in 0in 0.08in;&quot; width=&quot;123&quot;&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot;&gt;[COOKIE TITLE]&lt;/p&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding: 0in 0in 0in 0.08in;&quot; width=&quot;124&quot;&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot;&gt;[COOKIE NAME]&lt;/p&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border: 1px solid #000000; padding: 0in 0.08in;&quot; width=&quot;266&quot;&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0in;&quot;&gt;[DESCRIPTION OF THE PURPOSE FOR WHICH THE COOKIE IS USED]&lt;/p&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0in;&quot;&gt;Examples of purposes for which a cookie may be used:&lt;/p&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0in;&quot;&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p class=&quot;western&quot; lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0in;&quot;&gt;This cookie [is essential for our site to][enables us to]:&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;[Estimate our audience size and usage pattern.]&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;[Store information about your preferences, and so allow us to customise our site and to provide you with offers that are targeted at your individual interests.]&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;[Speed up your searches.]&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;[Recognise you when you return to our site.]&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot;&gt;[OTHER PURPOSES]&lt;/p&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;[Please note that third parties (including, for example, advertising networks and providers of external services like web traffic analysis services) may also use cookies, over which we have no control. These cookies are likely to be analytical/performance cookies or targeting cookies]&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;You block cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including essential cookies) you may not be able to access all or parts of our site.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;Except for essential cookies, all cookies will expire after [INSERT EXPIRY PERIOD].&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;', '1', '2015-08-05 13:15:28'),
(2, 'Arlians Privacy Policy (SB) ', 'arlians_privacy_policy_sb', '&lt;p&gt;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0in; line-height: 0.14in;&quot; align=&quot;LEFT&quot;&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0in; line-height: 0.14in;&quot; align=&quot;LEFT&quot;&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;strong&gt;Privacy Policy&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;Our mission at Arlians Limited (&quot;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;we&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&quot;) is to help businesses network, communicate, and collaborate across the globe. Our &amp;ldquo;members&amp;rdquo; share relevant information with us and communicate with other such &amp;ldquo;members&amp;rdquo;. We believe that collecting such information provides great benefit to users of Arlians. In doing so, we make your privacy one of our top priorities, this policy is intended to set out what we may do with collected information in order to provide the best service, and adhere to industry standards. This policy sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By visiting any of &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: #0000ff;&quot;&gt;&lt;u&gt;&lt;a class=&quot;western&quot; href=&quot;http://www.arlians.com/&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;www.arlians.com&lt;/span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/u&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;, &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: #0000ff;&quot;&gt;&lt;u&gt;&lt;a class=&quot;western&quot; href=&quot;http://www.arlians.net/&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;www.arlians.net&lt;/span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/u&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;, www.arlians.org, www.arlians.co.uk (&amp;ldquo;the Arlians website&amp;rdquo;) you are accepting and consenting to the practices described in this policy. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;For the purpose of the Data Protection Act 1998 (the &amp;ldquo;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;Act&lt;/span&gt;&lt;/span&gt;&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;color: #000000;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;span style=&quot;font-weight: normal;&quot;&gt;&amp;rdquo;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;), the data controller is Arlians Limited of Level 3, 207 Regent Street, London, W1B 3HH.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Information we may collect from you&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;We may collect and process the following data about you:&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;ul&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Information you give us.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt; You may give us information about you by filling in forms on &lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;span style=&quot;background: #ffff00;&quot;&gt;the Arlians website&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt; (including by creating a profile) or by corresponding with us by phone, e-mail or otherwise. This includes information you provide when you register to use our site, subscribe to the Arlians website and when you report a problem with our site. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information, personal description and photograph. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Information we collect about you.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt; With regard to each of your visits to our site (as is normal) we may automatically collect the following information:&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;/ul&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform; &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our site (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our customer service number. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;ul&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Information we receive from other sources.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt; We may receive information about you if you use any of the other websites we operate or the other services we provide. We are also working closely with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, credit reference agencies) and may receive &lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;information about you from them.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;/ul&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;text-indent: 0.1in; margin-bottom: 0.17in;&quot;&gt;&lt;br /&gt;&lt;br /&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;text-indent: 0.1in; margin-bottom: 0.17in;&quot;&gt;&lt;br /&gt;&lt;br /&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Cookies&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;The Arlians website uses cookies to distinguish you from other users of the Arlians Website. This helps us to provide you with a good experience when you browse the Arlians Website and also allows us to improve it. More information about the cookies we use and how you can block them is available from our Cookies Polic&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size: xx-small;&quot;&gt;y&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-top: 0.08in; font-variant: small-caps;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Uses made of the information&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;We use information held about you in the following ways:&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;ul&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Information you give to us.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt; We will use this information:&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;/ul&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and services that you request from us;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to provide you with information about other goods and services we offer that are similar to those that you have already purchased or enquired about;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to notify you about changes to our service;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to ensure that content from the Arlians website is presented in the most effective manner for you and for your computer. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;ul&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Information we collect about you.&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt; We will use this information:&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;/ul&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to administer the Arlians website and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to improve the Arlians website to ensure that content is presented in the most effective manner for you and for your computer; &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to allow you to participate in features of our service, when you choose to do so, for example, by placing information on the Arlians website;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;as part of our efforts to keep the Arlians website safe and secure;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in; line-height: 100%;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;to make suggestions and recommendations to you and other users of [&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;span style=&quot;background: #ffff00;&quot;&gt;website name to be inserted&lt;/span&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;] about goods or services that may interest you or them.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;ul&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Information we receive from other sources. &lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;We may combine this information with information you give to us and information we collect about you. We may use this information and the combined information for the purposes set out above (depending on the types of information we receive).&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;/ul&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Disclosure of your information&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;We may share your information with selected third parties including:&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;ul&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;analytics and search engine providers that assist us in the improvement and optimisation of the Arlians website.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;/ul&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;We may disclose your personal information to third parties:&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;ul&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;If Arlians Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;li&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce any agreement we have with you; or to protect the rights, property, or safety of Arlians, our customers, or others. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;/li&gt;\n&lt;/ul&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;By entering your own information on the Arlians website, you acknowledge that other users of the Arlians website will have access to that information and that we have no control over the use of that information by third parties. Do not place on the Arlians website any information that you are not comfortable sharing with other users of the Arlians website.&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;br /&gt;&lt;br /&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-top: 0.08in; font-variant: small-caps;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Where we store your personal data&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area (&quot;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;EEA&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&quot;). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. &lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Your rights&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;support@arlians.co&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size: xx-small;&quot;&gt;m&lt;/span&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;The Arlians website may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Access to Information&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of &amp;pound;10 to meet our costs in providing you with details of the information we hold about you.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Changes to our Privacy Policy&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;&lt;strong&gt;Contact&lt;/strong&gt;&lt;/span&gt;&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\n&lt;p lang=&quot;en-GB&quot; style=&quot;margin-bottom: 0.17in;&quot;&gt;&lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to&lt;/span&gt;&lt;/span&gt; &lt;span style=&quot;font-family: Arial,sans-serif;&quot;&gt;&lt;span style=&quot;font-size: small;&quot;&gt;hello@arlians.com&lt;/span&gt;&lt;/span&gt;&lt;span style=&quot;font-size: xx-small;&quot;&gt;.&lt;/span&gt;&lt;/p&gt;\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;', '1', '2015-08-06 08:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `ar_members`
--

CREATE TABLE IF NOT EXISTS `ar_members` (
  `mid` int(255) NOT NULL,
  `fname` text NOT NULL,
  `lname` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '0',
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `verification_code` text
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_members`
--

INSERT INTO `ar_members` (`mid`, `fname`, `lname`, `email`, `password`, `status`, `update_date`, `verification_code`) VALUES
(2, '', '', 'indrajit@digitalaptech.com', '5ce8b4e33c2362213dc22f6c908a46c6', '1', '2015-08-04 11:18:09', NULL),
(3, '', '', 'amit@digitalaptech.com', '343d9040a671c45832ee5381860e2996', '1', '2015-08-04 11:18:45', NULL),
(4, '', '', 'i.auddy@live.com', 'f27f6f1c7c5cbf4e3e192e0a47b85300', '1', '2015-08-05 11:15:30', NULL),
(5, '', '', 'ritayan@digitalaptech.com', 'ce384427c2cc33242c61b8e8cf41b632', '1', '2015-08-06 07:34:43', NULL),
(10, '', '', 'yahoo@gmail.com', 'f0a4058fd33489695d53df156b77c724', '1', '2015-08-07 13:13:45', NULL),
(12, '', '', 'i.auddy@rocketmail.com', '61283349500421908b97ba9c521fec9f', '1', '2015-08-07 13:57:31', NULL),
(15, '', '', 'aaa@gmail.com', '6950e44ec0a8a33e3ccf5af24a09aa60', '1', '2015-08-08 05:42:22', NULL),
(16, '', '', 'sales@wholesaleshopping.co.uk', '9806712c7a46a7b6b3176f3a60015d59', '1', '2015-08-08 05:43:01', NULL),
(22, '', '', 'yahoo1@gmail.com', '6950e44ec0a8a33e3ccf5af24a09aa60', '0', '2015-08-08 06:47:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ar_member_data_wiz_one`
--

CREATE TABLE IF NOT EXISTS `ar_member_data_wiz_one` (
  `wiz_one_id` int(11) NOT NULL,
  `mid` int(11) DEFAULT NULL,
  `ans_set_cat_id` int(11) DEFAULT NULL,
  `code` bigint(30) DEFAULT NULL,
  `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ar_message`
--

CREATE TABLE IF NOT EXISTS `ar_message` (
  `msg_id` bigint(11) NOT NULL,
  `msg_sub` text,
  `msg_body` text,
  `msg_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ar_message_trans`
--

CREATE TABLE IF NOT EXISTS `ar_message_trans` (
  `mt_id` int(11) NOT NULL,
  `sender_id` int(11) DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `msg_id` int(11) DEFAULT NULL,
  `msg_read` enum('0','1') DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ar_request_access_page`
--

CREATE TABLE IF NOT EXISTS `ar_request_access_page` (
  `rap_id` int(255) NOT NULL,
  `bussinessname` text NOT NULL,
  `country` text,
  `zip` varchar(255) DEFAULT NULL,
  `bussinesstype` text,
  `sector` text,
  `mid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ar_request_access_page`
--

INSERT INTO `ar_request_access_page` (`rap_id`, `bussinessname`, `country`, `zip`, `bussinesstype`, `sector`, `mid`) VALUES
(1, '', NULL, NULL, 'prof', NULL, 2),
(2, '', NULL, NULL, 'prof', NULL, 3),
(3, '', NULL, NULL, 'sep', NULL, 4),
(4, '', NULL, NULL, 'prof', NULL, 5),
(5, '', NULL, NULL, NULL, NULL, 8),
(6, '', NULL, NULL, NULL, NULL, 9),
(7, '', NULL, NULL, NULL, NULL, 10),
(9, '', NULL, NULL, NULL, NULL, 12),
(12, '', NULL, NULL, NULL, NULL, 15),
(13, '', NULL, NULL, NULL, NULL, 16),
(19, '', NULL, NULL, NULL, NULL, 22);

-- --------------------------------------------------------

--
-- Table structure for table `ar_request_access_page_mem`
--

CREATE TABLE IF NOT EXISTS `ar_request_access_page_mem` (
  `rapm_id` int(255) NOT NULL,
  `rap_id` int(255) NOT NULL,
  `mid` int(11) DEFAULT NULL,
  `is_member` enum('yes','no') NOT NULL DEFAULT 'no',
  `email_id` text NOT NULL,
  `role` text NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `update_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category_wizone`
--

CREATE TABLE IF NOT EXISTS `category_wizone` (
  `catid` int(11) NOT NULL,
  `catname` text,
  `wizonecode` int(11) DEFAULT NULL,
  `pcatid` int(11) DEFAULT NULL,
  `type` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL,
  `section` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=Inactive,1=Active'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `section`
--

INSERT INTO `section` (`id`, `section`, `status`) VALUES
(1, 'Section A', '1'),
(2, 'Section B', '1'),
(3, 'Section C', '1'),
(4, 'Section D', '1');

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE IF NOT EXISTS `site_settings` (
  `id` int(11) NOT NULL,
  `field_name` varchar(255) NOT NULL,
  `field_value` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`id`, `field_name`, `field_value`) VALUES
(1, 'site_name', 'Arlians'),
(2, 'site_url', 'http://115.115.146.52/arlians/'),
(3, 'admin_email', 'admin@domain.com'),
(4, 'phone', '123456'),
(5, 'facebook', 'http://facebook.com'),
(6, 'twitter', 'http://twitter.com'),
(7, 'logo', 'Arlians-Full-Logo.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ar_bussiness`
--
ALTER TABLE `ar_bussiness`
  ADD PRIMARY KEY (`bussiness_id`);

--
-- Indexes for table `ar_cms`
--
ALTER TABLE `ar_cms`
  ADD PRIMARY KEY (`cms_id`);

--
-- Indexes for table `ar_members`
--
ALTER TABLE `ar_members`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `ar_member_data_wiz_one`
--
ALTER TABLE `ar_member_data_wiz_one`
  ADD PRIMARY KEY (`wiz_one_id`);

--
-- Indexes for table `ar_message`
--
ALTER TABLE `ar_message`
  ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `ar_message_trans`
--
ALTER TABLE `ar_message_trans`
  ADD PRIMARY KEY (`mt_id`);

--
-- Indexes for table `ar_request_access_page`
--
ALTER TABLE `ar_request_access_page`
  ADD PRIMARY KEY (`rap_id`);

--
-- Indexes for table `ar_request_access_page_mem`
--
ALTER TABLE `ar_request_access_page_mem`
  ADD PRIMARY KEY (`rapm_id`);

--
-- Indexes for table `category_wizone`
--
ALTER TABLE `category_wizone`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ar_bussiness`
--
ALTER TABLE `ar_bussiness`
  MODIFY `bussiness_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar_cms`
--
ALTER TABLE `ar_cms`
  MODIFY `cms_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ar_members`
--
ALTER TABLE `ar_members`
  MODIFY `mid` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ar_member_data_wiz_one`
--
ALTER TABLE `ar_member_data_wiz_one`
  MODIFY `wiz_one_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar_message`
--
ALTER TABLE `ar_message`
  MODIFY `msg_id` bigint(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar_message_trans`
--
ALTER TABLE `ar_message_trans`
  MODIFY `mt_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ar_request_access_page`
--
ALTER TABLE `ar_request_access_page`
  MODIFY `rap_id` int(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `ar_request_access_page_mem`
--
ALTER TABLE `ar_request_access_page_mem`
  MODIFY `rapm_id` int(255) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `category_wizone`
--
ALTER TABLE `category_wizone`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `site_settings`
--
ALTER TABLE `site_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
