
	<div class="bottom-nav">
			<div class="container-fluid">
				<div class="row">
					<div class="container">
						<ul>
							<li class="col-lg-3 col-md-3 col-sm-4 col-xs-12 bottom-nav-col">
								<h3 class="bot-nav-col-title">COMPANY</h3>
								<ul>
									<li><a href="<?php echo base_url();?>home/pages/5/49">About Us</a></li>
									<li><a href="<?php echo base_url();?>home/pages/5/57">Career</a></li>
                                    <li><a href="<?php echo base_url();?>home/pages/5/58">Become A Broker</a></li>
									<li><a href="<?php echo base_url();?>home/pages/5/53">Rewards</a></li>
                                    <li><a href="<?php echo base_url();?>home/pages/5/61">Affiliate</a></li>
                                    <li><a href="<?php echo base_url();?>home/pages/5/62">Media Center</a></li>
								</ul>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-4 col-xs-12 bottom-nav-col ">
								<h3 class="bot-nav-col-title">SERVICES</h3>
								<ul>
									<li><a href="<?php echo base_url();?>home/pages/6/37">Home Loan</a></li>
									<li><a href="<?php echo base_url();?>home/pages/2/1">Buy</a></li>
                                    <li><a href="<?php echo base_url();?>home/pages/3/11">Invest</a></li>
                                    <li><a href="<?php echo base_url();?>home/pages/4/19">Build</a></li>
                                    <li><a href="<?php echo base_url();?>home/pages/1/29">Refinance</a></li>
                                    <li><a href="<?php echo base_url();?>home/pages/6/37">Resources</a></li>
                                    <li><a href="<?php echo base_url();?>home/pages/8/63">FAQ's</a></li>
								</ul>
							</li>
							<li class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
								<h3 class="bot-nav-col-title">SISTER COMPANIES</h3>
								<ul>
									<li><a href="#">Service Needs</a></li>
									<li><a href="#">Build Needs</a></li>
								</ul>
                                <h3 class="bot-nav-col-title">LEGAL</h3>
                                <?php $rest_of_page = $this->model_common->get_rest_pages(7,5);?>
                                <?php 
								foreach($rest_of_page as $restpage){
									if($restpage->page_title=='Privacy') $privacyid=$restpage->id;
									if($restpage->page_title=='Fees & Charges') $chargeid=$restpage->id;
									if($restpage->page_title=='Terms & Conditions') $termid=$restpage->id;
								}?>
                                <ul>
									<li><a href="<?php echo base_url();?>home/rest_pages/7/<?php echo $privacyid;?>">Privacy</a></li>
									<li><a href="<?php echo base_url();?>home/rest_pages/7/<?php echo $chargeid;?>">Fees & Charges</a></li>
                                    <li><a href="<?php echo base_url();?>home/rest_pages/7/<?php echo $termid;?>">Terms & Conditions</a></li>
								</ul>
							</li>
                            <li class="col-lg-3 col-md-3 col-xs-12">
								<h3 class="bot-nav-col-title">CONTACT US</h3>
								<ul>
									<li>Phone : 1300 41 31 21</li>
									<li>Overseas : (+61)-8-722-50441</li>
                                    <li>FAX : 1300 367181</li>
                                    <li>Email : enquiry@mortgageneeds.com.au</li>
                                    <li>Address : Suite 116<br/>147 Pirie St<br/> Adelaide SA 5000</li>
								</ul>
							</li>
							<?php /*?><li class="bottom-nav-col col-lg-5ths">
								<h3 class="bot-nav-col-title">SUBSCRIBE</h3>
								<div class="err_msg"><?php if(isset($err_msg)) echo $err_msg;?></div>
                                <div class="succ_msg"><?php if(isset($succ_msg)) echo $succ_msg;?></div>
                                <form name="subsription" id="subsription" method="post" action="<?php echo base_url();?>home/subscribe_form/">
								<input class="subscribe-input" name="subscrib_email" id="subscrib_email" value="" placeholder="Enter your email" />
                                <input class="subscribe-btn" type="submit" name="subsription-btn" value="SUBSCRIBE"  />
                                </form>
							</li>
							<li class="bottom-nav-col col-lg-5ths">
								<ul>
									<li class="social-btn fb-icon"></li>
									<li class="social-btn linkdn-icon"></li>
									<li class="social-btn twitter-icon"></li>
									<li class="social-btn gplus-icon"></li>
								</ul>
							</li><?php */?>
						</ul>
						<div class="clearfix"></div>
                        <ul class="footer-bottom">
							<li class="col-lg-4 col-sm-6">
								<div class="mfaa-logo"><img src="<?php echo base_url();?>assets/images/MFAAD_logo.png" title="MFAA LOGO" width="100%"/></div>
							</li>
                            <li class="col-lg-4 col-sm-6">
								<h3 class="bot-nav-col-title">Refer a friend to us and receive $100*</h3>
								<span style="color:#FF0000" id="referral_error"></span>
								<div class="err_msg" style="display:block;"><?php if(isset($footer_err_msg)) echo $footer_err_msg;?></div>
                                <div class="succ_msg" style="display:block;"><?php if(isset($footer_succ_msg)) echo $footer_succ_msg;?></div>
                                <form name="referral" id="referral" method="post" action="<?php echo base_url();?>page/refer_friend" >
								
                                <p>
									<input type="text" class="subscribe-input" name="user_name" id="user_name" value="" placeholder="Your name" />
									<div class="clearfix" style="color:#FF0000;" id="user_name_error"></div>
								</p>
								<p>
									<input type="text" class="subscribe-input" name="ref_friend_name" id="ref_friend_name" value="" placeholder="Your friend's name" />
									<div class="clearfix" style="color:#FF0000;" id="ref_friend_name_error"></div>
								</p>
                                <input class="subscribe-btn" type="button"	id="refer_now" value="Refer now"  onclick="validate_referral();"/>
                                </form>
							</li>
                            <li class="bottom-nav-col col-lg-4 col-sm-12">
								<h3 class="bot-nav-col-title">WE ARE SOCIAL</h3>
								<ul class="social-btn-cont">
									<li class="social-btn fb-icon"></li>
									<li class="social-btn twitter-icon"></li>
									<li class="social-btn gplus-icon"></li>
									<li class="social-btn linkdn-icon"></li>
								</ul>
                                <div class="clearfix"></div>
                                <h3 class="bot-nav-col-title"><a href="#">OUR BLOG</a></h3>
							</li>
                        </ul>
                        <DIV class="clearfix"></DIV>
                        <div style="color:#fff; text-align:center; padding-top: 15px">
							<p class="copyright">Copyright © 2014 Mortgage Needs Pty Ltd <span>|</span> ABN : 9014665594 <span>|</span> Mortgage Needs Pty Ltd.(Credit Representative Number 462031) is a corporate credit representative of BLSSA Pty Ltd. (Australian credit License Number 391237)</p>
                        
					</div>
				</div>
			</div>
		</div>

		<!--<footer class="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="container">
						<div class="footer-copyright-section">
							<p class="copyright">Copyright © 2014 Mortgage Needs Pty Ltd <span>|</span> ABN : 9014665594 <span>|</span> Mortgage Needs Pty Ltd.(Credit Representative Number 462031) is a corporate credit representative of BLSSA Pty Ltd. (Australian credit License Number 391237)</p>
							<p class="copyright"></p>
							
						</div>
					</div>
				</div>
			</div>
		</footer>-->
		<script>
		function validate_referral()
		{
			var has_error=0;
			if($('#user_name').val()=='')
			{
				$('#user_name_error').html('Your name is required.');
				$('#user_name').focus();
				has_error++;
				return false;
			}
			else
			{
				$('#user_name_error').html('');
			}
			if($('#ref_friend_name').val()=='')
			{
				$('#ref_friend_name_error').html('Referred friend name is required.');
				$('#ref_friend_name').focus();
				has_error++;
				return false;
			}
			else
			{
				$('#ref_friend_name_error').html('');
			}
			if(has_error==0)
			{
				if($('#user_id').val()=='')
				{
					var action_url='<?php echo base_url();?>page/new_user_refer_friend';
				}
				else
				{
					var action_url='<?php echo base_url();?>page/refer_friend';
				}
				$('#referral').attr('action',action_url);
				$('#referral').submit();
			}
			
		}
		</script>
	</body>
</html>
<script type="text/javascript">
/*function subscription_validation()
{
	var has_error=0;
}*/
</script>

