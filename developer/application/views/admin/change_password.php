<?php

$this->load->view('admin/header');

?>

<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="widfth:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Change Password
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/change_password/update_password')?>" id="change_password_form">
               			<input type="hidden" name="userid" id="userid" value="<?php echo $this->session->userdata('admin_user_data');?>">
               			<input type="hidden" name="usertype" id="usertype" value="<?php echo $this->session->userdata('admin_user_type');?>">
               		<!--Old Password Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">Old Password</label>
	                        <div class="col-lg-7">
	                            <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Old Password">
	                            <p style="color:#A94442" id="old_password_error"></p>
	                        </div>
	                    </div>
	                <!--New Password Section-->
	                    <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">New Password</label>
	                        <div class="col-lg-7">
	                            <input type="password" class="form-control" id="new_password" name="new_password" placeholder="New Password">
	                            <p style="color:#A94442" id="new_password_error"></p>
	                        </div>
	                    </div>
	                <!--Confiem New Password Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Confirm Password</label>
	                        <div class="col-lg-7">
	                            <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Confirm Password">
	                            <p style="color:#A94442" id="confirm_password_error"></p>
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-7">
	                        	<input type="button" class="btn btn-danger" id="hit" onclick="form_validation();" value="Update Password">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                    </div>
                	</form>
                </div>
            </div>
        </section>

    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>
<script type="text/javascript">
	function form_validation()
	{
		//alert('Jai');
		var has_error=0;
		var userid=$('#userid').val();
		var usertype=$('#usertype').val();
		if($.trim($('#old_password').val())=='')
		{
			$('#old_password_error').html('Old password is required.');
			$('#old_password').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			
			var password=$('#old_password').val();
			$.ajax({
				url : '<?php echo base_url()?>admin/change_password/check_password',
				data : 'userid='+userid+'&usertype='+usertype+'&password='+password,
				async: false,
				success : function(res){
					//alert(res);
					if(res=="False")
					{
						$('#old_password_error').html('Old password is wrong.');
						$('#old_password').css('border-color','#A94442').focus();
						has_error++;
						return false;
					}
					else
					{
						$('#old_password_error').html('');
						$('#old_password').css('border-color','');
						if($.trim($('#new_password').val())=='')
						{
							$('#new_password_error').html('New password is required.');
							$('#new_password').css('border-color','#A94442').focus();
							has_error++;
							return false;
						}
						else
						{
							$('#new_password_error').html('');
							$('#new_password').css('border-color','');
						}

						if($.trim($('#confirm_password').val())=='')
						{
							$('#confirm_password_error').html('Confirm password is required.');
							$('#confirm_password').css('border-color','#A94442').focus();
							has_error++;
							return false;
						}
						else if($('#confirm_password').val()!='')
						{
							if($('#new_password').val()!=$('#confirm_password').val())
							{
								$('#confirm_password_error').html('Password and confirm password should be same.');
								$('#confirm_password').css('border-color','#A94442').focus();
								has_error++;
								return false;
							}
							else
							{
								$('#confirm_password_error').html('');
								$('#confirm_password').css('border-color','');
							}
						}
						else
						{
							$('#confirm_password_error').html('');
							$('#confirm_password').css('border-color','');
						}
						if(has_error==0)
						{
							$('#change_password_form').submit();
						}
					}

				}
			})
		}
	}
</script>