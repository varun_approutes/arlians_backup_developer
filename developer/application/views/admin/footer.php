</section>
</section>
<?php
    //require('right_side_bar.php');
    $this->load->view('admin/right_side_bar.php');
?>
</section>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->

<script src="<?php echo base_url();?>assets/admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/bs3/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.nicescroll.js"></script>

<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="<?php echo base_url();?>assets/admin/js/skycons/skycons.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.scrollTo/jquery.scrollTo.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/calendar/clndr.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/calendar/moment-2.2.1.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/evnt.calendar.init.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jvector-map/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jvector-map/jquery-jvectormap-us-lcc-en.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/gauge/gauge.js"></script>



<!--clock init-->
<script src="<?php echo base_url();?>assets/admin/js/css3clock/js/css3clock.js"></script>
<!--Easy Pie Chart-->
<script src="<?php echo base_url();?>assets/admin/js/easypiechart/jquery.easypiechart.js"></script>
<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/admin/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="<?php echo base_url();?>assets/admin/js/scripts.js"></script>
<!--<script src="<?php echo base_url();?>assets/admin/js/external-dragging-calendar.js"></script>-->
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script> 
<!--script for this page-->
<!--dynamic table initialization -->
<script src="<?php echo base_url();?>assets/admin/js/dynamic_table_init.js"></script>
</body>
</html>

