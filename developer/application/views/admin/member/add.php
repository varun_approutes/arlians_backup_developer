<?php

$this->load->view('admin/header');
//echo count($result);
?>

<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="width:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Add Student
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/student/insert')?>" id="student_form" name="student_form" enctype="multipart/form-data">
               		<input type="hidden" name="school_id" id="school_id" value="<?php echo $this->session->userdata('admin_school_id');?>">
               		<input type="hidden" name="type_name" id="type_name" value="<?php echo $this->session->userdata('admin_school_type')?>">	
               		<input type="hidden" name="count" id="count" value="">
               		<?php
               		if($this->session->userdata('admin_user_type')=="Super_Admin")
               		{
               		?>	
               			<div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Type</label>
	                        <div class="col-lg-7">
	                           
                                <select class="form-control" id="type" name="type" onchange="show_type(this.value);">
                                    <option value="">Select Type</option>
                                    <option value="0">School</option>
                                    <option value="1">College</option>
                                </select>
                                <p style="color:#A94442" id="type_error"></p>
                               
	                            
	                        </div>
	                    </div>
               		<!--School Name Section-->
               			<div id="show_type_section">
		                    
	                    </div>
               		<!--Class Year Section-->
               			<div id="show_class_year_section">
               			</div>

                    <?php
                    }
                    else
                    {
                    	if($this->session->userdata('admin_school_type')==0)
                    	{
                    ?>
	                    	<div class="form-group">
		                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Class</label>
		                        <div class="col-lg-7">	                            
	                                    <select class="form-control" id="class" name="class">
	                                        <option value="">Select Class</option>
	                                        <?php
	                                        for($i=1;$i<=12;$i++)
	                                        {
	                                       	?>
	                                       	<option value="<?php echo $i?>"><?php echo "Class ".$i;?></option>
	                                        <?php
	                                        	
	                                        }

	                                        ?>
	                                    </select>
	                                    <p style="color:#A94442" id="class_error"></p>                               
		                        </div>
		                    </div>
	                <?php
	                   	}
	                    else
	                    {
                    		$getYear=$this->model_student->GetYear($this->session->userdata('admin_school_id'));
                    		if($getYear!='')
                    		{
	                    			
	                    ?>
	                    	<div class="form-group">
		                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Years</label>
		                        <div class="col-lg-7">	                            
	                                    <select class="form-control" id="class" name="class">
	                                        <option value="">Select Years</option>
	                                        <?php
	                                        for($year=1;$year<=$getYear;$year++)
	                    					{	
	                                       	?>
	                                       	<option value="<?php echo $year?>"><?php echo $year." Years";?></option>
	                                        <?php
	                                        	
	                                        }

	                                        ?>
	                                    </select>
	                                    <p style="color:#A94442" id="class_error"></p>                               
		                        </div>
		                    </div>
	                    <?php
	                    	}
	                    }
	                }
	                    
	                    ?>
	                   <!--Student's Name Section-->
	                   <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Name</label>
   		                    <div class="col-lg-7">
   		                            <input type="text" class="form-control" id="name" name="name" placeholder="Student FullName" value="">
   		                            <p style="color:#A94442" id="name_error"></p>
   		                    </div>
	                   </div>
	                   <!--Student's Roll No Section-->
	                   <!-- <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Roll No</label>
		                    <div class="col-lg-7">
		                            <input type="text" class="form-control" id="roll_no" name="roll_no" placeholder="Student RollNo" value="">
		                            <p style="color:#A94442" id="roll_no_error"></p>
		                    </div>
	                   </div> --> 
	                  <!-- Student's Roll No Section -->
	                   <!-- <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Registration No</label>
		                    <div class="col-lg-7">
		                            <input type="text" class="form-control" id="reg_no" name="reg_no" placeholder="Student Registration No" value="1">
		                            <p style="color:#A94442" id="reg_no_error"></p>
		                    </div>
		                    
	                   </div>  -->
	                   
	                <!--Student's Email Section-->
	                    <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Email</label>
	                        <div class="col-lg-7">
	                            <input type="email" class="location form-control" id="email" name="email" placeholder="john.doue@gmail.com" value="">
	                            <p style="color:#A94442" id="email_error"></p>
	                        </div>
	                    </div>

	              	<!--Student's Password Section-->
	                    <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Password</label>
	                        <div class="col-lg-7">
	                            <input type="password" class="location form-control" id="password" name="password" placeholder="Password" value="">
	                            <p style="color:#A94442" id="password_error"></p>
	                        </div>
	                    </div>

	                <!--Student Phone Number Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Phone Number</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Student Phone Number" value="">
	                            <p style="color:#A94442" id="phone_error"></p>
	                        </div>
	                    </div>
	                 <!--Student Address Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Address</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="address" name="address" placeholder="Student's Address" value="" onkeyup="show_address(this.id);">
	                            <p style="color:#A94442" id="address_error"></p>
	                        </div>
	                    </div>

	               <!--Student Admission Date Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Date of Admission</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="admission_date" name="admission_date" placeholder="Date of Admission" value="">
	                            <p style="color:#A94442" id="admission_date_error"></p>
	                        </div>
	                    </div>
					

	                <!--Image Upload Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Image</label>
	                        <div class="col-lg-7">
	                            <input type="file" name="image" id="image">
	                            <p style="color:#A94442" id="image_error"></p>
	                        </div>
	                    </div>
	                    <div class="form-group">
                                <label class="col-lg-3 col-sm-2 control-label">Status</label>
                                <div class="col-lg-7">
                                    <select id="source" class="form-control" id="status" name="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                    </select>
                                    <p style="color:#A94442" id="status_error"></p>
                                </div>
                        </div>
	                    <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-2">
	                        	<input type="button" class="btn btn-danger" id="hit" onclick="form_validation();" value="Add">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                        <div class="col-lg-6">
	                        	<input type="button" value="Cancel" onclick="return_back();" class="btn" style="background:#c6c6c6;color:#ffffff">
	                        	<span style="font-style: italic; margin-left: 10px;">* Field are mandatory</span>
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                    </div>
                	</form>
                </div>
            </div>
        </section>

    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>

<script>
	var dateToday = new Date(); 
  $(function() {
    $( "#admission_date" ).datepicker({
    	defaultDate: "+1w",
    	dateFormat: 'dd-mm-yy',
    	minDate: dateToday,
    });
  });
  </script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
<script>
	
	function return_back()
	{
		var back_url='<?php echo base_url()?>admin/student';
		window.location=back_url;
	}
	
	function show_address(id)
	{
		//alert(id);
		var location_id=id;
		var autocomplete = new google.maps.places.Autocomplete($("#"+location_id)[0], {});

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			console.log(place.address_components);
		});
	}

	function show_type(value)
	{
		if(value!='')
		{
			$.ajax({
				url : '<?php echo base_url()?>admin/student/show_type',
				data : 'type='+value,
				success : function(res){
					//alert(res);
					var split_response=res.split('[DIGITAL_APTECH]');
					$('#count').val(split_response[1]);
					$('#show_type_section').html(split_response[0]);
					$('#type_name').val(value);
				}
			});
		}
		else
		{
			$('#count').val('');
			$('#show_type_section').html('');
			$('#type_name').val('');
		}
		$('#show_class_year_section').html('');
	}

	function Show_Class_Year(value)
	{
		//alert(value);
		if(value!='')
		{
			var type_name=$('#type_name').val();
			$.ajax({
				url : '<?php echo base_url();?>admin/student/show_class_year',
				data : 'value='+value+'&type_name='+type_name,
				success : function(res)
				{
					$('#show_class_year_section').html(res);
				}
			});
		}
		else
		{
			$('#show_class_year_section').html('');
		}
	}
</script>
<script type="text/javascript">

	function form_validation()
	{
		var has_error=0;
		var userid=0;
		if($('#type_name').val()==0)
		{
			var class_name="Class";
		}
		if($('#type_name').val()==1)
		{
			var class_name="Year";
		}
		if($('#school_id').val()==0)
		{
			if($('#type').val()=='')
			{
				$('#type_error').html('Type is required.');
				$('#type').css('border-color','#A94442').focus();
				has_error++;
				return false;
			}
			else
			{
				$('#type_error').html('');
				$('#type').css('border-color','');
			}
			if($.trim($('#school_name').val())=='')
			{
				$('#school_name_error').html('Name is required.');
				$('#school_name').css('border-color','#A94442').focus();
				has_error++;
				return false;
			}
			else
			{
				$('#school_name_error').html('');
				$('#school_name').css('border-color','');
			}
		}
		
		if($.trim($('#class').val())=='')
		{
			$('#class_error').html(class_name+' is required.');
			$('#class').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#class_error').html('');
			$('#class').css('border-color','');
		}
		
		if($.trim($('#name').val())=='')
		{
			$('#name_error').html('Student name is required.');
			$('#name').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#name_error').html('');
			$('#name').css('border-color','');
		}
		if($.trim($('#email').val())=='')
		{
			$('#email_error').html('Email is required.');
			$('#email').css('border-color','#A94442').focus();
		}
		else
		{
			var email=$('#email').val();
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if(!re.test(email))
			{
				$('#email_error').html('Please enter valid email.');
				$('#email').css('border-color','#A94442').focus();
				has_error++;
				return false;
			}
			else
			{
				$.ajax({
					url : '<?php echo basE_url();?>admin/student/check_email_exist',
					data : 'email='+email+'&userid='+userid,
					success : function(res_email)
					{
						if(res_email=='False')
						{
							$('#email_error').html('This email is already exist. Please try another.');
							$('#email').css('border-color','#A94442').focus();
							has_error++;
							return false;
						}
						else
						{
							$('#email_error').html('');
							$('#email').css('border-color','');
							if($.trim($('#password').val())=='')
							{
								$('#password_error').html('Password is required.');
								$('#password').css('border-color','#A94442').focus();
								has_error++;
								return false;
							}
							else
							{
								var password=$('#password').val();
								if(password.length<6)
								{
									$('#password_error').html('Password length should not be less than 6 charecters.');
									$('#password').css('border-color','#A94442').focus();
									has_error++;
									return false;
								}
								else
								{
									$('#password_error').html('');
									$('#password').css('border-color','');
								}
							}

							if($.trim($('#phone').val())=='')
							{
								$('#phone_error').html('Phone number is required.');
								$('#phone').css('border-color','#A94442').focus();
								has_error++;
								return false;
							}
							else
							{
								var phone=$('#phone').val();
								$.ajax({
									url : '<?php echo base_url();?>admin/student/check_phone_exist',
									data : 'phone='+phone+'&userid='+userid,
									success : function(res_phone)
									{
										if(res_phone=='False')
										{
											$('#phone_error').html('This phone number is already exist. Please try another.');
											$('#phone').css('border-color','#A94442').focus();
											has_error++;
											return false;
										}
										else
										{
											$('#phone_error').html('');
											$('#phone').css('border-color','');
											if($.trim($('#address').val())=='')
											{
												$('#address_error').html('Address is required');
												$('#address').css('border-color','#A94442').focus();
												has_error++;
												return false;
											}
											else
											{
												$('#address_error').html('');
												$('#address').css('border-color','');
											}
											if($.trim($('#admission_date').val())=='')
											{
												$('#admission_date_error').html('Admission date is required');
												$('#admission_date').css('border-color','#A94442').focus();
												has_error++;
												return false;
											}
											else
											{
												$('#admission_date_error').html('');
												$('#admission_date').css('border-color','');
											}


											if($('#image').val()!='')
											{
												var extension = $('#image').val().split('.').pop().toUpperCase();
												if (extension!="PNG" && extension!="JPG" && extension!="GIF" && extension!="JPEG")
												{
													$('#image_error').html('File format is not supported');
													$('#image').focus();
													has_error++;
													return false;
												}
												else
												{
													$('#image_error').html('');
												}
											}
											else
											{
												$('#image_error').html('');
											}
											if(has_error==0)
											{
												$('#student_form').submit();
											}
										}
									}
								})
							}
						}
					}
				});
			}
		}
	}
</script>
