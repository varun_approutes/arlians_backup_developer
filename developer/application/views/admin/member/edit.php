<?php

$this->load->view('admin/header');
//echo count($result);
?>

<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="width:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Edit Student
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/student/update')?>" id="student_form" name="student_form" enctype="multipart/form-data">
               		<input type="hidden" name="exist_image" id="exist_image" value="<?php echo $rows[0]->student_image;?>">	
               		<input type="hidden" name="userid" id="userid" value="<?php echo $rows[0]->id;?>">	
               		<!--School Id Section-->
               		<?php
               		if($this->session->userdata('admin_user_type')=="Super_Admin")
               		{
               		?>
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*School Name</label>
	                        <div class="col-lg-7">	                            
                                    <select class="form-control" id="school_id" name="school_id" disabled="disabled">
                                        <option value="">Select School Name</option>
                                        <?php
                                        $school_list=$this->model_student->GetSchoolList();
                                        if(count($school_list)>0)
                                        {
                                        	foreach($school_list as $details)
                                        	{
                                        		if($details->id==$rows[0]->school_id)
                                        		{
                                        			$select_school="selected='selected'";
                                        		}
                                        		else
                                        		{
                                        			$select_school="";
                                        		}
                                       	?>
                                       	<option value="<?php echo $details->id?>" <?php echo $select_school;?>><?php echo $details->school_name;?></option>
                                        <?php
                                        	}
                                        }

                                        ?>
                                    </select>
                                    <p style="color:#A94442" id="school_id_error"></p>                               
	                        </div>
	                    </div>
	                <?php
	                }
	                if($rows[0]->type==0)
	                {
	                ?>
	                <!--Select Student's Class Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Class</label>
	                        <div class="col-lg-7">	                            
                                    <select class="form-control" id="class" name="class" disabled="disabled">
                                        <option value="">Select Class</option>
                                        <?php
                                        for($i=1;$i<=12;$i++)
                                        {
                                        	if($i==$rows[0]->class)
                                        	{
                                        		$select_class="selected='selected'";
                                        	}
                                        	else
                                        	{
                                        		$select_class="";
                                        	}
                                       	?>
                                       	<option value="<?php echo $i?>" <?php echo $select_class;?>><?php echo "Class ".$i;?></option>
                                        <?php
                                        	
                                        }

                                        ?>
                                    </select>
                                    <p style="color:#A94442" id="class_error"></p>                               
	                        </div>
	                    </div>
                    <?php
                    }
                    else
                    {
                    ?>
                    	<div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Years</label>
	                        <div class="col-lg-7">	                            
                                    <select class="form-control" id="class" name="class" disabled="disabled">
                                        <option value="">Select Years</option>
                                        <?php
                                        for($year=1;$years<=$rows[0]->no_of_years;$years++)
                                        {
                                        	if($years==$rows[0]->class)
                                        	{
                                        		$select_class="selected='selected'";
                                        	}
                                        	else
                                        	{
                                        		$select_class="";
                                        	}
                                       	?>
                                       	<option value="<?php echo $years;?>" <?php echo $select_class;?>><?php echo $years.' Years';?></option>
                                        <?php
                                        	
                                        }

                                        ?>
                                    </select>
                                    <p style="color:#A94442" id="class_error"></p>                               
	                        </div>
	                    </div>
                    <?php
                    }
                    ?>

	                    
	              		<!--Student's Roll number Section-->
	                   <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Roll Number</label>
   		                    <div class="col-lg-7">
   		                            <input type="text" class="form-control" id="roll_no" name="roll_no" placeholder="Student Roll Number" value="<?php echo $rows[0]->roll_no;?>" readonly="true">
   		                            <p style="color:#A94442" id="roll_no_error"></p>
   		                    </div>
	                   </div>

	                   <!--Student's Registration Section-->
	                   <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Registration Section</label>
   		                    <div class="col-lg-7">
   		                            <input type="text" class="form-control" id="reg_no" name="reg_no" placeholder="Student Registration Number" value="<?php echo $rows[0]->reg_no;?>" readonly="true">
   		                            <p style="color:#A94442" id="reg_no_error"></p>
   		                    </div>
	                   </div>

	                   <!--Student's Name Section-->
	                   <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Name</label>
   		                    <div class="col-lg-7">
   		                            <input type="text" class="form-control" id="name" name="name" placeholder="Student Full Name" value="<?php echo $rows[0]->name;?>">
   		                            <p style="color:#A94442" id="name_error"></p>
   		                    </div>
	                   </div>
	                   <!--Student's Roll No Section-->
	                   <!-- <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Roll No</label>
		                    <div class="col-lg-7">
		                            <input type="text" class="form-control" id="roll_no" name="roll_no" placeholder="Student RollNo" value="">
		                            <p style="color:#A94442" id="roll_no_error"></p>
		                    </div>
	                   </div> --> 
	                  <!-- Student's Roll No Section -->
	                   <!-- <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Registration No</label>
		                    <div class="col-lg-7">
		                            <input type="text" class="form-control" id="reg_no" name="reg_no" placeholder="Student Registration No" value="1">
		                            <p style="color:#A94442" id="reg_no_error"></p>
		                    </div>
		                    
	                   </div>  -->
	                   
	                <!--Student's Email Section-->
	                    <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Email</label>
	                        <div class="col-lg-7">
	                            <input type="email" class="location form-control" id="email" name="email" placeholder="john.doue@gmail.com" value="<?php echo $rows[0]->email;?>" readonly="true">
	                            <p style="color:#A94442" id="email_error"></p>
	                        </div>
	                    </div>

	              	
	                <!--Student Phone Number Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Phone Number</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Student Phone Number" value="<?php echo $rows[0]->phone_no;?>" readonly="true">
	                            <p style="color:#A94442" id="phone_error"></p>
	                        </div>
	                    </div>
	                 <!--Student Address Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Address</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="address" name="address" placeholder="Student's Address" value="<?php echo $rows[0]->address;?>" onkeyup="show_address(this.id);">
	                            <p style="color:#A94442" id="address_error"></p>
	                        </div>
	                    </div>

	               <!--Student Admission Date Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Date of Admission</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="admission_date" name="admission_date" placeholder="Date of Admission" value="<?php echo date('m/d/Y',strtotime($rows[0]->admission_date));?>" readonly="true">
	                            <p style="color:#A94442" id="admission_date_error"></p>
	                        </div>
	                    </div>
					
	                <!-- Student Existing Image Section -->
	                <?php
	                if($rows[0]->student_image!='')
	                {
	                	$image_path='uploads/student_image/thumb/'.$rows[0]->student_image;
	                	if(file_exists($image_path))
	                	{
	                		$get_image=base_url().'uploads/student_image/thumb/'.$rows[0]->student_image;
	                ?>
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Exisitng Image</label>
	                        <div class="col-lg-7">
	                            <img src="<?php echo $get_image?>">
	                        </div>
	                    </div>
	                    <?php
	                	}
	                    ?>
	                <?php
	            	}
	                ?>
	                <!--Image Upload Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Image</label>
	                        <div class="col-lg-7">
	                            <input type="file" name="image" id="image">
	                            <p style="color:#A94442" id="image_error"></p>
	                        </div>
	                    </div>
	                    <div class="form-group">
                                <label class="col-lg-3 col-sm-2 control-label">Status</label>
                                <div class="col-lg-7">
                                    <select id="source" class="form-control" id="status" name="status">
                                            <option value="1" <?php if($rows[0]->status==1){echo "selected";}?>>Active</option>
                                            <option value="0" <?php if($rows[0]->status==0){echo "selected";}?>>Inactive</option>
                                    </select>
                                    <p style="color:#A94442" id="status_error"></p>
                                </div>
                        </div>
	                    <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-2">
	                        	<input type="button" class="btn btn-danger" id="hit" onclick="form_validation();" value="Update">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                        <div class="col-lg-6">
	                        	<input type="button" value="Cancel" onclick="return_back();" class="btn" style="background:#c6c6c6;color:#ffffff">
	                        	<span style="font-style: italic; margin-left: 10px;">* Field are mandatory</span>
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                    </div>
                	</form>
                </div>
            </div>
        </section>

    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>

<script>
	var dateToday = new Date(); 
  $(function() {
    $( "#admission_date" ).datepicker({
    	defaultDate: "+1w",
    	dateFormat: 'dd-mm-yy',
    	minDate: dateToday,
    });
  });
  </script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
<script>
	
	function return_back()
	{
		var back_url='<?php echo base_url()?>admin/student';
		window.location=back_url;
	}
	
	function show_address(id)
	{
		//alert(id);
		var location_id=id;
		var autocomplete = new google.maps.places.Autocomplete($("#"+location_id)[0], {});

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			console.log(place.address_components);
		});
	}
</script>
<script type="text/javascript">

	function form_validation()
	{
		var has_error=0;
		var userid=$('#userid').val();
		
		
		if($.trim($('#name').val())=='')
		{
			$('#name_error').html('Student name is required.');
			$('#name').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#name_error').html('');
			$('#name').css('border-color','');
		}
		if($.trim($('#email').val())=='')
		{
			$('#email_error').html('Email is required.');
			$('#email').css('border-color','#A94442').focus();
		}
		else
		{
			var email=$('#email').val();
			var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			if(!re.test(email))
			{
				$('#email_error').html('Please enter valid email.');
				$('#email').css('border-color','#A94442').focus();
				has_error++;
				return false;
			}
			else
			{
				$.ajax({
					url : '<?php echo base_url();?>admin/student/check_email_exist',
					data : 'email='+email+'&userid='+userid,
					success : function(res_email)
					{
						if(res_email=='False')
						{
							$('#email_error').html('This email is already exist. Please try another.');
							$('#email').css('border-color','#A94442').focus();
							has_error++;
							return false;
						}
						else
						{
							$('#email_error').html('');
							$('#email').css('border-color','');

							if($.trim($('#phone').val())=='')
							{
								$('#phone_error').html('Phone number is required.');
								$('#phone').css('border-color','#A94442').focus();
								has_error++;
								return false;
							}
							else
							{
								var phone=$('#phone').val();
								$.ajax({
									url : '<?php echo base_url();?>admin/student/check_phone_exist',
									data : 'phone='+phone+'&userid='+userid,
									success : function(res_phone)
									{
										if(res_phone=='False')
										{
											$('#phone_error').html('This phone number is already exist. Please try another.');
											$('#phone').css('border-color','#A94442').focus();
											has_error++;
											return false;
										}
										else
										{
											$('#phone_error').html('');
											$('#phone').css('border-color','');
											if($.trim($('#address').val())=='')
											{
												$('#address_error').html('Address is required');
												$('#address').css('border-color','#A94442').focus();
												has_error++;
												return false;
											}
											else
											{
												$('#address_error').html('');
												$('#address').css('border-color','');
											}
											if($.trim($('#admission_date').val())=='')
											{
												$('#admission_date_error').html('Admission date is required');
												$('#admission_date').css('border-color','#A94442').focus();
												has_error++;
												return false;
											}
											else
											{
												$('#admission_date_error').html('');
												$('#admission_date').css('border-color','');
											}


											if($('#image').val()!='')
											{
												var extension = $('#image').val().split('.').pop().toUpperCase();
												if (extension!="PNG" && extension!="JPG" && extension!="GIF" && extension!="JPEG")
												{
													$('#image_error').html('File format is not supported');
													$('#image').focus();
													has_error++;
													return false;
												}
												else
												{
													$('#image_error').html('');
												}
											}
											else
											{
												$('#image_error').html('');
											}
											if(has_error==0)
											{
												$('#student_form').submit();
											}
										}
									}
								})
							}
						}
					}
				});
			}
		}
	}
</script>
