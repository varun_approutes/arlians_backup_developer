<?php
$this->load->view('admin/header.php');
//echo count($rows);
?>
<div class="row">
    <div class="col-sm-12">
        <?php
        if($error_msg!='')
        {
        ?>
        <div class="error_msg">
            <img src="<?php echo base_url().'assets/images/error.png'?>" style="width:30px;height:30px;"><?php echo $error_msg;?>
        </div>
        <?php
        }
        if ($success_msg!='') 
        {
        ?>
        <div class="success_msg">
            <img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
        </div>
        <?php
        }
        ?>
        <section class="panel">
            <header class="panel-heading">
                Member Management
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
            </header>
            <div class="panel-body">
                <!--<div class="add_new"><a href="<?php echo site_url('admin/member/add')?>">Add New Member</a></div>-->
            	<div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>
                                <?php
                                if($this->session->userdata('admin_user_type')=="Super_Admin")
                                {
                                ?>
                                <th>Member Id</th>
                                <th>Member Name</th>
                                <?php
                                }
                                ?>
                                <th>Email</th>
                               
                                <th>Bussness Name</th>
                                 <th>Bussness Type</th>
                                 <th>Registration date</th>
                                 
								 <th>Member Type</th>
								 <!--<th>Joined On</th> -->
                                <th>Status</th>
                                <!--<th class="hidden-phone">Action</th>-->
                            </tr>
                        </thead>
                        <?php
                        if(count($rows)>0)
                        {
                        ?>
                        <tbody>
                            <?php
                            foreach ($rows as $data) 
                            {   //print_r($data);die();
                            ?>
                            <tr class="gradeX">
                                <?php
                                if($this->session->userdata('admin_user_type')=="Super_Admin")
                                {
                                ?>
                                <td><?php echo $data->mid;?></td>
                                <td><?php echo $data->fname.' '.$data->lname;?></td>
                                <?php
                                }
                                ?>
                                <td>
                                    <?php
                                     
                                        echo $data->email;
                                    
                                    ?>
                                </td>
                                <td><?php echo $data->bussinessname;?></td>
                                <td><?php echo $data->bussinesstype;?></td>
								<?php $newDate = date("d-m-Y", strtotime($data->update_date));?>
                                <td><?php echo $newDate;?></td>
                                <td>
                                    <?php
									//echo $data->user_type;
                                    if($data->user_type=='Free')
                                    {
                                        echo "<a href='javascript:void(0)' onclick='change_member_status(".$data->mid.",".$data->user_type.")' title='Click here to Free'>Free</a>";
                                    }
                                    if($data->user_type=='Premium')
                                    {
                                        echo "<a href='javascript:void(0)' onclick='change_member_status(".$data->mid.",".$data->user_type.")' title='Click here to Premium'>Premium</a>";
                                    }
                                    ?>
                                </td>
								<!--<td>Joined On</td>-->
								<td>
                                    <?php
                                    if($data->status==1)
                                    {
                                        echo "<a href='javascript:void(0)' onclick='change_status(".$data->mid.",".$data->status.")' title='Click here to inactive'>ACTIVE</a>";
                                    }
                                    if($data->status==0)
                                    {
                                        echo "<a href='javascript:void(0)' onclick='change_status(".$data->mid.",".$data->status.")' title='Click here to active'>INACTIVE</a>";
                                    }
                                    ?>
                                </td>
								
                                <!--<td class="center hidden-phone">
                                   <a href="javascript:void(0)" onclick="edit(<?php echo $data->mid;?>)"><span class="fa fa-pencil-square-o"></span></a>
                                   <a href="javascript:void(0)" onclick="delete_data(<?php echo $data->mid;?>)"><span class="fa fa-trash-o"></span></a>
                                </td>-->
                                
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <?php
                        }
                        ?>
                        
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
</section>
</section>
<form id="another_form" action="" method="POST">
    <input type="hidden" name="edit_id" id="edit_id" value="">
    <input type="hidden" name="status" id="status" value="">
</form>
<?php
    //require('right_side_bar.php');
    $this->load->view('admin/right_side_bar.php');
?>
</section>

<!--Core js-->
<script src="<?php echo base_url();?>assets/admin/js/jquery.js"></script>
<script src="<?php echo base_url();?>assets/admin/bs3/js/bootstrap.min.js"></script>
<script class="include" type="text/javascript" src="<?php echo base_url();?>assets/admin/js/jquery.dcjqaccordion.2.7.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/jquery.nicescroll.js"></script>
<!--Easy Pie Chart-->
<script src="<?php echo base_url();?>assets/admin/js/easypiechart/jquery.easypiechart.js"></script>
<!--Sparkline Chart-->
<script src="<?php echo base_url();?>assets/admin/js/sparkline/jquery.sparkline.js"></script>
<!--jQuery Flot Chart-->
<script src="<?php echo base_url();?>assets/admin/js/flot-chart/jquery.flot.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/flot-chart/jquery.flot.tooltip.min.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/flot-chart/jquery.flot.resize.js"></script>
<script src="<?php echo base_url();?>assets/admin/js/flot-chart/jquery.flot.pie.resize.js"></script>

<!--dynamic table-->
<script type="text/javascript" language="javascript" src="<?php echo base_url();?>assets/admin/js/advanced-datatable/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/admin/js/data-tables/DT_bootstrap.js"></script>
<!--common script init for all pages-->
<script src="<?php echo base_url();?>assets/admin/js/scripts.js"></script>

<!--dynamic table initialization -->
<script src="<?php echo base_url();?>assets/admin/js/dynamic_table_init.js"></script>

<script type="text/javascript">
    function edit(edit_id)
    {
        var action="<?php echo site_url('admin/member/edit')?>";
        $('#another_form').attr('action',action);
        $('#edit_id').val(edit_id);
        $('#another_form').submit();
    }

    function delete_data(edit_id)
    {
        //alert(edit_id);
        var confirm_box=confirm("Are you sure to delete this member?");
        if(confirm_box==true)
        {
            var action="<?php echo site_url('admin/member/delete')?>";
            $('#another_form').attr('action',action);
            $('#edit_id').val(edit_id);
            $('#another_form').submit();
        }
    }

    function change_status(edit_id,recent_status)
    {
        if(recent_status==1)
        {
            var update_status='inactive';
        }
        if(recent_status==0)
        {
            var update_status='active';
        }
        var confirm_box=confirm("Are you sure to "+update_status+" this member?");
        if(confirm_box==true)
        {
            var action="<?php echo site_url('admin/member/update_status')?>";
            $('#another_form').attr('action',action);
            $('#edit_id').val(edit_id);
             $('#status').val(recent_status);
            $('#another_form').submit();
        }
    }

    function change_member_status(edit_id,status)
    {
		
        if(status=='Premium')
        {
            var update_status='Free';
        }
        if(status=='Free')
        {
            var update_status='active';
        }
        var confirm_box=confirm("Are you sure to "+update_status+" this member?");
        if(confirm_box==true)
        {
            var action="<?php echo site_url('admin/member/update_type')?>";
            $('#another_form').attr('action',action);
            $('#edit_id').val(edit_id);
             $('#status').val(status);
            $('#another_form').submit();
        }
    }
</script>


</body>
</html>