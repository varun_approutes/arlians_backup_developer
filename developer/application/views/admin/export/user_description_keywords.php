<?php
    echo $header;
?>
<div class="row">
    <div class="col-sm-12">
        <?php echo $this->utility->showMsg();?>
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="<?php echo base_url().'admin/'?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url().'admin/export/description_concepts'?>">Description Keywords</a>
                    </li>
					<li>
                        <a href="<?php echo base_url().'admin/export/description_concepts/view/'.$user_id?>" class="current"><?php echo $title;?></a>
                    </li>
                </ul>
            </div>
        </div>
        <section class="panel">
            <header class="panel-heading">
                <?php echo $title;?>
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
            </header>
            <div class="panel-body">
            	<div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>
								<th>#</th>
                                <th>Keyword</th>
                                <th>Score</th>
                                <th class="no_sort">Action</th>
                            </tr>
                        </thead>
                        <?php
                        if(count($rows)>0)
                        {
                        ?>
                        <tbody>
                            <?php
                            foreach ($rows as $key=>$data) 
                            {   
                            ?>
                                <tr class="gradeX">
									<td><?php echo $key+1;?></td>
                                    <td><?php echo $data['keyword'];?></td>
									<td><?php echo $data['score'];?></td>
                                    <td class="center hidden-phone">
                                       <a href="<?php echo base_url().'admin/export/delete_description_keyword/'.  base64_encode($data['id']).'/'.$this->utility->getSecurity();?>" class="confirmME" ><span class="fa fa-trash-o"></span></a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <?php
                        }
                        ?>
                        
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
</section>
</section>
<form id="another_form" action="" method="POST">
    <input type="hidden" name="edit_id" id="edit_id" value="">
    <input type="hidden" name="status" id="status" value="">
</form>
</section>
<?php echo $footer;?>