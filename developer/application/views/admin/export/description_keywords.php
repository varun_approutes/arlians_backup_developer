<?php
    echo $header;
?>
<script type="text/javascript">
	jQuery(function($){
		$('#export_btn').click(function(){
			var hrefx=$(this).attr('href');
			//$(this).attr('href','');
			$(this).attr('disabled','disabled');
			$(this).html('Executing...');
			//alert(hrefx);
			//window.location.href = hrefx;
		});
	});
</script>
<div class="row">
    <div class="col-sm-12">
        <?php echo $this->utility->showMsg();?>
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumbs-alt">
                    <li>
                        <a href="<?php echo base_url().'admin/'?>">Home</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url().'admin/export/description_concepts'?>" class="current"><?php echo $title;?></a>
                    </li>
                </ul>
            </div>
        </div>
        <section class="panel">
            <header class="panel-heading">
                <?php echo $title;?>
                <span class="tools pull-right">
                    <a href="javascript:;" class="fa fa-chevron-down"></a>
                    <a href="javascript:;" class="fa fa-cog"></a>
                    <a href="javascript:;" class="fa fa-times"></a>
                 </span>
            </header>
            <div class="panel-body">
				<div class="add_new"><a id="export_btn" href="<?php echo site_url('admin/export/description_concepts_db')?>">Get Users Keywords</a></div>
            	<div class="adv-table">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>
                                <th>User</th>
                                <th>Description</th>
                                <th>Total Keywords</th>
								<th>Last Exported Date</th>
                                <th class="no_sort">Action</th>
                            </tr>
                        </thead>
                        <?php
                        if(count($rows)>0)
                        {
                        ?>
                        <tbody>
                            <?php
                            foreach ($rows as $data) 
                            {   
                            ?>
                                <tr class="gradeX">
                                    <td><?php echo $data['mid'];?></td>
                                    <td><?php echo html_entity_decode($data['bussinessdesc']);?></td>
                                    <td><?php echo count($data['user_description_keywords']);?></td>
                                    <td><?php echo date('d-M-Y h:i A',$data['user_description_keywords'][0]['date_of_creation']);?></td>
                                    <td class="center hidden-phone">
                                        <a href="<?php echo base_url().'admin/export/description_concepts/view/'.  base64_encode($data['mid'])?>"><span class="fa fa-pencil-square-o"></span></a>
                                       <a href="<?php echo base_url().'admin/export/description_concepts/delete/'.  base64_encode($data['mid']).'/'.$this->utility->getSecurity();?>" class="confirmME" ><span class="fa fa-trash-o"></span></a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <?php
                        }
                        ?>
                        
                    </table>
                </div>
            </div>
        </section>
    </div>
</div>
</section>
</section>
</section>
<?php echo $footer;?>