<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="ThemeBucket">
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/admin/images/favicon.png">
    <title>Arlians Admin</title>
    <!--Core CSS -->
    <link href="<?php echo base_url();?>assets/admin/bs3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/js/jquery-ui/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/bootstrap-reset.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/css/bootstrap-switch.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/js/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/admin/js/bootstrap-timepicker/css/timepicker.css" />
    <link href="<?php echo base_url();?>assets/admin/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/js/jvector-map/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/clndr.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/developer.css" rel="stylesheet">
    <!--clock css-->
    <link href="<?php echo base_url();?>assets/admin/js/css3clock/css/style.css" rel="stylesheet">
    <!--Morris Chart CSS -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/js/morris-chart/morris.css">
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url();?>assets/admin/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/admin/css/style-responsive.css" rel="stylesheet"/>
    <!--dynamic table-->
    <link href="<?php echo base_url();?>assets/admin/js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>assets/admin/js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo base_url();?>assets/admin/js/data-tables/DT_bootstrap.css" />
    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<script src="<?php echo base_url();?>assets/admin/js/jquery.js"></script>
</head>
<body>
<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    <a href="index.html" class="logo">
        
        <img src="<?php echo base_url();?>uploads/logo/thumbs/<?php echo LOGO;?>" alt="">
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->


<div class="top-nav clearfix">
    <!--search & user info start-->
    <ul class="nav pull-right top-menu">
        <?php
        $user_details=$this->modeladmin->User_Details();
        //echo '<pre>';print_r($user_details);
        ?>
       
        <!-- user login dropdown start-->
        <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img alt="" src="<?php echo $user_details['admin_image'];?>">
                <span class="username"><?php echo $user_details['admin_name'];?></span>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
                <li><a href="<?php echo site_url('admin/profile');?>"><i class=" fa fa-suitcase"></i>Profile</a></li>
                <li><a href="<?php echo site_url('admin/change_password');?>"><i class="fa fa-key"></i> Change Password</a></li>
                <li><a href="<?php echo base_url();?>" target="_blank"><i class="fa fa-desktop"></i> View Site</a></li>
                <li><a href="<?php echo site_url('admin/admin/logout');?>"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
        </li>
        <!-- <li>
            <div class="toggle-right-box">
                <div class="fa fa-bars"></div>
            </div>
        </li> -->
    </ul>
    <!--search & user info end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a <?php if($this->uri->segment(2)=='dashboard'){echo "class='active'";}?> href="<?php echo site_url('admin/dashboard');?>">
                        <i class="fa fa-dashboard"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
            
                <li>
                    <a <?php if($this->uri->segment(2)=='site_settings'){echo "class='active'";}?> href="<?php echo site_url('admin/site_settings');?>">
                        <i class="fa fa-cog"></i>
                        <span>Site Settings</span>
                    </a>
                </li>
              
               <!-- <li>
                    <a <?php if($this->uri->segment(2)=='category'){echo "class='active'";}?> href="<?php echo site_url('admin/category');?>">
                        <i class="fa fa-building-o"></i>
                        <span>Manage Own Business</span>
                    </a>
                </li>-->
  <?php /* ?>
                 <li>
                    <a <?php if($this->uri->segment(2)=='admin_management'){echo "class='active'";}?> href="<?php echo site_url('admin/admin_management');?>">
                        <i class="fa fa-user"></i>
                        <span>Admin Management</span>
                    </a>
                </li>

                
                 <li>
                    <a <?php if($this->uri->segment(2)=='principal'){echo "class='active'";}?> href="<?php echo site_url('admin/principal');?>">
                        <i class="fa fa-user"></i>
                        <span>Principal</span>
                    </a>
                </li>

                <li>
                    <a <?php if($this->uri->segment(2)=='teacher'){echo "class='active'";}?> href="<?php echo site_url('admin/teacher');?>">
                        <i class="fa fa-user"></i>
                        <span>Teacher</span>
                    </a>
                </li>
                <?php */ ?>
				
                <li>
                    <a <?php if($this->uri->segment(2)=='member' && $this->uri->segment(3)!='referral' ){echo "class='active'";}?> href="<?php echo site_url('admin/member');?>">
                        <i class="fa fa-user"></i>
                        <span>Member</span>
                    </a>
                </li>
				<li>
                    <a <?php if($this->uri->segment(3)=='referral'){echo "class='active'";}?> href="<?php echo site_url('admin/member/referral');?>">
                        <i class="fa fa-user"></i>
                        <span>Referral</span>
                    </a>
                </li>
				<li class="sub-menu">
                    <a <?php if($this->uri->segment(3)=='export'){echo "class='active'";}?> href="javascript:;" class="dcjq-parent">
                        <i class="fa fa-cloud-download"></i>
                        <span>Export</span>
						<span class="dcjq-icon"></span></a>
						<ul class="sub" style="display: block;">
							<li><a href="<?php echo base_url().'admin/export/description_concepts'?>">Description Keywords</a></li>
						</ul>
                </li>
                <li>
                    <a <?php if($this->uri->segment(3)=='referral'){echo "class='active'";}?> href="<?php echo site_url('admin/hub/reportHub');?>">
                        <i class="fa fa-user"></i>
                        <span>Hub Report</span>
                    </a>
                </li>
                <li>
                    <a <?php if($this->uri->segment(3)=='referral'){echo "class='active'";}?> href="<?php echo site_url('admin/hub/subsector_view');?>">
                        <i class="fa fa-user"></i>
                        <span>Sub sector View</span>
                    </a>
                </li>
                <li>
                    <a <?php if($this->uri->segment(3)=='referral'){echo "class='active'";}?> href="<?php echo site_url('admin/hub/bussnissview');?>">
                        <i class="fa fa-user"></i>
                        <span>Bussniss View</span>
                    </a>
                </li>
                 <li>
                    <a <?php if($this->uri->segment(3)=='referral'){echo "class='active'";}?> href="<?php echo site_url('admin/hub/industry_view');?>">
                        <i class="fa fa-user"></i>
                        <span>Industry View</span>
                    </a>
                </li>
                <?php /* ?>
                <?php if($this->session->userdata('admin_user_type')=='Super_Admin'){?>
                <li>
                    <a <?php if($this->uri->segment(2)=='student_parent'){echo "class='active'";}?> href="<?php echo site_url('admin/student_parent');?>">
                        <i class="fa fa-user"></i>
                        <span>Parent</span>
                    </a>
                </li>
                <?php }?>
                
                <?php $show_class=$this->modeladmin->GetList($this->session->userdata('admin_school_id'));
                if(count($show_class)>0){?>
                <li>
                    <a <?php if($this->uri->segment(2)=='student_parent'){echo "class='active'";}?> href="<?php echo site_url('admin/student_parent');?>">
                        <i class="fa fa-user"></i>
                        <span>Parent</span>
                    </a>
                </li>
                <?php }?>
                
                <?php
                if(($this->session->userdata('admin_user_type')=='Principal') || ($this->session->userdata('admin_user_type')=='Teacher'))
                {
                ?>
                <li>
                    <?php
                    if($this->session->userdata('admin_user_type')=='Principal')
                    {
                        $class_topic_site_url=site_url('admin/class_topic');
                    }
                    if($this->session->userdata('admin_user_type')=='Teacher')
                    {
                        $class_topic_site_url=site_url('admin/class_topic/view');
                    }
                    ?>
                    <a <?php if($this->uri->segment(2)=='class_topic'){echo "class='active'";}?> href="<?php echo $class_topic_site_url;?>">
                        <i class="fa fa-book"></i>
                        <span>Class Topic</span>
                    </a>
                </li>
                <?php
                }
                if($this->session->userdata('admin_user_type')=='Teacher')
                {
                ?>
                <li>
                    <a <?php if($this->uri->segment(2)=='attendence'){echo "class='active'";}?> href="<?php echo site_url('admin/attendence');?>">
                        <i class="fa fa-pencil-square-o"></i>
                        <span>Attendance</span>
                    </a>
                </li>
                <?php
                }
                ?>
                <li>
                    <a <?php if($this->uri->segment(2)=='notice'){echo "class='active'";}?> href="<?php echo site_url('admin/notice');?>">
                        <i class="fa fa-file-text"></i>
                        <span>Notice</span>
                    </a>
                </li>
                <?php
                if($this->session->userdata('admin_user_type')!='Super_Admin')
                {
                ?>
                <li class="sub-menu">
                    <a href="javascript:void(0);" <?php if(($this->uri->segment(2)=='teacher_report') || ($this->uri->segment(2)=='student_report') || ($this->uri->segment(2)=='class_topic_report') || ($this->uri->segment(2)=='class_report') || ($this->uri->segment(2)=='monthly_report') || ($this->uri->segment(2)=='per_day_report')){echo 'class="active"';}?>>
                        <i class=" fa fa-bar-chart-o"></i>
                        <span>Report</span>
                    </a>
                    <ul class="sub">
                        <li <?php if($this->uri->segment(2)=='per_day_report'){echo 'class="active"';}?>><a href="<?php echo site_url('admin/per_day_report');?>">Per Day Report</a></li>
                        <li <?php if($this->uri->segment(2)=='teacher_report'){echo 'class="active"';}?>><a href="<?php echo site_url('admin/teacher_report');?>">Teacher Report</a></li>
                        <li <?php if($this->uri->segment(2)=='student_report'){echo 'class="active"';}?>><a href="<?php echo site_url('admin/student_report');?>">Student Report</a></li>
                        <li <?php if($this->uri->segment(2)=='class_topic_report'){echo 'class="active"';}?>><a href="<?php echo site_url('admin/class_topic_report');?>">Class Topic Report</a></li>
                        <li <?php if($this->uri->segment(2)=='class_report'){echo 'class="active"';}?>><a href="<?php echo site_url('admin/class_report');?>">Class Report</a></li>
                        <li <?php if($this->uri->segment(2)=='monthly_report'){echo 'class="active"';}?>><a href="<?php echo site_url('admin/monthly_report');?>">Monthly Report</a></li>
                        <!-- <li><a href="chartjs.html">Chartjs</a></li>
                        <li><a href="flot_chart.html">Flot Charts</a></li>
                        <li><a href="c3_chart.html">C3 Chart</a></li> -->
                    </ul>
                </li>
                <?php
                }
                ?>

                <?php */ ?>
            </ul>            
        </div>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
<!--main content start-->
<section id="main-content" class="">
<section class="wrapper">