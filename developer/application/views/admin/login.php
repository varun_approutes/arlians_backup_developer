<?php
$this->load->view('admin/login_header');

?>

    <div class="container">
      <?php
      if($error_msg!='')
      {
      ?>
      <div class="admin_error_msg">
        <img src="<?php echo base_url().'assets/images/error.png'?>" style="30px;height:30px;"><?php echo $error_msg;?>
      </div>
      <?php
      }
      if($success_msg!='')
      {
      ?>
      <div class="admin_success_msg">
        <img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="30px;height:30px;"><?php echo $error_msg;?>
      </div>
      <?php
      }
      ?>
      <form class="form-signin" action="<?php echo site_url('admin/admin/authenticate');?>" style="margin:16px auto;" id="admin-signin" enctype="multipart/form-data" method="POST">
        <h2 class="form-signin-heading">sign in now </h2>
        <div class="login-wrap">
            <div class="user-login-info"> 

            <!-- Section start for user type -->

             <!-- <select class="form-control input-sm m-bot15" style="padding : 5px !important;" name="usertype" id="usertype">
                  <option value="" <?php if($this->input->cookie('admin_cookie_usertype')==''){echo "selected";}?>>Select Usertype</option>
                  <option value="0" <?php if(($this->input->cookie('admin_cookie_usertype')!='') && ($this->input->cookie('admin_cookie_usertype')==0)){echo "selected";}?>>Super Admin</option>
                  <option value="1" <?php if($this->input->cookie('admin_cookie_usertype')==1){echo "selected";}?>>Admin</option>
                  <option value="2" <?php if($this->input->cookie('admin_cookie_usertype')==2){echo "selected";}?>>Principal</option>
                  <option value="3" <?php if($this->input->cookie('admin_cookie_usertype')==3){echo "selected";}?>>Teacher</option>
              </select>-->
              <input type="hidden" name="usertype" value="0"/>
              <div class="clearfix"></div>
              <div style="color:#ff0000;margin-bottom:10px;display:none;" id="usertype_error"></div>
                
            <!-- Section start for user name -->
              <input type="text" class="form-control" placeholder="User Name" name="username" id="username" value="<?php if($this->input->cookie('admin_cookie_username')!=''){echo $this->input->cookie('admin_cookie_username');}?>">
              <div class="clearfix"></div>
              <div style="color:#ff0000;margin-bottom:10px;display:none;" id="username_error"></div>

            <!-- Section start for password -->
              <input type="password" class="form-control" placeholder="Password" name="password" id="password" value="<?php if($this->input->cookie('admin_cookie_password')!=''){echo $this->input->cookie('admin_cookie_password');}?>">
              <div class="clearfix"></div>
              <div style="color:#ff0000;margin-bottom:10px;display:none;" id="password_error"></div>
            </div>
            <label class="checkbox">
                <input type="checkbox" name="remember_me" id="remember_me" value="1" <?php if($this->input->cookie('admin_cookie_value')=='1'){echo 'checked';}?>> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password? </a>

                </span>
            </label>
            <button class="btn btn-lg btn-login btn-block" type="button" onclick="form_validation();">Sign in</button>

            <!-- <div class="registration">
                Don't have an account yet?
                <a class="" href="registration.html">
                    Create an account
                </a>
            </div> -->

        </div>

          <!-- Modal -->
          <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_x">&times;</button>
                          <h4 class="modal-title">Forgot Password ?</h4>
                      </div>                       
                      <div class="modal-body">
                        <p>Select your User Type to reset your password.</p>
                          <select class="form-control input-sm m-bot15" style="padding : 5px !important;" name="usertype_name" id="usertype_name" onchange="getval(this);">
                            <option value="" <?php if($this->input->cookie('admin_cookie_usertype')==''){echo "selected";}?>>Select Usertype</option>
                            <option value="0" <?php if(($this->input->cookie('admin_cookie_usertype')!='') && ($this->input->cookie('admin_cookie_usertype')==0)){echo "selected";}?>>Super Admin</option>
                            <option value="1" <?php if($this->input->cookie('admin_cookie_usertype')==1){echo "selected";}?>>Admin</option>
                            <option value="2" <?php if($this->input->cookie('admin_cookie_usertype')==2){echo "selected";}?>>Principal</option>
                            <option value="3" <?php if($this->input->cookie('admin_cookie_usertype')==3){echo "selected";}?>>Teacher</option>
                          </select>
                          <!-- <div id="hid_user"></div> -->
                          <input type="hidden" id="hid_user" value="">
                          <div class="clearfix"></div>
                          <p style="color:#A94442;display:none;" id="usertype_name_error"></p>
                          <p>Enter your e-mail address below to reset your password.</p>
                          <input type="email" id="email" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
                          <p style="color:#A94442" id="email_error"></p>
                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button" id="f_cancel">Cancel</button>
                          <button class="btn btn-success" type="button" onclick="email_validation();">Submit</button>
                      </div>
                  </div>
              </div>
          </div>
          <!-- modal -->

      </form>

    </div>
<?php
$this->load->view('admin/login_footer');
?>
<script type="text/javascript">
  $('#f_cancel').click(function()
  {
    $("#usertype_name").val("");
    $("#email").val("");
  });
  $('#close_x').click(function()
  {
    $("#usertype_name").val("");
    $("#email").val("");
  });

  function form_validation()
  {
    var haserror=0;
    if($('#usertype').val()=='')
    {
      $('#usertype_error').css('display','block').html('Please select user type.');
      $('#usertype').focus();
      haserror++;
      return false;
    }
    else
    {
      $('#usertype_error').css('display','none').html('');
      
    }

    if($.trim($('#username').val())=='')
    {
      $('#username_error').css('display','block').html('User name is required.');
      $('#username').focus();
      haserror++;
      return false;
    }
    else
    {
      $('#username_error').css('display','none').html('');
      
    }

    if($.trim($('#password').val())=='')
    {
      $('#password_error').css('display','block').html('Please select user type.');
      $('#password').focus();
      haserror++;
      return false;
    }
    else
    {
      $('#password_error').css('display','none').html('');
      
    }
    if(haserror==0)
    {
      $('#admin-signin').submit();
    }
  }

  function getval(sel) 
    {      
      var str = sel.value;
      //alert(str);
      //$( "#hid_user" ).text( str );
      $( "#hid_user" ).val( str );
      //$( "div" ).text( str );
    }
  function email_validation()
  {
    var haserror=0;
    var usertype=$('#hid_user').val();  
    //alert(userid);  
        if($('#usertype_name').val()=='')
        {
          $('#usertype_name_error').css('display','block').html('Please select user type.');
          $('#usertype_name').focus();
          haserror++;
          return false;
        }
        else
        {
          $('#usertype_name_error').css('display','none').html('');
          
        }

    if($.trim($('#email').val())=='')
                {
                  $('#email_error').html('Email is required.');
                  $('#email').css('border-color','#A94442').focus();
                  haserror++;
                  return false;
                }else{
              var email=$('#email').val();
              var valid_email = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
                  if(!valid_email.test(email))
                  {
                    $('#email_error').html('Please enter valid Email Id.');
                    $('#email').css('border-color','#A94442').focus();
                    haserror++;
                    return false;
                  }             
               else{

                  var email=$('#email').val();
                    $.ajax({
                      url : '<?php echo base_url();?>admin/usertype/check_email_exist',
                      data : 'email='+email+'&usertype='+usertype,
                      success : function(res_email)
                      {
                        if(res_email=='False')
                        {
                          $('#email_error').html('This Email Does not exist.');
                          $('#email').css('border-color','#A94442').focus();
                          haserror++;
                          return false;
                        }
                        else{                         
                          $('#email_error').html('');
                          $('#email').css('border-color','');
                         /*  mail function start*/
                           $.ajax({
                            url : '<?php echo base_url();?>admin/usertype/send_mail',
                            data : 'email='+email+'&usertype='+usertype,
                            success : function(res_email)
                              {
                               $("#usertype_name").val("");
                               var back_url='<?php echo base_url()?>admin';
                               window.location=back_url;
                             }
                               
                            });
                          
                          /*  mail function end*/

                          }

                       }
                     });

                  } 
              }      
  }



</script>

    
