<script language="javascript">
function checkMail() {
	if(document.getElementById('new_email').value=='') {
		alert('New email id should not be blank...');	
		document.getElementById('new_email').focus();
		return false;
	}
	if(document.getElementById('new_email').value) {
		var x = document.getElementById('new_email').value;	
		var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;	
		if (filter.test(x)==false) {	
			alert("Enter valid email id.");	
			document.getElementById('new_email').focus();	
			return false;
		}
	}
}
</script>
<table width="100%" align="center" cellpadding="2" cellspacing="2" border="0">

	<tr>

		<td width="100%" align="left" valign="middle" style="padding:5pxfont-size:12px;font-face:Verdana,Times New Roman;"><strong> Administrator >> Change Email </strong></td>

	</tr>

	<tr>

		<td valign="top" bgcolor="#FFFFFF">

			<br />

			<br />

			<form name="form_change" action="<?php echo base_url();?>login/do_changeemail/" method="post" >
			<table width="60%" align="center" border="0" class="border" cellpadding="5" cellspacing="1" style="BORDER: #306AAA 1px solid;">

				<tr class="TDHEAD">

					<td colspan="3">Change Email</td>

				</tr>

				<tr>
			
					<td width="25%" align="canter" valign="middle" style="padding:10px;">&nbsp;</td>
					<td width="5%" align="canter" valign="middle" style="padding:10px;">&nbsp;</td>
					<td width="30%" align="canter" valign="middle" style="padding:10px;"><?php if(isset($succmsg)) { ?>
					  <div class="success"><?php echo $succmsg;?></div>
					  <?php } else if(isset($errmsg)) { ?>
					  <div class="error"><?php echo $errmsg; ?></div>
					  <?php }?></td>
				</tr>

				<tr>

					<td align="right" valign="top" class="tbllogin">Current Email</td>

					<td align="center" valign="top" class="tbllogin">:</td>

					<td align="left" valign="top"><?php echo $email;?></td>
				</tr>

				<tr>

					<td align="right" valign="top" class="tbllogin">New Email<font color="#FF0000">*</font></td>

					<td align="center" valign="top" class="tbllogin">:</td>

					<td align="left" valign="top"><input type="text" name="new_email" id="new_email" value="" class="inplogin" size="35" maxlength="100" />&nbsp;&nbsp;<?php echo form_error('new_email'); ?></td>

				</tr>

				<tr>

					<td>&nbsp;</td>

					<td>&nbsp;</td>

					<td><input type="submit" value="Change" class="inplogin" onclick="return checkMail();"/>&nbsp;&nbsp;<input type="button" name="btn" value="Cancel" onClick="javascript:window.location='<?php echo base_url();?>login/index';" class="inplogin" /></td>

				</tr>

			</table>

			</form>

		</td>

	</tr>

</table>