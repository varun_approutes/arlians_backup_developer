<?php $this->load->view('admin/header'); ?>

<div class="row-fluid">
	<div class="span12">
		<ul class="breadcrumb-beauty">
			<li>
				<a href="<?php echo base_url();?>login"><span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span> Dashboard</a>
			</li>
			<li>	
				<a href="<?php echo base_url();?>admin/changepass">Change Password</a>
			</li>
		</ul>
	</div>
</div>
<br>

<div class="row-fluid">
	<div class="span6">
		<div class="widget">
			<div class="widget-header">
				<div class="title">
					<span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> Change Password
				</div>
			</div>
			<div class="widget-body">
				<form id="frmValidation" name="frm" action="<?php echo base_url();?>admin/login/do_changepass" method="post" class="form-horizontal no-margin">
					<div class="control-group">
						<label class="control-label">
							* Old Password
						</label>
						<div class="controls controls-row">
							<input name="old_password" type="password" value="" class="validate[required,minSize[5]]">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">
							* New Password
						</label>
						<div class="controls controls-row">
							<input id="new_password" name="new_password" type="password" value="" class="validate[required,minSize[5]]">
						</div>
					</div>
					<div class="control-group">
						<label class="control-label">
							* Confirm New Password
						</label>
						<div class="controls controls-row">
							<input name="confirm_password" type="password" value="" class="validate[required,minSize[5]]">
						</div>
					</div>
					<div class="form-actions no-margin">
						<input type="submit" name="submit" value="Update" class="btn btn-info">
						<button class="btn" type="button" onClick="javascript:window.location='<?php echo base_url();?>admin/login';" >
							Cancel
						</button>
					</div>
					<div class="clearfix"></div>
				</form>
			</div>
		</div>
	</div>
          </div>

<?php $this->load->view('admin/footer'); ?>