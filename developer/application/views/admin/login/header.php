<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <!--[if lt IE 7]>
    <html class="lt-ie9 lt-ie8 lt-ie7" lang="en">
  <![endif]-->

  <!--[if IE 7]>
    <html class="lt-ie9 lt-ie8" lang="en">
  <![endif]-->

  <!--[if IE 8]>
    <html class="lt-ie9" lang="en">
  <![endif]-->

  <!--[if gt IE 8]>
    <!-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" dir="ltr" >
    <!--
  <![endif]-->

	<head>
		<meta charset="utf-8">
		<title>Admin Panel</title>
		<!--<link rel="icon" type="image/png" href="#" data-icon="&#xe0a1;"/>-->
		<meta name="author" content="Pradipto jana">
		<meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
		<meta name="description" content="Admin Panel">
		<meta name="keywords" content="Admin Panel">
		
		<?php
		error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);
		$sql = "SELECT `theme_name` FROM admin WHERE id = '1' ";
		$query = $this->db->query($sql);
		
		if($query->num_rows()>0)
		{
			$rs_theme = $query->result_array();
		}

		if($rs_theme[0]['theme_name'] == 'dark' || $rs_theme[0]['theme_name'] == 'colorful')
		{
			$theme = trim($rs_theme[0]['theme_name']);
		}
		else
		{
			$theme = 'light';
		}
		?>
		<script src="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/js/jquery-1.8.2.min.js"></script>
		<script src="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/js/html5-trunk.js"></script>
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/icomoon/style.css" rel="stylesheet">
		<!--[if lte IE 7]>
		<script src="<?php echo $this->config->item('css_images_js_base_url'); ?>css/icomoon-font/lte-ie7.js"></script>
		<![endif]-->
		
		<!-- bootstrap css -->
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/css/<?php echo $theme; ?>/main.css" rel="stylesheet">
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/css/common/common.css" rel="stylesheet">
		<!--<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>css/<?php echo $theme; ?>/fullcalendar.css" rel="stylesheet">

		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>css/<?php echo $theme; ?>/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet">
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>css/<?php echo $theme; ?>/wysiwyg/wysiwyg-color.css" rel="stylesheet">-->
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/css/<?php echo $theme; ?>/timepicker.css" rel="stylesheet">
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/css/<?php echo $theme; ?>/bootstrap-editable.css" rel="stylesheet">
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/css/<?php echo $theme; ?>/select2.css" rel="stylesheet">
		<!-- rating js & css -->
		<script src="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/js/rating_bar.js"></script>
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/css/rating/rating_bar.css" rel="stylesheet">
		<!-- Validation css -->
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/css/common/validationEngine.jquery.css" rel="stylesheet">

		<!-- Datepicker css -->
		<link href="<?php echo $this->config->item('css_images_js_base_url'); ?>admin/public/css/common/jquery-ui-1.10.3.custom.css" rel="stylesheet">

		<!--<script type="text/javascript">
		
		var _gaq = _gaq || [];
		_gaq.push(['_setAccount', 'UA-40571798-1']);
		_gaq.push(['_trackPageview']);
		
		(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
		})();
		
		</script>-->
	</head>

	<body>
		<header>
			<?php 
			//ini_set('display_errors' 0);
			$first = $this->uri->segment(2);
			?>
			<div id="mini-nav" >
				<ul class="hidden-phone" >
					<li><a href="<?php echo base_url();?>admin/login/changepass">Change Password</a></li>
					<li>
						<a href="<?php echo base_url();?>admin/logout"><?php echo 'Logout'; ?></a>
					</li>

				</ul>
			</div>
		</header>
		<div class="container-fluid">
		<div class="hidden-phone hidden-tablet custom-leftpanel" style="top: 70px;">
			<div id="scrollbar-three" style="width: 86px;">
				<div class="scrollbar">
					<div class="trac k">
					<div class="thumb">
						<div class="end"></div>
					</div>
					</div>
				</div>
				<div class="viewport">
					<div class="overview">
						<div class="mainnav_custom">
							<ul>
								<li <?php if($first == 'dashboard') { ?> class="active" <?php } ?>>
									<?php if($first == 'dashboard') { ?> <span class="current-arrow"></span><?php } ?>
									<a href="<?php echo base_url();?>admin/dashboard" style="font-size: 14px;">
										<div class="icon">
											<span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>
										</div>
										Dashboard
									</a>
								</li>
                                
								 <li <?php if($first == 'registration') { ?> class="active" <?php } ?>>
									<?php if($first == 'registration') { ?> <span class="current-arrow"></span><?php } ?>
									<a href="<?php echo base_url();?>admin/registration" style="font-size: 14px;">
										<div class="icon">
											<span class="fs1" aria-hidden="true" data-icon="&#xe071;"></span>
										</div>
										User
									</a>
								</li>
								
							    <li <?php if($first == 'banner') { ?> class="active" <?php } ?>>
									<?php if($first == 'banner') { ?> <span class="current-arrow"></span><?php } ?>
									<a href="<?php echo base_url();?>admin/banner" style="font-size: 14px;">
										<div class="icon">
											<span class="fs1" aria-hidden="true" data-icon="&#xe13a;"></span>
										</div>
										Banner
									</a>
								</li>

								 <li <?php if($first == 'event') { ?> class="active" <?php } ?>>
									<?php if($first == 'event') { ?> <span class="current-arrow"></span><?php } ?>
									<a href="<?php echo base_url();?>admin/event" style="font-size: 14px;">
										<div class="icon">
											<span class="fs1" aria-hidden="true" data-icon="&#xe0ca;"></span>
										</div>
										Event
									</a>
								</li>

                                
								<li <?php if($first == 'cms') { ?> class="active" <?php } ?>>
									<?php if($first == 'cms') { ?> <span class="current-arrow"></span><?php } ?>
									<a href="<?php echo base_url();?>admin/cms" style="font-size: 14px;">
										<div class="icon">
											<span class="fs1" aria-hidden="true" data-icon="&#xe08b;"></span>
										</div>
										CMS
									</a>
								</li>
                               
								<?php /*?><li <?php if($first == 'contactus_settings') { ?> class="active" <?php } ?>>
									<?php if($first == 'contactus_settings') { ?> <span class="current-arrow"></span><?php } ?>
									<a href="<?php echo base_url();?>admin/contactus_settings">
										<div class="icon">
											<span class="fs1" aria-hidden="true" data-icon="&#xe090;"></span>
										</div>
										Contact us Settings 
									</a>
								</li><?php */?>
								
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="dashboard-wrapper">
				<div class="main-container">
					<div class="navbar hidden-desktop">
						<div class="navbar-inner">
							<div class="container">
								<a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</a>
							</div>
						</div>
					</div>

					<?php
					if(isset($error_msg))
					{
						if($error_msg != '')
						{
						?>
							<div class="alert alert-block alert-error fade in">
								<button data-dismiss="alert" class="close" type="button">
								×
								</button>
								<h4 class="alert-heading" data-icon="&#xe1cb;">
									Error!
								</h4>
								<p>
									<?php if(function_exists('validation_errors') AND validation_errors() != '')echo validation_errors(); ?>
									<?php echo $error_msg; ?>
								</p>
							</div>
						<?php
						}
					}
					if(isset($success_msg))
					{
						if($success_msg != '')
						{
						?>
							<div class="alert alert-block alert-success fade in">
								<button data-dismiss="alert" class="close" type="button">
								×
								</button>
								<h4 class="alert-heading" data-icon="&#xe0fe;">
									Success!
								</h4>
								<p>
									<?php echo $success_msg; ?>
								</p>
							</div>
						<?php
						}
					}
					?>
					<div class="alert alert-block alert-success fade in" id="alert-success" style="display:none;">
						<button class="close" type="button" onclick="javascript:closing();">
						×
						</button>
						<h4 class="alert-heading" data-icon="&#xe0fe;">
							Success!
						</h4>
						<p id="message_action">&nbsp;</p>
					</div>
<script type="text/javascript">
$( document ).ready(function() {
$('.scrollbar').attr('style','display:none');
$('.custom-leftpanel').mouseover(function() {
	$('.scrollbar').removeAttr('style');	
});
$('.custom-leftpanel').mouseleave(function() {
	$('.scrollbar').attr('style','display:none');
});
});
function notificationchange()
{
  
  $.post('<?php echo base_url(); ?>admin/globalajax/notificationchnagefunc',function(data){	
						
					$("#messagesCountDown").html('');

					});
}
</script>