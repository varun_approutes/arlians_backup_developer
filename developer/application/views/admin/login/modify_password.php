<?php $this->load->view('admin/login_header'); ?>
<script type="text/javascript">
$(document).ready(function(){
	var topheight = parseInt(parseInt($(window).height()) - parseInt($('.signin').height()) - parseInt($('.logo').height())) / 2;
	topheight = (topheight > 50) ? 50 : (topheight < 0 ? 0 : topheight) ;
	$('.signin').css('margin-top', topheight+'px');
});
</script>
<div class="row-fluid">

	<div class="span4 offset4">

		<div class="signin">

			<?php /*?><a href="<?php echo base_url();?>">

				<img src="<?php echo $this->config->item('css_images_js_base_url'); ?>images/logo.png" alt="Logo" style="padding-left:75px;">

			</a><?php */?>

			<!--<h1 class="center-align-text">Login</h1>-->
			<img class="logo" src="<?php echo base_url()?>uploads/logo/thumbs/<?php echo LOGO;?>" alt="Mortgage Needs" style="max-width:100%" />
			<div class="row-fluid">

				<div class="span12">

					<div class="widget">

						<div class="widget-body">

							<?php
							if($this->session->userdata('error_msg') != '')
							{

								?>

									<div class="alert alert-block alert-error fade in">

										<button data-dismiss="alert" class="close" type="button">

										�

										</button>

										<h4 class="alert-heading" data-icon="&#xe1cb;">

											Error!

										</h4>

										<p>
										<?php
										echo $this->session->userdata('error_msg');
										$this->session->unset_userdata('error_msg');
										?>
										</p>

									</div>

								<?php

								}

							if($this->session->userdata('success_msg') != '')
							{

								?>

									<div class="alert alert-block alert-success fade in">

										<button data-dismiss="alert" class="close" type="button">

										�

										</button>

										<h4 class="alert-heading" data-icon="&#xe0fe;">

											Success!

										</h4>

										<p>
										<?php
										echo $this->session->userdata('success_msg');
										$this->session->unset_userdata('success_msg');
										?>
										</p>

									</div>

								<?php

								}

							?>

						</div>

					</div>

				</div>

			</div>

			<form  id="frm_login" name="frm_login" class="signin-wrapper" action="<?php echo base_url();?>admin/login/update_password" method="post">
			<input type="hidden" name="admin_id" id="admin_id" value="<?php echo $this->uri->segment(4);?>">
				<div class="content">

					<input type="password" id="password" name="password" class="input input-block-level" placeholder="New Password">


				</div>
				<div id="password_error" style="margin-left:10px;color:#CC0000;width:100%;text-align:left;"></div>
				<div class="content">

					<input type="password" id="confirm_password" name="confirm_password" class="input input-block-level" placeholder="Confirm Password">


				</div>
				<div id="confirm_password_error" style="margin-left:10px;color:#CC0000;width:100%;text-align:left;"></div>
				<div class="actions">

					<input class="btn btn-info pull-right" type="button" value="Submit" onClick="form_validation();">

					<div class="clearfix"></div>

				</div>

			</form>

		</div>

	</div>

</div>
<script>
function form_validation()
{
var has_error=0;
if($.trim($('#password').val())=='')
{
	$('#password_error').html('Password is required');
	$('#password').focus();
	has_error=1;
	return false;
}
if($('#password').val()!='')
{
	$('#password_error').html('');
	if($.trim($('#confirm_password').val())=='')
	{
		$('#confirm_password_error').html('Confirm password is required');
		$('#confirm_password').focus();
		has_error=1;
		return false;
	}
	else if($('#password').val() != $('#confirm_password').val())
	{
		$('#confirm_password_error').html('Password and confirm password should be same');
		$('#confirm_password').focus();
		has_error=1;
		return false;
	}
	else
	{
		$('#confirm_password_error').html('');
		has_error=0;
	}
}
//alert(has_error);
if(has_error==0)
{
	$('#frm_login').submit();
}
}
</script>
<?php $this->load->view('admin/login_footer'); ?>