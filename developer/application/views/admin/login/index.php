<?php $this->load->view('admin/login_header'); ?>
<script type="text/javascript">
$(document).ready(function(){
	var topheight = parseInt(parseInt($(window).height()) - parseInt($('.signin').height()) - parseInt($('.logo').height())) / 2;
	topheight = (topheight > 50) ? 50 : (topheight < 0 ? 0 : topheight) ;
	$('.signin').css('margin-top', topheight+'px');
});
</script>
<div class="row-fluid">

	<div class="span4 offset4">

		<div class="signin">

			<?php /*?><a href="<?php echo base_url();?>">

				<img src="<?php echo $this->config->item('css_images_js_base_url'); ?>images/logo.png" alt="Logo" style="padding-left:75px;">

			</a><?php */?>

			<!--<h1 class="center-align-text">Login</h1>-->
			<img class="logo" src="<?php echo base_url()?>uploads/logo/thumbs/<?php echo LOGO;?>" alt="Mortgage Needs" style="max-width:100%" />
			<div class="row-fluid">

				<div class="span12">

					<div class="widget">

						<div class="widget-body">

							<?php

							if(isset($error_msg))

							{

								if($error_msg != '')

								{

								?>

									<div class="alert alert-block alert-error fade in">

										<button data-dismiss="alert" class="close" type="button">

										×

										</button>

										<h4 class="alert-heading" data-icon="&#xe1cb;">

											Error!

										</h4>

										<p>

											<?php echo validation_errors(); ?>

											<?php echo $error_msg; ?>

										</p>

									</div>

								<?php

								}

							}

							if(isset($success_msg))

							{

								if($success_msg != '')

								{

								?>

									<div class="alert alert-block alert-success fade in">

										<button data-dismiss="alert" class="close" type="button">

										×

										</button>

										<h4 class="alert-heading" data-icon="&#xe0fe;">

											Success!

										</h4>

										<p>

											<?php echo $success_msg; ?>

										</p>

									</div>

								<?php

								}

							}

							?>

						</div>

					</div>

				</div>

			</div>

			<form  id="frmValidation" name="frm_login" class="signin-wrapper" action="<?php echo base_url();?>admin/login/do_login/" method="post">

				<div class="content">

					<input type="text" id="username" name="username" class="input input-block-level validate[required]" placeholder="Username">

					<input type="password" id="password" name="password" class="input input-block-level validate[required]" placeholder="Password">

				</div>
				<!--<a href="<?php //echo base_url().'admin/login/forgotpassword';?>" style="float:left;">Forgot Password</a> -->
				<div class="actions">

					<input class="btn btn-info pull-right" type="submit" value="Login">

					<div class="clearfix"></div>

				</div>

			</form>

		</div>

	</div>

</div>

<?php $this->load->view('admin/login_footer'); ?>