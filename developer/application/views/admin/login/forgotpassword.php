<?php $this->load->view('admin/login_header'); ?>
<script type="text/javascript">
$(document).ready(function(){
	var topheight = parseInt(parseInt($(window).height()) - parseInt($('.signin').height()) - parseInt($('.logo').height())) / 2;
	topheight = (topheight > 50) ? 50 : (topheight < 0 ? 0 : topheight) ;
	$('.signin').css('margin-top', topheight+'px');
});
</script>
<div class="row-fluid">

	<div class="span4 offset4">

		<div class="signin">

			<?php /*?><a href="<?php echo base_url();?>">

				<img src="<?php echo $this->config->item('css_images_js_base_url'); ?>images/logo.png" alt="Logo" style="padding-left:75px;">

			</a><?php */?>

			<!--<h1 class="center-align-text">Login</h1>-->
			<img class="logo" src="<?php echo base_url()?>uploads/logo/thumbs/<?php echo LOGO;?>" alt="Mortgage Needs" style="max-width:100%" />
			<div class="row-fluid">

				<div class="span12">

					<div class="widget">

						<div class="widget-body">

							<?php
							if($this->session->userdata('error_msg') != '')
							{

								?>

									<div class="alert alert-block alert-error fade in">

										<button data-dismiss="alert" class="close" type="button">

										�

										</button>

										<h4 class="alert-heading" data-icon="&#xe1cb;">

											Error!

										</h4>

										<p>
										<?php
										echo $this->session->userdata('error_msg');
										$this->session->unset_userdata('error_msg');
										?>
										</p>

									</div>

								<?php

								}

							if($this->session->userdata('success_msg') != '')
							{

								?>

									<div class="alert alert-block alert-success fade in">

										<button data-dismiss="alert" class="close" type="button">

										�

										</button>

										<h4 class="alert-heading" data-icon="&#xe0fe;">

											Success!

										</h4>

										<p>
										<?php
										echo $this->session->userdata('success_msg');
										$this->session->unset_userdata('success_msg');
										?>
										</p>

									</div>

								<?php

								}

							?>

						</div>

					</div>

				</div>

			</div>

			<form  id="frm_login" name="frm_login" class="signin-wrapper" action="<?php echo base_url();?>admin/login/forgot_email_check" method="post">

				<div class="content">

					<input type="text" id="email" name="email" class="input input-block-level" placeholder="Email">


				</div>
				<div id="email_error" style="margin-left:10px;color:#CC0000;width:100%;text-align:left;"></div>
				<div class="actions">

					<input class="btn btn-info pull-right" type="button" value="Submit" onClick="form_validation();">

					<div class="clearfix"></div>

				</div>

			</form>

		</div>

	</div>

</div>
<script>
function form_validation()
{
var has_error=0;
var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
if($.trim($('#email').val())=='')
{
	$('#email_error').html('Email address is required');
	$('#email').focus();
	has_error=1;
	return false;
}
if($('#email').val()!='')
{
	var email_val=$('#email').val();
	if(pattern.test(email_val))
	{
		$('#email_error').html('');
		
	}
	else
	{
		$('#email_error').html('Please enter valid email');
		$('#email').focus();
		has_error=1;
		return false;
	}
}
if(has_error==0)
{
	$('#frm_login').submit();
}
}
</script>
<?php $this->load->view('admin/login_footer'); ?>