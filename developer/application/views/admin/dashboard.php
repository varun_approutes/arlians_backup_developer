<?php

$this->load->view('admin/header');

?>
<!--mini statistics start--> 
<div class="row">    
    <!--<div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">Dashbaord</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/dashboard');?>"><span class="widget-h fa fa-dashboard fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div>-->
    <?php
    if($this->session->userdata('admin_user_type')=='Super_Admin')
    {
    ?>
    <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">Site Settings</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/site_settings');?>"><span class="widget-h fa fa-cog fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div> 

   <!-- <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">School / College</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/school');?>"><span class="widget-h fa fa-building-o fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div> -->

     <!-- <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">Admin Management</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/admin_management');?>"><span class="widget-h fa fa-building-o fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div> -->

    <?php
    }
    ?>
    <!--<div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">Principal</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/principal');?>"><span class="widget-h fa fa-user fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div>-->

   <!-- <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">Teacher</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/teacher');?>"><span class="widget-h fa fa-user fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div>-->

    <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">Member</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/member');?>"><span class="widget-h fa fa-user fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div>
<?php if($this->session->userdata('admin_user_type')=='Super_Admin'){?>
    <!--<div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">Parent</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/student_parent');?>"><span class="widget-h fa fa-user fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div>-->
<?php }?>

   
    

   <!-- <div class="col-md-3">
        <section class="panel">
            <div class="panel-body">
                <div class="top-stats-panel">
                        <h4 class="widget-h">Notice</h4>

                        <div class="dashboard-div"><a href="<?php echo site_url('admin/notice');?>"><span class="widget-h fa fa-file-text fa-4x" style="font-size:10em;margin-left:40%;"></span></a></div>
                </div>
            </div>
        </section>
    </div>


    </div>
</div>-->
<!--mini statistics start-->
<!-- <div class="row">
    <div class="col-md-3">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon orange"><i class="fa fa-gavel"></i></span>
            <div class="mini-stat-info">
                <span>320</span>
                New Order Received
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon tar"><i class="fa fa-tag"></i></span>
            <div class="mini-stat-info">
                <span>22,450</span>
                Copy Sold Today
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon pink"><i class="fa fa-money"></i></span>
            <div class="mini-stat-info">
                <span>34,320</span>
                Dollar Profit Today
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="mini-stat clearfix">
            <span class="mini-stat-icon green"><i class="fa fa-eye"></i></span>
            <div class="mini-stat-info">
                <span>32720</span>
                Unique Visitors
            </div>
        </div>
    </div>
</div> -->
<!--mini statistics end-->
<div class="row">


</div>
</section>
</section>
<!--main content end-->

</section>
<?php
$this->load->view('admin/footer');
//var_dump($this->session->userdata('admin_user_type'));
die();
?>