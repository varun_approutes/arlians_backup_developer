<?php

$this->load->view('admin/header');
//echo count($result);
?>
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/admin/js/bootstrap-fileupload/bootstrap-fileupload.css" />
<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="widfth:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Site Settings
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/site_settings/update_settings')?>" id="site_settings_form" name="site_settings_form" enctype="multipart/form-data">
               			<input type="hidden" name="existing_image" id="existing_image" value="<?php if((count($result)>0) && (!empty($result['logo']))){echo $result['logo'];}?>" />
               		<!--Site Name Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">Site Name</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="site_name" name="site_settings[site_name]" placeholder="Site Name" value="<?php if((count($result)>0) && (!empty($result['site_name']))){echo $result['site_name'];}?>">
	                            <p style="color:#A94442" id="site_name_error"></p>
	                        </div>
	                    </div>
	                <!--Site URL Section-->
	                    <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Site URL</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="site_url" name="site_settings[site_url]" placeholder="Site Url" value="<?php if((count($result)>0) && (!empty($result['site_url']))){echo $result['site_url'];}?>">
	                            <p style="color:#A94442" id="site_url_error"></p>
	                        </div>
	                    </div>
	                <!--Admin Email Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Admin Email</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="admin_email" name="site_settings[admin_email]" placeholder="Admin Email" value="<?php if((count($result)>0) && (!empty($result['admin_email']))){echo $result['admin_email'];}?>">
	                            <p style="color:#A94442" id="admin_email_error"></p>
	                        </div>
	                    </div>
	                <!--Phone Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Phone</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="phone" name="site_settings[phone]" placeholder="Phone" value="<?php if((count($result)>0) && (!empty($result['phone']))){echo $result['phone'];}?>">
	                            <p style="color:#A94442" id="phone_error"></p>
	                        </div>
	                    </div>
	                <!-- Exisitng Image -->
	                <?php
	               	if((!empty($result['logo']) && ($result['logo']!='') && (count($result)>0)))
	               	{
	               		$file_exist='uploads/logo/thumbs/'.$result['logo'];
						
						$base_image=base_url().'uploads/logo/thumbs/'.$result['logo'];
							//echo $base_image;
						if(file_exists($file_exist))
						{
	               	?>
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Existing Image</label>
	                        <div class="col-lg-7">
	                            <img src="<?php echo $base_image;?>">
	                        </div>
	                    </div>
	                <?php
	            		}
	            	}
	                ?>
	                <!--Image Upload Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Site Image</label>
	                        <div class="col-lg-7">
	                            <input type="file" name="logo" id="logo">
	                            <p style="color:#A94442" id="logo_error"></p>
	                        </div>
	                    </div>
	                <!--Facebook Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Facebook</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="facebook" name="site_settings[facebook]" placeholder="Facebook" value="<?php if((count($result)>0) && (!empty($result['facebook']))){echo $result['facebook'];}?>">
	                            <p style="color:#A94442" id="facebook_error"></p>
	                        </div>
	                    </div>
	                <!--Twitter Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Twitter</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="twitter" name="site_settings[twitter]" placeholder="Twitter" value="<?php if((count($result)>0) && (!empty($result['twitter']))){echo $result['twitter'];}?>">
	                            <p style="color:#A94442" id="twitter_error"></p>
	                        </div>
	                    </div>

	                

	                    <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-7">
	                        	<input type="button" class="btn btn-danger" id="hit" onclick="form_validation();" value="Update Settings">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                    </div>
                	</form>
                </div>
            </div>
        </section>

    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>
<script type="text/javascript" src="<?php echo base_url();?>asets/admin/js/bootstrap-fileupload/bootstrap-fileupload.js"></script>
<script type="text/javascript">

	function form_validation()
	{
		var has_error=0;
		if($.trim($('#site_name').val())=='')
		{
			$('#site_name_error').html('Site Name is required.');
			$('#site_name').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#site_name_error').html('');
			$('#site_name').css('border-color','');
		}
		if($.trim($('#site_url').val())=='')
		{
			$('#site_url_error').html('Site URL is required.');
			$('#site_url').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else if($('#site_url').val()!='')
		{
			var re = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
			if(!re.test($('#site_url').val()))
			{
				$('#site_url_error').html('Please enter proper url');
				$('#site_url').css('border-color','#A94442').focus();
				has_error++;
				return false;
			}
			else
			{
				$('#site_url_error').html('');
				$('#site_url').css('border-color','');
			}
		}
		else
		{
			$('#site_url_error').html('');
			$('#site_url').css('border-color','');
		}
		if($.trim($('#admin_email').val())=='')
		{
			$('#admin_email_error').html('Admin email is required.');
			$('#admin_email').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else if($('#admin_email').val()!='')
		{
			var re = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
			if(!re.test($('#admin_email').val()))
			{
				$('#admin_email_error').html('Please enter valid email');
				$('#admin_email').css('border-color','#A94442').focus();
				return false;
			}
			else
			{
				$('#admin_email_error').html('');
				$('#admin_email').css('border-color','');
			}
		}
		else
		{
			$('#admin_email_error').html('');
			$('#admin_email').css('border-color','');
		}
		if($.trim($('#phone').val())=='')
		{
			$('#phone_error').html('Phone is required.');
			$('#phone').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#phone_error').html('');
			$('#phone').css('border-color','');
		}

		if($('#existing_image').val()=='')
		{
			if($('#logo').val()=='')
			{
				$('#logo_error').html('Logo is required');
				$('#logo').focus();
				has_error++;
				return false;
			}
			else
			{
				$('#logo_error').html('');
			}
		}
		else
		{
			$('#logo_error').html('');
		}

		if($.trim($('#facebook').val())=='')
		{
			$('#facebook_error').html('Facebook link is required.');
			$('#facebook').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else if($('#facebook').val()!='')
		{
			var re = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
			if(!re.test($('#facebook').val()))
			{
				$('#facebook_error').html('Please enter proper url');
				$('#facebook').css('border-color','#A94442').focus();
				return false;
			}
			else
			{
				$('#facebook_error').html('');
				$('#facebook').css('border-color','');
			}
		}
		else
		{
			$('#facebook_error').html('');
			$('#facebook').css('border-color','');
		}
		if($.trim($('#twitter').val())=='')
		{
			$('#twitter_error').html('Twitter link is required.');
			$('#twitter').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else if($('#twitter').val()!='')
		{
			var re = /(http(s)?:\\)?([\w-]+\.)+[\w-]+[.com|.in|.org]+(\[\?%&=]*)?/;
			if(!re.test($('#twitter').val()))
			{
				$('#twitter_error').html('Please enter proper url');
				$('#twitter').css('border-color','#A94442').focus();
				return false;
			}
			else
			{
				$('#twitter_error').html('');
				$('#twitter').css('border-color','');
			}
		}
		else
		{
			$('#twitter_error').html('');
			$('#twitter').css('border-color','');
		}
		if(has_error==0)
		{
			$('#site_settings_form').submit();
		}
	}
</script>