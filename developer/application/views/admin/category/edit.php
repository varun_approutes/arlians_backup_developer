<?php
$this->load->view('admin/header');
//var_dump($rows);
//die();
?>

<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="width:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Edit Sector
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/category/update')?>" id="student_form" name="student_form" enctype="multipart/form-data">
               		<input type="hidden" name="exist_image" id="exist_image" value="<?php echo $rows[0]->profile_image;?>">	
               		<input type="hidden" name="toplineid" id="toplineid" value="<?php echo $rows[0]->top_code;?>">	
               		
               		<!--School Id Section-->
               		
               		

	                   <!--Student's Name Section-->
	                   <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Topline  Name</label>
   		                    <div class="col-lg-7">
   		                            <input type="text" class="form-control" id="name" name="name" placeholder="Student Full Name" value="<?php echo $rows[0]->top_name ;?>">
   		                            <p style="color:#A94442" id="name_error"></p>
   		                    </div>
	                   </div>
	                 					
	                <!-- Student Existing Image Section -->
	                <?php
	                if($rows[0]->profile_image!='')
	                {
	                	//$image_path='/uploads/profile_image/'.$rows[0]->profile_image;
	                	//if(file_exists($image_path))
	                	//{
	                		$get_image=base_url().'uploads/profile_image/'.$rows[0]->profile_image;
	                ?>
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Exisitng Image</label>
	                        <div class="col-lg-7">
	                            <img src="<?php echo $get_image?>">
	                        </div>
	                    </div>
	                    <?php
	                	//}
	                    ?>
	                <?php
	            	}
	                ?>
	                <!--Image Upload Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Image</label>
	                        <div class="col-lg-7">
	                            <input type="file" name="image" id="image">
	                            <p style="color:#A94442" id="image_error"></p>
	                        </div>
	                    </div>
	                    
	                    <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-2">
	                        	<input type="button" class="btn btn-danger" id="hit" onclick="form_validation();" value="Update">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                        <div class="col-lg-6">
	                        	<input type="button" value="Cancel" onclick="return_back();" class="btn" style="background:#c6c6c6;color:#ffffff">
	                        	<span style="font-style: italic; margin-left: 10px;">* Field are mandatory</span>
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                    </div>
                	</form>
                </div>
            </div>
        </section>

    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>

<script>
	
	function return_back()
	{
		var back_url='<?php echo base_url()?>admin/student';
		window.location=back_url;
	}
	
	function show_address(id)
	{
		//alert(id);
		var location_id=id;
		var autocomplete = new google.maps.places.Autocomplete($("#"+location_id)[0], {});

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			console.log(place.address_components);
		});
	}
</script>
<script type="text/javascript">

	function form_validation()
	{
		var has_error=0;
		var userid=$('#userid').val();
		
		
		if($.trim($('#name').val())=='')
		{
			$('#name_error').html('Student name is required.');
			$('#name').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#name_error').html('');
			$('#name').css('border-color','');
		}
		if($('#image').val()!='')
			{
				var extension = $('#image').val().split('.').pop().toUpperCase();
					if (extension!="PNG" && extension!="JPG" && extension!="GIF" && extension!="JPEG")
						{
							$('#image_error').html('File format is not supported');
								$('#image').focus();
								has_error++;
								return false;
							}
							else
							{
								$('#image_error').html('');
							}
		}
			else
				{
					$('#image_error').html('');
				}
				if(has_error==0)
				{
					$('#student_form').submit();
				}
		
	}
</script>