<?php
$this->load->view('admin/header');
?>

<div class="row">
	<div class="col-lg-12">		
	        <section class="panel">
            <header class="panel-heading">
                Conceptinside Label Search
            </header>
 			<div class="panel-body">
                <div class="position-center">
					<form class="form-horizontal" role="form" name="search_frm"  action="<?php echo base_url(); ?>admin/category/fetchString" method="post">
	
					<div class="form-group">
		                <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Top Lines</label>
		                <div class="col-lg-7">
		                    <select name="level1" id="level1" class="form-control">
								<option value=''> -- Select -- </option>
								<?php

								$sql = "SELECT `top_name` FROM `ar_top_lines`";
								$res = $this->db->query($sql);

								foreach($res->result() as $row){
									//echo "<pre>";print_r($row);echo "</pre>";exit();
									?>											
										<option value='<?php echo $row->top_name;?>'><?php echo $row->top_name;?></option>
									<?php
								}?>
							</select>
		                </div>
		            </div>

					<div class="form-group">
		                <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Sector</label>
		                <div class="col-lg-7">
		                    <select name="level2" id="level2" class="form-control">
								<option value=''> -- Select -- </option>						
							</select>
		                </div>
		            </div>

		            <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-7">
	                        	<input type="submit" value="Submit" class="btn btn-danger">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                </div>

					<div class="adv-table" id="result">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>
                                <th>SL No.</th>
                                <th>Top Line Name</th>
                                <th>Top Line Image</th>                                
                            </tr>
                        </thead>
                        <?php						
                        if(count($rows)>0){?>
                        <tbody>
                            <?php
                            foreach ($rows as $data){?>
	                            <tr class="gradeX">
	                            	<td><?php echo $data['top_code'];?></td>
	                                <td><?php echo $data['top_code'];?></td>
	                                <td><?php echo $data['top_name'];?></td>                                 
	                            </tr><?php
                            }?>
                        </tbody>
                        <?php
                        }?>
                        
                    </table>
                	</div>
										
					</form>
				</div>
			</div>
       </section>
    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>

<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script>

function get_value(){
	var level1 = $("#level1").val();
    
    $.ajax({
		  type: "POST",
		  url: '<?php echo base_url();?>admin/category/getLevel2AJax',
		  data: {level1:level1},
		  success: function(result){
	        $("#level2").html(result);

	        var level2 = '<?php if(!empty($level2)){echo $level2;}?>';
    
			if(level2!=''){
				$('#level2').val(level2);		
			};
		}
	}); 	

}

$("#level1").change(function(){
	get_value();
});

$( document ).ready(function() {

	var level1 = '<?php if(!empty($level1)){echo $level1;}?>';
	var level2 = '<?php if(!empty($level2)){echo $level2;}?>';

	if(level1!='' || level2!=''){
		$('#result').show();
		$('#level1').val(level1);		
		get_value();				
	
	}else{
		$('#level1').val('');
		$('#level2').val('');
		$('#result').hide();
		
	}	
});

</script>