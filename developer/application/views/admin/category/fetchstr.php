<?php
$this->load->view('admin/header');
?>

<div class="row">
	<div class="col-lg-12">		
	        <section class="panel">
            <header class="panel-heading">
                Conceptinside Label Search
            </header>
 			<div class="panel-body">
                <div class="position-left">
					<form class="form-horizontal" role="form" name="search_frm"  action="<?php echo base_url(); ?>admin/category/addCorpusKey" method="post">
	
					<div class="form-group">
		                <label for="inputPassword1" class="col-lg-2 col-sm-1 control-label">Sub Sector</label>
		                <div class="col-lg-7">						
		                	<?php

								$sql = "SELECT `subsector_name` FROM `ar_subsector`";
								$res = $this->db->query($sql);
							?>	
		                    <select name="subsector" id="subsector" class="form-control">
								<option value=''> -- Select -- </option>
								<?php
								foreach($res->result() as $row){
										$name = stripslashes($row->subsector_name);
									?>																	
										<option value='<?php echo $name;?>'><?php echo $name;?></option>
									<?php
								}?>
							</select>
		                </div>
		            </div>

		            <div class="form-group">
	                        <div class="col-lg-offset-2 col-lg-7">
	                        	<input type="submit" value="Submit" class="btn btn-danger">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                </div>

					<div class="adv-table" id="result">
                    <table  class="display table table-bordered table-striped" id="dynamic-table">
                        <thead>
                            <tr>
                                <th style="width:5%">SL</th>
                                <th style="width:20%">Subsector</th>
                                <th style="width:35%">Lebel</th>                                
                                <th style="width:35%">Encoded Keyword</th>
                                <th style="width:5%">Action</th>                                
                            </tr>
                        </thead>
                        <?php						

						$sql = "SELECT * FROM `relevant_keys`";
						$res = $this->db->query($sql);					

                        if($res->num_rows()>0){?>
                        <tbody>
                            <?php
                            $i=0;
                        	foreach($res->result() as $row){						

                        	$subsector 			= stripslashes($row->subsector);
                        	$encoded_keyword	= stripslashes($row->encoded_keyword);
                        	$label  			= stripslashes($row->label);					

					       	$i++;
                        	?>
                            <tr class="gradeX">
                            	<td><?php echo $i;?></td>
                                <td><?php echo $subsector;?></td>
                                <td><?php echo $label;?></td>    
                                <td><?php echo $encoded_keyword;?></td>                             
                                <td><a href= "<?php echo base_url();?>admin/category/deleteRow/<?php echo $row->id?>"><?php echo "Delete";?></a>
                                </td>                                 
                            </tr><?php
                            }?>
                        </tbody>
                        <?php
                        }else{?>

                            <tr class="gradeX">
                            	<td colspan="3">No record found .....</td>                                 
                            </tr>                        
                        <?php
                        }?>
                        
                    </table>
                	</div>
										
					</form>
				</div>
			</div>
       </section>
    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>
