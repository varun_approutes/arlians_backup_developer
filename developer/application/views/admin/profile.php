<?php

$this->load->view('admin/header');
$username='';
$usertype='';
$userimage='';
$userdescription='';
//echo '<pre>';print_r($profile);
if(count($profile)>0)
{
    foreach($profile as $details)
    {
        $username=$details->full_name;
        $usertype=$details->type;
        $userimage=$details->user_image;
        $userdescription=$details->description;
    }
}
if($userimage!='')
{
    $image_path='uploads/user_image/thumb/'.$userimage;
    if(file_exists($image_path))
    {
        $user_image=base_url().'uploads/user_image/thumb/'.$userimage;
    }
    else
    {
        $user_image=base_url().'assets/images/no-user.png';
    }
}
else
{
    $user_image=base_url().'assets/images/no-user.png';
}

if($usertype!='')
{
    if(($this->session->userdata('admin_user_type')=='Super_Admin') || ($this->session->userdata('admin_user_type')=='Super_Admin'))
    {
        if($usertype==0)
        {
            $designation='Super Admin';
        }
        if($usertype==1)
        {
            $designation='Admin';
        }
    }
    else
    {
      if($usertype==0)
      {
        $designation='Principal';
      }
      if($usertype==1)
      {
        $designation='Teacher';
      }
    }
}
else
{
    $designation='N/A';
}

?>

<div class="row">
    <div class="col-lg-12">
        <?php
        if($error_msg!='')
        {
        ?>
        <div class="error_msg">
            <img src="<?php echo base_url().'assets/images/error.png'?>" style="widfth:30px;height:30px;"><?php echo $error_msg;?>
        </div>
        <?php
        }
        if ($success_msg!='') 
        {
        ?>
        <div class="success_msg">
            <img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
        </div>
        <?php
        }
        ?>
        <section class="panel">
            <div class="panel-body profile-information">
               <div class="col-md-4">
                   <div class="profile-pic text-center">
                       <img src="<?php echo $user_image;?>" alt=""/>
                   </div>
               </div>
               <div class="col-md-8">
                   <div class="profile-desk">
                       <h1><?php echo $username;?></h1>
                       <span class="text-muted"><?php echo $designation;?></span><br/>
                       <p><?php echo $userdescription;?></p><br/>
                       <a href="<?php echo site_url('admin/profile/edit')?>" class="btn btn-primary">Edit Profile</a>
                   </div>
               </div>
           </div>
        </section>
    </div>
</div>
<?php
$this->load->view('admin/footer');
?>