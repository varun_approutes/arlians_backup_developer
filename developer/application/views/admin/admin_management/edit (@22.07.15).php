<?php

$this->load->view('admin/header');
//print_r($rows);
/*$admin_full_name=$admin_username=$admin_email='';
$admin_details=$this->model_school->AdminDetails($rows[0]->admin_id);
if(count($admin_details)>0)
{
	foreach ($admin_details as $result) 
	{
		$admin_full_name=$result->full_name;
		$admin_username=$result->username;
		$admin_email=$result->email;
	}
}*/
?>

<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="widfth:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Edit Admin Management
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/admin_management/update')?>" id="school_form" name="school_form" enctype="multipart/form-data">
               		<input type="hidden" name="edit_id" id="edit_id" value="<?php echo $rows[0]->id;?>">
               		 <input type="hidden" name="exist_image" id="exist_image" value="<?php echo $rows[0]->user_image;;?>">
               	    <!-- <input type="hidden" name="admin_id" id="admin_id" value="<?php echo $rows[0]->admin_id;?>">
               			<input type="hidden" name="type_value" id="type_value" value="<?php echo $rows[0]->type;?>">               	 -->	
               		<!--School Name Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">School Name</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="school_name" name="school_name" placeholder="School Name" value="<?php echo $rows[0]->school_name;?>" readonly="true">
	                            <p style="color:#A94442" id="school_name_error"></p>
	                        </div>
	                    </div>
	                    <!--Full Name Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">Admin Full Name</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name" value="<?php echo $rows[0]->full_name;?>" readonly="true">
	                            <p style="color:#A94442" id="full_name_error"></p>
	                        </div>
	                    </div>
	                <!--User Name Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">Admin Username</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="username" name="username" placeholder="User Name" value="<?php echo $rows[0]->username;?>" readonly="true">
	                            <p style="color:#A94442" id="username_error"></p>
	                        </div>
	                    </div>
	                 <!--Email Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">Admin Email</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $rows[0]->email;?>" readonly="true">
	                            <p style="color:#A94442" id="email_error"></p>
	                        </div>
	                    </div>
	                  
	                <!--Password Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">Admin Password</label>
	                        <div class="col-lg-7">
	                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="">
	                            <p style="color:#A94442" id="password_error"></p>
	                        </div>
	                    </div>                 
	                	
	              
	                <!--Admin Phone Number Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Phone Number</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="phone_no" name="phone_no" placeholder="Admin Phone Number" value="<?php echo $rows[0]->phone_no;?>" >
	                            <p style="color:#A94442" id="school_phone_error"></p>
	                        </div>
	                    </div>
	                <!-- School Description Section -->
	                	<div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="6" name="description" id="description" style="height:220px; width:350px;resize:none;"><?php echo $rows[0]->description;?></textarea>
                                <p style="color:#A94442" id="school_description_error"></p>
                            </div>
                        </div>
                    <!-- Exist Image -->
                    	<?php
                    	if($rows[0]->user_image!='')
                    	{
                    		$exist_file_path='uploads/user_image/thumb/'.$rows[0]->user_image;
                    		if(file_exists($exist_file_path))
                    		{
                    			$image_path=base_url().'uploads/user_image/thumb/'.$rows[0]->user_image;
                    	?>
                    		<div class="form-group">
	                            <label class="col-sm-3 control-label">Existing Image</label>
	                            <div class="col-sm-6">
	                                <img src="<?php echo $image_path;?>">
	                            </div>
	                        </div>
                    	<?php
                    		}
                    	}
                    	?>
	                <!--Image Upload Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Image</label>
	                        <div class="col-lg-7">
	                            <input type="file" name="logo" id="logo">
	                            <p style="color:#A94442" id="logo_error"></p>
	                        </div>
	                    </div>
	                    <div class="form-group">
                                <label class="col-lg-3 col-sm-2 control-label">Status</label>
                                <div class="col-lg-7">
                                    <select id="source" class="form-control" id="status" name="status">
                                            <option value="1" <?php if($rows[0]->status==1){echo "selected";}?>>Active</option>
                                            <option value="0" <?php if($rows[0]->status==0){echo "selected";}?>>Inactive</option>
                                    </select>
                                    <p style="color:#A94442" id="status_error"></p>
                                </div>
                            </div>
	                    <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-3">
	                        	<input type="button" class="btn btn-danger" id="hit" onclick="form_validation();" value="Update Admin">
	                        </div>
	                        <div class="col-lg-6">
	                        	<input type="button" value="Cancel" onclick="return_back();" class="btn" style="background:#c6c6c6;color:#ffffff">
	                        	<!-- <span style="font-style: italic; margin-left: 10px;">* Field are mandatory</span> -->
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                    </div>
                	</form>
                </div>
            </div>
        </section>

    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&language=en-AU"></script>
<script>
	
	function return_back()
	{
		var back_url='<?php echo base_url()?>admin/admin_management';
		window.location=back_url;
	}
	function show_address(id)
	{
		//alert(id);
		var location_id=id;
		var autocomplete = new google.maps.places.Autocomplete($("#school_address")[0], {});

		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			var place = autocomplete.getPlace();
			console.log(place.address_components);
		});
	}
</script>
<script type="text/javascript">

	function form_validation()
	{
		var has_error=0;
		//var userid=$('#admin_id').val();

		var password=$('#password').val();
		//alert(password);
		if(password!=''){
			if(password.length<6)
			{
				$('#password_error').html('Password length should not be less than 6 charecters.');
				$('#password').css('border-color','#A94442').focus();
				has_error++;
				return false;
			}else{			
				$('#password_error').html('');
				$('#password').css('border-color','');
			}
		}
		
		if($('#logo').val()!='')
			{
				var extension = $('#logo').val().split('.').pop().toUpperCase();
				if (extension!="PNG" && extension!="JPG" && extension!="GIF" && extension!="JPEG")
				{
					$('#logo_error').html('File format is not supported');
					$('#logo').focus();
					has_error++;
					return false;
				}
				else
				{
					$('#logo_error').html('');
				}
			}
			else
			{
				$('#logo_error').html('');
			}

			
			if(has_error==0)
			{
				$('#school_form').submit();
			}	



	}	
</script>
