<?php

$this->load->view('admin/header');
//echo count($result);
$designation='';
if(($this->session->userdata('admin_user_type')=='Super_Admin') || ($this->session->userdata('admin_user_type')=='Admin'))
{
	if($profile[0]->type==0)
	{
		$designation="Super Admin";
	}
	if($profile[0]->type==1)
	{
		$designation="Admin";
	}
}
else
{
	if($profile[0]->type==0)
	{
		$designation="Principal";
	}
	if($profile[0]->type==1)
	{
		$designation="Teacher";
	}
}

?>

<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="widfth:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Edit Profile
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/profile/update')?>" id="update_profile_form" name="site_settings_form" enctype="multipart/form-data">
               			<input type="hidden" name="existing_image" id="existing_image" value="<?php echo $profile[0]->user_image;?>" />
               			<input type="hidden" name="usertype" id="usertype" value="<?php echo $this->session->userdata('admin_user_type');?>" />
               			<input type="hidden" name="userid" id="userid" value="<?php echo $this->session->userdata('admin_user_data');?>" />

               			<div class="form-group">
	                        <label class=" col-sm-3 control-label">User Type</label>
	                        <div class="col-lg-6">
	                            <p class="form-control-static"><?php echo $designation;?></p>
	                        </div>
	                    </div>

               		<!--Name Section-->
	                    <div class="form-group">
	                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Name</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="full_name" name="full_name" placeholder="Full Name" value="<?php echo $profile[0]->full_name;?>">
	                            <p style="color:#A94442" id="full_name_error"></p>
	                        </div>
	                    </div>

	                <!--Email Section-->
	                    <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Email</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $profile[0]->email;?>">
	                            <p style="color:#A94442" id="email_error"></p>
	                        </div>
	                    </div>
	                
	                <!--Phone Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Phone</label>
	                        <div class="col-lg-7">
	                            <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="<?php echo $profile[0]->phone_no;?>">
	                            <p style="color:#A94442" id="phone_error"></p>
	                        </div>
	                    </div>
	                <!-- Exisitng Image -->
	                <?php
	               	if($profile[0]->user_image!='')
	               	{
	               		$file_exist='uploads/user_image/thumb/'.$profile[0]->user_image;
						
						$base_image=base_url().'uploads/user_image/thumb/'.$profile[0]->user_image;
							//echo $base_image;
						if(file_exists($file_exist))
						{
	               	?>
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Existing Image</label>
	                        <div class="col-lg-7">
	                            <img src="<?php echo $base_image;?>">
	                        </div>
	                    </div>
	                <?php
	            		}
	            	}
	                ?>
	                <!--Image Upload Section-->
	                	<div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Image</label>
	                        <div class="col-lg-7">
	                            <input type="file" name="image" id="image">
	                            <p style="color:#A94442" id="image_error"></p>
	                        </div>
	                    </div>
	                

	                	<div class="form-group">
                            <label class="col-sm-3 control-label">*Description</label>
                            <div class="col-sm-6">
                                <textarea class="form-control" rows="6" name="description" id="description" style="height:220px; width:350px;resize:none;"><?php echo $profile[0]->description;?></textarea>
                                <p style="color:#A94442" id="description_error"></p>
                            </div>
                        </div>

	                    <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-7">
	                        	<input type="button" class="btn btn-danger" id="hit" onclick="form_validation();" value="Update Profile">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                    </div>
                	</form>
                </div>
            </div>
        </section>

    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>

<script type="text/javascript">

	function form_validation()
	{
		var has_error=0;
		if($.trim($('#full_name').val())=='')
		{
			$('#full_name_error').html('Full name is required.');
			$('#full_name').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#full_name_error').html('');
			$('#full_name').css('border-color','');
		}
		
		if($.trim($('#email').val())=='')
		{
			$('#email_error').html('Admin email is required.');
			$('#email').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else if($('#email').val()!='')
		{
			var re = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
			var email=$('#email').val();
			if(!re.test($('#email').val()))
			{
				$('#email_error').html('Please enter valid email');
				$('#email').css('border-color','#A94442').focus();
				has_error++;
				return false;
			}
			else
			{
				$('#email_error').html('');
				$('#email').css('border-color','');

				$.ajax({
					url : '<?php echo base_url();?>admin/profile/check_email',
					data : 'userid='+$('#userid').val()+"&usertype="+$('#usertype').val()+"&email="+email,
					success : function(res)
					{
						//alert(res);
						if(res=='FALSE')
						{
							$('#email_error').html('This email is already exist. Please try another.');
							$('#email').css('border-color','#A94442').focus();
							has_error++;
							return false;
						}
						else
						{
							$('#email_error').html('');
							$('#email').css('border-color','');

							if($.trim($('#phone').val())=='')
							{
								$('#phone_error').html('Phone is required.');
								$('#phone').css('border-color','#A94442').focus();
								has_error++;
								return false;
							}
							else
							{
								$('#phone_error').html('');
								$('#phone').css('border-color','');
							}
							if($('#image').val()!='')
							{
								var extension = $('#image').val().split('.').pop().toUpperCase();
								if (extension!="PNG" && extension!="JPG" && extension!="GIF" && extension!="JPEG")
								{
									$('#image_error').html('File format is not supported');
									$('#image').focus();
									has_error++;
									return false;
								}
								else
								{
									$('#image_error').html('');
								}
							}
							else
							{
								$('#image_error').html('');
							}

							if($.trim($('#description').val())=='')
							{
								$('#description_error').html('Description is required.');
								$('#description').css('border-color','#A94442').focus();
								has_error++;
								return false;
							}
							else
							{
								$('#description_error').html('D');
								$('#description').css('border-color','');
							}
							if(has_error==0)
							{
								$('#update_profile_form').submit();
							}
						}
					}
				});

			}
		}
	}
</script>