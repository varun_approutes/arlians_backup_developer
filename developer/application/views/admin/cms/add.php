<?php
$this->load->view('admin/header');
?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
tinymce.init({
    selector: "textarea"
 });
</script>
<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="width:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Add Cms
            </header>
            <div class="panel-body">
                <div class="position-center">
                    <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/cms/insert')?>" id="cms_form" name="cms_form" enctype="multipart/form-data">
               		 <!--cms Name Section-->
	                   <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Title</label>
   		                    <div class="col-lg-7">
   		                            <input type="text" class="form-control" id="title" name="title" placeholder="Cms Title" value="">
   		                            <p style="color:#A94442" id="name_error"></p>
   		                    </div>
	                   </div>
	                                     
	                <!--Student's Email Section-->
	                    <div class="form-group">
	                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">Content</label>
	                        <div class="col-lg-7">
	                            	<textarea name="content" rows="10" cols="10" ></textarea>
	                            <p style="color:#A94442" id="email_error"></p>
	                        </div>
	                    </div>

	              	
	                    <div class="form-group">
                                <label class="col-lg-3 col-sm-2 control-label">Status</label>
                                <div class="col-lg-7">
                                    <select id="source" class="form-control" id="status" name="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                    </select>
                                    <p style="color:#A94442" id="status_error"></p>
                                </div>
                        </div>
	                    <div class="form-group">
	                        <div class="col-lg-offset-3 col-lg-2">
	                        	<input type="button" class="btn btn-danger" id="hit" onclick="form_validation();" value="Add">
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                        <div class="col-lg-6">
	                        	<input type="button" value="Cancel" onclick="return_back();" class="btn" style="background:#c6c6c6;color:#ffffff">
	                        	<span style="font-style: italic; margin-left: 10px;">* Field are mandatory</span>
	                            <!-- <button type="button" class="btn btn-danger" oncilck="form_validation();">Update Password</button> -->
	                        </div>
	                    </div>
                	</form>
                </div>
            </div>
        </section>

    </div>
</div>
    
<?php
$this->load->view('admin/footer');
?>
<script>
	function return_back()
	{
		var back_url='<?php echo base_url()?>admin/cms';
		window.location=back_url;
	}
	
</script>
<script type="text/javascript">

	function form_validation()
	{
		var has_error=0;
		
		
		if($.trim($('#title').val())=='')
		{
			$('#name_error').html('cms page Title is required.');
			$('#title').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#name_error').html('');
			$('#title').css('border-color','');
		}
		
		if(has_error==0){$('#cms_form').submit();
			}
		
	}
</script>
