<?php

$this->load->view('admin/header');
//echo count($result);
$all_class_list=$this->model_calendar->AllClass();
?>
<style>
.fc-event-skin{
	cursor: pointer;
}
</style>
<link href="<?php echo base_url()?>assets/admin/js/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="width:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Calendar Module
            </header>
            <div class="panel-body">
                <div class="row">
                    <aside class="col-lg-12">
                          <div id="calendar" class="has-toolbar"></div>
                    </aside>
                </div>

<a data-toggle="modal" href="#myModal" id="forgot_password" style="display:none;"></a>
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              		<div class="modal-dialog">
                  		<div class="modal-content">
                      		<div class="modal-header">
                          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_x">&times;</button>
                          		<h4 class="modal-title">Manage Holiday</h4>
                      		</div>                       
                  			<div class="modal-body">
                  				<form class="form-horizontal" role="form" method="POST" action="" id="holiday_form" name="holiday_form" enctype="multipart/form-data">
                  					<input type="hidden" name="holiday_date" id="holiday_date" value="">
                  					<div class="form-group">
				                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Holiday Title</label>
				                        <div class="col-lg-7">
				                            <input type="text" class="location form-control" id="title" name="title" placeholder="Title" value="">
				                            <p style="color:#A94442" id="title_error"></p>
				                        </div>
				                    </div>
				                    <div class="form-group">
			                            <label class="col-lg-3 col-sm-2 control-label">*Description</label>
			                            <div class="col-sm-9">
			                                <textarea class="form-control ckeditor" name="description" id="description" rows="6"></textarea>
			                                <p style="color:#A94442" id="description_error"></p>
			                            </div>
			                        </div>
					        	</form>
					        </div>
					        <div class="modal-footer">
		                        <button data-dismiss="modal" class="btn btn-default" type="button" id="f_cancel">Cancel</button>
		                        <button class="btn btn-success" type="button" onclick="email_validation();">Submit</button>
					        </div>
					    </div>
					</div>
				</div>
            </div>
        </section>

    </div>
</div>
<!---->
<?php
$this->load->view('admin/footer');
?>
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/ckeditor/ckeditor.js"></script>
<script>
$('#f_cancel').click(function()
{
    $("#title").val("");
    CKEDITOR.instances.description.setData('');
});
$('#close_x').click(function()
{
    $("#title").val("");
    CKEDITOR.instances.description.setData('');
});
$(document).ready(function () {
    /* initialize the calendar
     -----------------------------------------------------------------*/

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();

    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: true,
        disableDragging: true,
        droppable: false, // this allows things to be dropped onto the calendar !!!
        
        events: [
        	<?php
        	if(count($all_class_list)>0)
        	{
        		$i=1;
        		foreach($all_class_list as $class_list)
        		{
        			if($i!=count($all_class_list))
        			{
        				$get_comma=',';
        			}
        			else
        			{
        				$get_comma='';
        			}
        	?>
        	{
                title: '<?php echo $class_list->topic_name;?>',
                start: '<?php echo $class_list->start_time;?>',
                end: '<?php echo $class_list->end_time?>',
                
            }<?php echo $get_comma;?>
        	<?php
        		$i++;
        		}
        	}
        	?>
            
        ],
        dayClick: function(date, jsEvent, view) {

	        //alert('Clicked on: ' + date.format());
	        //alert(date.toString());
	        var date_format=date.toString();
	        var split_date_format=date_format.split(' 00:00:00 ');
	        var date = new Date(split_date_format[0]);
	        var get_month=date.getMonth() + 1;
	        var get_day=date.getDate();
	        if(parseInt(get_month)<10)
	        {	
	        	//alert('PAPAI');
	        	var new_month='0'+get_month;
	        }
	        else
	        {
	        	//alert('JAY');
	        	var new_month=get_month;
	        }
	        if(parseInt(get_day)<10)
	        {
	        	var new_day='0'+get_day;
	        }
	        else
	        {
	        	var new_day=get_day;
	        }
	        var new_generated_date=date.getFullYear() + '/' + new_month + '/' +  new_day;
			
			$('#holiday_date').val(new_generated_date);
	        $('#forgot_password').click();

	        // change the day's background color just for fun
	        //$(this).css('background-color', 'red');

	    }
    });
});
</script>
<!-- Right section -->

