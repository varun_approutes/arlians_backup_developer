<?php

$this->load->view('admin/header');
//echo count($result);
$all_class_list=$this->model_calendar->AllClass();
$all_holiday_list=$this->model_calendar->holiday_list();
?>
<style>
.fc-event-skin{
	cursor: pointer;
}
</style>
<link href="<?php echo base_url()?>assets/admin/js/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
<div class="row">
	<div class="col-lg-12">
		<?php
		if($error_msg!='')
		{
		?>
		<div class="error_msg">
			<img src="<?php echo base_url().'assets/images/error.png'?>" style="width:30px;height:30px;"><?php echo $error_msg;?>
		</div>
		<?php
		}
		if ($success_msg!='') 
		{
		?>
		<div class="success_msg">
			<img src="<?php echo base_url().'assets/images/icon_checked-512.png'?>" style="width:50px;height:50px;"><?php echo $success_msg;?>
		</div>
		<?php
		}
		?>
		
        <section class="panel">
            <header class="panel-heading">
                Calendar Module
            </header>
            <div class="panel-body">
                <div class="row">
                    <aside class="col-lg-12">
                          <div id="calendar" class="has-toolbar"></div>
                    </aside>
                </div>

<a data-toggle="modal" href="#myModal" id="forgot_password" style="display:none;"></a>
                <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
              		<div class="modal-dialog">
                  		<div class="modal-content">
                      		<div class="modal-header">
                          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_x">&times;</button>
                          		<h4 class="modal-title">Manage Holiday</h4>
                      		</div>                       		                      
                  			<div class="modal-body">
                  				<?php $school_id = $this->session->userdata('admin_school_id');?> 
                  				<form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/calendar/add_holiday/'.$school_id)?>" id="holiday_form" name="holiday_form" enctype="multipart/form-data">
                  					<input type="hidden" name="holiday_date" id="holiday_date" value="">
                  					<input type="hidden" name="school_id" id="school_id" value="<?php echo $this->session->userdata('admin_school_id');?>">
                  					<div class="form-group">
				                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Holiday Title</label>
				                        <div class="col-lg-7">
				                            <input type="text" class="location form-control" id="title" name="title" placeholder="Title" value="">
				                            <p style="color:#A94442" id="title_error"></p>
				                        </div>
				                    </div>
				                    <div class="form-group">
			                            <label class="col-lg-3 col-sm-2 control-label">*Description</label>
			                            <div class="col-sm-9">
			                                <textarea class="form-control ckeditor" name="description" id="description" rows="6"></textarea>
			                                <p style="color:#A94442" id="description_error"></p>
			                            </div>
			                        </div>
					        	</form>
					        </div>
					        <div class="modal-footer">
		                        <button data-dismiss="modal" class="btn btn-default" type="button" id="f_cancel">Cancel</button>
		                        <button class="btn btn-success" type="button" onclick="form_validation();">Submit</button>
					        </div>
					    </div>
					</div>
				</div>
<!---------------------- UPDATE HOLIDAY ---------------------------------->
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="UpdateHolidayModal" class="modal fade">
              		<div class="modal-dialog">
                  		<div class="modal-content">
                      		<div class="modal-header">
                          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_x">&times;</button>
                          		<h4 class="modal-title">Update Holiday</h4>
                      		</div>                       		                      
                  			<div class="modal-body">
                  				
                  				<form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/calendar/update_holiday')?>" id="update_holiday_form" name="holiday_form" enctype="multipart/form-data">
                  					<input type="hidden" name="holiday_id" id="holiday_id" value="">
                  					<div class="form-group">
				                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Holiday Title</label>
				                        <div class="col-lg-7">
				                            <input type="text" class="location form-control" id="edit_holiday_title" name="edit_holiday_title" placeholder="Title" value="">
				                            <p style="color:#A94442" id="edit_holiday_title_error"></p>
				                        </div>
				                    </div>
				                    <div class="form-group">
			                            <label class="col-lg-3 col-sm-2 control-label">*Description</label>
			                            <div class="col-sm-9">
			                                <textarea class="form-control ckeditor" name="edit_holiday_description" id="edit_holiday_description" rows="6"></textarea>
			                                <p style="color:#A94442" id="edit_holiday_description_error"></p>
			                            </div>
			                        </div>
					        	</form>
					        </div>
					        <div class="modal-footer">
		                        <button data-dismiss="modal" class="btn btn-default" type="button" id="f_cancel">Cancel</button>
		                        <button class="btn btn-success" type="button" onclick="update_holiday_form_validation();">Update</button>
					        </div>
					    </div>
					</div>
				</div>
<!---------------------- UPDATE CLASS SCHEDULE ---------------------------------->
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="UpdateClassModal" class="modal fade">
              		<div class="modal-dialog">
                  		<div class="modal-content">
                      		<div class="modal-header">
                          		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id="close_x">&times;</button>
                          		<h4 class="modal-title">Update Class Schedule</h4>
                      		</div>                       		                      
                  			<div class="modal-body">
                  				
                  				<form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('admin/calendar/update_class_module')?>" id="update_class_module" name="update_class_module" enctype="multipart/form-data">
                  					<input type="hidden" name="holiday_id" id="holiday_id" value="">
                  					
				                    <div class="form-group">
				                        <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label" id="show_class_time_label"></label>
				                        <div class="col-lg-7">	                            
			                                  <p class="form-control-static" id="show_class_time"></p>                           
				                        </div>
				                    </div>
					                
					                <div class="form-group">
				                        <label for="inputPassword1" class="col-lg-3 col-sm-2 control-label">*Topic_name</label>
			   		                    <div class="col-lg-7">
			   		                            <input type="text" class="form-control" id="topic_name" name="topic_name" placeholder="Class topic name" value="">
			   		                            <p style="color:#A94442" id="topic_name_error"></p>
			   		                    </div>
				                   	</div>
				                   	<div class="form-group">
			                            <label class="col-lg-3 col-sm-2 control-label">* Topic</label>
			                            <div class="col-sm-9">
			                                <textarea class="form-control ckeditor" name="class_topic_description" id="class_topic_description" rows="6"></textarea>
			                                <p style="color:#A94442" id="class_topic_description_error"></p>
			                            </div>
			                        </div>
			                        <div class="form-group">
			                            <label class="col-lg-3 col-sm-2 control-label">* Class Date</label>
			                            <div class="col-sm-7">
			                                <input class="form-control form-control-inline input-medium default-date-picker"  size="16" type="text" value="" name="select_date" id="select_date" readonly="true"/>
			                                <p style="color:#A94442" id="select_date_error"></p>
			                            </div>
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label col-md-3">*Start Time</label>
			                            <div class="col-md-7">
			                                <div class="input-group bootstrap-timepicker">
			                                    <input type="text" class="form-control timepicker-default" name="start_time" id="start_time" readonly="true">
			                                    <span class="input-group-btn">
			                                    	<button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
			                                    </span>
			                                    
			                                </div>
			                                <p style="color:#A94442" id="start_time_error"></p>
			                            </div>
			                        </div>

			                        <div class="form-group">
			                            <label class="control-label col-md-3">*End Time</label>
			                            <div class="col-md-7">
			                                <div class="input-group bootstrap-timepicker">
			                                    <input type="text" class="form-control timepicker-default" name="end_time" id="end_time" readonly="true">
			                                    <span class="input-group-btn">
			                                    	<button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
			                                    </span>
			                                    
			                                </div>
			                                <p style="color:#A94442" id="end_time_error"></p>
			                            </div>
			                        </div>


			                        <div class="form-group">
			                            <label class="control-label col-md-3">*Assign Teacher</label>
			                            <div class="col-md-7" id="assign_teacher_div">
			                            </div>
			                        </div>
					        	</form>
					        </div>
					        <div class="modal-footer">
		                        <button data-dismiss="modal" class="btn btn-default" type="button" id="f_cancel">Cancel</button>
		                        <button class="btn btn-success" type="button" onclick="update_holiday_form_validation();">Update</button>
					        </div>
					    </div>
					</div>
				</div>
            </div>
        </section>

    </div>
</div>
<!---->
<?php
$this->load->view('admin/footer');
?>
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="<?php echo base_url()?>assets/admin/js/advanced-form.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/admin/js/ckeditor/ckeditor.js"></script>
<script>
$('#f_cancel').click(function()
{
    $("#title").val("");
    CKEDITOR.instances.description.setData('');
});
$('#close_x').click(function()
{
    $("#title").val("");
    CKEDITOR.instances.description.setData('');
});
$(document).ready(function () {
    /* initialize the calendar
     -----------------------------------------------------------------*/

    var new_date = new Date();
    var d = new_date.getDate();
    var m = new_date.getMonth();
    var y = new_date.getFullYear();

    $('#calendar').fullCalendar({
    	viewDisplay   : function(view) {
	      var now = new Date(); 
	      var end = new Date();
	      end.setMonth(now.getMonth() + 11); //Adjust as needed

	      var cal_date_string = view.start.getMonth()+'/'+view.start.getFullYear();
	      var cur_date_string = now.getMonth()+'/'+now.getFullYear();
	      var end_date_string = end.getMonth()+'/'+end.getFullYear();

	      if(cal_date_string == cur_date_string) { jQuery('.fc-button-prev').addClass("fc-state-disabled"); }
	      else { jQuery('.fc-button-prev').removeClass("fc-state-disabled"); }

	      if(end_date_string == cal_date_string) { jQuery('.fc-button-next').addClass("fc-state-disabled"); }
	      else { jQuery('.fc-button-next').removeClass("fc-state-disabled"); }
	    },
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: true,
        disableDragging: true,
        droppable: false, // this allows things to be dropped onto the calendar !!!
        
        events: [
        	<?php
        	if(count($all_class_list)>0)
        	{
        		$i=1;
        		foreach($all_class_list as $class_list)
        		{
        		
        			if($i!=count($all_class_list))
        			{
        				$get_comma=',';
        			}
        			else if(count($all_holiday_list)>0)
        			{
        				$get_comma=',';
        			}
        			else
        			{
        				$get_comma='';
        			}
        	?>
        	{
        		id: '<?php echo $class_list->id?>',
        		type:'Class',
                title: '<?php echo $class_list->topic_name;?>',
                start: '<?php echo $class_list->start_time;?>',
                end: '<?php echo $class_list->end_time?>',
                
            }<?php echo $get_comma;?>
        	<?php 
        		$i++;
        		}
        	}
        	if(count($all_holiday_list)>0)
        	{
        		$j=1;
        		foreach($all_holiday_list as $holiday_list)
        		{
        			if($j!=count($all_holiday_list))
        			{
        				$get_holiday_comma=',';
        			}
        			
        			else
        			{
        				$get_holiday_comma='';
        			}
        	?>
        	{
        		id: '<?php echo $holiday_list->id?>',
        		type:'Holiday',
                title: '<?php echo $holiday_list->title;?>',
                start: '<?php echo $holiday_list->date;?>',
                backgroundColor: '#F78181 !important',
                borderColor: '#f78181 !important'
            }<?php echo $get_holiday_comma;?>
        	<?php
        		}
        	}
        	?>
            
        ],
        eventClick: function(event) {
		    /*alert(event.id);*/
		    //alert(event.type);
		    
		    if(event.start<new_date)
		    {
		    	alert('PAPAI');
		    }
		    if(event.type=='Holiday')
		    {
		    	$.ajax({
		    		url : '<?php echo base_url();?>admin/calendar/getHolidayDetails',
		    		data : 'value='+event.id,
		    		success : function(res_holiday)
		    		{
		    			if(res_holiday!='')
		    			{
		    				//alert(res_holiday);
		    				var split_holiday=res_holiday.split('[DIGITAL_APTECH]');
		    				//alert(split_holiday);
		    				$('#holiday_id').val(split_holiday[0]);
		    				$('#edit_holiday_title').val(split_holiday[1]);
		    				CKEDITOR.instances.edit_holiday_description.setData(split_holiday[2]);
		    				$('#UpdateHolidayModal').modal('show');
		    			}
		    		}
		    	});
		    }
		    if(event.type=='Class')
		    {
		    	//$('#UpdateClassModal').modal('show');
		    	$.ajax({
		    		url : '<?php echo base_url();?>admin/calendar/getClassTopicDetails',
		    		data : 'value='+event.id,
		    		success : function(res_class)
		    		{
		    			if(res_class!='')
		    			{
		    				//alert(res_class);
		    				var split_class=res_class.split('[DIGITAL_APTECH]');
		    				$('#show_class_time_label').html(split_class[0]);
		    				$('#show_class_time').html(split_class[1]);
		    				$('#topic_name').val(split_class[2]);
		    				//$('#class_topic_description').val(split_class[3]);
		    				CKEDITOR.instances.class_topic_description.setData(split_class[3]);
		    				$('#select_date').val(split_class[4]);
		    				$('#start_time').val(split_class[5]);
		    				$('#end_time').val(split_class[6]);
		    				$('#assign_teacher_div').html(split_class[7]);
		    				$('#UpdateClassModal').modal('show');
		    			}
		    		}
		    	});
		    }
		    
		},
        dayClick: function(date, jsEvent, view) {

	        //alert('Clicked on: ' + date.format());
	        //alert(date.toString());
	        if(date<new_date)
	        {
	        	alert('You can not select the previous date');
	        }
	        else
	        {
		        var date_format=date.toString();
		        var split_date_format=date_format.split(' 00:00:00 ');
		        var date = new Date(split_date_format[0]);
		        var get_month=date.getMonth() + 1;
		        var get_day=date.getDate();
		        if(parseInt(get_month)<10)
		        {	
		        	//alert('PAPAI');
		        	var new_month='0'+get_month;
		        }
		        else
		        {
		        	//alert('JAY');
		        	var new_month=get_month;
		        }
		        if(parseInt(get_day)<10)
		        {
		        	var new_day='0'+get_day;
		        }
		        else
		        {
		        	var new_day=get_day;
		        }
		        var new_generated_date=date.getFullYear() + '/' + new_month + '/' +  new_day;
				
				$('#holiday_date').val(new_generated_date);
		        $('#forgot_password').click();

		        // change the day's background color just for fun
		        //$(this).css('background-color', 'red');
	        }
	    },

	    
    });
});
</script>
<!-- Right section -->
<!-- form validation start-->
<script type="text/javascript">

	function form_validation()
	{
		var has_error=0;
			if($('#title').val()=='')
			{
				$('#title_error').html('Please Add Title.');
				$('#title').css('border-color','#A94442').focus();
				has_error++;
				return false;
			}
			else
			{
				$('#title_error').html('');
				$('#title').css('border-color','');
			}

			
			if(has_error==0)
			{
				$('#holiday_form').submit();
			}
	}

	function update_holiday_form_validation()
	{
		var has_error=0;
		if($('#edit_holiday_title').val()=='')
		{
			$('#edit_holiday_title_error').html('Please Add Title.');
			$('#edit_holiday_title').css('border-color','#A94442').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#edit_holiday_title_error').html('');
			$('#edit_holiday_title').css('border-color','');
		}
		if(CKEDITOR.instances.edit_holiday_description.getData()=='')
		{
			$('#edit_holiday_description_error').html('Holiday description is required.');
			$('#edit_holiday_description').focus();
			has_error++;
			return false;
		}
		else
		{
			$('#edit_holiday_description').html('');
		}
		if(has_error==0)
		{
			$('#update_holiday_form').submit();
		}
	}
</script>		


