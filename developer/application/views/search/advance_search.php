<div class="dashboard-wrap" id="ad_search_segment">
    <div class="row">
        <div class="fl100">
            <div class="fl20">
                <div class="left-search-list">
                    <h3>Industries</h3>
                    <ul class="left-search-ul" data-bind="foreach:industries_list">
                        <li>
                            <input type="checkbox" name="industries" data-bind="checkedValue: top_code, checked: $parent.selected_industries" >
                            <a href="javascript:void(0)" data-bind="text:top_name">Lorem ipsum</a>
                        </li>                        
                    </ul>
                </div>
                <div class="left-search-list">
                    <h3>Sectors</h3>
                    <ul class="left-search-ul" data-bind="foreach:sector_list">
                        <li>
                            <input type="checkbox" name="sector" data-bind="checkedValue: sector_code, checked: $parent.selected_sector">
                            <a href="javascript:void(0)" data-bind="text:sector_name">Lorem ipsum</a>
                        </li>                        
                    </ul>
                </div>
                <div class="left-search-list">
                    <h3>Sub-Sectors</h3>
                    <ul class="left-search-ul" data-bind="foreach:sub_sector_list">
                        <li>
                            <input type="checkbox" name="sub_sector" data-bind="checkedValue: subsector_code, checked: $parent.selected_sub_sector">
                            <a href="javascript:void(0)" data-bind="text:subsector_name">Lorem ipsum</a>
                        </li>                        
                    </ul>
                </div>
                <div class="left-search-list">
                    <h3>Country</h3>
                    <ul class="left-search-ul" data-bind="foreach:country_list">
                        <li>
                            <input type="checkbox" name="country" data-bind="checkedValue: id, checked: $parent.selected_country">
                            <a href="javascript:void(0)" data-bind="text:country_name">Lorem ipsum</a>
                        </li>                        
                    </ul>
                </div>
                <div class="left-search-list">
                    <h3>Suggestion type</h3>
                    <ul class="left-search-ul" data-bind="foreach:suggestion_list">
                        <li>
                           <!-- <input type="radio" name="looking_name" data-bind="checkedValue: looking_for_id+'-'+search_type, checked: $parent.selected_suggestion">-->
                            <input type="radio" name="looking_name" data-bind="checkedValue: looking_for_id, checked: $parent.selected_suggestion">
                            <a href="javascript:void(0)" data-bind="text:looking_for_name">Lorem ipsum</a>
                        </li>                        
                    </ul>
                </div>            
                <!--<div class="left-search-list">
                    <h3>Locality</h3>
                    <ul class="left-search-ul" data-bind="foreach:industries_list">
                        <li>
                            <input type="checkbox" name="check" id="check">
                            <a href="javascript:void(0)">Lorem ipsum</a>
                        </li>
                        <li>
                            <input type="checkbox" name="check" id="check">
                            <a href="javascript:void(0)">Lorem ipsum</a>
                        </li>
                        <li>
                            <input type="checkbox" name="check" id="check">
                            <a href="javascript:void(0)">Lorem ipsum</a>
                        </li>
                        <li>
                            <input type="checkbox" name="check" id="check">
                            <a href="javascript:void(0)">Lorem ipsum</a>
                        </li>
                        <li>
                            <input type="checkbox" name="check" id="check">
                            <a href="javascript:void(0)">Lorem ipsum</a>
                        </li>
                    </ul>
                </div>-->
            </div>
            <div class="fl80">
                <div class="compose-inner-bg profile-bg">
                    <div class="profile-inner search-inner">
                        <h3>Search Results</h3>
                        <ul class="profile-connect" data-bind="foreach:search_result_list">
                            <li>
                                <div class="inbox-img"> 
                                    <a data-bind="attr:{href:'<?php echo base_url()?>viewprofile/profile/'+curr_id()}">
                                    <!--<a href="http://localhost/arlians/social-network/viewprofile/profile/8">-->
                                        <img data-bind="attr:{src:'<?php echo base_url()?>uploads/profile_image/'+user().background_image}" alt=""> 
                                        <!--<img src="http://localhost/arlians/social-network/uploads/profile_image/profile_image.png" alt=""> -->
                                    </a>
                                </div>
                                <div class="inbox-content">
                                    <a  databind="attr:{href:'<?php echo base_url()?>viewprofile/profile/'+curr_id()}">
                                        <h4 data-bind="text:user().bussinessname">test business</h4>
                                    </a>
                                    <p class="date" data-bind="text:suggested_user_type"></p>
                                </div>
                                <!-- ko if:is_connected()=='true'-->
                                <div class="right-inbox-icon">
                                    <a href="javascript:void(0);" class="icon-border" ><i class="fa fa-times fa-1"></i></a>
                                </div>    
                                <!-- /ko -->
                                <!-- ko if:is_connected()=='false'-->
                                <div class="right-inbox-icon">
                                    <a href="javascript:void(0);" class="icon-border" data-bind="click:$root.reqestForConnection"><i class="fa fa-check fa-1"></i></a>
                                </div>    
                                <!-- /ko -->
                                <!-- ko if:is_connected()=='pending'-->                               
                                    <a href="javascript:void(0);" class="request-send-btn" >Request Send</a>                                    
                                <!-- /ko -->
                                
                            </li>                            
                        </ul>
                    </div>
                </div>
                <button class="refine" data-bind="click:refineSearch">Refine</button>  
            </div>  
            
        </div>
    </div>
    <!--<div data-bind="text: ko.toJSON($root)"></div>-->
</div>

<script>
    var vmAdSearch = {
        industries_list:ko.observableArray(),
        selected_industries: ko.observableArray(),
        sector_list:ko.observableArray(),
        selected_sector: ko.observableArray(),
        sub_sector_list:ko.observableArray(),
        selected_sub_sector: ko.observableArray(),
        country_list:ko.observableArray(),
        selected_country: ko.observableArray(),
        suggestion_list:ko.observableArray(),
        selected_suggestion: ko.observable(),
        search_result_list:ko.observableArray(),
        reqestForConnection:function(){
            var current_item = this;            
            $("#loader-wrapper").show(); 
            $.post('<?php echo base_url(); ?>member/reqestForConnection',{'id':current_item.curr_id()},function(result){
                $("#loader-wrapper").hide(); 
                if(result==1){
                    current_item.is_connected("pending");                   
                }else{
                    alert('This is embrassing.Some error occured');
                }
            });
        },
        reqestForDisconnection:function(){
            var current_item = this;            
            $("#loader-wrapper").show(); 
            $.post('<?php echo base_url(); ?>member/reqestForDisconnection',{'id':current_item.curr_id()},function(result){
                $("#loader-wrapper").hide(); 
                if(result==1){
                    current_item.is_connected("false");                   
                }else{
                    alert('This is embrassing.Some error occured');
                }
            });
        },
        refineSearch:function(){
            $("#loader-wrapper").show(); 
            $.post('<?php echo base_url(); ?>search/ajaxAdvanceSearch',{selected_industries:vmAdSearch.selected_industries(),selected_sector:vmAdSearch.selected_sector(),selected_sub_sector:vmAdSearch.selected_sub_sector(),selected_country:vmAdSearch.selected_country(),selected_suggestion:vmAdSearch.selected_suggestion()},function(result){
                $("#loader-wrapper").hide();
                result = eval("(" + result + ")");                
                var items = result;
                    var search_items = new Array();
                    for (var i = 0; i < items.length; i++) {
                        var search_item = {};
                        for (var s in items[i]) {
                            search_item[s] = ko.observable(items[i][s]);
                        }                        
                        search_items.push(search_item);
                    }                
                vmAdSearch.search_result_list(search_items);
            });
        }
    }
    $(function(){
        try{
            $("#loader-wrapper").show(); 
            $.post('<?php echo base_url(); ?>category/get_top_list',{},function(result){ 
                $("#loader-wrapper").hide(); 
                result = eval("(" + result + ")"); 
                vmAdSearch.industries_list(result);
                //console.log(result); 
            });
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>category/get_sector_list',{},function(result){ 
                $("#loader-wrapper").hide();    
                result = eval("(" + result + ")");
                //console.log(result); 
                vmAdSearch.sector_list(result);
            });
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>category/get_subsector_list',{},function(result){ 
                $("#loader-wrapper").hide();
                result = eval("(" + result + ")"); 
                //console.log(result); 
                vmAdSearch.sub_sector_list(result);   
            });
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>category/get_looking_for_list',{},function(result){ 
                $("#loader-wrapper").hide();
                result = eval("(" + result + ")");
                //console.log(result); 
                vmAdSearch.suggestion_list(result);   
            }); 
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>member/fetch_country_list',{},function(result){ 
                $("#loader-wrapper").hide();
                result = eval("(" + result + ")");
                //console.log(result); 
                vmAdSearch.country_list(result);   
            }); 
        }catch(e){
            alertify.error(e);
        }
    });
    ko.applyBindings(vmAdSearch, $("#ad_search_segment")[0]);
</script>