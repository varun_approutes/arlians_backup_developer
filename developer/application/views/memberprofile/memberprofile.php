<?php //print_r($upload_success); exit; 
    if(!empty($upload_success)){
        foreach ($upload_success as $key => $value) { ?>
            <div class="sucess-msg"><?php echo $value;?></div>
        <?php }
    }
    if(!empty($upload_error)){
        foreach ($upload_error as $key => $value) { ?>
            <div class="error-msg"><?php echo $value;?></div>
        <?php }
    }
?>
<div class="dashboard-wrap member-profile-wrap" id="manage_profile_section">
    <div class="row">
        <div class="fl100">
            <div class="compose-inner-bg profile-new-bg member-profile-bg">
                <div class="profile-inner">
                    <h3><?php echo $bussinessname; ?><a href="#div4" class="edit-sec" id="pr_buss"><i class="fa fa-pencil"></i></a></h3>
                </div>
                <div class="profile-inner member-inner">
                     <!--<h3><span data-bind="text:first_name()+' '+last_name()"></span> <a class="edit-sec" href="#manage_profile" id="manage_profile_link"><i class="fa fa-pencil"></i></a></h3>-->
                     <h3 data-bind="visible:show_tag_line"><span data-bind="text:tag_line()!=''?tag_line():'Add tagline'"></span> <a class="edit-sec" href="javascript:void(0);" data-bind="click:viewEditTagLine"><i class="fa fa-pencil"></i></a></h3>
                     <span data-bind="visible:edit_tag_line">
                        <span style="float:left"> Edit tagline </span>
                        <ul class="edit-user-name">
                            <form id="update_tag_line_form" method="post">
                                <li><input type="text" name="tag_line" data-bind="value:tag_line"></li>
                                <li><button type="submit" class="button" >Save</button></li>
                                <!--<a class="edit-sec" href="javascript:void(0);" data-bind="click:viewName"><i class="fa fa-save"></i></a>-->
                            </form>
                        </ul>     
                    </span>
                </div>
                <div class="profile-inner member-inner">
                     <!--<h3><span data-bind="text:first_name()+' '+last_name()"></span> <a class="edit-sec" href="#manage_profile" id="manage_profile_link"><i class="fa fa-pencil"></i></a></h3>-->
                     <h3 data-bind="visible:show_name"><span data-bind="text:first_name()!=''||last_name()!=''?first_name()+' '+last_name():'Edit name'"></span> <a class="edit-sec" href="javascript:void(0);" data-bind="click:viewEditName"><i class="fa fa-pencil"></i></a></h3>
                     <span data-bind="visible:edit_name">
                        <span style="float:left"> Edit name </span>
                        <ul class="edit-user-name">
                            <form id="update_name_form" method="post">
                                <li><input type="text" name="first_name" data-bind="value:first_name" ></li>
                                <li><input type="text" name="last_name" data-bind="value:last_name" ></li>
                                <li><button type="submit" class="button" id="manage_profile_update" >Save</button></li>
                                <!--<a class="edit-sec" href="javascript:void(0);" data-bind="click:viewName"><i class="fa fa-save"></i></a>-->
                            </form> 
                        </ul>    
                    </span>
                </div>
                <div class="profile-inner member-inner">
                     <!--<h3><?php echo $email; ?><a href="#div3" class="edit-sec" id="pr_name"><i class="fa fa-pencil"></i></a></h3>-->
                     <h3 data-bind="visible:show_email"><span data-bind="text:email()"></span><a href="javascript:void(0);" class="edit-sec" data-bind="click:viewEditEmail"><i class="fa fa-pencil"></i></a></h3>
                     <span data-bind="visible:edit_email">
                        <span style="float:left"> Edit email </span>
                        <ul class="edit-user-name">
                            <form id="update_email_form" method="post">
                                <li><input type="email" data-bind="value:email" name="email"></li>
                                <li><button type="submit" class="button">Save</button></li>
                                <!--<a href="javascript:void(0);" class="edit-sec" data-bind="click:viewEmail"><i class="fa fa-save"></i></a>-->
                            </form>
                        </ul>     
                     </span>
                </div>
                <?php if(check_member_type()=='Premium'){?>
                    <div class="info-check">
                        <input type="checkbox" value="hide" data-bind="checked:hide_from_user">
                        <span class="info-check-cont">View other profiles anonymously</span>
                        <a href="javascript:void(0);" class="info-save-btn" data-bind="click:manageHideFromViewMe"><i class="fa fa-floppy-o fa-2"></i>Save</a>
                    </div>
                <?php }?>    
                <?php $countryName = country_name($country); $c_name = $countryName['country']; ($c_name != NULL)? $c_name : "Add Your Country"; ?>
                <div class="profile-inner details-title">
                 <div class="post-heading" style="padding:0 0 20px 0; display:inline-block; width:100%;"><h2>Info</h2></div>
                    <div class="clear"></div>
                    <i class="fa fa-map-marker fa-2"></i>
                    <ul class="location-info">
                        <li style="border-right: 1px solid #8e99a3;">
                            <?php $office_name = $office_name==''?'N/A':$office_name;?>
                            <!--<span id="loc"><?php echo $office_name.",".$city.",".$c_name; ?></span>-->
                            
                            <span class="off_name" id="off_name"><?php echo $office_name?></span><a href="#div5" class="edit-sec" id="pr_loc"><i class="fa fa-pencil"></i></a><br />
                            <span class="ct_name" id="ct_name"><?php echo $city ?></span><br />
                            <span class="cntry_name" id="cntry_name"><?php echo $c_name ?></span>
                            
                            
                        </li>
                        <!-- ko foreach: selected_more_address_list -->
                        <li style="border-right: 1px solid #8e99a3;">
                            <!--<span id="loc" data-bind="text:city+','+country_name"></span>-->
                            <span class="off_name" data-bind="text:office_name==''?'N/A':office_name"></span> <a href="#javascript:void(0);" class="edit-sec" data-bind="click:$root.manage_more_address"><i class="fa fa-pencil"></i></a><br />
                            <span class="ct_name" data-bind="text:city"></span><br />
                            <span class="cntry_name" data-bind="text:country_name"></span><br />
                           
                        </li>
                        <!-- /ko -->
                            <a href="#manage_new_address" style="display:none" id="add_new_add_link"></a>                        
                    </ul>
                </div>
                <div class="profile-conntection-left" style="padding-right:0;">
                    <div class="profile-connection-details">
                    <div class="profile-connection-col-left">
                        <div class="profile-connection-col">
                            <h4 class="heading">Industries</h4>
                        </div>
                    </div>
                        <div class="profile-connection-col-right">
                        <ul>
                        <?php foreach($wiz_one_ans as $row){  
                            $code = $row['ans_set_cat_id'];?>
                            <?php $arr = explode('-',$row['ans_set_cat_id']); 
                            $top_line_industry_code = $arr[0];
                            $top_line_industry_name = topline_name($top_line_industry_code);                        
                            $bscode = $arr[1]; 
                            $sectorName = sector_name($bscode); 
                            $img = getImageFromSector($bscode);
                            ?>
                        <li>
                            <h4><?php echo $top_line_industry_name['top_name']; ?><a class="edit-sec" onclick="removeUserDetailFromWizOne('<?php echo $code;?>')"><i class="fa fa-trash"></i></a></h4>
                            <!--<h4><?php echo $sectorName['sector_name']; ?><a class="edit-sec" onclick="removeUserDetailFromWizOne('<?php echo $code;?>')"><i class="fa fa-trash"></i></a></h4>-->
                        </li>
                        <!--<li>
                            <a href="javascript:void(0);" class="cust-profile-detail-img"><img alt="" src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $img[0]['profile_image']; ?>">
                                <a href="javascript:void(0)" class="cust-profile-detail-trash" onclick="removeUserDetailFromWizOne(<?php echo $code;?>)"><i class="fa fa-trash"></i></a>
                                <div class="business-listing-heading">
                                    <h4><?php echo $sectorName['sector_name']; ?></h4>
                                </div>
                            </a>
                        </li>-->
                        <?php } ?>
                        
                        <!--<div class="profile-connection-col">
                            <h4><?php echo $sectorName['sector_name']; ?></h4>
                        </div>-->
                        </ul>
                        </div>
                    </div>
                    <div class="profile-connection-details">   
                    <div class="profile-connection-col-left">                               
                        <div class="profile-connection-col">
                            <h4 class="heading">Sectors</h4>
                        </div>
                        </div>
                        <div class="profile-connection-col-right">
                        <ul>
                        <?php foreach($wiz_one_ans as $row){  ?>
                            <?php $arr = explode('-',$row['ans_set_cat_id']);                             
                            $bscode = $arr[1]; 
                            $sectorName = sector_name($bscode); ?>                      
                            <li>
                                <h4><?php echo $sectorName['sector_name']; ?></h4>
                            </li>                                   
                        <?php } ?>                                
                        <!--<div class="profile-connection-col">
                            <h4>Performing Art</h4>
                        </div>-->
                        </ul>
                        </div>
                    </div>
                    <div class="profile-connection-details">   
                    <div class="profile-connection-col-left">                               
                        <div class="profile-connection-col">
                            <h4 class="heading">Sub-Sectors</h4>
                        </div>
                        </div>
                        <div class="profile-connection-col-right">
                        <ul>
                        <?php foreach($wiz_one_ans as $row){  ?>
                            <?php $arr = explode('-',$row['ans_set_cat_id']); $bscode = $arr[2]; $subSectorName = sub_sector_name($bscode); ?>                      
                            <li>
                                <h4><?php echo $subSectorName['subsector_name']; ?></h4>
                            </li>                                   
                        <?php } ?>                                
                        <!--<div class="profile-connection-col">
                            <h4>Performing Art</h4>
                        </div>-->
                        </ul>
                        </div>
                    </div>
                    <div class="profile-connection-details">
                     <div class="profile-connection-col-left"> 
                        <div class="profile-connection-col">
                            <h4 class="heading">Business Type</h4>
                        </div>
                        </div>
                        <div class="profile-connection-col-right">
                        <ul>
                        <?php foreach($wiz_one_ans as $row){  ?>
                            <?php $arr = explode('-',$row['ans_set_cat_id']); $bscode = $arr[3]; $b = bussiness_name($bscode); ?>                       
                            <li>
                                <h4><?php echo $b['bussiness_name']; ?></h4>
                            </li>
                        <?php } ?>                       
                        <!--<div class="profile-connection-col">
                            <h4>Retailer</h4>
                        </div>-->
                        </ul>
                        </div>
                    </div>
                </div>
                <a class="add-area" href="#add_info_wiz_one" id="add_info_wiz_one_link" onclick="OpenModalForAddAreaOfWork()">Add business type</a>
                <a class="add-area" href="#improve_match" id="improve_match_link" onclick="OpenWixOneModalForImproveMatch()">Improve your match</a>                
                <a class="add-area" href="#change_password" id="change_password_link">Change password</a>
                <a class="add-area" href="javascript:void(0);" onclick="checkValidateBeforeUpload()">Upload document</a>
                <a class="add-area" style="display:none" href="#upload_document" id="upload_document_link"></a>
                <a class="add-area" href="javascript:void(0);" data-bind="visible:selected_more_address_list().length<4,click:resetpopupForm">Add Office Location</a>
            </div>
            
            <div class="compose-inner-bg profile-new-bg member-profile-bg member-profile-bg">
                <div class="profile-inner member-inner">
                    <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Business Summary</h2></div>  
                    <!--<a href="#manage_buisness_description" class="edit-sec FT-RT" id="manage_buisness_description_link"><i class="fa fa-pencil"></i></a>                                                            -->
                    <a href="javascript:void(0);" class="edit-sec FT-RT" data-bind="visible:show_biz_summary,click:viewEditBizSummary"><i class="fa fa-pencil"></i></a>                                                            
                    <p data-bind="visible:show_biz_summary" id='pr_desc'><?php echo $type = ($bussinessdesc == null) ? "Add description" : $bussinessdesc; ?></p>                                        
                    <textarea id="edit_buiz_description" data-bind="visible:show_edit_biz_summary"><?php echo $bussinessdesc;?></textarea>  
                    <button type="button" data-bind="click:update_buiz_desc,visible:show_edit_biz_summary" class="button" id="manage_buiz_desc_update" >Save</button> 
                    <?php if(!empty($doc_uploaded)){?>
                          <div class="post-inner business-summary-inner all-post">
                            <ul>
                                <?php $count =0;?>
                                <?php foreach($doc_uploaded as $doc){
                                    $ext = pathinfo($doc['file_name'], PATHINFO_EXTENSION);
                                    if($ext=='jpg'|| $ext == 'png' || $ext == 'jpeg' || $ext == 'gif' || $ext == 'bmp'){?>
                                        <li>                                            
                                            <a class="video_popup" href="#image_<?php echo $count;?>" >
                                                <img class="example-image" src="<?php echo base_url().$doc['path'].$doc['file_name']?>" alt="image-1" />
                                            </a>
                                            <a href="javascript:void(0)" class="delet" onclick="deleteDocument(<?php echo $doc['doc_id']?>,'<?php echo $doc['file_name'] ?>')"><i class="fa fa-trash-o f2"></i></a>
                                            <div id="image_<?php echo $count;?>" class="edit-profile document-img-open" style="display:none">
                                                <div class="popup-doc-title"><label style="float:left">Title</label><input type="text" value="<?php echo $doc['file_title']?>"/><button onclick="updateTitleForDoc(this,<?php echo $doc['doc_id']?>);">Update</button></div>
                                                <img class="example-image" src="<?php echo base_url().$doc['path'].$doc['file_name']?>" alt="image-1" />
                                            </div>
                                            <div class="doc-title-wrap"><div class="doc-title-inner" id="update_title_<?php echo $doc['doc_id'];?>"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div></div>
                                        </li> 
                                    <?php }else if($ext=='mp4'){?> 
                                        <li>
                                            <a class="video_popup" href="#video_<?php echo $count;?>" >
                                                <img src="<?php echo base_url()?>assets/images/video-icon.png">
                                            </a>
                                            <a href="javascript:void(0)" class="delet" onclick="deleteDocument(<?php echo $doc['doc_id']?>,'<?php echo $doc['file_name'] ?>')"><i class="fa fa-trash-o f2"></i></a>
                                            <div class="doc-title-wrap"><div class="doc-title-inner" id="update_title_<?php echo $doc['doc_id'];?>"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div></div>                                             
                                        </li>
                                        <div id="video_<?php echo $count;?>" class="edit-profile video-wrap" style="display:none">       
                                            <div class="popup-doc-title"><label style="float:left">Title</label><input type="text" value="<?php echo $doc['file_title']?>"/><button onclick="updateTitleForDoc(this,<?php echo $doc['doc_id']?>);">Update</button></div>
                                            <video width="700" controls>
                                            <source src="<?php echo base_url().$doc['path'].$doc['file_name']?>" type="video/mp4">  
                                            Your browser does not support HTML5 video.
                                            </video>
                                        </div>
                                    <?php }else if($ext=='pptx' || $ext=='pdf'){?>
                                        <li>
                                            <a class="video_popup" href="#video_<?php echo $count;?>" >
                                                <?php if($ext=='pptx'){ ?>
                                                    <img src="<?php echo base_url()?>assets/images/ppt-file.png">
                                                <?php }else{ ?>
                                                    <img src="<?php echo base_url()?>assets/images/pdf-file.png">
                                                <?php } ?> 
                                            </a>
                                            <a href="javascript:void(0)" class="delet" onclick="deleteDocument(<?php echo $doc['doc_id']?>,'<?php echo $doc['file_name'] ?>')"><i class="fa fa-trash-o f2"></i></a>
                                            <div class="doc-title-wrap"><div class="doc-title-inner" id="update_title_<?php echo $doc['doc_id'];?>"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div></div>
                                        </li>  
                                        <div id="video_<?php echo $count;?>" class="edit-profile video-wrap" style="display:none">       
                                            <div class="popup-doc-title"><label style="float:left">Title</label><input type="text" value="<?php echo $doc['file_title']?>"/><button onclick="updateTitleForDoc(this,<?php echo $doc['doc_id']?>);">Update</buttom></div>
                                            <iframe src="http://docs.google.com/gview?url=<?php echo base_url().$doc['path'].$doc['file_name']?>&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>
                                        </div>                                        
                                    <?php }else{?>
                                    <li>
                                        <a href="<?php echo base_url().$doc['path'].$doc['file_name']?>" target="_blank"><div class="document-icon"><img src="<?php echo base_url()?>assets/images/other-icon.png"></div></a>                      
                                        <a href="javascript:void(0)" class="delet" onclick="deleteDocument(<?php echo $doc['doc_id']?>,'<?php echo $doc['file_name'] ?>')"><i class="fa fa-trash-o f2"></i></a>
                                        <div class="doc-title-wrap"><div class="doc-title-inner" id="update_title_<?php echo $doc['doc_id'];?>"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div></div>
                                    </li>    
                                    <?php }?>
                                    <?php $count++;?>
                                <?php }?>
                            </ul>
                        </div>
                      <?php }else{?>
                          <div style="color:#000 !important;font-size:10pt;font-weight:600"></div>
                      <?php }?>
                </div>
                </div>
            </div>
            <div class="compose-inner-bg profile-new-bg member-profile-bg">
                <div class="profile-inner member-inner">
                    <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>About me</h2></div> 
                    <!--<a href="#manage_about_me" class="edit-sec FT-RT" id="manage_about_me_link"><i class="fa fa-pencil"></i></a>-->
                    <a href="javascript:void(0);" class="edit-sec FT-RT" data-bind="visible:show_about_me,click:viewEditAboutMe"><i class="fa fa-pencil"></i></a>
                    <p data-bind="visible:show_about_me" id='pr_about_me_desc'><?php echo $type = ($about_me == '') ? "Add your view" : $about_me; ?></p>                                       
                    
                    <textarea id="edit_about_you_description" data-bind="visible:show_edit_about_me"><?php echo $about_me;?></textarea>  
                    <button type="button" data-bind="click:update_about_you_desc,visible:show_edit_about_me" class="button" id="manage_about_you_desc_update" >Save</button> 
                </div>
            </div> 
			<div class="compose-inner-bg profile-new-bg">
                <div class="profile-inner">
                    <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Tags</h2></div>                    
                    <a href="#manage_tag" class="edit-sec FT-RT" id="manage_tag_link"><i class="fa fa-pencil"></i></a>
					<p data-bind="visible:selected_tags_list().length==0">Add Your Tags</p>
                    <p id='pr_tags' data-bind="foreach:selected_tags_list,visible:selected_tags_list().length>0"><span style="color: #636a6f; font-weight:600;font-size:12pt; margin-right:1%; padding: 4px" data-bind="text:tag_name"></span></p>                                       
                </div>
            </div>
            <div class="compose-inner-bg profile-new-bg">
                <div class="profile-inner">
                    <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Business objectives</h2></div>
                    <div class="profile-connection-inner MAR-TOP">
                        <div class="profile-conntection-left MAR-TOP">
                            <div class="profile-connection-details">
                                <div class="profile-connection-col">
                                    <h4 class="heading">What are you looking for ?</h4>
                                </div>
                                <div class="profile-connection-col"><a class="edit-sec" href="#manage_looking" id="manage_looking_link"><i class="fa fa-pencil"></i></a>
                                    <?php foreach($questionone as $row){ ?>
                                    <h4><?php echo $row; ?></h4>
                                    <?php } ?>
                                    <!--<h4>Find suppliers Look for non-executive directors <a class="edit-sec"><i class="fa fa-pencil"></i></a> </h4>-->
                                </div>
                            </div>
                            <div class="profile-connection-details">
                                <div class="profile-connection-col">
                                    <h4 class="heading">Where do you want to explore?</h4>
                                </div>
                                <div class="profile-connection-col"><a class="edit-sec" href="#manage_locality_explore" id="manage_locality_explore_link"><i class="fa fa-pencil"></i></a>
                                      <?php foreach($locality_to_explore as $row){ ?>
                                      <h4><?php echo $row->locality_type; ?></h4>
                                      <?php } ?>
                                    <!--<h4>Global <a class="edit-sec"><i class="fa fa-pencil"></i></a> </h4>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="compose-inner-bg profile-new-bg">
                <div class="profile-inner">
                    <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Who are your customers?</h2></div>
                    <div class="profile-connection-inner MAR-TOP who-customer">
                        <div class="profile-conntection-left MAR-TOP" style="padding-right:0;">
                            <div class="profile-connection-details">
                                <div class="profile-connection-col-left">
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Customer Type</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">                               
                                    <ul>                                   
                                    <?php foreach($customer_type as $row){ ?>                                     
                                        <li style="border:none; min-height:inherit; padding-bottom:0;">
                                            <h4><?php echo $row->service_name; ?> </h4>
                                            <a class="edit-sec" href="#manage_customer_type" id="manage_customer_type_link"><i class="fa fa-pencil"></i></a>  
                                        </li>
                                    <?php } ?>
                                   </ul>
                                </div>
                            </div>
                             <div class="profile-connection-details">
                                <div class="profile-connection-col-left">
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Industries</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">                                 
                                    <ul>
                                    <?php foreach($wiz_two_quiz_four_and_five_selected_biz_ids as $industry){?>
                                        <li>
                                            <h4><?php echo $industry['industry_name']?><a class="edit-sec" onclick="removeUserDetail('industry_code',<?php echo $industry['industry_code']?>)"><i class="fa fa-trash"></i></a></h4>
                                        </li>
                                    <?php }?>
                                    </ul>
                                </div>
                            </div>
                            <div class="profile-connection-details">
                                <div class="profile-connection-col-left">
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Sectors</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">                                 
                                 <ul>
                                <?php foreach($wiz_two_quiz_four_and_five_selected_biz_ids as $target_sector){?>  
                                  <li>
                                      <h4><?php echo $target_sector['tar_sector_name']?></h4>
                                  </li>                               
                                <?php }?>
                                </ul>
                                </div>
                            </div>
                            <div class="profile-connection-details">
                                <div class="profile-connection-col-left">
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Function you serve</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">                                                                
                                    <ul>
                                    <?php foreach($looking_service as $row){ ?>
                                        <li style="border:none; min-height:inherit; padding-bottom:0;">
                                            <h4><?php echo $row->look_service_name; ?> </h4>
                                            <a class="edit-sec" href="#manage_function_you_serve" id="manage_function_you_serve_link"><i class="fa fa-pencil"></i></a>  
                                        </li>                               
                                    <?php }?>
                                    </ul>
                                </div>
                            </div>                           
                        </div>
                    </div>
                </div>
            </div>

            
            <!-- ----------------- -->

        </div>  
        <div id="manage_tag" style="width:600px; display:none; height:270px;">
        <div class="popup-box-part-center" >           
            <h2>Manage Tags</h2>
            <!--<button type="button" data-bind="click:update_tag" class="button" id="" >Save</button> -->  
            <div class="business-img-listing-wrap cust-fancy-label-input manage-tag">                
                <select data-bind="options: available_tags, selectedOptions: chosen_tags,optionsText: 'tag_name', optionsValue: 'tag_code'" id="tags" size="5" multiple="true"></select>
                
            </div>    
            <div style="clear:both;"></div>
            <button type="button" data-bind="click:update_tag" class="button" id="" style="background:#ed5463; margin:15px 0 0 0;" >Save</button>   
        </div>
    </div>     

    <div id="manage_new_address" class="edit-profile" style="display:none">       
        <input type="text" name="more_office_name" id="more_office_name" data-bind="value:more_office_name">            
        <input type="text" name="more_city_name" id="more_city_name" data-bind="value:more_city_name">            
        <select  name="select1" id="more_country_list"data-bind="options: country_list,optionsText: 'country_name',optionsValue: 'id',value: selected_country,optionsCaption: '- Select Country -'"></select>
        <div style="clear:both;"></div>
        <input type="button" value="Save" data-bind="click:manageMoreAddressSaveButton">
        <input type="button" value="Delete" data-bind="click:manageMoreAddressDeleteButton">
    </div> 
           
    <div id="change_password" style="width:400px; display:none;">
        <div class="popup-box-part-center">
            <form id="change_password_form" method="post">
                <h2>Manage password</h2>
                <div class="business-img-listing-wrap cust-fancy-label-input">
                    <div class="cust-fancy-label-input">
                        <label>Current password</label>
                        <input type="password" name="current_password" id="current_password" data-bind="value:current_password"> 
                    </div>
                    <div class="cust-fancy-label-input"> 
                        <label>Password</label>
                        <input type="password" name="password" id="password" data-bind="value:password"> 
                    </div> 
                    <div class="cust-fancy-label-input"> 
                        <label>Confirm password</label>
                        <input type="password" name="confirm_password" id="confirm_password" data-bind="value:confirm_password"> 
                    </div>   
                    <button type="submit" class="button" id="manage_profile_update" >Save</button> 
                </div>      
            </form>    
        </div>
    </div>
    <div id="manage_customer_type" style="width:600px; display:none; height:280px;">
        <div class="popup-box-part-center">           
            <h2>What is your customer type?</h2>
            <div class="business-img-listing-wrap cust-fancy-label-input lookin-for-u">
                <div class="business-img-listing-wrap">
                    <ul class="business-content-listing service_list" data-bind="foreach:service_list">          
                        <li onclick="setActive(this)" data-bind="attr:{id:'cus_'+service_id},click:$root.setService"><a href="javascript:void(0);" data-bind="text:service_name"> #1Lorem ipsum</a><input type="hidden" data-bind="value:service_id"></li>                                        
                    </ul>  
                </div>          
            </div> 
            <div style="clear:both;"></div>
            <button type="button"  class="button" data-bind="click:editCustomerType" style="background:#ed5463; margin:20px 0 0 0;" >Save</button>                                           
        </div>
    </div>
    <div id="manage_function_you_serve" style="width:650px; display:none; height:300px;">
        <div class="popup-box-part-center">           
            <h2>What sort of buisnesses do your service?</h2>
            <div class="business-img-listing-wrap cust-fancy-label-input lookin-for-u">
                <div class="business-img-listing-wrap">
                    <ul class="business-content-listing biz_serve_list" data-bind="foreach:biz_serve_list">          
                        <li onclick="setActive(this)" data-bind="attr:{id:'bizu_'+look_service_id},click:$root.setBizServe"><a href="javascript:void(0);" data-bind="text:look_service_name"> #1Lorem ipsum</a></li>                                        
                    </ul>  
                </div>          
            </div> 
            <div style="clear:both;"></div>
            <button type="button"  class="button" data-bind="click:editBuiznessService" style="background:#ed5463; margin:20px 0 0 0;" >Save</button>                                           
        </div>
    </div>
    <div id="manage_looking" style="width:600px; display:none; height:280px;">
        <div class="popup-box-part-center">           
            <h2>What are you looking for?</h2>
            <div class="business-img-listing-wrap cust-fancy-label-input lookin-for-u">
                <div class="business-img-listing-wrap">
                    <ul class="business-img-listing looking_for_list" data-bind="foreach:looking_for_list">          
                        <li onclick="setActive(this)" data-bind="attr:{id:looking_for_id},click:$root.setLookingForList">
                            <a href="javascript:void(0);" data-bind="value:looking_for_id"><img style="opacity:0" src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>
                                <div class="business-listing-heading">
                                    <h4 data-bind="text:looking_for_name">Capital Market</h4>
                                </div>
                            </a>
                        </li>                   
                    </ul>
                </div>          
            </div> 
            <div style="clear:both;"></div>
            <button type="button"  class="button" data-bind="click:editLookingFor" style="background:#ed5463; margin:20px 0 0 0;" >Save</button>                                           
        </div>
    </div>
    <div id="manage_locality_explore" style="width:600px; display:none;">
        <div class="popup-box-part-center">           
            <h2>Where do you want to explore?</h2>
            <div class="business-img-listing-wrap cust-fancy-label-input">
                <div class="business-img-listing-wrap">
                <div class="business-img-listing-wrap">
                  <ul class="business-content-listing locality_list" data-bind="foreach:locality_list">          
                    <li onclick="setActive(this)" data-bind="attr:{id:'loc_'+locality_id},click:$root.selLocalityList"><a href="javascript:void(0);" data-bind="value:locality_id,text:locality_type"> #1Lorem ipsum</a></li>
                      <!--<li data-bind="value:locality_id,text:locality_type">#1Lorem ipsum</li>-->
                  </ul>  
                </div> 
              </div>          
            </div> 
            <div style="clear:both;"></div>
               <button type="button"  class="button" data-bind="click:editLocality" style="background:#ed5463; margin:20px 0 0 0;" >Save</button>                                             
        </div>
    </div>
</div>
    
    
    <div id="upload_document" class="upload-document-wrap" style="width:600px; display:none;">
        <div class="popup-box-part-center">           
               <!-- <h2>Upload Document</h2>-->
                <div class="business-img-listing-wrap cust-fancy-label-input">
                    <div id="formdiv">
                    <form enctype="multipart/form-data" id="upload_document_form"  method="post">
                    
                        <div id="filediv" class="document-open">
                            <input name="file[]" type="file" id="file"/>
                            <input name="file_title[]" type="text" palceholder="File Title"/>
                        </div><br/>
                        <div class="document-submit-btn">                        
                        <input type="button" id="add_more" class="add_more" value="Add more files"/>
                        <input type="submit" value="Upload file" name="submit" id="upload" class="upload"/>
                        </div>
                    </form>
                    <br/>
                    <br/>
                    </div>                    
                </div>   
        </div>
    </div>
	
    
    
     



<!-- -------------fancy box div-------------------- --> 

<div id="div3" class="edit-profile" style="display:none"></div>
<div id="div4" class="edit-profile" style="display:none">

</div>

<div id="div5" class="edit-profile" style="display:none">
    <input type="text" name="office_name" id="office_name">
    <input type="text" name="city_name" id="city_name">
    <?php $country_list = getCountryList(); ?>
    <select name="country_name" id="country_name">
        <?php foreach($country_list as $c){ ?>
            <option value="<?php echo $c->id; ?>"><?php echo $c->country_name; ?></option>
        <?php } ?>
    </select> 
    <div style="clear:both;"></div>
    <input type="button" value="Submit" id="pr_loc_submit">
</div>


<div id="div6" class="edit-profile" style="display:none"></div>
<div id="div7" class="edit-profile" style="display:none"></div>

<style>
.select2-container{z-index:9999;}
</style>

<script type="text/javascript">  
  var checkUserType = '<?php echo check_member_type();?>';  
  var vmManageProfile ={
    first_name:ko.observable('<?php echo $fname;?>'),
    last_name:ko.observable('<?php echo $lname;?>'),
    email:ko.observable('<?php echo $email;?>'),
    tag_line:ko.observable('<?php echo $tag_line;?>'),    
    looking_for_list:ko.observableArray(),
    selected_looking_for_list:ko.observableArray(),
    service_list:ko.observableArray(),
	available_tags:ko.observableArray(),
    selected_locality_list:ko.observableArray(),
    selected_service:ko.observableArray(),
    sel_biz_serve_list:ko.observableArray(),
	chosen_tags:ko.observableArray(),
	selected_tags_list:ko.observableArray(),
    biz_serve_list:ko.observableArray(),
    locality_list:ko.observableArray(),
    current_password:ko.observable(''),
    password:ko.observable(''),
    confirm_password:ko.observable(''),
    show_name:ko.observable(true),
    edit_name:ko.observable(false),
    show_email:ko.observable(true),
    edit_email:ko.observable(false),
    show_biz_summary:ko.observable(true),
    show_edit_biz_summary:ko.observable(false),
    show_about_me:ko.observable(true),
    show_edit_about_me:ko.observable(false),
    show_tag_line:ko.observable(true),
    edit_tag_line:ko.observable(false),
    country_list:ko.observableArray(),
    more_city_name:ko.observable(''),
    more_office_name:ko.observable(''),
    more_city_id:ko.observable(''),
    selected_country:ko.observable(''),
    hide_from_user:ko.observable(false),
    setService:function(){
         var sel_ser=this;
         var is_removed = 0;       
        if(vmManageProfile.selected_service().length>0){       
          for(var i=0;i<vmManageProfile.selected_service().length;i++){          
             if(vmManageProfile.selected_service()[i].service_id==sel_ser.service_id) {            
              vmManageProfile.selected_service.remove(function (item) {
                if(sel_ser.service_id == item.service_id){      
                  is_removed++;          
                  return true;
                } 
              });
             }
          }
          if(is_removed==0)
            vmManageProfile.selected_service.push(sel_ser);
        }else{
          vmManageProfile.selected_service.push(this);
        }   
    },
    setBizServe:function(){
        var sel_biz_serve_item=this;
        var is_removed = 0; 
        if(vmManageProfile.sel_biz_serve_list().length>0){       
          for(var i=0;i<vmManageProfile.sel_biz_serve_list().length;i++){          
             if(vmManageProfile.sel_biz_serve_list()[i].look_service_id==sel_biz_serve_item.look_service_id) {            
              vmManageProfile.sel_biz_serve_list.remove(function (item) {
                if(sel_biz_serve_item.look_service_id == item.look_service_id){      
                  is_removed++;          
                  return true;
                } 
              });
             }
          }
          if(is_removed==0)
            vmManageProfile.sel_biz_serve_list.push(sel_biz_serve_item);
        }else{
          vmManageProfile.sel_biz_serve_list.push(sel_biz_serve_item);
        }      
    },
    resetpopupForm:function(){
        //alert(checkUserType);more_office_name
        if(checkUserType!='Beta'){
            vmManageProfile.more_city_name('');
            vmManageProfile.more_office_name('');
            vmManageProfile.selected_country('');
            vmManageProfile.more_city_id('');
            $('#add_new_add_link').trigger("click");
        }else{
            //reset();
            alertify.set({ labels: { ok: "Accept", cancel: "Deny" } });
            alertify.confirm("To add more locations and improve you matching reach, you need to be a Premium member. Sign up for free trial today", function (e) {
                if (e) {
                    window.location = "<?php echo base_url()?>payment/paymentoptiondetails";
                    //alertify.success("You've clicked OK");
                } 
            });
            return false;
            //alertify.alert("To add more locations and improve you matching reach, you need to be a Premium member. Sign up for free trial today");
        }
    },
    selected_more_address_list:ko.observableArray(),
    manage_more_address:function(){      
        vmManageProfile.more_city_name(this.city);
        vmManageProfile.more_office_name(this.office_name);
        vmManageProfile.selected_country(this.country);
        vmManageProfile.more_city_id(this.loc_id);
        $('#more_country_list').trigger('change');
        $('#add_new_add_link').trigger("click");
    },
    manageMoreAddressDeleteButton:function(){
        //console.log(this.more_city_id());
        var location_id = this.more_city_id();
        alertify.confirm("Are you want to remove the address", function (e) {
            if (e) {
                $("#loader-wrapper").show();
                $.post('<?php echo base_url(); ?>memberprofile/manageMoreAddressDelete',{loc_id:location_id},function(result_data){
                    $("#loader-wrapper").hide();
                    if(result_data==1){
                          vmManageProfile.selected_more_address_list.remove(function (item) {
                            if(location_id == item.loc_id){    
                              return true;
                            } 
                          });
                        $(".fancybox-close").trigger("click");
                        alertify.success("Address successfully removed");
                    }else{
                        alertify.alert("Some error occured please try again later");
                    }
                });
            }
        });
    },
    manageMoreAddressSaveButton:function(){        
        if(vmManageProfile.more_city_name()!=''){
            if(vmManageProfile.selected_country()==undefined){alertify.alert("Please select a country"); return false}
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>memberprofile/manageMoreAddressSave',{id:vmManageProfile.more_city_id(),office_name:vmManageProfile.more_office_name(),city_name:vmManageProfile.more_city_name(),country_code:vmManageProfile.selected_country()},function(result){ 
                $("#loader-wrapper").hide();
                if(result==1){
                    $('.fancybox-close').trigger('click');  
                    alertify.success("Successfully updated");
                    getMoreAddress();
                }else{
                    alertify.error("Some error occured please try again later")
                }
            });
        }else{
            alertify.alert('Please enter the city name!');
        }
    },
    viewEditTagLine:function(){
        vmManageProfile.edit_tag_line(true);
        vmManageProfile.show_tag_line(false);
    },
    manageHideFromViewMe:function(){
        alertify.confirm(vmManageProfile.hide_from_user()==true?"Are you sure you want to disable anonymous browsing?":"Are you sure you want to enable anonymous browsing?", function (e) {
            if (e) {
                $("#loader-wrapper").show();
                $.post('<?php echo base_url(); ?>memberprofile/manageHideFromViewMe',{status:vmManageProfile.hide_from_user()==true?1:0},function(result_data){
                    $("#loader-wrapper").hide();
                    if(result_data==1){
                        alertify.success("Your request has been successfully saved");
                    }else{
                        alertify.error("Some error occured please try again later");
                    }
                });
            }
        });
    },
    viewTagLine:function(){
        vmManageProfile.show_tag_line(true);
        vmManageProfile.edit_tag_line(false);
    },
    viewName:function(){
        vmManageProfile.show_name(true);
        vmManageProfile.edit_name(false);
    },
    viewEditName:function(){
        vmManageProfile.show_name(false);
        vmManageProfile.edit_name(true);
    },
    viewEmail:function(){
        vmManageProfile.show_email(true);
        vmManageProfile.edit_email(false);
    },
    viewEditEmail:function(){
        vmManageProfile.show_email(false);
        vmManageProfile.edit_email(true);
    },
    viewEditBizSummary:function(){
        vmManageProfile.show_biz_summary(false);
        vmManageProfile.show_edit_biz_summary(true);
    },
    viewBizSummary:function(){
        vmManageProfile.show_edit_biz_summary(false);
        vmManageProfile.show_biz_summary(true);
    },
    viewEditAboutMe:function(){
        vmManageProfile.show_about_me(false);
        vmManageProfile.show_edit_about_me(true);
    },
    viewAboutMe:function(){
        vmManageProfile.show_edit_about_me(false);
        vmManageProfile.show_about_me(true);
    },
	update_tag:function(){
		console.log(vmManageProfile.chosen_tags());
		$.post('<?php echo base_url(); ?>memberprofile/updateTag',{selected_tags_ids:vmManageProfile.chosen_tags()},function(result_data){ 
			console.log(result_data);   
			if(result_data==1){
				$('.fancybox-close').trigger('click');  
				alertify.success("Successfully updated");
				try{
					$("#loader-wrapper").show();
					$.post('<?php echo base_url(); ?>memberprofile/getSelectedTags',{},function(result){ 
					$("#loader-wrapper").hide();
						result = eval("(" + result + ")");				
						var select_arr = [];
						vmManageProfile.selected_tags_list(result);				
						for(var i =0;i<result.length;i++){
							select_arr.push(result[i].tag_code);
						}
						vmManageProfile.chosen_tags(select_arr);
						$("#tags").trigger('change');
					});		
				}catch(e){
				  alert(e);
				}
				//location.reload();
			}else{
			alertify.error("This is embrassing.Something error occured.Please try again!");
			}      
		});
	},
    editLocality:function(){
        if(vmManageProfile.selected_locality_list().length>0){          
                $.post('<?php echo base_url(); ?>memberprofile/editLocality',{locality_ids:vmManageProfile.selected_locality_list()},function(result_data){ 
                  //console.log(result_data);   
                  if(result_data==1){
                    $('.fancybox-close').trigger('click');  
                    alertify.success("successfully updated");
                    location.reload();
                  }else{
                    alertify.error("This is embrassing.Something error occured.Please try again!");
                  }      
                });
        }else{
          alert('Please select category to proceed');
        }
    },
    selLocalityList:function(){ 
        //debugger;
        var sel_locality_item=this;
        var is_removed = 0; 
        if(vmManageProfile.selected_locality_list().length>0){       
          for(var i=0;i<vmManageProfile.selected_locality_list().length;i++){          
             if(vmManageProfile.selected_locality_list()[i].locality_id==sel_locality_item.locality_id) {            
              vmManageProfile.selected_locality_list.remove(function (item) {
                if(sel_locality_item.locality_id == item.locality_id){      
                  is_removed++;          
                  return true;
                } 
              });
             }
          }
          if(is_removed==0)
            vmManageProfile.selected_locality_list.push(sel_locality_item);
        }else{
          vmManageProfile.selected_locality_list.push(sel_locality_item);
        }      
      },
    editCustomerType:function(){
        if(vmManageProfile.sel_biz_serve_list().length>0){          
                $.post('<?php echo base_url(); ?>memberprofile/editService',{service_ids:vmManageProfile.selected_service()},function(result_data){ 
                  //console.log(result_data);   
                  if(result_data==1){
                    $('.fancybox-close').trigger('click');  
                        alertify.success("successfully updated");
                        location.reload();
                  }else{
                    alertify.error("This is embrassing.Something error occured.Please try again!");
                  }      
                });
        }else{
          alert('Please select customer type');
        }
    },
    editBuiznessService:function(){
        if(vmManageProfile.sel_biz_serve_list().length>0){          
                $.post('<?php echo base_url(); ?>memberprofile/editBuiznessService',{buisness_service_ids:vmManageProfile.sel_biz_serve_list()},function(result_data){ 
                  //console.log(result_data);   
                  if(result_data==1){
                    $('.fancybox-close').trigger('click');  
                        alertify.success("successfully updated");
                        location.reload();
                  }else{
                    alertify.error("This is embrassing.Something error occured.Please try again!");
                  }      
                });
        }else{
          alert('Please select buisness you serve');
        }
    },
    editLookingFor:function(){
        //console.log(vmManageProfile.selected_looking_for_list());
        //return false;
        if(vmManageProfile.selected_looking_for_list().length>0){          
                $.post('<?php echo base_url(); ?>memberprofile/editLookingFor',{looking_for_ids:vmManageProfile.selected_looking_for_list()},function(result_data){ 
                  //console.log(result_data);   
                  if(result_data==1){
                    $('.fancybox-close').trigger('click');  
                    alertify.success("successfully updated");
                    location.reload();
                  }else{
                    alertify.error("This is embrassing.Something error occured.Please try again!");
                  }      
                });
        }else{
          alert('Please select category to proceed');
        }
    },
    setLookingForList:function(){
        //console.log(this);
        var sel_looking_item=this;
        var is_removed = 0;       
        if(vmManageProfile.selected_looking_for_list().length>0){       
          for(var i=0;i<vmManageProfile.selected_looking_for_list().length;i++){          
             if(vmManageProfile.selected_looking_for_list()[i].looking_for_id==sel_looking_item.looking_for_id) {            
              vmManageProfile.selected_looking_for_list.remove(function (item) {
                if(sel_looking_item.looking_for_id == item.looking_for_id){      
                  is_removed++;          
                  return true;
                } 
              });
             }
          }
          if(is_removed==0)
            vmManageProfile.selected_looking_for_list.push(sel_looking_item);
        }else{
          vmManageProfile.selected_looking_for_list.push(sel_looking_item);
        }          
      },
    //buisness_decription:ko.observable(),
    update_buiz_desc:function(){        
        var update_biz_desc = $('#edit_buiz_description').val();
        if(update_biz_desc!=''){
            $("#loader-wrapper").show();
            update_biz_desc = update_biz_desc.replace(/(?:\r\n|\r|\n)/g, '<br />');
            $.post('<?php echo base_url(); ?>memberprofile/updateBizDescription',{biz_description:update_biz_desc},function(result){
                if(result=='1'){
                    $("#loader-wrapper").hide();
                    //vmManageProfile.buisness_decription(update_biz_desc);
                    //$('#edit_buiz_description').val(update_biz_desc);
                    $('#pr_desc').html('');
                    $('#pr_desc').html(update_biz_desc);
                   // $('.fancybox-close').trigger('click');    
                    alertify.success("Buisness Decsription successfully updated!");  
                    vmManageProfile.viewBizSummary();        
                }
                else{
                    $("#loader-wrapper").hide();
                    alertify.error("Some error occured please try again!");
                }
            });
        }else{
            $("#loader-wrapper").hide();
            alertify.error("Buisness Description can't be empty");
        }
    },
    update_about_you_desc:function(){
        var update_about_you_desc = $('#edit_about_you_description').val();
        if(update_about_you_desc!=''){
            $("#loader-wrapper").show();
            update_about_you_desc = update_about_you_desc.replace(/(?:\r\n|\r|\n)/g, '<br />');
            $.post('<?php echo base_url(); ?>memberprofile/updateAboutMeDescription',{about_me_description:update_about_you_desc},function(result){
                if(result=='1'){
                    $("#loader-wrapper").hide();
                    //vmManageProfile.buisness_decription(update_biz_desc);
                    //$('#edit_about_you_description').val(update_about_you_desc);
                    $('#pr_about_me_desc').html('');
                    $('#pr_about_me_desc').html(update_about_you_desc);
                    //$('.fancybox-close').trigger('click');    
                    vmManageProfile.viewAboutMe();
                    alertify.success("Views successfully updated!");            
                }
                else{
                    $("#loader-wrapper").hide();
                    alertify.error("Some error occured please try again!");
                }
            });
        }else{
            $("#loader-wrapper").hide();
            alertify.error("Buisness Description can't be empty");
        }
    }
  }
  
   $(".post-inner ul li a img").load(function() {
		var max_size = 180;
		$(".post-inner ul li a img").show();
		$(".post-inner ul li a img").each(function(i) {
		  if ($(this).height() > $(this).width()) {
			var h = max_size;
			var w = Math.ceil($(this).width() / $(this).height() * max_size);
		  } else {
			var w = max_size;
			var h = Math.ceil($(this).height() / $(this).width() * max_size);
		  }
		  $(this).css({ height: h, width: w });
		});
   });
  function updateTitleForDoc(cur_obj,doc_id){
        var parent = $(cur_obj).parent();
        var update_title = $(parent).find('input[type=text]').val();
        if(update_title!=''){
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>memberprofile/updateTitleForDoc',{update_title:update_title,doc_id:doc_id},function(result_data){
                $("#loader-wrapper").hide();
                if(result_data==1){
                   alertify.alert("Successfully updated");
                  // debugger;
                   $('#update_title_'+doc_id).html(update_title);
                }else{
                    alertify.error("Some error occured.Please try again later");
                }
            });
        }else{
            alertify.alert("Please enter title for the document");
        }
  }
  //--------------------------
  var abc = 0;
  function checkValidateBeforeUpload(){     
    if(checkUserType !='Premium'){
        $("#loader-wrapper").show();
        $.post('<?php echo base_url(); ?>memberprofile/validateUploadFile',{},function(result_data){
            $("#loader-wrapper").hide();
            if(result_data<5){
                $('#upload_document_link').trigger('click');
            }else{
                alertify.set({ labels: { ok: "Accept", cancel: "Deny" } });
                alertify.confirm("As a free member you can only upload 5 files. To add more files and improve your market reach, you need to be a Premium member. Sign up for free trial today.", function (e) {
                    if (e) {
                        window.location = "<?php echo base_url()?>payment/paymentoptiondetails";
                        //alertify.success("You've clicked OK");
                    } 
                });
                return false;
                //alertify.error('As a free member you can only upload 5 files. Upgrade to premium membership to upload an unlimited documents.');
            }
        });
    }else{
        $('#upload_document_link').trigger('click');
    }

  }
  function deleteDocument(doc_id,doc_name){
    alertify.confirm("Are you sure you'd like to remove '"+doc_name+"'", function (e) {
        if (e) {
             $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>memberprofile/deleteDocument',{doc_id:doc_id},function(result){
                $("#loader-wrapper").hide();
                if(result==1){
                    location.reload();
                }else{
                    alertify.error("Some error occured.Please try again later");
                }
            });
        } 
    });
  }
  function getMoreAddress(){
    $("#loader-wrapper").show();
    $.post('<?php echo base_url(); ?>memberprofile/getMoreAddress',{},function(result){
        $("#loader-wrapper").hide();
        result = eval("(" + result + ")");       
        vmManageProfile.selected_more_address_list(result);
    });
  }
$(document).ready(function(){
    var type = '<?php echo check_member_type()?>';
    getMoreAddress();
    try{
        $.post('<?php echo base_url(); ?>memberprofile/getVisibleStatus',{},function(result){
            if(result==1){
                vmManageProfile.hide_from_user(true);
            }else{
                vmManageProfile.hide_from_user(false);
            }
        });
    }catch(e){
        alert(e);
    } 
    //To add new input file field dynamically, on click of "Add More Files" button below function will be executed    
    $('#add_more').click(function() {        
        var checkUserType = '<?php echo check_member_type();?>'; 
        var cur_obj = this;
        if(checkUserType !='Premium'){
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>memberprofile/validateUploadFile',{},function(result){  
                //debugger;
                var no_file_added = $(".abcd").length;
                var total = parseInt(result)+parseInt(no_file_added);
                $("#loader-wrapper").hide();              
                if(total<5){                    
                    $(cur_obj).parent().before($("<div/>", {id: 'filediv',class:'document-open'}).fadeIn('slow').append(
                            $("<input/>", {name: 'file[]', type: 'file', id: 'file'}), 
                            $("<input/>",{name:'file_title[]',type:'text',placeholder:'File Title'}),
                            $("<br/>")
                    ));
                }else{
                    //alertify.alert('As a free member you can only upload 5 files. Upgrade to premium membership to upload an unlimited documents.');
                    alertify.set({ labels: { ok: "Accept", cancel: "Deny" } });
                    alertify.confirm("As a free member you can only upload 5 files. To add more files and improve your market reach, you need to be a Premium member. Sign up for free trial today.", function (e) {
                        if (e) {
                            window.location = "<?php echo base_url()?>payment/paymentoptiondetails";
                            //alertify.success("You've clicked OK");
                        } 
                    });
                    return false;
                }
            });
        }else{                     
            $(this).parent().before($("<div/>", {id: 'filediv',class:'document-open'}).fadeIn('slow').append(
                    $("<input/>", {name: 'file[]', type: 'file', id: 'file'}), 
                    $("<input/>",{name:'file_title[]',type:'text',placeholder:'File Title'}),       
                    $("<br/>")
            ));
        }
   });

    //following function will executes on change event of file input to select different file   
    $('body').on('change', '#file', function(){

            if (this.files && this.files[0]) {
                 abc += 1; //increementing global variable by 1
                
                var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                //$(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src=''/></div>");
                var reader = new FileReader();
                reader.onload = imageIsLoaded;
                reader.readAsDataURL(this.files[0]);                
                $(this).before("<div id='abcd"+ abc +"' class='abcd'><i class='fa fa-file-text-o fa-3'></i>  "+this.files[0].name+"</div>");
                
                $(this).hide();
                $("#abcd"+ abc).append($("<i/>", {class:'upload_doc_cancel fa fa-times fa-2></i>', alt: 'delete'}).click(function() {
                $(this).parent().parent().remove();
                }));
            }
        });

    //To preview image     
    function imageIsLoaded(e) {
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    $('#upload').click(function(e) {
        $('#upload_document_form input[type="file"]').each(function() {           
            if($(this).val() == '') { 
                alertify.alert('Please upload no of file you have selected');
                return false;
            }
            if($(this).parent().find('input[type=text]').val() == ''){
                alertify.alert('Please enter file title what you uploaded');
                return false;
            }
        });       
    });

    try{
        $("#loader-wrapper").show();
        $.post('<?php echo base_url(); ?>member/fetch_country_list',{},function(result){
            $("#loader-wrapper").hide();
            result = eval("(" + result + ")");
            vmManageProfile.country_list(result);
        });
    }catch(e){
        alertify.error(e);
    }

    try{
        $.post('<?php echo base_url(); ?>category/get_locality_list',{},function(result){ 
			result = eval("(" + result + ")");
			console.log(result);
			vmManageProfile.locality_list(result);
			$.post('<?php echo base_url(); ?>memberprofile/getSelectedLocalities',{},function(result_data){               
			result_data = eval("(" + result_data + ")");
			//console.log(result_data);
				for(var data in result_data){
				   $('#loc_'+result_data[data].locality_id).addClass('active');
				   vmManageProfile.selected_locality_list.push(result_data[data]);
				   //console.log(vmManageProfile.selected_locality_list());
				}
			});
        });
	}catch(e){
	  alert(e);
	}
	try{
		$.post('<?php echo base_url(); ?>category/get_looking_for_list',{},function(result){ 
			result = eval("(" + result + ")");
			//console.log(JSON.parse(result));
			vmManageProfile.looking_for_list(result);              
			$.post('<?php echo base_url(); ?>memberprofile/getLookinfFor',{},function(result_data){               
			result_data = eval("(" + result_data + ")");
			//console.log(result_data);
			for(var data in result_data){
			   $('#'+result_data[data].looking_for_id).addClass('active');
			   vmManageProfile.selected_looking_for_list.push(result_data[data]);
			   //console.log(vmManageProfile.selected_looking_for_list());
			}
			});
		});
	}catch(e){
	  alert(e);
	}
	try{
		//debugger;
		$.post('<?php echo base_url(); ?>memberprofile/getTagsList',{},function(result){ 
			result = eval("(" + result + ")");
            //console.log(result.tag_list);
			vmManageProfile.available_tags(result.tag_list);	
            
			$("#tags").select2({
				maximumSelectionLength: 3
			});	
			$('.select2-container').addClass('.multi-select');			
			/*setTimeout(function(){
				debugger;
				var parent = $("#manage_tag").parent('.fancybox-inner');
				//var parent = $("#manage_tag").parent().find('.fancybox-inner');
				$(parent).addClass('cust-height');
			},500);*/
		});		
	}catch(e){
	  alert(e);
	}
    try{
        $.post('<?php echo base_url(); ?>category/get_is_service_list',{},function(result){ 
            result = eval("(" + result + ")");
            vmManageProfile.service_list(result.is_service);
            $.post('<?php echo base_url(); ?>category/wizTwoservice',{field_name:'is_service'},function(result_data){               
                result_data = eval("(" + result_data + ")");               
                for(var data in result_data){
                   $('#cus_'+result_data[data].service_id).addClass('active');
                   vmManageProfile.selected_service.push(result_data[data]);
                }
            });
        });
    }catch(e){
        alert(e);
    }
    try{
        $.post('<?php echo base_url(); ?>category/looking_services_list',{},function(result){ 
            result = eval("(" + result + ")");             
            vmManageProfile.biz_serve_list(result.target_sector_list); 
            $.post('<?php echo base_url(); ?>category/wizTwoservice',{field_name:'looking_service'},function(result_data){               
                result_data = eval("(" + result_data + ")");               
                for(var data in result_data){
                   $('#bizu_'+result_data[data].look_service_id).addClass('active');
                   vmManageProfile.sel_biz_serve_list.push(result_data[data]);
                }
            });
        });
      }catch(e){
          alert(e);
      }
	selectedTags();
    //vmManageProfile.first_name('<?php echo $fname;?>');
    $("#update_name_form").validate({
        rules: {
            first_name: { required: true },
            last_name: { required: true }
        },
        messages: {
            first_name: { required: "Please enter first name" },
            last_name: { required: "Please enter last name" }
        },
        submitHandler: function (form) {    
            $("#loader-wrapper").show();             
            $.post('<?php echo base_url(); ?>memberprofile/editName',{first_name:vmManageProfile.first_name(),last_name:vmManageProfile.last_name()},function(result){
                if(result==1){
                    $("#loader-wrapper").hide();
                    vmManageProfile.viewName();
                    //$('.profile-cont-left>p').html('');
                    //$('.profile-cont-left>p').html(edit_first_name);
                    alertify.success("Name successfully updated!");            
                }
                else{
                    $("#loader-wrapper").hide();
                    alertify.error("Some error occured please try again!");
                }
            });
            return false;
        }
    });
    $('#update_tag_line_form').validate({
        rules:{
            tag_line:{required: true}
        },
        messages:{
            tag_line:{required: "Please Enter Tag Line" }
        },
        submitHandler:function(){
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>memberprofile/editTagLine',{tag_line:vmManageProfile.tag_line()},function(result){
                if(result==1){
                    $("#loader-wrapper").hide();
                    vmManageProfile.viewTagLine();
                    //$('.profile-cont-left>p').html('');
                    //$('.profile-cont-left>p').html(edit_first_name);
                    alertify.success("Tagline successfully updated");            
                }
                else{
                    $("#loader-wrapper").hide();
                    alertify.error("Some error occured please try again!");
                }
            });
            return false;
        }
    });
    $('#update_email_form').validate({
        rules:{
            email: { required: true }
        },
        messages:{
            email: { required: "Please enter your email" }
        },
        submitHandler:function(){            
            $("#loader-wrapper").show();  
            $.post('<?php echo base_url(); ?>memberprofile/validateEmail',{'email':vmManageProfile.email()},function(result){            
                if(result == 'FALSE'){
                   $.post('<?php echo base_url(); ?>memberprofile/editEmail',{'email':vmManageProfile.email()},function(res){
                        $("#loader-wrapper").hide();  
                        alertify.success("Email successfully updated");
                        vmManageProfile.viewEmail();
                        //$('.fancybox-close').trigger('click');
                        //$('.profile-inner').children('h3').html(ed_name+'<a id="pr_name" href="#div3"><i class="fa fa-pencil"></i></a>');
                    });
                }else{
                    $.post('<?php echo base_url(); ?>memberprofile/editEmail',{'email':vmManageProfile.email()},function(res){
                        $("#loader-wrapper").hide();  
                        alertify.success("Email successfully updated");
                        vmManageProfile.viewEmail();
                        //$('.fancybox-close').trigger('click');
                        //$('.profile-inner').children('h3').html(ed_name+'<a id="pr_name" href="#div3"><i class="fa fa-pencil"></i></a>');
                    });
                }
            });
        }
    });
    $("#change_password_form").validate({
        rules: {
            current_password: { required: true },
            password: { required: true },
            confirm_password: { required: true,equalTo: "#password" }
        },
        messages: {
            current_password: { required: "Please your current password" },
            password: { required: "Please enter your password" },
            confirm_password: { required: "Please confirm your password" ,equalTo:"Does not match with password"}
        },
        submitHandler: function (form) {
            $("#loader-wrapper").show();
            $.post('<?php echo base_url(); ?>memberprofile/validateCurrentPassword',{password:vmManageProfile.current_password()},function(result){
                if(result=='TRUE'){
                    $.post('<?php echo base_url(); ?>memberprofile/resetPassword',{password:vmManageProfile.password()},function(result){
                        if(result==1){  
                            $("#loader-wrapper").hide();                          
                            alertify.success("Password reset successfully");
                            vmManageProfile.current_password();
                            vmManageProfile.password();
                            vmManageProfile.confirm_password();
                            $('.fancybox-close').trigger('click');
                        }else{
                            $("#loader-wrapper").hide();
                             alertify.error("Some error occurred.Please try again!");
                        }
                    });           
                }
                else{
                    $("#loader-wrapper").hide();
                    alertify.error("Invalid Password Entered!!");
                }
            });            
            return false;
        }
    });
	function selectedTags(){
		try{
			$("#loader-wrapper").show();
			$.post('<?php echo base_url(); ?>memberprofile/getSelectedTags',{},function(result){ 
			$("#loader-wrapper").hide();
				result = eval("(" + result + ")");				
				var select_arr = [];
				vmManageProfile.selected_tags_list(result);				
				for(var i =0;i<result.length;i++){
					select_arr.push(result[i].tag_code);
				}
				vmManageProfile.chosen_tags(select_arr);
				$("#tags").trigger('change');
			});		
		}catch(e){
		  alert(e);
		}
	}
    //----------------------- Edit Profile Name---------------------------
    var x = $('#pr_name').parent('h3').first().text();
    var html = "<input type='text' id='e_name' value='"+x+"'><br/><input type='button' id='e_name_submit' value='Submit'>";
    
    $('#div3').html(html);
    $("#manage_customer_type_link").fancybox();
    $("#manage_function_you_serve_link").fancybox();
    $('#pr_name').fancybox();
    $('.video_popup').fancybox();
    $('#manage_buisness_description_link').fancybox();
    $('#manage_locality_explore_link').fancybox();
    $('#manage_about_me_link').fancybox();
    $('#manage_tag_link').fancybox();
    $('#manage_looking_link').fancybox();
    $('#change_password_link').fancybox();
    $('#upload_document_link').fancybox();
    $('#e_name_submit').click(function(){
        var ed_name = $("#e_name").val();    
        $("#loader-wrapper").show();  
        $.post('<?php echo base_url(); ?>memberprofile/validateEmail',{'email':ed_name},function(result){            
           if(result == 'TRUE'){
                $("#loader-wrapper").hide();  
                alertify.error("Email already exist.Please enter another email");
           }else{
              $.post('<?php echo base_url(); ?>memberprofile/editEmail',{'email':ed_name},function(res){
                $("#loader-wrapper").hide();  
                alertify.error("Email successfully updated");
                $('.fancybox-close').trigger('click');
                $('.profile-inner').children('h3').html(ed_name+'<a id="pr_name" href="#div3"><i class="fa fa-pencil"></i></a>');
              });
           }
        });
    });
    //-----------------------------------------------------------------------
    //----------------------- Edit Bussiness Name ---------------------------
    var bussinessName = "<?php echo addslashes($bussinessname)?>";
	//console.log(bussinessName);
    var html = '<div class="popup-box-part-center"><h2>Manage Buisness Name</h2><div class="business-img-listing-wrap cust-fancy-label-input"><input type="text" id="bussiness_name" value="'+bussinessName+'"><div style="clear:both;"></div><input type="button" id="e_bussiness_name_submit" value="Submit"></div></div>';
    $('#div4').html(html);
    $('#pr_buss').fancybox();
    
    $('#e_bussiness_name_submit').click(function(){        
      var ed_bussiness_name = $("#bussiness_name").val();
      //console.log(ed_name);
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessName',{'name':ed_bussiness_name},function(res){        
        $('.fancybox-close').trigger('click');
        //$('.profile-inner').first().children('span').html(ed_bussiness_name+'<a id="pr_buss" href="#div4"><i class="fa fa-pencil"></i></a>');
        $(".profile-cont-left>h3").text(ed_bussiness_name);
        $('.MAR-BTN30>span').html('');
        $('.MAR-BTN30>span').html(ed_bussiness_name+'<a href="#div4" class="edit-sec" id="pr_buss"><i class="fa fa-pencil"></i></a>');
        window.location.reload();
      });
    });
    //-----------------------------------------------------------------------
    //----------------------- Edit Location ----------------------------------
    var location = $('#loc').text();
    //var arr = location.split(',');
    //$('#office_name').val(arr[0]);
    $("#office_name").val($("#off_name").text());
    $("#city_name").val($("#ct_name").text());
    //$('#city_name').val(arr[1]);
    //$('#country_name').val(arr[1]);
    
    $("#pr_loc").fancybox();
    $("#add_new_add_link").fancybox();
    $('#pr_loc_submit').click(function(){
      var office_name = $("#office_name").val();
      var cityname = $('#city_name').val();
      var countryname = $('#country_name').val();
      var countrytext = $('#country_name option[value="'+countryname+'"]').text();
      //alert(cityname+' '+countryname);
      if(office_name ==''){alertify.alert("Please enter the office name"); return false;}
      if(cityname ==''){alertify.alert("Please enter the city name"); return false;}
      $("#loader-wrapper").show();
      $.post('<?php echo base_url(); ?>memberprofile/editMemberLocation',{'office_name':office_name,'country':countryname,'city':cityname},function(res){
        $("#loader-wrapper").hide();
        $('.fancybox-close').trigger('click');
        $("#ct_name").text(cityname);
        $("#off_name").text(office_name);
        $("#cntry_name").text(countrytext);
        //$('#loc').html(office_name+','+cityname+','+countrytext);
      });
      
    });
    //-----------------------------------------------------------------------
    
    //-------------------------------------- Edit Bussiness Type-------------------------
    var x = $('#buss_type').text();
    var html = "<input type='text' id='bussiness_type_name' value='"+x+"'><br/><input type='button' id='bussiness_type' value='Submit'>";
    $('#div6').html(html);
    
    $('#pr_bussiness_type').fancybox();
    $('#manage_profile_link').fancybox();    
    $('#bussiness_type').click(function(){
      var bussiness_type_name = $("#bussiness_type_name").val();
      
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessType',{'name':bussiness_type_name},function(res){
        $('.fancybox-close').trigger('click');
        $('#buss_type').text(bussiness_type_name);
      });
    });
    
    //-----------------------------------------------------------------------------------
    //-------------------------------------------- Edit Bussiness Summery ------------------
      
      var x = $('#pr_desc').text();
      //alert(x);
      var html = "<textarea id='edit_bussiness_summery'>"+x+"</textarea><br/><input type='button' id='edit_bussiness_summery_submit' value='Submit'>";
      $('#div7').html(html);
      
      $('#bussiness_summery').fancybox();
      
      $('#edit_bussiness_summery_submit').click(function(){
        
        var bussiness_summery = $("#edit_bussiness_summery").val();
        
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessSummery',{'name':bussiness_summery},function(res){
        $('.fancybox-close').trigger('click');
        $('#pr_desc').text(x);
      });
        
      });
      
    //--------------------------------------------------------------------------------------
    
  });
    function OpenWixOneModalForImproveMatch(){ 
        $.post('<?php echo base_url(); ?>member/getUserTergetIndusNo',{},function(result_request){ 
            if(result_request<=10){       
                if(check_wiz_two=='true'){
                    vmWidzardSecond.part_one(false);
                    vmWidzardSecond.part_two(false);
                    vmWidzardSecond.part_three(false);
                    vmWidzardSecond.part_four(false);
                    vmWidzardSecond.part_five(true);
                }else{
                    vmWidzardSecond.part_one(false);
                    vmWidzardSecond.part_two(true);
                }
            }else{
              $('.fancybox-close').trigger('click');
              alertify.error("You have already added 10 codes");
            }
        });
    }
    function removeUserDetail(key,code){
        //alert(key+'//'+code);
        alertify.confirm("Are you sure to delete the target industry and sector?", function (e) {
            if (e) {
                $.post('<?php echo base_url(); ?>memberprofile/removeUserDetail',{'key':key,'code':code},function(res){
                    if(res==1){
                        location.reload();
                    }
                });
            } 
        });
    }
    function removeUserDetailFromWizOne(code){
        //alert(code);
        alertify.confirm("Are you sure you’d like to delete the own industry and sector?", function (e) {
            if (e) {
                $.post('<?php echo base_url(); ?>memberprofile/removeUserDetailFromWizOne',{'code':code},function(res){
                    if(res==1){
                        location.reload();
                    }
                });
            } 
        });
    }
	ko.applyBindings(vmManageProfile, $("#manage_profile_section")[0]);
</script>
