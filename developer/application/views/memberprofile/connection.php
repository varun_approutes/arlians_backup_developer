     <script type="text/javascript">
      // Wait until the DOM has loaded before querying the document
      $(document).ready(function(){
        $('ul.tabs').each(function(){
          // For each set of tabs, we want to keep track of
          // which tab is active and it's associated content
          var $active, $content, $links = $(this).find('a');

          // If the location.hash matches one of the links, use that as the active tab.
          // If no match is found, use the first link as the initial active tab.
          $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
          $active.addClass('active');

          $content = $($active[0].hash);

          // Hide the remaining content
          $links.not($active).each(function () {
            $(this.hash).hide();
          });

          // Bind the click event handler
          $(this).on('click', 'a', function(e){
            // Make the old tab inactive.
            $active.removeClass('active');
            $content.hide();

            // Update the variables with the new link and content
            $active = $(this);
            $content = $(this.hash);

            // Make the tab active.
            $active.addClass('active');
            $content.show();

            // Prevent the anchor's default click action
            e.preventDefault();
          });
        });
        
        $('#con').click();
        var seg = "<?php echo $this->uri->segment(3); ?>";
        if(seg == "p"){
			$('#pen').click();
		}else if(seg == "r"){
			$('#rec').click();
		}
      });
    </script>
   <?php $mid = $this->session->userdata['logged_in']['id']; ?>

   <?php
      //echo '<pre>'; var_dump($connected_id); die();
      $connected_arr = array();
     
      
    foreach($connected_id as $element){
        if($element['is_accepted'] == '1'){
            $connected_arr[] = $element;
        }
    }
      //echo '<pre>'; var_dump($connected_arr); 
      //echo '<pre>'; var_dump($pending_arr); die(); 
    ?>
<div class="dashboard-wrap">
    <div class="row">
      <div class="fl68">
        <div md="graphic" class="span8">
            <div md="main" class="main"></div>                      
        </div> 
			<!-- <div id="sidetreecontrol"><a href="javascript:void(0)">Collapse All</a> <a href="javascript:void(0)">Expand All</a></div>
			<div class="fl50 network-customer">
          <h3>Customers</h3>		   
    		  <div id="tree"></div>
		  </div>
		  <div class="fl50 network-customer">
		   <h3>Suppliers</h3>			   
            <div id="tree1"></div>
		  </div> -->
			  <br />
			  <br />
            <div class="compose-inner-bg profile-bg">
                <div class="profile-inner">
                      <h3>My Network</h3>

                      <ul class="tabs network-tabs">
                          <li><a id="con" href="#tab_1" >Connections</a></li>
                          <li><a id="pen" href="#tab_2" >Pending requests</a></li>
                          <li><a id="rec" href="#tab_3" >Requests received</a></li>
                      </ul>
                      <ul class="profile-connect">
                          <div class="tab-content" id="tab_1" style="display: none;">
                              <?php //print_r($connected_arr);?>
                              <?php foreach($connected_arr as $con){  
                              if($con['mid']==$mid){
                                  $user = getParticularUserDetails($con['connected_id']);  
                                  $link =   $con['connected_id'];                    
                              }
                              if($con['connected_id']==$mid){
                                  $user = getParticularUserDetails($con['mid']);
                                  $link =   $con['mid'];
                              }
                              //var_dump($user);
                              ?>
                              <li id="<?php echo $link; ?>">
                                  <div class="inbox-img"> 
                                      <a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $link; ?>"><img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $user['profile_image']?>" alt=""></a>
                                  </div>
                                  <div class="inbox-content">
                                      <h4><?php echo $user['bussinessname'];?></h4>
                                      <p class="date"> <?php echo $user['bussinesstype1'];?></p>
                                  </div>
                                  <div class="right-inbox-icon">
                                      <?php if($con['is_accepted']=='1'){?>
                                      <a href="javascript:void(0);" class="icon-border">
                                          <i class="fa fa-times fa-1"></i>
                                      </a> 
                                      <?php }else{ ?>
                                      <a href="javascript:void(0);" class="icon-border">
                                          <i class="fa fa-check fa-1"></i>
                                      </a>
                                      <?php } ?>
                                  </div>
                              </li>
                              <?php }?>
                          </div>
                          <div class="tab-content" id="tab_2" style="display: none;">
                              <?php foreach($pending_arr as $pen){ 
                              if($pen['mid']==$mid){
                                  $user = getParticularUserDetails($pen['connected_id']);   
                                   $link =   $pen['connected_id'];                     
                              }
                              if($pen['connected_id']==$mid){
                                  $user = getParticularUserDetails($pen['mid']);
                                   $link =   $pen['mid'];
                              }
                              ?>
                              <li id="<?php echo $link; ?>">
                                  <div class="inbox-img"> 
                                      <a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $link; ?>"><img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $user['profile_image']?>" alt=""></a>
                                  </div>
                                  <div class="inbox-content">
                                      <h4><?php echo $user['bussinessname'];?></h4>
                                      <p class="date"> <?php echo $user['bussinesstype1'];?></p>
                                  </div>

                                  <div class="right-inbox-icon">
                                      <?php //if($pen['is_accepted']=='1'){?>
                                      <a href="javascript:void(0);" class="icon-border"  >
                                          <i class="fa fa-times fa-1"></i>
                                      </a>
                                      <?php //}else{ ?>
                                     <!--  <a href="javascript:void(0);" class="icon-border">
                                          <i class="fa fa-check fa-1"></i>
                                      </a> -->
                                      <?php// } ?>
                                  </div>
                              </li>
                              <?php } ?>
                          </div>
                          <div class="tab-content" id="tab_3" >
                              <?php foreach($received_arr as $rec){ 
                              if($rec['mid']==$mid){
                                  $user = getParticularUserDetails($rec['connected_id']);  
                                  $link =   $rec['connected_id'];                       
                              } if($rec['connected_id']==$mid){
                                  $user = getParticularUserDetails($rec['mid']);
                                   $link =   $rec['mid'];
                              }                           
                              ?>
                              <li id="<?php echo $link; ?>">
                                  <div class="inbox-img"> 
                                      <a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $link; ?>"><img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $user['profile_image']?>" alt=""></a>
                                  </div>
                                  <div class="inbox-content">
                                      <h4><?php echo $user['bussinessname'];?></h4>
                                      <p class="date"> <?php echo $user['bussinesstype1'];?></p>
                                  </div>

                                  <div class="right-inbox-icon">
                                      <?php //if($rec['is_accepted']=='1'){?>
                                          <a href="javascript:void(0);"  class="icon-border"  >
                                              <i class="fa fa-times fa-1"></i>
                                          </a>
                                      <?php //}else{ ?>
                                          <a href="javascript:void(0);"  class="icon-border">
                                              <i class="fa fa-check fa-1"></i>
                                          </a>
                                      <?php //} ?>
                                  </div>
                              </li>
                              <?php } ?>
                          </div>
                      </ul>
                </div>
            </div>            
        </div> 
    </div>
</div>
<a href="#manage_connection_modal" id="manage_connection_modal_link" style="display:none"></a>
<div id="manage_connection_modal" style="width:600px; display:none;">
	<div class="popup-box-part-center">
		<form id="send_message_form" method="post">
			<h2>Manage Connection</h2>
			<h4 class="button_connect">Are you interested to connect this business? </h4>
			<h4 class="button_disconnect">Are you sure to disconnect this business? </h4>
			<div class="business-img-listing-wrap cust-fancy-label-input" style="margin-top:20px;">				 
				<button type="button" class="button button_connect" onclick="manageConnection();">Connect</button> 
				<button type="button" class="button button_disconnect" onclick="manageDisonnection();">Disconnect</button> 
			</div>      
		</form>    
	</div>
</div>
<!-- -------------fancy box div-------------------- --> 

<div id="div3" class="edit-profile" style="display:none"></div>
<div id="div4" class="edit-profile" style="display:none"></div>

<div id="div5" class="edit-profile" style="display:none">

<input type="text" name="city_name" id="city_name">

<?php 
$country_list = getCountryList();

?>
<select name="country_name" id="country_name">
<?php foreach($country_list as $c){ ?>
<option value="<?php echo $c->id; ?>"><?php echo $c->country_name; ?></option>
<?php } ?>
</select> 
<input type="button" id="pr_loc_submit">
</div>

<div id="div6" class="edit-profile" style="display:none"></div>
<div id="div7" class="edit-profile" style="display:none"></div>

<script type="text/javascript">
var uer_id_for_manage_connection = '';
var mid = "<?php echo $mid; ?>";
 $(document).ready(function() { 
    $("#img_upload").fancybox();
    $("#back_image").fancybox();
	$('#manage_connection_modal_link').fancybox();  
 });
 //network connection manage section //
 function checkConnection(user_id){  
  if(mid!=user_id){
    	try{
    		uer_id_for_manage_connection = user_id; 
    		$('#loader-wrapper').show();
    		$.post('<?php echo base_url(); ?>memberprofile/checkConnection',{user_id:uer_id_for_manage_connection},function(result){
    			$('#loader-wrapper').hide();
    			if(result=='true'){
    				$('#manage_connection_modal_link').trigger('click');
    				$('.button_connect').hide();
    				$('.button_disconnect').show();
    			}else{
    				$('#manage_connection_modal_link').trigger('click');
    				$('.button_connect').show();
    				$('.button_disconnect').hide();
    			}
    			//$('.fancybox-item').trigger('click');
    			//else{ $('.fancybox-item').trigger('click'); }
    		});
    	}catch(e){
    		alertify.alert(e);
    	} 
  }else{
      alertify.alert("You cannot manage connection with your own");
  }
 }
 function manageConnection(){
	//alert(uer_id_for_manage_connection);
	$('#loader-wrapper').show();
	$.post('<?php echo base_url(); ?>member/reqestForConnection',{'id':uer_id_for_manage_connection},function(result){
		$('#loader-wrapper').hide();
		if(result==1){
			$('.fancybox-close').trigger('click');
			alertify.success('Your request has been successfully proceed');
			//loadTree();
		}else{
			$('.fancybox-close').trigger('click');
			alertify.alert('This is embrassing.Some error occured');
		}
	}); 
 }
 function manageDisonnection(){
	$('#loader-wrapper').show();
	$.post('<?php echo base_url(); ?>memberprofile/reqestForDisonnection',{'id':uer_id_for_manage_connection},function(result){
		$('#loader-wrapper').hide();
		if(result=='true'){
			$('.fancybox-close').trigger('click');
			alertify.success('Your request has been successfully proceed');
			//loadTree();
		}else{
			$('.fancybox-close').trigger('click');
			alertify.alert('This is embrassing.Some error occured');
		}
	}); 
 }
$('#uploadImg').click(function(){
  var v = $('#blah').attr('src');
  var x = v.split('/');

  if(x[7] == 'profile-img.png'){
    $('#error_img').html('<p>Please select a picture to upload</p>');
  }else{
    $('#loader-wrapper').show();
    $.post('<?php echo base_url(); ?>memberprofile/uploadImage',{'imgFile':v},function(result){
      $('#loader-wrapper').hide();
      if(result==true)
        $('.fancybox-item').trigger('click');
      else{ $('.fancybox-item').trigger('click'); }
    });
    
  }
});

$('#uploadCoverImg').click(function(){
  var v = $('#blah1').attr('src');
  var x = v.split('/');

  if(x[7] == 'profile-img.png'){
    $('#error_img1').html('<p>Please select a picture to upload</p>');
  }else{
    $('#loader-wrapper').show();
    $.post('<?php echo base_url(); ?>memberprofile/uploadBackImage',{'imgFile':v},function(result){
      $('#loader-wrapper').hide();
      //console.log(result);
      if(result==true)
        $('.fancybox-close').trigger('click');
      else{ $('.fancybox-item').trigger('click'); }
    });
    
  }
});
  /*function tree_data()
  {
    $("#tree").treeview({          
          collapsed: true,
          animated: "medium",
          control:"#sidetreecontrol",
          persist: "location"
        });
		$("#tree1").treeview({          
          collapsed: true,
          animated: "medium",
          control:"#sidetreecontrol",
          persist: "location"
        });
    
  } */
 function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(152)
                    .height(152);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  
  function readBackURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(1200)
                    .height(190);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  
  //--------------------------

  /*function loadTree(){
	try{
       $('#loader-wrapper').show();
      $.post('<?php echo base_url();?>memberprofile/listing_customres',{},function(data){
         $('#loader-wrapper').hide();
        $('#tree').html(data);
        tree_data();
        setTimeout(function(){
          $('#loader-wrapper').hide();
        },5000);
      });

	  $('#loader-wrapper').show();
	  $.post('<?php echo base_url();?>memberprofile/listing_suppliers',{},function(data){
         $('#loader-wrapper').hide();
        $('#tree1').html(data);
        tree_data();
        setTimeout(function(){
            $('#loader-wrapper').hide();
        },5000);
      });        
    }catch(e){
      alertify.error(e);
    }  
  }*/
  $(document).ready(function(){
	  //loadTree();
    
    //----------------------- Edit Profile Name---------------------------
    var x = $('#pr_name').parent('h3').first().text();
    var html = "<input type='text' id='e_name' value='"+x+"'><br/><input type='button' id='e_name_submit' value='Submit'>";    
    $('#div3').html(html);    
    $('#pr_name').fancybox();    
    $('#e_name_submit').click(function(){
      var ed_name = $("#e_name").val();
      //console.log(ed_name);
      $('#loader-wrapper').show();
      $.post('<?php echo base_url(); ?>memberprofile/editName',{'name':ed_name},function(res){
        $('#loader-wrapper').hide();
        $('.fancybox-close').trigger('click');
        $('.profile-inner').children('h3').html(ed_name+'<a id="pr_name" href="#div3"><i class="fa fa-pencil"></i></a>');
      });
    });
    //-----------------------------------------------------------------------
    //----------------------- Edit Bussiness Name ---------------------------
    var bussinessName = $('#pr_buss').parent('span').first().text();
    var html = "<input type='text' id='bussiness_name' value='"+bussinessName+"'><br/><input type='button' id='e_bussiness_name_submit' value='Submit'>";
    $('#div4').html(html);
    $('#pr_buss').fancybox();
    
    $('#e_bussiness_name_submit').click(function(){
      var ed_bussiness_name = $("#bussiness_name").val();
      //console.log(ed_name);
      $('#loader-wrapper').show();
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessName',{'name':ed_bussiness_name},function(res){
        $('.fancybox-close').trigger('click');
        $('.profile-inner').first().children('span').html(ed_bussiness_name+'<a id="pr_buss" href="#div4"><i class="fa fa-pencil"></i></a>');
      });
    });
    //-----------------------------------------------------------------------
    //----------------------- Edit Location ----------------------------------
    var location = $('#loc').text();
    var arr = location.split(' ');
    
    $('#city_name').val(arr[0]);
    $('#country_name').val(arr[1]);
    
    $("#pr_loc").fancybox();
    $('#pr_loc_submit').click(function(){
      var cityname = $('#city_name').val();
      var countryname = $('#country_name').val();
      
      //alert(cityname+' '+countryname);
      $.post('<?php echo base_url(); ?>memberprofile/editMemberLocation',{'country':countryname,'city':cityname},function(res){
        $('.fancybox-close').trigger('click');
        $('#loc').html(cityname+' '+countryname);
      });
      
    });
    //-----------------------------------------------------------------------
    
    //-------------------------------------- Edit Bussiness Type-------------------------
    var x = $('#buss_type').text();
    var html = "<input type='text' id='bussiness_type_name' value='"+x+"'><br/><input type='button' id='bussiness_type' value='Submit'>";
    $('#div6').html(html);
    
    $('#pr_bussiness_type').fancybox();
    
    $('#bussiness_type').click(function(){
      var bussiness_type_name = $("#bussiness_type_name").val();
      
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessType',{'name':bussiness_type_name},function(res){
        $('.fancybox-close').trigger('click');
        $('#buss_type').text(bussiness_type_name);
      });
    });
    
    //-----------------------------------------------------------------------------------
    //-------------------------------------------- Edit Bussiness Summery ------------------
      
      var x = $('#pr_desc').text();
      //alert(x);
      var html = "<textarea id='edit_bussiness_summery'>"+x+"</textarea><br/><input type='button' id='edit_bussiness_summery_submit' value='Submit'>";
      $('#div7').html(html);
      
      $('#bussiness_summery').fancybox();
      
      $('#edit_bussiness_summery_submit').click(function(){
        
        var bussiness_summery = $("#edit_bussiness_summery").val();
        
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessSummery',{'name':bussiness_summery},function(res){
        $('.fancybox-close').trigger('click');
        $('#pr_desc').text(x);
      });
        
      });
      
    //--------------------------------------------------------------------------------------
    
  });

jQuery('document').ready(function(){
      jQuery('.fa-check').click(function(){
        var connected_id = jQuery(this).parent('a').parent('div').parent('li').attr('id');
        var mid = "<?php echo $mid; ?>";
        $('#loader-wrapper').show();
        jQuery.post('<?php echo base_url(); ?>suggestion/acceptRequestForConnectionNew',{'connected_id':connected_id,'mid':mid},function(res){
          debugger;
          $('#loader-wrapper').hide();
            if(res == "true"){      
               location.reload(); 
            }
        });
    });
    jQuery('.fa-times').click(function(){
        var connected_id = jQuery(this).parent('a').parent('div').parent('li').attr('id');
        var mid = "<?php echo $mid; ?>";
        $('#loader-wrapper').show();
        jQuery.post('<?php echo base_url(); ?>suggestion/deleteRequestForConnection',{'connected_id':connected_id,'mid':mid},function(res){
          debugger;
          $('#loader-wrapper').hide();
            if(res == "true"){
               location.reload(); 
            }
        });
    });

});
</script>