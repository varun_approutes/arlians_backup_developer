<section id="user_profile" style="display:none"  ng-controller="viewMemberProfileApiCtrl">
    <div class="row">
        <div class="col-md-9">
            <div class="profile_page box">
                <div class="profile_shadow">
                    <div class="profile_detailinner clearfix">
                        <div class="profile_img">
                            <!-- Modal Crop Image-->
                            <div class="modal fade" id="crop_profile_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Upload a Business Logo</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="container-crop">
                                                <div class="imageBox">
                                                    <div class="thumbBox"></div>
                                                    <div class="spinner" style="display: none">Loading...</div>
                                                </div>                                            
                                                <div class="cropped"></div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="file" id="file">                                               
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                            <button type="button" id="btnZoomIn" class="btn btn-primary">+</button>
                                            <button type="button" id="btnZoomOut" class="btn btn-primary">-</button>
                                            <button type="button" ng-click="uploadProfileImage()" id="btnCrop" class="btn btn-primary">Done</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <i class="fa fa-camera change_photos2" ng-show="show_if_own_profile" style="cursor:pointer" ng-show="toggle_edit" onclick="open_profile_crop_img_modal()"></i>
                            <img id="hub_profile_default_img" ng-show="profile_image != ''" ng-src="{{profile_image}}" src="<?php echo base_url(); ?>assets_phase2/images/profile_avatar.png">
                            <span ng-show="profile_image == ''" ng-bind="logobussinessname"></span>
                        </div>
                        <div class="prof_details clearfix">
                            <h4 class="edit_popup" ng-show="business_name != ''">
                                <span class="business_nm" ng-bind="business_name" ng-model="user_buisnessname">Financial Assets Ltd.</span>
                                <div class="edit_option">
                                    <!-- <a href="#credits" ng-show="show_if_own_profile" class="toggle"><i class="fa fa-edit"></i></a> -->
                                    <div id="credits" class="well hidden show_div">
                                        <span class="edit_heading">Your Business Name</span>
                                        <input type="text" class="form-control" ng-model="edit_business_name"><br>
                                        <button class="btn btn-primary" ng-click="updateBizName()">Save</button> &nbsp;
                                        <!--                                    <button class="btn btn-sucess">cancel</button>-->
                                    </div>
                                </div>
                            </h4>
                            <h4 class="edit_popup" ng-show="business_name == ''">
                                <span class="business_nm" ng-bind="fname">Financial Assets Ltd.</span>&nbsp;<span class="business_nm" ng-bind="lname">Financial Assets Ltd.</span>
                                <div class="edit_option">
                                    <a href="#credits_name" ng-show="show_if_own_profile" class="toggle"><i class="fa fa-edit"></i></a>
                                    <div id="credits_name" class="well hidden show_div">
                                        <span class="edit_heading">Your Name</span>
                                        <input type="text" class="form-control" ng-model="fname"><br>
                                        <input type="text" class="form-control" ng-model="lname"><br>
                                        <button class="btn btn-primary" ng-click="updateName()">Save</button> &nbsp;
                                        <!--                                    <button class="btn btn-sucess">cancel</button>-->
                                    </div>
                                </div>
                            </h4>
                            <span class="profile_detailstxt" >                 
                                <span ng-show="business_tagline != ''"ng-bind="business_tagline"><em>Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness.</em></span>
                                <span ng-show="business_tagline == ''"><em>Tag line</em></span>

                                <div class="edit_option">
                                    <!-- nilanjan <a href="#credits1" ng-show="show_if_own_profile" class="toggle"><i class="fa fa-edit"></i></a> -->
                                   <!--  <div ng-hide="{{status}}">Nilanjan Roy Choudhury</div> -->
                                    <div id="credits1"  ng-hide="{{status}}" >              

                                        <span class="edit_heading">Tag Name</span>
                                        <input type="text" class="form-control" ng-model="edit_tagline"><br>
                                        <button class="btn btn-primary" ng-click="updateTagName()">Save</button> &nbsp;
                                        <!--                                    <button class="btn btn-sucess">cancel</button>-->

                                    </div>
                                </div>

                            </span><br>

                            <strong class="city_location edit_popup" >
                                <span ng-bind="city!=''?city:'city'">city</span> , <span ng-bind="countryName!=null?countryName:'country'">country</span>
                                
                            </strong>
                                                   


                        </div>


                    </div>
                    <div class="edit_option" >
                                    <!-- nilanjan <a href="#credits2" ng-show="show_if_own_profile" class="toggle"><i class="fa fa-edit"></i></a> -->

                                    <div id="credits2" class="well hidden show_div" >               

                                        <span class="edit_heading">Country and City</span>
                                        <select class="form-control" ng-model="selected_country"  ng-options="country.id as country.country_name for country in country_list">
                                            <option>Select Country</option>
                                        </select>
    <!--                                    <select class="form-control">
                                        <option value=''>Select Party</option>
                                        <option ng-repeat="country in country_list" ng-model="selected_country" value="{{country._id}}" ng-selected="$scope.country_id == country.id">{{country.country_name}}
                                        </option>
                                        </select>-->
                                        <input type="text" ng-model="edit_city" class="form-control"><br>
                                        <button class="btn btn-primary" ng-click="editLocation()">Save</button> &nbsp; 
                                        <!--                                    <button class="btn btn-sucess">cancel</button>-->

                                    </div>
                                </div>




                    

                    <div class="profile_connects"> 
                <!-- code for toggle buttom top nilanjan  start -->

                <div ng-init="init()" class="edit_toggle" ng-if="EditFlag!=='N'">

                        <div class="">
                        <span class="ed-tg">Edit</span>
                                <i class="fa fa-toggle-on active" ng-if="status1 == false" ng-click="changeStatus();"></i>
                                <i class="fa fa-toggle-on fa-rotate-180 inactive" ng-if="status1 == true" ng-click="changeStatus();"></i>
                        </div>
                </div>    
                    <!-- code for toggle buttom nilanjan  end -->


                        <ul ng-show="show_if_other_profile" class="pconnect sm_none">
                            <li><a href="javascript:void(0)" ng-click="openMessagePopup(mid)"><i class="fa fa-paper-plane"></i></a></li>
                            <li ng-show="isConnected == 0" class="notconnected"><a href="javascript:void(0)" ng-click="connectionLogic(mid, isConnected)"  data-toggle="tooltip" data-placement="top" title="Add to my network"><i class="fa fa-user-plus"></i></a></li>
                            <li ng-show="isConnected == 2">Request Sent</li>
                            <li ng-show="isConnected == 1"><a href="javascript:void(0)" ng-click="connectionLogic(mid, isConnected)" class="connected" ><i class="fa fa-user-times"></i></a></li>
                        </ul>
                       <!--  <ul class="mob_msg">
                            <li><a href="javascript:void(0)">Message</a></li>
                            <li><a href="javascript:void(0)">Connect</a></li>
                        </ul> -->
                    </div>
                    
                    <!-- Modal Crop Image-->
                    <!-- <div class="modal fade" id="message_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Message</h4>
                                </div>
                                <div class="modal-body">
                                    <textarea ng-model="message" class="popmsg_area" placeholder="Type message..."></textarea>
                                </div>	

                                <div class="modal-footer">										                                              
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" ng-click="sendMessage(mid)" class="btn welcome_yellow">Save</button>
                                </div>

                            </div>
                        </div>
                    </div> -->

                   <div id="message_modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Message</h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <textarea ng-model="message" class="popmsg_area" placeholder="Type message..."></textarea>
                                                                                              
                                      
                                    </div>
                                    <div class="modal-footer">
                                        
                                       <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" ng-click="sendMessage(mid)" class="btn welcome_yellow">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>





                    
                </div>
                <!-- Modal Crop Image-->
                <div class="modal fade" id="crop_banner_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Upload an Banner Image</h4>
                            </div>
                            <div class="modal-body">
                                <div class="container-crop">
                                    <div class="imageBox_banner">
                                        <div class="thumbBox_banner"></div>
                                        <div class="spinner" style="display: none">Loading...</div>
                                    </div>
                                    <div class="action">

                                    </div>
                                    <div class="cropped"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="file" id="filebanner">                                               
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                <button type="button" id="btnZoomInb" class="btn btn-primary">+</button>
                                <button type="button" id="btnZoomOutb" class="btn btn-primary">-</button>
                                <button type="button" id="btnCropb" ng-click="uploadBannerImage()" class="btn btn-primary">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
                <i class="fa fa-camera change_photos" ng-show="show_if_own_profile" style="cursor:pointer" onclick="open_banner_crop_img_modal()"></i>

                <img class="full_width" id="hub_banner_default_img" ng-src="{{background_image}}" alt="">
                <!-- <img id="hub_banner_preview_img"  src="" alt=""> -->

            </div>
             <div ng-show="showtopeditdiv" class="edit-profilediv">
                <div>
                <form>
                  <div class="row">
                     <div class="col-sm-6">
                         <div class="form-group">
                           <label class="">Your Business Name</label>
                           <input type="text" class="form-control" ng-model="edit_business_name">
                      </div>
                     </div> 
                     <div class="col-sm-6">
                         <div class="form-group">
                             <label>Tagline</label>
                             <input type="text" class="form-control" maxlength="70"  ng-model="edit_tagline">
                          </div>
                     </div>
                  </div>             

                     
                       
                      
                     <div class="row">
                         
                         <div class="col-sm-6">
                             <div class="form-group">
                               <label>Select Country</label>
                                 <select class="form-control" ng-model="selected_country"  ng-options="country.id as country.country_name for country in country_list">
                                    <option>Select Country</option>
                                </select> 
                             </div>
                         </div>
                         <div class="col-sm-6">
                             <div class="form-group">
                              <label>City</label>
                                <input type="text" class="form-control" ng-model="edit_city" >
                              </div>
                         </div>

                     </div>
                      
                      
                      <button class="btn btn-primary" ng-click="updateProfile()">Save</button>
                    </form>






                    
                    
                    
                   
                   
                    
                    
                       
                   
                   
                   
               </div>
             </div>
            <!-- profile details -->
            <div class="profile_details box">
                <ul>
                    <li ><span ng-bind="connection_no">0</span><br><p>Connections</p></li>
                    <li ><span ng-bind="post_no">0</span><br><p>Posts</p></li>
                    <li ><span ng-bind="publish_no">0</span><br><p>Publications</p></li>
                    <li class="sm_none"><span ng-bind="opentalks_no">0</span><br><p>Open Talks</p></li>
                </ul>
            </div>
            <!-- sectors -->
            <div class="summary box">
                <div class="box-header">                 
                    <i class="fa fa-gears"></i> Sectors                 
                </div>
                <div class="box-body">
                    <ul class="sector_btn">
                        <li ng-repeat="list_sectorname in sectorname">
                            <a href="javascript:void(0)" class="blue_circlebtn">
                                <span ng-bind="list_sectorname.name">Beauty</span>
                            </a>
                        </li>

                    </ul>

                </div>
            </div>
            <div class="summary box">
                <div class="box-header">                 
                    <i class="fa  fa-file-text"></i>
                    Summary 
                    <!-- <div class="edit_summary edit_option" ng-show="show_if_own_profile">
                        <a href="javascript:void(0)" ng-click="editSummary(showSummaryEditorAndUploadDoc)"><i id="summary_close" class="fa fa-edit"></i></a>
                    </div>  -->
                    <!-- @Nilanjan button below start toggle -->
                    
                    <div ng-init="init()" class="white-tg" ng-if="EditFlag!=='N'">
                        <div > 
                        <span class="sum-tg">Edit</span>                            
                                <i class="fa fa-toggle-on active" ng-if="status2 == false" ng-click="changeStatus2();"></i>
                                <i class="fa fa-toggle-on fa-rotate-180 inactive" ng-if="status2 == true" ng-click="changeStatus2();"></i>
                        </div>
                </div> 
                <!-- @Nilanjan button below start toggle -->
                </div>
                <div class="box-body summary">                    
                   <!-- <span ng-bind-html="business_desc">-->
                    <span ta-bind ng-model="business_desc" ng-hide="showSummaryEditorAndUploadDoc">
                        
                    </span> 
                    <div ng-show="showSummaryEditorAndUploadDoc">
                        <div class="publish_editor box-body">
                            <div text-angular ng-model="business_desc_edit"  name="demo-editor" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
                        </div>
                        <button class="btn btn-primary" ng-click="editBizSumary()">Save</button> &nbsp; 
                        <!--<button class="btn btn-sucess">cancel</button>-->
                    </div>
                    <!-- summary carousel -->
                    <div class="summary_carousel" ng-show="doc_animate" >
                        <div id="summary">
                            <div class="item" ng-repeat="list_doc_uploaded in doc_uploaded">
                                <div class="summary_imginner" ng-show="show_if_own_profile">
                                    <div class="hover_cont" >
                                        <a href="javascript:void(0)" class="hover_btn" ng-click="showDocDetailInModal(list_doc_uploaded)">Show</a>
                                        <a href="javascript:void(0)" class="hover_btn" ng-click="deleteDoc(list_doc_uploaded, $event)">Delete</a>
                                        <!--<span ng-bind="list_doc_uploaded.file_title"></span>-->
                                    </div>
                                    <img ng-if="list_doc_uploaded.ext == 'gif'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                                    <img ng-if="list_doc_uploaded.ext == 'jpeg'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                                    <img ng-if="list_doc_uploaded.ext == 'png'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                                    <img ng-if="list_doc_uploaded.ext == 'jpg'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">

                                    <span ng-if="list_doc_uploaded.cover_image !== 'NULL'" >
                                        <img ng-if="list_doc_uploaded.cover_image !== 'NULL'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.cover_image}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                                    </span>
                                    <span ng-if="list_doc_uploaded.cover_image == NULL">
                                        <i ng-if="list_doc_uploaded.ext == 'txt'" class="fa  fa-file-text icon_large" ng-show="list_doc_uploaded.ext == 'doc'"></i>
                                        <i ng-if="list_doc_uploaded.ext == 'doc'" class="fa  fa-file-text icon_large" ng-show="list_doc_uploaded.ext == 'doc'"></i>
                                        <i ng-if="list_doc_uploaded.ext == 'docx'" class="fa  fa-file-text icon_large" ng-show="list_doc_uploaded.ext == 'docx'"></i>
                                        <i ng-if="list_doc_uploaded.ext == 'pdf'" class="fa  fa-file-pdf-o icon_large" ng-show="list_doc_uploaded.ext == 'pdf'"></i>                                    
                                        <i ng-if="list_doc_uploaded.ext == 'ppt'" class="fa  fa-file icon_large" ng-show="list_doc_uploaded.ext == 'ppt'"></i>                                    
                                        <i ng-if="list_doc_uploaded.ext == 'pptx'" class="fa  fa-file icon_large" ng-show="list_doc_uploaded.ext == 'pptx'"></i>                                    
                                        <i ng-if="list_doc_uploaded.ext == 'mp4'" class="fa  fa-file-video-o icon_large" ng-show="list_doc_uploaded.ext == 'mp4'"></i>  
                                    </span>

                                </div>
                                <p ng-bind="list_doc_uploaded.file_title">User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                            </div>                            
                        </div>
                    </div>
                    <!-- profile uploader -->
                    <ul class="profile_uploader" ng-show="showSummaryEditorAndUploadDoc">
                        <li>Upload :</li> 
                        <li><a href="javascript:void(0)" ng-click="openModalForDocUpload()"><i class="fa  fa-file-text"></i> <span class="sm_none">Document</span></a></li>
                        <li><a href="javascript:void(0)" ng-click="openModalForDocUpload()"><i class="fa  fa-file-photo-o"></i> <span class="sm_none">Media</span></a></li>
                        <li><a href="javascript:void(0)" ng-click="openModalForDocUpload()"><i class="fa  fa-file-excel-o"></i> <span class="sm_none">Report</span></a></li>
                        <li><a href="javascript:void(0)" ng-click="openModalForDocUpload()"><i class="fa  fa-file-powerpoint-o"></i> <span class="sm_none">Presentation</span></a></li>
                    </ul>
                    <div class="modal fade" id="docModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Upload your document</h4>                                       
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Tile:</label>
                                        <input type="text" ng-model="doc_title" class="form-control">
                                    </div>
                                    <input type="file" id="file1" name="file" ng-files="getTheFiles($files)" /> 
                                    <div class="form-group"  id="cover" style="display:none">
                                        <label for="message-text" class="control-label">Upload Cover Picture:</label>
                                        <input type="file" name="file" id="cover_file" />
                                        <div class="img_pic" id="cover_img" style="display:none"><img id="cover_img_src" src="" /></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" ng-click="uploadFile()">Upload</button>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="docDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="docDetailModalLabel" ng-bind="doc_detail_show.file_title">Upload your document</h4>                                       
                                </div>
                                <div class="modal-body">
                                    <div class="doc-pdf-ppt" ng-if="doc_detail_show.ext == 'pdf'">
                                        <iframe ng-src="{{'http://docs.google.com/gview?url='+doc_detail_show.path+doc_detail_show.file_name+'&embedded=true'}}" src=""  frameborder="0"></iframe>
                                    </div>
                                    <div class="doc-pdf-ppt" ng-if="doc_detail_show.ext == 'doc'">
                                        <iframe ng-src="{{'http://docs.google.com/gview?url='+doc_detail_show.path+doc_detail_show.file_name+'&embedded=true'}}" src=""  frameborder="0"></iframe>
                                    </div>
                                    <div class="doc-pdf-ppt" ng-if="doc_detail_show.ext == 'docx'">
                                        <iframe ng-src="{{'http://docs.google.com/gview?url='+doc_detail_show.path+doc_detail_show.file_name+'&embedded=true'}}" src=""  frameborder="0"></iframe>
                                    </div>
                                    <div class="doc-pdf-ppt" ng-if="doc_detail_show.ext == 'pptx'">
                                        <iframe ng-src="{{'http://docs.google.com/gview?url='+doc_detail_show.path+doc_detail_show.file_name+'&embedded=true'}}" src=""  frameborder="0"></iframe>
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'txt'">
                                        <a href="" ng-href='{{doc_detail_show.path + doc_detail_show.file_name}}' target="_blank" ng-bind='doc_detail_show.file_name'></a>
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'jpg'">
                                        <img ng-src="{{doc_detail_show.path}}{{doc_detail_show.file_name}}"  src="" alt="">
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'jpeg'">
                                        <img ng-src="{{doc_detail_show.path}}{{doc_detail_show.file_name}}"  src="" alt="">
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'png'">
                                        <img ng-src="{{doc_detail_show.path}}{{doc_detail_show.file_name}}"  src="" alt="">
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'gif'">
                                        <img ng-src="{{doc_detail_show.path}}{{doc_detail_show.file_name}}"  src="" alt="">
                                    </div>
                                    <div class="video-ply" ng-if="doc_detail_show.ext == 'mp4'">
                                        <video controls ng-src="{{doc_detail_show.path + doc_detail_show.file_name}}" src="http://www.w3schools.com/html/mov_bbb.mp4"></video>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                                                       
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!-- summary -->
            <!--            <div class="summary box">
                            <div class="box-header">
                                <i class="fa  fa-file-text"></i>
                                Summary
                                
                            </div>
            
                            <div class="box-body summary_pop">
                                <div class="mCustomScrollbar">
                                    <div class="edit_popup">
            
                                        <span ng-bind-html="business_desc">
                                            I am the co-founder and director of Financial Assets - communication and service design for better health, care and wellbeing. We engage and involve people in addressing challenges. People are at the heart of health and wellbeing challenges, so it makes sense to involve them in designing better solutions. We put people at the centre of the problem solving process to ensure we fully understand the issues, and we design the solutions that are cost effective and that deliver impact.I have specialist knowledge of co-design approaches to achieve social impact.
                                        </span>  
                                    </div>
            
                                </div>
            
                            </div>
                        </div>-->

            <!-- publish start -->
            <!-- read publish -->






            <div class="summary box">
                <div class="box-header">
                    <i class="fa  fa-file-text"></i>
                    Publish
                </div>
                <div class="box-body">
                    <div class="publish_line"></div>
                    <!-- publish carousel -->
                    <div class="publish_carousel">
                        <div id="publish" class="owl-carousel">
                            <div class="item" ng-repeat="list_publish in publishList">
                                <div class="summary_imginner" ng-if="list_publish.link != null" >
                                    <div class="hover_cont" ng-show="show_if_own_profile">
                                        <a href="javascript:void(0)" ng-click="editPublish(list_publish)" class="hover_btn">Edit</a>
                                        <a href="javascript:void(0)" ng-click="deletePost(list_publish, $event)" class="hover_btn">Delete</a>
                                        <a href="javascript:void(0)" ng-click="readPublish(list_publish)"  class="hover_btn">Read</a>
                                    </div>
                                    <img  id="profIMGID{{list_publish.hid}}" ng-show="list_publish.link != null" src="" ng-src="<?php echo base_url() ?>/uploads/hub_images/{{list_publish.link}}" alt="" class="summary_img">
									<!-- <img  id="profIMGID{{list_publish.hid}}" ng-show="list_publish.link === null" src="" ng-src="<?php echo base_url() ?>assets_phase2/images/no-image.jpg" alt="" class="summary_img"> -->
												
                                   
                                   <p ng-bind-html="list_publish.hub_title" class="pub_heading">User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                               
                                       <!--  <span class="artcl_autorname" ng-show="list_publish.link === null" ng-bind-html="list_publish.content| limitTo:340">By Author Name</span> -->
                                        <!-- <a href="javascript:void(0)" ng-show="list_publish.link === null">Read</a> -->
                                </div>
                                <!--- nilanjan It is for only text -->
                                <div class="summary_imginner" ng-if="list_publish.link == null" style="min-height:175px;" >
                                    <div class="hover_cont" ng-show="show_if_own_profile">
                                        <a href="javascript:void(0)" ng-click="editPublish(list_publish)" class="hover_btn">Edit</a>
                                        <a href="javascript:void(0)" ng-click="deletePost(list_publish, $event)" class="hover_btn">Delete</a>
                                        <a href="javascript:void(0)" ng-click="readPublish(list_publish)"  class="hover_btn">Read</a>
                                    </div>                             
                                                                               
                                 
                                   <p ng-bind-html="list_publish.hub_title" class="pub_heading">User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                               
                                     <div class="artcl_autorname" ng-bind-html="list_publish.content |limitTo: 300">By Author Name</div> 
                                 
                                    


                                </div>               





                               <!--  <div class="item" ng-if="list_publish.link != null">
                                    <div class="cararticl_top"> </div> 
                                    <div class="carartcl_body">
                                        
                                        <div class="hover_cont" ng-show="show_if_own_profile">
                                            <a href="javascript:void(0)" ng-click="editPublish(list_publish)" class="hover_btn">Edit</a>
                                            <a href="javascript:void(0)" ng-click="deletePost(list_publish, $event)" class="hover_btn">Delete</a> 
                                        
                                        </div>
                                        <p class="article_txt" ng-bind-html="list_publish.hub_title">Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business.</p>
                                        <span class="artcl_autorname" ng-bind-html="list_publish.content| limitTo:140">By Author Name</span>
                                        <a href="javascript:void(0)" class="article_read">Read</a>                                        
                                    </div>
                                    <div><a href="javascript:void(0)" ng-click="readPublish(list_publish)" class="hover_btn">Read</a></div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <!-- read publish -->
            <div class="modal fade" id="publish_details_read" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Publish</h4>
                        </div>
                        <div class="modal-body">
                        
                            <div class="publish_second publish_edit"> 
                               <!--  <div class="publish_toppart"  >
                                <i class="fa fa-camera change_photos" id="publish_img_icon" style="cursor:pointer" onclick="open_publish_crop_img_modal()"></i>  
                                    <img  id="image" src="" ng-if="hub.link!=null"  ng-src="<?php echo base_url() ?>/uploads/hub_images/{{hub.link}}" src="images/publish_img.jpg" alt="" >                                    
                                </div> -->
                                <div class="publish_toppart" ng-if="hub.link!=null" >
                                
                                    <img  id="image" src="" ng-if="hub.link!=null"  ng-src="<?php echo base_url() ?>/uploads/hub_images/{{hub.link}}" src="images/publish_img.jpg" alt="" >                                    
                                </div>
                                <div class="publish_actionarea">
                                    <!-- <input type="text" ng-model="publish_header" class="write_headline" placeholder="Write your headline" /> -->
                                    <h3 ng-bind-html="publish_header"></h3>

                                </div>                               
                                
                                <div >                                

                                    <div>                                    
                                        <p ng-bind-html="content_about_publish"></p>       
                                    </div>
                                    
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">                                                                           
                            <button class="btn btn_publish pull-right" data-dismiss="modal" ng-click="backToHub()">Back</button>
                            <!-- <button class="btn btn_publish pull-right" ng-click="rePublish()">Publish</button> -->
                        </div>
                    </div>
                </div>
            </div>
    <!-- read publish  end-->




            <!----- nilanjan marking for edit withdeitor -->
            <div class="modal fade" id="publish_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Publish</h4>
                        </div>
                        <div class="modal-body">
						
                            <div class="publish_second publish_edit"> 
                                                         
                                <!-- <div class="publish_toppart" ng-show="hub.link != null" > -->
                                <div class="publish_toppart">

                                <i class="fa fa-camera change_photos" id="publish_img_icon" style="cursor:pointer" onclick="open_publish_crop_img_modal()"></i>  
                                    <img id="image2"  ng-if="hub.link != null" src=""  ng-src="<?php echo base_url() ?>/uploads/hub_images/{{hub.link}}"  alt="" >                                    
                                
                                    <!-- <img  id="image2" src="" ng-if="hub.link ===null"  ng-src="<?php echo base_url() ?>/assets_phase2/images/no-image.jpg"  alt="" > -->
                                    <img  id="image2" src="" ng-if="hub.link ===null"    alt="" >
                                     <div class="publish_namecontainer">
                                            <div>
                                                <span ng-show="hub.logoimage == ''" class="publish_avator" ng-bind="hub.logobussinessname"></span>                                   
                                                <img ng-show="hub.logoimage != ''" ng-src="{{hub.logoimage}}" class="publish_avator" alt="User Image">
                                            </div>
                <!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                                            <div class="pulish_heading">
                                                <span class="publis_name" ng-bind="hub.bussinessname">Digitalweb</span>
                                                <p class="publish_subtxt" ng-bind-html="hub.tag_line">Production of modern website</p>
                                            </div> 
                                            <div class="publish_adduser" ng-show="connected">
                                                <i class="fa fa-user-plus" ng-click="connectionLogic(hub.hid)"></i>
                                            </div>
                                        </div>

                                </div>
                               
                                <div class="publish_actionarea">
                                    <input type="text" ng-model="publish_header" class="write_headline" placeholder="Write your headline" />


                                </div>
                                <div class="publishfooter publish_popupaction clearfix">                                           
                                            <ul class="latest_footerlink_right">
                                                <li><a href="javascript:void(0)"><span> {{hub.post_share_count}}</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a href="javascript:void(0)" ><span>{{hub.post_comment_count}}</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>{{hub.post_like_count}}</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                            <!-- {{hub.posted_on | date:"MM/dd/yyyy"}} -->
                                            <p class="pub_date">Published<span ng-bind="formatDate(hub.posted_on) |  date:'MM/dd/yyyy'"></span></p>


                                 </div>
  <!-- <style>
    .ta-editor {
       height: auto;
        overflow: auto;
        font-family: inherit;
        font-size: 100%;
    }
    </style>-->
                               <!--  <div class="publish_editor_txt">
                                    <div class="publish_editor">
                                       
                                       <div text-angular ng-model="content_about_publish" name="demo-editor1" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>          
                                    </div>
				 </div> -->

                 <div class="publish_editor">
                        <div text-angular ng-model="content_about_publish" name="demo-editor1" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
                       
                    </div>









                            </div>
                        </div>
                        <div class="modal-footer">  
                            <button class="btn btn_publish pull-right" ng-click="rePublish()">Publish</button>                                                                         
                            <button class="btn btn_publish pull-right" data-dismiss="modal" ng-click="backToHub()">Back</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- community activity -->
            <style>
                .imageBox2 {
    position: relative;
    height: 400px;
    width: 100%;
    border: 1px solid #aaa;
    background: #fff;
    overflow: hidden;
    background-repeat: no-repeat;
    cursor: move;
}
            </style>
            <!-- modal for popup popup photo upload start Nilanjan-->
          


            <div class="modal fade" id="crop_publish_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                           

                           <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Upload an Image</h4>
                                    </div>
                                    <div class="modal-body">
                                       <div class="container-crop">
                                            <div class="imageBox2" style="position: relative;height: 400px;width: 100%;border: 1px solid #aaa;background: #fff;overflow: hidden; background-repeat: no-repeat;cursor: move;">
                                                <div class="thumbBox2" style="position: absolute;top: 38%;left: 50%; width: 500px;height: 280px;margin-top: -100px;margin-left: -250px;box-sizing: border-box;border: 1px solid rgb(102, 102, 102);box-shadow: 0 0 0 1000px rgba(0, 0, 0, 0.5);background: none repeat scroll 0% 0% transparent;">

<!-- <div class="publish_toppart" ng-show="hub.link != null" >
                                    <img id="image"   src="<?php echo base_url() ?>/uploads/hub_images/{{hub.link}}"  alt="" onclick="open_publish_crop_img_modal()">                                    
                                </div> -->


</div>
                                                <div class="spinner" style="display: none">Loading...</div>
                                            </div>
                                            <div class="action">

                                            </div>
                                            <div class="cropped"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="note">image size 450 x 250  for best result</span>
                                        <input type="file" id="file2">                                               
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                        <button type="button" id="btnZoomIn2" class="btn btn-primary">+</button>
                                        <button type="button" id="btnZoomOut2" class="btn btn-primary">-</button>
                                        <button type="button" id="btnCrop2" class="btn btn-primary">Done</button>
                                    </div>
                                   <!--  <div class="publish_editor_txt">
                                    <div class="publish_editor box-body">
                                        <div text-angular ng-model="content_about_publish" name="demo-editor1" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>              
                                    </div>
                                    
                                </div> -->

                                </div>
                            </div>



                        </div>
                      
 <!-- modal for popup popup photo upload end-->


             <div class="summary box cummunity_activity">
                  <div class="box-header">
                  <i class="fa  fa-file-text"></i>
                  Community Activity                 
                </div>
                <div class="box-body">

                   <div class="mCustomScrollbar">

                  <div class="community_box" ng-repeat="community in communityResult">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the {{community.update_date}}</span></h6>
                   <strong>{{community.title}} </strong> 
        <p>{{community.content}}</p>
                  </div>
                  <!-- <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> LIVE TALK <span>/ Schedule or when it been started and date</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>
                  <div class="community_box">
                    <h6 class="community_heading"><i class="fa  fa-align-justify"></i> POST <span>/ Published the 12/04/2016 </span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div> -->
                  </div>

                </div>
                </div>
        </div>    
        <!-- tab Start -->


        <!-- all matching -->
        <div class="col-md-3 col-sm-12">
            <div class="right_sidebar">
                <div class="profile_complete box" ng-show="back_to_profile" >
                    <div class="box-header white">
                        <a href="javascript:void(0)" ng-click="backToProfile()"> Back to profile </a>              
                    </div>
                </div>
                <div class="profile_complete box" ng-show="show_if_own_profile">
                    <div class="box-header white">
                        <i class="fa  fa-circle-o"></i>
                        Profile Completion                 
                    </div>
                    <div class="progressbar_block">
                        <!-- <img src="<?php echo base_url(); ?>assets_phase2/images/progressbar.png" alt="">
                        <span class="progress_percnt" ng-bind="profileComplition">75%</span>-->
                        <input id="ex6" type="text" data-slider-min="0" data-slider-max="100" data-slider-step="1" data-slider-value="0" />
                        <span id="ex6CurrentSliderValLabel"><span class="profile_percent" id="ex6SliderVal"></span></span>
                    </div>

                </div>
                <div class="sm_half engagement_score" ng-show="show_if_own_profile">
                    <div class="summary box">
                        <div class="box-header">
                            <i class="fa  fa-bar-chart"></i>
                            Engagement Score

                        </div>
                        <div class="box-body text-center">
                            <img src="<?php echo base_url(); ?>assets_phase2/images/charts.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="sm_half connect_with" ng-show="show_if_own_profile">   
                    <div class="summary box">
                        <div class="box-header">
                            <i class="fa  fa-external-link-square"></i>
                            Connect With
                        </div>
                        <div class="box-body">
                            <div class="connect_businessinner">
                                <div id="connect_business" class="owl-carousel conect_bsn">
                                    <div class="item" ng-repeat="connected_list in suggested_connected_lists">
                                        <a href="javascript:void(0);"  ng-class="{'bg_yellow_md': connected_list.image_url == null}">
                                            <!--                                            <div class="pull-left msg_img bg_yellow_sm">-->
                                            <img id="hub_profile_default_img" ng-show="connected_list.image_url != null" ng-src="<?php echo base_url() ?>uploads/profile_image/{{connected_list.image_url}}" src="<?php echo base_url(); ?>assets_phase2/images/profile_avatar.png" class="business_avatar">
                                            <span ng-show="connected_list.image_url == null" ng-bind="connected_list.logobussinessname"></span>
                                            <!--                                            </div>-->
                                        </a>
                                        <div class="connect_details">
                                          <!--<a href="javascript:void(0);" class="business_name"  ng-href="<?php echo base_url() ?>viewprofile/viewOtherMemberProfileApi/{{connected_list.suggested_id}}"><strong ng-bind="connected_list.bussinessname">Name of the busines</strong></a>-->
                                            <a href="javascript:void(0);" class="business_name" ng-click="viewOtherProfile(connected_list.suggested_id);"><strong ng-bind="connected_list.bussinessname">Name of the busines</strong></a>
                                            <p ng-bind-html="connected_list.bussinessdesc| limitTo:140">Description of the businessto understand what they do we would like to have a small description of the bussiness as it appear on the summary than read more...</p>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="summary box latest_msg" ng-show="show_if_own_profile">
                    <div class="box-header">
                        <i class="fa  fa-envelope"></i>
                        Latest Messages                  
                    </div>
                    <div class=" latest_msg">
                        <div class="mCustomScrollbar">
                            <ul class="latestmsg_list" >
                                <li class="clearfix" ng-repeat="list_latest_message in list_latest_message_viewable"><!-- start message -->

                                    <div class="pull-left msg_img" ng-click="funRedirectToMsg();">
                                        <!--<a href="javascript:void(0)" class="online-status"><i class="fa fa-circle text-success"></i></a> -->
										 
                                        <img alt="" class="img-circle" ng-show="list_latest_message.image_url != null" ng-src="<?php echo base_url() ?>/uploads/profile_image/{{list_latest_message.image_url}}" src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" />
										
										<span class="bg_yellow_md" ng-show="list_latest_message.image_url == null" ng-bind="list_latest_message.logobussinessname"></span>
                                    </div>
                                    <a href="<?php echo base_url().'message'; ?>"><h4 ng-bind="list_latest_message.bussinessname"> Support Team </h4></a>
                                    <p class="profile_ltmsg" ng-bind-html="list_latest_message.msg_sub">Why not buy a new awesome theme?</p>
                                    <small class="msg_time"><i class="fa fa-clock-o"></i><span ng-bind="list_latest_message.posttime"></span></small>

                                </li><!-- end message -->                                 
                            </ul>
                        </div>
                    </div>
                </div>
            </div>  
        </div> 
    </div>
</section>
<!-- <script src="<?php echo base_url(); ?>assets_phase2/js/bootstrap-toggle.min.js"></script> -->
 <script src="<?php echo base_url(); ?>assets_phase2/js/bootstrap-toggle.js"></script>

<script src="<?php echo base_url(); ?>assets_phase2/js/viewMemberProfileApi.js"></script> 
<script type='text/javascript' src="<?php echo base_url(); ?>assets_phase2/js/bootstrap-slider.js"></script>
<script>

                                        (function ($) {
                                            $(window).load(function () {
                                                $("#content-1").mCustomScrollbar({
                                                    axis: "yx", //set both axis scrollbars
                                                    advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements
                                                    // change mouse-wheel axis on-the-fly 
                                                    callbacks: {
                                                        onOverflowY: function () {
                                                            var opt = $(this).data("mCS").opt;
                                                            if (opt.mouseWheel.axis !== "y")
                                                                opt.mouseWheel.axis = "y";
                                                        },
                                                        onOverflowX: function () {
                                                            var opt = $(this).data("mCS").opt;
                                                            if (opt.mouseWheel.axis !== "x")
                                                                opt.mouseWheel.axis = "x";
                                                        },
                                                    }
                                                });

                                            });
                                        })(jQuery);

                                        $(function () {
                                            $('.toggle').click(function (event) {
                                                event.preventDefault();
                                                var target = $(this).attr('href');
                                                $(target).toggleClass('hidden show');
                                            });
                                        });
</script>

<script type='text/javascript'>
    $(document).ready(function () {




        /* Example 6 */
        $("#ex6").slider();
        $("#ex6").on('slide', function (slideEvt) {
            $("#ex6SliderVal").text(slideEvt.value);

        });
        $("#ex6").slider("disable");

    });
</script>

<script>
  $(function() {
    $('#toggle-one').bootstrapToggle();
  })
</script>