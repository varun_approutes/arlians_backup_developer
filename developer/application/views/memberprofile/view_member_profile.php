<?php //echo $about_me; die(); ?>
<div class="dashboard-wrap" id="own_profile_section">
    <div class="row">
        <div class="fl100">
            <div class="compose-inner-bg profile-new-bg">
                <div class="profile-inner">
                    <h3><?php echo $bussinessname; ?></h3>
                   <!-- <p>Lorem ipsum dolor sit amet, consecteturadipiscing</p>-->
                    <?php if(!empty($has_founding_member)){?>
                        <div class="right-founder">
                            <i class="fa fa-star fa-2"></i>
                            <span><?php echo $fname.' '.$lname;?> <br> Member</span>
                        </div>
                    <?php }?>
                    <div class="profile-connection-inner">
                        <div class="details-title detail-title-headings">
                            <span><?php echo $tag_line;?></span>
                        </div>
                    </div>
					<div class="dashboard-wrap post-wrap" style="border-bottom:3px solid #cdcdcd;">

								<ul class="tabs">
								<li><a href="#about_the_team" class="active">Business Profile</a></li>
								<!--<li><a href="#tab2">About Me</a></li>-->
								<!--<li><a href="#tab2">My posts</a></li>-->
								<li><a href="#about_me">Team</a></li>
								</ul>
								
								<div id="about_the_team" class="tab-content">
                                    <div class="post-heading" style="padding:15px 0 15px 0; display:inline-block; width:100%;"><h2>Business Summary</h2></div>
                                    <p><?php echo $type = ($bussinessdesc == null) ? "Add Description" : $bussinessdesc; ?></p>
                                    <div class="post-heading"><h2>My latest posts</h2></div>
									<div class="post-inner">
										<?php if(count($latest_post>0)){?>
											<ul>
												<?php foreach($latest_post as $post){?>
                                                <?php //print_r($post);?>
													<li>
														<?php if($post['link']!=''){?>
														<a href="<?php echo base_url(); ?>hub/hubdetails/<?php echo $post["mid"].'/'.$post['hid'];?>">
                                                            <!--<a href="<?php echo base_url(); ?>hub/hubPublishEdit/<?php echo $post['hid']; ?>">-->
                                                                <img src="<?php echo base_url(); ?>uploads/hub_images/<?php echo $post['link']; ?>" alt=""/>
                                                            </a>
														<?php }
                                                        $content = $post['hub_title']!=null ? $post['hub_title']:$post['content'];
                                                        ?>   
														<h4><a href="<?php echo base_url(); ?>hub/hubdetails/<?php echo $post["mid"].'/'.$post['hid'];?>"><?php custom_echo($content,200); ?></a></h4>
														<p><?php echo  date("d F Y",strtotime($post['posted_on']));?></p>                        
													</li>
												<?php }?>
											</ul>
										<?php }else{?>
										<span>You have no recent post</span>
										<?php }?>
									</div>
								</div>
									<!--<div id="tab2" class="tab-content">
										<span style="color:#000">Lorem ipsum</span>
									</div>-->
                                    
								<div id="about_me" class="tab-content">
								<div class="post-heading" style="padding:15px 0 15px 0; display:inline-block; width:100%"><h2>About Me</h2></div>
                                    <div class="post-inner">
									    <div class="about-text">
                                        <?php echo $about_me; ?>
                                    </div>
                                    </div>
									<!--<div class="about_me"><?php echo $email;?></div>-->
								</div>
					</div>
					<div class="post-heading">
								<h2>Referral code</h2>
								</div>					
					<div class="profile-connection-inner">
                        <div class="details-title">
                            <span> <?php echo base_url();?>?q=<?php echo $sharecode;?></span>
                        </div>
                    </div>
                    <div class="profile-connection-inner">
                       <a class="manage_link" href="<?php echo base_url(); ?>memberprofile">Manage Profile</a>
                        <div class="details-title">
                            <i class="fa fa-map-marker fa-2"></i>
							 <ul class="location-info">
                        <li style="border-right: 1px solid #8e99a3;padding-right:8px">
                            <?php $office_name = $office_name==''?'N/A':$office_name;?>
                            <?php $countryName = country_name($country); $c_name = $countryName['country']; ($c_name != NULL)? $c_name : "Add Your Country"; ?>
                            
                            <span class="off_name" id="off_name"><?php echo $office_name?></span><br />
                            <span class="ct_name" id="ct_name"><?php echo $city ?></span><br />
                            <span class="cntry_name" id="cntry_name"><?php echo $c_name ?></span>
                            
                            
                        </li>
                        <!-- ko foreach: selected_more_address_list -->
                        <li style="border-right: 1px solid #8e99a3;padding-right:8px">
                            <!--<span id="loc" data-bind="text:city+','+country_name"></span>-->
                            <span class="off_name" data-bind="text:office_name==''?'N/A':office_name"></span><br />
                            <span class="ct_name" data-bind="text:city"></span><br />
                            <span class="cntry_name" data-bind="text:country_name"></span><br />
                           
                        </li>
                        <!-- /ko -->
                            <a href="#manage_new_address" style="display:none" id="add_new_add_link"></a>                        
                    </ul>
							
							
							
                            <!--<?php //$countryName = country_name($country); $c_name = $countryName['country']; ($c_name != NULL)? $c_name : "Add Your Country"; ?>
                            <h4>
							
                                <span id="loc"><?php //echo $city.",".$c_name; ?></span>
                                
                                    <span>|</span><span id="loc" data-bind="text:city+','+country_name"></span>
                                
                            </h4>-->
                        </div>
                        <div class="clear"></div>
                                                               
                        <div class="profile-conntection-left" style="padding-right:0;">
                            <div class="profile-connection-details">
                            <div class="profile-connection-col-left">
                                <div class="profile-connection-col">
                                    <h4 class="heading">Industries</h4>
                                </div>
                            </div>
                                 <div class="profile-connection-col-right">                                 
                                 <ul>
                                    <?php foreach($wiz_one_ans as $row){  
                                        $code = $row['ans_set_cat_id'];
                                        $arr = explode('-',$row['ans_set_cat_id']); 
                                        $top_line_industry_code = $arr[0];
                                        $top_line_industry_name = topline_name($top_line_industry_code);                        
                                        $bscode = $arr[1]; 
                                        $sectorName = sector_name($bscode); 
                                    ?>
                                    <li>
                                        <h4><?php echo $top_line_industry_name['top_name']; ?></h4>
                                    </li>
                                    <?php } ?>
                                <!--<div class="profile-connection-col">
                                    <h4><?php echo $sectorName['sector_name']; ?></h4>
                                </div>-->
                                </ul>
                                </div>
                            </div>
                            <div class="profile-connection-details">   
                                <div class="profile-connection-col-left">                               
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Sectors</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">
                                    <ul>
                                        <?php foreach($wiz_one_ans as $row){  ?>
                                            <?php $arr = explode('-',$row['ans_set_cat_id']);                             
                                            $bscode = $arr[1]; 
                                            $sectorName = sector_name($bscode); ?>                      
                                            <li>
                                                <h4><?php echo $sectorName['sector_name']; ?></h4>
                                            </li>                                   
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="profile-connection-details">                                
                                <div class="profile-connection-col-left">
                                <div class="profile-connection-col">
                                    <h4 class="heading">Sub-Sectors</h4>
                                </div>
                                </div>
                                <div class="profile-connection-col-right">                                 
                                 <ul>
                                <?php foreach($wiz_one_ans as $row){  ?>
                                    <?php $arr = explode('-',$row['ans_set_cat_id']); $bscode = $arr[2]; $subSectorName = sub_sector_name($bscode); ?>                      
                                    <li>
                                        <h4><?php echo $subSectorName['subsector_name']; ?></h4>
                                    </li>                                   
                                <?php } ?>                                
                                <!--<div class="profile-connection-col">
                                    <h4>Performing Art</h4>
                                </div>-->
                                 </ul>
                                </div>
                            </div>
                            <div class="profile-connection-details">
                            <div class="profile-connection-col-left">
                                <div class="profile-connection-col">
                                    <h4 class="heading">Business Type</h4>
                                </div>
                                </div>
                                <div class="profile-connection-col-right">                                 
                                 <ul>
                                <?php foreach($wiz_one_ans as $row){  ?>
                                    <?php $arr = explode('-',$row['ans_set_cat_id']); $bscode = $arr[3]; $b = bussiness_name($bscode); ?>                       
                                    <li>
                                        <h4><?php echo $b['bussiness_name']; ?></h4>
                                    </li>
                                <?php } ?>
                               
                                <!--<div class="profile-connection-col">
                                    <h4>Retailer</h4>
                                </div>-->
                                 </ul>
                                </div>
                            </div>
                        </div>
                        <div class="profile-conntection-right">
                            <div class="plus-connection">
                                <p>
                                <span data-bind="text:connection">500</span><br><span data-bind="text:connection>1?'Connections':'Connection'">Connections</span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="compose-inner-bg profile-new-bg">
                <div class="profile-inner">
				<div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Uploaded Document</h2></div>
                                             
                    <!--<p><?php echo $type = ($bussinessdesc == null) ? "Add Description" : $bussinessdesc; ?></p>-->
					<?php if(!empty($doc_uploaded)){?>
                          <div class="post-inner business-summary-inner all-post">
                            <ul>
                                <?php $count =0;?>
                                <?php foreach($doc_uploaded as $doc){
                                    $ext = pathinfo($doc['file_name'], PATHINFO_EXTENSION);
                                    if($ext=='jpg'|| $ext == 'png' || $ext == 'jpeg' || $ext == 'gif' || $ext == 'bmp'){?>
                                        <li>
                                            <a class="video_popup" href="#image_<?php echo $count;?>" >
                                                <img class="example-image" src="<?php echo base_url().$doc['path'].$doc['file_name']?>" alt="image-1" />
                                            </a>                                            
                                            <div id="image_<?php echo $count;?>" class="edit-profile document-img-open" style="display:none">
                                                <div class="popup-doc-title"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div>
                                                <img class="example-image" src="<?php echo base_url().$doc['path'].$doc['file_name']?>" alt="image-1" />                                                
                                            </div>
                                            <div class="doc-title-wrap"><div class="doc-title-inner"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div></div>
                                        </li> 
                                    <?php }else if($ext=='mp4'){?> 
                                        <li>                                            
                                            <a class="video_popup" href="#video_<?php echo $count;?>" >
                                                <img src="<?php echo base_url()?>assets/images/video-icon.png">                                               
                                            </a>
                                            <div class="doc-title-wrap"><div class="doc-title-inner"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div></div>
                                        </li>
                                        <div id="video_<?php echo $count;?>" class="edit-profile video-wrap" style="display:none">   
                                        <div class="popup-doc-title"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div>    
                                            <video width="600" controls>
                                                <source src="<?php echo base_url().$doc['path'].$doc['file_name']?>" type="video/mp4">  
                                                Your browser does not support HTML5 video.
                                            </video>                                            
                                        </div>                                         
                                    <?php }else if($ext=='pptx' || $ext=='pdf'){?>  
                                        <li>
                                            <a class="video_popup" href="#video_<?php echo $count;?>" >
                                                <?php if($ext=='pptx'){ ?>
                                                    <img src="<?php echo base_url()?>assets/images/ppt-file.png">
                                                <?php }else{ ?>
                                                    <img src="<?php echo base_url()?>assets/images/pdf-file.png">
                                                <?php } ?>                                                
                                            </a>                                            
                                            <div class="doc-title-wrap"><div class="doc-title-inner"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div></div>
                                        </li>   
                                        <div id="video_<?php echo $count;?>" class="edit-profile video-wrap" style="display:none">       
                                            <div class="popup-doc-title"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div>
                                            <iframe src="http://docs.google.com/gview?url=<?php echo base_url().$doc['path'].$doc['file_name']?>&embedded=true" style="width:600px; height:500px;" frameborder="0"></iframe>                                            
                                        </div>
                                    <?php }else{?>
                                        <li>
                                            <a href="<?php echo base_url().$doc['path'].$doc['file_name']?>" target="_blank"><div class="document-icon"><img src="<?php echo base_url()?>assets/images/other-icon.png"></div></a>                      
                                            <div class="doc-title-wrap"><div class="doc-title-inner"><?php echo $doc['file_title'] != ''?$doc['file_title']:$doc['file_name']?></div></div>
                                        </li>
                                    <?php }?> 
                                    <?php $count++;?>   
                                <?php }?>
                            </ul>
                        </div>
                      <?php }?>
                    
                    <!--<ul class="businnes-summary">
                        <li>
                            <img src="<?php echo base_url()?>assets/images/summary-img-one.png" alt="" />
                            <div class="business-summary-heading">Finally a social network that works for business!</div>
                        </li>
                        
                    </ul>-->
                </div>
            </div>
            
            <div class="compose-inner-bg profile-new-bg">
                <div class="profile-inner">
					<div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Business objectives</h2></div>
                    
                    <div class="profile-connection-inner">
                        <div class="profile-conntection-left MAR-TOP">
                            <div class="profile-connection-details">
                                <div class="profile-connection-col">
                                    <h4 class="heading">What are you looking for ?</h4>
                                </div>
                                <div class="profile-connection-col">
                                    <?php foreach($questionone as $row){ ?>
                                    <h4><?php echo $row; ?></h4>
                                    <?php } ?>
                                    <!--<h4>Find suppliers Look for non-executive directors <a class="edit-sec"><i class="fa fa-pencil"></i></a> </h4>-->
                                </div>
                            </div>
                            <div class="profile-connection-details">
                                <div class="profile-connection-col">
                                    <h4 class="heading">Where do you want to explore?</h4>
                                </div>
                                <div class="profile-connection-col">
                                      <?php foreach($locality_to_explore as $row){ ?>
                                      <h4><?php echo $row->locality_type; ?></h4>
                                      <?php } ?>
                                    <!--<h4>Global <a class="edit-sec"><i class="fa fa-pencil"></i></a> </h4>-->
                                </div>
                            </div>
                        </div>
						              
                    </div>
                </div>
            </div>
            <div class="compose-inner-bg profile-new-bg">
                <div class="profile-inner">
                    <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Who are your customers?</h2></div>
					  <div class="profile-connection-inner MAR-TOP">
                        <div class="profile-conntection-left MAR-TOP" style="padding-right:0;">
                            <div class="profile-connection-details">
                                <div class="profile-connection-col-left">
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Customer Type</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">                                 
                                    <ul>
                                    <?php foreach($customer_type as $row){ ?>                                     
                                        <li style="border:none; min-height:inherit; padding-bottom:0;">
                                          <h4><?php echo $row->service_name; ?> </h4>
                                        </li>
                                    <?php } ?>
                                    <!--<div class="profile-connection-col">
                                        <h4>Business <a class="edit-sec"><i class="fa fa-pencil"></i></a> </h4>
                                    </div>-->
                                   </ul>
                               </div>
                            </div>
                             <div class="profile-connection-details">
                                <div class="profile-connection-col-left">
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Industries</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">                                 
                                    <ul>
                                    <?php foreach($wiz_two_quiz_four_and_five_selected_biz_ids as $industry){?>
                                        <li>
                                            <h4><?php echo $industry['industry_name']?></h4>
                                        </li>
                                    <?php }?>  
                                    <!--<div class="profile-connection-col">
                                        <h4>Music, Art & Culture</h4>
                                    </div>-->
                                    </ul>
                               </div>
                            </div>
                            <div class="profile-connection-details">
                                <div class="profile-connection-col-left">
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Sectors</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">                                 
                                <ul>
                                    <?php foreach($wiz_two_quiz_four_and_five_selected_biz_ids as $target_sector){?>  
                                        <li>
                                            <h4><?php echo $target_sector['tar_sector_name']?></h4>
                                        </li>                               
                                    <?php }?>
                                    <!-- <div class="profile-connection-col">
                                        <h4>Music and Movie Production</h4>
                                    </div>-->
                                </ul>
                               </div>
                            </div>
                            <div class="profile-connection-details">
                                <div class="profile-connection-col-left">
                                    <div class="profile-connection-col">
                                        <h4 class="heading">Function you serve</h4>
                                    </div>
                                </div>
                                <div class="profile-connection-col-right">                                 
                                    <ul>
                                    <?php foreach($looking_service as $row){ ?>
                                        <li style="border:none; min-height:inherit; padding-bottom:0;">
                                            <h4><?php echo $row->look_service_name; ?> </h4>
                                        </li>                               
                                    <?php }?>
                                    <!-- <div class="profile-connection-col">
                                        <h4>General Management</h4>
                                    </div>-->
                                     </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- -------------fancy box div-------------------- --> 

<div id="div3" class="edit-profile" style="display:none"></div>
<div id="div4" class="edit-profile" style="display:none"></div>

<div id="div5" class="edit-profile" style="display:none">

<input type="text" name="city_name" id="city_name">

<?php 
$country_list = getCountryList();

?>
<select name="country_name" id="country_name">
<?php foreach($country_list as $c){ ?>
<option value="<?php echo $c->id; ?>"><?php echo $c->country_name; ?></option>
<?php } ?>
</select> 
<input type="button" id="pr_loc_submit">
</div>

<div id="div6" class="edit-profile" style="display:none"></div>
<div id="div7" class="edit-profile" style="display:none"></div>

<script type="text/javascript">

var vmProfile = {
     connection:ko.observable(0),
     selected_more_address_list:ko.observableArray(),
}
 $(".post-inner ul li a img").load(function() {
		var max_size = 180;
		$(".post-inner ul li a img").show();
		$(".post-inner ul li a img").each(function(i) {
		  if ($(this).height() > $(this).width()) {
			var h = max_size;
			var w = Math.ceil($(this).width() / $(this).height() * max_size);
		  } else {
			var w = max_size;
			var h = Math.ceil($(this).height() / $(this).width() * max_size);
		  }
		  $(this).css({ height: h, width: w });
		});
   });
 $(document).ready(function() {
$(".post-inner ul li a img").hide();	 
    //$("#img_upload").fancybox();
    //$("#back_image").fancybox();
  
 });
 
$('#uploadImg').click(function(){
  var v = $('#blah').attr('src');
  var x = v.split('/');

  if(x[7] == 'profile-img.png'){
    $('#error_img').html('<p>Please select a picture to upload</p>');
  }else{
    $.post('<?php echo base_url(); ?>memberprofile/uploadImage',{'imgFile':v},function(result){
      console.log(result);
      if(result==true)
        $('.fancybox-item').trigger('click');
      else{ $('.fancybox-item').trigger('click'); }
    });
    
  }
});

$('#uploadCoverImg').click(function(){
  var v = $('#blah1').attr('src');
  var x = v.split('/');

  if(x[7] == 'profile-img.png'){
    $('#error_img1').html('<p>Please select a picture to upload</p>');
  }else{
    $.post('<?php echo base_url(); ?>memberprofile/uploadBackImage',{'imgFile':v},function(result){
      //console.log(result);
      if(result==true)
        $('.fancybox-close').trigger('click');
      else{ $('.fancybox-item').trigger('click'); }
    });
    
  }
});
 
 function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah')
                    .attr('src', e.target.result)
                    .width(152)
                    .height(152);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  
  function readBackURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah1')
                    .attr('src', e.target.result)
                    .width(1200)
                    .height(190);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
  
  //--------------------------
  function getMoreAddress(){
    $("#loader-wrapper").show();
    $.post('<?php echo base_url(); ?>memberprofile/getMoreAddress',{},function(result){
        $("#loader-wrapper").hide();
        result = eval("(" + result + ")");
        //console.log(result);
        vmProfile.selected_more_address_list(result);
    });
  }
  $(document).ready(function(){
    getMoreAddress();
    try{
        $('#loader-wrapper').show();
        $.post('<?php echo base_url(); ?>memberprofile/getOwnNetworkConnectionList',{},function(result_connection){
            $('#loader-wrapper').hide();
            var result_connection_list = eval("(" + result_connection + ")");
            var recieved = 0;
            for(var i=0; i< result_connection_list.received_arr.length;i++){              
                recieved++;
            }
            vmProfile.connection(recieved);
        });
    }catch(e){
        alertify.error(e);
    }
    
    //----------------------- Edit Profile Name---------------------------
    var x = $('#pr_name').parent('h3').first().text();
    var html = "<input type='text' id='e_name' value='"+x+"'><br/><input type='button' id='e_name_submit' value='Submit'>";
    
    $('#div3').html(html);
    
    $('#pr_name').fancybox();
    
    $('#e_name_submit').click(function(){
      var ed_name = $("#e_name").val();
      //console.log(ed_name);
      $.post('<?php echo base_url(); ?>memberprofile/editName',{'name':ed_name},function(res){
        $('.fancybox-close').trigger('click');
        $('.profile-inner').children('h3').html(ed_name+'<a id="pr_name" href="#div3"><i class="fa fa-pencil"></i></a>');
      });
    });
    //-----------------------------------------------------------------------
    //----------------------- Edit Bussiness Name ---------------------------
    var bussinessName = $('#pr_buss').parent('span').first().text();
    var html = "<input type='text' id='bussiness_name' value='"+bussinessName+"'><br/><input type='button' id='e_bussiness_name_submit' value='Submit'>";
    $('#div4').html(html);
    $('#pr_buss').fancybox();
    
    $('#e_bussiness_name_submit').click(function(){
      var ed_bussiness_name = $("#bussiness_name").val();
      //console.log(ed_name);
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessName',{'name':ed_bussiness_name},function(res){
        $('.fancybox-close').trigger('click');
        $('.profile-inner').first().children('span').html(ed_bussiness_name+'<a id="pr_buss" href="#div4"><i class="fa fa-pencil"></i></a>');
      });
    });
    //-----------------------------------------------------------------------
    //----------------------- Edit Location ----------------------------------
    var location = $('#loc').text();
    var arr = location.split(' ');
    
    $('#city_name').val(arr[0]);
    $('#country_name').val(arr[1]);
    
    $("#pr_loc").fancybox();
    $('#pr_loc_submit').click(function(){
      var cityname = $('#city_name').val();
      var countryname = $('#country_name').val();
      
      //alert(cityname+' '+countryname);
      $.post('<?php echo base_url(); ?>memberprofile/editMemberLocation',{'country':countryname,'city':cityname},function(res){
        $('.fancybox-close').trigger('click');
        $('#loc').html(cityname+' '+countryname);
      });
      
    });
    //-----------------------------------------------------------------------
    
    //-------------------------------------- Edit Bussiness Type-------------------------
    var x = $('#buss_type').text();
    var html = "<input type='text' id='bussiness_type_name' value='"+x+"'><br/><input type='button' id='bussiness_type' value='Submit'>";
    $('#div6').html(html);
    $('.video_popup').fancybox();
    $('#pr_bussiness_type').fancybox();
    
    $('#bussiness_type').click(function(){
      var bussiness_type_name = $("#bussiness_type_name").val();
      
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessType',{'name':bussiness_type_name},function(res){
        $('.fancybox-close').trigger('click');
        $('#buss_type').text(bussiness_type_name);
      });
    });
    
    //-----------------------------------------------------------------------------------
    //-------------------------------------------- Edit Bussiness Summery ------------------
      
      var x = $('#pr_desc').text();
      //alert(x);
      var html = "<textarea id='edit_bussiness_summery'>"+x+"</textarea><br/><input type='button' id='edit_bussiness_summery_submit' value='Submit'>";
      $('#div7').html(html);
      
      $('#bussiness_summery').fancybox();
      
      $('#edit_bussiness_summery_submit').click(function(){
        
        var bussiness_summery = $("#edit_bussiness_summery").val();
        
      $.post('<?php echo base_url(); ?>memberprofile/editBussinessSummery',{'name':bussiness_summery},function(res){
        $('.fancybox-close').trigger('click');
        $('#pr_desc').text(x);
      });
        
      });
      
    //--------------------------------------------------------------------------------------
    $('ul.tabs').each(function(){
		// For each set of tabs, we want to keep track of
		// which tab is active and it's associated content
		var $active, $content, $links = $(this).find('a');

		// If the location.hash matches one of the links, use that as the active tab.
		// If no match is found, use the first link as the initial active tab.
		$active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
		$active.addClass('active');

		$content = $($active[0].hash);

		// Hide the remaining content
		$links.not($active).each(function () {
			$(this.hash).hide();
		});

		// Bind the click event handler
		$(this).on('click', 'a', function(e){
			// Make the old tab inactive.
			$active.removeClass('active');
			$content.hide();

			// Update the variables with the new link and content
			$active = $(this);
			$content = $(this.hash);

			// Make the tab active.
			$active.addClass('active');
			$content.show();

			// Prevent the anchor's default click action
			e.preventDefault();
		});
	});
  });
     ko.applyBindings(vmProfile, $("#own_profile_section")[0]);
</script>