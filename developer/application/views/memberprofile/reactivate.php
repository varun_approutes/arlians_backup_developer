<?php $member_type = check_member_type();?>
<?php $split_date = explode(" ",$user['deactivate_date']);?>
<?php $date = explode("-",$split_date[0]);?>
<?php $deactivated_date = $date[2].'/'.$date[1].'/'.$date[0];?>
<div class="premium-cancel-wrap" id="reactive_profile_section">
    <h2>Membership Reactivation</h2>
    <a class="delect-account suspend-account">Reactivate my account</a>
    <p class="suspend-mess">Your [<?php echo $member_type;?>] membership was suspended on [<?php echo $deactivated_date;?>].<br><br>
    To reactivate your account, follow the instruction below.</p>
    <!--<div class="select-premium">
        <h3>Are you sure you wish to continue?</h3>
        <div class="reactivate-premium">
            <label>Please reactivate my account as:</label>
            <ul>
                <li><input type="radio" name="radio" id="radio">FREE MEMBERSHIP</li>
                <li><input type="radio" name="radio" id="radio">PREMIUM MEMBERSHIP</li>
            </ul>
            <a href="#" class="member-click-link">Click here for more information</a>
        </div>
    </div>-->
    <form id="reactive_account_form" method="post">
        <div class="account-delet-form">
            <ul>
                <li>
                    <div class="account-delet-left">
                        <label>Enter your password</label>
                    </div>
                    <div class="account-delet-right">
                        <input type="password" name="password" data-bind="value:current_password" id="password">
                    </div>
                </li>
                <li>
                    <div class="account-delet-left">
                        <label>Re-enter your password</label>
                    </div>
                    <div class="account-delet-right">
                        <input type="password" name="rewrite_password" id="rewrite_password">
                    </div>
                </li>
                <li>
                    <div class="account-delet-right">
                        <input type="submit" value="Submit" id="submit">
                    </div>
                </li>
            </ul>
        </div>
    </form>    
</div>
<script>
    var vmReactiveProfile = {
        current_password:ko.observable(),
    }
  $(function(){
    $("#reactive_account_form").validate({
            rules: {
                password: { required: true },
                rewrite_password: { required: true,equalTo: "#password" }
            },
            messages: {
                password: { required: "Please enter your password" },
                rewrite_password: { required: "Please confirm your password" ,equalTo:"Does not match with password"}
            },
            submitHandler: function (form) {                 
                $('#loader-wrapper').show();
                $.post('<?php echo base_url(); ?>memberprofile/validateCurrentPassword',{password:vmReactiveProfile.current_password()},function(result){                 
                    if(result=='TRUE'){                        
                        $.post('<?php echo base_url(); ?>memberprofile/reactiveAccount',{},function(result_data){
                            $('#loader-wrapper').hide();
                            if(result_data==1){
                                window.location= "<?php echo base_url();?>member/dashboard";         
                            }else{
                                alertify.error('Some error occured please try again later.');
                            }
                        });
                    }else{
                        alertify.error("You enter a wrong credential!");
                    }

                });
                return false;
            }
        });
  });
  ko.applyBindings(vmReactiveProfile, $("#reactive_profile_section")[0]);
</script>