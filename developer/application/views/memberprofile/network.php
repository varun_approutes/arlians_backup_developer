<div class="dashboard-wrap" id="network_section">
    <div class="row">
        <div class="fl68">
            <div class="compose-inner-bg profile-bg">
                <div class="profile-inner">
                    <h3>Connection</h3>
                    <ul class="profile-connect" data-bind="foreach:connected_list">
                      <li>
                          <div class="inbox-img"> 
                              <a href="javascript:void(0);" data-bind="attr:{href:'<?php echo base_url();?>viewprofile/profile/'+mid">
                                  <img alt="" data-bind="attr:{src:image_url !=null ?'<?php echo base_url();?>uploads/profile_image/'+image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}">
                              </a>                        
                          </div>
                          <div class="inbox-content">
                              <h4 data-bind="text:bussinessname">bussinessname</h4>
                              <p class="date" data-bind="text:bussinesstype"> bussinesstype</p>
                          </div>
                          <div class="right-inbox-icon">
                              <!-- ko if:is_accepted==1 -->
                                <a class="icon-border" data-bind="click:$root.removeConnectionRequest"><i class="fa fa-times fa-1"></i></a>                             
                              <!-- /ko -->
                              <!-- ko if:is_accepted==0 -->
                                <span>Request sent</span>   
                              <!-- /ko -->                          
                          </div>
                      </li>                                            
                    </ul>
                    <ul class="profile-connect" data-bind="visible:connected_list().length==0">
                        <li>
                          <div style="color:#000;">Currently we have no suggestion for you</div>
                        </li>
                    </ul>  
                    </li>
                </div>
            </div>
        </div>       
    </div>
    <!--<div class="row">
        <div class="fl68">
            <div class="compose-inner-bg profile-bg">
                <div class="profile-inner">
                    <h3>Suggestion</h3>
                    <ul class="profile-connect" data-bind="foreach:suggested_list">
                    <li>
                        <div class="inbox-img"> 
                            <a href="javascript:void(0);" data-bind="attr:{href:'<?php echo base_url();?>viewprofile/profile/'+mid">
                                <img alt="" data-bind="attr:{src:image_url !=null ?'<?php echo base_url();?>uploads/profile_image/'+image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}">
                            </a>                        
                        </div>
                        <div class="inbox-content">
                            <h4 data-bind="text:bussinessname">bussinessname</h4>
                            <p class="date" data-bind="text:bussinesstype"> bussinesstype</p>
                        </div>
                        <div class="right-inbox-icon">
                            <a href="javascript:void(0);" class="icon-border" data-bind="click:$root.connectionRequest"><i class="fa fa-check fa-1"></i></a>                             
                        </div>
                    </li>                                            
                    </ul>
                    <ul class="profile-connect" data-bind="visible:suggested_list().length==0">
                        <li>
                          <div style="color:#000;">Currently we have no suggestion for you</div>
                        </li>
                    </ul>  
                    </li>
                </div>
            </div>
        </div>       
    </div>  -->  
</div>

<script type="text/javascript">
    var vmNetwork={
        connected_list:ko.observableArray(),
        suggested_list:ko.observableArray(),
        connectionRequest:function(){  
          var curObj = this;
          curObj['is_accepted'] = 0; 
          $('#loader-wrapper').show();
          $.post('<?php echo base_url(); ?>memberprofile/connectionRequest',{connected_id:curObj.mid},function(result){
            $('#loader-wrapper').hide();
            if(result==1){
              alertify.success('Request Sent');
              vmNetwork.connected_list.push(curObj);              
              vmNetwork.suggested_list.remove(function (item) {
                return curObj.mid == item.mid;
              });
            }else{
              alertify.error('Some error occured please try again later!')
            }
          });  

        },
        removeConnectionRequest:function(){
          var curObj = this;               
          alertify.confirm("Are you sure to remove '"+curObj.bussinessname+"'", function (e) {
            if (e) {
              $('#loader-wrapper').show();
              $.post('<?php echo base_url(); ?>member/deleteSuggestion',{cid:curObj.cid},function(result){
                $('#loader-wrapper').hide();
                vmNetwork.connected_list.remove(function (item) {
                  return curObj.mid == item.mid;
                });
              });
            } else {
              alertify.error("You've clicked Cancel");
            }
          });
          
        }
    }
    $(document).ready(function() {   

        try{
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>memberprofile/getNetworkSuggestedList',{},function(result_suggested){
                $('#loader-wrapper').hide();
                var result_suggested_list = eval("(" + result_suggested + ")");
                vmNetwork.suggested_list(result_suggested_list);
                //console.log(result_suggested);
            });
        }catch(e){
            alertify.error(e);
        }
        try{
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>memberprofile/getNetworkConnectionList',{},function(result_connection){
                $('#loader-wrapper').hide();
                var result_connection_list = eval("(" + result_connection + ")");
                vmNetwork.connected_list(result_connection_list.connection);
                //console.log(result_suggested);
            });
        }catch(e){
            alertify.error(e);
        }
        
    });
    ko.applyBindings(vmNetwork, $("#network_section")[0]);
</script>