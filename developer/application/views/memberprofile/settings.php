<div class="row settings_tab" ng-controller="settingsCtrl">      
    <!-- settings page -->
    <div class="col-md-12 col-sm-12" id="parentVerticalTab">

        <ul class="resp-tabs-list tab-settings hor_1">
            <li><a href="javascript:void(0);"  class="hvr-bounce-to-right" ><i class="fa fa-gear"></i> User Settings</a></li>
            <li><a href="javascript:void(0);"  class="hvr-bounce-to-right" ><i class="fa fa-home"></i> Account Details </a></li>
			 <li><a href="javascript:void(0);"  class="hvr-bounce-to-right" ><i class="fa fa-envelope"></i> Preferences</a></li>
<!--            <li><a href="javascript:void(0);"  class="hvr-bounce-to-right"> <i class="fa  fa-tachometer"></i> Subscrition and Payment</a></li>
            <li><a href="javascript:void(0);" class="hvr-bounce-to-right"> <i class="fa fa-lock"></i> Privacy</a></li>
            <li><a href="javascript:void(0);" class="hvr-bounce-to-right"> <i class="fa fa-gears"></i> Advanced Settings</a></li>
            <li><a href="javascript:void(0);" class="hvr-bounce-to-right"> <i class="fa  fa-credit-card"></i> Wallet</a></li>-->
        </ul>



        <div class="resp-tabs-container hor_1">            
            <div class="summary box" >
                <div class="box-header">
                    <i class="fa fa-gears"></i>
                    User Settings                   
                </div>
                <div class="box-body">
                    <div class="wizard_inner">
                        <div class="wizard_hading">
                            <h6><i class="fa fa-gear"></i> Your User Settings Overview</h6>
                            <p>You can modify your business settings here. </p>
                        </div>
                        <div class="wiz_content">
                            <span class="setting_subheading">
                                Matching engine preferences
                            </span>
                            <div class="usersettings_inner">
                                <form>
                                    <!--                                    <div class="form-group">
                                                                            <input type="text" class="form-control" id="inputEmail3" placeholder="">
                                                                        </div>-->
                                    <div class="row">
                                        <div class="col-md-6"> 
                                            <button type="submit" ng-click="descriptionSetting()" class="btn payment_btn hvr-radial-out ">Business description and settings</button>
                                        </div>
                                        <div class="col-md-6"> 
                                            <button type="submit" ng-click="matchigSetting()" class="btn payment_btn hvr-radial-out" ng-class="{active:MatchFlag ==='YES' && MatchFlag!=null}">Matching settings</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            <span class="setting_subheading">
                                Your Target Sector
                            </span>
                            <div class="usersettings_inner">
                                <div class="art_culture">                                              
                                    <ul class="sector_btn clearfix">
                                        <li class="blue_circlebtn" ng-repeat="targer_sub in user_target_sub_sector"><span ng-bind="targer_sub.targetsubname"></span> <i class="fa fa-close" ng-click="deleteTargetSector(targer_sub, $event)"></i></li>                                                        
                                    </ul>
                                </div>
                            </div>   
                            <span class="setting_subheading">
                                Your Sector
                            </span>
                            <div class="usersettings_inner">
                                <div class="art_culture">                                              
                                    <ul class="sector_btn clearfix">
                                        <li class="blue_circlebtn"  ng-repeat="own_sub in user_own_sub_sector" >
                                            <span ng-bind="own_sub.subname"></span><i class="fa fa-close" ng-click="openModalforsubsector(own_sub)" ></i>
                                        </li>                                                        
                                    </ul>
                                </div>
                            </div>   

<!--                            <div class="wiz_validate"><a href="javascript:void(0)">save <i class="fa fa-long-arrow-right "></i></a></div>-->
                        </div>

                        <div class="modal fade payment_modal" id="selected_sub_sector" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Your business details</h4>

                                    </div>
                                    <div class="modal-body">                                        

                                        <div class="step3">

                                            <div class="art_culture">  
                                                <div class="art_culture clearfix">                                                   
                                                    <ul class="subtag_select">
                                                        <li ng-repeat="sub in selected_sub_sector">
                                                            <span ng-bind="sub.topline_name"></span>&nbsp;|&nbsp;
                                                            <span ng-bind="sub.sector_name"></span>&nbsp;|&nbsp;
                                                            <span ng-bind="sub.sub_sector_name"></span>&nbsp;|&nbsp;
                                                            <span ng-bind="sub.bussiness_name"></span>&nbsp;|&nbsp;
                                                            <i class="fa fa-close" ng-click="deleteSubSector(sub, $event)"></i>
                                                        </li> 
                                                    </ul>
                                                </div>                                                 
                                            </div>                                           
                                        </div>
                                    </div> 

                                    <div class="modal-footer">                                       
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>                                   

                                </div>

                            </div>
                        </div>

                        <div class="modal fade payment_modal" id="wizoneModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" ng-show="wizardOne.show_buisness_list">My Business</h4>
                                        <!--                                        <h4 class="modal-title" ng-show="wizardOne.show_top_list">Who are your customers?</h4>
                                                                                <h4 class="modal-title" ng-show="wizardOne.show_sub_sector_list">Who are your customers?</h4>-->
                                    </div>
                                    <div class="modal-body">

                                        <div class="step3" ng-show="wizardOne.show_buisness_list">
                                            <div class="art_culture">                                              
                                                <ul class="subtag_select clearfix">
                                                    <li onclick="animatedSubSector(this)" ng-repeat="biz in buisness_list" ng-click="setBuisness(biz)" class="sub_sector" ng-bind="biz.bussiness_name">Tag name sub</li>                                                        
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="step3 mCustomScrollbar" ng-show="wizardOne.show_top_list">
                                            <div class="section__content clearfix">
                                                <div  ng-repeat="top in buisness_top_lists">
                                                    <div class="card effect__click" onclick="animationBuisnessTopLines(this)" ng-click="setTopList(top)">
                                                        <div class="card__front">
                                                            <div class="card_img">
                                                                <img ng-src="<?php echo base_url() ?>uploads/profile_image/{{top.profile_image}}" alt="">
                                                            </div>
                                                            <span class="card__text" ng-bind="top.top_name">Art &amp; Culture</span>
                                                        </div>
                                                        <div class="card__back">
                                                            <div class="card_img">
                                                                <i class="fa fa-close close_card"></i>
                                                                <div class="selected">Selected</div>
                                                                <img ng-src="<?php echo base_url() ?>uploads/profile_image/{{top.profile_image}}" alt="">
                                                            </div>
                                                            <span class="card__text" ng-bind="top.top_name">Art &amp; Culture</span>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div> 
                                        </div>  
                                        <div class="step3 mCustomScrollbar" ng-show="wizardOne.show_sub_sector_list">
                                            <div class="art_culture">  
                                                <div class="art_culture clearfix" ng-repeat="sub in sub_sector_list">
                                                    <h1 ng-bind="sub.toplinename">Art &amp; Culture </h1>
                                                    <ul class="subtag_select">
                                                        <li ng-repeat="sector in sub.subsector" onclick="animatedSubSector(this)" class="sub_sector" ng-click = "setUserSelectedSubSector(sector)" ng-bind="sector.name">Tag name sub</li> 
                                                    </ul>
                                                </div> 

                                            </div>
                                        </div>
                                    </div> 
                                    <div class="modal-footer" ng-show="wizardOne.show_buisness_list">                                        
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                                        <button type="button" class="btn btn-primary" ng-click="openTopList()">Next</button>
                                    </div>
                                    <div class="modal-footer" ng-show="wizardOne.show_top_list">                                        
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                                        <button type="button" class="btn btn-primary" ng-click="showBuisnessList()">Back</button>
                                        <button type="button" class="btn btn-primary" ng-click="showSubSectorSection()">Next</button>
                                    </div>
                                    <div class="modal-footer" ng-show="wizardOne.show_sub_sector_list">                                        
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> 
                                        <button type="button" class="btn btn-primary" ng-click="openTopList()">Back</button>
                                        <button type="button" class="btn btn-primary" ng-click="sendCombinationCode()">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="modal fade payment_modal" id="myWizTwoFirstModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_wizard_notice">A world of business connections</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_industry_list">Who are your customers?</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_target_sector">in the field of…..</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_looking_for">Finally, tell us what you are looking for.</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_locality">Your target locality</h4>
                                        <!--                                        show_services<h4 class="modal-title" id="myModalLabel" ng-show="show_services">Do you service</h4>-->
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_target_list">and tell us about the people …..</h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <div class="step3" ng-show="show_wizard_notice">
                                            <div class="art_culture"> 
                                                <h3>Let’s set up your business matching engine</h3>
                                                <p>We will ask you 4 questions and it should take less than a minute. Don't worry though, we won't ask any thing sensitive and your answer won't be visible to other members</p>
                                            </div>
                                        </div>
                                        <div  ng-show="show_industry_list" class="mCustomScrollbar">
                                            <div class="section__content clearfix">
                                                <div class="card effect__click industry_list" onclick="animationBuisnessTopLines(this)" ng-click="setIndustryCode(industry)" ng-repeat="industry in industry_list_for_wizard_two">
                                                    <div class="card__front">
                                                        <div class="card_img">
                                                            <img ng-src="<?php echo base_url() ?>uploads/profile_image/{{industry.profile_image}}" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="industry.industry_name">Art &amp; Culture</span>
                                                    </div>
                                                    <div class="card__back">
                                                        <div class="card_img">
                                                            <i class="fa fa-close close_card"></i>
                                                            <div class="selected">Selected</div>
                                                            <img ng-src="<?php echo base_url() ?>uploads/profile_image/{{industry.profile_image}}" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="industry.industry_name">Art &amp; Culture</span>
                                                    </div>
                                                </div>
                                                <div class="card effect__click category wiz_2_hid_card"></div>
                                                <div ng-show="type == 'insert_new_customer_type'" class="card effect__click category" onclick="animationBuisnessTopLines(this)" ng-click="setConsumer(is_consumer)">
                                                    <div class="card__front">
                                                        <div class="card_img">
                                                            <img ng-src="<?php echo base_url() ?>uploads/profile_image/consumer.jpg" alt="">
                                                        </div>
                                                        <span class="card__text">Consumer</span>
                                                    </div>
                                                    <div class="card__back">
                                                        <div class="card_img">
                                                            <i class="fa fa-close close_card"></i>
                                                            <div class="selected">Selected</div>
                                                            <img ng-src="<?php echo base_url() ?>uploads/profile_image/consumer.jpg" alt="">
                                                        </div>
                                                        <span class="card__text">Consumer</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="step3 mCustomScrollbar" ng-show="show_target_sector">
                                            <div class="section__content clearfix art_culture ">
                                                <div class="clearfix" ng-repeat="sub in sub_sector_list_wiz_2">
                                                    <h1 ng-bind="sub.industry_name"></h1>
                                                    <div class="card effect__click sub_sector" onclick="animationBuisnessTopLines(this)" ng-click="setSubSectorCode(sub_sector)" ng-repeat="sub_sector in sub.target_data">

                                                        <div class="card__front">
                                                            <div class="card_img">
                                                                <img ng-src="<?php echo base_url() ?>uploads/profile_image/{{sub_sector.profile_image}}" alt="">
                                                            </div>
                                                            <span class="card__text" ng-bind="sub_sector.tar_sector_name">Art &amp; Culture</span>
                                                        </div>
                                                        <div class="card__back">
                                                            <div class="card_img">
                                                                <i class="fa fa-close close_card"></i>
                                                                <div class="selected">Selected</div>
                                                                <img ng-src="<?php echo base_url() ?>uploads/profile_image/{{sub_sector.profile_image}}" alt="">
                                                            </div>
                                                            <span class="card__text" ng-bind="sub_sector.tar_sector_name">Art &amp; Culture</span>
                                                        </div>
                                                    </div>  
                                                </div>
                                            </div>
                                        </div>


                                        <div class="step3" ng-show="show_looking_for">
                                            <div class="art_culture">                                              
                                                <ul class="subtag_select">
                                                    <li onclick="animatedSubSector(this)" ng-repeat="look in looking_for_list" ng-click="setLookingFor(look)" ng-bind="look.looking_for_name">Tag name sub</li>                                                        
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="step3" ng-show="show_target_list">
                                            <div class="art_culture">                                              
                                                <ul class="subtag_select">
                                                    <li onclick="animatedSubSector(this)" ng-repeat="target in target_sector_list" ng-click="setTarget(target)" ng-bind="target.look_service_name">Tag name sub</li>                                                        
                                                </ul>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="modal-footer" ng-show="show_wizard_notice">                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="openWizardTwoForNewUser()">Next</button>
                                    </div>
                                    <div class="modal-footer" ng-show="show_industry_list">                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="getTargetSectorByIndustry()">Next</button>
                                    </div>
                                    <div class="modal-footer" ng-show="show_target_sector">                                            
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('one')">Back</button>
                                        <button type="button" class="btn btn-primary" ng-click="checkBusinessCodeExitForUser()">Next</button>
                                    </div>

                                    <div class="modal-footer" ng-show="show_target_list">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('two')">Back</button>
                                        <!--                                        <button type="button" class="btn btn-primary" ng-click="saveDataForWizard()">Next</button>-->
                                        <button type="button" class="btn btn-primary" ng-click="fetchServices()">Next</button>
                                    </div>


                                    <!--                                    <div class="modal-footer" ng-show="show_locality">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('three')">Back</button>
                                                                            <button type="button" class="btn btn-primary" ng-click="fetchServices()">Next</button>
                                                                        </div>-->
                                    <!--                                    <div class="modal-footer" ng-show="show_services">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('three')">Back</button>
                                                                            <button type="button" class="btn btn-primary" ng-click="checkServices()">Next</button>
                                                                        </div>-->
                                    <div class="modal-footer" ng-show="show_looking_for">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('three')">Back</button>
                                        <!--                                        <button type="button" class="btn btn-primary" ng-click="fetchServices()">Next</button>-->
                                        <button type="button" class="btn btn-primary" ng-click="saveDataForWizard()">Next</button>
                                        <!--                                        <button type="button" class="btn btn-primary" ng-click="fetchLocalityList()">Next</button>-->
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="modal fade payment_modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_industry_list">Who are your customers?</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_target_sector">in the field of…..</h4> 
                                    </div>
                                    <div class="modal-body">                                        
                                        <div  ng-show="show_industry_list" >
                                            <div class="section__content mCustomScrollbar clearfix">
                                                <div class="card effect__click industry_list" onclick="animationBuisnessTopLines(this)" ng-click="setIndustryCode(industry)" ng-repeat="industry in industry_list_for_wizard_two">
                                                    <div class="card__front">
                                                        <div class="card_img">
                                                            <img ng-src="<?php echo base_url() ?>uploads/profile_image/{{industry.profile_image}}" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="industry.industry_name">Art &amp; Culture</span>
                                                    </div>
                                                    <div class="card__back">
                                                        <div class="card_img">
                                                            <i class="fa fa-close close_card"></i>
                                                            <div class="selected">Selected</div>
                                                            <img ng-src="<?php echo base_url() ?>uploads/profile_image/{{industry.profile_image}}" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="industry.industry_name">Art &amp; Culture</span>
                                                    </div>
                                                </div>
                                                <div class="card effect__click category wiz_2_hid_card"></div>
                                            </div>
                                        </div>
                                        <div class="step3" ng-show="show_target_sector">

                                            <div class="art_culture">  
                                                <div class="art_culture clearfix" ng-repeat="sub in sub_sector_list_wiz_2">
                                                    <h1 ng-bind="sub.industry_name">Art &amp; Culture </h1>
                                                    <ul class="subtag_select" ng-repeat="sub_sector in sub.target_data">
                                                        <li onclick="animatedSubSector(this)" ng-click="setSubSectorCode(sub_sector)" ng-bind="sub_sector.tar_sector_name">Tag name sub</li> 
                                                    </ul>
                                                </div>                                                 
                                            </div>

                                            <!--                                            <div class="section__content">
                                                                                            <div class="art_culture clearfix" ng-repeat="sub in sub_sector_list">
                                                                                                <h1 ng-bind="sub.industry_name">Art &amp; Culture </h1>
                                                                                                <div class="card effect__click sub_sector" onclick="animationBuisnessTopLines(this)" ng-click="setSubSectorCode(sub_sector)" ng-repeat="sub_sector in sub.target_data">
                                                                                                    <div class="card__front">
                                                                                                        <div class="card_img">
                                                                                                            <img src="<?php echo base_url() ?>uploads/profile_image/{{sub_sector.profile_image}}" alt="">
                                                                                                        </div>
                                                                                                        <span class="card__text" ng-bind="sub_sector.tar_sector_name">Art &amp; Culture</span>
                                                                                                    </div>
                                                                                                    <div class="card__back">
                                                                                                        <div class="card_img">
                                                                                                            <i class="fa fa-close close_card"></i>
                                                                                                            <div class="selected">Selected</div>
                                                                                                            <img src="<?php echo base_url() ?>uploads/profile_image/{{sub_sector.profile_image}}" alt="">
                                                                                                        </div>
                                                                                                        <span class="card__text" ng-bind="sub_sector.tar_sector_name">Art &amp; Culture</span>
                                                                                                    </div>
                                                                                                </div> 
                                                                                            </div>
                                                                                        </div>-->
                                        </div>
                                    </div> 

                                    <div class="modal-footer" ng-show="show_industry_list">
                                        <p class="wiz_2_note">Select all the sectors you do business with</p>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="getTargetSectorByIndustry()">Next</button>
                                    </div>
                                    <div class="modal-footer" ng-show="show_target_sector">
                                        <p class="wiz_2_note">Select all the sectors you do business with</p>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('one')">Back</button>
                                        <button type="button" class="btn btn-primary" ng-click="checkBusinessCodeExitForUser()">Save</button>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix" >
               <div class="summary box">
                <div class="box-header">
                    <i class="fa  fa-home"></i>
                    Account Details                 
                </div>
                <div class="box-body">
                    <div class="wizard_inner">
                        <div class="wizard_hading">
                            <h6><i class="fa fa-home"></i> Manage your account</h6>
                            <p>You can modify all parameters for your account here.</p>
                        </div>
                        <div class="modify_form">
                            <form class="form-horizontal" name="myFormName" ng-submit="saveUserNameSetting(form_name_data)" novalidate>
                                <!--<div class="form-group" ng-hide="show_name_form">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-7">
                                        <label class="edit-field" ng-bind="form_name_data.first_name">Milan</label>
                                        <label class="edit-field" ng-bind="form_name_data.last_name">Mal</label>&nbsp;
                                    </div>
                                </div>-->
                                <div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">First Name</label>
                                        <div class="col-sm-7">
                                            <input type="text" id="cur_pass" class="form-control" name="first_name" ng-model="form_name_data.first_name" required/>
                                            <span ng-show="myFormName.first_name.$error.required && myFormName.first_name.$dirty">required</span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="input_suggest">Change First Name</label>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Last Name</label>
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" id="password" name="last_name"  ng-model="form_name_data.last_name" required  />
                                            <span ng-show="myFormName.last_name.$error.required && myFormName.last_name.$dirty">required</span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="input_suggest">Change Last Name</label>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-7">
                                            <button type="submit" class="btn submit_frm payment_btn hvr-radial-out">Submit</button>
                                        </div>
                                    </div>
                                </div>    

                            </form>
                        </div>
                        <div class="modal fade" id="wiz_name_modal"  data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div role="document" class="modal-dialog welcome_popup">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <!--<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>-->
                                        <h4 id="myModalLabel" class="modal-title">Please enter your password</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="password" class="welcominput" ng-model="user_password_validate" placeholder="your password">                                       
                                        <div class="modal-footer">
                                            <button class="btn first_open welcome_yellow" ng-click="validatePassword()" type="button">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modify_form">
                            <form class="form-horizontal" name="myEmailForm" ng-submit="saveMailSettingd(formData)" novalidate>
                                <!--<div class="form-group" ng-hide="show_email_form">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-7">
                                        <label class="edit-field" ng-bind="formData.email">Milan</label>&nbsp;                                        
                                        <label></label>
                                    </div>
                                </div>-->
                                <div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Email Address</label>
                                        <div class="col-sm-7">
                                            <input type="email" id="email" class="form-control" name="email" ng-model="formData.email" required/>
                                            <span ng-show="myEmailForm.email.$error.required && myEmailForm.email.$dirty">required</span>
                                            <span ng-show="!myEmailForm.email.$error.required && myEmailForm.email.$error.email && myEmailForm.email.$dirty">invalid email</span>
                                        </div>
                                        <div class="col-sm-3">
                                            <label class="input_suggest">Change Email Address</label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2"></label>
                                        <div class="col-sm-7">
                                            <button type="submit" class="btn submit_frm payment_btn hvr-radial-out">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal fade" id="wiz_email_modal"  data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div role="document" class="modal-dialog welcome_popup">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <!--<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>-->
                                        <h4 id="myModalLabel" class="modal-title">Please enter your password</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="password" class="welcominput" ng-model="user_password_validate" placeholder="your password">                                       
                                        <div class="modal-footer">
                                            <button class="btn first_open welcome_yellow" ng-click="validateEmailPassword()" type="button">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modify_form">
                            <form class="form-horizontal" name="myForm" ng-submit="saveUserPasswordSetting(form_cred_data)" novalidate>                               
                                <!--                                <div class="form-group">
                                                                    <label for="inputPassword3" class="col-sm-2 control-label">Current Password</label>
                                                                    <div class="col-sm-7">
                                                                        <input type="password" id="cur_pass" class="form-control" name="cur_pass" ng-model="form_cred_data.cur_pass" required/>
                                                                        <span ng-show="myForm.cur_pass.$error.required && myForm.cur_pass.$dirty">required</span>
                                                                        <span ng-show="!myForm.cur_pass.$error.required && myForm.cur_pass.$error.cur_pass && myForm.cur_pass.$dirty">Current Password required</span>
                                                                    </div>
                                                                    <div class="col-sm-3">
                                                                        <label class="input_suggest">Change Email Address</label>
                                                                    </div>
                                                                </div> -->
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">New Password</label>
                                    <div class="col-sm-7">
<!--                                        <input type="password" class="form-control" id="inputPassword3" placeholder="">-->
                                        <input type="password" class="form-control" id="password" name="password" ng-model="form_cred_data.password" required  />
                                        <span ng-show="myForm.password.$error.required && myForm.password.$dirty">required</span>
<!--                                        <span ng-show="!myForm.password.$error.required && (myForm.password.$error.minlength || myForm.password.$error.maxlength) && myForm.password.$dirty">Passwords must be between 8 and 20 characters.</span>
                                        <span ng-show="!myForm.password.$error.required && !myForm.password.$error.minlength && !myForm.password.$error.maxlength && myForm.password.$error.pattern && myForm.password.$dirty">Must contain one lower &amp; uppercase letter, and one non-alpha character (a number or a symbol.)</span>-->
                                    </div>
                                    <div class="col-sm-3">
                                        <label class="input_suggest">Change Password</label>
                                    </div>
                                </div> 
                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Repeat Password</label>
                                    <div class="col-sm-7">
<!--                                        <input type="password" class="form-control" id="inputPassword3" placeholder="">-->
                                        <input type="password" class="form-control" id="password_c" name="password_c" ng-model="form_cred_data.password_c" valid-password-c required  />
                                        <span ng-show="myForm.password_c.$error.required && myForm.password_c.$dirty">Please confirm your password.</span>
                                        <span ng-show="!myForm.password_c.$error.required && myForm.password_c.$error.noMatch && myForm.password.$dirty">Passwords do not match.</span>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div> 

                                <div class="form-group">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-7">
                                        <button type="submit" ng-disabled="myForm.password_c.$error.noMatch" class="btn submit_frm payment_btn hvr-radial-out">Submit</button>
                                    </div>
                                </div>  
                            </form>
                        </div>
                        <div class="modal fade" id="wiz_password_modal"  data-keyboard="false" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div role="document" class="modal-dialog welcome_popup">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <!--<button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>-->
                                        <h4 id="myModalLabel" class="modal-title">Please enter your password</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="password" class="welcominput" ng-model="user_password_validate" placeholder="your password">                                       
                                        <div class="modal-footer">
                                            <button class="btn first_open welcome_yellow" ng-click="validatePassPassword()" type="button">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<!-- <div class="wiz_validate"><a href="">validate <i class="fa fa-long-arrow-right "></i></a></div>-->
                      
                    </div>
                </div>
                </div>
                <div class="summary box">
                    <div class="box-header">
                        <i class="fa  fa-home"></i>
                        Account Type                 
                    </div>
                    <div class="box-body">
                        <div class="wizard_inner">                            
                            <div class="modify_form">
                            <form class="form-horizontal">
                            <div class="form-group">
                                       <label class="col-sm-2 control-label">Basic</label>                                   
                                    <div class="col-sm-7">                                       
<!--                                        <input type="password" class="form-control" id="inputPassword3" placeholder="">-->
                                        <a href="javascript:void(0)" ng-click="shareHub2()"><button type="button"  class="btn delete-btn payment_btn hvr-radial-out" id="delete_account"  name="delete_account">Delete my Account</button></a>
                                        
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div> 
                        </form>
                        </div>
                        </div>
                    </div>
                </div>
            </div>


             <div class="summary box" >
                <div class="box-header">
                    <i class="fa fa-envelope"></i>
                    Email Preferences                   
                </div>
                <div class="box-body">
                    <div class="wizard_inner">
                        <div class="wizard_hading">
                            <h6><i class="fa fa-gear"></i> Your Email Settings Overview</h6>
                            <p>You can modify your email settings here. </p>
                        </div>
                        <div class="wiz_content" style="margin:0px !important;">
                            
                            <div class="usersettings_inner">
                                <form name="emailsettingsform" id="emailsettingsform" method="POST" action="">
                                    
                                   <!--<div class="row">
                                        <div class="col-md-6"> 
                                            <button type="submit"  class="btn payment_btn hvr-radial-out ">Email Subscription</button>
                                        </div>
                                        <div class="col-md-6"> 
                                            <input type="checkbox" name="email_sub" ng-change="FnCheckSubscription('weekly_business_email_sub_status')" id="email_sub" value="0" ng-model="email_sub" />
                                        </div>
                                    </div> -->
									<div class="form-group" >
										
										 <div class="inline_chkbox">
										 <input type="checkbox" name="weekly_business_email_sub_status" ng-change="FnCheckSubscription_email($event)"  ng-model="weekly_business_email_sub_status" id="weekly_business_email_sub_status" value="0" />
										  </div>
                                          <label class="control-label" ng-show="weekly_business_email_sub_status==false" for="inputPassword3">Subscribe to receiving company updates  </label>
                                          <label class="control-label" ng-show="weekly_business_email_sub_status==true" for="inputPassword3">Unsubscribe to receiving company updates  </label>
										
									</div>
									<div class="form-group">
										
										 <div class="inline_chkbox">
										 <input type="checkbox" name="weekly_bing_news_sub_status " ng-change="FnCheckSubscription_news()" ng-model="weekly_bing_news_sub_status " id="weekly_bing_news_sub_status " value="0" />
										  </div>
                                          <label class=" control-label" ng-show="weekly_bing_news_sub_status==false" for="inputPassword3">Subscribe to the Weekly Newsletter </label>
                                          <label class=" control-label" ng-show="weekly_bing_news_sub_status==true" for="inputPassword3">Unsubscribe to the Weekly Newsletter </label>
										
									</div>
									
									<div class="form-group">
										
										<div class="inline_chkbox">
											<input type="checkbox" name="connection_message_sub_status" ng-change="FnCheckSubscription_new_msg()" ng-model="connection_message_sub_status" id="connection_message_sub_status" value="0" />
										</div>
                                          <label class=" control-label" ng-show="connection_message_sub_status==false" for="inputPassword3">Subscribe to receiving notifications of new messages and connection requests  </label>
                                          <label class=" control-label" ng-show="connection_message_sub_status==true" for="inputPassword3"> Unsubscribe to receiving notifications of new messages and connection requests  </label>
									</div>
									 
									
									
									
									
									
									
									
									
									
									
									
                                </form>
                            </div>

                        
<!--                            <div class="wiz_validate"><a href="javascript:void(0)">save <i class="fa fa-long-arrow-right "></i></a></div>-->
                        </div>

                        </div>
                </div>
            </div>
           
			<!-- <div class="summary box" >
                <div class="box-header">
                    <i class="fa  fa-file"></i>
                    Settings                   
                </div>
                <div class="box-body subscription">
                    <div class="wizard_inner">
                        <div class="wizard_hading">
                            <h6><i class="fa fa-tachometer"></i> Subscrition and Payment</h6>
                            <p>You can modify all subscription plans for your account here.</p>
                        </div>
                        <div class="plan_inner clearfix">
                            <div class="plan_half">
                                <span class="setting_subheading">
                                    Selected plan
                                </span>
                                <p>You have subscribe for a <a href="javascript:void(0)">free account</a> on Arlians.com. If you like to upgrade your plan for more inside you can click:</p>
                                <a href="#" class="account_link">See Free account details</a><br><br>

                                <a href="#" class="account_link">See Premium account details</a>
                            </div>
                            <div class="plan_half">
                                <span class="setting_subheading">
                                    Payment Details
                                </span>

                                <div class="payment_icons">
                                    <img src="<?php echo base_url() ?>assets_phase2/images/payment_icon1.png" alt="">
                                    <img src="<?php echo base_url() ?>assets_phase2/images/payment_icon3.png" alt="">
                                    <img src="<?php echo base_url() ?>assets_phase2/images/payment_icon2.png" alt="">
                                </div>
                                <div class="payment_form">
                                    <form>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Card Holder</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">Card Number</label>
                                                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="">
                                                </div> 
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label for="exampleInputPassword1">CVV</label>
                                                    <input type="text" class="form-control" id="exampleInputPassword1" placeholder="">
                                                </div> 
                                            </div>
                                        </div> 
                                        <div class="expire_date clearfix">
                                            <label>Expire Date</label>
                                            <div class="select-style select_month">
                                                <select>
                                                    <option value="volvo">Augues</option>
                                                    <option value="saab">Septembar</option>
                                                    <option value="saab">October</option>
                                                </select>
                                            </div>
                                            <div class="select-style selet_year">
                                                <select>
                                                    <option value="volvo">2016</option>
                                                    <option value="saab">2017</option>
                                                </select>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn payment_btn hvr-radial-out ">Make a Payment</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="wiz_validate"><a href="">validate <i class="fa fa-long-arrow-right "></i></a></div>
                    </div>
                </div>
            </div> -->



              
        </div>
    </div>
</div>


 <div id="myShareModal2" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">

                                        <button type="button" class="close" data-dismiss="modal">&times;</button>  
                                        <h4 class="modal-title">Deleting your Arlians account</h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                      <input type="text" class="form-control" id="status" placeholder="Type DELETE to confirm"> 
                                
                                <!-- <select name="status"  id="status">
                                <option value="0" selected>Choose your option</option>
                                <option value="3">Delete</option>
                                </select> -->
                                  
                                    <div class="modal-footer">
                                       <button type="button" class="btn btn-primary"  id="statusbtn"  >Ok</button> 
                                       
                                       
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>

<script src="<?php echo base_url(); ?>assets_phase2/js/settings.js"></script>