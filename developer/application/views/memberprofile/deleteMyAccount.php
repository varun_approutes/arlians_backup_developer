<div id="delete_profile_section">
	<div class="premium-cancel-wrap" data-bind="visible:show_delete_form_section">
		<h2 data-bind="visible:radioDeleteOptionValue() == 'delete'">Account deletion</h2>
		<h2 data-bind="visible:radioDeleteOptionValue() == 'suspend'">Membership cancellation</h2>
		<p class="warning-delet" data-bind="visible:radioDeleteOptionValue() == 'delete'">Warning! Deleting your account means you will lose all data, without any means to retrieve it. We highly recommend you suspend your account instead, to avoid loss of potentially valuable information</p>

		<p class="suspend-mess" style="margin-bottom:0;"><a style="color:#f05564;" href="<?php echo base_url(); ?>payment/paynow/unsub">Are you sure you want to cancel your membership? Why not explore the benefits of our free and premium packages?</a></p>
		<p class="suspend-mess" data-bind="visible:radioDeleteOptionValue() == 'suspend'">Suspending your account means you will not be able to use Arlians until you reactivate your account. Your account will not be visible to anyone, and you will not be able to communicate with members.</p>
		<p style="margin-bottom:25px; margin-top:20px;"><a style="color:#f05564;" href="mailto:hello@arlians.com" target="_top">If you have any feedback we’d love to hear from you.</a></p>

		<div class="select-premium">
			<h3>Are you sure you wish to continue?</h3>
			<ul>				
				<li><input type="radio" value="suspend" data-bind="checked:radioDeleteOptionValue" name="radio" id="radio">Please suspend my account</li>
				<li><input type="radio" value="delete" data-bind="checked:radioDeleteOptionValue" name="radio" id="radio">Please delete my account</li>
			</ul>
		</div>
		<form id="delete_account_form" method="post">
			<div class="account-delet-form">
				<ul>
					<li>
						<div class="account-delet-left">
							<label>Enter your password</label>
						</div>
						<div class="account-delet-right">
							<input type="password" name="password" data-bind="value:current_password" id="password">
						</div>
					</li>
				<li>
					<div class="account-delet-left">
						<label>Re-enter your password</label>
					</div>
					<div class="account-delet-right">
						<input type="password" name="rewrite_password" id="rewrite_password">
					</div>
				</li>
				<li>
					<div class="account-delet-right">
						<input type="submit" value="Submit" id="submit">
					</div>
					</li>
				</ul>
			</div>
		</form>	
	</div>
	<div class="premium-cancel-wrap" data-bind="visible:show_delete_reason_section">
		<h2 data-bind="visible:delete_reason">Account deleted</h2>
		<h2 data-bind="visible:suspend_reason">Account suspended</h2>
		<div class="account-submit-msg account-suspended" data-bind="visible:suspend_reason">Your account has been suspended</div>
		<div class="account-submit-msg account-suspended" data-bind="visible:delete_reason">Your account has been deleted</div>
		<p class="suspend-mess">We’re very sorry to see you leave, was it something we did?</p>
		<p class="we-believe-prodduct-head">We believe in product evolution, so we can provide an ever better service to you in the future, could you tell us why you have downgraded from premium / founding members?</p>
		<div class="select-premium">
			<ul>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="Not enough matches" >Not enough matches</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="Matches weren’t relevant" >Matches weren’t relevant</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="I wasn’t using Arlians often" >I wasn’t using Arlians often</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="The benefits of Arlians are no longer relevant to me" >The benefits of Arlians are no longer relevant to me</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="I have achieved what I wanted to from Arlians" >I have achieved what I wanted to from Arlians</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="Other" >Other (please specifiy)
               <div class="clear"></div>
               <textarea data-bind="visible:reason_for_delete() == 'Other',value:other_reason" style="padding:10px;width: 35%;border: 1px solid #ccc;background-color: #f9f9f9;resize:none;height: 90px;margin: 20px 0 0 0;}"></textarea>
	       </li>
			</ul>
		</div>
		<input type="submit" data-bind="click:feedback" value="Submit feedback" class="feedback-submit">
	</div>
</div>	        
<script>
	var vmDeleteProfile ={
		show_delete_form_section:ko.observable(true),
		show_delete_reason_section:ko.observable(false),
		reason_for_delete:ko.observable('Not enough matches'),
		radioDeleteOptionValue: ko.observable("false"),
		delete_reason:ko.observable(false),
		suspend_reason:ko.observable(false),
		other_reason:ko.observable(''),
		current_password:ko.observable(),
		feedback:function(){
			var reason = '';
			if(vmDeleteProfile.reason_for_delete()=='Other')
				reason = vmDeleteProfile.other_reason();				
			else
				reason = vmDeleteProfile.reason_for_delete();
			$.post('<?php echo base_url(); ?>memberprofile/feedbackReason',{reason:reason},function(result){  
				if(result==true){
					window.location.href = '<?php echo base_url();?>member/logout';
				}else{
	            	alertify.error('Some error occured please try again later.');
	            }          		
			});
		}
	}
	$(function(){
		vmDeleteProfile.radioDeleteOptionValue('suspend');
		$("#delete_account_form").validate({
			rules: {
	            password: { required: true },
	            rewrite_password: { required: true,equalTo: "#password" }
	        },
	        messages: {
	            password: { required: "Please enter your password" },
	            rewrite_password: { required: "Please confirm your password" ,equalTo:"Does not match with password"}
	        },
            submitHandler: function (form) { 
            	if(vmDeleteProfile.radioDeleteOptionValue()=='false'){
            		alertify.error("Please select the radio button");
            		return false;
            	}
            	$('#loader-wrapper').show();
            	$.post('<?php echo base_url(); ?>memberprofile/validateCurrentPassword',{password:vmDeleteProfile.current_password()},function(result){            		
            		$('#loader-wrapper').hide();
	                if(result=='TRUE'){
	                	var status = vmDeleteProfile.radioDeleteOptionValue() == 'delete' ? '3' : '2';
	                	$.post('<?php echo base_url(); ?>memberprofile/deleteAccount',{status:status},function(result_data){
	                		$('#loader-wrapper').hide();
	                		if(result_data==1){
				            	vmDeleteProfile.show_delete_form_section(false);
				            	vmDeleteProfile.show_delete_reason_section(true);
				            	if(vmDeleteProfile.radioDeleteOptionValue() == 'delete')
				            		vmDeleteProfile.delete_reason(true);
				            	else
				            		vmDeleteProfile.suspend_reason(true);
				            	
				            }else{
				            	alertify.error('Some error occured please try again later.');
				            }
			            });
		            }else{
                        alertify.error("You enter a wrong credential!");
                    }

		        });
            	return false;
            }
		});
	});

	ko.applyBindings(vmDeleteProfile, $("#delete_profile_section")[0]);
</script>