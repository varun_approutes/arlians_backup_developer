<div class="dashboard-wrap">
    <div class="row" id="notification_segment">
        <div class="fl68">            
            <div class="compose-inner-bg profile-bg">
                <div class="profile-inner">
                    <h3>My Notification</h3>
                    <ul class="profile-connect" data-bind="foreach:noti_list">
                        <li>
                            <div class="inbox-img"> 
                                <a href="http://localhost/arlians/social-network/viewprofile/profile/50">
                                    <img data-bind="attr:{src:image_url!=null?'<?php echo base_url();?>uploads/profile_image/'+image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}" alt="">
                                </a>
                            </div>
                            <div class="inbox-content">
                                <h4 data-bind="text:bussinessname">IT &amp; Telecom Test 1</h4>
                                <p class="date" ><span data-bind="text:bussinesstype !=null ?bussinesstype:'N/A'">Type of business</span>, <span data-bind="text:noti_type=='prof_view'?'viewed your profile':noti_type=='hub_like'?'like your post':noti_type=='hub_share'?'share your post':'comment on your post'"></span></p>
                            </div>                            
                        </li>                  
                    </ul>
                </div> 
            </div>
        </div> 
    </div>
</div>
<script>    
    var vmNoti = {
        noti_list:ko.observableArray()
    }
    $(function(){
        var total_groups = "<?php echo $total_groups?>";
        var loading = false;
        var track_load = 1;
        $(window).scroll(function() { 
            if($(window).scrollTop() + $(window).height() == $(document).height()){ 
                if(track_load <= total_groups && loading==false) {
                    loading = true;   
                    $("#loader-wrapper").show();          
                    $.post('<?php echo base_url();?>memberprofile/fetchNotification',{group_no: track_load},function(data){
                        $("#loader-wrapper").hide();
                        data = eval("(" + data + ")");                       
                        if(track_load!=1){ 
                            for(var q in data.notification_list){
                                vmNoti.noti_list.push(data.notification_list[q]);
                            }
                        }else{
                            vmNoti.noti_list(data.notification_list);
                        }
                        $('.loader-wrapper').hide();
                        track_load++;
                        loading = false;              
                    });
                }
            }
        });
    });
    ko.applyBindings(vmNoti, $("#notification_segment")[0]);
</script>