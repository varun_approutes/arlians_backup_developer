<section id="user_profile" ng-controller="profileCtrl">
    <div class="row">
        <div class="col-md-12">
            <div class="profile_page box">
                <div class="profile_shadow">
                    <div class="profile_detailinner clearfix">
                        <div class="profile_img">
                            <!-- Modal Crop Image-->
                            <div class="modal fade" id="crop_profile_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Upload an Busines Logo</h4>
                                        </div>otherProfileApi
                                        <div class="modal-body">
                                            <div class="container-crop">
                                                <div class="imageBox">
                                                    <div class="thumbBox"></div>
                                                    <div class="spinner" style="display: none">Loading...</div>
                                                </div>                                            
                                                <div class="cropped"></div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <input type="file" id="file">                                               
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                            <button type="button" id="btnZoomIn" class="btn btn-primary">+</button>
                                            <button type="button" id="btnZoomOut" class="btn btn-primary">-</button>
                                            <button type="button" ng-click="uploadProfileImage()" id="btnCrop" class="btn btn-primary">Done</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <i class="fa fa-camera change_photos2" ng-show="show_if_own_profile" style="cursor:pointer" ng-show="toggle_edit" onclick="open_profile_crop_img_modal()"></i>
                            <img id="hub_profile_default_img" ng-show="profile_image != ''" ng-src="{{profile_image}}" src="<?php echo base_url(); ?>assets_phase2/images/profile_avatar.png">
                            <span ng-show="profile_image == ''" ng-bind="logobussinessname"></span>
                        </div>
                        <div class="prof_details clearfix">

                            <h4 class="edit_popup">
                                <span class="business_nm" ng-show="business_name != ''" ng-bind="business_name">Financial Assets Ltd.</span>
                                <span ng-show="business_name == ''">N/A</span>
                                <div class="edit_option">
                                    <a href="#credits" ng-show="show_if_own_profile" class="toggle"><i class="fa fa-edit"></i></a>
                                    <div id="credits" class="well hidden show_div">
                                        <span class="edit_heading">Your Business Name</span>
                                        <input type="text" class="form-control" ng-model="edit_business_name"><br>
                                        <button class="btn btn-primary" ng-click="updateBizName()">Save</button> &nbsp;
                                        <!--                                    <button class="btn btn-sucess">cancel</button>-->



                                    </div>
                                </div>

                            </h4>
                            <span class="profile_detailstxt edit_popup" >                 
                                <span ng-show="business_tagline != ''"ng-bind="business_tagline"><em>Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness.</em></span>
                                <span ng-show="business_tagline == ''"><em>Tag line</em></span>

                                <div class="edit_option">
                                    <a href="#credits1" ng-show="show_if_own_profile" class="toggle"><i class="fa fa-edit"></i></a>

                                    <div id="credits1" class="well hidden show_div">              

                                        <span class="edit_heading">Tag Name</span>
                                        <input type="text" class="form-control" ng-model="edit_tagline"><br>
                                        <button class="btn btn-primary" ng-click="updateTagName()">Save</button> &nbsp;
                                        <!--                                    <button class="btn btn-sucess">cancel</button>-->

                                    </div>
                                </div>

                            </span><br>

                            <strong class="city_location edit_popup">
                                <span ng-bind="city!=''?city:'city'">city</span> , <span ng-bind="countryName!=null?countryName:'country'">country</span>
                                <div class="edit_option">
                                    <a href="#credits2" ng-show="show_if_own_profile" class="toggle"><i class="fa fa-edit"></i></a>

                                    <div id="credits2" class="well hidden show_div">               

                                        <span class="edit_heading">Country and City</span>
                                        <select class="form-control" ng-model="selected_country"  ng-options="country.id as country.country_name for country in country_list">
                                            <option>Select Country</option>
                                        </select>
    <!--                                    <select class="form-control">
                                        <option value=''>Select Party</option>
                                        <option ng-repeat="country in country_list" ng-model="selected_country" value="{{country._id}}" ng-selected="$scope.country_id == country.id">{{country.country_name}}
                                        </option>
                                        </select>-->
                                        <input type="text" ng-model="edit_city" class="form-control"><br>
                                        <button class="btn btn-primary" ng-click="editLocation()">Save</button> &nbsp; 
                                        <!--                                    <button class="btn btn-sucess">cancel</button>-->

                                    </div>
                                </div>
                            </strong>
                        </div>


                    </div>

                    <div class="profile_connects"> 
                        <ul ng-show="show_if_other_profile" class="pconnect sm_none">
                            <li><a href="javascript:void(0)" ng-click="openMessagePopup(mid)"><i class="fa fa-paper-plane"></i></a></li>
                            <li ng-show="isConnected===0"><a href="javascript:void(0)" ng-click="connectionLogic(mid,isConnected)"><i class="fa fa-user-plus"></i></a></li>
                            <li ng-show="isConnected===2">Request Sent</li>
                            <li ng-show="isConnected===1"><a href="javascript:void(0)" ng-click="connectionLogic(mid,isConnected)"><i class="fa fa-user-times"></i></a></li>
                        </ul>
                        <ul class="mob_msg">
                            <li><a href="javascript:void(0)">Message</a></li>
                            <li><a href="javascript:void(0)">Connect</a></li>
                        </ul>
                    </div>
					<!-- Modal Crop Image-->
						<div class="modal fade" id="message_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Message</h4>
									</div>
									<div class="modal-body">
										<textarea ng-model="message" class="popmsg_area" placeholder="Type message..."></textarea>
									</div>	

									<div class="modal-footer">										                                              
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
										<button type="button" ng-click="sendMessage(mid)" class="btn welcome_yellow">Save</button>
									</div>
                                    
								</div>
							</div>
						</div>
                </div>
                <!-- Modal Crop Image-->
                <div class="modal fade" id="crop_banner_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Upload an Banner Image</h4>
                            </div>
                            <div class="modal-body">
                                <div class="container-crop">
                                    <div class="imageBox_banner">
                                        <div class="thumbBox_banner"></div>
                                        <div class="spinner" style="display: none">Loading...</div>
                                    </div>
                                    <div class="action">
<!--<input type="button" id="uploadImg" style="float: right" value="Upload"/>
<input type="button" id="btnCrop" value="Upload" style="float: right">
<input type="button" id="btnZoomIn" value="+" style="float: right">
<input type="button" id="btnZoomOut" value="-" style="float: right">-->
                                    </div>
                                    <div class="cropped"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="file" id="filebanner">                                               
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                <button type="button" id="btnZoomInb" class="btn btn-primary">+</button>
                                <button type="button" id="btnZoomOutb" class="btn btn-primary">-</button>
                                <button type="button" id="btnCropb" ng-click="uploadBannerImage()" class="btn btn-primary">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
                <i class="fa fa-camera change_photos" ng-show="show_if_own_profile" style="cursor:pointer" onclick="open_banner_crop_img_modal()"></i>

                <img class="full_width" id="hub_banner_default_img" ng-src="{{background_image}}" src="{{background_image}}" alt="">
                <img id="hub_banner_preview_img"  src="" alt="">
            </div>
            <!-- profile details -->
            <div class="profile_details box">
                <ul>
                    <li ><span ng-bind="connection_no">234</span><br><p>Connections</p></li>
                    <li ><span ng-bind="post_no">234</span><br><p>Posts</p></li>
                    <li ><span ng-bind="publish_no">234</span><br><p>Publications</p></li>
                    <li class="sm_none"><span ng-bind="opentalks_no">234</span><br><p>Open Talks</p></li>
                </ul>
            </div>
            <!-- sectors -->
            <div class="summary box">
                <div class="box-header">                 
                    <i class="fa fa-gears"></i> Sectors                 
                </div>
                <div class="box-body">
                    <ul class="sector_btn">
                        <li ng-repeat="list_sectorname in sectorname">
                            <a href="javascript:void(0)" class="blue_circlebtn">
                                <span ng-bind="list_sectorname.name">Beauty</span>
                            </a>
                        </li>

                    </ul>

                </div>
            </div>
            <div class="summary box">
                <div class="box-header">                 
                    <i class="fa  fa-file-text"></i>
                    Summary 
                    <div class="edit_summary edit_option">
                        <a href="javascript:void(0)" ng-click="editSummary(showSummaryEditorAndUploadDoc)"><i id="summary_close" class="fa fa-edit"></i></a>
<!--                        <a href="#credits3" ng-show="show_if_own_profile" ng-click="editSummary()" class="toggle"><i id="summary_close" class="fa fa-edit"></i></a>
                        <div id="credits3" class="well hidden show_div">
                            <div class="publish_editor box-body">
                                <div text-angular ng-model="business_desc_edit"  name="demo-editor" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
                            </div>
                            <button class="btn btn-primary" ng-click="editBizSumary()">Save</button> &nbsp; 
                            <button class="btn btn-sucess">cancel</button>
                        </div>-->
                    </div> 
                </div>
                <div class="box-body summary">                    
                   <!-- <span ng-bind-html="business_desc">-->
                    <span ta-bind ng-model="business_desc" ng-hide="showSummaryEditorAndUploadDoc">
                        I am the co-founder and director of Financial Assets - communication and service design for better health, care and wellbeing. We engage and involve people in addressing challenges. People are at the heart of health and wellbeing challenges, so it makes sense to involve them in designing better solutions. We put people at the centre of the problem solving process to ensure we fully understand the issues, and we design the solutions that are cost effective and that deliver impact.I have specialist knowledge of co-design approaches to achieve social impact.
                    </span> 
                    <div ng-show="showSummaryEditorAndUploadDoc">
                        <div class="publish_editor box-body">
                            <div text-angular ng-model="business_desc_edit"  name="demo-editor" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
                        </div>
                        <button class="btn btn-primary" ng-click="editBizSumary()">Save</button> &nbsp; 
                        <!--<button class="btn btn-sucess">cancel</button>-->
                    </div>
                    <!-- summary carousel -->
                    <div class="summary_carousel" ng-show="doc_animate" >
                        <div id="summary">
                            <div class="item" ng-repeat="list_doc_uploaded in doc_uploaded">
                                <div class="summary_imginner" ng-show="show_if_own_profile">
                                    <div class="hover_cont" >
                                        <a href="javascript:void(0)" class="hover_btn" ng-click="showDocDetailInModal(list_doc_uploaded)">Show</a>
                                        <a href="javascript:void(0)" class="hover_btn" ng-click="deleteDoc(list_doc_uploaded, $event)">Delete</a>
                                        <!--<span ng-bind="list_doc_uploaded.file_title"></span>-->
                                    </div>
                                    <img ng-if="list_doc_uploaded.ext == 'gif'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                                    <img ng-if="list_doc_uploaded.ext == 'jpeg'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                                    <img ng-if="list_doc_uploaded.ext == 'png'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                                    <img ng-if="list_doc_uploaded.ext == 'jpg'" ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">

                                    <span ng-show="list_doc_uploaded.cover_image != 'NULL'" >
                                        <img ng-src="{{list_doc_uploaded.path}}{{list_doc_uploaded.cover_image}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                                    </span>
                                    <span ng-if="list_doc_uploaded.cover_image == NULL">
                                        <i ng-if="list_doc_uploaded.ext == 'txt'" class="fa  fa-file-text icon_large" ng-show="list_doc_uploaded.ext == 'doc'"></i>
                                        <i ng-if="list_doc_uploaded.ext == 'doc'" class="fa  fa-file-text icon_large" ng-show="list_doc_uploaded.ext == 'doc'"></i>
                                        <i ng-if="list_doc_uploaded.ext == 'docx'" class="fa  fa-file-text icon_large" ng-show="list_doc_uploaded.ext == 'docx'"></i>
                                        <i ng-if="list_doc_uploaded.ext == 'pdf'" class="fa  fa-file-pdf-o icon_large" ng-show="list_doc_uploaded.ext == 'pdf'"></i>                                    
                                        <i ng-if="list_doc_uploaded.ext == 'ppt'" class="fa  fa-file icon_large" ng-show="list_doc_uploaded.ext == 'ppt'"></i>                                    
                                        <i ng-if="list_doc_uploaded.ext == 'pptx'" class="fa  fa-file icon_large" ng-show="list_doc_uploaded.ext == 'pptx'"></i>                                    
                                        <i ng-if="list_doc_uploaded.ext == 'mp4'" class="fa  fa-file-video-o icon_large" ng-show="list_doc_uploaded.ext == 'mp4'"></i>  
                                    </span>

                                </div>
                                <p ng-bind="list_doc_uploaded.file_title">User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                            </div>                            
                        </div>
                    </div>
                    <!-- profile uploader -->
                    <ul class="profile_uploader" ng-show="showSummaryEditorAndUploadDoc">
                        <li>Upload :</li> 
                        <li><a href="javascript:void(0)" ng-click="openModalForDocUpload()"><i class="fa  fa-file-text"></i> <span class="sm_none">Document</span></a></li>
                        <li><a href="javascript:void(0)" ng-click="openModalForDocUpload()"><i class="fa  fa-file-photo-o"></i> <span class="sm_none">Media</span></a></li>
                        <li><a href="javascript:void(0)" ng-click="openModalForDocUpload()"><i class="fa  fa-file-excel-o"></i> <span class="sm_none">Report</span></a></li>
                        <li><a href="javascript:void(0)" ng-click="openModalForDocUpload()"><i class="fa  fa-file-powerpoint-o"></i> <span class="sm_none">Presentation</span></a></li>
                    </ul>
                    <div class="modal fade" id="docModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Upload your document</h4>                                       
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Tile:</label>
                                        <input type="text" ng-model="doc_title" class="form-control">
                                    </div>
                                    <input type="file" id="file1" name="file" ng-files="getTheFiles($files)" /> 
                                    <div class="form-group"  id="cover" style="display:none">
                                        <label for="message-text" class="control-label">Upload Cover Picture:</label>
                                        <input type="file" name="file" id="cover_file" />
                                        <div class="img_pic" id="cover_img" style="display:none"><img id="cover_img_src" src="" /></div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" ng-click="uploadFile()">Upload</button>                                        
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="docDetailModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="docDetailModalLabel" ng-bind="doc_detail_show.file_title">Upload your document</h4>                                       
                                </div>
                                <div class="modal-body">
                                    <div class="doc-pdf-ppt" ng-if="doc_detail_show.ext == 'pdf'">
                                        <iframe ng-src="{{'http://docs.google.com/gview?url='+doc_detail_show.path+doc_detail_show.file_name+'&embedded=true'}}" src=""  frameborder="0"></iframe>
                                    </div>
                                    <div class="doc-pdf-ppt" ng-if="doc_detail_show.ext == 'doc'">
                                        <iframe ng-src="{{'http://docs.google.com/gview?url='+doc_detail_show.path+doc_detail_show.file_name+'&embedded=true'}}" src=""  frameborder="0"></iframe>
                                    </div>
                                    <div class="doc-pdf-ppt" ng-if="doc_detail_show.ext == 'docx'">
                                        <iframe ng-src="{{'http://docs.google.com/gview?url='+doc_detail_show.path+doc_detail_show.file_name+'&embedded=true'}}" src=""  frameborder="0"></iframe>
                                    </div>
                                    <div class="doc-pdf-ppt" ng-if="doc_detail_show.ext == 'pptx'">
                                        <iframe ng-src="{{'http://docs.google.com/gview?url='+doc_detail_show.path+doc_detail_show.file_name+'&embedded=true'}}" src=""  frameborder="0"></iframe>
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'txt'">
                                        <a href="" ng-href='{{doc_detail_show.path + doc_detail_show.file_name}}' target="_blank" ng-bind='doc_detail_show.file_name'></a>
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'jpg'">
                                        <img ng-src="{{doc_detail_show.path}}{{doc_detail_show.file_name}}"  src="" alt="">
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'jpeg'">
                                        <img ng-src="{{doc_detail_show.path}}{{doc_detail_show.file_name}}"  src="" alt="">
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'png'">
                                        <img ng-src="{{doc_detail_show.path}}{{doc_detail_show.file_name}}"  src="" alt="">
                                    </div>
                                    <div ng-if="doc_detail_show.ext == 'gif'">
                                        <img ng-src="{{doc_detail_show.path}}{{doc_detail_show.file_name}}"  src="" alt="">
                                    </div>
                                    <div class="video-ply" ng-if="doc_detail_show.ext == 'mp4'">
                                        <video controls ng-src="{{doc_detail_show.path + doc_detail_show.file_name}}" src="http://www.w3schools.com/html/mov_bbb.mp4"></video>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                                                       
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
            <!-- summary -->
            <!--            <div class="summary box">
                            <div class="box-header">
                                <i class="fa  fa-file-text"></i>
                                Summary
                                
                            </div>
            
                            <div class="box-body summary_pop">
                                <div class="mCustomScrollbar">
                                    <div class="edit_popup">
            
                                        <span ng-bind-html="business_desc">
                                            I am the co-founder and director of Financial Assets - communication and service design for better health, care and wellbeing. We engage and involve people in addressing challenges. People are at the heart of health and wellbeing challenges, so it makes sense to involve them in designing better solutions. We put people at the centre of the problem solving process to ensure we fully understand the issues, and we design the solutions that are cost effective and that deliver impact.I have specialist knowledge of co-design approaches to achieve social impact.
                                        </span>  
                                    </div>
            
                                </div>
            
                            </div>
                        </div>-->

            <!-- publish start -->
            <div class="summary box">
                <div class="box-header">
                    <i class="fa  fa-file-text"></i>
                    Publish
                </div>
                <div class="box-body">
                    <div class="publish_line"></div>
                    <!-- publish carousel -->
                    <div class="publish_carousel">
                        <div id="publish" class="owl-carousel">
                            <div class="item" ng-repeat="list_publish in publishList">
                                <div class="summary_imginner" ng-show="list_publish.link != null">
                                    <!--<div class="hover_cont" ng-show="show_if_own_profile">
                                        <a href="javascript:void(0)" class="hover_btn">Edit</a>
                                        <a href="javascript:void(0)" class="hover_btn">Delete</a>
                                    </div>-->
                                    <img ng-show="list_publish.link != null" src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" ng-src="<?php echo base_url() ?>/uploads/hub_images/{{list_publish.link}}" alt="" class="summary_img">
                                    <p ng-bind-html="list_publish.hub_title" class="pub_heading">User document Joint enterprise law wrongly interpreted for 30 years, court... </p>

                                </div>


                                <div class="summary_imginner carousel_article" ng-show="list_publish.link == null">
                                    <div class="cararticl_top"> </div> 
                                    <div class="carartcl_body">
                                        <span class="article_label" >Article</span>
                                        <div class="hover_cont" ng-show="show_if_own_profile">
                                            <a href="javascript:void(0)" class="hover_btn">Edit</a>
                                            <a href="javascript:void(0)" class="hover_btn">Delete</a>
                                        </div>
                                        <p class="article_txt" ng-bind-html="list_publish.hub_title">Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business.</p>
                                        <span class="artcl_autorname" ng-bind-html="list_publish.content| limitTo:140">By Author Name</span>
                                        <a href="javascript:void(0)" class="article_read">Read</a>
                                        <!--									<ul class="article_share">
                                                                                                                  <li>Share :</li>
                                                                                                                  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                                                                                  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                                                                                  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                                                                                </ul>-->

                                    </div>

                                </div> 




                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- community activity -->
        </div>    
        <!-- tab Start -->
    </div>
</section>
<script type='text/javascript'>
   var name = '<?php echo $name;?>';
   var id = '<?php  echo $id;?>';
</script>
<script src="<?php echo base_url(); ?>assets_phase2/js/bootstrap-toggle.min.js"></script>
<script src="<?php echo base_url(); ?>assets_phase2/js/profile.js"></script>
<script type='text/javascript' src="<?php echo base_url(); ?>assets_phase2/js/bootstrap-slider.js"></script>
<script>

                                        (function ($) {
                                            $(window).load(function () {
                                                $("#content-1").mCustomScrollbar({
                                                    axis: "yx", //set both axis scrollbars
                                                    advanced: {autoExpandHorizontalScroll: true}, //auto-expand content to accommodate floated elements
                                                    // change mouse-wheel axis on-the-fly 
                                                    callbacks: {
                                                        onOverflowY: function () {
                                                            var opt = $(this).data("mCS").opt;
                                                            if (opt.mouseWheel.axis !== "y")
                                                                opt.mouseWheel.axis = "y";
                                                        },
                                                        onOverflowX: function () {
                                                            var opt = $(this).data("mCS").opt;
                                                            if (opt.mouseWheel.axis !== "x")
                                                                opt.mouseWheel.axis = "x";
                                                        },
                                                    }
                                                });

                                            });
                                        })(jQuery);

                                        $(function () {
                                            $('.toggle').click(function (event) {
                                                event.preventDefault();
                                                var target = $(this).attr('href');
                                                $(target).toggleClass('hidden show');
                                            });
                                        });
</script>

