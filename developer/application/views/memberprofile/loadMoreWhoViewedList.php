<?php $mid = $this->session->userdata['logged_in']['id'];
$user = getParticularUserDetails($mid);
$user_own_type = $user['bussinesstype1'];
?>
<script>
jQuery(document).ready(function(){

    jQuery('.fa-check').click(function(){
        var connected_id = jQuery(this).parent('a').parent('div').parent('li').attr('id');
        var mid = "<?php echo $mid; ?>";

        jQuery.post('<?php echo base_url(); ?>suggestion/acceptRequestForConnection',{'connected_id':connected_id,'mid':mid},function(res){
            if(res == "true"){
               location.reload(); 
            }
        });
    });

    jQuery('.fa-times').click(function(){
        var connected_id = jQuery(this).parent('a').parent('div').parent('li').attr('id');
        var mid = "<?php echo $mid; ?>";

        jQuery.post('<?php echo base_url(); ?>suggestion/deleteRequestForConnection',{'connected_id':connected_id,'mid':mid},function(res){
            if(res == "true"){
               location.reload(); 
            }
        });
    });
});
</script>
<div class="dashboard-wrap">
    <div class="row">
        <div class="fl100">
            <div class="compose-inner-bg profile-bg">
			 <div class="profile-heading">
                      
                      <h2>Profile views</h2>
                  </div>
                <div class="profile-inner search-inner">
                    <h3>People who have viewed you</h3>
                    <ul class="profile-connect">

                        <?php 
                        $new_member_ids = array();
                       // echo '<pre>'; var_dump($last_suggested_id); die();
                        foreach($result_list as $user){

                            //$curr_id = $suggested_id; 
                            //$user = getParticularUserDetails($curr_id);

							//var_dump($user);
                            //$suggested_user_type = userProfType($curr_id);
                            

                            

                              
                                   
                                    //$new_member_ids[] = $curr_id;
						        //if($country!=''){
							        //if($country==$user['country']){
							
                           
                         ?>  

                            <li id="<?php echo $user['who_viewed']; ?>">
                            <div class="inbox-img">
                              <a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $user['who_viewed']; ?>">
                                <?php if($user['image_url']!=''){?> 
                                  <img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $user['image_url']?>" alt="">
                                <?php }else{?>  
                                  <img src="<?php echo base_url(); ?>assets/images/inbox-img.png" alt="">
                                <?php }?>
                              </a>
                            </div>
                            <div class="inbox-content">
                              <h4><a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $user['who_viewed']; ?>"><?php echo $user['bussinessname'];?></a></h4>
                              <p class="date"> <?php echo $user['bussinesstype1'];?></p>
                            </div>
                            <div class="right-inbox-icon">                              
                                <?php $is_connected = is_connected($mid,$curr_id);
                                    if($is_connected != 'true'){
                                 ?>
                                <a href="javascript:void(0)" class="icon-border" >
                                    <i class="fa fa-check fa-1"></i>
                                </a>
                                <?php } else { ?>
                              <a href="javascript:void(0)" class="icon-border"  >
                                <i class="fa fa-times fa-1"></i>
                              </a>
                              <?php } ?>
                            </div>
                          </li>
                          <?php 
							      
                        } 

                        ?>
						 
                    </ul>
                </div>
				<div style="padding:10px"></div>
				 <div class="profile-inner search-inner">
                    <h3>People you have viewed</h3>
                    <ul class="profile-connect">

                        <?php 
                        $new_member_ids = array();
                       // echo '<pre>'; var_dump($last_suggested_id); die();
                        foreach($iviewed as $user){

                            //$curr_id = $suggested_id; 
                            //$user = getParticularUserDetails($curr_id);

							//var_dump($user);
                            //$suggested_user_type = userProfType($curr_id);
                            

                            

                              
                                   
                                    //$new_member_ids[] = $curr_id;
						        //if($country!=''){
							        //if($country==$user['country']){
							
                           
                         ?>  

                            <li id="<?php echo $user['who_viewed']; ?>">
                            <div class="inbox-img">
                              <a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $user['who_viewed']; ?>">
                                <?php if($user['image_url']!=''){?> 
                                  <img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $user['image_url']?>" alt="">
                                <?php }else{?>  
                                  <img src="<?php echo base_url(); ?>assets/images/inbox-img.png" alt="">
                                <?php }?>
                              </a>
                            </div>
                            <div class="inbox-content">
                              <h4><a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $user['who_viewed']; ?>"><?php echo $user['bussinessname'];?></a></h4>
                              <p class="date"> <?php echo $user['bussinesstype1'];?></p>
                            </div>
                            <div class="right-inbox-icon">                              
                                <?php $is_connected = is_connected($mid,$curr_id);
                                    if($is_connected != 'true'){
                                 ?>
                                <a href="javascript:void(0)" class="icon-border" >
                                    <i class="fa fa-check fa-1"></i>
                                </a>
                                <?php } else { ?>
                              <a href="javascript:void(0)" class="icon-border"  >
                                <i class="fa fa-times fa-1"></i>
                              </a>
                              <?php } ?>
                            </div>
                          </li>
                          <?php 
							      
                        } 

                        ?>
						 
                    </ul>
                </div>
            </div>
        </div>       
    </div>
</div>
<script>
    function connectedToUser(curObj,conected_id){      
        try{
          //$('#loader-wrapper').show();
            //$.post('<?php echo base_url(); ?>member/reqestForConnection',{'id':conected_id},function(result){
              //console.log(result);
              //$('#loader-wrapper').hide();
              //if(result==1){
                $(curObj).removeClass("fa-times");
                $(curObj).addClass('fa-check');
                //alertify.success('Request Send');
              //}else{
              //alert('This is embrassing.Some error occured');
              //}
            //});
          }catch(e){
              alertify.error(e);
          }
    }
</script>
