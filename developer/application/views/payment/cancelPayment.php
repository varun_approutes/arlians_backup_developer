<div id="delete_profile_section">
	<div class="premium-cancel-wrap" data-bind="visible:show_delete_form_section">
	  <h2>Premium subscription cancellation</h2>
	  <p class="suspend-mess">You’re currently subscribed to the premium / founding members package, are you sure that you want to downgrade to a free account?</p>
	  <br><br>
	  <h4 class="prem-subscription-text">Take a look at the <a href="<?php echo base_url(); ?>payment/paynow/unsub"><span> benefits of staying with premium / founding members package. </span></a></h4>
	  <h4 class="prem-subscription-text"> Still unsure? Let us help you get more from Arlians, <a href="mailto:hello@arlians.com"><span>get in touch and tell us your thoughts.</span></a></h4>
	  <div class="downgrade-section">
		<label> Do you still wish to downgrade?</label>
		<!--<a href="javascript:void(0);" onclick="confirmationAndDowngrade()" class="downgrade">Downgrade</a>-->
		<a href="javascript:void(0);" data-bind="click:showFeedbackPannel" class="downgrade">Downgrade</a>
		<!--<a href="#" class="downgrade">Downgrade my account to <br> free</a> -->
		<a style="display:none" target="_blank" id="downgrade_redirect_paypall" href="https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=NNTDWDC7V4X58">
		  <img src="https://www.paypalobjects.com/en_US/i/btn/btn_unsubscribe_LG.gif" BORDER="0">
		</a>
	  </div>
	  <p class="subscription-cancel">Your subscription will be cancelled immediately, and your account will not be debited again. You will not be issued a refund for any unused days of your remaining subscription. </p>
	  <a href="<?php echo base_url(); ?>company/termsAndConditions"><p class="subscription-cancel">Please see out full terms and conditions.</p></a> 
	</div>

	<div class="premium-cancel-wrap" data-bind="visible:show_delete_reason_section">
				<h2 >Premium subscription cancellation</h2>
		
		<div class="account-submit-msg account-suspended" >Your premium subscription will be cancel after click feedback</div>
		<p class="suspend-mess">We’re very sorry to see you leave, was it something we did?</p>
		<p class="we-believe-prodduct-head">We believe in product evolution, so we can provide an ever better service to you in the future, could you tell us why you have downgraded from premium / founding members?</p>
		<div class="select-premium">
			<ul>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="Not enough matches" >Not enough matches</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="Matches weren’t relevant" >Matches weren’t relevant</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="I wasn’t using Arlians often" >I wasn’t using Arlians often</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="The benefits of Arlians are no longer relevant to me" >The benefits of Arlians are no longer relevant to me</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="I have achieved what I wanted to from Arlians" >I have achieved what I wanted to from Arlians</li>
				<li><input type="radio" name="radio" data-bind="checked:reason_for_delete" value="Other" >Other (please specifiy)
               <div class="clear"></div>
               <textarea data-bind="visible:reason_for_delete() == 'Other',value:other_reason" style="padding:10px;width: 35%;border: 1px solid #ccc;background-color: #f9f9f9;resize:none;height: 90px;margin: 20px 0 0 0;}"></textarea>
	       </li>
			</ul>
		</div>
		<div><p class="we-believe-prodduct-head">With this, you can go back to your free account. But if you do that, please remember that you won't have access to several features. If you still want to go ahead and cancel your paid account, please click 'Submit feedback' to confirm. You will be sent to Paypal to unsubscribe.</p></div>
		<input type="submit" data-bind="click:feedback" value="Submit feedback" class="feedback-submit">
	</div>  
</div>	
	
<script>
  /*  function confirmationAndDowngrade(){
        alertify.confirm("With this, you can go back to your free account. But if you do that, please remember that you won't have access to several features. If you still want to go ahead and cancel your paid account, please click 'OK' to confirm. You will be sent to Paypal to unsubscribe.", function (e) {
            if (e) {  
               $("#downgrade_redirect_paypall").trigger("click");
                $.post('<?php echo base_url(); ?>memberprofile/confirmationAndDowngrade',{},function(result){
                  if(result==1){
                     window.open("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=NNTDWDC7V4X58", '_blank');    window.location='<?php echo base_url(); ?>member/dashboard';         
                  }else{
                      alertify.error("Some error occured please trr again later");
                  }
                });
            }
        });
        return false;
    }*/
</script>
<script>
	var vmDowngrateProfile ={
		show_delete_form_section:ko.observable(true),
		show_delete_reason_section:ko.observable(false),
		showFeedbackPannel:function(){
			vmDowngrateProfile.show_delete_reason_section(true);
			vmDowngrateProfile.show_delete_form_section(false);
		},		
		reason_for_delete:ko.observable('Not enough matches'),
		radioDeleteOptionValue: ko.observable("false"),
		delete_reason:ko.observable(false),
		suspend_reason:ko.observable(false),
		other_reason:ko.observable(''),
		current_password:ko.observable(),
		feedback:function(){
			var reason = '';
			if(vmDowngrateProfile.reason_for_delete()=='Other')
				reason = vmDowngrateProfile.other_reason();				
			else
				reason = vmDowngrateProfile.reason_for_delete();
			$.post('<?php echo base_url(); ?>memberprofile/feedbackReasonforDowngrade',{reason:reason},function(result){  
				if(result==1){
					window.open("https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=NNTDWDC7V4X58", '_blank');    window.location='<?php echo base_url(); ?>member/dashboard';         
				}else{
	            	alertify.error('Some error occured please try again later.');
	            }          		
			});
		}
	}
	/*$(function(){
		vmDowngrateProfile.radioDeleteOptionValue('suspend');
		$("#delete_account_form").validate({
			rules: {
	            password: { required: true },
	            rewrite_password: { required: true,equalTo: "#password" }
	        },
	        messages: {
	            password: { required: "Please enter your password" },
	            rewrite_password: { required: "Please confirm your password" ,equalTo:"Does not match with password"}
	        },
            submitHandler: function (form) { 
            	if(vmDowngrateProfile.radioDeleteOptionValue()=='false'){
            		alertify.error("Please select the radio button");
            		return false;
            	}
            	$('#loader-wrapper').show();
            	$.post('<?php echo base_url(); ?>memberprofile/validateCurrentPassword',{password:vmDowngrateProfile.current_password()},function(result){            		
            		$('#loader-wrapper').hide();
	                if(result=='TRUE'){
	                	var status = vmDowngrateProfile.radioDeleteOptionValue() == 'delete' ? '3' : '2';
	                	$.post('<?php echo base_url(); ?>memberprofile/deleteAccount',{status:status},function(result_data){
	                		$('#loader-wrapper').hide();
	                		if(result_data==1){
				            	vmDowngrateProfile.show_delete_form_section(false);
				            	vmDowngrateProfile.show_delete_reason_section(true);
				            	if(vmDowngrateProfile.radioDeleteOptionValue() == 'delete')
				            		vmDowngrateProfile.delete_reason(true);
				            	else
				            		vmDowngrateProfile.suspend_reason(true);
				            	
				            }else{
				            	alertify.error('Some error occured please try again later.');
				            }
			            });
		            }else{
                        alertify.error("You enter a wrong credential!");
                    }

		        });
            	return false;
            }
		});
	});*/

	ko.applyBindings(vmDowngrateProfile, $("#delete_profile_section")[0]);
</script>