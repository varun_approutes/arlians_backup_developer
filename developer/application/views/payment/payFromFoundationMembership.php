<?php 
$id = $this->session->userdata['logged_in']['id'];
$details = getParticularUserDetails($id);

$country = $details['country'];

$tot = 99.00;
?> 

<div class="paypal-wrap">
        <div class="paypal-inner">
         <h3>Great selection, <?php echo $details['bussinessname']; ?>!</h3>
          <p>You’re a step closer to growing and nurturing your network.</p>
        </div>
        <div class="paypal-inner payment-price-sec">
          <p>Membership plan selected: <b>FOUNDING MEMBER</b></p>
          <h3>One off payment</h3>
			
			<?php if($country != 1){ ?>
          <div class="payment-section">
            <ul>
              <li>
                <div class="premium-price">A lifetime premium membership for the price of just 2.5 month</div>
                <div class="premium-price premium-price-right">$99.00</div>
              </li>
              <li class="subtotal">
                <div class="premium-price">Sub total  </div>
                <div class="premium-price premium-price-right">$99.00</div>
              </li>
            </ul>
          </div>
			<?php } else{ 
			
			$vat = (99*20)/100;
		
			$vat = number_format((float)$vat, 2, '.', '');
			$tot = number_format($vat+99, 2, '.', '');
			?>
			
		<div class="payment-section">
            <ul>
              <li>
                <div class="premium-price">A lifetime premium membership for the price of just 2.5 month</div>
                <div class="premium-price premium-price-right">$99.00</div>
              </li>
              <li class="subtotal">
                <div class="premium-price">VAT (20%)</div>
                <div class="premium-price premium-price-right"><?php echo '$'.$vat; ?></div>
              </li>
			  <li class="subtotal">
                <div class="premium-price">Sub total  </div>
                <div class="premium-price premium-price-right"><?php echo '$'.$tot; ?></div>
              </li>
            </ul>
          </div>
			
			<?php } ?>
        </div>
        <div class="paypal-inner">
          <h4>Payment method</h4>
        </div>
        <div class="clear"></div>
	 			<?php
				$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
				//$paypal_url='https://www.paypal.com/cgi-bin/webscr';       // Live Paypal API URL
				$paypal_id='i.auddy@rocketmail.com'; // Business email ID
				?>
	           <form target="_blank" action="<?php echo $paypal_url; ?>" method="post" name="frmPayPal1">
					<input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
					<input type="hidden" name="cmd" value="_xclick">
					<input type="hidden" name="item_name" value="Life time subcription">
					<input type="hidden" name="item_number" value="1">
					<input type="hidden" name="credits" value="510">
					<input type="hidden" name="userid" value="1">
					<input type="hidden" name="amount" value="<?php echo $tot; ?>">
					<input type="hidden" name="tx" value="TransactionID"> 
					<input type="hidden" name="no_shipping" value="1">
					<input type="hidden" name="currency_code" value="USD">
					<input type="hidden" name="handling" value="0">
					<input type="hidden" name="cancel_return" value="<?php echo base_url(); ?>payment/paymentCancel">
					<input type="hidden" name="return" value="<?php echo base_url(); ?>payment/paymentSuccess">
					<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
					<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
				</form>
	 
      </div>
	  <a class="payment-back" href="javascript: window.history.go(-1)">Go back</a>