<div class="premium-wrap">
          <h2>Join our exclusive Founding Members club</h2>
          <div class="premium-wrap-inner premium-wrap-maximum">
          
      <div class="founding-member">
      <h3>FOUNDING MEMBERSHIP</h3>
      <p>A one off opportunity for a lifetime of benefits <br> FOR LIMITED TIME ONLY</p>
      </div>
      <div class="what-we-say">
      <h3>We want to say thank you for being a trailblazer</h3>
      <p>You’re our hero, one of the first to try Arlians! That’s why we want you to really stand out</p>
      </div>
      <div class="never-pay-joint-exclusive">
      <ul>
      <li>
      <h4>Never pay again…ever</h4>
      <img alt="" src="<?php echo base_url(); ?>assets/images/never-pay.png">
      <p>Premium access normally costs $39.99 / month. As a founding member you’ll pay a one off fee of $99, and never pay again….ever</p>
      </li>
       <li>
      <h4>Join an exclusive club</h4>
      <i class="fa fa-certificate fa-5"></i>
      <p>What’s more, every business who sees you will know you were an innovator, at the heart of Arlians. Your profile will forever display a Founding members badge</p>
      </li>
      </ul>
      </div>
         <div class="premium-package-btn life-time-prem">
         <ul>
         <li>
          <a class="package-btn" href="<?php echo base_url(); ?>payment/payFromFoundationMembership">
          <p><b>$99</b> one off payment…..for lifetime of premium </p>
          <span>(offer only available to a limited number of businesses, for a limited time only)</span>
          </a>
          </li>
          </ul>
          </div>
		  
		<?php   $user_type = check_member_payment_status($this->session->userdata['logged_in']['id']);
	  
	  if($user_type[0]['user_type']=='Free'){ ?>  
      <div class="explore-premium-heading">
      <a href="<?php echo base_url(); ?>payment/paymentoptiondetails">
      <i class="fa fa-angle-down"></i>
      <div class="clear"></div>
      Explore premium features & normal pricing
      </a>
      </div>
		<?php } ?>
   
          <p class="subscription-pckage-text">Have any questions about our subscription packages? Contact <a href="mailto:subscriptions@arlians.com">subscriptions@arlians.com </a>  
		  
		  
		  </p>
		  <a class="payment-back" href="javascript: window.history.go(-1)">Go back</a>
          </div>
        </div>