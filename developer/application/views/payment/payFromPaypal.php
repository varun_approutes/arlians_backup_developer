<?php 
$id = $this->session->userdata['logged_in']['id'];
$details = getParticularUserDetails($id);
$country = $details['country'];

$tot = 14.99;

$obj = new Payment();
$no_of_days_from_signUp = $obj->noOfDaysSinceSignUp();

$dt_after_fourteen_days = date('F j, Y', strtotime("+14 days"));
$dt_after_fifteen_days = date('F j, Y', strtotime("+15 days"));
$day_after_fifteen_days = date('j', strtotime("+15 days"));


$user_type = check_member_payment_status($id);
$date=getUserRegistrationDate($id);
			$now = time(); // or your date as well
			$your_date = strtotime($date);
			$datediff = $now - $your_date;
			$day_diff= floor($datediff/(60*60*24));

?>

<?php if($day_diff < 35){ ?>
<div class="paypal-wrap">
        <div class="paypal-inner">
          <h3>Great selection, <?php echo $details['bussinessname']; ?>!</h3>
          <p>You’re a step closer to growing and nurturing your network.</p>
        </div>
        <div class="paypal-inner payment-price-sec">
         <p>Membership plan selected: <b>PREMIUM MEMBER</b></p>
          <h3>Payment per month</h3>
                <?php if($country != 1){ ?>
          <div class="payment-section">
            <ul>
              <li>
                <div class="premium-price">Monthly premium membership</div>
                <div class="premium-price premium-price-right">$39.99</div>
              </li>
              <li class="premium-deduct">
                <div class="premium-price">Special introductory discount (62.5% off)</div>
                <div class="premium-price premium-price-right">-$25.00</div>
              </li>
              <li class="subtotal">
                <div class="premium-price">Sub total (inc. VAT) </div>
                <div class="premium-price premium-price-right">$14.99</div>
              </li>
            </ul>
          </div>
        <?php } else{ 
			
        $vat = (14.99*20)/100;

        $vat = number_format((float)$vat, 2, '.', '');
        $tot = $vat+14.99;
        ?>
           <div class="payment-section">
            <ul>
              <li>
                <div class="premium-price">Monthly premium membership</div>
                <div class="premium-price premium-price-right">$39.99</div>
              </li>
              <li class="premium-deduct">
                <div class="premium-price">Special introductory discount (62.5% off)</div>
                <div class="premium-price premium-price-right">-$25.00</div>
              </li>
              <li class="subtotal">
                <div class="premium-price">Sub total (inc. VAT(20%) for UK businesses) </div>
                <div class="premium-price premium-price-right"><?php echo '$'.$tot; ?></div>
              </li>
            </ul>
          </div>
                <?php } ?>
        </div>
        <div class="paypal-inner">
          <h4>Your 14 day risk free trial…</h4>
          <br>
          <p>You pay $0.01 for your trial which begins today and ends <?php echo $dt_after_fourteen_days; ?>. Cancel anytime before <?php echo $dt_after_fourteen_days; ?> to avoid further charges. <br>
            <br>
            Payment will begin from <?php echo $dt_after_fifteen_days; ?>, and renews automatically on <?php echo $day_after_fifteen_days; ?>th of every subsequently month. You can cancel anytime before the renewal monthly date, to avoid charges for the next month. </p>
        </div>
        <div class="paypal-inner">
          <h4>Payment method</h4>
        </div>
        <div class="clear"></div>
                <?php if($country != 1){ ?>
                        <?php if($user_type[0]['user_type'] == "Free"){ ?>
        
                        <?php if(userUpgrationStatus($id) == 0){ ?>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="FGL4MM3SKHQ3Q">
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>
                         <?php } else{ ?>
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="D3CEEZRQ7FG6L">
<input type="image" src="https://www.sandbox.paypal.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>
<?php } ?>

                        <?php } else{ ?>
                                <A HREF="https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=LELLZQBU7LM78">
                                <IMG SRC="https://www.paypalobjects.com/en_GB/i/btn/btn_unsubscribe_SM.gif" BORDER="0">
                                </A>
                        <?php } ?>
        
                <?php } else{ ?>
                       <?php if($user_type[0]['user_type'] == "Free"){ ?> 
                                        
                                                                <?php if(userUpgrationStatus($id) == 0){ ?>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="427AT72SA9PLG">
<input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.paypalobjects.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>
                         <?php } else{ ?>
                                        
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="D3CEEZRQ7FG6L">
<input type="image" src="https://www.sandbox.paypal.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>
                       <?php } ?>
                       <?php } else{ ?>
                               <A HREF="https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=LELLZQBU7LM78">
                                <IMG SRC="https://www.paypalobjects.com/en_GB/i/btn/btn_unsubscribe_SM.gif" BORDER="0">
                                </A>         
                       <?php } ?>
                <?php } ?>
      </div>
<?php } else{ 
$tot = 39.99;
?>

<div class="paypal-wrap">
        <div class="paypal-inner">
         <h3>Great selection, <?php echo $details['bussinessname']; ?>!</h3>
          <p>You’re a step closer to growing and nurturing your network</p>
        </div>
        <div class="paypal-inner payment-price-sec">
         <p>Membership plan selected: <b>PREMIUM MEMBER</b></p>
          <h3>Payment per month</h3>
                <?php if($country != 1){ ?>
          <div class="payment-section">
            <ul>
              <li>
                <div class="premium-price">Monthly premium membership</div>
                <div class="premium-price premium-price-right">$39.99</div>
              </li>

              <li class="subtotal">
                <div class="premium-price">Sub total (inc. VAT) </div>
                <div class="premium-price premium-price-right">$39.99</div>
              </li>
            </ul>
          </div>
        <?php } else{ 
			
        $vat = (39.99*20)/100;

        $vat = number_format((float)$vat, 2, '.', '');
        $tot = $vat+39.99;
        ?>
           <div class="payment-section">
            <ul>
              <li>
                <div class="premium-price">Monthly premium membership</div>
                <div class="premium-price premium-price-right">$39.99</div>
              </li>

              <li class="subtotal">
                <div class="premium-price">Sub total (inc. VAT(20%) for UK businesses) </div>
                <div class="premium-price premium-price-right"><?php echo '$'.$tot; ?></div>
              </li>
            </ul>
          </div>
                <?php } ?>
        </div>
       <div class="paypal-inner">
          <h4>Your 14 day risk free trial…</h4>
          <br>
          <p>You pay $0.01 for your free trial which begins today and ends <?php echo $dt_after_fourteen_days; ?>. Cancel anytime before <?php echo $dt_after_fourteen_days; ?> to avoid further charges. <br>
            <br>
            Payment will begin from <?php echo $dt_after_fifteen_days; ?>, and renews automatically on <?php echo $day_after_fifteen_days; ?>th of every subsequently month. You can cancel anytime before the renewal monthly date, to avoid charges for the next month. </p>
        </div>
        <div class="paypal-inner">
          <h4>Payment method</h4>
        </div>
        <div class="clear"></div>
		<?php $isAlredyDowngradeAccount=isAlredyDowngradeAccount($id);
		if($isAlredyDowngradeAccount=='TRUE'){?>
			               <?php if($country != 1){ ?>
                     <?php if($user_type[0]['user_type'] == "Free"){ ?> 
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="RAGP8S6YWNEDL">
<input type="image" src="https://www.sandbox.paypal.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>

                        <?php } else{ ?>
                                <A HREF="https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=NNTDWDC7V4X58">
<IMG SRC="https://www.sandbox.paypal.com/en_GB/i/btn/btn_unsubscribe_LG.gif" BORDER="0">
</A>
                        <?php } ?>
                <?php } else{ ?>
                        <?php if($user_type[0]['user_type'] == "Free"){ ?> 
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="2DR668P3PVTQW">
<input type="image" src="https://www.sandbox.paypal.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>

                        <?php } else{ ?>
                                <A HREF="https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=NNTDWDC7V4X58">
<IMG SRC="https://www.sandbox.paypal.com/en_GB/i/btn/btn_unsubscribe_LG.gif" BORDER="0">
</A>
                        <?php } ?>
        
                <?php } ?>
			
		<?php }else{?>
		
               <?php if($country != 1){ ?>
                     <?php if($user_type[0]['user_type'] == "Free"){ ?> 
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="D3CEEZRQ7FG6L">
<input type="image" src="https://www.sandbox.paypal.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>

                        <?php } else{ ?>
                                <A HREF="https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=LELLZQBU7LM78">
                                   <IMG SRC="https://www.paypalobjects.com/en_GB/i/btn/btn_unsubscribe_SM.gif" BORDER="0">
                                </A>
                        <?php } ?>
                <?php } else{ ?>
                        <?php if($user_type[0]['user_type'] == "Free"){ ?> 
<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="D3CEEZRQ7FG6L">
<input type="image" src="https://www.sandbox.paypal.com/en_US/GB/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal – The safer, easier way to pay online.">
<img alt="" border="0" src="https://www.sandbox.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1">
</form>
                        <?php } else{ ?>
                                <A HREF="https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias=LELLZQBU7LM78">
                                <IMG SRC="https://www.paypalobjects.com/en_GB/i/btn/btn_unsubscribe_SM.gif" BORDER="0">
                                </A>
                        <?php } ?>
        
                <?php } ?>
		<?php } ?>
      </div>
<?php } ?>
<a class="payment-back" href="javascript: window.history.go(-1)">Go back</a>
