 <?php 
$id = $this->session->userdata['logged_in']['id'];
$details = getParticularUserDetails($id);
$country = $details['country'];

$tot = 14.99;

$obj = new Payment();
$no_of_days_from_signUp = $obj->noOfDaysSinceSignUp();

$dt_after_fourteen_days = date('F j, Y', strtotime("+14 days"));
$dt_after_fifteen_days = date('F j, Y', strtotime("+15 days"));
$day_after_fifteen_days = date('j', strtotime("+15 days"));


$user_type = check_member_payment_status($id);

?>
<div class="premium-wrap">
          <!--<h2>Join the elite today</h2>-->
          <div class="premium-wrap-inner">
		 <div class="payment-right-top">As a founding member, you have a lifetime access to our PREMIUM package</div>
          <div class="premium-top-heading">
            <div class="premium-heading-free">
          <h3>PREMIUM</h3>
          </div>
          <div class="premium-heading-free">
          <h3>FREE</h3>
          </div> 
          </div>
          <div class="clear"></div>    
          <div class="premium-col-listing">
          <h4>Exposure</h4>
          <ul>
          <li>
          <div class="exposure-heading">Business Matching</div>
          <div class="free-left">
          <div class="full-access">Full access</div>
          </div>
          <div class="free-right">
          <div class="full-access last">Full access</div>
          </div>
          </li>
           <li>
          <div class="exposure-heading">The Hub</div>
          <div class="free-left">
          <div class="full-access">View content on the hub</div>
          </div>
          <div class="free-right">
          <div class="full-access last">View content on the hub</div>
          </div>
          </li>
           <li>
          <div class="exposure-heading"></div>
          <div class="free-left">
          <div class="full-access">Post content on the hub for your connections to see</div>
          </div>
          <div class="free-right">
          <div class="full-access last">Post content on the hub for your connections, business in your industry and the industry you target to see</div>
          </div>
          </li>
          <li>
           <div class="exposure-heading"></div>
           <div class="free-left">
          <div class="full-access">Posts will appear normally</div>
          </div>
          <div class="free-right">
          <div class="full-access last">Post will be highlighted and remain at the top of newsfeeds for longer, for maximum exposure</div>
          </div>
          </li>
          <li>
           <div class="exposure-heading">Profile</div>
           <div class="free-left">
          <div class="full-access">Add a business description & up to 5 images or PDF's to showcase your business </div>
          </div>
          <div class="free-right">
          <div class="full-access last">Add a business description, and an unlimited number of documents of any type </div>
          </div>
          </li>
          </ul>
          </div>
          <div class="premium-col-listing">
          <h4>Privacy & Control</h4>
          <ul>
          <li>
          <div class="exposure-heading">Messaging</div>
          <div class="free-left">
          <div class="full-access">Free to message anyone, anytime. Your business name will be disclosed</div>
          </div>
          <div class="free-right">
          <div class="full-access last">Have the option to message anyone anonymously. Arlians will share your industry classification only</div>
          </div>
          </li>
           <li>
          <div class="exposure-heading">Browsing</div>
          <div class="free-left">
          <div class="full-access">Businesses will know if you have viewed them</div>
          </div>
          <div class="free-right">
          <div class="full-access last">Option to browse profiles anonymously</div>
          </div>
          </li>
           <li>
          <div class="exposure-heading">User settings</div>
          <div class="free-left">
          <div class="full-access">1 user account per business profile</div>
          </div>
          <div class="free-right">
          <div class="full-access last">Up to 5 user accounts per business profile. Admin can restrict what other users can do</div>
          </div>
          </li>
          <li>
           <div class="exposure-heading">Geographic settings</div>
           <div class="free-left">
          <div class="full-access">Your business is registered in 1 location</div>
          </div>
          <div class="free-right">
          <div class="full-access last">Your business can be registered in up to 5 locations</div>
          </div>
          </li>
          </ul>
          </div>
          <!--<div class="premium-col-listing">
          <h4>Insight</h4>
          <ul>
          <li>
          <div class="exposure-heading">Benchmarking & analysis module</div>
          <div class="full-access">One-off purchases available</div>
          <div class="full-access last">25% discount on One-off purchases </div>
          </li>
          </ul>
          </div>-->
		  <?php if($is_unsubscribe=='unsub'){?>
     <div class="premium-col-listing">
		<!-- <div class="premium-package-btn">
        <ul>
          <li><a href="javascript: window.history.go(-1)" class="package-btn">GO BACK to premium membership unsubscribe</a></li>
          <li><a href="<?php echo base_url(); ?>member/dashboard" class="package-btn">Yes, I would like to continue my premium subscription</a></li>
          </ul>
        </div> -->
		</div>
		  <?php }else{?>
			  
			  <div class="premium-col-listing">
          <div class="premium-package-btn">
          <ul>
          <li>
          <a href="<?php echo base_url(); ?>member/dashboard" class="package-btn">No thanks, keep me on free</a>
          </li>
          <li>
          <a href="<?php echo base_url(); ?>payment/payFromFoundationMembership" class="package-btn">
		  <span>Exclusive offer</span><br/>
          Founding members
          <p><b>$99</b> one off payment</p>
          <span>for lifetime premium membership access</span>
          </a>
          </li>
          <li>
          <a href="<?php echo base_url(); ?>payment/payFromPaypal" class="package-btn">
          Subscribe / monthly premium
          <?php if($no_of_days_from_signUp <= 35){ ?>
          <p><b>$14.99</b></p>
          <span>Exclusive offer</span>
          <?php } else { ?>
               <p><b>$39.99</b></p>
          <?php } ?>
          </a>
          </li>
          </ul>
          </div>
          
          </div>
			  
		<?php  } ?>
          <p class="subscription-pckage-text">Have any questions about our subscription packages? Contact <a href="mailto:subscriptions@arlians.com">subscriptions@arlians.com </a></p>
          </div>
        </div>
<a class="payment-back" href="javascript: window.history.go(-1)">Go back</a>

