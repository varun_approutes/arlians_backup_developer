<?php 
$id = $this->session->userdata['logged_in']['id'];
$details = getParticularUserDetails($id);
$country = $details['country'];

$tot = 14.99;

$obj = new Payment();
$no_of_days_from_signUp = $obj->noOfDaysSinceSignUp();

$dt_after_fourteen_days = date('F j, Y', strtotime("+14 days"));
$dt_after_fifteen_days = date('F j, Y', strtotime("+15 days"));
$day_after_fifteen_days = date('j', strtotime("+15 days"));


$user_type = check_member_payment_status($id);

?>

 <div class="premium-wrap">
          <h2>As a founding member, you have a lifetime <br> access to our PREMIUM package </h2>
          <div class="premium-wrap-inner premium-wrap-maximum">
          
      <div class="founding-member">
      <h3>PREMIUM</h3>
      <div class="premium-maximum-heading">
      <ul>
      <li>Maximum <br> exposure</li>
      <li>Maximum <br> control</li>
      </ul>
      </div>
      </div>
      <div class="compare-premium">
		  
      <ul class="compare-premium-ul">
       <li>
        <div class="compare-images-left">
      <i class="fa fa-user-secret fa-5"></i>
      </div>
       <h4>Incognito</h4>
        <p>Browse profiles anonymously.</p>
        <div class="prem-tool-tips">
        <div class="prem-tool-tips-open">
       Browse profiles anonymously.
        </div>
        </div>
      </li>
      <li>
      <div class="compare-images-left">
      <i class="fa fa-globe fa-5"></i>
      </div>
     <h4> Tell the world </h4>
     <p> Posts on the hub will travel beyond just your own connections. </p>
      <div class="prem-tool-tips">
        <div class="prem-tool-tips-open">
        Posts on the hub will travel beyond just your own connections.
        </div>
        </div>
      </li>
      <li>
       <div class="compare-images-left">
      <i class="fa fa-comment fa-5"></i>
      </div>
     <h4> Anonymously</h4>
     <p> Explore opportunities without having to reveal your identity. </p>
     <div class="prem-tool-tips">
        <div class="prem-tool-tips-open">
       Explore opportunities without having to reveal your identity.
        </div>
        </div>
      </li>
      <li>
       <div class="compare-images-left">
      <i class="fa fa-star fa-5"></i>
      </div>
     <h4>Stand out</h4>
     <p>Your content will stand out from the crowd, and appear at the top of the hub.</p>
     <div class="prem-tool-tips">
        <div class="prem-tool-tips-open">
        Your content will stand out from the crowd, and appear at the top of the hub.
        </div>
        </div>
      </li>
        <li>
         <div class="compare-images-left">
      <i class="fa fa-users fa-5"></i>
      </div>
     <h4>Free time,delegate</h4>
     <span style="color: rgb(235, 22, 22); font-size: 15px;">(Feature coming soon)</span>
     <p>Add up to 5 users to the business, and control what they can do.</p>
     <div class="prem-tool-tips">
        <div class="prem-tool-tips-open">
        Add up to 5 users to the business, and control what they can do.
        </div>
        </div>
      </li>
      <li>
       <div class="compare-images-left">
      <i class="fa fa-share-alt fa-5"></i>
      </div>
     <h4>A shop window without limits</h4>
     <p>No limits to what you can share on your profile.</p>
     <div class="prem-tool-tips">
        <div class="prem-tool-tips-open">
        No limits to what you can share on your profile.
        </div>
        </div>
      </li>
       <li>
        <div class="compare-images-left">
      <i class="fa fa-map-marker fa-5"></i>
      </div>
     <h4>Teleport</h4>
     <p>Register your business in multiple locations. Increase exposure to local opportunities.</p>
     <div class="prem-tool-tips">
        <div class="prem-tool-tips-open">
        Register your business in multiple locations. Increase exposure to local opportunities.
        </div>
        </div>
      </li>
      </ul>
		<div id="info-pop" style="display:none;">
			<div style="position: relative;">As one of our early members you qualify for our exclusive offer to become a founding member. <a href="<?php echo base_url(); ?>payment/paymentOption" style="color:#4a4a4a; font-size:20px; text-decoration:underline; margin-left:4px;">Find out more here.</a>
				<div style="position: absolute;top:-18px;right:-5px;"><i id="cl" class="fa fa-times fa-1"></i></div>
			</div>
		</div> 
		  
      <div class="comapre-back-btn">
      <a href="<?php echo base_url(); ?>payment/paynow">
      <img alt="" src="<?php echo base_url(); ?>/assets/images/compare-back-btn.png"><br>
      Compare free and <br> premium packages
      </a>
      </div>
      </div>
      <div class="click-member">
      <a href="<?php echo base_url(); ?>payment/paymentOption">Click here to take advantage of the Arlians Founding members offer</a>
      </div>
         <div class="premium-package-btn life-time-prem">
         <ul>
         <li>
		 <?php 
			$date=getUserRegistrationDate($mid);
			$now = time(); // or your date as well
			$your_date = strtotime($date);
			$datediff = $now - $your_date;
			$day_diff= floor($datediff/(60*60*24));
			
		 ?>
          <a class="package-btn" href="<?php echo base_url(); ?>payment/payFromPaypal">
            <?php if($day_diff <= 35){ ?>
                <p class="month-price">$39.99 / month</p>
                <p>14 day free trial, then <b>$14.99</b> / month </p>
                <span>(Introductory offer, valid for 30 days after you first signed in to Arlians)</span>
               
            <?php }else {?>
                    <p> Subscribe premium membership for just $39 / month </p>
            <?php } ?>
           </a>
			
           </li>
           </ul>
          </div>
      
   
          <p class="subscription-pckage-text">Have any questions about our subscription packages? Contact <a href="mailto:subscriptions@arlians.com" >subscriptions@arlians.com </a></p>
		  <a class="payment-back" href="javascript: window.history.go(-1)">Go back</a>
          </div>
        </div>
		<script>
		$(document).ready(function () {
			timer = setTimeout(function () {
				$('#info-pop').removeAttr('style');
			}, 6000);
			
			$("#cl").click(function(){
				$("#info-pop").hide();
			});
		});
		</script>