<div class="row" ng-controller="messageCtrl">     
    <!-- Message page -->
    <div class="col-md-10 col-sm-10">
        <div class="summary box message_header">
            <div class="box-header clearfix">
                <div class="dropdown pull-left">
                    <button class="btn bulk_action dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Action
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a ng-click="refresh_message_page()" href="javascript:void(0);">Compose a new message</a></li>
						<li><a ng-click="deleteSelect()" href="javascript:void(0);">Delete</a></li>
                        <!--                        <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#">Separated link</a></li>-->
                    </ul>
                </div>
                <!--                <button class="btn btn-apply" type="submit">Apply</button>-->
                <!--                <form role="search" class="navbar-form navbar-right"> <div class="form-group"> <input type="text" placeholder="Search" class="form-control"> </div> <button class="btn btn-search" type="submit">Go</button> </form>            -->
            </div>
        </div>
        <div class="show_message" style="display:none; background-color:#357cb7;color:#fff; size:20px; text-align:center; width:20%;"> Message successfully deleted...</div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="table_inner message_first">
                        <div class="mCustomScrollbar">  
                            <div class="table-responsive">
                                <table class="table table-striped"> 
                                    <thead> 
                                        <tr> 
                                            <th>&nbsp;</th> 
                                            <th>Name</th> 
                                            <th>Subject</th> 
                                            <th>Date</th> 
                                            <th>
											
                                        <!--<div id="circularG" style="display:none;" >
                                                  <div id="circularG_1" class="circularG"></div>
                                                  <div id="circularG_2" class="circularG"></div>
                                                  <div id="circularG_3" class="circularG"></div>
                                                  <div id="circularG_4" class="circularG"></div>
                                                  <div id="circularG_5" class="circularG"></div>
                                                  <div id="circularG_6" class="circularG"></div>
                                                  <div id="circularG_7" class="circularG"></div>
                                                  <div id="circularG_8" class="circularG"></div>
                                        </div>
                                         -->
                                        </th>
                                        </tr> 
                                    </thead> 
                                    <tbody> 
                                        <tr ng-repeat="inbox in inbox_list" ng-attr-id="m_{{inbox.msg_id}}" ng-click="showPreviousMail()"> 
                                            <th scope="row">
                                                <input type="checkbox" ng-attr-id="c_{{inbox.msg_id}}" ng-click="selectlist(inbox)" class="cehkbox_select" ng-model="hasChecked[inbox.msg_id]">
<!--                                                <br/><i style="cursor:pointer" ng-click="replyMessage(inbox)" class="fa fa-reply" aria-hidden="true"></i>-->
                                            </th> 
                                            <td style="cursor:pointer" ng-click="replyMessage(inbox)">
                                                <div class="pull-left msg_img bg_yellow_sm">                                                     
                                                    <span ng-show="inbox.image_url == null" ng-bind="inbox.logobussinessname"></span>
                                                    <img ng-show="inbox.image_url != null" src="" ng-src="{{inbox.base_url + inbox.image_url}}" class="img-circle mCS_img_loaded" alt="User Image">
                                                </div>
                                                <h4 ng-show="inbox.inboxname!=''" ng-bind="inbox.inboxname">                                                    
<!--                                                    <a ng-show="inbox.status == 'Receive'" href="<?php echo base_url() ?>memberprofile/profile/{{inbox.bussinessname + '/' + inbox.sender_id}}" ng-bind="inbox.bussinessname">Shaunta Pollan</a>
                                                    <a ng-show="inbox.status == 'Sent'" href="<?php echo base_url() ?>memberprofile/profile/{{inbox.bussinessname + '/' + inbox.receiver_id}}" ng-bind="inbox.bussinessname">Shaunta Pollan</a>-->
                                                </h4>
						<!--<h4 ng-show="inbox.bussinessname==''" ng-bind="inbox.fname+' '+inbox.lname"></h4>-->
												
                                            </td> 
											
                                            <td><p ng-bind="inbox.msg_sub"></p></td>
                                            <td><p class="tabl_date"><span >{{inbox.msgdate | limitTo : 10}}</span></p></td>     
                                            <td style="cursor:pointer" ng-click="Sent_Receive_msg(inbox)"><span  ng-bind="inbox.status" ng-class="{'label label-success' :inbox.status == 'Received', 'label label-primary' :inbox.status == 'Sent'}"></span></td>
                                            <td >	
                                            <div >
                                                            <div  id="circularG" style="display:none;">
                                                              <div id="circularG_1" class="circularG"></div>
                                                              <div id="circularG_2" class="circularG"></div>
                                                              <div id="circularG_3" class="circularG"></div>
                                                              <div id="circularG_4" class="circularG"></div>
                                                              <div id="circularG_5" class="circularG"></div>
                                                              <div id="circularG_6" class="circularG"></div>
                                                              <div id="circularG_7" class="circularG"></div>
                                                              <div id="circularG_8" class="circularG"></div>
                                                    </div>
                                            </div>
                                            </td>
											
											
											
                                        </tr>                                       
                                    </tbody> 
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="message_editpart box mCustomScrollbar">
                    <div class="box-body">
                        <div class="messagesend_box">
                            <div class="message_header">
                                <label>Compose</label>
                                <!--<a href="#" class="refresh_message" name="RefreshBtn" id="RefreshBtn"><i class="fa fa-edit"></i></a> -->                             
                            </div>
                            <form>
                                <div class="form-group">							    
                                    <input type="text" id="demo-input-custom-limits" class="form-control fld"  placeholder="To: [auto fill by user name or email adress]">
                                </div>
                                <div class="form-group">							   
                                    <input type="text" class="form-control"  ng-model="message.message_sub" id="exampleInputPassword1" >
                                </div>
								<!-- <div class="publish_editor">
									<div text-angular ng-model="message.message_content" name="demo-editor6" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
									                        <textarea id="txtEditor"></textarea> 
								</div> -->
								<div class="form-group">	
                                    <div class="publish_editor">
                                        <div text-angular ng-model="message.message_content" name="demo-editor" ta-text-editor-class="border-around form-control" ta-html-editor-class="border-around" rows="3" ng-keypress="total_msg($event)" placeholder="Message:"></div>
                <!--                        <textarea id="txtEditor"></textarea> -->
                                    </div>					   
                                    <!-- <textarea class="form-control"  rows="3" ng-keypress="total_msg($event)"  ng-model="message.message_content" placeholder="Message:"></textarea> -->
                                </div>
								

                               <!-- <span class="expnd_dot"><i class="fa fa-ellipsis-h msg-expnd" data-toggle="tooltip" data-placement="r" title="Click to expand" data-ng-show="expand" aria-hidden="true" ng-click="showPreviousMail()"></i></span>-->
                                <!--                                <div data-ng-show="show_chain" ng-bind-html="chain_msg"></div>-->
                                <button type="submit" ng-click="sendMessage(message,$event)" class="btn btn-primary">Send</button>
								<!--<div class="sm_loader" style="display:inline-block;">
									<div id="circularG" style="display:none;">
										  <div id="circularG_1" class="circularG"></div>
										  <div id="circularG_2" class="circularG"></div>
										  <div id="circularG_3" class="circularG"></div>
										  <div id="circularG_4" class="circularG"></div>
										  <div id="circularG_5" class="circularG"></div>
										  <div id="circularG_6" class="circularG"></div>
										  <div id="circularG_7" class="circularG"></div>
										  <div id="circularG_8" class="circularG"></div>
									</div>
								</div>-->
                            </form>
                        </div>
                        <div>
                           <!--<div data-ng-show="show_chain" ng-bind-html="message.msg_body"></div>-->
							<div  data-ng-show="show_chain" ng-repeat="sent_msg in inbox_list_by_id"  >                
								<div id="rmove">
									<!--<span class="msg_send">Message sent to <span ng-bind="sent_msg.replybusinessname"></span> on <span ng-bind="sent_msg.replyemaildate"></span></span>-->
<!--									<p class="msg_sub" ng-bind="sent_msg.subject"></p>-->
									<p class="msg_bdy" ng-bind-html="sent_msg.body_content">
									  
									</p>
								</div>
							</div> 

                        
                                                      

                        </div>
                    </div>
                </div>
            </div>
        </div>
		
    </div>
		<div class="modal fade" id="sentReceiveModal" tabindex="-1" role="dialog" >
			<div class="modal-dialog">
				<div class="modal-content" ng-repeat="msg in inbox_list_by_id_on_sent_recv">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" ng-bind="msg.subject"></h4>
					</div>
					<div class="modal-body">
						<p ng-bind-html="msg.body_content">&hellip;</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div> 
    <div class="col-md-2 col-sm-2">
        <div class="summary box online_chtlish">
            <div class="box-header">
                <i class="fa fa-user"></i>
                Connections                
            </div>
            <div class="box-body latest_msg">
                <div class="mCustomScrollbar">
                    <ul class="latestmsg_list connections-list" >
                        <li class="clearfix" ng-show="user_mid != connection.mid" ng-repeat="connection in connection_list"><!-- start message -->
                            <div class="pull-left msg_img bg_yellow_sm">                                                     
                                <span ng-show="connection.profile_image == ''" ng-bind="connection.logobussinessname"></span>
                                <img ng-show="connection.profile_image != ''" src="" ng-src="{{connection.image_basepath + connection.profile_image}}" class="img-circle mCS_img_loaded" alt="">
                            </div>
<!--                            <div class="pull-left msg_img">
                                <a href="javascript:void(0)" class="online-status"><i class="fa fa-circle text-success"></i></a>
                                <a href="<?php echo base_url() ?>memberprofile/profile/{{connection.bussiness_name + '/' + connection.mid}}">
                                    <img alt="User Image" class="img-circle" ng-src="{{connection.image_basepath + connection.profile_image}}" src="images/user2-160x160.jpg">
                                </a>
                            </div>-->
                            <h4> 
                                <a href="<?php echo base_url() ?>memberprofile/profile/{{connection.bussiness_name + '/' + connection.mid}}" ng-bind="connection.bussiness_name"></a>
                            </h4>
<!--                            <p>UI Graphic</p>-->
                        </li><!-- end message -->
                        <!--                        <li>
                                                    <div class="pull-left msg_img">
                                                        <a href="#" class="online-status"><i class="fa fa-circle text-success"></i></a>
                                                        <img alt="User Image" class="img-circle" src="images/user3-128x128.jpg">
                                                    </div>
                                                    <h4>Alyssa Molligan </h4>
                                                    <p>Art director, Movie Cut</p>
                        
                                                </li>
                                                <li>
                                                    <div class="pull-left msg_img">
                                                        <a href="#" class="online-status"><i class="fa fa-circle text-success"></i></a>
                                                        <img alt="User Image" class="img-circle" src="images/user4-128x128.jpg">
                                                    </div>
                                                    <h4>Kaitlyn Eddington</h4>
                                                    <p>Writter, Mag Editor</p>
                        
                                                </li>
                                                <li>
                                                    <div class="pull-left msg_img">
                                                        <a href="#" class="online-status"><i class="fa fa-circle text-success"></i></a>
                                                        <img alt="User Image" class="img-circle" src="images/user3-128x128.jpg">
                                                    </div>
                                                    <h4>Rajiv Sharma</h4>
                                                    <p>Art director, Movie Cut</p>
                                                </li>
                                                <li>
                                                    <div class="pull-left">
                                                        <img alt="User Image" class="img-circle" src="images/user4-128x128.jpg">
                                                    </div>
                                                    <h4>Anupan Mallick</h4>
                                                    <p>Designer</p>
                                                </li>
                                                <li>
                                                    <div class="pull-left">
                                                        <img alt="User Image" class="img-circle" src="images/user4-128x128.jpg">
                                                    </div>
                                                    <h4>Dipankar Pal</h4>
                                                    <p>Lorem ipsum</p>
                                                </li>
                                                <li>
                        
                                                    <div class="pull-left">
                                                        <img alt="User Image" class="img-circle" src="images/user3-128x128.jpg">
                                                    </div>
                                                    <h4>Moumita Pal</h4>
                                                    <p>Lorem ipsum</p>
                        
                                                </li>
                                                <li>
                        
                                                    <div class="pull-left">
                                                        <img alt="User Image" class="img-circle" src="images/user4-128x128.jpg">
                                                    </div>
                                                    <h4>Prasenjit Chatterjee</h4>
                                                    <p>Lorem ipsum</p>
                        
                                                </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="styleSheet" href="<?php echo base_url(); ?>assets/css/token-input.css" />	
<script src="<?php echo base_url(); ?>assets/js/jquery.tokeninput.js"></script>
<script src="<?php echo base_url(); ?>assets_phase2/js/message.js"></script>
<script>
                                    $(function () {
                                        $("#demo-input-custom-limits").tokenInput("<?php echo base_url() . 'member/autocomplete' ?>", {
                                            searchDelay: 15,
                                            minChars: 1,
                                            propertyToSearch: "email",
                                            propertyToEmail : "bussinessname",
                                            tokenValue: "mid",
                                        });
                                    });
</script>

