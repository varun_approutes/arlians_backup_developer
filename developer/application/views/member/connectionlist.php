<div class="row" id="dashboard_display1" style="display:block">
    <div class="col-md-9 col-sm-12" ng-controller="UserConnectionsCtrl">
       
          
            <div class="connection_sorting">
              <ul>
                <li><a href="#" ng-click="functionselectchar('a')">a</a></li>
                <li><a href="#"  ng-click="functionselectchar('b')">b</a></li>
                <li><a href="#"  ng-click="functionselectchar('c')">c</a></li>
                <li><a href="#"  ng-click="functionselectchar('d')">d</a></li>
                <li><a href="#"  ng-click="functionselectchar('e')">e</a></li>
                <li><a href="#"  ng-click="functionselectchar('f')">f</a></li>
                <li><a href="#"  ng-click="functionselectchar('g')">g</a></li>
                <li><a href="#"  ng-click="functionselectchar('h')">h</a></li>
                <li><a href="#"  ng-click="functionselectchar('i')">i</a></li>
                <li><a href="#"  ng-click="functionselectchar('j')">j</a></li>
                <li><a href="#"  ng-click="functionselectchar('k')">k</a></li>
                <li><a href="#"  ng-click="functionselectchar('l')">l</a></li>
                <li><a href="#"  ng-click="functionselectchar('m')">m</a></li>
                <li><a href="#"  ng-click="functionselectchar('n')">n</a></li>
                <li><a href="#"  ng-click="functionselectchar('o')">o</a></li>
                <li><a href="#"  ng-click="functionselectchar('p')">p</a></li>
                <li><a href="#"  ng-click="functionselectchar('q')">q</a></li>
                <li><a href="#"  ng-click="functionselectchar('r')">r</a></li>
                <li><a href="#"  ng-click="functionselectchar('s')">s</a></li>
                <li><a href="#"  ng-click="functionselectchar('t')">t</a></li>
                <li><a href="#"  ng-click="functionselectchar('u')">u</a></li>
                <li><a href="#"  ng-click="functionselectchar('v')">v</a></li>
                <li><a href="#"  ng-click="functionselectchar('w')">w</a></li>
                <li><a href="#"  ng-click="functionselectchar('x')">x</a></li>
                <li><a href="#"  ng-click="functionselectchar('y')">y</a></li>
				<li><a href="#"  ng-click="functionselectchar('z')">z</a></li>
				</ul>
				<ul class="numbershort">
                <li><a href="#"  ng-click="functionselectchar('0')">0</a></li>
				<li><a href="#"  ng-click="functionselectchar('1')">1</a></li>
				<li><a href="#"  ng-click="functionselectchar('2')">2</a></li>
				<li><a href="#"  ng-click="functionselectchar('3')">3</a></li>
				<li><a href="#"  ng-click="functionselectchar('4')">4</a></li>
				<li><a href="#"  ng-click="functionselectchar('5')">5</a></li>
				<li><a href="#"  ng-click="functionselectchar('6')">6</a></li>
				<li><a href="#"  ng-click="functionselectchar('7')">7</a></li>
				<li><a href="#"  ng-click="functionselectchar('8')">8</a></li>
				<li><a href="#"  ng-click="functionselectchar('9')">9</a></li>
				<li><a href="#"  ng-click="functionselectchar('')"><span>Show All</span></a></li>
              </ul>
			  <div id="message_modal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Message</h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <textarea ng-model="message" class="popmsg_area" placeholder="Type message..."></textarea>
                                                                                              
                                      
                                    </div>
                                    <div class="modal-footer">
                                        
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" ng-click="sendMessage(mid)" class="btn btn-primary">Save</button>
                                    </div>
                                </div>
                            </div>
                        </div>
			  
			  
			  
			  
			  
            </div>
            
             <div class="row">
              <div class="connection_list clearfix" >
                 <div class="col-sm-4 col-md-3" ng-repeat="ufriend in userfriend  | startsWithLetter : fval" ng-attr-id="{{ 'UID-' + ufriend.mid }}">
                   <div class="connection_box" >
                       <div class="connection_header clearfix">
                          <div class="image default_image"> 
                            <span ng-bind="ufriend.logobussinessname">bn</span>                                                 
                            </div>
                            
                            <div class="latest_content">
                                <div class="latest_header">
                                  <a target="_blank"  href="<?php echo base_url() ?>memberprofile/profile/{{ufriend.bussiness_name + '/' + ufriend.mid}}"><h6 class="ng-binding" ng-bind="ufriend.bussiness_name">Business Name</h6></a>
                                 </div>
                                  <span  class="connection_location" ng-bind="ufriend.location">location</span>
                            </div>
                            
                            </div>
                            
                       <div class="connection_body">
                        <!--  <img src="<?php echo base_url(); ?>assets_phase2/images/{{ufriend.back_image}}" alt=""> -->
                         <img ng-if="ufriend.back_image!=''" src="{{ufriend.image_basepath + ufriend.back_image}}" alt="">
						  <img ng-if="ufriend.back_image ==''" src="<?php echo base_url(); ?>assets_phase2/images/connection-default.jpg" alt="">
                       </div>
                       <div class="connection_footer clearfix">
                         <a href="#" class="pull-left delete_connection"  ng-click="connectionLogic(ufriend.mid)" ><i class="fa  fa-minus-circle"></i></a>
                         <a href="#" class="pull-right" ng-click="openMessagePopup(ufriend.mid)"><i class="fa  fa-envelope"></i></a>
                       </div>
                    </div>
					
					
					
					
					
                 </div>
               
              </div>

        </div>
    </div>
                                        
                                        
                                        
    <div class="col-md-3 col-sm-12  fixed-div1" id="id-31"   ng-controller="DashboardSuggestionCtrl as suggestion"  >
        <div class="row">   
            <div class="match_s mCustomScrollbar">  
                <div class="col-sm-6 col-md-12" ng-show="search_module">
                    <div class="view_result_box fixd_box1">
                        <div class="view_box_top"><h4>See All Matching <!--<small><i class="fa fa-search"></i></small>--></h4></div>
                        <div class="view_box_body">
                            <div class="match_result">
                                <span class="mathc_txt">MATCHING <br> RESULTS</span><br>
                                <span class="mathc_resulttxt" ng-bind="total_suggestion_count">132</span>
                            </div>
                            <div class="result_bottom clearfix">
                                <div id="result-circle" class="owl-carousel">
                                    <div class="item" ng-repeat="list in detail_suggestion_list">
                                        <div class="result_numb">
                                            <div class="result_numbin" ng-bind="list.type_count">69</div>
                                        </div>
                                        <span class="result_name" ><a ng-bind="list.type" href="javascript:void(0)" ng-click="getDetailSuggestion(list)">Customers</a></span>
                                    </div>                                 
                                </div>
                            </div>
                        </div>
                        <div class="view_box_bottom">
                           <!-- <a href="javascript:void(0);" class="view_menu" ng-click="openWizardTwoModal()"><i class="fa fa-list"></i></a>-->
                            <a href="javascript:void(0);" class="view_menu" ng-click="funRedirectToSetting()" ><i class="fa fa-list"></i></a>
   
                        </div>                      


                    </div>
                </div>
                <div class="col-sm-6 col-md-12"  ng-show="show_detail" id="detail_suggestion">
                    <div class="view_result_box fixd_box1" ng-repeat="detail in details_suggestion">
                        <div class="view_box_top1 matching clearfix"><h4 ng-bind="search_for_suggestion">Customers</h4>
                            <div class="result_numb1">
                                <div class="result_numbin1" ng-bind="total_count">69</div>
                            </div>
                        </div>
                        <div>
                            <div class="view_box_body1">
                                <div class="matchign_cont">
                                    <a  href="<?php echo base_url() ?>memberprofile/profile/{{detail.bussinessname + '/' + detail.mid}}" class="matchig_logo">
                                        <span  ng-show="detail.image == ''" ng-bind="detail.logobussinessname"></span>
                                        <!-- <img ng-show="detail.image != ''" ng-src="{{detail.image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+detail.image:'<?php echo base_url(); ?>assets/images/inbox-img.png'}}" alt="" >                                     -->
                                    
                                         <img ng-show="detail.image != ''" ng-src="{{detail.image!=null?detail.image:'<?php echo base_url(); ?>assets/images/inbox-img.png'}}" alt="" >                                  

                                    </a>
                                    <h5 ng-bind-html="detail.bussinessname | limitTo:100">Company Name</h5>
                                    <p class="match_detail" ng-bind-html="detail.desc | limitTo:120">Excel at enterprise, mobile and web development with Java, Scala, Groovy and Kotlin, with all the latest modern technologies and frameworks available out of the box. </p>
                                </div>
                                <img src="<?php echo base_url() ?>assets_phase2/images/match_enginebg.jpg" alt="">
                            </div>
                            <div class="view_box_body1 text-center clearfix">
                                <a href="javascript:void(0);" class="result_numb3" ng-click="individual_suggestion_user_feedback(detail, 0)">
                                    <div class="result_numbin3"><i class="fa fa-minus"></i></div>
                                </a>
                                <a href="javascript:void(0);" class="result_numb4" ng-click="individual_suggestion_user_feedback(detail, 1)">
                                    <div class="result_numbin4"><i class="fa fa-plus"></i></div>
                                </a>
                                <div class="clearfix match_score">
                                    <div class="" ng-class="{'result_numb_red'   : detail.score == 'one',
                                    'result_numb_amber' : detail.score == 'two',
                                    'result_numb_green'    : detail.score == 'three'}">
                                        <div class=""ng-class="{'result_numbin_red'   : detail.score == 'one',
                                    'result_numbin_amber' : detail.score == 'two',
                                    'result_numbin_green'    : detail.score == 'three'}" ng-bind="detail.score">69</div>

                                    </div>

                                    <span class="score_txt" ng-bind="detail.matched_text | limitTo:140">Match Score</span>
                                </div>
                            </div>
                        </div>
                        <div class="view_box_bottom">
                            <span class="backto_result" >
                                <a href="javascript:void(0);" ng-click="backToSuggestionList()">Back to matching results</a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-12">
                    <img class="p-d"  src="<?php echo base_url(); ?>assets_phase2/images/p-d.jpg" alt=""/>
                   
                </div>
            </div>
        </div>
    </div>
</div>

 <div class="modal fade" id="participateModal" tabindex="-1" role="dialog" >
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" ng-bind="talk.bussiness_name"></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>This talk is private.So you need to take permission before participate&hellip;</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary" id="sendreqstid" >Send Request</button>
                                                    </div>
													
											<!--<div id="loader-wrapper">
												<div id="loading-center">
													<div id="loading-center-absolute">
														<div class="object" id="object_one"></div>
															<div class="object" id="object_two"></div>
														<div class="object" id="object_three"></div>
													</div>
												</div>
											</div>-->
													
													
													
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        
                                        



<!-- common-helper JS -->   
<link rel="styleSheet" href="<?php echo base_url(); ?>assets/css/token-input.css" />

<script src="<?php echo base_url(); ?>assets_phase2/js/userConnection.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.tokeninput.js"></script> 
<script src="<?php echo base_url(); ?>assets_phase2/js/custom-file-input.js"></script>
<!-- 05-04-16 for fade post -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets_phase2/js/waypoint-fade.min.js"></script>





<script>
                                            $(window).resize(function() {
                                    $('.mCustomScrollbar').height($(window).height());
                                    });
                                            $(window).trigger('resize');

                                            </script>




<script>
            $(function(){
            $('#hub_publish_default_img').show();
                    $('#hub_publish_preview_img').hide();
                    $('.left-block, .tab-sticky-scroll')
                    .theiaStickySidebar({
                    additionalMarginTop: 50
                    });
//CKEDITOR.replace('txtEditor');
                    
                    $('.mCustomScrollbarN').mCustomScrollbar({
            //setHeight: 250
            //autoExpandScrollbar : false
            });
            });</script>

<script>
            $(document).ready(function() {

    });
            (function($){
            $(window).load(function(){

            $("#content-1").mCustomScrollbar({
            axis:"yx", //set both axis scrollbars
                    advanced:{autoExpandHorizontalScroll:true}, //auto-expand content to accommodate floated elements
                    // change mouse-wheel axis on-the-fly 
                    callbacks:{
                    onOverflowY:function(){
                    var opt = $(this).data("mCS").opt;
                            if (opt.mouseWheel.axis !== "y") opt.mouseWheel.axis = "y";
                    },
                            onOverflowX:function(){
                            var opt = $(this).data("mCS").opt;
                                    if (opt.mouseWheel.axis !== "x") opt.mouseWheel.axis = "x";
                            },
                    }
            });
            });
            })(jQuery);</script>
<script>
            $(function () {
            $("#demo-input-custom-limits").tokenInput("<?php echo base_url() . 'member/autocomplete' ?>", {
            //$("#demo-input-custom-limits_recommend").tokenInput("<?php echo base_url() . 'member/autocomplete' ?>", {
            searchDelay: 2000,
                    minChars: 2,
                    propertyToSearch: "bussinessname",
                    tokenValue: "mid",
            });
            });

</script>