  <div class="dashboard-wrap"> 
          <div class="row MAR-TOP25">
            <div class="publish-post-left">
				<h2>Yours Post</h2> 
				<ul>
					<?php foreach($my_publish as $pub){ ?>
					<li id="<?php echo $pub['hid']; ?>">
						<h4><a href="<?php echo base_url(); ?>hub/hubPublishEdit/<?php echo $pub['hid']; ?>"><?php echo $pub['hub_title']; ?></a></h4>
						<p><?php echo $pub['posted_on']; ?></p>
						<a class="edit" href="<?php echo base_url(); ?>hub/hubPublishEdit/<?php echo $pub['hid']; ?>"><i class="fa fa-pencil fa-1"></i></a>
						<a class="del" href="javascript:void(0);"><i class="fa fa-times fa-1"></i></a>
					</li>
					<?php } ?>
				</ul>
            </div>
             
              <div class="publish-right-left">
			  <form name="pub_post" id="pub_post" action="<?php echo base_url(); ?>hub/hub_edit_post" method="post" enctype="multipart/form-data">
			  
              <div class="publish-btn">
			  <input type="submit" value="Publish" class="Publish">
              </div>
              <div class="publish-upload">
              <div class="upload-file">
               <input type="file" name="file_hub_post" id="img_submit">
			   
               <label>Choose files</label>
               </div>
               <img id="show_hub_img" src="<?php echo base_url(); ?>uploads/hub_images/<?php echo $link; ?>" style="height:300px !important" alt=""/>
			   <input type="hidden" name="prev_img" id="prev_img" value="<?php echo $link; ?>">
			   <input type="hidden" name="hid" value="<?php echo $hid; ?>">
               <div class="clear"></div>
              <h2>Add an image to bring your post to life</h2>
              <div class="clear"></div>
              <p>Images that are at least 700*400 pixels look best </p>
              <div class="publish-cross"><a href="javascript:void(0)" id="del_publish_img"><i class="fa fa fa-times fa-2"></i></a></div>
              </div>
              <!--<div class="publish-post-images">
              <div class="publish-images">
               <img src="images/publish-images.png" alt=""/>
              </div>
              </div> -->
              <div class="headline-text-publish">
			  <input type="text" placeholder="Write Your Headline" name="title" value="<?php echo $hub_title; ?>" required>
			  </div>
			  
              <textarea name="comments" style="width:100%; height:200px; border:1px solid #cecece; padding:15px; background:#e1e1e1;">
			  <p><?php echo $content; ?></p>
              </textarea>
             
			  <?php $tag_arr = explode(',',$tag_id); ?>
			<div class="publish-multi-select">
			  <span class="select">
				 <select class="tag" onchange="count_option()" name="tag[]" id="tag" multiple>
				<?php foreach($tag as $tag_row){
					$attr;					
						if(in_array($tag_row['tag_code'], $tag_arr)){
							$attr = 'selected="selected"';
						}else{
							$attr = '';
						}
				?>
				
				  <option value='<?php echo $tag_row['tag_code']; ?>' <?php echo $attr; ?>><?php echo $tag_row['tag_name']; ?></option>
				<?php } ?>
				</select>
			  </span>
			</div>
			
			  </form>
              </div>
                  
                   </div>
          
                   
                   </div>
				   
<script>
//Preview & Update an image before it is uploaded
var is_image_cancel=0;
function readURL_hub(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#show_hub_img').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#img_submit").change(function(){
    readURL_hub(this);
});
 $("#show_hub_img").load(function() {
		var max_size = 837;
		$("#show_hub_img").show();
		$("#show_hub_img").each(function(i) {
		  if ($(this).height() > $(this).width()) {
			var h = max_size;
			var w = Math.ceil($(this).width() / $(this).height() * max_size);
		  } else {
			var w = max_size;
			var h = Math.ceil($(this).height() / $(this).width() * max_size);
		  }
		  $(this).css({ height: h, width: w });
		});
   });
   
	$(document).ready(function(){
    $('#tag'). tokenize({placeholder:"Start typing your tags"});
		$('#tag').change(function(event) {
		if ($(this).val().length > 3) {
		  alertify.error('You can only choose 3!');
		  $(this).val(last_valid_selection);
		} else {
		  last_valid_selection = $(this).val();
		}
	  });	
	  
	   $("#pub_post").validate({
                 rules: {
                    title: { required: true }
                },
                messages: {
                    title: { required: "Please enter your hub title" }
                },
                submitHandler: function (form) {
					if(is_image_cancel>0){
						var imgVal = $('#img_submit').val(); 
						if(imgVal=='') 
						{ 
							alertify.error("Please upload your file");
							return false; 
						}
					}
					form.submit();
					return true;
				}
	   });
	});
</script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/tinymce/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({

    selector: "textarea"

 });
 
 $('.del').click(function(){
 	var cur_obj = this;
 	alertify.confirm("Are you want to delete the post", function (e) {
		if (e) {
			var id = $(cur_obj).parent('li').attr('id');
			var url = '<?php echo base_url(); ?>hub/hub_delete';
			var obj = $(cur_obj);	
			$.post(url,{'id':id},function(res){
				if(res == 1){
					$(obj).parent('li').remove();
					alertify.success('Publish deleted');
					window.location= "<?php echo base_url();?>hub/hubPublish"
				}else{
					alertify.error('Error occured while deleting. Please try again');
				}		 
			});
		}
	});
 });
 
$('#del_publish_img').click(function(){
	is_image_cancel++;
	$("#img_submit").val("");	
	$('#show_hub_img').attr('src','');
	$('#prev_img').val('');
});

</script>
				   
				   


    