<div class="row" id="dashboard_display" style="display:block">
    <div class="col-md-9 col-sm-12" ng-controller="DashboardHub as hub">
        <div class="row">
            <div class="col-md-6 col-sm-6" id="id-1"   >
                <div class="row" id="class_animate">
                    <div class="default_post post_s">
                        <div class="col-md-12" ng-show="show_default_hub">
                            <div class="box post_update">
                                <div class="post-header">
                                    <ul class="post_link">
                                        <li><a href="javascript:void(0)"><i class="fa fa-edit"></i> Status</a></li>
                                        <li> 
                                            <div>
                                                <input type="file" name="file-3" ng-files="getTheFiles($files)" id="file-3" class="inputfile inputfile-3" data-multiple-caption="{count} files selected"/>
                                                <label for="file-3">                                             
                                                    <span><i class="fa fa-photo"></i> <label class="sm_none">Photos</label> / <i class="fa fa-video-camera"></i> <label class="sm_none">Videos</label> </span>
                                                </label>
                                            </div>
                                        </li>
                                        <!-- <li><a href="javascript:void(0)"><i class="fa fa-photo" ng-click="showPublishSection()"></i><span class="sm_none">Photos</span> / <i class="fa fa-video-camera" onclick="alertify.alert('Section is under construction')"></i><span class="sm_none"> Videos</span></a></li> -->
                                        <!--<li><a href="<?php echo base_url() ?>hub/hubPublish"><i class="fa fa-sign-out"></i> Publish</a></li>-->
                                        <li><a href="javascript:void(0);"  ng-click="showPublishSection()"><i class="fa fa-sign-out"></i> Publish</a></li>
                                    </ul>
                                </div>
                                <form ng-submit="sharePost()"> 
                                    <div class="box-body">
                                       <!--  <textarea class="form-control post_field" placeholder="What’s on your mind?" ng-blur="postUrl(text_in_mind)" ng-model="text_in_mind"></textarea>                    -->
                                      
                                        <textarea class="form-control post_field" placeholder="What’s on your mind?"  ng-model="text_in_mind"></textarea>                   


                                        <div class="img_pic" id="post_img" style="display:none"><i class="fa fa-close" ng-click="removeDoc($event)"></i><img id='p_img' src="" /></div>                                       

                                        <div class="news_headlineholder" id="postUrlBlank" ng-bind-html="html" style="display:none">

                                            <i class="fa fa-close"></i>
                                        </div> 
                                    </div>
                                    <div class="post_footer clearfix">
                                        <span class="share_list" style="color:#fff" ng-bind="show_selected">Hello world</span>
                                        <!--<ul class="post_footerlink">
                                            <li><a href="#"><i class="fa fa-user-plus"></i></a></li>
                                            <li><a href="#"><i class="fa fa-video-camera"></i></a></li>
                                            <li><a href="#"><i class="fa fa-photo"></i></a></li>
                                        </ul>-->
                                        <div class="post_footebtn pull-right">
                                            <div class="btn-group post_footerdrpbtn">
                                                <!--<i class="fa fa-gears pull-left"></i>-->
                                                <!-- <button aria-expanded="false"  ng-model="loadbtn" ng-if="loadbtn" ng-attr-id="loadbtn"  type="button" ng-click="postUrl(text_in_mind)" class="btn btn-sm">Load</button> -->
                                                <button aria-expanded="false" ng-model="postbtn" ng-if="postbtn" ng-attr-id="postbtn" type="button" ng-click="select()" class="btn btn-sm">Post</button>
                                                <!--                                                <button aria-expanded="false" type="button" data-toggle="dropdown" class="btn  btn-sm dropdown-toggle">Post</button>
                                                                                                <ul role="menu" class="dropdown-menu pull-right">                                        
                                                                                                    <li><a href="javascript:void(0);" ng-click="select(list)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                                    
                                                                                                </ul>-->
                                                <!--                                        <div class="share_btn">
                                                                                            <div class="select-style">
                                                                                                <select ng-options="opt.shareId as opt.name for opt in share_with_list">
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>-->
                                            </div>
                                            <!--<button type="button" class="btn post_btn" ng-click='sharePost()'>Post</button>-->
                                            <!--                                    <a href="javascript:void(0)" class="btn post_btn" ng-click='sharePost()'>Post</a>-->

                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                        </div>

                        <!-- for publish section -->
                        <div class="box publish_section clearfix" ng-show="show_publish">
                    <div class="publish_toppart">
                        <!--                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                          Launch demo modal
                        </button>-->

                        <!-- Modal Crop Image-->
                        <div class="modal fade" id="crop_publish_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Upload an Image</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-crop">
                                            <div class="imageBox">
                                                <div class="thumbBox"></div>
                                                <div class="spinner" style="display: none">Loading...</div>
                                            </div>
                                            <div class="action">

                                            </div>
                                            <div class="cropped"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="note">image size 450 x 250  for best result</span>
                                        <input type="file" id="file">                                               
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                        <button type="button" id="btnZoomIn" class="btn btn-primary">+</button>
                                        <button type="button" id="btnZoomOut" class="btn btn-primary">-</button>
                                        <button type="button" id="btnCrop" class="btn btn-primary">Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        <input type="file" id="upload_img_for_post" onchange="readURLPostImage(this);"/>-->
                        <i class="fa fa-camera change_photo" id="publish_img_icon" style="cursor:pointer" onclick="open_publish_crop_img_modal()"></i>
                        <p class="pubimg_txt">Add an image to your publication<br><span>Best if 450 x 250pixel</span></p>
<!--                        <input type="file" ng-model="image" ng-change="readURLPostImage(this)"><i class="fa fa-camera change_photos"></i></input>-->

                        <div class="publish_namecontainer">
                           <!--  <img ng-src="{{front_img_name != '' && path + front_img_name|| 'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image"> -->
<!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                            <img ng-src="{{front_img_name != '' && path + front_img_name|| 'http://arlians.com/developer/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image">
                            <div class="pulish_heading">
                                <span class="publis_name" ng-bind="bussinessname">Digitalweb</span>
                                <p class="publish_subtxt" ng-bind-html="tag_title">Production of modern website</p>
                            </div>
                        </div>
                        <!--                        <div></div>-->
                        <img id="hub_publish_default_img" src=""  alt="">
                        <img id="hub_publish_preview_img" class="hub_publish_preview_img" src="" alt="">
                    </div>



                    <input type="text" ng-model="publish_header" class="write_headline" placeholder="Write your headline" />

                    <!--                    <h5>Write your headline</h5>-->

                    <div class="publish_editor">
                        <div text-angular ng-model="content_about_publish" name="demo-editor" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
<!--                        <textarea id="txtEditor"></textarea> -->
                    </div>
<!--                    <div class="post-header add_tags"><i class="fa fa-tag"></i>Add tags</div>   
                    <div class="bs-example">
                        <input type="text" value="tag name" data-role="tagsinput" />
                    </div>-->
                    <!--<div class="clearfix">
                        <span style="color:#345672" ng-bind="show_post_selected">Hello world</span>
                        <div class="btn-group post_footerdrpbtn">
                            <i class="fa fa-gears pull-left"></i>
                            <button aria-expanded="false" data-toggle="dropdown" class="btn  btn-sm dropdown-toggle">Share with</button>
                            <ul role="menu" class="dropdown-menu pull-right">                                        
                                <li><a href="javascript:void(0);" ng-click="select_post(list)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                
                            </ul>
                        </div>
                    </div>-->

                    <!--  <div class="editor_btn">
                         <button class="btn btn_publish pull-right" ng-click="showDefault()">Back</button>
                         <button class="btn btn_publish pull-right" ng-click="savePublish()">Publish</button>
                     </div> -->
                    <div class="editor_btn box-body pull-right">
                        <button class="btn btn_publish" ng-click="showDefault()">Back</button>
                        <button class="btn btn_publish" ng-click="savePublish()">Publish</button>
                    </div>

                </div>





                        <!-- for publish section end -->




                                    
                    </div>

<!--- nilanjan add modal -->
                         <!-- Share Modal -->
                        <div id="myShareModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>  
                                    </div>
                                    <div class="modal-body clearfix">
                                        <textarea ng-model="shareObj.hbComment" class="share_input" placeholder="Say something about this..."></textarea>
                                        <div ng-bind="shareObj.hub_title"></div>
                                        <span ng-if="shareObj.ext != 'mp4'">                                            
                                            <span ng-if="shareObj.news_link != null && shareObj.link != ''">
                                                <!--<img  ng-class="shareObj.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{shareObj.link}}" src="">-->
                                                <img class="" alt="" ng-src="{{shareObj.link}}" src="">
                                                <!--  </span>
                                                 <span ng-if="shareObj.news_link == null && shareObj.ext != null">
                                                     <img ng-show="shareObj.ext != null" ng-class="shareObj.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{shareObj.base_url + shareObj.link}}" src="<?php echo base_url(); ?>assets_phase2/images/post-img.jpg">
                                                 </span> -->
                                            </span>
                                            <span ng-show="shareObj.news_link == null && shareObj.ext != null">
                                                <img ng-show="shareObj.ext != null" ng-class="shareObj.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{shareObj.base_url + shareObj.link}}" src="<?php echo base_url(); ?>assets_phase2/images/post-img.jpg">
                                            </span>
                                        </span>
                                <!--<img ng-show="shareObj.link != null" ng-model="shareObj.link" src="{{shareObj.base_url + shareObj.link}}"></img>-->
                                        <!-- <div ng-hide="shareObj.content == null || shareObj.content == '' || shareObj.content == ' ' && shareObj.hbcomment != null  " ng-bind-html="shareObj.content"></div>
                                        <div ng-hide="shareObj.hbcomment == null || shareObj.hbcomment == '' || shareObj.hbcomment == ' '" ng-bind-html="shareObj.hbcomment"></div>
                                        -->

                                        <!--  <div  ng-bind-html="shareObj.hbcomment?shareObj.hbcomment:shareObj.content"></div> -->
                                        <!--  <div ng-show="shareObj.content == null && shareObj.shared_for !=='0'"  ng-bind-html="shareObj.hbcomment"></div> -->
                                        <div ng-show="shareObj.content == null && shareObj.shared_for !== '0'"  ng-bind-html="getstringmodal(shareObj.hbcomment)"></div>
                                        <div ng-show="shareObj.content != ''"  ng-bind-html="shareObj.content"></div>
                                        <div ng-show="shareObj.hbcomment != '' && shareObj.flag != 1 && shareObj.shared_for === '0'"  ng-bind-html="shareObj.hbcomment"></div>

                                    </div>
                                    <div class="modal-footer">
                                        <!--                                        <button type="button" class="btn btn-primary" ng-click="saveShareComments(shareObj)" >Post</button>-->
                                        <!-- <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Share post With &nbsp;<span class="caret"></span></button> -->
                                        <button type="button" class="btn btn-primary" id="savesharecmnt" ng-click="saveShareComments(share_with_list[0], shareObj)" >Share post</button>
                                        <!-- <ul class="dropdown-menu  pull-right">
                                            <li><a href="javascript:void(0);" ng-click="saveShareComments(list, shareObj)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                       
                                        </ul> -->
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="myShareNewsModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <!--                                        <div class="btn-group">
                                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        Share post With &nbsp;<span class="caret"></span>
                                                                                    </button>
                                                                                    <ul class="dropdown-menu">
                                                                                        <li><a href="#">Action</a></li>                                          
                                                                                    </ul>
                                                                                </div>-->

                                    </div>
                                    <div class="box-body">
                                        <textarea ng-model="shareNewsObj.hbComment" class="share_input" placeholder="Say something about this..."></textarea>
                                        <div ng-bind="shareNewsObj.hub_title"></div>
                                        <span ng-if="shareNewsObj.link != null">
                                            <img ng-show="shareNewsObj.link != null" class="post_img" alt="" ng-src="{{shareNewsObj.link}}" src="<?php echo base_url(); ?>assets_phase2/images/post-img.jpg">
                                        </span>                                        
                                <!--<img ng-show="shareObj.link != null" ng-model="shareObj.link" src="{{shareObj.base_url + shareObj.link}}"></img>-->
                                        <div ng-bind-html="shareNewsObj.content"></div>

                                    </div>
                                    <div class="modal-footer">
<!--                                        <button type="button" class="btn btn-primary" ng-click="saveShareNewsComments(shareNewsObj)" >Post<span class="caret"></span></button>-->

                                        <!--  <button type="button" class="btn btn-primary" id="savesharecmnt" ng-click="saveShareComments(share_with_list[0], shareObj)" >Share post With </button> -->
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" ng-click="saveShareNewsComments(share_with_list[0], shareNewsObj)">Share post</button>
                                        <!-- <ul class="dropdown-menu  pull-right">
                                            <li><a href="javascript:void(0);" ng-click="saveShareNewsComments(list, shareNewsObj)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                       
                                        </ul> -->
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div> 
                        </div>

                        <div class="modal fade" id="publish_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Publish</h4>
                            </div>
                            <div class="modal-body">
                                <div class="publish_second">
                                    <br/>  
                                    <div class="publish_toppart">
                                        <img ng-show="hub.link != null" ng-src="{{hub.base_url + hub.link}}"  alt="">
                                        <div class="publish_namecontainer">
                                            <div>
                                                <span ng-show="hub.prof_img == ''" class="publish_avator" ng-bind="hub.profile_initional"></span>                                   
                                                <img ng-show="hub.prof_img != ''" ng-src="{{hub.prof_img}}" class="publish_avator" alt="User Image">
                                            </div>
                <!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                                            <div class="pulish_heading">
                                                <span class="publis_name" ng-bind="hub.bussinessname">Digitalweb</span>
                <!--                                <p class="publish_subtxt">Production of modern website</p>-->
                                            </div>
                                            <div class="publish_adduser" ng-show="connected">
                                                <i class="fa fa-user-plus" ng-click="connectionLogic(hub.hid)"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="publish_actionarea">
                                        <h5 ng-bind="hub.hub_title">Write your headline</h5>

                                        <div class="publishfooter clearfix">
                                            <ul class="latest_footerlink pull-left publishfooterlink">
                                                <li><a href="javascript:void(0)" ng-click="shareHub(hub)"> Share</a></li>
                                                <li><a href="javascript:void(0)" ng-click="open_comment_Animation(hub)"> Comments</a></li>
                                                <li><a href="javascript:void(0)" ng-class="{'like': hub.like != 0}" ng-click="feedback(hub)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right">
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.total_share"> 23 </span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a href="javascript:void(0)" ng-click="commentsAnimation(hub)"><span ng-bind="hub.comment_count">45</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.likes">23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                            <p class="pub_date">Published <span ng-bind="hub.hub_post_time">23/05/16</span></p>


                                        </div>
                                    </div>
                                    <div class="publish_editor_txt  box-body">
                                        <div  ng-bind-html="hub.content">

                                        </div>
                                        <div class="pub_comment_section" ng-show="hub.open_comment_box">
                                            <textarea rows="2" class="publish_cmntpart" ng-model="hub.comment"></textarea>
                                            <button class="btn btn_publish  pull-right" ng-click="postHubComments(hub)">post</button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">                                                                           
                                <button class="btn btn_publish pull-right" data-dismiss="modal">Back</button>
                            </div>
                        </div>
                    </div>
                </div>






                        <!-- end modal add -->






                    <!-- Normal All Type of post Start-->
                        <div class="alltype_post" ng-repeat="hub in hub_list" >


                        <div class="col-sm-12 normal_textpost" ng-if="hub.POSTFLAG =='TEXT'">
                            <div class="box">
                                <div class="box-body">

                                    <div class="image default_image" ng-if='hub.prof_img == ""'> 
                                    <span  ng-bind="hub.profile_initional"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}" target="_blank" ><h6>{{hub.bussinessname}}</h6></a>                                                                                   
                                        </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>

                                          <span class="post_time" ng-bind="hub.hub_post_time">9 min ago</span>
                                          <!-- <p class="post_normaltxt" ng-bind-html="getstringsplitvalcom(hub.hbcomment)  | limitTo: 200"></p> -->

                                        <p ng-if="lesstxtcomment == hub.hid || lesstxtcomment2" ng-attr-id="s_{{hub.hid}}"><span  ng-bind-html="getstringsplitvalcom(hub.hbcomment)  | limitTo: 200"></span>
                                                    <a ng-if="funcseemore(hub.hbcomment) > 200;"  href="javascript:void(0)" ng-click="showMoreFunction(hub.hid);" class="read_more">read more &gt;</a>
                                                </p>
                                                <p   ng-if="moretxtcomment == hub.hid || moretxtcomment2" ng-attr-id="l_{{hub.hid}}"><span  ng-bind-html="getstringsplitvalcom(hub.hbcomment)"></span>
                                                    <a ng-if="funcseemore(hub.hbcomment) > 200" href="javascript:void(0)" ng-click="showLessFunction(hub.hid);" class="read_more" >less &gt;</a>
                                                </p>  


                                    </div>


                                </div>

                                <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span>3</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>




                            </div>
                        </div>

                        <!-- SUBCOMMENT DIV START -->
                              <div class="col-md-11 sub_posts col-md-offset-1" ng-if="false">
                            <div class="box">
                                <div class="box-body">

                                    <div class="image default_image">                           
                                           <img src="<?php echo base_url(); ?>uploads/profile_image/default.png" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="#" ><h6>George Edwards</h6> <span class="subpost_time">9 min ago</span></a>                                                                             
                                        </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>

                                          
                                          <p class="post_normaltxt">Thats a really nice job cant wait to see it</p>

                                    </div>


                                </div>

                                <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">                                                
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">                                               
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>




                            </div>
                        </div>
                        <!-- SUBCOMMENT DIV END -->
                        
                        <!-- BING NEWS POST START -->

                        <div class="col-sm-12 url_post" ng-if="hub.POSTFLAG =='BINGNEWS'">
                            <div class="box">
                                <div class="box-body">

                                    <div class="image default_image">                           
                                           <img ng-src="http://www.google.com/s2/favicons?domain={{hub.url}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="{{hub.url}}" target="_blank" ><h6>{{hub.source}}</h6></a>                                                                                   
                                        </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>

                                          <span class="post_time">{{hub.date}}</span>
                                         <!--  <p class="post_normaltxt">[User text Imput] Look at this article from CNN.com about Fort...</p> -->

                                    </div>


                                </div>

                                <div class="post_innercont clearfix">
                                    <div class="post_img">
                                        <a ng-href="{{hub.url}}" target="_blank"><img src="{{hub.newsimg}}" alt=""></a>
                                    </div>
                                    <div class="post-rightcont clearfix">
                                      <span class="shared-link">
                                     <!--  <i class="fa  fa-link link"></i> -->
                                        <a ng-href="{{hub.url}}" target="_blank"><h5>{{hub.title}}</h5></a>
                                      </span> 

                                      <p>
                                          {{hub.desc  | limitTo: 100}} <a href="{{hub.url}}" target="_blank" class="read_more">read ></a>
                                      </p> 

                                    </div>

                                </div>

                                 <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span></span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                            </div>
                        </div>

                         <!-- NEWS POST END -->


                         <!-- URL POST START -->

                        <div class="col-sm-12 url_post" ng-if="hub.POSTFLAG =='HUBURL'">
                            <div class="box">
                                <div class="box-body">

                                   <div class="image default_image" ng-if='hub.prof_img == ""'> 
                                    <span  ng-bind="hub.profile_initional"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}" target="_blank" ><h6>{{hub.bussinessname}}</h6></a>                                                                                     
                                        </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>

                                          <span class="post_time">{{hub.hub_post_time}}</span>
                                         <!--  <p class="post_normaltxt">[User text Imput] Look at this article from CNN.com about Fort...</p> -->

                                    </div>


                                </div>

                                <div class="post_innercont clearfix">
                                    <div class="post_img">
                                        <a ng-href="{{hub.news_link}}" target="_blank"><img ng-src="{{hub.link}}" alt=""></a>
                                    </div>
                                    <div class="post-rightcont clearfix">
                                      <span class="shared-link">
                                      <i class="fa  fa-link link"></i>
                                        <a ng-href="{{hub.news_link}}" target="_blank"><h5>{{hub.hub_title}} <span><small>via</small> {{hub.url_provider}}</span></h5></a>
                                      </span> 

                                      <p>
                                          {{hub.content  | limitTo: 100}} <a href="{{hub.news_link}}" target="_blank" class="read_more">read ></a>
                                      </p> 

                                    </div>

                                </div>

                                 <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span></span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                            </div>
                        </div>

                         <!-- URL POST END -->
						 
						 
						 <!-- URL Hub POST START -->
                           
						   <div class="col-sm-12 Share_post" ng-if="hub.POSTFLAG =='SHAREHUBURL'">
                               <div class="box">
                                   <div class="box-body">

                                   <div class="image default_image" ng-if='hub.prof_img == ""'> 
                                    <span  ng-bind="hub.profile_initional"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}" ><h6>{{hub.bussinessname}} </h6></a><span class="sharedtxt">shared</span> <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}" class="share_othername"> {{hub.whose_share}}<span>’s</span></a> <span class="share_txtpost">post</span>
                                           </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>                                          
                                          <p class="sharetxt post_normaltxt" ng-bind-html="getstringsplitvalcom(hub.hbcomment)  | limitTo: 200">User text imput]of Pennsylvania, Vijay Kumar and his team develop a crazy Algorithm.</p>

                                    </div>


                                </div>

                                <div class="share_outer">
                                    <div class="clearfix share_innerblock">
                                        <div class="box-body">

                                    <div class="image default_image" ng-if='hub.whose_share_prof_img == ""'> 
                                    <span  ng-bind="hub.whose_share_logoname"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.whose_share_prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.whose_share_prof_img_path + hub.whose_share_prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}" target="_blank" ><h6>{{hub.whose_share}}</h6></a>                                                                                   
                                        </div>
                                         

                                          <span class="post_time">{{hub.hub_post_time}}</span>
                                          

                                    </div>


                                </div>
                                <div class="share_innercont">
                                    <div class="post_img">
                                       <a ng-href="{{hub.news_link}}" target="_blank"><img ng-src="{{hub.link}}" alt=""></a>
                                    </div>
                                    <div class="post-rightcont clearfix">
                                      <span class="shared-link">
                                      <i class="fa  fa-link link"></i>
                                        <h5>{{hub.hub_title}} <span><small>via</small> {{hub.share_url_provider}}</span></h5>
                                      </span> 

                                      <p>
                                          {{getstringsplitvalcont(hub.hbcomment)|limitTo: 200}}<a href="{{hub.news_link}}" target="_blank" class="read_more">read ></a>
                                      </p> 

                                    </div>

                                </div>

                                    </div>
                                </div>
                                <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span>3</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                               </div>
                           </div>
						   
						   
						   
                       
                         <!-- URL hub POST END -->













                         <!-- PUBLISHED POST START -->

                        <div class="col-sm-12 url_post" ng-if="hub.POSTFLAG =='PUBLISHHUB'">
                            <div class="box">
                                <div class="box-body">

                                   <div class="image default_image" ng-if='hub.prof_img == ""'> 
                                    <span  ng-bind="hub.profile_initional"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}" target="_blank" ><h6>{{hub.bussinessname}}</h6></a><span class="sharedtxt">published this</span>                                                                                   
                                        </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>

                                           <span class="post_time" ng-bind="hub.hub_post_time">9 min ago</span>                                       

                                    </div>


                                </div>

                                <div class="post_innercont clearfix">
                                    <div class="post_img" ng-if="hub.link!=null">
                                        <img ng-src="{{hub.base_url + hub.link}}" alt="">
                                    </div>
                                    <div class="post-rightcont clearfix">
                                      <span class="shared-link">
                                      <i class="fa  fa-file-text-o link"></i>
                                        <h5 class="published_title">{{hub.hub_title}}</h5>
                                      </span> 

                                      <p ng-bind-html="hub.content |limitTo: 200">
                                          A convoy of 1,500 vehicles carrying evacuees fleeing the Fort McMurray fire began rolling south early Friday under the watchful eye of Canadian police.Helicters hovered
                                        
                                      </p> 
                                      <a href="javascript:void(0)" ng-click="readmore(hub)">read more &gt;</a>

                                    </div>

                                </div>

                                 <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span>3</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                            </div>
                        </div>

                         <!-- PUBLISHED POST END -->
						 
						 <!-- Share Published Share start -->
						 <div class="col-sm-12 Share_post" ng-if="hub.POSTFLAG =='SHAREPUBLISHHUB'">
                               <div class="box">
                                   <div class="box-body">

                                    <div class="image default_image" ng-if='hub.prof_img == ""'> 
                                    <span  ng-bind="hub.profile_initional"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}" ><h6>{{hub.bussinessname}} </h6></a><span class="sharedtxt">shared</span> <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}" class="share_othername">{{hub.whose_share}}<span>’s</span></a> <span class="share_txtpost">post</span>
                                           </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>                                          
                                          <p class="sharetxt post_normaltxt" ng-bind-html="getstringsplitvalcom(hub.hbcomment)  | limitTo: 200">User text imput]of Pennsylvania, Vijay Kumar and his team develop a crazy Algorithm.</p>

                                    </div>


                                </div>

                                <div class="share_outer">
                                    <div class="clearfix share_innerblock">
                                    <div class="box-body normal_textshare">

                                   <div class="image default_image" ng-if='hub.whose_share_prof_img == ""'> 
                                    <span  ng-bind="hub.whose_share_logoname"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.whose_share_prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.whose_share_prof_img_path + hub.whose_share_prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}" target="_blank" ><h6>{{hub.whose_share}}</h6></a><span class="sharedtxt">published this</span>                                                                                   
                                        </div>

                                         
                                           <span class="post_time" ng-bind="hub.hub_post_time">9 min ago</span>                                       

                                    </div>


                                </div>
								
								
								 <div class="post_innercont clearfix">
                                    <div class="post_img" ng-if="hub.link!=null">
                                        <img ng-src="{{hub.base_url + hub.link}}" alt="">
                                    </div>
                                    <div class="post-rightcont clearfix">
                                      <span class="shared-link">
                                      <i class="fa  fa-file-text-o link"></i>
                                        <h5 class="published_title">{{hub.hub_title}}</h5>
                                      </span> 

                                      <p ng-bind-html="getstringsplitvalcont(hub.hbcomment)|limitTo: 200">
                                          A convoy of 1,500 vehicles carrying evacuees fleeing the Fort McMurray fire began rolling south early Friday under the watchful eye of Canadian police.Helicters hovered
                                        
                                      </p> 
                                      <a href="javascript:void(0)" ng-click="readmore(hub)">read more &gt;</a>

                                    </div>

                                </div>


                                 

                                    </div>
                                </div>
                                
								
								
								<div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span>3</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                               </div>
                           </div>

						 
						 
						 
						 
						 
						 <!-- Share Published Share end -->
						 
						 
						 
						 


                         <!-- IMAGE or VIDEO POST START  ng-show="hub.POSTFLAG =='IMAGE'"-->

                        <div class="col-sm-12 url_post" ng-if="hub.POSTFLAG =='IMAGE'">
                            <div class="box">
                                <div class="box-body">

                                   <div class="image default_image" ng-if='hub.prof_img == ""'> 
                                    <span  ng-bind="hub.profile_initional"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.prof_img}}" alt="">                                         
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header">
                                          <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}" target="_blank" ><h6>{{hub.bussinessname}}</h6></a>                                                                                                                                                                      
                                        </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>

                                          <span class="post_time" ng-bind="hub.hub_post_time"></span>                                        

                                    </div>


                                </div>

                                <div class="post_innercont">
                                    
                                    <div class="img_vid_container clearfix">
                                    <div ng-if="hub.ext != 'mp4'" >                                 
                                       <img ng-src="{{hub.base_url + hub.link}}" alt="">   
                                     </div> 
                                      <div class="video-ply" ng-if="hub.ext == 'mp4'">
                                                    <video controls ng-src="{{hub.base_url + hub.link}}" src=""></video>
                                                </div>
                                      <!-- <p class="imgvid_text">
                                          A convoy of 1,500 vehicles carrying evacuees fleeing the Fort McMurray fire began rolling south early Friday under the watchful eye of Canadian police.Helicters hovered</a>
                                      </p>  -->
                                      <p class="imgvid_text" ng-if="lesstxtcomment == hub.hid || lesstxtcomment2" ng-attr-id="s_{{hub.hid}}"><span  ng-bind-html="getstringsplitvalcom(hub.hbcomment)  | limitTo: 200"></span>
                                                    <a ng-if="funcseemore(hub.hbcomment) > 200;"  href="javascript:void(0)" ng-click="showMoreFunction(hub.hid);" class="read_more">read more &gt;</a>
                                                </p>
                                                <p class="imgvid_text"  ng-if="moretxtcomment == hub.hid || moretxtcomment2" ng-attr-id="l_{{hub.hid}}"><span  ng-bind-html="getstringsplitvalcom(hub.hbcomment)"></span>
                                                    <a ng-if="funcseemore(hub.hbcomment) > 200" href="javascript:void(0)" ng-click="showLessFunction(hub.hid);" class="read_more" >less &gt;</a>
                                                </p> 


                                    </div>

                                </div>

                                 <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span>3</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                            </div>
                        </div>

                         <!-- IMAGE or VIDEO POST END -->
						 
						 
						<!-- SHARE IMAGE or VIDEO POST START  ng-show="hub.POSTFLAG =='SHAREIMAGE/VIDEO'"-->
                        <div class="col-sm-12 Share_post" ng-if="hub.POSTFLAG =='SHAREIMAGE'">
                               <div class="box">
                                   <div class="box-body">
                                    
                                    <div class="image default_image" ng-if='hub.prof_img == ""'> 
                                    <span  ng-bind="hub.profile_initional"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}" ><h6>{{hub.bussinessname}} </h6></a><span class="sharedtxt">shared</span> <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}" class="share_othername">{{hub.whose_share}}<span>’s</span></a> <span class="share_txtpost">post</span>
                                           </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>                                          
                                          <p class="sharetxt post_normaltxt" ng-bind-html="getstringsplitvalcom(hub.hbcomment)  | limitTo: 200">User text imput]of Pennsylvania, Vijay Kumar and his team develop a crazy Algorithm.</p>

                                    </div>


                                </div>

                                <div class="share_outer">
                                    <div class="clearfix share_innerblock">
                                      <div class="box-body normal_textshare">

                                   <div class="image default_image" ng-if='hub.whose_share_prof_img == ""'> 
                                    <span  ng-bind="hub.whose_share_logoname"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.whose_share_prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.whose_share_prof_img_path + hub.whose_share_prof_img}}" alt="">                                         
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header">
                                          <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}" target="_blank" ><h6>{{hub.whose_share}}</h6></a>                                                                                                                                                                      
                                        </div>

                                        <span class="post_time" ng-bind="hub.hub_post_time"></span>                                        

                                    </div>


                                </div>

                                <div class="post_innercont">
                                    
                                    <div class="img_vid_container clearfix">
                                    <div ng-if="hub.ext != 'mp4'" >                                 
                                       <img ng-src="{{hub.base_url + hub.link}}" alt="">   
                                     </div> 
                                      <div class="video-ply" ng-if="hub.ext == 'mp4'">
                                                    <video controls ng-src="{{hub.base_url + hub.link}}" src=""></video>
                                                </div>
                                      <!-- <p class="imgvid_text">
                                          A convoy of 1,500 vehicles carrying evacuees fleeing the Fort McMurray fire began rolling south early Friday under the watchful eye of Canadian police.Helicters hovered</a>
                                      </p>  -->
                                      <p class="imgvid_text" ng-if="lesstxtcomment == hub.hid || lesstxtcomment2" ng-attr-id="s_{{hub.hid}}"><span  ng-bind-html="getstringsplitvalcont(hub.hbcomment)|limitTo: 200"></span>
                                                    <a ng-if="funcseemore(hub.hbcomment) > 200;"  href="javascript:void(0)" ng-click="showMoreFunction(hub.hid);" class="read_more">read more &gt;</a>
                                                </p>
                                                <p class="imgvid_text"  ng-if="moretxtcomment == hub.hid || moretxtcomment2" ng-attr-id="l_{{hub.hid}}"><span  ng-bind-html="getstringsplitvalcom(hub.hbcomment)"></span>
                                                    <a ng-if="funcseemore(hub.hbcomment) > 200" href="javascript:void(0)" ng-click="showLessFunction(hub.hid);" class="read_more" >less &gt;</a>
                                                </p> 


                                    </div>

                                </div>
  

                                    </div>
                                </div>
                                <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span>3</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                               </div>
                           </div>

                       
                        <!-- SHARE IMAGE or VIDEO POST END  ng-show="hub.POSTFLAG =='SHAREIMAGE/VIDEO'"-->

						 
						 
						 
						 
						 
						 
						 
						 
						 






                         <!-- ALL SHARE COMMENT AND LIKE DESIGN START-->
                           <div class="col-sm-12 Share_post" ng-if="hub.POSTFLAG =='SHARETEXT'">
                               <div class="box">
                                   <div class="box-body">

                                    <div class="image default_image" ng-if='hub.prof_img == ""'> 
                                    <span  ng-bind="hub.profile_initional"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}" ><h6>{{hub.bussinessname}} </h6></a><span class="sharedtxt">shared</span> <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}" class="share_othername">{{hub.whose_share}}<span>’s</span></a> <span class="share_txtpost">post</span>
                                           </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>                                          
                                          <p class="sharetxt post_normaltxt" ng-bind-html="getstringsplitvalcom(hub.hbcomment)  | limitTo: 200">User text imput]of Pennsylvania, Vijay Kumar and his team develop a crazy Algorithm.</p>

                                    </div>


                                </div>

                                <div class="share_outer">
                                    <div class="clearfix share_innerblock">
                                        <div class="box-body normal_textshare">

                                    <div class="image default_image" ng-if='hub.whose_share_prof_img == ""'> 
                                    <span  ng-bind="hub.whose_share_logoname"></span>                          
                                                                                   
                                    </div>
                                    <div class="image default_image" ng-if='hub.whose_share_prof_img !=""'> 
                                                             
                                    <img  ng-src="{{hub.whose_share_prof_img_path + hub.whose_share_prof_img}}" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}" target="_blank" ><h6>{{hub.whose_share}}</h6></a>                                                                                   
                                        </div>

                                         

                                          <span class="post_time" ng-bind="hub.hub_post_time">9 min ago</span>
                                          <!-- <p class="post_normaltxt" ng-bind-html="getstringsplitvalcom(hub.hbcomment)  | limitTo: 200"></p> -->

                                        <p ng-if="lesstxtcomment == hub.hid || lesstxtcomment2" ng-attr-id="s_{{hub.hid}}"><span  ng-bind-html="getstringsplitvalcont(hub.hbcomment)|limitTo: 200"></span>
                                                    <a ng-if="funcseemore(hub.hbcomment) > 200;"  href="javascript:void(0)" ng-click="showMoreFunction(hub.hid);" class="read_more">read more &gt;</a>
                                                </p>
                                                <p   ng-if="moretxtcomment == hub.hid || moretxtcomment2" ng-attr-id="l_{{hub.hid}}"><span  ng-bind-html="getstringsplitvalcom(hub.hbcomment)"></span>
                                                    <a ng-if="funcseemore(hub.hbcomment) > 200" href="javascript:void(0)" ng-click="showLessFunction(hub.hid);" class="read_more" >less &gt;</a>
                                                </p>  


                                    </div>


                                </div>
                                        

                                    </div>
                                </div>
                                <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span>3</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                               </div>
                           </div>

                          <!-- ALL SHARE COMMENT AND LIKE DESIGN  END-->


                          <!-- ALL SHARE COMMENT AND LIKE DESIGN START-->
                           <div class="col-sm-12 Share_post" ng-if="false">
                               <div class="box">
                                   <div class="box-body">

                                    <div class="image default_image">                           
                                           <img src="<?php echo base_url(); ?>uploads/profile_image/member-test.png" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="#" ><h6>Simon Dylan </h6></a><span class="sharedtxt">shared</span> <a href="#" class="share_othername"> Bob Pinta<span>’s</span></a> <span class="share_txtpost">post</span>
                                           </div>

                                         <div class="btn-group confidentiality dropdwn">
                                           <button class="btn dlt_btn btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="reportPost(hub)" href="javascript:void(0)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid" class="ng-hide"><a ng-click="deletePost(hub, $event)" href="javascript:void(0)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a ng-click="blockUser(hub)" href="javascript:void(0)">Block User</a></li>
                                                </ul>
                                          </div>                                          
                                          <p class="sharetxt post_normaltxt">User text imput]of Pennsylvania, Vijay Kumar and his team develop a crazy Algorithm.</p>

                                    </div>


                                </div>

                                <div class="share_outer">
                                    <div class="clearfix share_innerblock">
                                        <div class="box-body">

                                    <div class="image default_image">                           
                                           <img src="<?php echo base_url(); ?>uploads/profile_image/default.png" alt="">                                         
                                    </div>

                                    <div class="latest_content">
                                        <div class="latest_header">
                                           <a href="#" ><h6>Bob Pinta</h6></a>                                                                                   
                                        </div>
                                         

                                          <span class="post_time">9 min ago</span>
                                          <p class="post_normaltxt">[User text Imput] Look at this article from CNN.com about...</p>

                                    </div>


                                </div>
                                <div class="share_innercont">
                                    <div class="post_img">
                                        <img src="<?php echo base_url(); ?>uploads/profile_image/defaulturl-img.jpg" alt="">
                                    </div>
                                    <div class="post-rightcont clearfix">
                                      <span class="shared-link">
                                      <i class="fa  fa-link link"></i>
                                        <h5>Fort McMurray fire: Escape convoy starts heading south <span><small>via</small> CNN.com</span></h5>
                                      </span> 

                                      <p>
                                          A convoy of 1,500 vehicles carrying evacuees fleeing the Fort McMurray fire began rolling south early Friday under the watchful eye of Canadian police.Helicters hovered <a href="#" class="read_more">read ></a>
                                      </p> 

                                    </div>

                                </div>

                                    </div>
                                </div>
                                <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a ng-click="shareHub(hub)" href="javascript:void(0)"> Share</a></li>
                                                <li><a ng-click="open_comment_Animation(hub)" href="javascript:void(0)">Comments</a></li>
                                                <li><a ng-click="feedback(hub)" ng-class="{'like': hub.like != 0}" href="javascript:void(0)">Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span>3</span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a ng-click="commentsAnimation(hub)" href="javascript:void(0)"><span>12</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span>23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                </div>

                               </div>
                           </div>

                          <!-- ALL SHARE COMMENT AND LIKE DESIGN  END-->




                        </div>
                   <!-- Normal All Type of post End-->
                </div>                 
            </div>
            
            
            <div class="col-md-6 col-sm-6 col-sm-12 fixed-div" id="id-2"  >
                <div class="tab_s mCustomScrollbar"> 
                   <!-- <img class="tab-img" ng-show="show_default_community"  src="<?php echo base_url(); ?>assets_phase2/images/tab-img.jpg" alt="">-->


                    <!-- <div class="article_section">
                        <div class="article_toppart">
                            <h2>Lorem ipsum doler sit amet dummy text</h2>
                            <span class="article_subheading">By Textwriter</span>                     
                        </div>

                        <div class="article_body box box-body">
                            <div class="article_icon"><i class="fa fa-user"></i></div>
                            <span class="article_quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisi urna, ullamcorper ac ligula vitae"</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisi urna, ullamcorper ac ligula vitae, sollicitudin rhoncus eros? Pellentesque a accumsan leo. Nulla facilisi. Nullam odio nisi, feugiat ac magna eget, accumsan dictum urna. Nunc at leo dolor. Etiam eleifend, lacus eu feugiat venenatis, velit nisl malesuada sapien, a efficitur nulla eros ac mauris? Duis non leo vitae est tristique sodales ut sed metus. Nulla facilisi. Curabitur blandit, lacus id scelerisque molestie, purus magna ultricies tortor, in consequat tellus velit non sapien. Praesent justo metus; venenatis et ligula in, viverra aliquet purus. Cras ullamcorper purus in purus rutrum porttitor. Vivamus quis dui ex. Nullam congue arcu ut nisi euismod porttitor. Integer turpis felis, elementum facilisis elit sodales, interdum vestibulum neque. Integer eu nulla tellus.</p>
                        </div>               

                    </div>  -->

                    <div class="details_tab">
                        <div id="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1 clearfix">
                                <li ng-click="latest_talk_toggle_animation_back(talk)"><i class="fa  fa-refresh"></i><br>Latest Talks</li>
                                <li ng-click="latest_talk_toggle_animation_back(talk)"><i class="fa  fa-cloud-download"></i><br>My Talks</li>
                                <li ng-click="latest_talk_toggle_animation_back(talk)"><i class="fa  fa-th"></i><br>Top Talks</li>
                                <li ng-click="latest_talk_toggle_animation_back(talk)"><i class="fa  fa-users"></i><br>Start a Talk</li>
                                <li ng-click="latest_talk_toggle_animation_back(talk)"><i class="fa  fa-list"></i><br>More</li>
                            </ul>
                                

                            <div class="resp-tabs-container hor_1" >
                                <div>

                                    <div ng-repeat="talk in fetch_Latest_Talk" >

                                        <div class="box latest_talk" ng-click="latest_talk_toggle_animation(talk)" ng-show="talk.show_latest_talk_section" >

                                            <div class="box-body" >

                                                <div class="image default_image">
                                                    <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                    <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">

                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6 ng-bind="talk.member_name"></h6>                                           

                                                        <a href="#" class="user_likes" ><i class="fa fa-user"></i> {{talk.total_comment}}</a>                                           

                                                    </div>
                                                    <p ><strong ng-bind="talk.title"></strong></p>
                                                    <p ng-bind="talk.content"></p> 
                                                </div>


                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                   <!--<li ng-disabled="talk.mid == user_mid"><a href="javascript:void(0)" ng-if="participate_stattus == false" id="participate_button" class="toggle" ng-click="open_comment_Animation_latestTalk(talk)">Participate</a></li>-->
                                                   <li ng-show="talk.participate_stattus == false"><a href="javascript:void(0)"  id="participate_button" class="toggle" >Participate</a></li>
                                                   <li ng-show="talk.participate_stattus == true"><a href="javascript:void(0)"  id="participate_btn" class="toggle" style="color:blue;" >Participated</a></li>
                                                   <li><a href="javascript:void(0)" ng-class="{'com_like': talk.com_like != 0 }" ng-click="feedback_latest_talk(talk)"><i ng-class="{'fa fa-heart' :talk.com_like != 0, 'fa fa-heart-o' :talk.com_like == 0}" class="fa"></i>Like </a></li>
                                                   <li><a href="javascript:void(0)"  ng-click="open_recommend_Animation_latestTalk(talk)">Recommend</a></li>
                                                </ul>

                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)" ng-click="toggleComment(talk)" ><span ng-bind=""> </span><i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)" > <span ng-bind="talk.total_like"></span> <i class="fa  fa-heart-o"></i> </a></li>
                                                    <li>
                                                        <div class="btn-group gear-btn">
                                                            <button type="button" class="dropdown-toggle settings_icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-gear"></i>                              
                                                            </button>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li ng-show="talk.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteParticipateTalk(talk, $event)">Delete</a></li>
                                                                <li><a href="#">Hide</a></li>

                                                            </ul>
                                                        </div>
                                                    </li>

                                                </ul>

                                            </div> 


                                        </div> 
                                        

                                        <!-- comment on latest talk start -->
                                         <div ng-hide="talk.show_post_button">  
                                            <div class="box latest_talk">
                                                <form > 
                                                    <div class="box-body">
                                                        <div class="image default_image">
                                                            <span ng-show="talk.currentsessionlogourl == 'NULL'" ng-bind="talk.currentsessioninitial"></span>
                                                            <img ng-show="talk.currentsessionlogourl != 'NULL'" ng-src="{{talk.currentsessionlogourl}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="latest_content comment_content">
                                                            <div class="latest_header m-b10">
                                                                <h6><span>Write an update</span></h6>                                                    
                                                            </div>
                                                            <textarea class="form-control latest_commentarea" ng-model="talk.latest_talkParticipation" placeholder="Say Something..."></textarea>                     
                                                        </div>
                                                    </div>
                                                    <div class="post_footer clearfix">
                                                        <span class="share_list" style="color:#fff" ng-bind="show_selected">Hello world</span>

                                                        <div class="post_footebtn pull-right"  ng-show="talk.talk_access==0">
                                                            <div class="btn-group post_footerdrpbtn" >
                                                                <!--<button aria-expanded="false" type="button" ng-repeat="pub_private_post_button in fetch_Latest_Talk_show_post_button" ng-bind="pub_private_post_button.mid" ng-bind="user_mid" ng-show="pub_private_post_button.mid==user_mid" class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">Post</button>-->
                                                                <button aria-expanded="false" type="button" ng-repeat="access_confirmeduser in talk.access_confirmedusers" ng-show="access_confirmeduser.mid==user_mid" class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">post</button>
                                                            </div>
                                                        </div>
                                                        <div class="post_footebtn pull-right" ng-show="talk.talk_access==1">
                                                            <div class="btn-group post_footerdrpbtn">
                                                                
                                                                <button aria-expanded="false" type="button"  class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">Post</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-primary btn_goback" style="float:right;" ng-click="latest_talk_toggle_animation_back(talk)" ng-show="talk.button_section">Go Back</button> 
                                        <div class="clearfix"></div>
                                        
                                        <div ng-hide="talk.open_comment_box">

                                            <div class="comment_subcmnt " id="comment_subcmnt">
                                                <div ng-repeat="comment in talk.comments">
                                                    <div class="latestcomment_part clearfix" >

                                                        <div class="image default_image">
                                                            <span ng-show="comment.commentlogomembername_url == 'NULL'" ng-bind="comment.commentlogomembername"></span>
                                                            <img ng-show="comment.commentlogomembername_url != 'NULL'" ng-src="{{comment.commentlogomembername_url}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="">
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="comment.bussiness_name"><span>talks</span></h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i>{{comment.comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="comment.comment"></p>                     
                                                                </div>
                                                            </div>
                                                            <div class="box-footer clearfix">
                                                                <ul class="latest_footerlink pull-left">
                                                                    <li><a href="javascript:void(0)" ng-click="open_animation_sub_comment(comment)">Comment</a></li>
                                                                    <li><a href="javascript:void(0)" ng-class="{'comment_like_status': comment.comment_like_status != 0 }" ng-click="feedback_latest_talk_comment_subComment(comment)"><i ng-class="{'fa fa-heart' :comment.comment_like_status != 0, 'fa fa-heart-o' :comment.comment_like_status == 0}" class="fa fa-heart-o">Like </i></a></li>                                                
                                                                </ul>

                                                                <ul class="latest_footerlink_right pull-right">
                                                                    <li><a href="javascript:void(0)" ng-click="comment_sub_comment(comment)" >{{comment.subcomment_count}}<i class="fa fa-comments"></i></a></li>
                                                                    <li><a href="javascript:void(0)" >{{comment.comment_like_count}} <i class="fa  fa-heart-o"></i> </a></li>
                                                                    <li>
                                                                        <div class="btn-group gear-btn">
                                                                            <button type="button" class="settings_icon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i class="fa fa-gear"></i>                              
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li >
                                                                                    <a href="javascript:void(0)" ng-click="deleteCommentLatestTalk(talk, comment, $event)">Delete</a>
                                                                                </li>
                                                                                <li><a href="#">Hide</a></li>

                                                                            </ul>
                                                                        </div>                                                  
                                                                    </li>
                                                                </ul>
                                                                <div class="clearfix"></div>

                                                                <div class="comment_box box-footer tab-footer" ng-hide="comment.show_sub_post_button">
                                                                    <div class="comment_default"> 
                                                                        <span ng-show="talk.currentsessionlogourl == 'NULL'" ng-bind="talk.currentsessioninitial"></span>
                                                                        <img ng-show="talk.currentsessionlogourl != 'NULL'" ng-src="{{talk.currentsessionlogourl}}" alt="" class="latest_talkimg img-circle">
                                                                        
                                                                    </div>

<!--<img class="comment_img" ng-src="{{hub.prof_img != '' && hub.prof_img|| 'http://arlians.com/assets/images/profile-img.png'}}" />-->
                                                                    <form>  
                                                                        <input type="text" ng-model="comment.latestTalk_sub_comment" class="form-control"  placeholder="Write your comment">
                                                                        <button class="btn btn_publish" ng-click="postLatestTalkSubComments(comment)">Post</button>
                                                                    </form>
                                                                </div>
                                                            </div> 
                                                        </div>



                                                    </div>


                                                    <div ng-repeat="subComment in comment.subcomment" class="latestcomment_part clearfix col-sm-offset-1" ng-hide="comment.show_sub_comment">



                                                        <div class="image default_image">
                                                            <span ng-show="subComment.subcommentlogomembername_url == 'NULL'" ng-bind="subComment.subcommentlogomembername"></span>
                                                            <img ng-show="subComment.subcommentlogomembername_url != 'NULL'" ng-src="{{subComment.subcommentlogomembername_url}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="" >
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="subComment.bussiness_name"> <span>commented</span> </h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i> {{subComment.sub_comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="subComment.sub_comment"></p>                     
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>



                                            </div>   
                                        </div>   

                                                
                                            
                                        <!-- </a>comment on latest talk End -->

                                    </div>
                                        
                                </div> 
                            </div> 


                            <div class="resp-tabs-container hor_1">
                                <div>
                                   
                                    <div ng-repeat="talk in myTalk" >

                                        <div class="box latest_talk" ng-click="my_talk_toggle_animation(talk)" ng-show="talk.show_my_talk_section" >

                                            <div class="box-body" >

                                                <div class="image default_image">
                                                    <span ng-bind="talk.logomembername"></span>
                                                    <!--<img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">-->

                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6 ng-bind="talk.member_name"></h6>                                           

                                                        <a href="#" class="user_likes" > <i class="fa fa-user"></i>  {{talk.total_comment}}</a>                                         

                                                    </div>
                                                    <p ><strong ng-bind="talk.title"></strong></p>
                                                    <p ng-bind="talk.content"></p> 
                                                </div>


                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                   <!--<li ng-disabled="talk.mid == user_mid"><a href="javascript:void(0)" ng-if="participate_stattus == false" id="participate_button" class="toggle" ng-click="open_comment_Animation_latestTalk(talk)">Participate</a></li>-->
                                                   <li ng-show="talk.participate_stattus == false"><a href="javascript:void(0)"  id="participate_button" class="toggle" >Participate</a></li>
                                                   <li ng-show="talk.participate_stattus == true"><a href="javascript:void(0)"  id="participate_btn" class="toggle" style="color:blue;" >Participated</a></li>
                                                   <li><a href="javascript:void(0)" ng-class="{'com_like': talk.com_like != 0 }" ng-click="feedback_latest_talk(talk)"><i ng-class="{'fa fa-heart' :talk.com_like != 0, 'fa fa-heart-o' :talk.com_like == 0}" class="fa"></i>Like </a></li>
                                                   <li><a href="javascript:void(0)"  ng-click="open_recommend_Animation_latestTalk(talk)">Recommend</a></li>
                                                </ul>

                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)" ng-click="toggleComment(talk)" > <span ng-bind=""></span><i class="fa fa-comments"></i> </a></li>
                                                    <li><a href="javascript:void(0)" > <span ng-bind="talk.total_like"></span> <i class="fa  fa-heart-o"></i> </a></li>
                                                    <li>
                                                        <div class="btn-group gear-btn">
                                                            <button type="button" class="dropdown-toggle settings_icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-gear"></i>                              
                                                            </button>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li ng-show="talk.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteParticipateTalk(talk, $event)">Delete</a></li>
                                                                <li><a href="#">Hide</a></li>

                                                            </ul>
                                                        </div>
                                                    </li>

                                                </ul>

                                            </div> 


                                        </div> 
                                        

                                        <!-- comment on latest talk start -->
                                         <div ng-hide="talk.show_post_button">  
                                            <div class="box latest_talk">
                                                <form > 
                                                    <div class="box-body">
                                                        <div class="image default_image">
                                                            <span ng-show="talk.currentsessionlogourl == 'NULL'" ng-bind="talk.currentsessioninitial"></span>
                                                            <img ng-show="talk.currentsessionlogourl != 'NULL'" ng-src="{{talk.currentsessionlogourl}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="latest_content comment_content">
                                                            <div class="latest_header m-b10">
                                                                <h6><span>Write an update</span></h6>                                                    
                                                            </div>
                                                            <textarea class="form-control latest_commentarea" ng-model="talk.my_talkParticipation" placeholder="Say Something..."></textarea>                     
                                                        </div>
                                                    </div>
                                                    <div class="post_footer clearfix">
                                                        <span class="share_list" style="color:#fff" ng-bind="show_selected">Hello world</span>

                                                        <div class="post_footebtn pull-right"  ng-show="talk.talk_access==0">
                                                            <div class="btn-group post_footerdrpbtn" >
                                                                <!--<button aria-expanded="false" type="button" ng-repeat="pub_private_post_button in fetch_Latest_Talk_show_post_button" ng-bind="pub_private_post_button.mid" ng-bind="user_mid" ng-show="pub_private_post_button.mid==user_mid" class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">Post</button>-->
                                                                <button aria-expanded="false" type="button" ng-repeat="access_confirmeduser in talk.access_confirmedusers" ng-show="access_confirmeduser.mid==user_mid" class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_my_Talk(talk)">post</button>
                                                            </div>
                                                        </div>
                                                        <div class="post_footebtn pull-right" ng-show="talk.talk_access==1">
                                                            <div class="btn-group post_footerdrpbtn">
                                                                
                                                                <button aria-expanded="false" type="button"  class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_my_Talk(talk)">Post</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <button type="button" class="btn btn-primary" style="float:right;" ng-click="my_talk_toggle_animation_back(talk)" ng-show="talk.button_section">Go Back</button>    
                                        
                                        <div ng-hide="talk.open_comment_box">

                                            <div class="comment_subcmnt " id="comment_subcmnt">
                                                <div ng-repeat="comment in talk.comments">
                                                    <div class="latestcomment_part clearfix" >

                                                        <div class="image default_image">
                                                            <span ng-show="comment.commentlogomembername_url == 'NULL'" ng-bind="comment.commentlogomembername"></span>
                                                            <img ng-show="comment.commentlogomembername_url != 'NULL'" ng-src="{{comment.commentlogomembername_url}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="">
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="comment.bussiness_name"><span>talks</span></h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i>{{comment.comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="comment.comment"></p>                     
                                                                </div>
                                                            </div>
                                                            <div class="box-footer clearfix">
                                                                <ul class="latest_footerlink pull-left">
                                                                    <li><a href="javascript:void(0)" ng-click="open_animation_sub_comment(comment)">Comment</a></li>
                                                                    <li><a href="javascript:void(0)" ng-class="{'comment_like_status': comment.comment_like_status != 0 }" ng-click="feedback_latest_talk_comment_subComment(comment)"><i ng-class="{'fa fa-heart' :comment.comment_like_status != 0, 'fa fa-heart-o' :comment.comment_like_status == 0}" class="fa fa-heart-o">Like </i></a></li>                                                
                                                                </ul>

                                                                <ul class="latest_footerlink_right pull-right">
                                                                    <li><a href="javascript:void(0)" ng-click="comment_sub_comment(comment)" >{{comment.subcomment_count}}<i class="fa fa-comments"></i></a></li>
                                                                    <li><a href="javascript:void(0)" >{{comment.comment_like_count}} <i class="fa  fa-heart-o"></i> </a></li>
                                                                    <li>
                                                                        <div class="btn-group gear-btn">
                                                                            <button type="button" class="settings_icon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i class="fa fa-gear"></i>                              
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li >
                                                                                    <a href="javascript:void(0)" ng-click="deleteCommentLatestTalk(talk, comment, $event)">Delete</a>
                                                                                </li>
                                                                                <li><a href="#">Hide</a></li>

                                                                            </ul>
                                                                        </div>                                                  
                                                                    </li>
                                                                </ul>
                                                                <div class="clearfix"></div>

                                                                <div class="comment_box box-footer tab-footer" ng-hide="comment.show_sub_post_button">
                                                                    <div class="comment_default"> 
                                                                        <span ng-show="talk.currentsessionlogourl == 'NULL'" ng-bind="talk.currentsessioninitial"></span>
                                                                        <img ng-show="talk.currentsessionlogourl != 'NULL'" ng-src="{{talk.currentsessionlogourl}}" alt="" class="latest_talkimg img-circle">
                                                                        
                                                                    </div>

<!--<img class="comment_img" ng-src="{{hub.prof_img != '' && hub.prof_img|| 'http://arlians.com/assets/images/profile-img.png'}}" />-->
                                                                    <form>  
                                                                        <input type="text" ng-model="comment.latestTalk_sub_comment" class="form-control"  placeholder="Write your comment">
                                                                        <button class="btn btn_publish" ng-click="postLatestTalkSubComments(comment)">Post</button>
                                                                    </form>
                                                                </div>
                                                            </div> 
                                                        </div>



                                                    </div>


                                                    <div ng-repeat="subComment in comment.subcomment" class="latestcomment_part clearfix col-sm-offset-1" ng-hide="comment.show_sub_comment">



                                                        <div class="image default_image">
                                                            <span ng-show="subComment.subcommentlogomembername_url == 'NULL'" ng-bind="subComment.subcommentlogomembername"></span>
                                                            <img ng-show="subComment.subcommentlogomembername_url != 'NULL'" ng-src="{{subComment.subcommentlogomembername_url}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="" >
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="subComment.bussiness_name"> <span>commented</span> </h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i> {{subComment.sub_comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="subComment.sub_comment"></p>                     
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>



                                            </div>   
                                        </div>   

                                                
                                            
                                        <!-- </a>comment on latest talk End -->

                                    </div>
                                   <!-- mytalk end --> 
                                </div>
                            </div>
                                
                            <!-- tab 3 -->
                             <div class="resp-tabs-container hor_1">
                                <div>

                                    <div ng-repeat="talk in topTalk" >
                                        
                                        <div class="box latest_talk" ng-click="top_talk_toggle_animation(talk)" ng-show="talk.show_top_talk_section" >
                                             
                                            <div class="box-body" >

                                                <div class="image default_image">
                                                    <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                    <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">

                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6 ng-bind="talk.member_name"></h6>                                           

                                                      <a href="#" class="user_likes" ><i class="fa fa-user"></i> {{talk.total_comment}}</a>                                           

                                                    </div>
                                                    <p ><strong ng-bind="talk.title"></strong></p>
                                                    <p ng-bind="talk.content"></p> 
                                                </div>


                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li ng-show="talk.participate_stattus == false"><a href="javascript:void(0)"  id="participate_button" class="toggle" ng-click="open_comment_Animation_latestTalk(talk)">Participate</a></li>
                                                   <li ng-show="talk.participate_stattus == true"><a href="javascript:void(0)"   id="participate_btn" class="toggle" style="color:blue;" ng-click="open_comment_Animation_latestTalk(talk)">Participated</a></li>
                                                    <li><a href="javascript:void(0)" ng-class="{'com_like': talk.com_like != 0 }" ng-click="feedback_latest_talk(talk)"><i ng-class="{'fa fa-heart' :talk.com_like != 0, 'fa fa-heart-o' :talk.com_like == 0}" class="fa"></i>Like </a></li>
                                                    <li><a href="javascript:void(0)"  ng-click="open_recommend_Animation_latestTalk(talk)">Recommend</a></li>
                                                </ul>

                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)" ng-click="toggleComment(talk)" ><span ng-bind=""></span><i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)" ><span ng-bind="talk.total_like"></span> <i class="fa  fa-heart-o"></i> </a></li>
                                                    <li>
                                                        <div class="btn-group gear-btn">
                                                            <button type="button" class="dropdown-toggle settings_icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-gear"></i>                              
                                                            </button>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li ng-show="talk.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteParticipateTalk(talk, $event)">Delete</a></li>
                                                                <li><a href="javascript:void(0)"  >Hide</a></li>

                                                            </ul>
                                                        </div>
                                                    </li>

                                                </ul>

                                            </div> 
                                         

                                        </div> 
                                       

                                        <div ng-hide="talk.show_post_button">   
                                            <div class="box latest_talk">
                                                <form > 
                                                    <div class="box-body">
                                                        <div class="image default_image">
                                                           <span ng-show="talk.currentsessionlogourl == 'NULL'" ng-bind="talk.currentsessioninitial"></span>
                                                            <img ng-show="talk.currentsessionlogourl != 'NULL'" ng-src="{{talk.currentsessionlogourl}}" alt="" class="latest_talkimg img-circle">
                                                                                                             </div>

                                                        <div class="latest_content comment_content">
                                                            <div class="latest_header m-b10">
                                                                <h6><span>Write an update</span></h6>                                                    
                                                            </div>
                                                            <textarea class="form-control latest_commentarea" ng-model="talk.latest_talkParticipation" placeholder="Say Something..."></textarea>                     
                                                        </div>
                                                    </div>
                                                    <div class="post_footer clearfix">
                                                        <span class="share_list" style="color:#fff" ng-bind="show_selected">Hello world</span>

                                                        <div class="post_footebtn pull-right"  ng-show="talk.talk_access==0">
                                                            <div class="btn-group post_footerdrpbtn" style="background:blue;"  data-toggle="tooltip" data-placement="top" title="Remove from my network" >
                                                                <!--<button aria-expanded="false" type="button" ng-repeat="pub_private_post_button in fetch_Latest_Talk_show_post_button" ng-bind="pub_private_post_button.mid" ng-bind="user_mid" ng-show="pub_private_post_button.mid==user_mid" class="btn  btn-sm dropdown-toggle"  class="connected" data-toggle="tooltip" data-placement="top" title="Remove from my network"  ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">Post</button>-->
                                                                <button aria-expanded="false" type="button" ng-repeat="access_confirmeduser in talk.access_confirmedusers" ng-show="access_confirmeduser.mid==user_mid" class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">post</button>
                                                                <!--<button aria-expanded="false" type="button" ng-repeat="access_confirmeduser in talk.access_confirmedusers" ng-bind="access_confirmeduser.mid" ng-show="access_confirmeduser.mid!=user_mid"  ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">Remove from my network</button>-->
                                                            </div>
                                                        </div>
                                                        <div class="post_footebtn pull-right" ng-show="talk.talk_access==1">
                                                            <div class="btn-group post_footerdrpbtn">
                                                                
                                                                <button aria-expanded="false" type="button"  class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">Post</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                         <button type="button" class="btn btn-primary" style="float:right;" ng-click="top_talk_toggle_animation_back(talk)" ng-show="talk.button_section">Go Back</button>  
                                        <div ng-hide="talk.open_comment_box">

                                            <div class="comment_subcmnt " id="comment_subcmnt">
                                                <div ng-repeat="comment in talk.comments">
                                                    <div class="latestcomment_part clearfix" >

                                                        <div class="image default_image">
                                                             <span ng-show="comment.commentlogomembername_url == 'NULL'" ng-bind="comment.commentlogomembername"></span>
                                                            <img ng-show="comment.commentlogomembername_url != 'NULL'" ng-src="{{comment.commentlogomembername_url}}" alt="" class="latest_talkimg img-circle">

                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="">
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="comment.bussiness_name"><span>talks</span></h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i>{{comment.comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="comment.comment"></p>                     
                                                                </div>
                                                            </div>
                                                            <div class="box-footer clearfix">
                                                                <ul class="latest_footerlink pull-left">
                                                                    <li><a href="javascript:void(0)" ng-click="open_animation_sub_comment(comment)">Comment</a></li>
                                                                    <li><a href="javascript:void(0)" ng-class="{'comment_like_status': comment.comment_like_status != 0 }" ng-click="feedback_latest_talk_comment_subComment(comment)"><i ng-class="{'fa fa-heart' :comment.comment_like_status != 0, 'fa fa-heart-o' :comment.comment_like_status == 0}" class="fa fa-heart-o">Like </i></a></li>                                                
                                                                </ul>

                                                                <ul class="latest_footerlink_right pull-right">
                                                                    <li><a href="javascript:void(0)" ng-click="comment_sub_comment(comment)" >{{comment.subcomment_count}}<i class="fa fa-comments"></i></a></li>
                                                                    <li><a href="javascript:void(0)" >{{comment.comment_like_count}} <i class="fa  fa-heart-o"></i> </a></li>
                                                                    <li>
                                                                        <div class="btn-group gear-btn">
                                                                            <button type="button" class="settings_icon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i class="fa fa-gear"></i>                              
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li >
                                                                                    <a href="javascript:void(0)" ng-click="deleteCommentLatestTalk(talk, comment, $event)">Delete</a>
                                                                                </li>
                                                                                <li><a href="#">Hide</a></li>

                                                                            </ul>
                                                                        </div>                                                  
                                                                    </li>
                                                                </ul>
                                                                <div class="clearfix"></div>

                                                                <div class="comment_box box-footer tab-footer" ng-hide="comment.show_sub_post_button">
                                                                    <div class="comment_default"> 
                                                                        <span ng-show="talk.currentsessionlogourl == 'NULL'" ng-bind="talk.currentsessioninitial"></span>
                                                                        <img ng-show="talk.currentsessionlogourl != 'NULL'" ng-src="{{talk.currentsessionlogourl}}" alt="" class="latest_talkimg img-circle">
                                                                    </div>


                                                                    <form>  
                                                                        <input type="text" ng-model="comment.latestTalk_sub_comment" class="form-control"  placeholder="Write your comment">
                                                                        <button class="btn btn_publish" ng-click="postLatestTalkSubComments(comment)">Post</button>
                                                                    </form>
                                                                </div>
                                                            </div> 
                                                        </div>



                                                    </div>


                                                    <div ng-repeat="subComment in comment.subcomment" class="latestcomment_part clearfix col-sm-offset-1" ng-hide="comment.show_sub_comment">



                                                        <div class="image default_image">
                                                            <span ng-show="subComment.subcommentlogomembername_url == 'NULL'" ng-bind="subComment.subcommentlogomembername"></span>
                                                            <img ng-show="subComment.subcommentlogomembername_url != 'NULL'" ng-src="{{subComment.subcommentlogomembername_url}}" alt="" class="latest_talkimg img-circle">

                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="" >
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="subComment.bussiness_name"> <span>commented</span> </h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i> {{subComment.sub_comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="subComment.sub_comment"></p>                     
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>



                                            </div>   
                                        </div>   



                                      

                                    </div>

                                </div> 
                            </div> 







                            <!-- Tab 4 -->
                            <form novalidate>
                                <div class="resp-tabs-container hor_1">
                                    <div> 

                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image default_image">
                                                    <span ng-show="front_img_name == ''" ng-bind="user_initial"></span>
                                                    <img class="latest_talkimg" alt="" ng-show="front_img_name != ''" ng-src="{{path + front_img_name}}" />
                                                </div> 
                                                <!--                                            <div class="image">
                                                    <img class="latest_talkimg img-circle" alt="" src="images/user2-160x160.jpg">
                                                    <a href="#" class="online-status"><i class="fa fa-circle text-success"></i></a>
                                                </div>-->
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6> <span>verified talker</span></h6>                     
                                                    </div>
                                                    <div class="form-group">                        
                                                        <div class="">
                                                            <div class="input-group" id="title_section" style="border:1px solid white">
                                                                <span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
                                                                <input type="text" class="form-control" id="titleBlank" name="title" placeholder="Talk title" id="inputGroupSuccess2" ng-model="title" aria-describedby="inputGroupSuccess2Status">
                                                            </div>                          
                                                        </div>
                                                    </div>

                                                    <div class="form-group" id="content_section" style="border:1px solid white">                        
                                                        <div class="">
                                                            <div class="input-group" >
                                                                <textarea class="form-control" placeholder="&#xf14b; describe your view" id="contentBlank" name="content" ng-model="content"></textarea>
                                                            </div>                          
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">

                                            </div> 
                                        </div>

                                        <div class="box">
                                            <div class="box-header"><i class="fa fa-dot-circle-o"></i> Assign tags to your talk </div>
                                            <div class="box-body asign_talk">
                                                <!-- <form class="sidebar-form top_search" method="get" action="#">
                                                     <div class="input-group">
                                                         <span class="input-group-btn">
                                                             <button class="btn btn-flat" id="search-btn" name="search" type="submit"><i class="fa fa-search"></i></button>
                                                         </span>
                                                         <input type="text" placeholder="Search..." class="form-control search_asign" name="q">         
                                                     </div>
                                                 </form>
                                                -->


                                                <div class="bs-example_1">
                                                    <div class="searchtag_input">
                                                        <i class="fa fa-search"></i>
                                                        <input type="text" value="tag name" id="tag_input" ng-keyup="talkTags(tagsinput)" ng-model="tagsinput" >
                                                    </div>

                                                    <ul id="tag"><li  ng-repeat="tag in sugessted_tags track by $index" class="dropdown_list_class" id="dropdown_list" ng-click="saveSuggestedTag(tag)" ng-bind="tag.tag_name"></li></ul>
                                                </div>
                                                <small>Tags not reconignizied? Suggest it to Arlians</small>
                                                <div class="clearfix"></div>
                                                <div >
                                                    <a href="#" class="tag_field"  ng-repeat="tag in savedtags track by $index" >
                                                        <i class="fa fa-tag tagicon"></i>
                                                        <span class="tag_list" id="close"  ng-bind="tag.tag_name"></span>
                                                        <i class="close_taglist fa fa-close" ng-click="closeTags(tag)"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="box-footer clearfix">
                                                <!-- Single button -->
                                                <div class="btn-group pull-right">
                                                    <button aria-expanded="false" type="button" data-toggle="dropdown" class="btn btn-primary  btn-sm dropdown-toggle ">Post</button>
                                                    <ul role="menu" class="dropdown-menu">                                        
                                                        <li ng-repeat='list in share_list'><a href="javascript:void(0);" ng-click="startTalkSubmit(content, title, list)"  ng-bind="list.name">Add new event</a></li>
                                                    </ul>
                                                    <!--<button type="submit" class="btn btn-primary pull-right" ng-click="startTalkSubmit(content,title)" >Post</button>-->
                                                </div>
                                            </div> 
                                        </div>  
                                        <div class="" style="width:100%; height:100px; visibility:hidden;">
                                        </div>

                                        <!--<div class="box">
                                            <div class="box-header"><i class="fa  fa-users"></i> Invite your audience to paritcipate </div>
                                            <div class="box-body asign_talk participate">
                                                <form class="sidebar-form top_search pull-right" method="get" action="#">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-flat" id="search-btn" name="search" type="submit"><i class="fa fa-search"></i></button>
                                                        </span>
                                                        <input type="text" placeholder="Search..." class="form-control search_asign" name="q">             
                                                    </div>
                                                </form>
                                                <p>Promote your talk via your favourite social media to invite peers to discuss </p>
                                                <div class="promote_talk clearfix">
                                                    
                                                    <textarea class="form-control" placeholder="&#xf14b;  Promote your talk." ng-model="talkText"></textarea>
                                                        <button ng-click="startTalkSubmit(talkText)">Post</button>-->
                                                    <!--<a href="#" class="promote_link"><i class="fa  fa-linkedin-square"></i></a> 
                                                    <a href="#" class="promote_link"><i class="fa  fa-twitter-square"></i></a>
                                                    
                                                </div>
    
                                            </div>
    
                                        </div>
    
    
                                        <!--<div class="box">
                                            <div class="box-header">
                                                <i class="fa fa-calendar"></i> Schedule your live talk</div>
    
    
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="date_clock">
                                                        <div class="col-md-7">
                                                            <div class="multiple"></div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="clocksjs">
                                                                <img src="images/clock.png" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
    
    
    
                                            </div>
    
                                        </div>-->


                                    </div>                
                                </div>
                            </form>
                        </div>    
                    </div>
                </div>
            </div>  

        </div>
    </div>
                                        <div class="modal fade" id="recommend" tabindex="-1" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Recommend Talk</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form novalidate name="recommend">


                                                            <input type="text" id="demo-input-custom-limits_recommend" ng-model="business_name" class="form-control fld"  placeholder="To: [auto fill by user name or email adress]" >
                                                            
                                                        
                                                            <div class="loading" style="display:none;">searching...</div>
                                                            <div class="no_result" style="display:none;">no result found...</div>
                                                            <!--<ul id="show_lilst"></ul>   -->
                                                            <div ><a href="#"  ng-repeat="business in savedBusinessName" >
                                                                    <span class="tag_list" id="close"  ng-bind="business.bussinessname"></span><i class="close_taglist" ng-click="closeBusinessname(business)">x</i></a>
                                                            </div>
                                                            <ul id="business_name_recommend"><li ng-repeat="business in businessname"  class="dropdown_list_class" id="dropdown_list_recommend" ng-click="saveBusiness_name(business)" ng-bind="business.bussinessname"></li></ul>
                                                            <textarea name="text" id="txt_msg" class="recomnd_txtarea"   placeholder="write talk"></textarea>
                                                         
                                                        </form>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="closeModal" ng-click="recommend_talk()">Recommend</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--<div class="modal fade" id="recommend" tabindex="-1" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Recommend Talk</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form novalidate name="recommend">


                                                            <input type="text" id="demo-input-custom-limits_recommend"  class="form-control fld"  placeholder="To: [auto fill by user name or email adress]">
                                                            
                                                            <textarea name="text" id="txt_msg" class="recomnd_txtarea"  placeholder="write talk"></textarea>
                                                         
                                                        </form>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="closeModal" ng-click="recommend_talk(message)">Recommend</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>-->
                                        
    <div class="col-md-3 col-sm-12  fixed-div1" id="id-3"  ng-controller="DashboardSuggestionCtrl as suggestion"  >
        <div class="row">   
            <div class="match_s mCustomScrollbar">  
                <div class="col-sm-6 col-md-12" ng-show="search_module">
                    <div class="view_result_box fixd_box1">
                        <div class="view_box_top"><h4>See All Matching <!--<small><i class="fa fa-search"></i></small>--></h4></div>
                        <div class="view_box_body">
                            <div class="match_result">
                                <span class="mathc_txt">MATCHING <br> RESULTS</span><br>
                                <span class="mathc_resulttxt" ng-bind="total_suggestion_count">132</span>
                            </div>
                            <div class="result_bottom clearfix">
                                <div id="result-circle" class="owl-carousel">
                                    <div class="item" ng-repeat="list in detail_suggestion_list">
                                        <div class="result_numb">
                                            <div class="result_numbin" ng-bind="list.type_count">69</div>
                                        </div>
                                        <span class="result_name" ><a ng-bind="list.type" href="javascript:void(0)" ng-click="getDetailSuggestion(list)">Customers</a></span>
                                    </div>                                 
                                </div>
                            </div>
                        </div>
                        <div class="view_box_bottom">
                           <!-- <a href="javascript:void(0);" class="view_menu" ng-click="openWizardTwoModal()"><i class="fa fa-list"></i></a>-->
                            <a href="javascript:void(0);" class="view_menu" ng-click="funRedirectToSetting()" ><i class="fa fa-list"></i></a>
    <!--                        <a href="javascript:void(0);" class="view_menu" data-toggle="modal" data-target="#myModal"><i class="fa fa-list"></i></a>-->
    <!--                        <p class="match_text">Improve your match</p>-->
                            <!-- Modal -->

                            <!--                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="myModalLabel">What sort of businesses do you service?</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    </div> 
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </div>
                            
                                                        </div>
                                                    </div>-->
                        </div>                      


                    </div>
                </div>
                <div class="col-sm-6 col-md-12"  ng-show="show_detail" id="detail_suggestion">
                    <div class="view_result_box fixd_box1" ng-repeat="detail in details_suggestion">
                        <div class="view_box_top1 matching clearfix"><h4 ng-bind="search_for_suggestion">Customers</h4>
                            <div class="result_numb1">
                                <div class="result_numbin1" ng-bind="total_count">69</div>
                            </div>
                        </div>
                        <div>
                            <div class="view_box_body1">
                                <div class="matchign_cont">
                                    <a  href="<?php echo base_url() ?>memberprofile/profile/{{detail.bussinessname + '/' + detail.mid}}" class="matchig_logo">
                                        <span  ng-show="detail.image == ''" ng-bind="detail.logobussinessname"></span>
                                        <!-- <img ng-show="detail.image != ''" ng-src="{{detail.image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+detail.image:'<?php echo base_url(); ?>assets/images/inbox-img.png'}}" alt="" >                                     -->
                                    
                                         <img ng-show="detail.image != ''" ng-src="{{detail.image!=null?detail.image:'<?php echo base_url(); ?>assets/images/inbox-img.png'}}" alt="" >                                  

                                    </a>
                                    <h5 ng-bind-html="detail.bussinessname | limitTo:100">Company Name</h5>
                                    <p class="match_detail" ng-bind-html="detail.desc | limitTo:120">Excel at enterprise, mobile and web development with Java, Scala, Groovy and Kotlin, with all the latest modern technologies and frameworks available out of the box. </p>
                                </div>
                                <img src="<?php echo base_url() ?>assets_phase2/images/match_enginebg.jpg" alt="">
                            </div>
                            <div class="view_box_body1 text-center clearfix">
                                <a href="javascript:void(0);" class="result_numb3" ng-click="individual_suggestion_user_feedback(detail, 0)">
                                    <div class="result_numbin3"><i class="fa fa-minus"></i></div>
                                </a>
                                <a href="javascript:void(0);" class="result_numb4" ng-click="individual_suggestion_user_feedback(detail, 1)">
                                    <div class="result_numbin4"><i class="fa fa-plus"></i></div>
                                </a>
                                <div class="clearfix match_score">
                                    <div class="" ng-class="{'result_numb_red'   : detail.score == 'one',
                                    'result_numb_amber' : detail.score == 'two',
                                    'result_numb_green'    : detail.score == 'three'}">
                                        <div class=""ng-class="{'result_numbin_red'   : detail.score == 'one',
                                    'result_numbin_amber' : detail.score == 'two',
                                    'result_numbin_green'    : detail.score == 'three'}" ng-bind="detail.score">69</div>

                                    </div>

                                    <span class="score_txt" ng-bind="detail.matched_text | limitTo:140">Match Score</span>
                                </div>
                            </div>
                        </div>
                        <div class="view_box_bottom">
                            <span class="backto_result" >
                                <a href="javascript:void(0);" ng-click="backToSuggestionList()">Back to matching results</a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-12">
                    <img class="p-d"  src="<?php echo base_url(); ?>assets_phase2/images/p-d.jpg" alt=""/>
                    <!--                <div class="view_result_box portfolio">
                                        <div class="view_box_top"><h4>Portfolio Documents</h4></div>
                                        <div class="view_box_body clearfix">
                    
                                            <div class="portfolio_box white_box">
                                                <i class="fa fa-pencil"></i>
                                            </div>
                    
                                            <div class="portfolio_box normal_box">
                                                <span class="portbox_txt">Notes</span><br>
                                                <span class="port_inncircle">12</span>  
                                            </div>
                    
                                            <div class="portfolio_box  normal_box ">
                                                <span class="portbox_txt">Documents</span><br>
                                                <span class="port_inncircle">12</span> 
                    
                                            </div>
                    
                                            <div class="portfolio_box white_box ">
                                                <i class="fa  fa-folder"></i>    
                                            </div>
                    
                                            <div class="portfolio_box white_box">
                                                <i class="fa fa-comment"></i>
                                            </div>
                    
                                            <div class="portfolio_box normal_box">
                                                <span class="portbox_txt">Talks</span><br>
                                                <span class="port_inncircle">12</span>  
                                            </div>
                    
                                        </div>
                                        <div class="view_box_bottom">
                                            <a href="javascript:void(0)" class="view_menu"><i class="fa fa-list"></i></a>
                                        </div>
                                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>


                                        
                                        
                                        
                                        <div class="modal fade" id="participateModal" tabindex="-1" role="dialog" >
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" ng-bind="talk.bussiness_name"></h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>This talk is private.So you need to take permission before participate&hellip;</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        <button type="button" class="btn btn-primary" id="sendreqstid" >Send Request</button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        
                                        



<!-- common-helper JS -->   
<link rel="styleSheet" href="<?php echo base_url(); ?>assets/css/token-input.css" />

<script src="<?php echo base_url(); ?>assets_phase2/js/dashboard_page.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.tokeninput.js"></script> 
<script src="<?php echo base_url(); ?>assets_phase2/js/custom-file-input.js"></script>
<!-- 05-04-16 for fade post -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets_phase2/js/waypoint-fade.min.js"></script>





<script>
                                            $(window).resize(function() {
                                    $('.mCustomScrollbar').height($(window).height());
                                    });
                                            $(window).trigger('resize');

                                            </script>




<script>
            $(function(){
            $('#hub_publish_default_img').show();
                    $('#hub_publish_preview_img').hide();
                    $('.left-block, .tab-sticky-scroll')
                    .theiaStickySidebar({
                    additionalMarginTop: 50
                    });
//CKEDITOR.replace('txtEditor');
                    
                    $('.mCustomScrollbarN').mCustomScrollbar({
            //setHeight: 250
            //autoExpandScrollbar : false
            });
            });</script>

<script>
            $(document).ready(function() {

    });
            (function($){
            $(window).load(function(){

            $("#content-1").mCustomScrollbar({
            axis:"yx", //set both axis scrollbars
                    advanced:{autoExpandHorizontalScroll:true}, //auto-expand content to accommodate floated elements
                    // change mouse-wheel axis on-the-fly 
                    callbacks:{
                    onOverflowY:function(){
                    var opt = $(this).data("mCS").opt;
                            if (opt.mouseWheel.axis !== "y") opt.mouseWheel.axis = "y";
                    },
                            onOverflowX:function(){
                            var opt = $(this).data("mCS").opt;
                                    if (opt.mouseWheel.axis !== "x") opt.mouseWheel.axis = "x";
                            },
                    }
            });
            });
            })(jQuery);</script>
<script>
            $(function () {
            $("#demo-input-custom-limits").tokenInput("<?php echo base_url() . 'member/autocomplete' ?>", {
            //$("#demo-input-custom-limits_recommend").tokenInput("<?php echo base_url() . 'member/autocomplete' ?>", {
            searchDelay: 2000,
                    minChars: 2,
                    propertyToSearch: "bussinessname",
                    tokenValue: "mid",
            });
            });

</script>

