<div class="premium-cancel-wrap">
    <h2>Membership Reactivation</h2>
    <a class="delect-account suspend-account">Reactivate my account</a>
    <p class="suspend-mess">Your [MEMBERSHIP TYPE – FREE or PREMIUM] membership was suspended on [dd/mm/yyyy].<br><br>
    To reactivate your account, follow the instruction below.</p>
    <!--
    <div class="select-premium">
        <h3>Are you sure you wish to continue?</h3>
        <div class="reactivate-premium">
            <label>Please reactivate my account as:</label>
            <ul>
                <li><input type="radio" name="radio" id="radio">FREE MEMBERSHIP</li>
                <li><input type="radio" name="radio" id="radio">PREMIUM MEMBERSHIP</li>
            </ul>
            <a href="#" class="member-click-link">Click here for more information</a>
        </div>
    </div>
  -->
    <div class="account-delet-form">
        <ul>
            <li>
                <div class="account-delet-left">
                    <label>Enter your password</label>
                </div>
                <div class="account-delet-right">
                    <input type="password" name="pass" id="pass">
                </div>
            </li>
            <li>
                <div class="account-delet-left">
                    <label>Re-enter your password</label>
                </div>
                <div class="account-delet-right">
                    <input type="password" name="pass" id="pass">
                </div>
            </li>
            <li>
                <div class="account-delet-right">
                    <input type="submit" value="Submit" id="submit">
                </div>
            </li>
        </ul>
    </div>
</div>
<script>
  $(function(){
    $("#loader-wrapper").show();
  });
</script>