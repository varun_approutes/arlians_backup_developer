<!doctype html>
<html lang="en" class="no-js">
    <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.core.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/loader.css">
    <!-- CSS reset -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/onepage-scroll.css" type='text/css'>
        <!-- Resource style -->
        <!-- Modernizr -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/demo-1.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/knockout-3.2.0.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/alertify.js"></script>
    <!-- <script src="js/main.js"></script> --> 
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.onepage-scroll.js"></script>
    <title>Arlians</title>    
    </head>
    <!-- hijacking: on/off - animation: none/scaleDown/rotate/gallery/catch/opacity/fixed/parallax -->
    <body data-hijacking="off" data-animation="scaleDown">
        <!--<header>
            <div class="logo">
                <a href="" class="logo-img">
                    <img src="<?php echo base_url(); ?>assets/images/Arlians-Full-Logo.png" title="Arlians logo" />
                </a>
            </div>
            <div class="login-btn-rt">
                <a href="<?php echo base_url(); ?>member/login/" class="login login-target">Login</a>
                <a href="javascript:void(0);" class="sign-up login-target">Signup</a>
            </div>
        </header>-->
        <header class="top-header" id="sign_up_config_section"> 
            <div class="logo">
                <a href="" class="logo-img">
                    <img src="<?php echo base_url(); ?>assets/images/Arlians-Full-Logo-transparent.png" title="Arlians logo" />
                </a>
            </div>
            <div class="login-btn-rt top-login">
                <form id="sign_up_login_form" name="sign_up_login_form" data-bind="visible:show_login_section">
                    <ul>
                        <li>
                            <label>Email</label>
                            <input type="email" name="email" data-bind="value:user_email" placeholder="Email">
                        </li>
                        <li>
                            <label>Password</label>
                            <input type="password" name="password" data-bind="value:user_password" placeholder="Password">
                            <a href="javascript:void(0)" data-bind="click:showForgetCredentialSection" class="forget-pass">Forgotten your password?</a>
                        </li>
                        <li class="width-auto">
                            <input type="submit" value="Login" class="sign-up">
                        </li>
                    </ul>
                </form>
                <form id="sign_up_forget_credential_form" name="sign_up_forget_credential_form" data-bind="visible:show_forget_credential_section">
                    <ul>
                        <li>
                            <label>Email</label>
                            <input type="email" name="forget_email" data-bind="value:forgrt_password_email" placeholder="Email">
                            <a href="javascript:void(0)" data-bind="click:showLoginSection" class="forget-pass">Back to Login</a>
                        </li>
                        <li class="width-auto">
                            <input type="submit" value="Send" class="sign-up">
                        </li>
                    </ul>
                </form>
            </div>
        </header>
        <div id="loader-wrapper" style="display:none">
            <!-- loader start -->
            <div class="loader"></div>
        </div>
        <?php echo $content;?>
    </body>
	<script>
        var vmSignUpConfig = {
            show_login_section:ko.observable(true),
            show_forget_credential_section:ko.observable(false),
            user_email:ko.observable(),
            user_password:ko.observable(),
            forgrt_password_email:ko.observable(),
            showLoginSection:function(){
                vmSignUpConfig.show_login_section(true);
                vmSignUpConfig.show_forget_credential_section(false);
            },
            showForgetCredentialSection:function(){
                vmSignUpConfig.show_login_section(false);
                vmSignUpConfig.show_forget_credential_section(true);
            }
        }
		jQuery(document).ready(function(){           
			jQuery(".main").onepage_scroll({
				sectionContainer: "section",
				responsiveFallback: 600,
				loop: true
			});
			jQuery('#arlians-left').click(function(){
				jQuery('ul.onepage-pagination li a').each(function(){
					if(jQuery(this).attr('data-index')=='2'){
						jQuery(this).click();
					}
				});
			});
			jQuery('.login-target').click(function(){
				jQuery('ul.onepage-pagination li a').each(function(){
					if(jQuery(this).attr('data-index')=='5'){
						jQuery(this).click();
					}
				});
			});
            $("#sign_up_login_form").validate({
                rules: {
                    email: { required: true },
                    password: { required: true }
                },
                messages: {
                    email: { required: "Please enter email" },
                    password: { required: "Please enter password" },
                },
                submitHandler: function (form) {
                    $('#loader-wrapper').show();
                    $.post('<?php echo base_url(); ?>member/validateSignUpMember',{email:vmSignUpConfig.user_email(),password:vmSignUpConfig.user_password()},function(result_data){ 
                        $('#loader-wrapper').hide();
                        result_data = eval("(" + result_data + ")");
                        if(result_data.validate==false){                            
                            alertify.error('Invalid Login Credentials');
                        }else{                            
                            if(result_data.status==1){
                                window.location= "<?php echo base_url();?>"+result_data.redirect;
                            }else if(result_data.status==2){
                                vmSignUpConfig.user_email('');
                                vmSignUpConfig.user_password('')
                                alertify.success(result_data.error_message);    
                            }else{
                                window.location= "<?php echo base_url();?>"+result_data.redirect;
                            }
                        }
                    });                     
                    return false;
                }
            });
             $("#sign_up_forget_credential_form").validate({
                 rules: {
                    forget_email: { required: true }
                },
                messages: {
                    forget_email: { required: "Please enter email to find your account" }
                },
                submitHandler: function (form) {
                    $.post('<?php echo base_url(); ?>member/changepassword',{data:vmSignUpConfig.forgrt_password_email()},function(result){            
                        if(result == "1"){
                            alertify.success("An email has been forwarded to your email address to reset your password");
                            vmSignUpConfig.forgrt_password_email('')
                            vmSignUpConfig.show_login_section(true);
                            vmSignUpConfig.show_forget_credential_section(false);
                        } else{
                           alertify.error("Your email address has not been registered!");
                        }
                    });
                    return false;
                }
             });
            $('.onepage-pagination').hide();           
		});
        ko.applyBindings(vmSignUpConfig, $("#sign_up_config_section")[0]);
	</script>
</html>