<?php //die; ?>
<script>

    function validEmail(v) {
            var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
            return (v.match(r) == null) ? false : true;
    }

    function validPassword(v) {            
            if(v.length < 8){
                return false;
            }
            else if(!/\d/.test(v)){
                return false;
            }
            else if(!/[A-Z]/.test(v)){
                validated = false;   
            }
            else if(/[^0-9a-zA-Z]/.test(this.value)){
              validated = false;  
            } else{
               validated = true;  
            }
        return validated;
    }

    function valform(){
        var pass = $('#pass').val();
        var confirm_pass = $('#confirm_pass').val();

        if(pass == confirm_pass){
                var x = validPassword(pass);
                //alert(x);
                if(x == true){
                    $('#err').hide();
                    $('#err').html('');
                    return true;
                } else{
                      $('#err').show();  
                      $('#err').html('Please ensure it is at least 8 characters and contains at least one upper case letter'); 
                      return false; 
                }  
        }else{
            $('#err').show();
          $('#err').html('Password Missmatch'); 
          return false; 
        }
    }

$(document).ready(function(){

	$('#member_main_section').hide();
    
	$('#sub_member').click(function(){
    $('#err').show();    
        //========================== VALIDATION========================
		var email = $('#email').val();
        if(email == ''){
            $('#err').show();
            $('#err').html('Email Cannot be Blank.');
            return;
        }else{
            var x = validEmail(email);

            if(x == false){
                $('#err').show();
                $('#err').html('Email Invalid.');
                return;
            } else{

                $('#hemail').val(email);

            $.post('<?php echo base_url(); ?>member/validatemember',{data:email},function(res){
                
                if(res == "FALSE"){
                    $('#member_main_section').show();
                    $('.first_section').hide();
                    $('#err').html('');
                    $('#err').hide();
                } else{
                    $('#err').show();
                    $('#err').html("This email address is already registered. <br/>Someone else from your business may have already created a profile, </br/>would you like to <a href='<?php echo base_url(); ?>member/login'>request access</a>? OR Have you just <a href='javascript:void(0)' onclick='showForgetPassword()'>forgotten your password?</a>");
                    //$('#err').html('This email address is already registered. <br/>Someone else from your business may have already created a profile, </br/>would you like to request access? OR Have you just forgotten your password?');
                    //alert('Email address is already registered');
                }
            });


            }
        }

        //============================================================

	});
    $("#ab_SignUpSubmit").click(function(){
       var check_emai_id = $('#hemail').val();
        $.post('<?php echo base_url(); ?>member/validatemember',{data:check_emai_id},function(result_request){ 
          if(result_request=="FALSE"){
            $('#member_main_form').submit();
          }else{
             alertify.alert("Email Id Alreary exit.Please entry another email Id"); 
             alertify.error("Email Id already exit;");    
          }   
        });
    });
});


</script> 


        
<div class="wrapper login-wrapper">
    <div class="main" id="member_cofig_section">
        <section class="cd-section visible">
            <div>
                <div class="container">
                    <div class="center-box top-center-box">
                        <div class="center-box-hght">
                        <!-- <div class="hero">
                            <img class="hideme" src="img/placeholder.png" />
                        </div> -->
                        <h2 class="first">Connecting businesses. Unlocking growth</h2>
                        <div id="err" class="error" style="color:red;"> </div>
                        <div class="middle-signup-container first_section">
                        	<form id='email-form' push data-bind="visible:show_sign_up">
                            <ul>
                                <li>
                                    <input type='email' name='email' id='email' class='txt' placeholder='Email address' email required>
                                </li>
                                <li>
                                    <button id="sub_member" class="button" type="button">Sign up</button>
                                </li>
                            </ul>
                        	</form>
                            <form class='form_forget_pass' push data-bind="visible:show_forget_password_section">
                            <ul>
                                <li>
                                    <input type='email' name='forgrt_password_email' data-bind="value:forgrt_password_email"  class='txt' placeholder='Email address' email required>
                                </li>
                                <li>
									<button id="sub_member" class="button" type="button" onclick="forgetPassFrmSubmit();">Find your Account</button>
									<button class="button" data-bind="click:showSignUP" type="button">Back</button>                                                    
                                </li>
                            </ul>
                            </form> 
                             <div class="video">
                            <div class="fl copy">
                                <a href="https://vimeo.com/133324592" target="_blank" style="font-size:14px; color:#F55967">Watch our video</a>
                                </div>
                                <a href="https://vimeo.com/133324592" target="_blank" class="playbox nomobile" style="font-size:40px; color:#F55967"><i class="fa fa-play-circle"></i>
                                </a>
                            </div>
                        </div>
                        <div id='member_main_section'>
                        	   <div class="middle-signup-container">
                        	   		<form id='member_main_form' action='<?php echo base_url(); ?>member/index' method='post' onsubmit='return valform();'>                        	   			
					                    <ul>
    					                    <li>
    					                        <input name='data[hemail]' id='hemail' type="email" placeholder="user@domainname.com" class="txt" required>
    					                    </li>
    					                    <li>
    					                    	<input type='password' name='data[pass]' id='pass' placeholder='password' class='txt'>
    					                    </li>
    					                    <li>    					                
    					                   	 	<input type='password' name='data[confirm_pass]' id='confirm_pass' class="txt" placeholder='confirm password'>
    					                    </li>
    					                    <li>					                    	 
    					                    	<input id="ab_SignUpSubmit" type='button' onclcik="checkEmailBeforeRegister();" value='Sign up' class='button'>
    					                    </li>
					                   </ul>
					                 </form>
			                    </div>

                        </div>
                        <div class="middle-social-section">
                            <div class="term-privacy-left">
                                <ul>
                                    <li><a href="#">Terms</a></li>
                                    <li><a href="#">Privacy</a></li>
                                    <li><a href="#">Support</a></li>
                                </ul>
                            </div>
                            <div class="social-icon-right">
                                <ul>
                                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/facebook.png" /></a></li>
                                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/twiiter.png" /></a></li>
                                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/linkdin.png" /></a></li>
                                </ul>
                            </div>
                        </div>
                          </div>

                    </div>
                </div>
                <div class="bottom-button">
                    <p><a href="javascript:void(0);" id="arlians-left">What is Arlians?</a>
                        <br><i class="fa fa-chevron-down"></i>
                    </p>
                </div>
            </div>
        </section>
        <section id="down-1" class="cd-section cd-section-two">
            <div>
                <div class="container">
                    <div class="center-box">
                        <h2>Arlians is a simple business tool designed to help you grow</h2>
                        <h4>Customers | Suppliers | Alliances | Buyers | Investors </h4>
                        <p>Whatever ‘growth’ means to you from acquiring a new customer, to forming an alliance, Arlians will be with you every step of the way. Create your profile and then sit back, relax and watch us match you to the right connections.
                        From there it’s up to you… it really is that simple.
                        <div class="hero">
                            <img class="hideme" src="<?php echo base_url(); ?>assets/images/ipad-1.png" />
                        </div>
                        </p>
                        
                    </div>
                </div>
            </div>
        </section>
        <section id="down-2" class="cd-section">
            <div>
                <div class="container">
                    <div class="center-box">
                        <h2>Arlians is so much more than just a network. <br>It’s a market place</h2>
                        <p>
                        Discover opportunities to grow. Connect with businesses and form alliances. Learn from data and insights. Share your news with everyone. Collaborate with others and join forces
                        </p>
                        <div id="large-header" class="large-header">
                            <canvas id="demo-canvas"></canvas>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="down-3" class="cd-section">
            <div>
                <div class="container">
                    <div class="center-box">
                        <h2>It’s for everyone</h2>
                        <p>From local bakeries to global banks, little corner shops to giant consultancies, old manufacturers to new media agencies… Arlians is for any business looking to grow.
                        </p>
                        <div class="circle-container">
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-1.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-2.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-3.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-4.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-5.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-6.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-7.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-1.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-2.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-3.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-4.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-5.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-6.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-7.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-1.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-2.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-3.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-4.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-5.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-6.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-7.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-5.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-6.jpg" />
                            </div>
                            <div class="circle">
                                <img src="<?php echo base_url(); ?>assets/images/small-7.jpg" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="down-4" class="cd-section cd-section-four">
            <div>
                <div class="container">
                    <div class="center-box">
                        <h2>Connecting businesses. Unlocking growth</h2>


                        <div id="err_last" class="error" style="color:red;"> </div>
                        <div class="middle-signup-container first_section min-height-zero">
                            <form id='email-form_last' push  data-bind="visible:show_sign_up">
                            <ul>
                                <li>
                                    <input type='email' name='email' id='email_last' class='txt' placeholder='Email address' email required>
                                </li>
                                <li>
                                    <button id="sub_member_last" class="button" type="button">Sign up</button>
                                </li>
                            </ul>
                            </form>
							<form class='form_forget_pass' push data-bind="visible:show_forget_password_section">
                            <ul>
                                <li>
                                    <input type='email' name='forgrt_password_email' data-bind="value:forgrt_password_email"  class='txt' placeholder='Email address' email required>
                                </li>
                                <li>													
                                    <button id="sub_member" class="button" type="button" onclick="forgetPassFrmSubmit()">Find your Account</button>
									<button class="button" data-bind="click:showSignUP" type="button">Back</button>
                                </li>
                            </ul>
                            </form>
                            
                        </div>
                        <div id='member_main_section_last'>

                               <div class="middle-signup-container">

                                    <form id='member_main_form_last' action='<?php echo base_url(); ?>member/index' method='post' onsubmit='return valform_last();'>
                                        
                                        <ul>

                                         <li>
                                            <input name='data[hemail]' id='hemail_last' type="email" placeholder="user@domainname.com" class="txt" required>
                                        </li>

                                         <li>
                                            <input type='password' name='data[pass]' id='pass_last' placeholder='password' class='txt'>
                                        </li>

                                        <li>
                                    
                                            <input type='password' name='data[confirm_pass]' id='confirm_pass_last' class="txt" placeholder='confirm password'>
                                        </li>

                                         <li>
                                             
                                             <input id="a_SignUpSubmit_last" type='submit' value='Sign up' class='button'>
                                        </li>

                                       </ul>
                                     </form>

                                </div>

                        </div>


                        <p>Register now for updates and you’ll get the opportunity for exclusive free access to premium service.
                        <br>Together, we can change the way businesses connect and collaborate.
                        </p>
                        <div class="video">
                                <div class="fl copy">
                                    Want to know more?<br>
                                    <a href="https://vimeo.com/133324592" target="_blank" style="font-size:14px; color:#fff">Watch our video</a>
                                </div>
                                <a href="https://vimeo.com/133324592" target="_blank" class="playbox nomobile" style="font-size:40px; color:#fff"><i class="fa fa-play-circle"></i>
                                </a>
                            </div> 
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
                <!--   <nav>
                    <ul class="cd-vertical-nav">
                        <li><a href="#0" class="cd-prev inactive">Next</a>
                    </li>
                    <li><a href="#0" class="cd-next">Prev</a>
                </li>
            </ul>
        </nav> -->
        <!-- .cd-vertical-nav -->
        
        
        <!-- Resource jQuery -->
<script type="text/javascript">
	 var vmMemberConfiguration={
		 show_sign_up:ko.observable(true),
		 show_forget_password_section:ko.observable(false),
		 forgrt_password_email:ko.observable(''),
		 showSignUP:function(){
			vmMemberConfiguration.show_sign_up(true);
			vmMemberConfiguration.show_forget_password_section(false);
		 }
	 }
	 function showForgetPassword(){
        $("#err").hide();
        $("#err_last").hide();
		 vmMemberConfiguration.show_sign_up(false);
		 vmMemberConfiguration.show_forget_password_section(true);
	 }
     function forgetPassFrmSubmit(){
        if(vmMemberConfiguration.forgrt_password_email()==''){
            $('#err_last').show();
            $('#err').show();               
            $('#err_last').html("Email address cannot be blank");
            $('#err').html("Email address cannot be blank"); 
            return false;
        }
        var check_valid_email = validEmail(vmMemberConfiguration.forgrt_password_email());  
		
        if(check_valid_email==true){
            $.post('<?php echo base_url(); ?>member/changepassword',{data:vmMemberConfiguration.forgrt_password_email()},function(res){                
                if(res == "1"){					
                    $('#err_last').show();
                    $('#err').show();   
                
                    $('#err_last').html("An email has been forwarded to your email address to reset your password");
                    $('#err').html("An email has been forwarded to your email address to reset your password");
					
                } else{
                    $('#err_last').show();
                    $('#err').show();               
                    $('#err_last').html("Your email address has not been registered!");
                    $('#err').html("Your email address has not been registered!");
                }
            });
        }else{
            $('#err_last').show();
            $('#err').show();               
            $('#err_last').html("Enter a valid email address");
            $('#err').html("Enter a valid email address"); 
        }
    }
	jQuery(document).ready(function(){
		$('.error').hide();
	});


    function valform_last(){
        var pass = $('#pass_last').val();
        var confirm_pass = $('#confirm_pass_last').val();
        if(pass == confirm_pass){
                var x = validPassword(pass);
                //alert(x);
                if(x == true){
                    $('#err_last').hide();
                    $('#err_last').html('');
                    return true;
                } else{
                    $('#err_last').show();
                    $('#err_last').html('Please ensure it is at least 8 characters and contains at least one upper case letter'); 
                    return false; 
                }  
        }else{
            $('#err_last').show();
          $('#err_last').html('Password Missmatch'); 
          return false; 
        }
    }
	

$(document).ready(function(){

    $('#member_main_section_last').hide();
    
    $('#sub_member_last').click(function(){
        //========================== VALIDATION========================
        var email = $('#email_last').val();
        if(email == ''){
            $('#err_last').html('Email cannot be blank.');
            return;
        }else{
            var x = validEmail(email);

            if(x == false){
                $('#err_last').html('Email invalid.');
                return;
            } else{
                $('#hemail_last').val(email);
                  
        $.post('<?php echo base_url(); ?>member/validatemember',{data:email},function(res){           
            if(res == "FALSE"){
                $('#member_main_section_last').show();
                $('.first_section').hide();
                $('#err_last').html('');
            } else{
                $('#err_last').show();
                $('#err_last').html("This email address is already registered. <br/>Someone else from your business may have already created a profile, </br/>would you like to <a href='<?php echo base_url(); ?>member/login'>request access</a>? OR Have you just <a href='javascript:void(0)' onclick='showForgetPassword()'>forgotten your password?</a>");
                //alert('Email address is already registered');
            }
        });


            }
        }

        //============================================================

    });
});
ko.applyBindings(vmMemberConfiguration, $("#member_cofig_section")[0]);
   </script>      
        
   