 <?php $mid = $this->session->userdata['logged_in']['id']; 
$user_type = check_member_payment_status($mid); ?> 
<div class="dashboard-wrap">

       

    <div class="hub-wrap"> 
        <div class="dashboard-hub-heading">
               <h2><i class="fa fa-newspaper-o fa-2"></i>The hub</h2>
        </div>
       
 
        <div class="duo__cell example__demo" data-js-module="layout-complete-demo">
          
            <div class="grid grid--clickable">
             <div class="grid-item hub-listing">
              <div class="dashboard-post-upload">
                <ul class="tab-post" id="tabs">
                  <li ><a id="post-open" href="javascript:void(0)" name="#tab1">Post</a></li>
                  <li class="active-post"><a href="javascript:void(0)" name="#tab2">Upload</a></li>
                  <li ><a onclick="redirect_url()" href="javascript:void(0)">Publish</a></li>
                </ul>
                <div id="content_tabs">
                  <div id="tab1">
                    <div class="post-form-open">
                      <textarea id='post_desc' placeholder="What's on your mind?"></textarea>
                     
                      <div class="post-form-select">
                      <label >Share with: </label>
                      <div class="post-left">
                        <span class="select">
                        <select class="share-public" id='shr_to'>
                          <option value='1'>Private: your network only</option>
                          <?php //if( $user_type[0]['user_type'] == "Free"){?>
                            <!--<option value='2' disabled >Public: all businesses you match with </option>
                            <option value='3' disabled>Public: all relevant businesses</option>-->
                          <?php //}else{?>
                            <option value='2'>Public: all businesses you match with</option>
                            <option value='3'>Public: all relevant businesses</option>
                          <?php //}?>
                        </select>
                        </span>
                      </div>
                      <div class="post-right">
                        <span class="select">
                        <select class="tag" onchange="count_option()" name="tag[]" id="tag" multiple="multiple" placeholder="Select">
                          <?php foreach($tag as $tag_row){  ?>
                            <option value='<?php echo $tag_row['tag_code']; ?>'><?php echo $tag_row['tag_name']; ?></option>
                          <?php } ?>
                        </select>
                        </span>
                      </div>
                      <input id='share' type="button" value="Share">
                      </div>
                    </div>  
                </div>
                <div id="tab2">
                    <div class="post-form-open">
                        <textarea id='about_img' placeholder="What's on your mind?"></textarea>
                        
                        <div class="upload-file">
                          <input type='file' name="file" onchange="readURLPostImage(this);" />
                          <label style="color:#ed5463; padding:0 0 0 5px;">Choose file</label>
                          <img style="width:25%; height:65px; float:left;" id="post_upload"/>
                        </div>
                        <div class="post-form-select">
                            <label >Share with: </label>
                            <div class="post-left">
                                <span class="select">
                                <select class="share-public" id='shr_to_pic'>
                                  <option value='1'>Private: your network only</option>
                                  <?php //if( $user_type[0]['user_type'] == "Free"){?>
                                    <!--<option value='2' disabled >Public: all businesses you match with </option>
                                    <option value='3' disabled>Public: all relevant businesses</option>-->
                                  <?php //}else{?>
                                    <option value='2'>Public: all businesses you match with</option>
                                    <option value='3'>Public: all relevant businesses</option>
                                  <?php // }?>
                                </select>
                                </span>
                            </div>
                              <div class="post-right">
                              <span class="select">
                              <select class="tag" onchange="count_option_img()" name="tag[]" id="tag_for_img" multiple>
                                <?php foreach($tag as $tag_row){  ?>
                                  <option value='<?php echo $tag_row['tag_code']; ?>'><?php echo $tag_row['tag_name']; ?></option>
                                <?php } ?>
                              </select>
                              </span>
                            </div>
                            <input id='share_pic' type="button" value="Share">
                        </div>
                    </div>
                  </div>
                </div>
        </div>
      </div>
            <?php $user_priority = $user_type[0]['user_type'];?>
            <?php $i = 0; foreach($hubs as $hub){ 
                    $class = ($i%2 != 0)? ( $user_priority == "Free"? "grid-item hub-listing":"grid-item hub-listing premium"):($user_priority== "Free"? "hub-listing":"hub-listing premium");

              ?>               
                <div class="<?php echo $class; ?>" id="<?php echo $hub['hid']?>_hub">
                    <div class="hub-left">
                        <?php $imageName = getProfileImage($hub['mid']); ?>
                        <a href='<?php echo base_url(); ?>viewprofile/profile/<?php echo $hub["mid"]; ?>'>
                        <img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $imageName!=''?$imageName:'profile-img.png'; ?>" alt=""/>
                            </a>
                        <a href='<?php echo base_url(); ?>viewprofile/profile/<?php echo $hub["mid"]; ?>'><h3><?php echo getMemberbusinName($hub['mid']); ?></h3></a>
                    </div>
                    <div class="right-hub">
                      <?php if($hub['hub_title']!=''){?><h3><?php echo strip_tags($hub['hub_title']); ?></h3><?php } ?>
                        <div class="right-hub-back">
                            <?php if($hub['link'] != '') {?>
         
                    <div class="hub-img">
								<!--<?php if($hub['hub_title']!=''){?><h3><?php echo $hub['hub_title']; ?></h3><?php } ?>-->

                                <img src="<?php echo base_url(); ?>uploads/hub_images/<?php echo $hub['link']; ?>" alt=""/>
                                
                                
                            </div>
                            <?php } ?>
                            <div class="hub-content">
                                    <p><?php echo readMore(checkURLfromString($hub['content']),150); ?></p>
								<p>... <a href="<?php echo base_url(); ?>hub/hubdetails/<?php echo $hub["mid"].'/'.$hub['hid'];?>">Read More</a></p>
                                <a class="share-report" href="javascript:void(0)" onclick="showHideDiv('<?php echo $hub["hid"]?>_share-report-open')"> <i class="fa fa-ellipsis-v fa-2"></i></a>
                                <div class="share-report-sec" id="<?php echo $hub['hid']?>_share-report-open" style="display:none">
                                  <ul>
                                    <li><a href="javascript:void(0)" onclick="shared('<?php echo $hub['hid'];?>')">Share</a></li>
                                    <li><a class="edit-sec report_modal_link" href="#report_modal" onclick="setHubId('<?php echo $hub["hid"]?>')" >Report</i></a></li>
                                    <?php ;if(empty($hub['is_connected'])){?>
                                        <li><a href="javascript:void(0)" onclick="connectedToUser(this,<?php echo $hub['mid']?>)">Connect</a></li>
                                    <?php }else{?>                                        
                                        <li>
                                        <a href="javascript:void(0)">                                         
                                        <?php if($hub['is_connected']->is_accepted!='0'){?> 
                                            Connected
                                        <?php }else{?>
                                            Request send
                                        <?php }?>
                                        </a>
                                        </li>
                                    <?php }?>
                                    <?php if($hub['mid'] == $user['mid']){?>
                                      <li><a href="javascript:void(0)" onclick="deletePost(<?php echo $hub['hid']?>)">Delete</a></li>
                                    <?php }?>  
                                  </ul>
                                </div>
                            </div>
                        </div>
                      <div class="comment-post-wrap">
                          <div class="comment-post">
                              <div class="comment-like">
                                <?php if($hub['like'] == 1){?>
                                    <a class="like active" href="javascript:void(0)" onclick="manageLike(this,<?php echo $hub['hid']?>)"> <i class="fa fa-heart fa-2"></i></a>
                                <?php }else{?>
                                    <a class="like" href="javascript:void(0)" onclick="manageLike(this,<?php echo $hub['hid']?>)"> <i class="fa fa-heart fa-2"></i></a>
                                <?php }?>
                                <div class="count-like">
                                <span style="color:#000" id="<?php echo $hub['hid']?>"><?php echo $hub['likes'];?></span>
                                <span style="color:#000" id="<?php echo $hub['hid']?>_text"><?php echo $hub['likes']==1?' like':' likes'?></span>
                                </div>
                                <?php if(count($hub['comments'])>2){?>
                                    <a class="comment" href="<?php echo base_url(); ?>hub/hubdetails/<?php echo $hub["mid"].'/'.$hub['hid'];?>">View all comments</a>
                                <?php }?>
                                
                              </div>
                              <div id="<?php echo $hub['hid']?>_comments_list">
							   <div class="comment-read">
                                  <div class="comment-writing-logo">
                                      <img src="<?php echo base_url();?>uploads/profile_image/<?php echo $user['fornt_image']!=null ? $user['fornt_image']:'profile-img.png';?>" alt=""/>
                                  </div>
                                <div class="comment-textarea">
                                   <textarea id="<?php echo $hub['hid']?>_comments"></textarea>
                                  <a class="comment" onclick="postComments(<?php echo $hub['hid']?>);">Post</a>
                                </div>
                               
                              </div>
                              <?php if(count($hub['comments']) >0 ){ ?>
                                <?php $count=0;?>
                                <?php foreach($hub['comments'] as $comment){?>
                                  <?php if($count<=2){?>  
                                    <div class="comment-writing" id="<?php echo $comment['comm_id']?>_comment"> 
                                  <?php //}?>  
                                    <!--<div class="comment-writing hid_comment" id="<?php echo $comment['comm_id']?>_comment"> -->
                                  <?php //}?>  
                                  <?php                                   
                                    if($comment['pic_id']!=NULL) { 
                                          $pic = $comment['image_url'];
                                        } else{
                                          $pic = "profile-img.png";
                                    }
                                    //print_r($hub['comments']);
                                  ?>   
                                    <div class="comment-writing-logo" >
                                      <img alt="" src="<?php echo base_url();?>uploads/profile_image/<?php echo $pic; ?>">
									   <div class="comment-head-title"><?php echo getMemberbusinName($comment['mid']); ?></div>
                                    </div> 
                                    
                                    <div class="comment-righting-text" >
                                    <?php echo checkURLfromString($comment['comments']); ?>
                                    <?php if($comment['mid'] == $user['mid']){?>
                                      <div class="comment-delet">
                                      <a href="javascript:void(0)" onclick="deleteComment(<?php echo $comment['comm_id']?>)"><i class="fa fa-trash fa-2"></i></a>
                                      </div>
                                    <?php }?>
                                    </div>            
                                  </div>
								   <?php } ?>
                                  <?php $count++;?>
                                  <?php }?>
                                 <?php } ?>
                              </div>
                             
                            </div>
                      </div>
                    </div>
                </div>                   
                <?php $i++; } ?>          
            </div>
        </div>
    </div>     
    <div id="report_modal" style="width:400px; display:none;">
        <div class="popup-box-part-center">
            <form id="report_form" method="post">
                <h2>Report</h2>
                <input type="hidden" id="hidden_hub_id" >
                <div class="business-img-listing-wrap cust-fancy-label-input">
                    <div class="cust-fancy-label-input">
                        <label>Cause</label>
                        <input type="text" name="cause_report" id="cause_report" >
                    </div>                     
                    <button type="submit" class="button" >Send</button> 
                </div>      
            </form>    
        </div>
    </div>          
</div>
<script type="text/javascript">
     /*jQuery(document).ready(function(){
	  var user_types= "<?php echo $user_type[0]['user_type']; ?>";
	  if(user_types == 'Free'){
		  jQuery('.post-form-select').hide();
	  }
	 });*/
	  
  $(function(){
      setTimeout(function() {
        location.reload();
      }, 180000);
      $('#tag').tokenize({placeholder:"Start typing your tags"});
      $('#tag_for_img').tokenize({placeholder:"Start typing your tags"});
      $('p').load(function(){
         $('.grid').masonry();
      });
      $('img').load(function(){
           $('.grid').masonry();
        });
      $('.report_modal_link').fancybox();
      $("#report_form").validate({
          rules: {
              cause_report: { required: true }
          },
          messages: {
              cause_report: { required: "Please enter the reason" }
          },
          submitHandler: function (form) {
              try{
                $('#loader-wrapper').show();
                var reason_report = $('#cause_report').val();
                var hub_id = $('#hidden_hub_id').val();
                $.post('<?php echo base_url(); ?>hub/reportHub',{hub_id:hub_id,reason_report:reason_report},function(result){ 
                    $('#loader-wrapper').hide();
                    if(result=='1'){
                      alertify.success('Your report has successfully posted');
                      $('.fancybox-close').trigger('click');  
                      setHubId('');
                    }
                });
              }catch(e){
                alertify.error(e);
              }
          }
      });
      function resetTabs(){
          jQuery("#content_tabs > div").hide(); //Hide all content
          jQuery("#tabs a").attr("id",""); //Reset id's      
      }

      var myUrl = window.location.href; //get URL
      var myUrlTab = myUrl.substring(myUrl.indexOf("#")); // For mywebsite.com/tabs.html#tab2, myUrlTab = #tab2     
      var myUrlTabName = myUrlTab.substring(0,4); // For the above example, myUrlTabName = #tab

        jQuery(function(){
            jQuery("#content_tabs > div").hide(); // Initially hide all content
            jQuery("#tabs li:first a").attr("id","current"); // Activate first tab
            jQuery("#content_tabs > div:first").fadeIn(); // Show first tab content
            
            jQuery("#tabs a").on("click",function(e) {
                e.preventDefault();
                if (jQuery(this).attr("id") == "current"){ //detection for current tab
                 return       
                }
                else{             
                resetTabs();
                jQuery(this).attr("id","current"); // Activate this
                jQuery(jQuery(this).attr('name')).fadeIn(); // Show content for current tab
                }
            });

            for (i = 1; i <= $("#tabs li").length; i++) {
              if (myUrlTab == myUrlTabName + i) {
                  resetTabs();
                  jQuery("a[name='"+myUrlTab+"']").attr("id","current"); // Activate url tab
                  jQuery(myUrlTab).fadeIn(); // Show url tab content        
              }
            }
        });
        $('#post_desc').click(function(){
       $('#post_desc').val('');    
     }); 

      $('#share').click(function(){ 
        var post_desc = $('#post_desc').val();      
        if(post_desc==''){
          alertify.error("Please enter what's on your mind");
          $('#post_desc').focus();
          return false;
        }
        var shr_to = $('#shr_to').val();
        var tags = $('#tag').val();
        var user_type ='';
        var user_type = "<?php echo $user_type[0]['user_type']; ?>";
        
        if( (user_type == 'Free' && shr_to != 1) || (user_type == 'Free' && tags != null)){
          alertify.alert('This option is only available to premium members. <a href="<?php echo base_url();?>payment/paynow"> Upgrade </a> to access this feature and increase your reach');
        }else{
          $('#loader-wrapper').show();
          $.post('<?php echo base_url(); ?>member/hub_post',{'pm':post_desc,'st':shr_to,'tag':tags},function(res){
            $('#loader-wrapper').hide();
            if(res == 1){
              alertify.success('Content posted succesfully');
              $('#post_desc').val('');
              $('#tag').find('option').prop("selected", false);
              $('#shr_to').find('option').prop("selected", false);
              location.reload();
            }
          });
        }
      });

      $("#tag").removeAttr('disabled');
      
      $('#share_pic').click(function(){
        var about_the_img = $('#about_img').val();
        var share = $('#shr_to_pic').val();
        var tag = $('#tag_for_img').val();
        var img = $('#post_upload').attr('src');
        var user_type = "<?php echo $user_type[0]['user_type']; ?>";
        if( (user_type == 'Free' && share != 1) || (user_type == 'Free' && tag != null)){
          alertify.alert('This option is only available to premium members. <a href="<?php echo base_url();?>payment/paynow"> Upgrade </a> to access this feature and increase your reach');
        }else{
          $('#loader-wrapper').show();
          $.post('<?php echo base_url(); ?>member/postImage',{'imgFile':img,'share':share,'tag':tag,'about_the_img':about_the_img},function(result){          
            $('#loader-wrapper').hide();
            if(result == 1){
              alertify.success("Post updated");
              $('#about_img').val('');
              $('#post_upload').attr('src','#');
              $('#tag_for_img').find('option').prop("selected", false);
              $('#shr_to_pic').find('option').prop("selected", false);
              location.reload();
            }
          });
       } 

      });
    });
  function redirect_url(){
    $(location).attr('href','<?php echo base_url(); ?>hub/hubPublish');
    //window.open('<?php echo base_url(); ?>hub/hubPublish');
  }
  function readURLPostImage(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#post_upload').attr('src', e.target.result);
              }

              reader.readAsDataURL(input.files[0]);
          }
      }
 function showComments(hid){
    $("#"+hid+"_comments_list>div").removeClass('hid_comment');
    $('.grid').masonry();
 }
    function setHubId(hub_id){
      $('#hidden_hub_id').val(hub_id);
      $('#cause_report').val('');
    }
    function shared(hub_id){
      try{
        $('#loader-wrapper').show();
        $.post('<?php echo base_url(); ?>hub/shared',{'hub_id':hub_id},function(result){          
          $('#loader-wrapper').hide();
          if(result==1){            
            alertify.success('Request for shared was successfully done');
          }else{
          alert('This is embrassing.Some error occured');
          }
        });
      }catch(e){
          alertify.error(e);
      }  
    }
    function connectedToUser(curObj,conected_id){      
        try{
          $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>member/reqestForConnection',{'id':conected_id},function(result){
              //console.log(result);
              $('#loader-wrapper').hide();
              if(result==1){
                $(curObj).removeAttr("onclick");
                $(curObj).text('Request Send');
                alertify.success('Request Send');
              }else{
              alert('This is embrassing.Some error occured');
              }
            });
          }catch(e){
              alertify.error(e);
          }
    }
    function showHideDiv(id){
      var obj = document.getElementById(id);
        if (obj.style.display=="none"){
          obj.style.display='block';
        } else if(obj.style.display=="block"){
          obj.style.display='none';
        }
    }
    function manageLike(curObj,hub_id){
        if($(curObj).hasClass('active')){
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deleteLike',{'hub_id':hub_id},function(res){
                $('#loader-wrapper').hide();                
                $(curObj).removeClass('active');
                var like_count =  parseInt($('#'+hub_id).text()-1);              
                $('#'+hub_id).text(like_count);
                $('#'+hub_id+'_text').text(like_count==1||like_count==0?' like':' likes');
            });
        }else{            
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/postLike',{'hub_id':hub_id},function(res){              
                $('#loader-wrapper').hide();                
                $(curObj).addClass('active');
                var like_count = parseInt($('#'+hub_id).text())+parseInt(1);
                $('#'+hub_id).text(like_count);
                $('#'+hub_id+'_text').text(like_count==1||like_count==0?' like':' likes');
            });
                  
        }
    }
    function deletePost(hub_id){
      try{
        alertify.confirm("Are you sure you'd like to delete this post?", function (e) {
          if (e) {
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deletePost',{'hub_id':hub_id},function(res){
                if(res!=0){  
                  $('#loader-wrapper').hide(); 
                   location.reload();
                } 
            });
          }
        });
      }catch(e){
        alertify.error(e);
      }
    }
    function deleteComment(comment_id){
      try{
        alertify.confirm("Are you sure you’d like to delete this comment?", function (e) {
          if (e) {
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deleteComment',{'comment_id':comment_id},function(res){
                  if(res!=0){              
                    $('#'+comment_id+"_comment").remove();  
                    $('#loader-wrapper').hide(); 
                  } 
              });
          }
        });
      }catch(e){
        alertify.error(e);
      }
    }
    function postComments(hid){
        var comment = $('#'+hid+'_comments').val();
       // alert(comment);
        if(comment!= undefined){
          $('#loader-wrapper').show();
          $.post('<?php echo base_url(); ?>hub/postComment',{'hub_id':hid,'comment':comment},function(res){
            res = eval("(" + res + ")");
            $('#loader-wrapper').hide();           
                if(res.id!=0){ 
                    var user_image = res.user_image!=''?res.user_image:'profile-img.png';          
                    var html  = '<div class="comment-writing" id="'+res.id+'_comment">'    
                                        +'<div class="comment-writing-logo">'
                                          +'<img alt="" src="<?php echo base_url();?>uploads/profile_image/'+user_image+'">'
                                        +'</div>'
                                        +'<div class="comment-righting-text">'+
                                        res.comment
                                        +'<div class="comment-delet">'
                                      +'<a href="javascript:void(0)" onclick="deleteComment('+res.id+')"><i class="fa fa-trash fa-2"></i></a>'
                                      +'</div>'; 
                                        +'</div>'                                                    
                                      +'</div>';
                      $('#'+hid+'_comments_list').append(html);
                      $('#'+hid+'_comments').val('');                  
                   $('.grid').masonry(); 
                  //location.reload();
                } 
            });
        }else{
          alertify.error('Please enter your comment!');
        }    
    }
</script>