<?php $session_mid=$this->session->userdata['logged_in']['id']?>
<?php //print_r($hub); exit;?>
<div class="dashboard-wrap"> 
    <div class="row MAR-TOP25">
        <div class="hub-listing hub-details-list">
          <div class="hub-left">
            <?php $imageName = getProfileImage($hub['mid']); ?>
            <a href='<?php echo base_url(); ?>viewprofile/profile/<?php echo $hub["mid"]; ?>'>
              <img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $imageName!=''?$imageName:'profile-img.png'; ?>" alt=""/>
            </a>
            <a href='<?php echo base_url(); ?>viewprofile/profile/<?php echo $hub["mid"]; ?>'><h3><?php echo getMemberbusinName($hub['mid']); ?></h3></a>
          </div>
          <div class="right-hub">
        				<?php if($hub['hub_title']!=''){?><h3><?php echo $hub['hub_title']; ?></h3><?php } ?>
                  <div class="right-hub-back">
                    <?php if($hub['link'] != '') {?>
        						<div class="hub-img">
        						  <img id="hub_img" src="<?php echo base_url(); ?>uploads/hub_images/<?php echo $hub['link']; ?>" alt=""/>
                    </div>
                    <?php } ?>
                <div class="hub-content">
                  <p id="hub_content"><?php echo checkURLfromString($hub['content']); ?></p>
                  <a class="share-report" href="javascript:void(0)" onclick="showHideDiv('<?php echo $hub["hid"]?>_share-report-open')"> <i class="fa fa-ellipsis-v fa-2"></i></a>
                  <div class="share-report-sec" id="<?php echo $hub['hid']?>_share-report-open" style="display:none">
                    <ul>
                      <?php if($viewtype != 'own'){?>
                          <li><a href="javascript:void(0)" onclick="shared('<?php echo $hub['hid'];?>')">Share</a></li><?php }?>
                      <?php if($viewtype != 'own'){?>
                          <li><a class="edit-sec report_modal_link" href="#report_modal" onclick="setHubId('<?php echo $hub["hid"]?>')" >Report</i></a></li><?php }?>
                      <?php if($viewtype != 'own'){?>
                          <?php if(empty($hub['is_connected'])){?>
                            <li><a href="javascript:void(0)" onclick="connectedToUser(this,<?php echo $hub['mid']?>)">Connect</a></li>
                          <?php }else{?>                                        
                          <li>
                              <a href="javascript:void(0)">                                         
                                  <?php if($hub['is_connected']->is_accepted!='0'){?> 
                                  Connected
                                  <?php }else{?>
                                  Request send
                                  <?php }?>
                              </a>
                          </li>
                          <?php }?>
                      <?php }?>
                      <?php if($viewtype == 'own'){?>
                          <li><a href="javascript:void(0)" onclick="deletePost(<?php echo $hub['hid']?>)">Delete</a></li>
						  <?php if($hub['hub_title']==''){?>
                          <li><a href="javascript:void(0)" onclick="editPost('<?php echo $hub['link'] != ''?1:0?>')">Edit</a></li>
						  <?php }else{ ?>
                          <li><a href="<?php echo base_url(); ?>hub/hubPublishEdit/<?php echo $hub['hid']?>">Edit</a></li>
						<?php }?>  
                      <?php }?>  
                  </ul>
                </div>
              </div>
              <div class="comment-like">
                <?php if($hub['like'] == 1){?>
                    <a class="like active" href="javascript:void(0)" onclick="manageLike(this,<?php echo $hub['hid']?>)"> <i class="fa fa-heart fa-2"></i></a>
                <?php }else{?>
                    <a class="like" href="javascript:void(0)" onclick="manageLike(this,<?php echo $hub['hid']?>)"> <i class="fa fa-heart fa-2"></i></a>
                <?php }?>
                <div class="count-like">
                    <span style="color:#000" id="<?php echo $hub['hid']?>"><?php echo $hub['likes'];?></span>
                    <span style="color:#000" id="<?php echo $hub['hid']?>_text"><?php echo $hub['likes']==1?' like':' likes'?></span>
                </div>
              </div>
            </div>
            <div class="comment-post-wrap">
                <div class="comment-post hub-post-dynamic-height">
                    <div class="comment-read">
                        <div class="comment-writing-logo">
                          <img src="<?php echo base_url();?>uploads/profile_image/<?php echo $user['fornt_image']!=null ? $user['fornt_image']:'profile-img.png';?>" alt=""/>
                        </div>
                        <div class="comment-textarea">
                          <textarea id="<?php echo $hub['hid']?>_comments"></textarea>
                          <a class="comment" onclick="postComments(<?php echo $hub['hid']?>);">Post</a>
                        </div>           
                    </div>
                    <?php foreach($hub['comments'] as $comment){?>
                        <div class="comment-writing "> 
                        <?php if($comment['pic_id']!=NULL) { 
                          $pic = $comment['image_url'];
                        } else{
                          $pic = "profile-img.png";
                        }
                        //print_r($hub['comments']);
                        ?>   
                        <div class="comment-writing-logo" >
                            <img alt="" src="<?php echo base_url();?>uploads/profile_image/<?php echo $pic; ?>">
                            <div class="comment-head-title">
                                <?php echo getMemberbusinName($comment['mid']); ?>
                            </div>
                        </div>
                        <div class="comment-righting-text" >
                            <?php echo checkURLfromString($comment['comments']); ?>
                            <?php if($comment['mid'] == $session_mid){?>
                            <div class="comment-delet">
                                <a href="javascript:void(0)" onclick="deleteComment(<?php echo $comment['comm_id']?>)"><i class="fa fa-trash fa-2"></i></a>
                            </div>
                            <?php }?>
                        </div>            
                        </div>
                    <?php }?>       
                </div>
            </div>
          </div>
      </div>
    </div>
    <a href="#edit_publish_popup" id="edit_publish" type="button">
    <div id="edit_publish_popup"  class="upload-profile-photo" style="width:500px; display:none;">
      <div class="headding-pop">
        <h3>Edit Publish</h3>
        <?php if($hub['link'] != ''){?>
          <div class="change-btn-outer">
            <div class="change-btn">Change<input class="image_change_file" id="image_change" type="file" name="browse" value="change"/></div>
          </div>
        <?php }?>
      </div>
      <form id="edit_post" method="post" action="<?php echo base_url()?>hub/hubDetails/<?php echo $hubpostid.'/'.$hubid?>">
          <input type="hidden" value="<?php echo $hub['hid'];?>">
          <?php if($hub['link'] != ''){?>
          <div class="hub_image_edit">
            <img id="hub_edit_post_img" src="<?php echo base_url(); ?>uploads/hub_images/<?php echo $hub['link']; ?>" alt=""/>
            
          </div>  
          <?php }?>
          <div class="container-crop">
              <textarea id="post_edit_text"><?php echo checkURLfromString($hub['content']);?></textarea>
          </div>
          <div class="save-btn-outer">
            <input type="button" id="hub_edit_button" value="Save" class="save-btn">
          </div>
      </form>
    </div>  
</div>
 <script>
      function load() {
        $('p').each(function() {
		var $this = $(this);
		if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
			$this.remove();
		});
      }
      window.onload = load;
    </script>
<script>
//Preview & Update an image before it is uploaded
var image_change=0;
var img_base64 = '';
  function readURL_hub(input) {
      if (input.files && input.files[0]){
          var reader = new FileReader();
          reader.onload = function (e) {
              $('#show_hub_img').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
      }
  }
  $("#image_change").change(readImage);
  function readImage() {
      if ( this.files && this.files[0] ) {
          var FR = new FileReader();
          FR.onload = function(e) {
            image_change++;
            $('#hub_edit_post_img').attr( "src", e.target.result );
            //$('#base').text( e.target.result );
            img_base64 = e.target.result;
          };       
          FR.readAsDataURL( this.files[0] );
      }
  }
  $("#img_submit").change(function(){
      readURL_hub(this);
  });
  function editPost(type){
    $("#edit_publish").trigger("click");
  }
  $('#hub_edit_button').click(function(){  
      if($('#post_edit_text').val()==''){
          alertify.alert("Please enter some text");
          return false;
      }
      var hid = "<?php echo $hub['hid'];?>";
      $("#loader-wrapper").show();
      $.post('<?php echo base_url(); ?>hub/hubEditText',{edited_text:$('#post_edit_text').val(),hid:hid},function(result){ 
            if(result==1){
              if(image_change>0){
                  $.post('<?php echo base_url(); ?>hub/hubImgEdit',{'imgFile':img_base64,hid:hid },function(result_img_upload){         
                      if(result_img_upload==1){
                          $("#loader-wrapper").hide();
                          $('.fancybox-close').trigger('click');                          
                          $("#hub_img").attr('src', img_base64);
                          alertify.success("Hub Content Updated");
                      }
                  });                  
              }else{
                  $("#loader-wrapper").hide();
                  $('.fancybox-close').trigger('click');
                  alertify.success("Hub Content Updated");
              }              
              $("#hub_content").text($('#post_edit_text').val());              
            }
      });

      //$("#edit_post").submit();
  });
	$(document).ready(function(){
		
      $("#edit_publish").fancybox();
      $('#tag'). tokenize({placeholder:"Start typing your tags"});
      $('#tag').change(function(event) {
          if ($(this).val().length > 3) {
              alertify.error('You can only choose 3!');
              $(this).val(last_valid_selection);
          } else {
              last_valid_selection = $(this).val();
          }
      });	
      $('#del_publish_img').click(function(){
          $("#img_submit").val("");
          $('#show_hub_img').attr('src','');
      });
      $("#pub_post").validate({
          rules: {
              title: { required: true }
          },
          messages: {
              title: { required: "Please enter your hub title" }
          },
          submitHandler: function (form) {
              var imgVal = $('#img_submit').val(); 
              if(imgVal=='') 
              { 
                alertify.error("Please upload your file");
                return false; 
              }                
              form.submit();
              return true;
          }
      });
	});
</script>


<script type="text/javascript">
$('.del').click(function(){
    var cur_obj = this;
    alertify.confirm("Are you want to delete the post", function (e) {
        if (e) {
            var id = $(cur_obj).parent('li').attr('id');
            var url = '<?php echo base_url(); ?>hub/hub_delete';
            var obj = $(cur_obj);

            $.post(url,{'id':id},function(res){
                if(res == 1){
                    $(obj).parent('li').remove();
                    alertify.success('Publish deleted');
                    window.location.reload();
                }else{
                    alertify.error('Error occured while deleting. Please try again');
                }
            });
        }
    });
});
  function readURLPostImage(input) {
          if (input.files && input.files[0]) {
              var reader = new FileReader();

              reader.onload = function (e) {
                  $('#post_upload').attr('src', e.target.result);
              }
              reader.readAsDataURL(input.files[0]);
          }
      }
 function showComments(hid){
    $("#"+hid+"_comments_list>div").removeClass('hid_comment');
    $('.grid').masonry();
 }
    function setHubId(hub_id){
      $('#hidden_hub_id').val(hub_id);
      $('#cause_report').val('');
    }
    function shared(hub_id){
      try{
        $('#loader-wrapper').show();
        $.post('<?php echo base_url(); ?>hub/shared',{'hub_id':hub_id},function(result){          
          $('#loader-wrapper').hide();
          if(result==1){            
            alertify.success('Request for shared was successfully done');
          }else{
          alert('This is embrassing.Some error occured');
          }
        });
      }catch(e){
          alertify.error(e);
      }  
    }
    function connectedToUser(curObj,conected_id){      
        try{
          $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>member/reqestForConnection',{'id':conected_id},function(result){
              //console.log(result);
              $('#loader-wrapper').hide();
              if(result==1){
                $(curObj).removeAttr("onclick");
                $(curObj).text('Request Send');
                alertify.success('Request Send');
              }else{
              alert('This is embrassing.Some error occured');
              }
            });
          }catch(e){
              alertify.error(e);
          }
    }
    function showHideDiv(id){
      var obj = document.getElementById(id);
        if (obj.style.display=="none"){
          obj.style.display='block';
        } else if(obj.style.display=="block"){
          obj.style.display='none';
        }
    }
    function manageLike(curObj,hub_id){
        if($(curObj).hasClass('active')){
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deleteLike',{'hub_id':hub_id},function(res){
				
                $('#loader-wrapper').hide();                
                $(curObj).removeClass('active');
                var like_count =  parseInt(($('#'+hub_id).text()!=''?$('#'+hub_id).text():0)-1);              
                $('#'+hub_id).text(like_count);
                $('#'+hub_id+'_text').text(like_count==1||like_count==0?' like':' likes');
            });
        }else{            
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/postLike',{'hub_id':hub_id},function(res){
                $('#loader-wrapper').hide();                
                $(curObj).addClass('active');				
                var like_count = parseInt($('#'+hub_id).text()!=''?$('#'+hub_id).text():0)+parseInt(1);				
                $('#'+hub_id).text(like_count);
                $('#'+hub_id+'_text').text(like_count==1||like_count==0?' like':' likes');
            });                  
        }
    }
    function deletePost(hub_id){
      try{
        alertify.confirm("Are you sure you'd like to delete this post?", function (e) {
          if (e) {
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deletePost',{'hub_id':hub_id},function(res){
                if(res!=0){  
                  $('#loader-wrapper').hide(); 
                  // location.reload();
				    $(location).attr('href','<?php echo base_url(); ?>hub/morehub');
                } 
            });
          }
        });
      }catch(e){
        alertify.error(e);
      }
    }
    function deleteComment(comment_id){
      try{
        alertify.confirm("Are you sure you’d like to delete this comment?", function (e) {
          if (e) {
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deleteComment',{'comment_id':comment_id},function(res){
                  if(res!=0){              
                    $('#'+comment_id+"_comment").remove();  
                     $('#loader-wrapper').show();
					location.reload();
                  } 
              });
          }
        });
      }catch(e){
        alertify.error(e);
      }
    }
    function postComments(hid){
        var comment = $('#'+hid+'_comments').val();
       // alert(comment);
        if(comment!= undefined){
          $('#loader-wrapper').show();
          $.post('<?php echo base_url(); ?>hub/postComment',{'hub_id':hid,'comment':comment},function(res){
            res = eval("(" + res + ")");
            $('#loader-wrapper').hide();           
                if(res.id!=0){ 
                    var user_image = res.user_image!=''?res.user_image:'profile-img.png';          
                    var html  = '<div class="comment-writing" id="'+res.id+'_comment">'    
                                        +'<div class="comment-writing-logo">'
                                          +'<img alt="" src="<?php echo base_url();?>uploads/profile_image/'+user_image+'">'
                                        +'</div>'
                                        +'<div class="comment-righting-text">'+
                                        res.comment
                                        +'<div class="comment-delet">'
                                      +'<a href="javascript:void(0)" onclick="deleteComment('+res.id+')"><i class="fa fa-trash fa-2"></i></a>'
                                      +'</div>'; 
                                        +'</div>'                                                    
                                      +'</div>';
                      $('#'+hid+'_comments_list').append(html);
                      $('#'+hid+'_comments').val('');                  
                   $('.grid').masonry(); 
                  location.reload();
                } 
            });
        }else{
          alertify.error('Please enter your comment!');
        }    
    }
</script>
				   
				   


    