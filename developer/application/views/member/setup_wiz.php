
        <div class="main" id="widzart_one_section">
        <section class="cd-section visible top-section" >
            <div>
                <div class="container">
                    <div class="center-box top-center-box">
    							<!-- <div class="hero">
    								<img class="hideme" src="img/placeholder.png" />
    							</div> -->
    					<div class="center-box-hght">
    						<div data-bind="visible:part_one">
    							<h2 class="first">Are you signing up....</h2>
    							<div class="middle-signup-container signup-step">
    								<ul>
    									<li><input type="radio" name="areyoua" value='business'  data-bind="checked:buisness_type"><span> A business</span></li>
    									<li><input type="radio" name="areyoua" value='Yourself, as a self-employed professional' data-bind="checked:buisness_type"><span> Yourself, as a self-employed professional </span></li>
    									<li><button id="a_SignUpSubmit" class="button" type="button" onclick="nextWidzard(1)">Next</button></li>
    								</ul>
    							</div>
    						</div>
    						<div data-bind="visible:part_two">
                            <div class="signup-two">
    							<h2 class="first">Tell us about your business</h2>
    							<div class="form-min-hgt-wrap">
    								<div class="middle-signup-container">
    									<ul>
    										<li><input name="tb_email" data-bind="value:buiz_name" type="text" placeholder="Business/ Individual name" class="txt" required></li>
                                            <li><input name="tb_first_name" data-bind="value:first_name" type="text" placeholder="First Name" class="txt" required></li>
                                            <li><input name="tb_last_name" data-bind="value:last_name" type="text" placeholder="Last Name" class="txt" required></li>
                                            <li><input name="tb_office_name" data-bind="value:office_name" type="text" placeholder="Office Name" class="txt" required></li>
    										<li><select  name="select1" 
    														data-bind="options: country_list,
    														optionsText: 'country_name',
    														optionsValue: 'id',
    														value: selected_country,
    														optionsCaption: '- Select Country -'"></select>
    										</li>						
                                            <li><input name="tb_email" type="text" data-bind="value:city" placeholder="City/Town" class="txt" required></li>
    										<li><input name="tb_email" type="text"  data-bind="value:zipcode" placeholder="Post Code" class="txt" required></li>
    										<li><button id="a_SignUpSubmit" class="button" type="button"  onclick="nextWidzard(2)">Next</button></li>
    									</ul>
    								   <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(2)">Back to Previous Question</a></div>
    							    </div>
    						    </div>  
                                </div>
    						</div>
    						<div data-bind="visible:part_three">
                            <div class="signup-three">
    							<h2 class="first">What sort of business do you service?</h2>
    							<div class="business_tool-tips">
    								<div class="tool-tips-block">This helps us to understand what sort of businesses may be interested in you</div>
    							</div>
    							<div class="business-listing-wrap">
    								<div class="business-img-listing-wrap step-three-scroll">
    									<ul class="business-img-listing biz_list" data-bind="foreach:top_list">          
    										<li onclick="setActive(this,'biz_list')">
    											<a href="javascript:void(0);" data-bind="value:top_code,click:$root.getChildList">
                                                    <!--<img src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>-->
                                                    <img data-bind="attr:{src:profile_image!=null?'<?php echo base_url();?>uploads/profile_image/'+profile_image:'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
    												<div class="business-listing-heading">
    													<h4 data-bind="text:top_name">Capital Market</h4>
    												</div>
    											</a>
    										</li>										
    									</ul>
    								</div>
    								<p class="business-message">If your business covers more than one industry, don’t worry! You’ll be able to add more later on if you sign up to premium membership</p>
    							</div>
    							<div class="form-min-hgt-wrap">
    								<div class="middle-signup-container">
    									<ul>
    										<li> <button type="submit" class="button" id="a_SignUpSubmit" onclick="nextWidzard(3)">Next</button></li>
    									</ul>
    									<div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(3)">Back to Previous Question</a></div>
    								</div>
    							</div>
                                </div>
    						</div>    
                            <div data-bind="visible:part_four">
                                <h2 class="first">What best describes your sector?</h2>
                                 <div class="business-listing-wrap">
                                    <div class="business-select-wrap">
                                        <div class="business-img-left active">
                                            <a href="javascript:void(0)">
                                                <!--<img src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>-->
                                                <img data-bind="attr:{src:label_sel_sector_img()!=null?'<?php echo base_url();?>uploads/profile_image/'+label_sel_sector_img():'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                <div class="business-listing-heading"> 
                                                <h4 data-bind="text:selected_top">Consumer Durables</h4>
                                             </div>
                                            </a>
                                        </div>
                                        <div class="business-select-text">
                                            You selected <span data-bind="text:label_sel_sector"></span> in your previous selection. Please select your business sector classification from the below.
                                        </div>
                                    </div>
                                    <div class="business-img-listing-wrap">
                                        <ul class="business-img-listing biz_child_list" data-bind="foreach:child_top_list">          
                                            <li onclick="setActive(this,'biz_child_list')">
                                                <a href="javascript:void(0);" data-bind="value:sector_code,click:$root.selectSetorCode">
                                                    <img data-bind="attr:{src:profile_image!=null?'<?php echo base_url();?>uploads/profile_image/'+profile_image:'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                    <!--<img src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>-->
                                                    <div class="business-listing-heading">
                                                        <h4 data-bind="text:sector_name">Capital Market</h4>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="middle-signup-container">
                                        <ul>
                                            <li>
                                                <button type="submit" class="button" id="a_SignUpSubmit" onclick="nextWidzard(4)">Next</button>
                                            </li>
                                        </ul>
                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(4)">Back to Previous Question</a></div>
                                    </div>                                
                                </div>	
                            </div> 
                            <div data-bind="visible:part_five">
                                <h2 class="first">What best describes your sub-sector?</h2>
                                <div class="business-listing-wrap">
                                    <div class="business-select-wrap">
                                        <div class="business-img-left active">
                                            <a href="javascript:void(0);">
                                                <!--<img src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>-->
                                                <img data-bind="attr:{src:label_sel_sub_sector_img()!=null?'<?php echo base_url();?>uploads/profile_image/'+label_sel_sub_sector_img():'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                <div class="business-listing-heading"> 
                                                    <h4 data-bind="text:selected_sector_name">Consumer Durables</h4>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="business-select-text">
                                            You have selected <span data-bind="text:label_sel_sub_sector"></span> in previous selection. So please select the item which best describes your sub-sector.
                                        </div>
                                    </div>
                                    <div class="business-content-wrap">
                                        <ul class="business-content-listing sub_sector_list" data-bind="foreach:sub_sector_list">          
                                            <li onclick="setActive(this,'sub_sector_list')"><a href="javascript:void(o);" data-bind="value:subsector_code,text:subsector_name,click:$root.getBuisnessList"> #1Lorem ipsum</a></li>                                   
                                        </ul>
                                    </div>
                                    <div style="clear:both;"></div>
                                    <div class="middle-signup-container">
                                        <ul>
                                            <li>
                                                <button type="submit" class="button" id="a_SignUpSubmit" onclick="nextWidzard(5)">Next</button>
                                            </li>
                                        </ul>
                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(5)">Back to Previous Question</a></div>
                                    </div>
                                </div>                            		
                            </div>
                            <div data-bind="visible:part_six">
                                <h2 class="first">What best describes your business type?</h2>
                                <div class="business-listing-wrap">
                                    <div class="business-select-wrap">                                    
                                        <div class="business-select-text">
                                            You have selected <span data-bind="text:selected_subsector_name"></span> in previous selection. So please select the item which best describes your sub-sector.
                                        </div>
                                    </div>
                                    <div class="business-content-wrap">
                                        <ul class="business-content-listing sec_buiz_type" data-bind="foreach:buisness_list">          
                                            <li onclick="setActive(this,'sec_buiz_type')"><a href="javascript:void(o);" data-bind="value:bussiness_code,text:bussiness_name,click:$root.setBuisDetail"> #1Lorem ipsum</a></li>                                   
                                        </ul>
                                    </div>
                                    <div style="clear:both;"></div>
                                    <div class="middle-signup-container">
                                        <ul>
                                            <li>
                                                <button type="submit" class="button" id="a_SignUpSubmit" data-bind="click:saveWizSecond" onclick="nextWidzard(6)">Next</button>
                                            </li>
                                        </ul>
                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(6)">Back to Previous Question</a></div>
                                    </div>
                                </div>                                  
                            </div>   
                            <div data-bind="visible:part_seven">
                                <h2 class="first">Congratulations!</h2>
                                <!--<div class="form-min-hgt-wrap activate-mail-message">An Email has been forwarded to your accont to activate your account</div>-->
                                <div class="form-min-hgt-wrap">
                                    <p>You’re now registered on Arlians, the growth network! Please check your emails to verify your address.Please note that it may be in your spam box. If you did not receive the verification email  <a href="javascript:void(0);" data-bind="click:resendActivationRequestLink" style="color:#ed5664">click here</a> to resend it</p>
                                </div>                           
                            </div>
    					</div>
                   </div> 
               </div>
                <!--<div class="bottom-button">
                    <p><a href="javascript:void(0);" id="arlians-left">What is Arlians?</a>
                        <br><i class="fa fa-chevron-down"></i>
                    </p>
                </div>-->
            </div>
        </section>
    </div>
</div>

    <!--<div class="form-min-hgt-wrap footer-sec">
		<div class="middle-social-section">
			<div class="term-privacy-left">
			  <ul>
				 <li><a href="#">Terms</a></li>
				<li><a href="#">Privacy</a></li>
				<li><a href="#">Support</a></li>
			  </ul>
			</div>

			<div class="social-icon-right">
			   <ul>
			   <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/facebook.png" /></a></li>
				<li><a href="#"><img src="<?php echo base_url(); ?>assets/images/twiiter.png" /></a></li>
				 <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/linkdin.png" /></a></li>
				</ul>
			</div>
		</div>
	</div>-->
	
<script>
    var selected_list=null;
    var sub_sector =null;
    var buisness_det=null;
	var vmWizardOne={
		buisness_type:ko.observable(''),
		part_one:ko.observable(true),
		part_two:ko.observable(false),
		part_three:ko.observable(false),
		part_four:ko.observable(false),
		part_five:ko.observable(false),
		part_six:ko.observable(false),
        part_seven:ko.observable(false),
        top_list:ko.observableArray(),
        skip:ko.observable(''),
        zipcode:ko.observable(''),
        selected_top:ko.observable(),
        child_top_list:ko.observableArray(),
        sub_sector_list:ko.observableArray(),
        buisness_list:ko.observableArray(),
		buiz_name:ko.observable(''),
        first_name:ko.observable(''),
        last_name:ko.observable(''),
        selected_sector_id:ko.observable(),
        selected_sector_name:ko.observable(),
        office_name:ko.observable(),
        selected_subsector_code:ko.observable(),
        selected_subsector_name:ko.observable(),
        selected_subsector_id:ko.observable(''),
        label_sel_sub_sector_img:ko.observable(''),
        selected_subsector_name:ko.observable(),
		city:ko.observable(''),
        label_sel_sector:ko.observable(''),
        label_sel_sub_sector:ko.observable(''),
        label_sel_sector_img:ko.observable(''),
		country_list:ko.observableArray(),
		selected_country:ko.observable(),
        resendActivationRequestLink:function(){
            $.post('<?php echo base_url(); ?>member/resendActivationRequestLink',{},function(result){                
                if(result){
                    alertify.alert('An email has been forwarded to your email address to validate your account.');
                }else{
                    alertify.alert('Some error occured please try again!');
                }
            }); 
        },	
        getChildList:function(){            
            selected_list = this; 
            vmWizardOne.selected_top(selected_list.top_name);
            $.post('<?php echo base_url(); ?>category/get_sector_list',{top_link_id:selected_list.top_code},function(result_child_list){ 
                result_child_list = eval("(" + result_child_list + ")");
                vmWizardOne.child_top_list(result_child_list);
                if(vmWizardOne.child_top_list().length==1){                    
                    $('.biz_child_list>li>a').trigger("click");
                    vmWizardOne.skip(4);
                }
            });  
        },
        selectSetorCode:function(){         
            sub_sector =this;    
            vmWizardOne.selected_sector_id(this.sector_code);
            vmWizardOne.selected_sector_name(this.sector_name)
            $.post('<?php echo base_url(); ?>category/get_subsector_list',{sector_id:sub_sector.sector_code,top_link_list_id:selected_list.top_code},function(result_subchild_list){ 
                result_subchild_list = eval("(" + result_subchild_list + ")");
                vmWizardOne.sub_sector_list(result_subchild_list);
                if(vmWizardOne.sub_sector_list().length==1){                    
                    $('.sub_sector_list>li>a').trigger("click");
                    vmWizardOne.skip(5);
                }
            });
        },
        getBuisnessList:function(){
            vmWizardOne.selected_subsector_id(this.subsector_code);
            vmWizardOne.selected_subsector_name(this.subsector_name);
            $.post('<?php echo base_url(); ?>category/get_bussiness_list',{subsector_id:vmWizardOne.selected_subsector_id(),sector_id:sub_sector.sector_code,top_link_list_id:selected_list.top_code},function(result_subbiz_list){ 
                result_subbiz_list = eval("(" + result_subbiz_list + ")");
                vmWizardOne.buisness_list(result_subbiz_list);
                if(vmWizardOne.buisness_list().length==1){                    
                    $('.sec_buiz_type>li>a').trigger("click");
                    vmWizardOne.skip(6);
                    //vmWizardOne.saveWizSecond();
                }
            });
        },
        setBuisDetail:function(){
               buisness_det =this; 
               console.log(buisness_det);
        },
        saveWizSecond:function(){
            if(buisness_det!=null){
                $.post('<?php echo base_url(); ?>category/setup_wiz_one_part_two',{top_link_id:selected_list.top_code,sector_id:sub_sector.sector_code,subsector_id:vmWizardOne.selected_subsector_id(),bussiness_id:buisness_det.bussiness_code},function(result_data){                                     
                    console.log(result_data);
                    if(result_data=='active'){
                       window.location= "dashboard";
                    }else if(result_data=='not_inserted'){
                           alertify.alert("This is embrassing.Something error occured.Please try again!");
                    }else{
                            $(".top-section").removeClass('cd-inner-listing');
                            vmWizardOne.part_seven(true);
                            vmWizardOne.part_six(false);
                            vmWizardOne.part_five(false);
                            vmWizardOne.part_four(false);
                            vmWizardOne.part_three(false);
                        }
                    });
            }else{
                  alertify.alert("Please select your business type.");  
            }
            
        }
		
	}
	function nextWidzard(part){        
        part = vmWizardOne.skip()!=''?vmWizardOne.skip():part;
        if(part==1){
            if(vmWizardOne.buisness_type()!=''){
                vmWizardOne.part_two(true);
                vmWizardOne.part_one(false);
            }else{
                alertify.alert('Please Select your business type');
            }
        }
        if(part==2){
            var buisness_type=vmWizardOne.buisness_type()=='business'?'Buisness':'Yourself, as a self-employed professional';

            if(buisness_type=='Buisness'){
                if(vmWizardOne.buiz_name()==''){
                    alertify.alert("Please enter Buisness name");    return false;
                }
            }
            if(vmWizardOne.first_name()==''){
                alertify.alert("Please enter First name");    return false;
            }
            if(vmWizardOne.last_name()==''){
                alertify.alert("Please enter Last name");    return false;
            }            
            if(vmWizardOne.selected_country()=='' || vmWizardOne.selected_country()==undefined){
                alertify.alert("Please select Country");    return false;
            }    
            if(vmWizardOne.city()==''){
                alertify.alert("Please enter City name");    return false;
            }  
            if(vmWizardOne.office_name()==''){
                alertify.alert("Please enter Office name");    return false;
            }
            /*if(vmWizardOne.zipcode()==''){
                alertify.alert("Please enter your zipcode");    return false;
            }*/
            $(".top-section").addClass('cd-inner-listing');

            
            $.post('<?php echo base_url(); ?>member/setup_wiz_one_part_one',{bussinesstype:buisness_type,bussinessname:vmWizardOne.buiz_name(),first_name:vmWizardOne.first_name(),last_name:vmWizardOne.last_name(),country:vmWizardOne.selected_country(),city:vmWizardOne.city(),zipcode:vmWizardOne.zipcode(),office_name:vmWizardOne.office_name()},function(result){ 
                
            });
            vmWizardOne.part_three(true);
            vmWizardOne.part_two(false);
        }
        if(part==3){
            if(selected_list==null){
                alertify.alert("Please select a business type"); return false;
            }else{
                vmWizardOne.label_sel_sector(selected_list.top_name);
                vmWizardOne.label_sel_sector_img(selected_list.profile_image);
                $(".top-section").addClass('cd-inner-listing');
                vmWizardOne.part_four(true);
                vmWizardOne.part_three(false);
            }
        }
        if(part==4){  
            if(sub_sector==null){
                alertify.alert("Please select a sector"); return false;
            }else{
                vmWizardOne.label_sel_sub_sector(sub_sector.sector_name);
                vmWizardOne.label_sel_sub_sector_img(sub_sector.profile_image);
                vmWizardOne.part_three(false);
                vmWizardOne.part_five(true);
                vmWizardOne.part_four(false);
            }
        }
        if(part==5){
            if(vmWizardOne.selected_subsector_id()==null){
                alertify.alert("Please select a sub sector"); return false;
            }else{   
                vmWizardOne.part_three(false);
                vmWizardOne.part_six(true);
                vmWizardOne.part_four(false);
                vmWizardOne.part_five(false);
            }
        }
        if(part==6){
            vmWizardOne.saveWizSecond();
        }        
    }
	function previousWidzart(part){
		if(part==2){
			vmWizardOne.part_one(true);
			vmWizardOne.part_two(false);
		}
        if(part==3){
            $(".top-section").removeClass('cd-inner-listing');
            vmWizardOne.part_two(true);
            vmWizardOne.part_three(false);
        }
        if(part==4){
            vmWizardOne.part_three(true);
            vmWizardOne.part_four(false);
        }
        if(part==5){
            vmWizardOne.part_four(true);
            vmWizardOne.part_five(false);
        }
        if(part==6){
            vmWizardOne.part_five(true);
            vmWizardOne.part_six(false);
        }
	}
	$(function(){
        //vmWizardOne.part_three(true);
		try{
		$.post('<?php echo base_url(); ?>member/get_country',{},function(result){ 
			result = eval("(" + result + ")");
            //console.log(JSON.parse(result));
			vmWizardOne.country_list(result.country);
        });
		}catch(e){
			alertify.alert(e);
		}
        try{
        $.post('<?php echo base_url(); ?>category/get_top_list',{},function(result){ 
            result = eval("(" + result + ")");
            //console.log(JSON.parse(result));
            vmWizardOne.top_list(result);
        });
        }catch(e){
            alertify.alert(e);
        }

	});
    function setActive(curObj,cur_class){        
        $("."+cur_class+">li").removeClass('active');
        $(curObj).addClass('active');
    }
	ko.applyBindings(vmWizardOne, $("#widzart_one_section")[0]);
</script>


    
