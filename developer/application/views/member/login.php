  
    <div class="wrapper login-wrapper">
    <div class="main" id="user_login_section">
    <section class="cd-section visible top-section">
        <div>
            <div class="container">
                <div class="center-box top-center-box">
                	<!-- <div class="hero">
                        <img class="hideme" src="img/placeholder.png" />
                    </div> -->
                    <div class="center-box-hght">
                    <h2 class="first">Connecting businesses. Unlocking growth</h2>
                    <input class="err_msg" type="hidden" value="<?php if(!empty($error_message)){echo 1;}else{echo 0;} ?>">
                    <div id="err" class="error" style="color:red;"><?php if(!empty($error_message))echo $error_message;?></div>
                     <div class="form-min-hgt-wrap">
                    <div class="middle-signup-container">
                        <form id='login' name='login' method='post' data-bind="visible:show_login_frm" action='<?php echo base_url(); ?>member/login'>
                            <ul>
                                <li>
                                    <input type='email' placeholder='User Name' name='data[email]' class='txt' required>
                                </li>
                                <li>
                                    <input type='password' placeholder='Password' name='data[pass]' class='txt' required>
                                </li>
                                <li>
                                    <input id="a_SignUpSubmit" type='submit' name='data[loginsubmit]' value='Login' class='button'>
                                </li>
                                <a href="javascript:void(0);" onclick="showForgetPassword();" class="forget-password">Forgotten your password?</a>
                            </ul>
                        </form>
                        <form class='form_forget_pass' push data-bind="visible:show_forget_password_section">
                            <ul>
                                <li>
                                    <input type='email' name='forgrt_password_email' data-bind="value:forgrt_password_email"  class='txt' placeholder='Email address' email required>
                                </li>
                                <li>
                                    <button id="sub_member" class="button" type="button" onclick="forgetPassFrmSubmit();">Find your Account</button>
                                    <button class="button" data-bind="click:showSignUP" type="button">Back</button>                                                    
                                </li>
                            </ul>
                        </form> 
                    </div>
                    <div class="middle-social-section">
                    <div class="term-privacy-left">
                      <ul>
                         <li><a href="#">Terms</a></li>
                        <li><a href="#">Privacy</a></li>
                        <li><a href="#">Support</a></li>
                      </ul>
                    </div>

                    <div class="social-icon-right">
                   <ul>
                
                   <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/facebook.png" /></a></li>
                    <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/twiiter.png" /></a></li>
                     <li><a href="#"><img src="<?php echo base_url(); ?>assets/images/linkdin.png" /></a></li>
                    </ul>
                    </div>
                    </div>
                </div>
            </div>

                </div>
            </div>
            <div class="bottom-button">
                 <p><a href="javascript:void(0);" id="arlians-left">What is Arlians?</a>
                    <br><i class="fa fa-chevron-down"></i>
                </p>
            </div>
        </div>
    </section>

    <section id="down-1" class="cd-section cd-section-two">
        <div>

            <div class="container">
                <div class="center-box">
                    <h2>Arlians is a simple business tool designed to help you grow</h2>
                    <h4>Customers | Suppliers | Alliances | Buyers | Investors </h4>
                    <p>Whatever ‘growth’ means to you from acquiring a new customer, to forming an alliance, Arlians will be with you every step of the way. Create your profile and then sit back, relax and watch us match you to the right connections.
                        From there it’s up to you… it really is that simple.</p>
					<div class="hero">
                        <img class="hideme" src="<?php echo base_url(); ?>assets/images/ipad-1.png" />
                    </div>
                    
                    

                </div>
            </div>
        </div>
    </section>

    <section id="down-2" class="cd-section">
        <div>
            <div class="container">
                <div class="center-box">
                    <h2>Arlians is so much more than just a network. <br>It’s a market place</h2>
                    <p>

                        Discover opportunities to grow. Connect with businesses and form alliances. Learn from data and insights. Share your news with everyone. Collaborate with others and join forces

                    </p>
                    <div id="large-header" class="large-header">
                        <canvas id="demo-canvas"></canvas>
                    </div>

                </div>

            </div>

        </div>
    </section>

    <section id="down-3" class="cd-section">
        <div>
            <div class="container">
                <div class="center-box">
                    <h2>It’s for everyone</h2>

                    <p>From local bakeries to global banks, little corner shops to giant consultancies, old manufacturers to new media agencies… Arlians is for any business looking to grow.

                    </p>

                    <div class="circle-container">

                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-1.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-2.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-3.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-4.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-5.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-6.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-7.jpg" />
                        </div>

                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-1.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-2.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-3.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-4.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-5.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-6.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-7.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-1.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-2.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-3.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-4.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-5.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-6.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-7.jpg" />
                        </div>

                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-5.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-6.jpg" />
                        </div>
                        <div class="circle">
                            <img src="<?php echo base_url(); ?>assets/images/small-7.jpg" />
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="down-4" class="cd-section">
        <div>
            <div class="container">
                <div class="center-box">
                   <h2>Connecting businesses. Unlocking growth</h2>
                   <div id="err" class="error" style="color:red;"><?php if(!empty($error_message))echo $error_message;?></div>
                    <div class="middle-signup-container min-height-zero">                    
                        <form id='login' name='login' method='post' data-bind="visible:show_login_frm" action='<?php echo base_url(); ?>member/login'>
                        <ul>
                            <li>
                                <input type='email' placeholder='User Name' name='data[email]' class='txt' required>
                            </li>
                            <li>
                                <input type='password' placeholder='Password' name='data[pass]' class='txt' required>
                            </li>
                            <li>
                                <input id="a_SignUpSubmit" type='submit' name='data[loginsubmit]' value='Login' class='button'>
                            </li>
                            <a href="javascript:void(0);" onclick="showForgetPassword();" class="forget-password">Forgotten your password?</a>
                        </ul>
                        </form> 
                        <form class='form_forget_pass' push data-bind="visible:show_forget_password_section">
                            <ul>
                                <li>
                                    <input type='email' name='forgrt_password_email' data-bind="value:forgrt_password_email"  class='txt' placeholder='Email address' email required>
                                </li>
                                <li>                                                    
                                    <button id="sub_member" class="button" type="button" onclick="forgetPassFrmSubmit()">Find your Account</button>
                                    <button class="button" data-bind="click:showSignUP" type="button">Back</button>
                                </li>
                            </ul>
                        </form>                   
                    </div>
                    <p>Register now for updates and you’ll get the opportunity for exclusive free access to premium service.
                        <br>Together, we can change the way businesses connect and collaborate.
                    </p>
                </div>
            </div>
        </div>
    </section>

</div>
</div>

  <!--   <nav>
        <ul class="cd-vertical-nav">
            <li><a href="#0" class="cd-prev inactive">Next</a>
            </li>
            <li><a href="#0" class="cd-next">Prev</a>
            </li>
        </ul>
    </nav> -->
    <!-- .cd-vertical-nav -->

        
        
        <!-- Resource jQuery -->
        <script type="text/javascript">
            var vmLogin ={
                show_login_frm:ko.observable(true),
                show_forget_password_section:ko.observable(false),
                forgrt_password_email:ko.observable(''),
                showSignUP:function(){
                    vmLogin.show_login_frm(true);
                    vmLogin.show_forget_password_section(false);
                },
                showForgetPassword:function(){
                    vmLogin.show_login_frm(false);
                    vmLogin.show_forget_password_section(true);
                }
            }
            jQuery(document).ready(function(){
                jQuery(".main").onepage_scroll({
                    sectionContainer: "section",
                    responsiveFallback: 600,
                    loop: true
                });
            });

            jQuery(document).ready(function(){    
                if($('.err_msg').val()=='0'){
                    $('.error').hide();
                }
                jQuery('#arlians-left').click(function(){
                    jQuery('ul.onepage-pagination li a').each(function(){
                        if(jQuery(this).attr('data-index')=='2'){
                            jQuery(this).click();
                        }
                    });
                });
            });
            function validEmail(v) {
                    var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
                    return (v.match(r) == null) ? false : true;
            }
            function showForgetPassword(){
                vmLogin.show_login_frm(false);
                vmLogin.show_forget_password_section(true);
            }
            function forgetPassFrmSubmit(){
                if(vmLogin.forgrt_password_email()==''){
                    $('.error').show();
                    $('#err').show();               
                    $('.error').html("Email address cannot be blank");
                    $('#err').html("Email address cannot be blank"); 
                    return false;
                }
                var check_valid_email = validEmail(vmLogin.forgrt_password_email());        
                if(check_valid_email==true){
                    $.post('<?php echo base_url(); ?>member/changepassword',{data:vmLogin.forgrt_password_email()},function(res){            
                        if(res == "1"){
                            $('.error').show();
                            $('#err').show();
                            $('.error').html("An email has been forwarded to your email address for reset your password");
                            $('#err').html("An email has been forwarded to your email address for reset your password");
                            vmLogin.forgrt_password_email('');
                        } else{
                            $('#err_last').show();
                            $('#err').show();               
                            $('#err_last').html("Your email address has not been registered!");
                            $('#err').html("Your email address has not been registered!");
                        }
                    });
                }else{
                    $('.error').show();
                    $('#err').show();               
                    $('.error').html("Enter a valid Email address");
                    $('#err').html("Enter a valid Email address"); 
                }
            }
            ko.applyBindings(vmLogin, $("#user_login_section")[0]);
        </script> 
    
