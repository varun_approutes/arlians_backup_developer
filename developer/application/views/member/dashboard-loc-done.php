<div class="row">
    <div class="col-md-9">
        <div class="col-md-6 col-sm-6">
            <div class="row" ng-controller="DashboardHub as hub">
                <div class="col-md-12" ng-show="show_default_hub">
                    <div class="box post_update">
                        <div class="post-header">
                            <ul class="post_link">
                                <li><a href="javascript:void(0)"><i class="fa fa-edit"></i> Status</a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-photo" ng-click="showPublishSection()"></i>Photos / <i class="fa fa-video-camera"></i> Videos</a></li>
                                <!--<li><a href="<?php echo base_url() ?>hub/hubPublish"><i class="fa fa-sign-out"></i> Publish</a></li>-->
                                <li><a href="javascript:void(0);"  ng-click="showPublishSection()"><i class="fa fa-sign-out"></i> Publish</a></li>

                            </ul>
                        </div>
                        <div class="box-body">
                            <form>                     
                                <textarea class="form-control post_field" placeholder="What’s up in your mind?" ng-model="text_in_mind"></textarea>                    
                            </form>
                        </div>
                        <div class="post_footer clearfix">
                            <span style="color:#fff" ng-bind="show_selected">Hello world</span>
                            <!--<ul class="post_footerlink">
                                                            <li><a href="#"><i class="fa fa-user-plus"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-video-camera"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-photo"></i></a></li>
                                                        </ul>-->
                            <div class="post_footebtn pull-right">

                                <div class="btn-group post_footerdrpbtn">
                                    <i class="fa fa-gears pull-left"></i>
                                    <button aria-expanded="false" data-toggle="dropdown" class="btn  btn-sm dropdown-toggle">Share with</button>
                                    <ul role="menu" class="dropdown-menu pull-right">                                        
                                        <li><a href="javascript:void(0);" ng-click="select(list)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>
                                        <!--                                        <li><a href="#">Clear events</a></li>
                                                                                <li class="divider"></li>
                                                                                <li><a href="#">View calendar</a></li>-->
                                    </ul>
                                </div>
                                <button class="btn post_btn" ng-click='sharePost();'>Post</button>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="box post_update">
                        <div class="post-header">
                            <div class="btn-group confidentiality pull-right">
                                <button class="btn  btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Confidentiality  <i class="fa fa-chevron-down"></i></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="javascript:void(0)">Add new event</a></li>
                                    <li><a href="javascript:void(0)">Clear events</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)">View calendar</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-body">
                            <form>
                                <div class="user_pic">
                                    <img alt="" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg">
                                </div>
    
                                <textarea class="form-control post_field" placeholder="What’s up in your mind?"></textarea>
                                <div aria-label="..." role="group" class="btn-group btn-group-justified post_btngrp">
                                    <div role="group" class="btn-group">
                                        <button class="btn btn-primary" type="button"><i class="fa fa-edit"></i> Update</button>
                                    </div>
                                    <div role="group" class="btn-group">
                                        <button class="btn btn-primary" type="button"><i class="fa fa-photo"></i> Media</button>
                                    </div>
                                    <div role="group" class="btn-group">
                                        <button class="btn btn-primary" type="button"><i class="fa fa-sign-out"></i> Publish</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>-->
                </div>
                <!-- post block -->     

                <div class="col-md-12" ng-repeat="hub in hub_list" ng-show="show_default_hub">
                    <div class="box latest_talk"  ng-if="hub.type == 'hub'">
                        <div class="box-body">
                            <!--<img class="latest_talkimg" alt="" ng-src="{{hub.user_image != '' && '<?php echo base_url() ?>/uploads/profile_image/'+hub.user_image || +'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg" />-->
                            <img class="latest_talkimg" alt="" ng-src="<?php echo base_url() ?>/uploads/profile_image/{{hub.profile_image}}" />
                            <div class="latest_content">
                                <div class="latest_header clearfix">
                                <h6><avi ng-bind="hub.bussiness_name">Mike Johns</avi> <!--<span>author</span>--></h6>
                                    <div class="btn-group confidentiality pull-right">
                                        <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                        <ul role="menu" class="dropdown-menu pull-right">
                                            <li><a href="javascript:void(0)">Add new event</a></li>
                                            <li><a href="javascript:void(0)">Clear events</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">View calendar</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="post_innercont clearfix">
                            <a href="javascript:void(0)" target="_balnk" ng-if="hub.link != null">
                                <img class="post_img" alt="" ng-src="{{hub.base_url + hub.link}}" src="<?php echo base_url(); ?>assets_phase2/images/post-img.jpg">
                            </a>
                            <div ng-class="{'post_innertxt' : hub.image != ''}">
                                <!--<div class="post_innertxt" ng-if="news.author ==''" class = "full-width">-->
                                <a ng-href="javascript:void(0)" target="_balnk"><h5 ng-bind="hub.hub_title">The Future of Flying Robots For Suistanable Farming</h5></a>

                                <p ng-bind-html="hub.content">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>
                            </div>
                        </div>
                        <div class="box-footer post_bottom clearfix">
                            <ul class="latest_footerlink pull-left">
<!--                                <li><a href="javascript:void(0)"><i class="fa fa-share-alt"></i> Share</a></li>-->
                                <li><a href="javascript:void(0)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                <li><a href="javascript:void(0)" ng-class="{'like': hub.like != 0}" ng-click="feedback(hub)"><i ng-class="{'fa fa-heart' :hub.like != 0, 'fa fa-heart-o' :hub.like == 0}" class="fa fa-heart-o"></i> Like</a></li>
                            </ul>

                            <ul class="latest_footerlink_right pull-right">
                                <!--<li><a href="javascript:void(0)">43 <i class="fa fa-share-alt"></i></a></li>-->
                                <li><a href="javascript:void(0)"><span ng-bind="hub.comment_count">45</span> <i class="fa  fa-comment-o"></i> </a></li>
                                <li><a href="javascript:void(0)"><span ng-bind="hub.like_count">23</span> <i class="fa  fa-heart-o"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="box latest_talk"  ng-if="hub.type != 'hub'">

                    </div>
                </div>
                <div class="col-md-12"  ng-show="show_default_hub" >

                    <div class="box latest_talk" ng-repeat="current_publish in current_post_publish_list">
                        <div ng-if="current_publish.post == false">
                            <div class="box latest_talk">
                                <div class="box-body">
                                    <img class="latest_talkimg" alt="" ng-src="{{current_publish.image != '' && current_publish.path + current_publish.image|| 'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg" src="<?php echo base_url(); ?>assets_phase2/images/Alchemy-icon.png">
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                        <h6><avi ng-bind="current_publish.buisname">Mike Johns</avi> <!--<span>author</span>--></h6>
                                            <div class="btn-group confidentiality pull-right">
                                                <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                <ul role="menu" class="dropdown-menu pull-right">
                                                    <li><a href="javascript:void(0)">Add new event</a></li>
                                                    <li><a href="javascript:void(0)">Clear events</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="javascript:void(0)">View calendar</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                   <!-- <span class="post_time">9 Min ago</span>  -->
        <!--                            <p>Watch these adorable robots play follow the leader:</p>                  -->      
                                    </div>
                                </div>
                                <div class="post_innercont clearfix">
                                    <!--                                <a  target="_balnk" >-->
                                    <img class="post_img" alt="" ng-if="current_publish.image_post != ''" ng-src="{{current_publish.image_post}}"  src=""/>
                                    <!--                                </a>-->
                                    <div>
                                        <!--<div class="post_innertxt" ng-if="news.author ==''" class = "full-width">-->
                                        <!--                                    <a  target="_balnk"><h5>The Future of Flying Robots For Suistanable Farming</h5></a>-->

                                        <p ng-bind-html="current_publish.html_text">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>
    <!--                                    <p>{{current_publish.html_text | strLimit: 120}}</p>-->
                                    </div>
                                </div>
                                <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                        <ul class="latest_footerlink pull-left">
        <!--                                    <li><a href="javascript:void(0)"><i class="fa fa-share-alt"></i> Share</a></li>-->
                                            <li><a href="javascript:void(0)" ng-click="commentsAnimation(current_publish)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                            <li><a href="javascript:void(0)" ng-class="{'like': current_publish.like}" ng-click="feedback(current_publish)"><i ng-class="{'fa fa-heart' :current_publish.like, 'fa fa-heart-o' :!current_publish.like}" class="fa fa-heart-o"></i> Like</a></li>
                                        </ul>

                                        <ul class="latest_footerlink_right pull-right">
        <!--                                    <li><a href="javascript:void(0)">43 <i class="fa fa-share-alt"></i></a></li>-->
                                            <li><a href="javascript:void(0)"><span ng-bind="current_publish.comments_count">45 </span> <i class="fa  fa-comment-o"></i> </a></li>
                                            <li><a href="javascript:void(0)"><span ng-bind="current_publish.like_count"></span> <i class="fa  fa-heart-o"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="comment_box">
                                    <img class="comment_img" ng-src="{{current_publish.image != '' && current_publish.path + current_publish.image|| 'http://arlians.com/assets/images/profile-img.png'}}" />
                                    <input type="text" ng-model="current_publish.comment" class="form-control"  placeholder="Write your comment">
                                    <button class="btn btn_publish" ng-click="postComments(current_publish)">Post</button>
                                </div>
                            </div>
                            <div class="col-md-11 col-md-offset-1" ng-show="current_publish.show_comments" ng-repeat="comment in current_publish.comments">
                                <div class="box latest_talk comment_reply">
                                    <div class="box-body">
                                        <img ng-src="{{comment.image != '' && comment.path + comment.image|| 'http://arlians.com/assets/images/profile-img.png'}}" src="images/post-userimg.jpg" alt="" class="latest_talkimg">
                                        <div class="latest_content">
                                            <div class="latest_header clearfix">
<!--                                                <h6>Mike Johns <span>verified talker</span></h6>-->
                                                <h6 ng-bind="comment.buisnesname">Mike Johns</h6>
                                            </div>
                                            <!--<span class="post_time">9 Min ago</span>-->  
                                            <p ng-bind="comment.comment_view">Thats a really nice job cant wait to see it </p>                  
                                        </div>
                                    </div>

                                    <!--                                    <div class="box-footer post_bottom clearfix">
                                                                            <ul class="latest_footerlink pull-left">
                                                                                <li><a href="#"><i class="fa fa-share-alt"></i> <span>Share</span></a></li>
                                                                                <li><a href="#"><i class="fa  fa-comment-o"></i> <span>Comments</span></a></li>
                                                                                <li><a href="#"><i class="fa fa-heart-o"></i> <span>Like</span></a></li>
                                                                            </ul>
                                    
                                                                            <ul class="latest_footerlink_right pull-right">
                                                                                <li><a href="#">43 <i class="fa fa-share-alt"></i></a></li>
                                                                                <li><a href="#">45 <i class="fa  fa-comment-o"></i> </a></li>
                                                                                <li><a href="#">23 <i class="fa  fa-heart-o"></i></a></li>
                                                                            </ul>
                                                                        </div> -->
                                </div>
                            </div>
                        </div>

                        <div ng-if="current_publish.post == true">
                            <div class="box latest_talk">
                                <div class="box-body">
        <!--                            <img src="<?php echo base_url(); ?>assets_phase2/images/post-userimg.jpg" alt="" class="latest_talkimg">-->
                                    <img ng-src="{{current_publish.image != '' && current_publish.path + current_publish.image|| 'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image">
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6 ng-bind="current_publish.buisname">Mike Johns <span>verified talker</span></h6>
                                            <div class="btn-group confidentiality pull-right">
                                                <button class="btn  btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <li><a href="javascript:void(0)">Add new event</a></li>
                                                    <li><a href="javascript:void(0)">Clear events</a></li>
                                                    <li class="divider"></li>
                                                    <li><a href="javascript:void(0)">View calendar</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--<span class="post_time">9 Min ago</span>-->  
                                        <p ng-bind="current_publish.text">At his lab at the University of Pennsylvania, Vijay Kumar and his team develop a crazy AlgorithmWe are really looking forward to see it see live on <a href="javascript:void(0)"> www.liveteam.co.uk.</a> </p>                  
                                    </div>
                                </div>
                                <div class="box-footer post_bottom clearfix">
                                    <div class="comment_links clearfix">
                                        <ul class="latest_footerlink pull-left">
                                        <!--<li><a href="javascript:void(0)"><i class="fa fa-share-alt"></i> Share</a></li>-->
                                            <li><a href="javascript:void(0)" ng-click="commentsAnimation(current_publish)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                            <li><a href="javascript:void(0)" ng-class="{'like': current_publish.like}" ng-click="feedbackHub(current_publish)"><i ng-class="{'fa fa-heart' :current_publish.like, 'fa fa-heart-o' :!current_publish.like}" class="fa fa-heart-o"></i> Like</a></li>
                                        <!--<li><a href="javascript:void(0)" ng-class="{'like': current_publish.like}" ng-click="feedback(current_publish)"><i ng-class="{current_publish.like == true ? 'fa-heart' : 'fa-heart-o'}" class="fa"></i> Like</a></li>-->
                                        </ul>

                                        <ul class="latest_footerlink_right pull-right">
        <!--                                    <li><a href="javascript:void(0)">43 <i class="fa fa-share-alt"></i></a></li>-->
                                            <li><a href="javascript:void(0)"><span ng-bind="current_publish.comments_count">45 </span><i class="fa fa-comment-o"></i> </a></li>
                                            <li><a href="javascript:void(0)"><span ng-bind="current_publish.like_count">46</span> <i ng-class="{'fa fa-heart' :current_publish.like_count > 0, 'fa fa-heart-o' :current_publish.like_count == 0}" class="fa  fa-heart-o"></i></a></li>
                                        </ul>
                                    </div>
                                    <div class="comment_box">
                                        <img class="comment_img" ng-src="{{current_publish.image != '' && current_publish.path + current_publish.image|| 'http://arlians.com/assets/images/profile-img.png'}}" />
                                        <input type="text" ng-model="current_publish.comment" class="form-control"  placeholder="Write your comment">
                                        <button class="btn btn_publish" ng-click="postComments(current_publish)">Post</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-11 col-md-offset-1" ng-show="current_publish.show_comments" ng-repeat="comment in current_publish.comments">
                                <div class="box latest_talk comment_reply">
                                    <div class="box-body">
                                        <img ng-src="{{comment.image != '' && comment.path + comment.image|| 'http://arlians.com/assets/images/profile-img.png'}}" src="images/post-userimg.jpg" alt="" class="latest_talkimg">
                                        <div class="latest_content">
                                            <div class="latest_header clearfix">
<!--                                                <h6>Mike Johns <span>verified talker</span></h6>-->
                                                <h6 ng-bind="comment.buisnesname">Mike Johns</h6>
                                            </div>
                                            <!--<span class="post_time">9 Min ago</span>-->  
                                            <p ng-bind="comment.comment_view">Thats a really nice job cant wait to see it </p>                  
                                        </div>
                                    </div>

                                    <!--                                    <div class="box-footer post_bottom clearfix">
                                                                            <ul class="latest_footerlink pull-left">
                                                                                <li><a href="#"><i class="fa fa-share-alt"></i> <span>Share</span></a></li>
                                                                                <li><a href="#"><i class="fa  fa-comment-o"></i> <span>Comments</span></a></li>
                                                                                <li><a href="#"><i class="fa fa-heart-o"></i> <span>Like</span></a></li>
                                                                            </ul>
                                    
                                                                            <ul class="latest_footerlink_right pull-right">
                                                                                <li><a href="#">43 <i class="fa fa-share-alt"></i></a></li>
                                                                                <li><a href="#">45 <i class="fa  fa-comment-o"></i> </a></li>
                                                                                <li><a href="#">23 <i class="fa  fa-heart-o"></i></a></li>
                                                                            </ul>
                                                                        </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--<div class="col-md-11 col-md-offset-1">
                    <div class="box latest_talk comment_reply">
                        <div class="box-body">
                            <img src="<?php echo base_url(); ?>assets_phase2/images/post-userimg.jpg" alt="" class="latest_talkimg">
                            <div class="latest_content">
                                <div class="latest_header clearfix">
                                    <h6>Mike Johns <span>verified talker</span></h6>

                                </div>
                                <span class="post_time">9 Min ago</span>  
                                <p>Thats a really nice job cant wait to see it </p>                  
                            </div>
                        </div>

                        <div class="box-footer post_bottom clearfix">
                            <ul class="latest_footerlink pull-left">
                                <li><a href="javascript:void(0)"><i class="fa fa-share-alt"></i> <span>Share</span></a></li>
                                <li><a href="javascript:void(0)"><i class="fa  fa-comment-o"></i> <span>Comments</span></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-heart-o"></i> <span>Like</span></a></li>
                            </ul>

                            <ul class="latest_footerlink_right pull-right">
                                <li><a href="javascript:void(0)">43 <i class="fa fa-share-alt"></i></a></li>
                                <li><a href="javascript:void(0)">45 <i class="fa  fa-comment-o"></i> </a></li>
                                <li><a href="javascript:void(0)">23 <i class="fa  fa-heart-o"></i></a></li>
                            </ul>
                        </div> 
                    </div>
                </div>-->


                <div class="box publish_section clearfix" ng-show="show_publish">
                    <div class="publish_toppart">
                        <!--                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                          Launch demo modal
                        </button>-->

                        <!-- Modal Crop Image-->
                        <div class="modal fade" id="crop_publish_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Upload an Image</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-crop">
                                            <div class="imageBox">
                                                <div class="thumbBox"></div>
                                                <div class="spinner" style="display: none">Loading...</div>
                                            </div>
                                            <div class="action">

<!--<input type="button" id="uploadImg" style="float: right" value="Upload"/>
<input type="button" id="btnCrop" value="Upload" style="float: right">
<input type="button" id="btnZoomIn" value="+" style="float: right">
<input type="button" id="btnZoomOut" value="-" style="float: right">-->
                                            </div>
                                            <div class="cropped"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="file" id="file">                                               
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                        <button type="button" id="btnZoomIn" class="btn btn-primary">+</button>
                                        <button type="button" id="btnZoomOut" class="btn btn-primary">-</button>
                                        <button type="button" id="btnCrop" class="btn btn-primary">Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        <input type="file" id="upload_img_for_post" onchange="readURLPostImage(this);"/>-->
                        <i class="fa fa-camera change_photos" style="cursor:pointer" onclick="open_publish_crop_img_modal()"></i>
<!--                        <input type="file" ng-model="image" ng-change="readURLPostImage(this)"><i class="fa fa-camera change_photos"></i></input>-->

                        <div class="publish_namecontainer">
                            <img ng-src="{{front_img_name != '' && path + front_img_name|| 'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image">
<!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                            <div class="pulish_heading">
                                <span class="publis_name" ng-bind="bussinessname">Digitalweb</span>
                                <p class="publish_subtxt">Production of modern website</p>
                            </div>
                        </div>

                        <img id="hub_publish_default_img" src="<?php echo base_url(); ?>assets/images/publish_img.jpg" alt="">
                        <img id="hub_publish_preview_img"  src="" alt="">
                    </div>
                    <input type="text" ng-model="publish_header" placeholder="Write your headline" />
                    <!--                    <h5>Write your headline</h5>-->

                    <div class="publish_editor">
                        <textarea id="txtEditor"></textarea> 
                    </div>
<!--                    <div class="post-header add_tags"><i class="fa fa-tag"></i>Add tags</div>   
                    <div class="bs-example">
                        <input type="text" value="tag name" data-role="tagsinput" />
                    </div>-->
                    <!--<div class="clearfix">
                        <span style="color:#345672" ng-bind="show_post_selected">Hello world</span>
                        <div class="btn-group post_footerdrpbtn">
                            <i class="fa fa-gears pull-left"></i>
                            <button aria-expanded="false" data-toggle="dropdown" class="btn  btn-sm dropdown-toggle">Share with</button>
                            <ul role="menu" class="dropdown-menu pull-right">                                        
                                <li><a href="javascript:void(0);" ng-click="select_post(list)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                
                            </ul>
                        </div>
                    </div>-->
                    <div class="clearfix"></div>
                    <button class="btn btn_publish pull-right" ng-click="showDefault()">Back</button>
                    <button class="btn btn_publish pull-right" ng-click="savePublish()">Publish</button>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 sticky-scroll">
            <div class="details_tab">
                <div id="parentHorizontalTab">
                    <ul class="resp-tabs-list hor_1 clearfix">
                        <li><i class="fa  fa-refresh"></i><br>Latest Talks</li>
                        <li><i class="fa  fa-cloud-download"></i><br>My Talks</li>
                        <li><i class="fa  fa-th"></i><br>Top Talks</li>
                        <li><i class="fa  fa-users"></i><br>Start a Talk</li>
                        <li><i class="fa  fa-list"></i><br>More</li>
                    </ul>
                    <div class="resp-tabs-container hor_1">
                        <div>
                            <div class="box latest_talk">
                                <div class="box-body">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" alt="" class="latest_talkimg img-circle">
                                        <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6>Mike Johns <span>verified talker</span></h6>
                                            <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                        </div>
                                        <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                    </div>
                                </div>
                                <div class="box-footer clearfix">
                                    <ul class="latest_footerlink pull-left">
                                        <li><a href="javascript:void(0)">Participate</a></li>
                                        <li><a href="javascript:void(0)">Like </a></li>
                                        <li><a href="javascript:void(0)">Recommend</a></li>
                                    </ul>

                                    <ul class="latest_footerlink_right pull-right">
                                        <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                        <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                        <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                    </ul>
                                </div> 
                            </div>

                            <div class="box latest_talk">
                                <div class="box-body">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets_phase2/images/user7-128x128.jpg" alt="" class="latest_talkimg img-circle">
                                        <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6>Mike Johns <span>verified talker</span></h6>
                                            <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                        </div>
                                        <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                    </div>
                                </div>
                                <div class="box-footer clearfix">
                                    <ul class="latest_footerlink pull-left">
                                        <li><a href="javascript:void(0)">Participate</a></li>
                                        <li><a href="javascript:void(0)">Like </a></li>
                                        <li><a href="javascript:void(0)">Recommend</a></li>
                                    </ul>

                                    <ul class="latest_footerlink_right pull-right">
                                        <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                        <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                        <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                    </ul>
                                </div> 
                            </div>

                            <div class="box latest_talk">
                                <div class="box-body">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets_phase2/images/user5-128x128.jpg" alt="" class="latest_talkimg img-circle">
                                        <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6>Mike Johns <span>verified talker</span></h6>
                                            <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                        </div>
                                        <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                    </div>
                                </div>
                                <div class="box-footer clearfix">
                                    <ul class="latest_footerlink pull-left">
                                        <li><a href="javascript:void(0)">Participate</a></li>
                                        <li><a href="javascript:void(0)">Like </a></li>
                                        <li><a href="javascript:void(0)">Recommend</a></li>
                                    </ul>

                                    <ul class="latest_footerlink_right pull-right">
                                        <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                        <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                        <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                    </ul>
                                </div> 
                            </div>

                            <div class="box latest_talk">
                                <div class="box-body">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" alt="" class="latest_talkimg img-circle">
                                        <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6>Mike Johns <span>verified talker</span></h6>
                                            <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                        </div>
                                        <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                    </div>
                                </div>
                                <div class="box-footer clearfix">
                                    <ul class="latest_footerlink pull-left">
                                        <li><a href="javascript:void(0)">Participate</a></li>
                                        <li><a href="javascript:void(0)">Like </a></li>
                                        <li><a href="javascript:void(0)">Recommend</a></li>
                                    </ul>

                                    <ul class="latest_footerlink_right pull-right">
                                        <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                        <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                        <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                    </ul>
                                </div> 
                            </div>


                        </div>


                    </div>


                    <!-- Tab 2 -->
                    <div class="resp-tabs-container hor_1">
                        <div>
                            <div class="box latest_talk">
                                <div class="box-body">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" alt="" class="latest_talkimg img-circle">
                                        <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6>Mike Johns <span>verified talker</span></h6>
                                            <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                        </div>
                                        <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                    </div>
                                </div>
                                <div class="box-footer clearfix">
                                    <ul class="latest_footerlink pull-left">
                                        <li><a href="javascript:void(0)">Participate</a></li>
                                        <li><a href="javascript:void(0)">Like </a></li>
                                        <li><a href="javascript:void(0)">Recommend</a></li>
                                    </ul>

                                    <ul class="latest_footerlink_right pull-right">
                                        <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                        <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                        <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                    </ul>
                                </div> 
                            </div>
                            <div class="box latest_talk">
                                <div class="box-body">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets_phase2/images/user7-128x128.jpg" alt="" class="latest_talkimg img-circle">
                                        <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6>Mike Johns <span>verified talker</span></h6>
                                            <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                        </div>
                                        <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                    </div>
                                </div>
                                <div class="box-footer clearfix">
                                    <ul class="latest_footerlink pull-left">
                                        <li><a href="javascript:void(0)">Participate</a></li>
                                        <li><a href="javascript:void(0)">Like </a></li>
                                        <li><a href="javascript:void(0)">Recommend</a></li>
                                    </ul>
                                    <ul class="latest_footerlink_right pull-right">
                                        <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                        <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                        <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                    </ul>
                                </div> 
                            </div>

                            <div class="box latest_talk">
                                <div class="box-body">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets_phase2/images/user5-128x128.jpg" alt="" class="latest_talkimg img-circle">
                                        <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6>Mike Johns <span>verified talker</span></h6>
                                            <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                        </div>
                                        <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                    </div>
                                </div>
                                <div class="box-footer clearfix">
                                    <ul class="latest_footerlink pull-left">
                                        <li><a href="javascript:void(0)">Participate</a></li>
                                        <li><a href="javascript:void(0)">Like </a></li>
                                        <li><a href="javascript:void(0)">Recommend</a></li>
                                    </ul>

                                    <ul class="latest_footerlink_right pull-right">
                                        <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                        <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                        <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                    </ul>
                                </div> 
                            </div>
                            <div class="box latest_talk">
                                <div class="box-body">
                                    <div class="image">
                                        <img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" alt="" class="latest_talkimg img-circle">
                                        <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                    </div>
                                    <div class="latest_content">
                                        <div class="latest_header clearfix">
                                            <h6>Mike Johns <span>verified talker</span></h6>
                                            <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                        </div>
                                        <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                    </div>
                                </div>
                                <div class="box-footer clearfix">
                                    <ul class="latest_footerlink pull-left">
                                        <li><a href="javascript:void(0)">Participate</a></li>
                                        <li><a href="javascript:void(0)">Like </a></li>
                                        <li><a href="javascript:void(0)">Recommend</a></li>
                                    </ul>
                                    <ul class="latest_footerlink_right pull-right">
                                        <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                        <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                        <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                    </ul>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-12" ng-controller="DashboardSuggestionCtrl as suggestion">
        <div class="row">   

            <div class="col-sm-6 col-md-12" ng-show="search_module">
                <div class="view_result_box fixd_box1">
                    <div class="view_box_top"><h4>See All Matching <small><i class="fa fa-search"></i></small></h4></div>
                    <div class="view_box_body">
                        <div class="match_result">
                            <span class="mathc_txt">MATCHING <br> RESULTS</span><br>
                            <span class="mathc_resulttxt" ng-bind="total_suggestion_count">132</span>
                        </div>
                        <div class="result_bottom clearfix">
                            <div id="result-circle" class="owl-carousel">
                                <div class="item" ng-repeat="list in detail_suggestion_list">
                                    <div class="result_numb">
                                        <div class="result_numbin" ng-bind="list.type_count">69</div>
                                    </div>
                                    <span class="result_name" ><a ng-bind="list.type" href="javascript:void(0)" ng-click="getDetailSuggestion(list)">Customers</a></span>
                                </div>                                 
                            </div>
                        </div>
                    </div>
                    <div class="view_box_bottom">
                        <a href="<?php echo base_url() ?>suggestion/makeSuggestedListForloginuserMod" class="view_menu"><i class="fa fa-list"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-12"  ng-show="show_detail" id="detail_suggestion">
                <div class="view_result_box fixd_box1" ng-repeat="detail in details_suggestion">
                    <div class="view_box_top1 matching clearfix"><h4 ng-bind="search_for_suggestion">Customers</h4>
                        <div class="result_numb1">
                            <div class="result_numbin1" ng-bind="total_count">69</div>
                        </div>
                    </div>
                    <div class="view_box_body1">
                        <div class="matchign_cont">
                            <a href="#" class="matchig_logo"><img ng-src="{{notify.image_url!=null?'<?php echo base_url(); ?>uploads/profile_image/'+notify.image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}}" alt="" ></a>
                            <p class="match_detail" >Excel at enterprise, mobile and web development with Java, Scala, Groovy and Kotlin, with all the latest modern technologies and frameworks available out of the box. </p>
                        </div>
                        <img src="<?php echo base_url() ?>assets_phase2/images/match_enginebg.jpg" alt="">
                    </div>
                    <div class="view_box_body1 text-center clearfix">
                        <a href="javascript:void(0);" class="result_numb3" ng-click="individual_suggestion_user_feedback(detail, 0)">
                            <div class="result_numbin3"><i class="fa fa-minus"></i></div>
                        </a>
                        <a href="javascript:void(0);" class="result_numb4" ng-click="individual_suggestion_user_feedback(detail, 1)">
                            <div class="result_numbin4"><i class="fa fa-plus"></i></div>
                        </a>
                        <div class="clearfix match_score">
                            <div class="result_numb2">
                                <div class="result_numbin2" ng-bind="detail.score">69</div>
                            </div>
                            <span class="score_txt" ng-bind="detail.matched_text">Match Score</span>
                        </div>
                    </div>
                    <div class="view_box_bottom">
                        <span class="backto_result" ><a href="javascript:void(0);" ng-click="backToSuggestionList()">Back to matching results</span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-12">
                <div class="view_result_box portfolio">
                    <div class="view_box_top"><h4>Portfolio Documents <small><i class="fa fa-search"></i></small> </h4></div>
                    <div class="view_box_body clearfix">

                        <div class="portfolio_box white_box">
                            <i class="fa fa-pencil"></i>
                        </div>

                        <div class="portfolio_box normal_box">
                            <span class="portbox_txt">Notes</span><br>
                            <span class="port_inncircle">12</span>  
                        </div>

                        <div class="portfolio_box  normal_box ">
                            <span class="portbox_txt">Documents</span><br>
                            <span class="port_inncircle">12</span> 

                        </div>

                        <div class="portfolio_box white_box ">
                            <i class="fa  fa-folder"></i>    
                        </div>

                        <div class="portfolio_box white_box">
                            <i class="fa fa-comment"></i>
                        </div>

                        <div class="portfolio_box normal_box">
                            <span class="portbox_txt">Talks</span><br>
                            <span class="port_inncircle">12</span>  
                        </div>

                    </div>
                    <div class="view_box_bottom">
                        <a href="javascript:void(0)" class="view_menu"><i class="fa fa-list"></i></a>
                    </div>
                </div>
            </div>
        </div>

    </div>            
</div>

<!-- common-helper JS -->
<script src="<?php echo base_url(); ?>assets_phase2/js/dashboard_page.js"></script>
<script>
                                    $(function(){
                                    $('#hub_publish_default_img').show();
                                            $('#hub_publish_preview_img').hide();
                                            CKEDITOR.replace('txtEditor');
                                    });
</script>