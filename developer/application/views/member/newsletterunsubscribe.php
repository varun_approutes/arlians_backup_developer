<div class="container" ng-app="newsLetter" ng-controller="newsletterunsubscribe"> 
                     
	<h3 >&nbsp;</h3>
	<div class="email_validate1" >   
						   
		<form name="userEmail" ng-submit="validateUserEmail(user_email)">
			<span class="input input--haruki animated fadeInUp d1">
				<input class="input__field input__field--haruki"  ng-model="user_email" ng-class="{'has-error': userEmail.user_email.$error.pattern}" ng-pattern="/^\S+@\S+\.\S+$/i" required autocomplete="off" type="email" name="user_email" />
				<span ng-show="userEmail.user_email.$dirty && userEmail.user_email.$error.required" class="validation_error">Required</span>
				<span ng-show="!userEmail.user_email.$error.required && userEmail.user_email.$dirty && userEmail.user_email.$error.email" class="validation_error">Enter correct email format</span>   
				
				<label class="input__label input__label--haruki" for="user_email">
					<span class="input__label-content input__label-content--haruki">Enter your Email</span>
				</label>
			   
				
			</span>  
			
			<button class="btn get_started animated fadeInUp" type="submit" ng-click="FnUnsubscribe();">Unsubscribe</button>
		</form>
		<div class="line"></div>	   
	</div>  
</div>
<script>
	var arlians = angular.module('newsLetter',[]); 
	arlians.controller('newsletterunsubscribe', function($scope){
		$scope.user_email = "";
		$scope.FnUnsubscribe = function() {
        UserProxy.Unsubscribe($scope.user_email,function(response){
			if(response.success=='true')
			{
				alertify.success('You are successfully unsubscribed from the newsletter.');
			}
			else
			{
				 alertify.error('Email not registered in our database');
			}
			
		});
    };
	});
	
</script>