<div class="premium-cancel-wrap">
<h2>Account deletion</h2>

<p class="warning-delet">Warning! Deleting your account means you will lose all data, without any means to retrieve it. We highly recommend you suspend your account instead, to avoid loss of potentially valuable information</p>
<div class="select-premium">
<h3>Are you sure you wish to continue?</h3>
<ul>
<li><input type="radio" name="radio" id="radio">Please delete my account</li>
</ul>
</div>
<div class="account-delet-form">
<ul>
<li>
<div class="account-delet-left">
<label>Enter your password</label>
</div>
<div class="account-delet-right">
<input type="password" name="pass" id="pass">
</div>
</li>
<li>
<div class="account-delet-left">
<label>Re-enter your password</label>
</div>
<div class="account-delet-right">
<input type="password" name="pass" id="pass">
</div>
</li>
<li>
<div class="account-delet-right">
<input type="submit" value="Submit" id="submit">
</div>
</li>
</ul>
</div>
</div>