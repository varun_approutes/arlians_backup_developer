 <?php $mid = $this->session->userdata['logged_in']['id']; 
$user_type = check_member_payment_status($mid);?> 
<div class="dashboard-wrap">
    <div class="hub-wrap"> 
        <div class="dashboard-hub-heading">
               <h2><i class="fa fa-newspaper-o fa-2"></i>The hub</h2>
        </div>
        <div class="duo__cell example__demo" data-js-module="layout-complete-demo">
            <div class="grid grid--clickable">
            <?php $user_priority = $user_type[0]['user_type'];?>
            <?php $i = 0; foreach($hubs as $hub){ 
                    $class = ($i%2 != 0)? ( $user_priority == "Free"? "grid-item hub-listing":"grid-item hub-listing premium"):($user_priority== "Free"? "hub-listing":"hub-listing premium");

              ?>               
                <div class="<?php echo $class; ?>" id="<?php echo $hub['hid']?>_hub">
                    <div class="hub-left">
                        <?php $imageName = getProfileImage($hub['mid']); ?>
                        <img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $imageName!=''?$imageName:'profile-img.png'; ?>" alt=""/>
                        <h3><?php echo getMemberbusinName($hub['mid']); ?></h3>
                    </div>
                    <div class="right-hub">
                      <?php if($hub['hub_title']!=''){?><h3><?php echo $hub['hub_title']; ?></h3><?php } ?>
                        <div class="right-hub-back">
                            <?php if($hub['link'] != '') {?>
         
                    <div class="hub-img">
								<?php if($hub['hub_title']!=''){?><h3><?php echo $hub['hub_title']; ?></h3><?php } ?>

                                <img src="<?php echo base_url(); ?>uploads/hub_images/<?php echo $hub['link']; ?>" alt=""/>
                                
                                
                            </div>
                            <?php } ?>
                            <div class="hub-content">
                                <p><?php echo $hub['content']; ?></p>
                                <a class="share-report" href="javascript:void(0)" onclick="showHideDiv('<?php echo $hub["hid"]?>_share-report-open')"> <i class="fa fa-ellipsis-v fa-2"></i></a>
                                <div class="share-report-sec" id="<?php echo $hub['hid']?>_share-report-open" style="display:none">
                                  <ul>
                                    <li><a href="javascript:void(0)" onclick="shared('<?php echo $hub['hid'];?>')">Share</a></li>
                                    <li><a class="edit-sec report_modal_link" href="#report_modal" onclick="setHubId('<?php echo $hub["hid"]?>')" >Report</i></a></li>
                                    <?php ;if(empty($hub['is_connected'])){?>
                                        <li><a href="javascript:void(0)" onclick="connectedToUser(this,<?php echo $hub['mid']?>)">Connect</a></li>
                                    <?php }else{?>                                        
                                        <li>
                                        <a href="javascript:void(0)">                                         
                                        <?php if($hub['is_connected']->is_accepted!='0'){?> 
                                            Connected
                                        <?php }else{?>
                                            Request send
                                        <?php }?>
                                        </a>
                                        </li>
                                    <?php }?>
                                    <?php if($hub['mid'] == $user['mid']){?>
                                      <li><a href="javascript:void(0)" onclick="deletePost(<?php echo $hub['hid']?>)">Delete</a></li>
                                    <?php }?>  
                                  </ul>
                                </div>
                            </div>
                        </div>
                      <div class="comment-post-wrap">
                          <div class="comment-post">
                              <div class="comment-like">
                                <?php if($hub['like'] == 1){?>
                                    <a class="like active" href="javascript:void(0)" onclick="manageLike(this,<?php echo $hub['hid']?>)"> <i class="fa fa-heart fa-2"></i></a>
                                <?php }else{?>
                                    <a class="like" href="javascript:void(0)" onclick="manageLike(this,<?php echo $hub['hid']?>)"> <i class="fa fa-heart fa-2"></i></a>
                                <?php }?>
                                <div class="count-like">
                                <span style="color:#000" id="<?php echo $hub['hid']?>"><?php echo $hub['likes'];?></span>
                                <span style="color:#000" id="<?php echo $hub['hid']?>_text"><?php echo $hub['likes']==1?' like':' likes'?></span>
                                </div>
                                <?php if(count($hub['comments'])>1){?>
                                    <a class="comment" onclick="showComments(<?php echo $hub['hid']?>)">View comments</a>
                                <?php }?>
                                
                              </div>
                              <div id="<?php echo $hub['hid']?>_comments_list">
                              <?php if(count($hub['comments']) >0 ){ ?>
                                <?php $count=0;?>
                                <?php foreach($hub['comments'] as $comment){?>
                                  <?php if($count==0){?>  
                                    <div class="comment-writing" id="<?php echo $comment['comm_id']?>_comment"> 
                                  <?php }else{?>  
                                    <div class="comment-writing hid_comment" id="<?php echo $comment['comm_id']?>_comment"> 
                                  <?php }?>  
                                  <?php                                   
                                    if($comment['pic_id']!=NULL) { 
                                          $pic = $comment['pic_id'];
                                        } else{
                                          $pic = "profile-img.png";
                                    }
                                    //print_r($hub['comments']);
                                  ?>   
                                    <div class="comment-writing-logo" >
                                      <img alt="" src="<?php echo base_url();?>uploads/profile_image/<?php echo $pic; ?>">
                                    </div> 
                                    
                                    <div class="comment-righting-text" >
                                    <?php echo checkURLfromString($comment['comments']); ?>
                                    <?php if($comment['mid'] == $user['mid']){?>
                                      <div class="comment-delet">
                                      <a href="javascript:void(0)" onclick="deleteComment(<?php echo $comment['comm_id']?>)"><i class="fa fa-trash fa-2"></i></a>
                                      </div>
                                    <?php }?>
                                    </div>            
                                  </div>
                                  <?php $count++;?>
                                  <?php }?>
                                 <?php } ?>
                              </div>
                              <div class="comment-read">
                                  <div class="comment-writing-logo">
                                      <img src="<?php echo base_url();?>uploads/profile_image/<?php echo $user['fornt_image']!=null ? $user['fornt_image']:'profile-img.png';?>" alt=""/>
                                  </div>
                                <div class="comment-textarea">
                                   <textarea id="<?php echo $hub['hid']?>_comments"></textarea>
                                  <a class="comment" onclick="postComments(<?php echo $hub['hid']?>);">Post</a>
                                </div>
                                <span style="color: red;font-size: 11pt;">**Please add http:// before you post any URL</span>
                              </div>
                            </div>
                      </div>
                    </div>
                </div>                   
                <?php $i++; } ?>          
            </div>
        </div>
    </div>     
    <div id="report_modal" style="width:400px; display:none;">
        <div class="popup-box-part-center">
            <form id="report_form" method="post">
                <h2>Report</h2>
                <input type="hidden" id="hidden_hub_id" >
                <div class="business-img-listing-wrap cust-fancy-label-input">
                    <div class="cust-fancy-label-input">
                        <label>Cause</label>
                        <input type="text" name="cause_report" id="cause_report" >
                    </div>                     
                    <button type="submit" class="button" >Send</button> 
                </div>      
            </form>    
        </div>
    </div>          
</div>
<script type="text/javascript">
    
    $(function(){
      setTimeout(function() {
        location.reload();
      }, 180000);
      $('p').load(function(){
         $('.grid').masonry();
      });
      $('img').load(function(){
           $('.grid').masonry();
        });
      $('.report_modal_link').fancybox();
      $("#report_form").validate({
          rules: {
              cause_report: { required: true }
          },
          messages: {
              cause_report: { required: "Please enter the reason" }
          },
          submitHandler: function (form) {
              try{
                $('#loader-wrapper').show();
                var reason_report = $('#cause_report').val();
                var hub_id = $('#hidden_hub_id').val();
                $.post('<?php echo base_url(); ?>hub/reportHub',{hub_id:hub_id,reason_report:reason_report},function(result){ 
                    $('#loader-wrapper').hide();
                    if(result=='1'){
                      alertify.success('Your report has successfully posted');
                      $('.fancybox-close').trigger('click');  
                      setHubId('');
                    }
                });
              }catch(e){
                alertify.error(e);
              }
          }
      });
    });
 function showComments(hid){
    $("#"+hid+"_comments_list>div").removeClass('hid_comment');
    $('.grid').masonry();
 }
    function setHubId(hub_id){
      $('#hidden_hub_id').val(hub_id);
      $('#cause_report').val('');
    }
    function shared(hub_id){
      try{
        $('#loader-wrapper').show();
        $.post('<?php echo base_url(); ?>hub/shared',{'hub_id':hub_id},function(result){          
          $('#loader-wrapper').hide();
          if(result==1){            
            alertify.success('Request for shared was successfully done');
          }else{
          alert('This is embrassing.Some error occured');
          }
        });
      }catch(e){
          alertify.error(e);
      }  
    }
    function connectedToUser(curObj,conected_id){      
        try{
          $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>member/reqestForConnection',{'id':conected_id},function(result){
              //console.log(result);
              $('#loader-wrapper').hide();
              if(result==1){
                $(curObj).removeAttr("onclick");
                $(curObj).text('Request Send');
                alertify.success('Request Send');
              }else{
              alert('This is embrassing.Some error occured');
              }
            });
          }catch(e){
              alertify.error(e);
          }
    }
    function showHideDiv(id){
      var obj = document.getElementById(id);
        if (obj.style.display=="none"){
          obj.style.display='block';
        } else if(obj.style.display=="block"){
          obj.style.display='none';
        }
    }
    function manageLike(curObj,hub_id){
        if($(curObj).hasClass('active')){
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deleteLike',{'hub_id':hub_id},function(res){
                $('#loader-wrapper').hide();                
                $(curObj).removeClass('active');
                var like_count =  parseInt($('#'+hub_id).text()-1);              
                $('#'+hub_id).text(like_count);
                $('#'+hub_id+'_text').text(like_count==1||like_count==0?' like':' likes');
            });
        }else{            
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/postLike',{'hub_id':hub_id},function(res){              
                $('#loader-wrapper').hide();                
                $(curObj).addClass('active');
                var like_count = parseInt($('#'+hub_id).text())+parseInt(1);
                $('#'+hub_id).text(like_count);
                $('#'+hub_id+'_text').text(like_count==1||like_count==0?' like':' likes');
            });
                  
        }
    }
    function deletePost(hub_id){
      try{
        alertify.confirm("Are you sure to delete the post!", function (e) {
          if (e) {
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deletePost',{'hub_id':hub_id},function(res){
                if(res!=0){  
                  $('#loader-wrapper').hide(); 
                   location.reload();
                } 
            });
          }
        });
      }catch(e){
        alertify.error(e);
      }
    }
    function deleteComment(comment_id){
      try{
        alertify.confirm("​Are you sure you’d like to delete this comment?", function (e) {
          if (e) {
            $('#loader-wrapper').show();
            $.post('<?php echo base_url(); ?>hub/deleteComment',{'comment_id':comment_id},function(res){
                  if(res!=0){              
                    $('#'+comment_id+"_comment").remove();  
                    $('#loader-wrapper').hide(); 
                  } 
              });
          }
        });
      }catch(e){
        alertify.error(e);
      }
    }
    function postComments(hid){
        var comment = $('#'+hid+'_comments').val();
       // alert(comment);
        if(comment!= undefined){
          $('#loader-wrapper').show();
          $.post('<?php echo base_url(); ?>hub/postComment',{'hub_id':hid,'comment':comment},function(res){
            res = eval("(" + res + ")");
            $('#loader-wrapper').hide();           
                if(res.id!=0){              
                    var user_image = res.user_image!=''?res.user_image:'profile-img.png';          
                    var html  = '<div class="comment-writing" id="'+res.id+'_comment">'    
                                        +'<div class="comment-writing-logo">'
                                          +'<img alt="" src="<?php echo base_url();?>uploads/profile_image/'+user_image+'">'
                                        +'</div>'
                                        +'<div class="comment-righting-text">'+
                                        res.comment
                                        +'<div class="comment-delet">'
                                      +'<a href="javascript:void(0)" onclick="deleteComment('+res.id+')"><i class="fa fa-trash fa-2"></i></a>'
                                      +'</div>'; 
                                        +'</div>'                                                    
                                      +'</div>';
                      $('#'+hid+'_comments_list').append(html);
                      $('#'+hid+'_comments').val('');                  
                   $('.grid').masonry(); 
                  //location.reload();
                } 
            });
        }else{
          alertify.error('Please enter your comment!');
        }    
    }
</script>