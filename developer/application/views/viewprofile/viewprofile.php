<script type="text/javascript">
  jQuery(function() {
    jQuery("#more-drop-down").hover(
      function() { jQuery(".sub").slideToggle(400); },
      function() { jQuery(".sub").hide(); }
    );
    jQuery(function() {
      jQuery("#more-drop-down2").hover(
      function() { jQuery(".sub").slideToggle(400); },
      function() { jQuery(".sub").hide(); }
      );
    });
    jQuery(function() {
      jQuery("#setting-open").hover(function(){
        jQuery(".setting-drop-down-open").slideToggle( "slow", function() {     

        });
      });
      });
  });

//-------------------
$('.profile-img-left').first().remove();
//$('.profile-cont-left').first().remove();
</script>
<?php $id = $this->uri->segment(3); 
if($id== $this->session->userdata['logged_in']['id']){
	redirect('/memberprofile/viewProfile/', 'refresh');
}

$imageName =  getProfileImage($id); 
if($imageName==''){$imageName='profile_image.png';}
 ?>
<div class="dashboard-wrap other_profile" id="other_profile_section">
  <div class="row">
    <div class="fl100">
        <div class="compose-inner-bg profile-new-bg">
          <div class="profile-inner">
            <div class="user-left-images" style="margin-right:25px;"><img style="height:174px;width:174px;" src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $imageName;  ?>" alt="" /></div>
              <h3><?php echo $bussinessname; ?></h3>
             <!-- <p>Lorem ipsum dolor sit amet, consecteturadipiscing</p>-->
             <div class="inbox-reply-delet" style="position:inherit; display:block;">                
                <a class="reply" href="#send_message_modal" id="send_message_modal_link" data-bind="visible:show_msg_btn">Message</a>
                <a class="delet" href="javascript:void(0);" data-bind ="visible:show_connet_btn,click:reqestForConnection">Connect</a>
                <a class="delet" href="javascript:void(0);" data-bind="visible:show_requset_send_btn">Request Sent</a>
              </div>
              <?php if(isFoundingMember($mid)==true){?>
                  <div class="right-founder">
                      <i class="fa fa-star fa-2"></i>
                      <span>Premium Member</span>
                      <!--<span><?php echo $fname.' '.$lname;?> <br> Member</span>-->
                  </div>
              <?php }?>

              <div class="profile-connection-inner">
                  <div class="details-title">
                      <i class="fa fa-map-marker fa-2"></i>
                      <?php $countryName = country_name($country); $c_name = $countryName['country']; ($c_name != NULL)? $c_name : "Add Your Country"; ?>
                      <h4><?php echo $city." ".$c_name;?></h4>
                  </div>
                  <div class="clear"></div>
                  <?php 
                      foreach($wiz_one_ans as $row){ 
                          $arr = explode('-',$row['ans_set_cat_id']); 
                          $bscode = $arr[0]; 
                          $tn = topline_name($bscode); 
                          $img = get_topline_image($bscode);
                          $bsncode = $arr[1]; 
                          $sectorName = sector_name($bsncode);
                      } 
                  ?>                                            
                  <div class="profile-conntection-left">
                      <div class="profile-connection-details">
                          <div class="profile-connection-col">
                              <h4 class="heading">Industries</h4>
                          </div>
                          <div class="profile-connection-col border-right">
                              <h4><?php echo $tn['top_name']; ?></h4>
                          </div>
                      </div>
                    
                    <div class="profile-connection-details">
                        
                           <div class="profile-connection-col">
                              <h4 class="heading">Sector name</h4>
                          </div>
                          <div class="profile-connection-col">
                              <h4><?php echo $sectorName['sector_name']; ?></h4>
                          </div>
                      </div>
                    
                      <div class="profile-connection-details">                                
                          <div class="profile-connection-col">
                              <h4 class="heading">Sub-Sectors</h4>
                          </div>
                          <?php foreach($wiz_one_ans as $row){  ?>
                              <?php $arr = explode('-',$row['ans_set_cat_id']); $bscode = $arr[2]; $subSectorName = sub_sector_name($bscode); ?>                      
                              <div class="profile-connection-col border-right">
                                  <h4><?php echo $subSectorName['subsector_name']; ?></h4>
                              </div>                                   
                          <?php } ?>                                
                          <!--<div class="profile-connection-col">
                              <h4>Performing Art</h4>
                          </div>-->
                      </div>
                      <div class="profile-connection-details">
                          <div class="profile-connection-col">
                              <h4 class="heading">Business Type</h4>
                          </div>
                          <?php foreach($wiz_one_ans as $row){  ?>
                              <?php $arr = explode('-',$row['ans_set_cat_id']); $bscode = $arr[3]; $b = bussiness_name($bscode); ?>                       
                              <div class="profile-connection-col border-right">
                                  <h4><?php echo $b['bussiness_name']; ?></h4>
                              </div>
                          <?php } ?>
                         
                          <!--<div class="profile-connection-col">
                              <h4>Retailer</h4>
                          </div>-->
                      </div>
                  </div>
                  <div class="profile-conntection-right">
                      <div class="plus-connection">
                          <p>
                          <span data-bind="text:connection()">500+</span><br>Connections
                          </p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      
       <div class="compose-inner-bg profile-new-bg">
          <div class="profile-inner">
              <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Latest posts</h2></div>
             
            <div class="post-inner">
										<?php if(count($latest_post>0)){?>
											<ul>
												<?php foreach($latest_post as $post){?>
													<li>
														<?php if($post['link']!=''){?>
															<img src="<?php echo base_url(); ?>uploads/hub_images/<?php echo $post['link']; ?>" alt=""/>
														<?php }
                                                        $content = $post['hub_title']!=null ? $post['hub_title']:$post['content'];
                                                        ?>   
														<!--<h4><?php custom_echo($content,200); ?></h4>-->
														<h4><a href="<?php echo base_url(); ?>hub/hubdetails/<?php echo $post["mid"].'/'.$post['hid'];?>"><?php custom_echo($content,200); ?></a></h4>
														<p><?php echo  date("d F Y",strtotime($post['posted_on']));?></p>                        
													</li>
												<?php }?>
											</ul>
										<?php }else{?>
										<span>You have no recent post</span>
										<?php }?>
									</div>
             
          </div>  
      </div>
      <?php if($bussinessdesc!=null){?>
        <div class="compose-inner-bg profile-new-bg">
            <div class="profile-inner">
                <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Business Summary</h2></div>
                <p><?php echo $type = ($bussinessdesc == null) ? "Add Description" : $bussinessdesc; ?></p>                         
            </div>  
        </div>
      <?php }?>
      <div class="compose-inner-bg profile-new-bg">
                <div class="profile-inner">
          <div class="post-heading" style="padding:0 0 15px 0; display:inline-block; width:100%;"><h2>Business objectives</h2></div>
                    
                    <div class="profile-connection-inner">
                        <div class="profile-conntection-left MAR-TOP">
                            <div class="profile-connection-details">
                                <div class="profile-connection-col">
                                    <h4 class="heading">What are you looking for ?</h4>
                                </div>
                                <div class="profile-connection-col">
                                    <?php foreach($questionone as $row){ ?>
                                    <h4><?php echo $row; ?></h4>
                                    <?php } ?>
                                    <!--<h4>Find suppliers Look for non-executive directors <a class="edit-sec"><i class="fa fa-pencil"></i></a> </h4>-->
                                </div>
                            </div>
                            <div class="profile-connection-details">
                                <div class="profile-connection-col">
                                    <h4 class="heading">Where do you want to explore?</h4>
                                </div>
                                <div class="profile-connection-col">
                                      <?php foreach($locality_to_explore as $row){ ?>
                                      <h4><?php echo $row->locality_type; ?></h4>
                                      <?php } ?>
                                    <!--<h4>Global <a class="edit-sec"><i class="fa fa-pencil"></i></a> </h4>-->
                                </div>
                            </div>
                        </div>
                          
                    </div>
                </div>
            </div>     
           
        </div> 
      
   <!-- <div class="compose-inner-bg profile-bg">
      <h3>Connects</h3>
      <ul class="profile-connect">
        <?php foreach($connection_list as $con_list){  ?>
          <li>
            <div class="inbox-img"> <img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $con_list['image_url']?>" alt=""> </div>
            <div class="inbox-content">
              <h4><?php echo $con_list['bussinessname'];?></h4>
              <p class="date"> <?php echo $con_list['bussinesstype'];?></p>
            </div>
          </li>
        <?php }?>
        
      </ul>
    </div>-->
    </div>    
  </div>
  <div id="send_message_modal" style="width:600px; display:none;">
        <div class="popup-box-part-center">
            <form id="send_message_form" method="post">
                <h2>Send Message</h2>
                <div class="business-img-listing-wrap cust-fancy-label-input">
                    <div class="cust-fancy-label-input">
                        <label>Subject</label>
                        <input type="text" name="message_sub" id="message_sub" data-bind="value:message_sub"> 
                    </div>
                    <div class="cust-fancy-label-input"> 
                        <label>Message</label>
                        <textarea data-bind="value:message" name="message"></textarea>
                        <!--<input type="text" name="last_name" id="edit_last_name" data-bind="value:last_name"> -->
                    </div>   
                    <button type="submit" class="button" id="send_message_submit_button">Send</button> 
                </div>      
            </form>    
        </div>
    </div>
</div>
<script>
  var vmOtherProfile={
    profile_user_id:ko.observable(),
    message_sub:ko.observable(''),
    message:ko.observable(''),
    show_msg_btn:ko.observable(true),
    show_requset_send_btn:ko.observable(false),
    show_connet_btn:ko.observable(false),
    connection:ko.observable(0),
    reqestForConnection:function(){
      $.post('<?php echo base_url(); ?>member/reqestForConnection',{'id':vmOtherProfile.profile_user_id},function(result){
        //console.log(result);
        if(result==1){
          vmOtherProfile.show_connet_btn(false);
          vmOtherProfile.show_requset_send_btn(true);
          //vmOtherProfile.show_msg_btn(false);
        }else{
          alert('This is embrassing.Some error occured');
        }
      });
    }

  }
   $(".post-inner ul li img").load(function() {
		var max_size = 180;
		$(".post-inner ul li img").show();
		$(".post-inner ul li img").each(function(i) {
		  if ($(this).height() > $(this).width()) {
			var h = max_size;
			var w = Math.ceil($(this).width() / $(this).height() * max_size);
		  } else {
			var w = max_size;
			var h = Math.ceil($(this).height() / $(this).width() * max_size);
		  }
		  $(this).css({ height: h, width: w });
		});
   });
  $(function(){
	$(".post-inner ul li img").hide();
	$('#send_message_modal_link').fancybox();
    $('.video_popup').fancybox();
    vmOtherProfile.profile_user_id(<?php echo $this->uri->segment(3);?>);   
    try{
      $.post('<?php echo base_url(); ?>member/getConnectRequestDetail',{'id':vmOtherProfile.profile_user_id},function(result){       
        result = JSON.parse(result); 
        //console.log(result);      
        if(result.length==0){
          vmOtherProfile.show_connet_btn(true);
          vmOtherProfile.show_requset_send_btn(false);
          //vmOtherProfile.show_msg_btn(false);
        }else{
          if(result.is_accepted=='0'){
            vmOtherProfile.show_connet_btn(false);
            vmOtherProfile.show_requset_send_btn(true);
            //vmOtherProfile.show_msg_btn(false);
          }
        }
      });
    }catch(e){
      alert(e);
    }
    try{
        $('#loader-wrapper').show();
        $.post('<?php echo base_url(); ?>memberprofile/getViewProfileNetworkConnectionList',{id:vmOtherProfile.profile_user_id()},function(result_connection){
            $('#loader-wrapper').hide();
            var result_connection_list = eval("(" + result_connection + ")");            
            var recieved = 0;
            //console.log(result_connection_list);
            for(var i=0; i< result_connection_list.connection.length;i++){   
              if(result_connection_list.connection[i].is_accepted=='1'){                
                recieved++;
              }      
            }            
            vmOtherProfile.connection(recieved);

        });
    }catch(e){
        alertify.error(e);
    }
    $("#send_message_form").validate({
        rules: {
            message_sub: { required: true },
            message: { required: true }
        },
        messages: {
            message_sub: { required: "Please fill up subject" },
            message: { required: "Please enter the message" }
        },
        submitHandler: function (form) {  
          $.post('<?php echo base_url(); ?>message/sendMessage',{reciever_id:vmOtherProfile.profile_user_id(),subject:vmOtherProfile.message_sub(),message:vmOtherProfile.message()},function(result){
            $('#loader-wrapper').hide();
            if(result==1){
                alertify.success("Message Send");
                $('.fancybox-close').trigger('click');
                vmOtherProfile.message_sub('');
                vmOtherProfile.message('');
            } else {
               alertify.success("Some error occured.Please try again"); 
            }
          });
          return false;
        }
    });
  });
  ko.applyBindings(vmOtherProfile, $("#other_profile_section")[0]);
  
</script>