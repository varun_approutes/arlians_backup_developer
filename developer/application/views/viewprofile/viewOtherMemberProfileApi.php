 <section class="content" ng-controller="viewMemberProfileApiCtrl">
          <div class="row">
            
             <div class="col-md-9">

               <div class="profile_page box">
               <div class="profile_detailinner">
                 <img ng-src="{{profile_image}}" src="<?php echo base_url(); ?>assets_phase2/images/profile_avatar.png">
                 <h4 ng-bind="business_name">Financial Assets Ltd.</h4>
                 <p class="profile_detailstxt" ng-bind="business_tagline"><em>Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness Tag line of the bussiness.</em></p>
                 <strong><span ng-bind="city">city</span> , <span ng-bind="countryName">country</span></strong>
               </div>
               
                 <img class="full_width" ng-src="{{background_image}}" src="<?php echo base_url(); ?>assets_phase2/images/profilepage_img.jpg" alt="">
               </div>
                <!-- profile details -->
                <div class="profile_details box">
                  <ul>
                    <li ><span ng-bind="connection_no">234</span><br>Connections</li>
                    <li ><span ng-bind="post_no">234</span><br>Posts</li>
                    <li ><span ng-bind="publish_no">234</span><br>Publications</li>
                    <li ><span ng-bind="opentalks_no">234</span><br>Open Talks</li>
                  </ul>
                </div>

                 <!-- sectors -->
                <div class="summary box">
                  <div class="box-header">                  
                  Sectors                 
                </div>
                <div class="box-body">
                  <ul class="sector_btn">
                     <li ng-repeat="list_sectorname in sectorname"><a href="javascript:void(0)" class="blue_circlebtn"><span ng-bind="list_sectorname.name">Beauty</span></a>
					 </li>
                     
                  </ul>
                  
                </div>
                </div>

                <!-- summary -->
                <div class="summary box">
                <div class="box-header">
                  <i class="fa  fa-file-text"></i>
                  Summary
                  <a href="#" class="pull-right edit">Edit <i class="fa  fa-toggle-on"></i></a>
                </div>
                <div class="box-body">
                 <div class="mCustomScrollbar" ng-bind-html="business_desc">
                  <p>I am the co-founder and director of Financial Assets - communication and service design for better health, care and wellbeing. We engage and involve people in addressing challenges. People are at the heart of health and wellbeing challenges, so it makes sense to involve them in designing better solutions. We put people at the centre of the problem solving process to ensure we fully understand the issues, and we design the solutions that are cost effective and that deliver impact.I have specialist knowledge of co-design approaches to achieve social impact. My PhD thesis, ‘The emergence and practice of co-design as a method for social sustainability under New Labour’, reviewed how co-design has become a key method to tackle some of the UK’s most difficult social issues. </p>
            <p>Prior to my PhD, I studied design at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
            <p>I am the co-founder and director of Financial Assets - communication and service design for better health, care and wellbeing. We engage and involve people in addressing challenges. People are at the heart of health and wellbeing challenges, so it makes sense to involve them in designing better solutions. We put people at the centre of the problem solving process to ensure we fully understand the issues, and we design the solutions that are cost effective and that deliver impact.I have specialist knowledge of co-design approaches to achieve social impact. My PhD thesis, ‘The emergence and practice of co-design as a method for social sustainability under New Labour’, reviewed how co-design has become a key method to tackle some of the UK’s most difficult social issues. </p>

            </div>
                <!-- summary carousel -->
                <div class="summary_carousel">

                   <div id="summary" class="owl-carousel">
          
					  <div class="item" ng-repeat="list_doc_uploaded in doc_uploaded">
						<div class="summary_imginner">
						<div class="hover_cont">
						  <a href="#" class="hover_btn">Edit</a>
						  <a href="#" class="hover_btn">Delete</a>
						</div>
						<img ng-src="<?php echo base_url()?>{{list_doc_uploaded.path}}{{list_doc_uploaded.file_name}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
						</div>
						<p ng-bind="list_doc_uploaded.file_title">User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
					  </div>

                 <!--<div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... <a href="#" class="watch">watch</a> </p>
                  </div>


                 <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... <a href="#" class="watch">watch</a> </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... <a href="#" class="watch">watch</a> </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>-->

                  </div>

                </div>
                  
                <!-- profile uploader -->
					<ul class="profile_uploader">
					  <li>Upload :</li> 
					  <li><a href="#"><i class="fa  fa-file-text"></i> Document</a></li>
					  <li><a href="#"><i class="fa  fa-file-photo-o"></i> Media</a></li>
					  <li><a href="#"><i class="fa  fa-file-excel-o"></i> Report</a></li>
					  <li><a href="#"><i class="fa  fa-file-powerpoint-o"></i> Presentation</a></li>
					</ul>

                </div>
                  
                </div>

                <!-- publish start -->
                <div class="summary box">
                  <div class="box-header">
                  <i class="fa  fa-file-text"></i>
                  Publish
                  <a class="pull-right edit" href="#">Edit <i class="fa  fa-toggle-on"></i></a>
                </div>
                <div class="box-body">
                   <!--<div class="publish_block1 clearfix">
                     <ul>
                       <li ng-repeat="list_publish in publishList">
						<div class="publish_inner">
							<div class="summary_imginner">
							<div class="hover_cont">
							  <a class="hover_btn" href="#">Edit</a>
							  <a class="hover_btn" href="#">Delete</a>
							</div>
							  <i class="fa  fa-play-circle-o play_btn"></i>
							<img class="summary_img" alt="" ng-src="<?php echo base_url()?>/uploads/hub_images/{{list_publish.link}}"  src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg">
							</div>
							<p ng-bind-html="list_publish.content" >Joint enterprise law wrongly interpreted for 30 years, court rules...<a class="watch" href="#">watch</a> </p>
						  </div>
                       </li>

                      <!-- <li>
                         <div class="publish_inner">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a class="hover_btn" href="#">Edit</a>
                      <a class="hover_btn" href="#">Delete</a>
                    </div>
                     <i class="fa   fa-file-text-o play_btn"></i>
                    <img class="summary_img" alt="" src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg">
                    </div>
                    <p>Joint enterprise law wrongly interpreted for 30 years, court rules...<a class="watch" href="#">watch</a> </p>
                  </div>
                       </li>

                       <li>
                         <div class="publish_inner">
                          <p><strong>Title/  City blows into Australian cityFast-growing tumbleweed.</strong></p>
                    <p>begining of the articlebegining of the articlebegining of the article
                    begining of the articlebegining of the articlebegining of the articlebeginittng of the article
                    begining of the articlebegining  articlebegining of the article
                    begining or...<a class="watch" href="#">read</a></p>
                  </div>
                       </li>

                     </ul>                     

                   </div>-->

                   <div class="publish_line"></div>
                     
                      <!-- publish carousel -->

                    <div class="publish_carousel">

                   <div id="publish" class="owl-carousel">
          
                  <div class="item" ng-repeat="list_publish in publishList">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
					<span ng-if="list_publish.link != ''">
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" ng-src="<?php echo base_url()?>/uploads/hub_images/{{list_publish.link}}" alt="" class="summary_img"></span>
					</div>
                    
                    <p ng-bind-html="list_publish.content">User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>

                <!-- <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="up_photo">
                      <button class="change_photos"><i class="fa fa-camera"></i></button>
                    </div>
                    
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... <a href="#" class="watch">watch</a> </p>
                  </div>


                 <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... <a href="#" class="watch">watch</a> </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... <a href="#" class="watch">watch</a> </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>

                  <div class="item">
                    <div class="summary_imginner">
                    <div class="hover_cont">
                      <a href="#" class="hover_btn">Edit</a>
                      <a href="#" class="hover_btn">Delete</a>
                    </div>
                    <img src="<?php echo base_url(); ?>assets_phase2/images/summary_img1.jpg" alt="" class="summary_img">
                    </div>
                    <p>User document Joint enterprise law wrongly interpreted for 30 years, court... </p>
                  </div>-->

                  </div>

                </div>


                </div>
                </div>

                <!-- community activity -->
                <!--<div class="summary box cummunity_activity">
                  <div class="box-header">
                  <i class="fa  fa-file-text"></i>
                  Community Activity                 
                </div>
                <div class="box-body">

                   <div class="mCustomScrollbar">

                  <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>
                  <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> LIVE TALK <span>/ Schedule or when it been started and date</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>
                  <div class="community_box">
                    <h6 class="community_heading"><i class="fa  fa-align-justify"></i> POST <span>/ Published the 12/04/2016 </span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>

                   <div class="community_box">
                    <h6 class="community_heading"><i class="fa fa-users"></i> TALK <span>/ Started the 12/04/2016</span></h6>
                   <strong>Talk title set by the user will appear here. </strong> 
        <p>Description of the talk will appear here / at Goldsmiths College, where my work earned me the Business Design Centre’s ‘New Designer of the Year’ title.</p>
                  </div>
                  </div>

                </div>
                </div>-->

             </div>    
            <!-- tab Start -->
            

            <!-- all matching -->
           
                       
          </div>
        </section>
		<script src="<?php echo base_url(); ?>assets_phase2/js/viewMemberProfileApi.js"></script>
		<script>
		$(document).ready(function() {
		 
		});
(function($){
  $(window).load(function(){

    $("#content-1").mCustomScrollbar({
      axis:"yx", //set both axis scrollbars
      advanced:{autoExpandHorizontalScroll:true}, //auto-expand content to accommodate floated elements
      // change mouse-wheel axis on-the-fly 
      callbacks:{
        onOverflowY:function(){
          var opt=$(this).data("mCS").opt;
          if(opt.mouseWheel.axis!=="y") opt.mouseWheel.axis="y";
        },
        onOverflowX:function(){
          var opt=$(this).data("mCS").opt;
          if(opt.mouseWheel.axis!=="x") opt.mouseWheel.axis="x";
        },
      }
    });

  });
})(jQuery);
</script>