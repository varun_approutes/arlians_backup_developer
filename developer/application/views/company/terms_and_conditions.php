<div class="terms-wrap">
      <h2>Terms</h2>
      <h4>1. Introduction </h4>
      <ul>
<li>1.1	Arlians is the dating site of businesses. It is designed to help businesses network, communicate, and collaborate across the globe  </li>
<li>1.2	The Arlians website is operated by Arlians Limited ("we", “our”, “us”). We are registered in England and Wales under company number 09447080 and have our registered office at Level 3, 207 Regent Street, London, W1B 3HH. Our main trading address is Unit 3.05 Clerkenwell Workshops, 31 Clerkenwell Close, London, EC1R 0AT. We are a limited company. </li>
<li>1.2	By accessing, using and/or purchasing a subscription to the Arlians website you are agreeing to enter into a binding contract with us on these terms and conditions, including our Website Terms of Use and our Privacy Policy. </li>
<li>1.3	If you do not agree to these terms and conditions or Website Terms of Use or Privacy Policy please do not access, use or subscribe to the Arlians website. </li>
</ul>

<h4>2.	 Your status</h4>
<p>
The Arlians website is directed exclusively at commercial enterprises and individuals acting in a business capacity. You agree that you will be using the Arlians website for your own business purposes only. 
</p>

<h4>
3.	Your right to use the Arlians website
</h4>
<ul>
<li>
3.1	Subject to clauses 3.2 and 3.3 below and your compliance with these terms and conditions and the Website Terms of Use we grant to you a non-exclusive, non-transferable right to use the Arlians website solely for your internal business operations. 
</li>
<li>
3.2 You acknowledge that certain features of the Arlians website (“Restricted Features”) will only be available to you pursuant to clause 3.1 if you have purchased a subscription to the Arlians website (“Subscription”). 
</li>
<li>
3.3 If have purchased a Subscription, you shall be permitted to use the Restricted Features for the duration of the Subscription Period (as defined in clause 11 below).
</li>
<li>
3.4 We do not guarantee the availability of the Arlians website at any time or that the Arlians website will be un-interrupted or error free.
</li>
<li>
3.5 The Arlians website is directed to people residing in the United Kingdom. We do not represent that content available on or through the Arlians website is appropriate or available in other locations. We may limit the availability of the Arlians website to any person or geographic area at any time. If you choose to access our site from outside the United Kingdom, you do so at your own risk.
</li>
<li>	
3.6 In addition to these terms and conditions, in using the Arlians website you agree to comply with our Website Terms of Use
</li>
</ul>


<h4>4.Subscription Fee</h4>
<ul>
<li>4.1	If you wish to purchase a Subscription you agree to pay the applicable fees and taxes from time to time as set out on the Arlians website (“Subscription Fee”). If you fail to pay such fees and taxes, we may terminate your Subscription and your right to access and use the Restricted Features will terminate. </li>
<li>4.2	The Subscription Fee is payable by the methods set out on the Arlians website and is due on the first day of each Subscription Period. You authorise us to store your payment method (e.g. credit card details) in order to avoid interruptions to the Restricted Features and to facilitate the renewal of the Subscription Periods.
</li>
<li>4.3	If we have not received payment of the Subscription Fee by the relevant due date (whether due to incorrect payment details being entered or otherwise), and without prejudice to any of our other rights and remedies then we may, without liability to you, disable your access to the Restricted Features and we shall be under no obligation to provide any or all of the Restricted Features while the Subscription Fee remains unpaid. </li>
<li>	
4.4 The Subscription Fee: <br> <br>
<ul>
<li>4.4.1 is non-cancellable and non-refundable; and </li>
<li>4.4.2 is exclusive of any applicable taxes (such as value added tax), which shall be added to the Subscription Fee at the appropriate rate. </li>
</ul>
</li>
<li>
4.5 The Subscription Fee you pay may be subject to foreign exchange fees or differences in prices based on location (e.g. exchange rates) and you shall be solely responsible for these.
</li>
<li>
4.6 We shall be entitled to increase the Subscription Fee at the start of each Subscription Period.
</li>
</ul>

<h4>5.Your Data </h4>
<ul>
<li>5.1 As between you and us, you own all right, title and interest in and to all data, information and content that you submit to the Arlians website <strong>(“Your Data”)</strong> and shall have sole responsibility for the legality, reliability, integrity, accuracy and quality of Your Data.</li>
<li>5.2	You acknowledge that as the Arlians website is not a storage service, we have no obligation to store, maintain or provide you with a copy of Your Data except to the extent required by law. Accordingly you will be solely responsible for securing and backing up Your Data. </li>
<li>5.3	Subject to clause 5.4 below, by submitting Your Data to the Arlians website you grant to us a non-exclusive, worldwide, transferrable and sub-licensable right to copy, modify, distribute, publish and process Your Data without providing you with any further notice or obtaining from you any further consent. 
    <br>
	You also agree that other users of the Arlians website may access and share Your Data. </li>
<li>5.4	To the extent that you submit any personal data to the Arlians website we shall treat such personal data in accordance with the Privacy Policy as such document may be amended from time to time by us in our sole discretion. </li>
<li>5.5	You agree that Your Data will be accurate and that you have right to submit Your Data to the Arlians website. </li>
<li>5.6 We reserve the right to remove Your Data from the Arlians website at any time and at our discretion. </li>
<li>5.7	We have the right to disclose your identity to any third party who is claiming that any of Your Data constitutes a violation of their intellectual property rights, or of their right to privacy. </li>
</ul>

<h4>6.Third parties </h4>
<ul>
<li>6.1 You acknowledge that the Arlians website may enable or assist you to access the website content of, correspond with, and purchase products and services from, third parties. Any such activity carried out by you done so solely at its own risk. </li>
<li>6.2	We make no representation or commitment and shall have no liability or obligation whatsoever in relation to the content or use of, or correspondence with, any such third-party or any such third party website, or any transactions completed, and any contract entered into by you, with any such third party.  Any contract entered into and any transaction completed with a third party is between you and the relevant third party, and not us. 
</li> 
<li>6.3	We do not endorse or approve any third party, any third party’s products or services nor the content of any of the third party website made available via the Arlians website. </li>
<li>6.4	The views expressed by others on the Arlians website do not represent our views. </li>
</ul>
<h4>7. Proprietary rights </h4>
<p>
You acknowledge and agree that we and/or our licensors own all intellectual property rights in the Arlians website. Except as provided above you do not have any rights to, or in, patents, copyright, database right, trade secrets, trade names, trade marks (whether registered or unregistered), or any other rights or licences in respect of the Arlians website.
</p>
<h4>8. No reliance on information </h4>
<ul>
<li>8.1 The content on the Arlians website is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on the Arlians website. </li>
<li>8.2 Although we make reasonable efforts to update the information the Arlians website, we make no representations, warranties or guarantees, whether express or implied, that the content on our site is accurate, complete or up-to-date. </li>
</ul>
<h4>9. Indemnity</h4>
<p>
You agree to defend, indemnify and hold us harmless against claims, actions, proceedings, losses, damages, expenses and costs (including without limitation court costs and reasonable legal fees) arising out of or in connection with your use of the Arlians website including but not limited to any breach by you of the Website Terms of Use.
</p>

<h4>10.	Limitation of Liability</h4>
<ul>
<li>10.1	This clause 10 sets out the entire financial liability of us (including any liability for the acts or omissions of our employees, agents and sub-contractors) to you: <br> <br>
<ul>
	<li>10.1.1 arising under or in connection with these terms and conditions; </li>
<li>10.1.2 in respect of any use made by you of the Arlians website; and  </li>
	<li>10.1.3 in respect of any representation, statement or tortious act or omission (including 	negligence) arising under or in connection with the Arlians website. </li>
    </ul>
</li><li>10.2	Except as expressly and specifically provided in these terms and conditions:
<br><br>
<ul>
<li>
10.2.1 you assume sole responsibility for the results obtained from your use of the Arlians website and for conclusions drawn from such use. We shall have no liability for any damage caused by errors or omissions in any information, instructions or scripts provided to us by you; </li>
<li>
10.2.2	all warranties, representations, conditions and all other terms of any kind whatsoever 	implied by statute or common law are, to the fullest extent permitted by applicable 	law, excluded from the agreement between you and us; and </li>
    <li>
10.2.3	the Arlians website is provided to you on an "as is" basis. </li>
    </ul>
        </li>
        <li>
10.3	Nothing in these terms and conditions excludes our liability: <br><br>
<ul>
	<li>10.3.1	for death or personal injury caused by our negligence; or</li>
	<li>10.3.2 for fraud or fraudulent misrepresentation. </li>
    </ul>
    </li>
    <li>
 10.4	Subject to clause 10.2 and clause 10.3: <br><br>
<ul>
	<li>10.4.1	we shall not be liable to you whether in tort (including for negligence or breach of 	statutory duty), contract, misrepresentation, restitution or otherwise for any loss of 	profits, loss of business, depletion of goodwill and/or similar losses or loss or 	corruption of data or information, or pure economic loss, or for any special, indirect or 	consequential loss, costs, damages, charges or expenses however arising in 	connection with the Arlians website; and </li>
            <li> 10.4.2	our total aggregate liability to you in, tort (including negligence or breach of statutory duty), misrepresentation, restitution or otherwise, arising in connection with 	the Arlians website shall be limited to the total 	Subscription Fee paid during the 12 months immediately preceding the date on which the claim arose.</li>
            </ul>
            </li>
            </ul>
            <h4>11.	Term and Termination</h4>
            <ul>
<li>11.1	If you have purchased a Subscription, your Subscription shall commence on the date you 	purchased the Subscription and shall continue for one month and, thereafter, the 	Subscription 	shall be automatically renewed for successive periods of one month (each month a 	“Subscription Period”). </li>
<li>11.2	We may terminate your Subscription immediately if you are in material breach of any of the 	terms and conditions including if you breach any of our Website Terms of Use. You may terminate your Subscription at any time on 30 days’ notice in writing to us at <a href="#"> subscriptions@arlians.com </a>. </li>
<li>11.3 Any rights that have accrued to either party at the date of termination will remain enforceable after termination. </li>
</ul>
 <h4>	General </h4>
 <ul>
<li>12.1	If any provision (or part of a provision) of these terms and conditions is found by any court or administrative body of competent jurisdiction to be invalid, unenforceable or illegal, the other provisions shall remain in force. </li>
<li>12.2	We reserve the right at any time without notice to revise the content of our site (including the services offered by us) and these terms and conditions. Any changes to these terms and conditions will be posted on our site and by continuing to use our site following any such change you will signify that you agree to be bound by the revised terms and conditions of use. </li>
<li>12.3 These terms and conditions (and any dispute, controversy, proceedings or claim of whatever nature arising out of or in any way relating to them or their formation) shall be governed by and interpreted in accordance with the laws of England and Wales and, for these purposes, the parties submit to the exclusive jurisdiction of the courts of England and Wales. </li>
     </ul>        
      </div>