<div class="terms-wrap">
          
<h2>Privacy Policy</h2>
<p>
Our mission at Arlians Limited ("we") is to help businesses network, communicate, and collaborate across the globe. Our “members” share relevant information with us and communicate with other such “members”. We believe that collecting such information provides great benefit to users of Arlians. In doing so, we make your privacy one of our top priorities, this policy is intended to set out what we may do with collected information in order to provide the best service, and adhere to industry standards. This policy sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us.  Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By visiting any of <a href="#">www.arlians.com,</a> <a href="#">www.arlians.net,</a> www.arlians.org, www.arlians.co.uk (“the Arlians website”) you are accepting and consenting to the practices described in this policy. 
<br><br>
For the purpose of the Data Protection Act 1998 (the “Act”), the data controller is Arlians Limited of Level 3, 207 Regent Street, London, W1B 3HH.
</p>

<h4>Information we may collect from you</h4>
<p>We may collect and process the following data about you:<br><br>
Information you give us. You may give us information about you by filling in forms on the Arlians website (including by creating a profile) or by corresponding with us by phone, e-mail or otherwise. This includes information you provide when you register to use our site, subscribe to the Arlians website  and when you report a problem with our site. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information, personal description and photograph. </p>
<br>
<ul>
<li>
Information we collect about you. With regard to each of your visits to our site (as is normal) we may automatically collect the following information:
technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform; 
</li>
<li>
information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our site (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our customer service number. 
</li>
</ul>
<p>
Information we receive from other sources. We may receive information about you if you use any of the other websites we operate or the other services we provide. We are also working closely with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, credit reference agencies) and may receive  information about you from them.</p>

<h4>Cookies</h4>
<p>
The Arlians website uses cookies to distinguish you from other users of the Arlians Website. This helps us to provide you with a good experience when you browse the Arlians Website and also allows us to improve it. More information about the cookies we use and how you can block them is available from our Cookies Policy</p>
<h4>Uses made of the information</h4>
<p>We use information held about you in the following ways:</p>
<br>
<p>Information you give to us. We will use this information:</p>
<ul>
<li>
to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and services that you request from us;</li>
<li>
to provide you with information about other goods and services we offer that are similar to those that you have already purchased or enquired about;
to notify you about changes to our service;</li>
<li>
to ensure that content from the Arlians website is presented in the most effective manner for you and for your computer. 
</li>
</ul>

<h4>Information we collect about you. We will use this information:</h4>
<ul>
<li>to administer the Arlians website and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;
</li>
<li>
to improve the Arlians website to ensure that content is presented in the most effective manner for you and for your computer; 
</li>
<li>
to allow you to participate in features of our service, when you choose to do so, for example, by placing information on the Arlians website;
</li>
<li>
as part of our efforts to keep the Arlians website safe and secure;
</li>
<li>
to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you;
</li>
<li>
to make suggestions and recommendations to you and other users of [website name to be inserted] about goods or services that may interest you or them.
</li>
</ul>
<p>
Information we receive from other sources. We may combine this information with information you give to us and information we collect about you. We may use this information and the combined information for the purposes set out above (depending on the types of information we receive).
</p>

<h4>Disclosure of your information</h4>
<p>We may share your information with selected third parties including:</p>
<ul>
<li>business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you;</li>
<li>analytics and search engine providers that assist us in the improvement and optimisation of the Arlians website.</li>
</ul>

<h4>We may disclose your personal information to third parties:</h4>
<ul>
<li>in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;</li>
<li>If Arlians Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.</li>
<li>If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce any agreement we have with you; or to protect the rights, property, or safety of Arlians, our customers, or others. </li>
<li>By entering your own information on the Arlians website, you acknowledge that other users of the Arlians website will have access to that information and that we have no control over the use of that information by third parties. Do not place on the Arlians website any information that you are not comfortable sharing with other users of the Arlians website. </li>
</ul>

<h4>Where we store your personal data</h4>
<p>
The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things,  the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing.  <br> <br>
Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.  <br>  <br>
Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.  <br>  <br>
</p>

<h4>Your rights</h4>
<p>
You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data.  You can also exercise the right at any time by contacting us at support@arlians.com. <br><br>
The Arlians website may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates.  If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies.  Please check these policies before you submit any personal data to these websites. </p>

<h4>Access to Information</h4>
<p>
The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.
</p>
<h4>Changes to our Privacy Policy</h4>
<p>
Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.
</p>
<h4>Contact</h4>
<p>
Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to <a href="#"> hello@arlians.com. </a>
</p>
      </div>