<?php $mid = $this->session->userdata['logged_in']['id'];

//------
$user = getParticularUserDetails($mid);

$user_own_type = $user['bussinesstype1'];

 ?>
<script>
jQuery(document).ready(function(){

    jQuery('.fa-check').click(function(){
        var connected_id = jQuery(this).parent('a').parent('div').parent('li').attr('id');
        var mid = "<?php echo $mid; ?>";

        jQuery.post('<?php echo base_url(); ?>suggestion/acceptRequestForConnection',{'connected_id':connected_id,'mid':mid},function(res){
            if(res == "true"){
               location.reload(); 
            }
        });
    });

    jQuery('.fa-times').click(function(){
        var connected_id = jQuery(this).parent('a').parent('div').parent('li').attr('id');
        var mid = "<?php echo $mid; ?>";

        jQuery.post('<?php echo base_url(); ?>suggestion/deleteRequestForConnection',{'connected_id':connected_id,'mid':mid},function(res){
            if(res == "true"){
               location.reload(); 
            }
        });
    });
});
</script>
<div class="dashboard-wrap">
    <div class="row">
        <div class="fl100">
            <div class="compose-inner-bg profile-bg">
                <div class="profile-inner search-inner">
                    <h3>Search Results</h3>
                    <ul class="profile-connect">

                        <?php 
                        $new_member_ids = array();
                        //echo '<pre>'; var_dump($last_suggested_id); die();
                        foreach($last_suggested_id as $suggested_id){

                            $curr_id = $suggested_id['suggested_id']; 
                            $user = getParticularUserDetails($curr_id);


                            $suggested_user_type = userProfType($curr_id);

                            $looking_for_id = $looking_for[0];

                            $result = checkToShowUserInSearchResult($looking_for_id,$user_own_type);

                               if(strtolower($suggested_user_type) == strtolower($user_own_type) || $result == "BOTH"){
                                   
                                    $new_member_ids[] = $curr_id;

                           
                         ?>  

                            <li id="<?php echo $curr_id; ?>">
                            <div class="inbox-img"> <a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $curr_id; ?>"> <img src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $user['profile_image']?>" alt=""> </a></div>
                            <div class="inbox-content">
                              <h4><a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $curr_id; ?>"><?php echo $user['bussinessname'];?></a></h4>
                              <p class="date"> <?php echo $user['bussinesstype1'];?></p>
                            </div>
                            <div class="right-inbox-icon">
                                <?php /*$is_connected = is_connected($mid,$con_list);
                                    if($is_connected != 'true'){
                                 ?>
                                <a href="javascript:void(0);" class="icon-border" >
                                    <i class="fa fa-check fa-1"></i>
                                </a>
                                <?php } else { ?>
                              <a href="javascript:void(0);" class="icon-border"  >
                                <i class="fa fa-times fa-1"></i>
                              </a>
                              <?php } */?>
                            </div>
                          </li>
                          <?php 
                            }
                        
                          ?>
                        <?php } 

                        ?>
						 <?php if(count($last_suggested_id)==0){ echo ' <li> <p class="date">No new result found. maybe they are already added in your connects </p></li>' ;}
                    if(isset($error_msg)){
                      echo $error_msg;
                    }
             ?>
                    </ul>
                </div>
            </div>
        </div>
       
    </div>

</div>