<?php 

 $mid = $this->session->userdata['logged_in']['id']; 
//getLookingForById(1);


$last_suggested_id_new = array();

$type_converter = array('5a'=>'1','5b'=>'2','2'=>'3','1'=>'4','4a'=>'5','4b'=>'6','3a'=>'7','3b'=>'8','3c'=>'9');

foreach($last_suggested_id as $key=>$row){

	$last_suggested_id_new[$type_converter[$key]] = $row;
}

//echo '<pre>'; print_r($last_suggested_id_new); die();
//------
$user = getParticularUserDetails($mid);
$blocked_ids_by_user = listOfSuggestionBlockIds($mid);
$user_own_type = $user['bussinesstype1'];
 ?>
<script>
jQuery(document).ready(function(){

    jQuery('.fa-check').click(function(){
        var connected_id = jQuery(this).parent('a').parent('div').parent('li').attr('id');
        var mid = "<?php echo $mid; ?>";

        jQuery.post('<?php echo base_url(); ?>suggestion/acceptRequestForConnection',{'connected_id':connected_id,'mid':mid},function(res){
            if(res == "true"){
               location.reload(); 
            }
        });
    });

    jQuery('.fa-times').click(function(){
        var connected_id = jQuery(this).parent('a').parent('div').parent('li').attr('id');
        var mid = "<?php echo $mid; ?>";

        jQuery.post('<?php echo base_url(); ?>suggestion/deleteRequestForConnection',{'connected_id':connected_id,'mid':mid},function(res){
            if(res == "true"){
               location.reload(); 
            }
        });
    });
});
</script>

  <script type="text/javascript">
      // Wait until the DOM has loaded before querying the document
      $(document).ready(function(){
        $('ul.tabs').each(function(){
          // For each set of tabs, we want to keep track of
          // which tab is active and it's associated content
          var $active, $content, $links = $(this).find('a');

          // If the location.hash matches one of the links, use that as the active tab.
          // If no match is found, use the first link as the initial active tab.
          $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
          $active.addClass('active');

          $content = $($active[0].hash);

          // Hide the remaining content
          $links.not($active).each(function () {
            $(this.hash).hide();
          });

          // Bind the click event handler
          $(this).on('click', 'a', function(e){
            // Make the old tab inactive.
            $active.removeClass('active');
            $content.hide();

            // Update the variables with the new link and content
            $active = $(this);
            $content = $(this.hash);

            // Make the tab active.
            $active.addClass('active');
            $content.show();

            // Prevent the anchor's default click action
            e.preventDefault();
          });
        });
      });
    </script>
        <div class="dashboard-wrap">
		<ul class="tabs">
			<?php //print_r($last_suggested_id); exit;?>
			<?php foreach($last_suggested_id_new as $key=>$row){ ?>
				<li><a href="#tab_<?php echo $key; ?>"><?php echo getLookingForById($key); ?></a></li>
			<?php }?>			
		</ul>
		
        <?php foreach($last_suggested_id_new as $key=>$val){ ?>          	
				
			<div id="tab_<?php echo $key;?>" class="tab-content">
				<div class="suggestion-message-heading">
				<?php if($key==1){ $title= 'strategic alliances';}
					  elseif($key==2){ $title= 'M&A opportunities';}
					  elseif($key==3){ $title= 'customers';}
					  elseif($key==4){ $title= 'suppliers';}
					  elseif($key==5){ $title= 'Seek investor';}
					  elseif($key==6){ $title= 'investors';}
					  elseif($key==7){ $title= 'non-executive directors positions';}
					  elseif($key==8){ $title= 'Find non-executive directors';}
					  elseif($key==9){ $title= 'Connect with freelance professionals';}
					?>
					<h2>
					Potential <?php echo $title; ?>
					</h2>
				</div>
	        	<?php  $id_str = $val[0]['suggested_ids']; $value = explode(',', $id_str)  ?>
				<div class="row suggestion-row">       
				<div class="sub-heading-top customer-matchs">Possible Matches</div>
				<div class="sugges-midd-head match-score-top">
					<div class="match-score-heading">
						<div class="sub-heading-top">Match Score</div> 
					</div>
					<div class="arlians-say-heading">
						<div class="sub-heading-top">Arlians Says</div>
					</div>
					<div class="suggestion-action-heading">
						<div class="sub-heading-top">Actions</div>
					</div>
				</div>
				<div class="sub-heading-top feedback">Feedback</div>
					<ul class="inbox-message-list right-inbox-message-list">
						<?php 
						//echo '<pre>'; var_dump($value); die();
							$new_member_ids = array();
						
							foreach($value as $suggested_id){
							if (in_array($suggested_id, $blocked_ids_by_user))
								continue;  
							$curr_id = $suggested_id; 
							//print_r($curr_id);
							//echo '<br />';
							//print_r($user);
							$suggested_user_type = userProfType($curr_id);
							$looking_for_id = $key;
							
							//echo $user_own_type;
							
							$result = checkToShowUserInSearchResult($looking_for_id,$user_own_type);
							//var_dump(strtolower($suggested_user_type) .'=='. strtolower($user_own_type.'=='.$curr_id.'<br />') );
								if(strtolower($suggested_user_type) == strtolower($user_own_type) || $result == "BOTH"){
								
								  $is_score=isScoreNotZero($curr_id,$mid);
								  //print_r($curr_id.'=='.$is_score);
							//echo '<br />';
								   	  if( $is_score!=0){
										 if($mid!=$curr_id){
											$new_member_ids[] = $curr_id;
										 }
								  }
							}
						}
						//print_r($new_member_ids);
						//	echo '<br />';
						?> 
						<?php //var_dump($new_member_ids);
							foreach($new_member_ids as $row){ 
							$user = getParticularUserDetails($row);
						?>
							<li id="<?php echo $row; ?>">
								<div class="fl33">
									<div class="inbox-img">
										<img alt="" src="<?php echo base_url(); ?>uploads/profile_image/<?php echo $user['profile_image']?>">
									</div>
									<div class="inbox-content">
										<h4>
											<a href="<?php echo base_url(); ?>viewprofile/profile/<?php echo $row; ?>"><?php echo $user['bussinessname'];?></a>
										</h4>
										<p class="inbox-short-content"> <?php echo $user['tag_line'];?></p>
									</div>  
								</div>  
								<div class="fl47">
									<div class="match-score">
										<div class="score-img">
										<?php $score= matchScore($row,$mid,$key)?>
										<img src="<?php echo base_url(); ?>assets/images/match-score-<?php echo $score; ?>.png" alt="">
										</div>
									</div>
									<div class="arlians-say">
										<p><?php echo matchScoretext($row,$mid,$key)?></p>
									</div>
									<div class="suggestion-action">
										<div class="inbox-reply-delet">
										<?php $details = getConnectRequestDetail($row);
											
											if($details == NULL){
										?>
											<a class="delet" href="javascript:void(0);" onclick="reqestForConnection(this,<?php echo $row;?>)">Connect</a>
											<?php } else {?>
											<?php if($details->is_accepted == 1){ ?>
											<a class="reply" href="#send_message_modal" onclick="linkMessageUser(<?php echo $row;?>);" id="send_message_modal_link">Message</a>															
											<?php 		} else {   ?> 
											<a class="delet" href="javascript:void(0);">Request Sent</a>
														<?php } ?>
										<?php 	} ?>
										</div>
									</div>
								</div> 
								<div class="fr20">
									<div class="relevant-wrap">
									<?php  $is_exit = Feedbackdetails($mid,$row);?>
										<div class="relevant-left">											
											<a href="javascript:void(0);" onclick="configRelevant(this,'1',<?php echo $row;?>)"><div class="relevant-img <?php if($is_exit==1){?>relevant-hover<?php }?>"></div><br>Relevant</a>
										</div>
										<div class="relevant-right">
											<a href="javascript:void(0);" onclick="configRelevant(this,'0',<?php echo $row;?>)"><div class="relevant-img <?php if($is_exit==0){?>relevant-hover<?php }?>"></div><br>Not Relevant</a>
										</div>
									</div>
									<div class="relevant-business">
									<?php 
										if($details!=NULL){
									if($details->is_accepted == 1){?>
										<input type="checkbox" id="check" name="check" checked >
									<?php }elseif($details->is_accepted == 0 ){?>
										
										<input type="checkbox" id="check" name="check" >
										<?php } }?>
										<?php if($details==NULL ){?>
										
										<input type="checkbox" id="check" name="check" >
										<?php }?>
										<p>already doing business with this member</p>
									</div>
								</div>                       
							</li>
						<?php 
							}
						?>
						<?php if(count($last_suggested_id)==0){ echo ' <li> <p class="date">No new result found. maybe they are already added in your connects </p></li>' ;}
						if(isset($error_msg)){
						echo $error_msg;
						}
						?>
					</ul>
				</div>
           </div>

                    <!--<div id="tab2" class="tab-content">
							<span style="color:#000">Lorem ipsum</span>
                    </div>
                    <div id="tab3" class="tab-content">
							<span style="color:#000">Lorem ipsum dummy text</span>
                    </div>
                  
                   </div>-->
        <?php }?>  
<div id="send_message_modal" style="width:600px; display:none;">
	<div class="popup-box-part-center">
		<form id="send_message_form" method="post">
			<h2>Send Message</h2>
			<div class="business-img-listing-wrap cust-fancy-label-input">
				<div class="cust-fancy-label-input">
					<label>Subject</label>
					<input type="text" name="message_sub" id="message_sub" data-bind="value:message_sub"> 
				</div>
				<div class="cust-fancy-label-input"> 
					<label>Message</label>
					<textarea data-bind="value:message" name="message"></textarea>
					<!--<input type="text" name="last_name" id="edit_last_name" data-bind="value:last_name"> -->
				</div>   
				<button type="submit" class="button" id="send_message_submit_button">Send</button> 
			</div>      
		</form>    
	</div>
</div>

<script>
var vmMessage = {
		profile_user_id:ko.observable(),
		message_sub:ko.observable(),
		message:ko.observable(),
}
$( window ).load(function() {
  $('ul.tabs li:first').click();
  $('#send_message_modal_link').fancybox();
  $("#send_message_form").validate({
        rules: {
            message_sub: { required: true },
            message: { required: true }
        },
        messages: {
            message_sub: { required: "Please fill up subject" },
            message: { required: "Please enter the message" }
        },
        submitHandler: function (form) {  
          $.post('<?php echo base_url(); ?>message/sendMessage',{reciever_id:vmMessage.profile_user_id(),subject:vmMessage.message_sub(),message:vmMessage.message()},function(result){
            $('#loader-wrapper').hide();
            if(result==1){
                alertify.success("Message Send");
                $('.fancybox-close').trigger('click');
                vmMessage.message_sub('');
                vmMessage.message('');
				vmMessage.profile_user_id('');
            } else {
               alertify.success("Some error occured.Please try again"); 
            }
          });
          return false;
        }
    });
});
function reqestForConnection(obj,request_id){
	try{
      $.post('<?php echo base_url(); ?>member/reqestForConnection',{'id':request_id},function(result){
        console.log(result);
        if(result==1){
			$(obj).text('Request Send');
			$(obj).prop('onclick',null).off('click');
			alertify.success('Request send');
        }else{
          alertify.error('This is embrassing.Some error occured');
        }
      });
    }catch(e){
      alert(e);
    }
}
function linkMessageUser(reciever_id){	
	vmMessage.profile_user_id(reciever_id);

	//alert(vmMessage.profile_user_id());
}
function setval(searchid){
	$.post('<?php echo base_url(); ?>suggestion/setTabValue',{id:searchid},function(res){
		alert(res);
	});
}
function configRelevant(obj,rel_type,suggested_id){	
	//debugger;
	var parent;
	$.post('<?php echo base_url(); ?>suggestion/configRelevant',{suggested_id:suggested_id,rel_type:rel_type},function(res){
		if(res==1){
			if(rel_type==0){		
				parent = $(obj).parent().parent();
				$(parent).find('.relevant-hover').removeClass("relevant-hover");
				$(obj).find('div').addClass('not-relevant-hover');
			}else{
				parent = $(obj).parent().parent();
				$(parent).find('.not-relevant-hover').removeClass('not-relevant-hover');
				$(obj).find('div').addClass('relevant-hover');
			}
		}
	});
}
ko.applyBindings(vmMessage, $("#send_message_modal")[0]);
</script>