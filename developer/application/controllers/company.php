<?php 
class Company extends CI_Controller
{
    function __construct(){

		parent::__construct();
		
		$this->load->library('User_Manager');		
		$this->load->helper('text');				
		$this->user_manager = User_Manager::get_instance();
	}
	
	function index(){		
						
	}

	function privacyPolicy(){
        $this->load->view('company/privacy_policy');
    }

    function termsAndConditions(){
        $this->load->view('company/terms_and_conditions');
    }

    function aboutArlians(){
        $this->load->view('company/about_arlians');
    }
	
}