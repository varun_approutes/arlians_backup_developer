<?php 
class Search extends CI_Controller
{
    function __construct(){

		parent::__construct();
		$this->load->model('model_common');
		$this->load->model('model_member');
		$this->load->model('model_message');
		$this->load->library('User_Manager');
		$this->load->model('model_suggestion');
		$this->load->model('model_search');
		$this->load->helper('common_helper');
		$this->load->helper('text');
		$this->model_common->SiteSettingsDetails(); // This is mandatory
		$this->load->model('model_othermembersprofile');
		$this->user_manager = User_Manager::get_instance();
	}

	function acceptRequestForConnection(){
		$mid = (int)$_POST['mid'];
		$connected_id = (int)$_POST['connected_id'];

		echo $return = $this->model_suggestion->acceptRequestForConnection($mid,$connected_id);
		exit;
	}

	function deleteRequestForConnection(){
		$mid = (int)$_POST['mid'];
		$connected_id = (int)$_POST['connected_id'];

		echo $return = $this->model_suggestion->deleteRequestForConnection($mid,$connected_id);
		exit;
	}

	function advsearch(){		
		$text_search = $_POST['text'];
		$search_type = $_POST['search_type'];
		$data['country'] = $country = $_POST['selected_country'];
		$data['last_suggested_id']=$this->model_search->advSearch($text_search,$country,$search_type);
		//var_dump($data);
		//exit;
		$this->load->view('search/advsearch',$data);

	}

	function arrayFormat($arr){
		$main = array();

		foreach($arr as $key=>$value){
			$main[$key] = $value[0];

		}

		return $main;
	}

	function array_merge_new($arr1,$arr2){
		$arr3 = array();

		foreach($arr1 as $key=>$value){

			$arr3[$key]['own_industry'] = $value;
			$arr3[$key]['location'] = "M";
			$arr3[$key]['target_industry'] = $arr2[$key];
		}
		//echo '<pre>'; var_dump($arr3); die();
		return $arr3;
	}

	function makeSuggestedListForloginuserMod(){
		//$sids = $_POST['looking_for'];
		$mid = $this->session->userdata['logged_in']['id'];
		$sids = $this->model_suggestion->suggestionSearchTypes($mid);
	 	$data = array();
		$searchT = array();
		$type = array();

		foreach($sids as $sid){
			$s = explode('-', $sid);
			$searchT[] = $s[0];
			$type[] = $s[1];
		}	

		//echo '<pre>'; var_dump($type);
		 foreach ($type as $ty) {

		 	$data['last_suggested_id'][$ty] = $this->model_suggestion->getSuggestionByType($mid,$ty);
		 }
	//echo '<pre>'; var_dump($data); die();

	$data['looking_for'] = $searchT;
	$this->load->view('suggestion/makeSuggestion',$data);

	}

	function setTabValue(){
		$id = $_POST['id'];
		$this->session->set_userdata('tabval',$id);
		exit;
	}
	function configRelevant(){
		$data['suggested_id'] = $this->input->post('suggested_id');
		$data['rel_type'] = $this->input->post('rel_type');
		$data['mid'] = $this->session->userdata['logged_in']['id'];
		$result = $this->model_suggestion->configRelevant($data);
		echo $result;
		exit;
	}
	function advanceSearch(){
		$data['mid'] = $this->session->userdata['logged_in']['id'];
		$this->load->view('search/advance_search',$data);
	}
	function ajaxAdvanceSearch(){
		$data['selected_industries'] = $this->input->post('selected_industries');
		$data['selected_sector'] = $this->input->post('selected_sector');
		$data['selected_sub_sector'] = $this->input->post('selected_sub_sector');
		$data['selected_country'] = $this->input->post('selected_country');
		$result_sug=array();
		$result_com = $this->model_search->ajaxAdvanceSearch($data);
		if($this->input->post('selected_suggestion')!=''){
		$result_sug=$this->makeSuggestedListForloginuser($this->input->post('selected_suggestion'),$result_com);
		}
		if(count($result_sug)>0){
			$result = $result_sug;
			
		}else{
			$result = $result_com;
		}
		$result = array_unique($result);
		$new_member_ids = array();
		$mid = $this->session->userdata['logged_in']['id'];		
		foreach($result as $suggested_id){			
			$data = [];
			$data['curr_id'] = $suggested_id; 
			$data['user'] = getParticularUserDetails($data['curr_id']);
			//$data['is_connected'] = is_connected($mid,$curr_id);
			$data['is_connected'] = $this->model_suggestion->get_adsearch_connection($mid,$data['curr_id']);
			$data['suggested_user_type'] = userProfType($data['curr_id']);
			$new_member_ids[] = $data;
		}
		echo json_encode($new_member_ids);
        exit;

	}
		function makeSuggestedListForloginuser($looking_for,$searchIds){
			$midLookingfor=array();
			$results=$this->model_search->getTargetLookingForType($looking_for);
				foreach ($searchIds as $mid){
					$user = getParticularUserDetails($mid);
					foreach ($user['questionone'] as $looking_for){
						foreach ($results as $result){
						if($result == $looking_for){
							$midLookingfor[]=(int)$mid;
						}
						
					}	
				}
			}
			return $midLookingfor ;
		
	
		}
}//end 
