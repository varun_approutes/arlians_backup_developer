<?php 
class Suggestion extends CI_Controller
{
    function __construct(){

		parent::__construct();
		$this->load->model('model_common');
		$this->load->model('model_member');

		$this->load->model('model_message');
		$this->load->library('User_Manager');
		$this->load->model('model_suggestion');
		$this->load->helper('common_helper');
		$this->load->helper('text');
		$this->model_common->SiteSettingsDetails(); // This is mandatory
		$this->load->model('model_othermembersprofile');
		$this->user_manager = User_Manager::get_instance();
	}

	function acceptRequestForConnection(){
		$mid = (int)$_POST['mid'];
		$connected_id = (int)$_POST['connected_id'];

		echo $return = $this->model_suggestion->acceptRequestForConnection($mid,$connected_id);
		exit;
	}
	function acceptRequestForConnectionNew(){
		$mid = (int)$_POST['mid'];
		$connected_id = (int)$_POST['connected_id'];

		echo $return = $this->model_suggestion->acceptRequestForConnectionNew($mid,$connected_id);
		exit;
	}

	function deleteRequestForConnection(){
		$mid = (int)$_POST['mid'];
		$connected_id = (int)$_POST['connected_id'];

		echo $return = $this->model_suggestion->deleteRequestForConnection($mid,$connected_id);
		exit;
	}

	function makeSuggestedListForloginuser(){
		$sids = $_POST['looking_for'];
		
		$searchT = array();

		foreach($sids as $sid){
			$s = explode('-', $sid);
			$searchT[] = $s[0];
		}
		//echo '<pre>'; var_dump($searchT); die();
		$mid = $this->session->userdata['logged_in']['id'];
		$result = $this->model_suggestion->deleteMidFromSuggestion($mid);
		
		foreach($sids as $sid){
			
		$sid=explode('-',$sid);

		$mainMemberId = array();

		$id = array();
		$code;
		$uniqueMemberids = array();
		$uniqueMembers = array();
		$uniqueIds2 = array();
		$mainMid;
		$remove_arr = array();
		//foreach ($mid_arr as $value) {

			$mid = $this->session->userdata['logged_in']['id'];
			$rowSType = $sid[1];
			$mainMid = $mid;
			//=== check whether there is a search type for this member id or not ===
					$ch = $rowSType;



			if($ch != ''){

					switch($ch){
						case "1":
                        $user_type = $this->model_suggestion->userProfType($mid);
                        
						$members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);

						//echo '<pre>'; var_dump($members_ids_to_filter); die();
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);


						
						if(!empty($twenty_digit_code)){
							$i = 0;
							foreach($twenty_digit_code as $tw_code){
								$i_code = $tw_code['industry_code'];
								$s_code = $tw_code['target_sector_code'];

								$codeArr = $this->model_suggestion->suggestionPossibleTwentyDigitCode($i_code,$s_code); 

								foreach($members_ids_to_filter as $suggested_id){
									$flag = 0;
									$flag2= 0;
 
									foreach ($codeArr as $code) {
										$codeTypeOne = substr($code,0,10);
									
										$match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id,$code,$codeTypeOne);
											$uniqueMemberids[$suggested_id][] = $match_type;
												
									}
								}

							}

							//----------------------------------

							$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}



							} 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;
							//echo '<pre>'; var_dump($uniqueMemberids); die();
							//----------------------------------

							$ids = array_keys($uniqueMemberids);

							$sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 

							foreach($uniqueMemberids as $key=>$id){

								$this->model_suggestion->match1UserScore($sug_id,$mainMid,$key,$id);
							}  
						}
                        }else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }			
						break;
//=====================For 2================================
						case "2":
                        $user_type = $this->model_suggestion->userProfType($mid);
                        
						$members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);

						//echo '<pre>'; var_dump($members_ids_to_filter); die();
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid);
						//echo '<pre>'; var_dump($twenty_digit_code); die(); 
						if(!empty($twenty_digit_code)){
							$i = 0;
							foreach($twenty_digit_code as $tw_code){
								$tw_digit_code = $tw_code['mem_code'];

								$related_ten_digit_code = $this->model_suggestion->suggestionPossibleTenDigitCode($tw_digit_code);
								//echo '<pre>'; var_dump($related_ten_digit_code); die(); 
								foreach($members_ids_to_filter as $suggested_id){

									foreach($related_ten_digit_code as $tn_code){
										
										$target_industry_code = substr($tn_code,0,5);
										$target_sector_code = substr($tn_code,5,10);

										$match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id,$target_industry_code,$target_sector_code);
										$uniqueMemberids[$suggested_id][] = $match_type;
									}
								}
							}

							

							$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}



							}
							//echo '<pre>'; var_dump($uniqueMemberids); die(); 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;


							//----------------------------------

							$ids = array_keys($uniqueMemberids);

							$sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 
							
							foreach($uniqueMemberids as $key=>$id){

								$this->model_suggestion->match1UserScore($sug_id,$mainMid,$key,$id);
							}  
						}
                        }else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }			
						break;

//==============================================================================================================================
						//=====================For 3a================================
						case "3a":
						$uniqueIdsForTenDigitMatch = array();
                        
                        $user_type = $this->model_suggestion->userProfType($mid);
                        
                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);
                        
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid); 
						
						// For matching 20 digit code.
						$match_type = array();
						foreach($members_ids_to_filter as $suggested_id){
							foreach($twenty_digit_code as $tw_code){
								$flag = 0;
								$flag2= 0;

								$code = $tw_code['mem_code'];
								$codeTypeOne = substr($code,0,10);
								$match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id,$code,$codeTypeOne);

								/*
								if($match_type == "M" && $flag == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
									$flag =1;	
								} elseif($match_type == "PM" && $flag == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
									$flag2 =1;	
								} 
								elseif($match_type == "X" && $flag == 0 && $flag2 == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
									//$flag =1;	
								} */
								$uniqueMemberids[$suggested_id][] = $match_type; 
							}
						} 


							$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}
							} 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;

						//For matching 10 digit code.
						$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
							foreach($members_ids_to_filter as $suggested_id){
								foreach($ten_digit_code as $tn_code){
									$flag = 0;
									$flag2= 0;
									$match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id,$tn_code['industry_code'],$tn_code['target_sector_code']);

									/*if($match_type == "M" && $flag == 0){
										$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
										$flag =1;	
									} elseif($match_type == "PM" && $flag == 0){
										$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
										$flag2 =1;	
									} 
									elseif($match_type == "X" && $flag == 0 && $flag2 == 0){
										$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
										//$flag =1;	
									} */
									$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
								}
							}

						$newArr = $uniqueIdsForTenDigitMatch;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}
							} 
							unset($uniqueIdsForTenDigitMatch);
							$uniqueIdsForTenDigitMatch = $finalArr;

						$merged_arr = $this->array_merge_new($uniqueMemberids,$uniqueIdsForTenDigitMatch);
						//echo '<pre>'; var_dump($merged_arr); die();

							$ids = array_keys($merged_arr);

							$sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 

							foreach($merged_arr as $key=>$value){

								$str = $value['own_industry']."-".$value['location']."-".$value['target_industry'];
								$this->model_suggestion->match3UserScore($sug_id,$mainMid,$key,$str);
							}
                        }else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }	
							 
						break;
	//=================================================
						//====================== 3b=========================

					case "3b":
												$uniqueIdsForTenDigitMatch = array();
                        
                        $user_type = $this->model_suggestion->userProfType($mid);
                        
                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);
                        
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid); 
						
						// For matching 20 digit code.
						$match_type = array();
						foreach($members_ids_to_filter as $suggested_id){
							foreach($twenty_digit_code as $tw_code){
								$flag = 0;
								$flag2= 0;

								$code = $tw_code['mem_code'];
								$codeTypeOne = substr($code,0,10);
								$match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id,$code,$codeTypeOne);

								/*
								if($match_type == "M" && $flag == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
									$flag =1;	
								} elseif($match_type == "PM" && $flag == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
									$flag2 =1;	
								} 
								elseif($match_type == "X" && $flag == 0 && $flag2 == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
									//$flag =1;	
								} */
								$uniqueMemberids[$suggested_id][] = $match_type; 
							}
						} 


							$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}
							} 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;

						//For matching 10 digit code.
						$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
							foreach($members_ids_to_filter as $suggested_id){
								foreach($ten_digit_code as $tn_code){
									$flag = 0;
									$flag2= 0;
									$match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id,$tn_code['industry_code'],$tn_code['target_sector_code']);

									/*if($match_type == "M" && $flag == 0){
										$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
										$flag =1;	
									} elseif($match_type == "PM" && $flag == 0){
										$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
										$flag2 =1;	
									} 
									elseif($match_type == "X" && $flag == 0 && $flag2 == 0){
										$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
										//$flag =1;	
									} */
									$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
								}
							}

						$newArr = $uniqueIdsForTenDigitMatch;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}
							} 
							unset($uniqueIdsForTenDigitMatch);
							$uniqueIdsForTenDigitMatch = $finalArr;

						$merged_arr = $this->array_merge_new($uniqueMemberids,$uniqueIdsForTenDigitMatch);
						//echo '<pre>'; var_dump($merged_arr); die();

							$ids = array_keys($merged_arr);

							$sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 

							foreach($merged_arr as $key=>$value){

								$str = $value['own_industry']."-".$value['location']."-".$value['target_industry'];
								$this->model_suggestion->match3UserScore($sug_id,$mainMid,$key,$str);
							}
                        }else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }	
							 
						break;
	//=================================================
			

											//====================== 3c=========================

					case "3c":
						$uniqueIdsForTenDigitMatch = array();
                        
                        $user_type = $this->model_suggestion->userProfType($mid);
                        
                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);
                        
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid); 
						
						// For matching 20 digit code.
						$match_type = array();
						foreach($members_ids_to_filter as $suggested_id){
							foreach($twenty_digit_code as $tw_code){
								$flag = 0;
								$flag2= 0;

								$code = $tw_code['mem_code'];
								$codeTypeOne = substr($code,0,10);
								$match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id,$code,$codeTypeOne);


								$uniqueMemberids[$suggested_id][] = $match_type;
							}
						}

							$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}
							} 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;

						//For matching 10 digit code.
						$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
							foreach($members_ids_to_filter as $suggested_id){
								foreach($ten_digit_code as $tn_code){
									$flag = 0;
									$flag2= 0;
									$match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id,$tn_code['industry_code'],$tn_code['target_sector_code']);
									$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
								}
							}

							$newArr = $uniqueIdsForTenDigitMatch;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}
							} 
							unset($uniqueIdsForTenDigitMatch);
							$uniqueIdsForTenDigitMatch = $finalArr;

						$merged_arr = $this->array_merge_new($uniqueMemberids,$uniqueIdsForTenDigitMatch);
						//echo '<pre>'; var_dump($merged_arr); die();

							$ids = array_keys($merged_arr);

							$sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 

							foreach($merged_arr as $key=>$value){

								$str = $value['own_industry']."-".$value['location']."-".$value['target_industry'];
								$this->model_suggestion->match3UserScore($sug_id,$mainMid,$key,$str);
							}
                        }else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }	
							 
						break;

	//=====================================

						//=====================For 4a================================
						case "4a":

                        $user_type = $this->model_suggestion->userProfType($mid);
                        
						$members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);

						//echo '<pre>'; var_dump($members_ids_to_filter); die();
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
						//print_r($twenty_digit_code);
						
						if(!empty($twenty_digit_code)){
							$i = 0;
							foreach($twenty_digit_code as $tw_code){
								$i_code = $tw_code['industry_code'];
								$s_code = $tw_code['target_sector_code'];

								$codeArr = $this->model_suggestion->suggestionPossibleTwentyDigitCode($i_code,$s_code); 

								foreach($members_ids_to_filter as $suggested_id){
									$flag = 0;
									$flag2= 0;
 
									foreach ($codeArr as $code) {
										$codeTypeOne = substr($code,0,10);
									
										$match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id,$code,$codeTypeOne);
											$uniqueMemberids[$suggested_id][] = $match_type;
												
									}
								}

							}

							//----------------------------------

							$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}



							} 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;
							//----------------------------------

							$ids = array_keys($uniqueMemberids);

							$sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 

							foreach($uniqueMemberids as $key=>$id){

								$this->model_suggestion->match1UserScore($sug_id,$mainMid,$key,$id);
							}  
						}
                        }else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }	
						break;

//=====================
						//=====================================4B
						case "4b":

                        $user_type = $this->model_suggestion->userProfType($mid);
                        
						$members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);

						//echo '<pre>'; var_dump($members_ids_to_filter); die();
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
						//print_r($twenty_digit_code);
						
						if(!empty($twenty_digit_code)){
							$i = 0;
							foreach($twenty_digit_code as $tw_code){
								$i_code = $tw_code['industry_code'];
								$s_code = $tw_code['target_sector_code'];

								$codeArr = $this->model_suggestion->suggestionPossibleTwentyDigitCode($i_code,$s_code); 

								foreach($members_ids_to_filter as $suggested_id){
									$flag = 0;
									$flag2= 0;
 
									foreach ($codeArr as $code) {
										$codeTypeOne = substr($code,0,10);
									
										$match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id,$code,$codeTypeOne);
											$uniqueMemberids[$suggested_id][] = $match_type;
												
									}
								}

							}

							//----------------------------------

							$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}



							} 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;
							//----------------------------------

							$ids = array_keys($uniqueMemberids);

							$sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 

							foreach($uniqueMemberids as $key=>$id){

								$this->model_suggestion->match1UserScore($sug_id,$mainMid,$key,$id);
							}  
						}
                        }else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }	
						break;
//==================================

//================================ 5a=======================
						case "5a":

						$uniqueIdsForTenDigitMatch = array();
                        
                        $user_type = $this->model_suggestion->userProfType($mid);
                       
                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);
                       // var_dump( $members_ids_to_filter);
                      //  var_dump($searchT);
						//die();
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid); 
						//var_dump($twenty_digit_code);die();
						           //echo '<pre>'; var_dump($members_ids_to_filter); die();
						// For matching 20 digit code.
						$match_type = array();
						foreach($members_ids_to_filter as $suggested_id){
							foreach($twenty_digit_code as $tw_code){
								$flag = 0;
								$flag2= 0;

								$code = $tw_code['mem_code'];
								$codeTypeOne = substr($code,0,10);
								$match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id,$code,$codeTypeOne);

								/*if($match_type == "M" && $flag == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
									$flag =1;	
								} elseif($match_type == "PM" && $flag == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
									$flag2 =1;	
								} 
								elseif($match_type == "X" && $flag == 0 && $flag2 == 0){
									$uniqueMemberids[$suggested_id][] = $match_type;
								} */

								$uniqueMemberids[$suggested_id][] = $match_type;
							}
						}

						$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}



							} 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;
                         
						//For matching 10 digit code.
						$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
                        //echo '<pre>'; var_dump($ten_digit_code); die();
							foreach($members_ids_to_filter as $suggested_id){
								foreach($ten_digit_code as $tn_code){
									$flag = 0;
									$flag2= 0;
									$match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id,$tn_code['industry_code'],$tn_code['target_sector_code']);

									$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
								}
							}

							$newArr = $uniqueIdsForTenDigitMatch;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}

							} 
							unset($uniqueIdsForTenDigitMatch);
							$uniqueIdsForTenDigitMatch = $finalArr;
							 
						$merged_arr = $this->array_merge_new($uniqueMemberids,$uniqueIdsForTenDigitMatch);

						//Looking Service 
						$lookingService = $this->model_suggestion->getlookingservice($mainMid);
							
						$lookingService = trim($lookingService,',');
						$lookingServiceArray = explode(',',$lookingService);

						$last_suggested_ids = array_keys($merged_arr);

						foreach($last_suggested_ids as $id){
							$merged_arr[$id]['looking_service'] = $this->model_suggestion->getlookingservicemid($lookingServiceArray,$id);
						}
						
						$ids = array_keys($merged_arr);

						 $sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 
                           // echo '<pre>'; var_dump($merged_arr);
							
						foreach($merged_arr as $key=>$value){

							$str = $value['own_industry']."-".$value['location']."-".$value['target_industry']."-".$value['looking_service'];
							$this->model_suggestion->match5UserScore($sug_id,$mainMid,$key,$str);
						}
						//echo '<pre>'; var_dump($merged_arr); die();
                        } else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }	
						break;
	//=======================================

						//================================ 5b=======================
						case "5b":

						$uniqueIdsForTenDigitMatch = array();
                        
                        $user_type = $this->model_suggestion->userProfType($mid);
                       
                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor($searchT,$user_type);
                       // var_dump( $members_ids_to_filter);
                      //  var_dump($searchT);
						//die();
                        if(!empty($members_ids_to_filter)){
						//Getting 20 digit code of the particuler mid.
						$twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid); 
						//var_dump($twenty_digit_code);die();
						           //echo '<pre>'; var_dump($members_ids_to_filter); die();
						// For matching 20 digit code.
						$match_type = array();
						foreach($members_ids_to_filter as $suggested_id){
							foreach($twenty_digit_code as $tw_code){
								$flag = 0;
								$flag2= 0;

								$code = $tw_code['mem_code'];
								$codeTypeOne = substr($code,0,10);
								$match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id,$code,$codeTypeOne);

								$uniqueMemberids[$suggested_id][] = $match_type;
							}
						}

							$newArr = $uniqueMemberids;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}

							} 
							unset($uniqueMemberids);
							$uniqueMemberids = $finalArr;

                         //echo '<pre>'; var_dump($uniqueMemberids); die();
						//For matching 10 digit code.
						$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
                        //echo '<pre>'; var_dump($ten_digit_code); die();
							foreach($members_ids_to_filter as $suggested_id){
								//echo $suggested_id;
								foreach($ten_digit_code as $tn_code){

									//echo '<pre>'; var_dump($tn_code); 
									$match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id,$tn_code['industry_code'],$tn_code['target_sector_code']);

									$uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
								}
							} 

							//echo '<pre>'; var_dump($uniqueIdsForTenDigitMatch); die();

							$newArr = $uniqueIdsForTenDigitMatch;
							$finalArr = array();

							foreach($newArr as $key=>$value){

								$k = array_search('M',$value);

								if(isset($k)){

									$finalArr[$key] = $value[$k];
								} else{

									$ke = array_search('PM',$value);

									if(isset($ke)){
										$finalArr[$key] = $value[$ke];
									} else{
										$finalArr[$key] = 'X';
									}
								}

							} 
							unset($uniqueIdsForTenDigitMatch);
							$uniqueIdsForTenDigitMatch = $finalArr;

							//echo '<pre>'; var_dump($uniqueIdsForTenDigitMatch); die();
							 
						$merged_arr = $this->array_merge_new($uniqueMemberids,$uniqueIdsForTenDigitMatch);
						//echo '<pre>'; var_dump($merged_arr); die();
						//Looking Service 
						$lookingService = $this->model_suggestion->getlookingservice($mainMid);
							
						$lookingService = trim($lookingService,',');
						$lookingServiceArray = explode(',',$lookingService);

						$last_suggested_ids = array_keys($merged_arr);

						foreach($last_suggested_ids as $id){
							$merged_arr[$id]['looking_service'] = $this->model_suggestion->getlookingservicemid($lookingServiceArray,$id);
						}
						
						$ids = array_keys($merged_arr);

						 $sug_id = $this->model_suggestion->insertTypeTwo($mainMid,$ids,$ch); 
                           // echo '<pre>'; var_dump($merged_arr); die();
							
						foreach($merged_arr as $key=>$value){

							$str = $value['own_industry']."-".$value['location']."-".$value['target_industry']."-".$value['looking_service'];
							$this->model_suggestion->match5UserScore($sug_id,$mainMid,$key,$str);
						}
						//echo '<pre>'; var_dump($merged_arr); die();
                        }else{
                        	$data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        	//var_dump($data); die();
                        }	
						break;
	//=======================================
					}
			}
	}

	//=============== Deleting unwanted ids from ar_score table================

	$this->model_suggestion->deletedUnwantedIds();

	//=========================================================================
	if($sug_id!=''){
	$data['last_suggested_id'] = $this->model_suggestion->showSearchResult($sug_id);
	}else{
		$data['last_suggested_id'] ='';
	}
	$data['looking_for'] = $searchT;
	//var_dump($data); die();
	$this->load->view('suggestion/makeSuggestedListForloginuser',$data);

	}

	function arrayFormat($arr){
		$main = array();

		foreach($arr as $key=>$value){
			$main[$key] = $value[0];

		}

		return $main;
	}

	function array_merge_new($arr1,$arr2){
		$arr3 = array();

		foreach($arr1 as $key=>$value){

			$arr3[$key]['own_industry'] = $value;
			$arr3[$key]['location'] = "M";
			$arr3[$key]['target_industry'] = $arr2[$key];
		}
		//echo '<pre>'; var_dump($arr3); die();
		return $arr3;
	}

	function makeSuggestedListForloginuserMod(){
		//$sids = $_POST['looking_for'];
		//---------------------

		$arr['email'] = $this->input->post('user_email');
		
		$user_id = getIdByUserEmail($arr['email']);

		$mid = $user_id->mid;
		//var_dump($mid);
		//$mid = 163;

		//--------------------
		//$mid = $this->session->userdata['logged_in']['id'];
		$sids = $this->model_suggestion->suggestionSearchTypes($mid);
	 	$data = array();
		$searchT = array();
		$type = array();

		foreach($sids as $sid){
			$s = explode('-', $sid);
			$searchT[] = $s[0];
			$type[] = $s[1];
		}	
		
						//echo $ch; die();

		$user = getParticularUserDetails($mid);

		$user_own_type = $user['bussinesstype1']; 
		$pos;
		$pos1;
			if($user_own_type == 'Buisness'){
				if (in_array('3c', $type)) {
					$pos = array_search('3c', $type);

					if($pos)
					{
						unset($type[$pos]);
					}
				}
				if (in_array('4b', $type)) {
					$pos1 = array_search('4b', $type);
					if($pos1)
					{
						unset($type[$pos1]);
					}
				}
			}
		if(!empty($type))
		{
			foreach ($type as $ty) 
			 {

			 	$data['last_suggested_id'][$ty] = $this->model_suggestion->getSuggestionByType($mid,$ty);
			 }

		}

	//echo '<pre>'; var_dump($data); 

	$data['looking_for'] = $searchT;

	$last_suggested_id_new = array();

	$type_converter = array('5a'=>'1','5b'=>'2','2'=>'3','1'=>'4','4a'=>'5','4b'=>'6','3a'=>'7','3b'=>'8','3c'=>'9');
	//$type_converter = array('5a'=>'1','5b'=>'2','2'=>'3','1'=>'4','4a'=>'5','4b'=>'6','3a'=>'7','3b'=>'8','3c'=>'9');
	if(!empty($type))
	{
		foreach($data['last_suggested_id'] as $key=>$row)
		{
			$last_suggested_id_new[$type_converter[$key]] = $row;
		}
	}
  //echo '<pre>'; var_dump($last_suggested_id_new); die();
	$user = getParticularUserDetails($mid);
	//$blocked_ids_by_user = listOfSuggestionBlockIds($mid);

	$user_own_type = $user['bussinesstype1'];
	
	$new_member_ids = array();
		foreach($last_suggested_id_new as $key=>$val)
		{
			if($key==1){ $title= 'strategic alliances';}
			elseif($key==2){ $title= 'M&A opportunities';}
			elseif($key==3){ $title= 'customers';}
			elseif($key==4){ $title= 'suppliers';}
			elseif($key==5){ $title= 'Seek investor';}
			elseif($key==6){ $title= 'investors';}
			elseif($key==7){ $title= 'non-executive directors positions';}
			elseif($key==8){ $title= 'Find non-executive directors';}
			elseif($key==9){ $title= 'Connect with freelance professionals';}	

			$id_str = $val[0]['suggested_ids']; 
			$value = explode(',', $id_str);

			//$new_member_ids = array();
						
			foreach($value as $suggested_id){
				//if (in_array($suggested_id, $blocked_ids_by_user))
					//continue;  
				$curr_id = $suggested_id; 
				$suggested_user_type = userProfType($curr_id);
				$looking_for_id = $key;
				
				
				$result = checkToShowUserInSearchResult($looking_for_id,$user_own_type);
					if(strtolower($suggested_user_type) == strtolower($user_own_type) || $result == "BOTH"){
					
					  $is_score=isScoreNotZero($curr_id,$mid);

					   	  if( $is_score!=0){
							 if($mid!=$curr_id){
								$new_member_ids[$key][] = $curr_id;
							 }
					  }
					}
			}
		}
		//echo '==============';
		//echo '<pre>'; var_dump($new_member_ids); die();
		$result = array();

		foreach($new_member_ids as $key=>$value)
		{
			foreach($value as $k=>$rw)
			{
				$score=matchScore($rw,$mid,$key);
				if($score!='z'){
					$result[$key][$rw]['score'] = $score;
					$result[$key][$rw]['matched_text'] = matchScoretext($rw,$mid,$key);
					$details = getParticularUserDetails($rw);  
					//echo '<pre>'; var_dump($details);
					$result[$key][$rw]['logobussinessname'] = logobussinessname($rw); 
					$result[$key][$rw]['image'] = $details['profile_image'];
					$result[$key][$rw]['desc'] = $details['bussinessdesc'];
					$result[$key][$rw]['feedback'] = Feedbackdetails($mid,$rw);
					$result[$key][$rw]['bussinessname'] = getMemberbusinName($rw);
					$result[$key][$rw]['path'] = base_url();
					$result[$key][$rw]['mid'] = $rw;
				}
			}
		}

		$search_type = $this->input->post('type_key');
		//echo '<pre>'; var_dump($result); die();
		$ky = $type_converter[$search_type]; 
		if($result[$ky]==NULL){
			//$result=array('result'=>'fail');
			echo json_encode(array('result'=>'fail'));
			exit;		
		}else{

			echo json_encode($result[$ky]);
			exit;
		}
		
		
	}

	function setTabValue(){
		$id = $_POST['id'];
		$this->session->set_userdata('tabval',$id);
		exit;
	}
	function configRelevant(){
		$data['suggested_id'] = $this->input->post('suggested_id');
		$data['rel_type'] = $this->input->post('rel_type');
		$data['mid'] = $this->session->userdata['logged_in']['id'];
		$result = $this->model_suggestion->configRelevant($data);
		echo $result;
		exit;
	}
	
	
				
}//end 
