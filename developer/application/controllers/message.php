<?php

class Message extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('model_common');
        $this->load->model('model_member');
        $this->load->model('model_message');
        $this->load->model('model_memberprofile');
        $this->load->helper('common_helper');
        $this->load->helper('text');
        $this->model_common->SiteSettingsDetails(); // This is mandatory
    }

//    function index() {
//        if (count($_POST) > 0) {
//            $this->model_member->insertmemberdata($_POST['data']);
//        }
//    }
    function index(){
	   // $this->output->cache(5);
        $this->load->view('message/message');
    }

    function message_count($id = null, $type = null) {
        $id = $this->session->userdata['logged_in']['id'];
        $count = $this->model_message->message_count_q($id, $type);
        echo $count;
        exit;
    }

    function msglist() {
        $data_bag['mid'] = $this->session->userdata['logged_in']['id'];
        //$data = $this->model_memberprofile->getUserDetails();
        if (count($_POST) > 0) {
            //print_r($_POST);	 die();
            $sender_id = $this->session->userdata['logged_in']['id'];
            $data['msg_body'] = $_POST['data']['message-body'];
            $data['msg_sub'] = $_POST['data']['message-sub'];
            //$data['hide_biz_name'] = $_POST['biz_not_visible'];
            if (!empty($_POST['biz_not_visible'])) {
                $data['hide_biz_name'] = 1;
            }
            $msgid = $this->model_message->insertMessage($data);

            //------------------------------------Memeber Id-----------------------------------------

            $mem = $_POST['data']['email'];

            foreach ($mem as $m) {

                $mem_id = $this->getMidFromBizname($m);
                $d['sender_id'] = $sender_id;
                $d['receiver_id'] = $mem_id[0]['mid'];
                $d['msg_id'] = $msgid;

                $this->model_member->insertMessageTrans($d);
            }
            $redirect_url = base_url() . "message/msglist/cm";

            header("Refresh:0; url=" . $redirect_url);
        }

        $this->load->view('message/message', $data_bag);
    }

    function msglist_api() {
        //=======================================================
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
        //======================================================

        $data_bag['mid'] = $user_id;
        if (count($_POST) > 0) {
            $sender_id = $user_id;
            $data['msg_body'] = $this->input->post('message_content');
            $data['msg_sub'] = $this->input->post('message_sub');
			$data['msg_type'] = $this->input->post('flag');
            if (!empty($this->input->post('biz_not_visible'))) {
                $data['hide_biz_name'] = 1;
            }
            $msgid = $this->model_message->insertMessage($data);

            //------------------------------------Memeber Id-----------------------------------------

            $mem = $this->input->post('message_send_to');
            $res = array();
            foreach ($mem as $m) {
                $mem_id = $this->getMidFromBizname($m);
                $d['sender_id'] = $sender_id;
                $d['receiver_id'] = $mem_id[0]['mid'];
                $d['msg_id'] = $msgid;

                $res[] = $this->model_member->insertMessageTrans($d);
				
				// send mail to users
				
				
				// send mail to users
			}
        }
        $r = array();
        if (in_array(0, $res)) {
            $r['response'] = 'fail';
        } else {
            $r['response'] = 'success';
        }

        echo json_encode($r);
        //$this->load->view('message/message',$data_bag);
        exit;
    }

    function replyMessageApi() {
        //=======================================================
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
        //======================================================
        $sender_id = $user_id;
        $data['msg_body'] = $this->input->post('msg_body');
        $msgid = $this->model_message->insertMessage($data);
        $d['sender_id'] = $this->input->post('sender_id');
        $d['receiver_id'] = $this->input->post('receiver_id');
        $d['msg_id'] = $msgid;
        $res = $this->model_member->insertMessageTrans($d);

        $r = array();
        if ($res == 1) {
            $r['response'] = 'success';
        } else {
            $r['response'] = 'fail';
        }

        echo json_encode($r);
        exit;
    }

    function replyMessage() {
        //=======================================================
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
        //======================================================
        $sender_id = $user_id;
        $data['msg_body'] = $this->input->post('msg_body');
        $msgid = $this->model_message->insertMessage($data);
        $d['sender_id'] = $this->input->post('sender_id');
        $d['receiver_id'] = $this->input->post('receiver_id');
        $d['msg_id'] = $msgid;
        $this->model_member->insertMessageTrans($d);

        $r = array();
        if ($res == 1) {
            $r['response'] = 'success';
        } else {
            $r['response'] = 'fail';
        }

        echo json_encode($r);
        exit;
    }

    function getInbox() {
        //=======================================================
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
        //======================================================
        $data['mid'] = $user_id;
        $inbox = $this->model_message->inbox($data['mid']);
        echo json_encode($inbox);
        exit;
    }

    function getInboxApi() {
        //=======================================================
        $data1['user_email'] = $this->input->post('user_email');
		
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
		//======================================================
        $data['mid'] = $user_id;
        //$inbox = $this->model_message->inbox($data['mid']);
		$inbox = $this->model_message->Fninbox($data['mid']);
	
		echo json_encode($inbox);
        exit;
    }

    function getSentApi() {
        //=======================================================
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
        //======================================================
        $data['mid'] = $user_id;
        $inbox = $this->model_message->sent($data['mid']);
        echo json_encode($inbox);
        exit;
    }

    function getSent() {
        //=======================================================
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
        //======================================================
        $data['mid'] = $user_id;
        $inbox = $this->model_message->sent($data['mid']);
        echo json_encode($inbox);
        exit;
    }

    function getBussiness() {

        $this->load->model('model_category');
        $bussinessType = $this->model_category->getBussinessCode();

        $result = $bussinessType->result_array;

        $html = "<option value='ALL'>Select your sender type: supplier, customer, investor</option>";

        foreach ($result as $row) {
            $bc = $row['bussiness_code'];
            $bn = $row['bussiness_name'];


            $html .= "<option value='" . $bc . "'>" . $bn . "</option>";
        }
        echo $html;

        exit;
    }

    function saveMessage() {
        //=======================================================
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
        //======================================================

        $count['msg_count'] = $this->message_count($user_id);
        $count['msg_count_sent'] = $this->message_count($user_id, 'sent');
        if (count($_POST) > 0) {

            $sender_id = $user_id;

            $data['msg_body'] = $_POST['data']['message-body'];
            $msgid = $this->model_message->insertMessage($data);

            //------------------------------------Memeber Id-----------------------------------------			
            $mem = $_POST['data']['email'];
            foreach ($mem as $m) {
                $mem_id = $this->getMidFromBizname($m);
                $d['sender_id'] = $sender_id;
                $d['receiver_id'] = $mem_id[0]['mid'];
                $d['msg_id'] = $msgid;
                $this->model_member->insertMessageTrans($d);
            }
        }
        $this->load->view('message/message', $count);
    }

    function saveMessageApi() {

        //===========================================================
        $data['user_email'] = $this->input->post('user_emai');
        //$data['user_email'] = 'sims12781+41@gmail.com';
        $result = $this->model_member->getIdByUserEmail($data);

        $user_id = $result->mid;
        //============================================================= 

        $count['msg_count'] = $user_id;
        $count['msg_count_sent'] = $this->message_count($user_id, 'sent');
        if (count($_POST) > 0) {

            $sender_id = $user_id;

            $data['msg_body'] = $_POST['data']['message-body'];
            $msgid = $this->model_message->insertMessage($data);

            //------------------------------------Memeber Id-----------------------------------------			
            $mem = $_POST['data']['email'];
            foreach ($mem as $m) {
                $mem_id = $this->getMidFromBizname($m);
                $d['sender_id'] = $sender_id;
                $d['receiver_id'] = $mem_id[0]['mid'];
                $d['msg_id'] = $msgid;
                $this->model_member->insertMessageTrans($d);
            }
        }
        $this->load->view('message/message', $count);
    }

    function getMemberIdsFromBussinessCode($code) {
        $mids = $this->model_member->fetchMemberId($code);
        return $mids;
    }

    function getMidFromBizname($bizname) {
        return $this->model_message->getId($bizname);
    }

    function deleteMessage() {
        $id = $this->input->post("msg_id");
        $result = $this->model_message->deleteMessage($id);
        echo $result;
        exit;
    }

	function deleteSelectMessage() {
        $all_message = $this->input->post("delete_msg_obj");
		 $data_info['user_email'] = $this->input->post('user_email');
         $result = $this->model_member->getIdByUserEmail($data_info);
		$user_id = $result->mid;
		//print_r($all_message);die();
		$returnarr=array();
		for($i=0;$i<=count($all_message)-1;$i++)
		{
			 $message_id=$all_message[$i];
			 $message_status=$message->status;
			$result = $this->model_message->deleteMessage($message_id,$user_id,$message_status);
			$returnarr[$i]['message_id']=$message_id;		
			
		}	
		
		
		/* foreach($all_message as $message){
			
echo $message_id=$message->msg_id; die();
			$message_status=$message->status;
			$result = $this->model_message->deleteMessage($message_id,$user_id,$message_status);
						
		} */
		$res_api['response'] = $all_message;
        echo json_encode($returnarr);
		exit;
    }


    function updateMessageStatus() {
        $mt_id = $this->input->post('mt_id');
        $result_request = $this->model_message->requestConnectionSeen($mt_id);
       if($result_request==true){
		$res_api['response'] = "success";
	   }else{
		 $res_api['response'] = "false";  
	   }
        echo json_encode($res_api);
        exit;
    }

    function sendMessage() {
        //===========================================================
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $user_id = $result->mid;
        //============================================================= 
        $data['msg_body'] = $this->input->post('message');
        $data['msg_sub'] = $this->input->post('subject');
        $reciever_id = $this->input->post('reciever_id');
        $msgid = $this->model_message->insertMessage($data);
        //$user=$this->user_manager->get_looged_in_user();
        //print_r($user);
        //exit;
        $d['sender_id'] = $user_id;
        $d['receiver_id'] = $reciever_id;
        $d['msg_id'] = $msgid;
        $this->model_member->insertMessageTrans($d);
        $res_api['response'] = "success";
        echo json_encode($res_api); 
        exit;
    }
	
	function FnGetMessagebyMsgId()
	{
		 $data1['user_email'] = $this->input->post('user_email');
         $result = $this->model_member->getIdByUserEmail($data1);
         $user_id = $result->mid;
		 $message_id = $this->input->post('message_id');
		 $inboxid = $this->input->post('inboxid');
		 $message_subject = $this->input->post('msg_sub');
		 $session_user_id=$user_id;
		 $res_api=$this->model_message->FnGetMessageDetails($message_id,$user_id,$message_subject,$inboxid,$session_user_id);
		 echo json_encode($res_api);
         exit;
	}
    
}
