<?php

class Explore extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('model_common');
        $this->load->model('model_member');
        $this->load->model('model_message');
        $this->load->library('User_Manager');
        $this->load->model('model_suggestion');
        $this->load->helper('common_helper');
        $this->load->helper('text');
        $this->model_common->SiteSettingsDetails(); // This is mandatory
        $this->load->model('model_othermembersprofile');
        $this->user_manager = User_Manager::get_instance();
    }

    function index() {
        $user = $this->user_manager->get_looged_in_user();
        if ($user != null) {
            $this->afterLogin();
        }
        $this->load->view('landing/landing');
    }

    function signUpMember() {
        $data['hemail'] = $this->input->post('user_email');
        $data['pass'] = $this->input->post('password');
        $data['referal'] = $this->input->post('referal');
        //$data['hemail'] = 'indrajit.auddy27@gmail.com';
        //$data['pass'] = md5(AB123456);
        $id = $this->model_member->insertmemberdata($data);

        $mid = $this->session->userdata['logged_in']['id'];
        $bussinessName = getBusinessNameById($mid);

        if ($id != 'FALSE') {
            //=========================Welcome Mail==========================
            $mail = $data['hemail'];
            if ($bussinessName[0]['bussinessname'] == '') {
                $bussinessName[0]['bussinessname'] = $mail;
            }

$mailBody = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Arlians</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="keywords"  content="" />
	<meta name="Resource-type" content="Document" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Oxygen:400,700" rel="stylesheet" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet" type="text/css">
	
 <style type="text/css">
  * { box-sizing: border-box;}
   body{margin: 0; font-family: Arial; font-size: 13px; line-height: 20px; color:#515151;}
   ::-moz-placeholder{ color: #357cb7; opacity: 1;}
    .main-body{ width: 352px;}

    @media screen and (max-width: 767px) {
    .main-body{ width: 90%;}   
   }

  </style>  
	

</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image: url('.base_url().'email-template/images/email-bg02.jpg); background-repeat: no-repeat; background-position: center top;  background-size: cover;  padding: 88px 0;">
    <tr>
      <td style="text-align:center;">
        <h1 style="font-family: \'source_sans_prolight\', Arial; font-size: 67px; color: #fff; margin: 0; line-height:67px;">Hello!</h1>
        <span style="display: block; margin: 30px 0; text-transform: uppercase; font-size: 22px; color: #fff; line-height:25px;">Welcome to your profile!</span>
      </td>
    </tr>
  <tr>
    <td>
        <table border="0" cellpadding="0" cellspacing="0" class="main-body" style="background: #357cb7; padding: 25px 40px; color: #fff; text-align: center; margin: 0 auto;">
          <tr>
            <td>
               <span style="background: #383838 ; border-radius: 50%;  height: 100px;  margin: 0 auto 20px; width: 100px; display:inline-block;"><img style="width:100%;" src="images/logo-circle.png" alt=""></span>
            </td>
          </tr>  
          <tr>
            <td>
               <h2 style="font-size: 34px; margin: 0 0 20px; line-height: 40px; font-family: \'Source Sans Pro\', sans-serif;font-weight:300;">'.$mail.'</h2>
               <p>Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you  every step of the way.</p>
               <a href="'.base_url().'" style="background-color: #fff ; text-decoration:none; border-radius: 4px; color: #383838; display: inline-block; font-weight: bold;  padding: 15px 40px; font-size: 16px;">Customize Profile</a>
               <!--<p>Refer Arlians <a href="javascript:void(0)" style=" color: #fff; text-decoration: underline;">here</a></p>-->
            </td>
          </tr>  
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="text-align:center;  font-size: 13px; margin-top: 30px; text-align: center; color:#fff;">
        <tr>
          <td>
            <img src="'.base_url().'email-template/images/noti-logo.png" alt="">
            <p style="margin-top:10px; margin-bottom:0;">Manage email preferences in your settings</p>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
 
</body>
</html>';
            $subject = "Welcome to Arlians!";
            $this->sentmail($mail, $mailBody, $subject);

            /* $sender_id = 0;
              $messagebody='Welcome to Arlians, '.$bussinessName[0]['bussinessname']."\r\n".'Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you every step of the way.'."\r\n".'
              Spread the word'."\r\n".'
              Remember, as the network grows, so do your opportunities. Make the most of Arlians by inviting your customers, suppliers and any other business you\'ve ever worked with.
              '."\r\n".'
              Become a Founding Member'."\r\n".'
              As a thank you for being an early supporter of Arlians, you will have the possibility to acquire the status of Founding Member for a small one-off fee of $99 and enjoy FREE Lifetime Premium Membership on Arlians. '."\r\n".'
              Premium Membership will give access to many exclusive features and functionalities that are currently being developed.
              This is a limited offer so don\'t miss out!
              '."\r\n";
              $data['msg_body'] = $messagebody;
              $msgid = $this->model_message->insertMessage($data);

              //------------------------------------Memeber Id-----------------------------------------


              $d['sender_id'] = 0;
              $d['receiver_id'] = $id;
              $d['msg_id'] = $msgid;
              $this->model_member->insertMessageTrans($d); */





            //=========================Activation Mail ========================
            //$activationCode = md5($_POST['data']['hemail']);				
            $sess_array = array(
                'id' => $id,
                'username' => $this->input->post('user_email'),
                'status' => '0'
            );
            $this->session->set_userdata('logged_in', $sess_array);
            $subject = "Activation Mail for Arlians";
            $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Arlians</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Activate your account</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;"><br /><br />
                                            <a href="' . base_url() . '">
                                                <img src="' . base_url() . '/assets/images/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Hi  ' . $mail . '<br />
                                            <br />
                                            <span style="font-size:14px;color:#4D575E;">Thank you for signing up to Arlians! <br>
                                                Before you get started, 
                                                please activate your account below:
                                            </span>
                                            <br /><br />                                            
                                        </td>
                                    </tr>       
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                    <td align="center" style="background-color:#01A69C;font-family:Calibri, Arial, sans-serif;font-size:16px;color:#fff;line-height:1.2;">
                                    <strong><a style="color:#fff;" href="' . base_url() . 'member/activateUser/' . $id . '/' . md5($mail) . '" target="_blank">Activate your account</a></strong>
                                    

                                </table>
                                
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright © 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                            Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';
           // $this->sentmail($mail, $mailBody, $subject);
            //referrel mail

            if ($data['referal'] != '0') {
                $refers_details = $this->model_member->getReferalData($data['referal']);

                $bussinessName = getBusinessNameById($refers_details->mid);
                $refers_deta = $this->model_member->getUserById($refers_details->mid);

                $mail = $refers_deta->email;
                //$mail ='rityan@digitalaptech.com';
                $refe_mail = $data['hemail'];
                $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Thank you for signing up to Arlians</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Thank you for signing up to Arlians.</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;"><br /><br />
                                             <a href="' . base_url() . '">
                                                <img title="Arlians logo" src="' . base_url() . '/assets/images/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Welcome to Arlians, ' . $bussinessName[0]['bussinessname'] . '<br />
                                            <br />
											<span style="font-size:14px;color:#4D575E;">' . $refe_mail . ' has used your unique code.</span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you every step of the way.</span><br /><br />
                                            <span style="color:#00AAA0;"><strong>Spread the word</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">Remember, as the network grows, so do your opportunities. Make the most of Arlians by inviting your customers, suppliers and any other business you\'ve ever worked with.</span>
                                            <br /><br />
                                            <span style="color:#00AAA0"><strong>Be rewarded</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">
                                                You\'ll get the opportunity for exclusive free access to premium features when you recommend someone and they sign up to Arlians. All you have to do to take advantage of this is share your unique code.</span>
                                            <br /><br /> 
                                        </td>
                                    </tr>       
                                </table>
                               
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright © 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                           Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
													

                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';
                $subject = "Congrats! " . $refe_mail . " use your code";

                $this->sentmail($mail, $mailBody, $subject);
            }

            echo true;
            //$this->load->view('member/setup_wiz');
        } else {
            echo false;
        }
        exit;
    }
    /**Phase 2 User Sign up**/
    function userSignUpMember() {
        $data['hemail'] = $this->input->post('user_email');
        $data['pass'] = $this->input->post('password');
        $data['referal'] = $this->input->post('referal');
        //$data['hemail'] = 'indrajit.auddy27@gmail.com';
        //$data['pass'] = md5(AB123456);
        $id = $this->model_member->insertmemberdata($data);

        $mid = $this->session->userdata['logged_in']['id'];
        $bussinessName = getBusinessNameById($mid);

        if ($id != 'FALSE') {
            //=========================Welcome Mail==========================
            $mail = $data['hemail'];
            if ($bussinessName[0]['bussinessname'] == '') {
                $bussinessName[0]['bussinessname'] = $mail;
            }

            $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Thank you for signing up to Arlians</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Thank you for signing up to Arlians.</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;"><br /><br />
                                             <a href="' . base_url() . '">
                                                <img title="Arlians logo" src="' . base_url() . '/assets/images/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Welcome to Arlians, ' . $bussinessName[0]['bussinessname'] . '<br />
                                            <br />
                                            <span style="font-size:14px;color:#4D575E;">Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you every step of the way.</span><br /><br />
                                            <span style="color:#00AAA0;"><strong>Spread the word</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">Remember, as the network grows, so do your opportunities. Make the most of Arlians by inviting your customers, suppliers and any other business you\'ve ever worked with.</span>
                                            <br /><br />
                                            <span style="color:#00AAA0"><strong>Become a Founding Member</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">
                                                As a thank you for being an early supporter of Arlians, you will have the possibility to acquire the status of Founding Member for a small one-off fee of $99 and enjoy FREE Lifetime Premium Membership on Arlians. <br />
											Premium Membership will give access to many exclusive features and functionalities that are currently being developed.
											This is a limited offer so don\'t miss out!</span>
                                            <br /><br /> 
                                        </td>
                                    </tr>       
                                </table>
                                                                
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright  &copy; 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                            Our mailing address is:<br />
                                            Arlians Limited<br />
                                            Level 3, 207 Regent Street<br/>
                                            London, W1B 3HH<br/>
                                            United Kingdom<br/>
                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';
            $subject = "Welcome to Arlians!";
            $this->sentmail($mail, $mailBody, $subject);

            /* $sender_id = 0;
              $messagebody='Welcome to Arlians, '.$bussinessName[0]['bussinessname']."\r\n".'Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you every step of the way.'."\r\n".'
              Spread the word'."\r\n".'
              Remember, as the network grows, so do your opportunities. Make the most of Arlians by inviting your customers, suppliers and any other business you\'ve ever worked with.
              '."\r\n".'
              Become a Founding Member'."\r\n".'
              As a thank you for being an early supporter of Arlians, you will have the possibility to acquire the status of Founding Member for a small one-off fee of $99 and enjoy FREE Lifetime Premium Membership on Arlians. '."\r\n".'
              Premium Membership will give access to many exclusive features and functionalities that are currently being developed.
              This is a limited offer so don\'t miss out!
              '."\r\n";
              $data['msg_body'] = $messagebody;
              $msgid = $this->model_message->insertMessage($data);

              //------------------------------------Memeber Id-----------------------------------------


              $d['sender_id'] = 0;
              $d['receiver_id'] = $id;
              $d['msg_id'] = $msgid;
              $this->model_member->insertMessageTrans($d); */





            //=========================Activation Mail ========================
            //$activationCode = md5($_POST['data']['hemail']);				
            $sess_array = array(
                'id' => $id,
                'username' => $this->input->post('user_email'),
                'status' => '0'
            );
            $this->session->set_userdata('logged_in', $sess_array);
            $subject = "Activation Mail for Arlians";
            $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Arlians</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Activate your account</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;"><br /><br />
                                            <a href="' . base_url() . '">
                                                <img src="' . base_url() . '/assets/images/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Hi  ' . $mail . '<br />
                                            <br />
                                            <span style="font-size:14px;color:#4D575E;">Thank you for signing up to Arlians! <br>
                                                Before you get started, 
                                                please activate your account below:
                                            </span>
                                            <br /><br />                                            
                                        </td>
                                    </tr>       
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                    <td align="center" style="background-color:#01A69C;font-family:Calibri, Arial, sans-serif;font-size:16px;color:#fff;line-height:1.2;">
                                    <strong><a style="color:#fff;" href="' . base_url() . 'member/activateUser/' . $id . '/' . md5($mail) . '" target="_blank">Activate your account</a></strong>
                                    

                                </table>
                                
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright © 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                            Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';
            //$this->sentmail($mail, $mailBody, $subject);
            //referrel mail

            if ($data['referal'] != '0') {
                $refers_details = $this->model_member->getReferalData($data['referal']);

                $bussinessName = getBusinessNameById($refers_details->mid);
                $refers_deta = $this->model_member->getUserById($refers_details->mid);

                $mail = $refers_deta->email;
                //$mail ='rityan@digitalaptech.com';
                $refe_mail = $data['hemail'];
                $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Thank you for signing up to Arlians</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Thank you for signing up to Arlians.</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;"><br /><br />
                                             <a href="' . base_url() . '">
                                                <img title="Arlians logo" src="' . base_url() . '/assets/images/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Welcome to Arlians, ' . $bussinessName[0]['bussinessname'] . '<br />
                                            <br />
											<span style="font-size:14px;color:#4D575E;">' . $refe_mail . ' has used your unique code.</span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you every step of the way.</span><br /><br />
                                            <span style="color:#00AAA0;"><strong>Spread the word</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">Remember, as the network grows, so do your opportunities. Make the most of Arlians by inviting your customers, suppliers and any other business you\'ve ever worked with.</span>
                                            <br /><br />
                                            <span style="color:#00AAA0"><strong>Be rewarded</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">
                                                You\'ll get the opportunity for exclusive free access to premium features when you recommend someone and they sign up to Arlians. All you have to do to take advantage of this is share your unique code.</span>
                                            <br /><br /> 
                                        </td>
                                    </tr>       
                                </table>
                               
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright © 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                           Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
													

                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';

                $subject = "Congrats! " . $refe_mail . " use your code";

                $this->sentmail($mail, $mailBody, $subject);
            }
            
            $result = array('success'=>true);       
            //$this->load->view('member/setup_wiz');
        } else {
           $result = array('success'=>false);
        }
        echo json_encode($result);
        exit;
    }

    function afterLogin() {
        $data = $this->user_manager->get_looged_in_user();
        $chk_wiz_one = $this->model_member->data_exists_wiz_one($data['mid']);
        if ($chk_wiz_one == "TRUE" && $data['status'] == 1) {
            redirect('member/dashboard');
        } elseif ($chk_wiz_one == "TRUE" && $data['status'] == 0) {
            $data['error_message'] = 'You are not activated. please active your account from mail link';
            $this->load->view('member/login', $data);
        } else {
            $this->load->view('member/setup_wiz');
        }
    }

    function sentmail($mail, $mailBody, $subject) {

        $this->load->library('email');
        $this->email->from('admin@arlians.com', 'Arlians');
        $this->email->to($mail);
        $this->email->set_mailtype('html');

        $this->email->subject($subject);

        $this->email->message($mailBody);

        $this->email->send();
        return true;
    }
	
	// Webservice API 
    //Social Login account check
    function socialUserValidation(){                

        $data = $result = array();
        $result['landing_page'] = 'setup_wiz_one';
        
        $data['email']      = $this->input->post('email');
        $data['fname']      = $this->input->post('fname');
        $data['lname']      = $this->input->post('lname');
        $data['type']       = $this->input->post('type');
        $data['social_id']  = $this->input->post('social_id');
        $data['referal']    = $this->input->post('referal');
        
        /*
        $data = array(  'email'=>"abhisekkundu25@gmail.com",
                        'fname'=>"Abhisek",
                        'lname'=>"Kundu",
                        'type'=>"google_plus", 
                        'social_id'=>"105961531760709105360",
                        'referal'=>'0'
                    );
        */ 


       //if(!empty($this->input->post)){

            try{

                $rs = $this->model_member->socialUserValidation($data);        
                $result['process']  = 'success';
                if($rs){       
             
                    $result['msg']  = 'User data already exist.';
                    
                    $rs1 = $this->model_member->data_exists_wiz_one($rs);
                    if($rs1=='TRUE'){
                        $result['landing_page'] = 'dashboard';
                    }           

                }else{
                    $rs = $this->model_member->insertSocialMemberData($data);            
                    $result['msg']  = 'User data added succesfully.';
                }
            
            }catch (Exception $e) {
                $result['process']  = 'fail';    
            }
       //}    
            
        echo json_encode($result);
        exit();
    }

}

