<?php 
class Newscron extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('cookie'); 
        //$this->load->model('admin/modeladmin'); 
        $this->load->model('model_memberprofile');
        $this->load->model('model_category');
		$this->load->model('model_member');
        $this->load->model('model_hub');
		$this->load->library('bingsearch');
		$this->load->library('User_Manager');
		$this->load->helper('common_helper');
		$this->load->model('model_suggestion');
        //$this->modeladmin->AllSiteSettings();
        
    }

    public function startCron()
    {
        $result = $this->insertNewsCron();
        $res;
        $r;
        $array = array();
        if($result == 1)
        {
            $res = $this->insertNewsImageNew();
        }

        if($res == 1)
        {
            $arr['responce'] = 'success';
        }
        else
        {
            $arr['responce'] = 'fail';
        }

        echo json_encode($arr);
    }

    public function insertNewsCron_old()
    {
        $activeSubSectors = $this->model_memberprofile->fetchActiveSubSectorCode();

        foreach($activeSubSectors as $cd)
        {
            $cd = explode('-', $cd['ans_set_cat_id']);
            $code = $cd[2];

            $d = $this->model_memberprofile->check_news_by_sub_sector($code);

                $current_date = date('Y-m-d'); 
                $result = $this->model_memberprofile->checkNewsAvailable($current_date,$code);

            if($d[0]['c'] < 7 && $result)
            {
                $str = get_bing_str($code);
                $string = $str['bing_query_str'];

                $r = $this->bingsearch->query('News',array(
                    'Query'=>"'".$string."'",//string
                    'Adult'=>"'Moderate'",//string
                    //'ImageFilters'=>"'Size:Small+Aspect:Square'",//string
                    //'Latitude'=>47.603450,//double
                    //'Longitude'=>-122.329696,//double
                    'Market'=>"'en-US'",//string
                    'NewsCategory'=>"'rt_Business'",//string
                    'Options'=>"'EnableHighlighting'",//string
                    'NewsSortBy'=>"'Relevance'",//string
                    
                )); 

                $data['mid'] = 0;
                $data['jsondata']= json_encode($r);
                $data['sub_sector_id'] = $code;
                $data['news_from'] = 'bing';
                $data['on_date'] = date('Y-m-d');

                $res = $this->model_category->insertBingNewsRespectToSubsector($data);
            }
            else
            {
                $current_date = date('Y-m-d'); 
                $res = $this->model_memberprofile->get_lastest_new_id($code);
                $result = $this->model_memberprofile->checkNewsAvailable($current_date,$code);

                if($result)
                {
                    $str = sub_sector_name($code);
                    $string = $str['subsector_name'];

                    $r = $this->bingsearch->query('News',array(
                        'Query'=>"'".$string."'",//string
                        'Adult'=>"'Moderate'",//string
                        //'ImageFilters'=>"'Size:Small+Aspect:Square'",//string
                        //'Latitude'=>47.603450,//double
                        //'Longitude'=>-122.329696,//double
                        'Market'=>"'en-US'",//string
                        'NewsCategory'=>"'rt_Business'",//string
                        'Options'=>"'EnableHighlighting'",//string
                        'NewsSortBy'=>"'Relevance'",//string
                        
                    ));

                    $data['mid'] = 0;
                    $data['jsondata']= json_encode($r);
                    $data['sub_sector_id'] = $code;
                    $data['news_from'] = 'bing';
                    $data['on_date'] = date('Y-m-d'); 
                   // $data['al_id'] =  $res[0]['al_id'];
                    //$data['type'] =  'bing'; 

                    $res = $this->model_category->updateBingNewsRespectToSubsector($data);
                }
            }
        }

        return 1;
        exit;
    }

    public function insertNewsCron()
    {
        $activeSubSectors = $this->model_memberprofile->fetchActiveSubSectorCode();
		//print_r($activeSubSectors);
        foreach($activeSubSectors as $cd)
        {
            $cd = explode('-', $cd['ans_set_cat_id']);
            echo $code = $cd[2]; 
            $d = $this->model_memberprofile->check_news_by_sub_sector($code);
            $distinctdates = $this->model_memberprofile->check_news_by_sub_sector_distinctdates($code);
            $first_date_for_code=$distinctdates['on_date'];
        

                $current_date = date('Y-m-d'); 
                $result = $this->model_memberprofile->checkNewsAvailable($current_date,$code);
            
              if($result)
              {
                
                if($d[0]['c']==7)
                {
                 $deleterecords= $this->model_memberprofile->deleterecordsbylastdate($first_date_for_code,$code);  
                }

                $str = get_bing_str($code);
                $string = $str['bing_query_str'];

                $r = $this->bingsearch->query('News',array(
                    'Query'=>"'".$string."'",//string
                    'Adult'=>"'Moderate'",//string
                    //'ImageFilters'=>"'Size:Small+Aspect:Square'",//string
                    //'Latitude'=>47.603450,//double
                    //'Longitude'=>-122.329696,//double
                    'Market'=>"'en-US'",//string
                    'NewsCategory'=>"'rt_Business'",//string
                    'Options'=>"'EnableHighlighting'",//string
                    'NewsSortBy'=>"'Relevance'",//string
                    
                ));
				//print_r($r);die();

                foreach($r as $key=>$resultarr)
                {
                    
                    foreach($resultarr->results as $singlearr)
                    {
                       
                        $data['mid'] = 0;
                        $data['id'] = $singlearr->ID;
                        $data['title'] = $singlearr->Title;
                        $data['url'] = $singlearr->Url;
                        $data['source'] = $singlearr->Source;
                        $data['description'] = $singlearr->Description; 
                        $data['sub_sector_id'] = $code;
                        $data['news_from'] = 'bing';
                        $data['on_date'] = date('Y-m-d'); 
                        $data['date'] = $singlearr->Date;  
                       $res = $this->model_category->insertBingNewsRespectToSubsector($data);
                    }
                }

              }
              else
              {
                //echo 'else';die();
              }

       }
       // echo 'lal';die();
        return 1;
        exit;
    }

    function insertNewsImageNew()
    {
        $result = $this->model_hub->getAllBingNews();
        $news = array();
       
        foreach($result as $key=>$row)
        {
			
           
			
		    $final['news_id']   = $row['id'];
			$final['image_url'] = getImageFromUrl($row['url']);
			
			//$final['logo']        = @getFavicon($r['url']);
		  // print_r($final);die(); 
			$chk = $this->model_hub->checkImageExist($row['id']);
			//echo $chk;die();
			if(!$chk)
			{
				$this->model_hub->insertNewsImage($final);
				echo $row['id']."------";
			}
			else
			{
				$this->model_hub->updateNewsLogo($final);
			}
			//die();
            /* $data = json_decode($data);

            $res = $data->d->results;
            if($res != NULL)
            {
                foreach($res as $k=>$r)
                {
                    $news[$key][$k]['id'] = $r->ID;
                    $news[$key][$k]['url'] = $r->Url;
                }
            }
            else
            {
                foreach($res as $k=>$r)
                {
                    $news[$key][$k]['id'] = $r->id;
                    $news[$key][$k]['url'] = $r->url;
                }  

            } */
        }
//echo '<pre>'; var_dump($news);
       /*  foreach($news as $row)
        {
            foreach($row as $r)
            {
                $final['news_id']   = $r['id'];
                $final['image_url'] = getImageFromUrl($r['url']);
                //$final['logo']        = @getFavicon($r['url']);
               
                $chk = $this->model_hub->checkImageExist($r['id']);
                
                if(!$chk)
                {
                    //pr($final,false);
                    $this->model_hub->insertNewsImage($final);
                    
                    echo $r['id']."<br />";
                }
                else
                {
                    $this->model_hub->updateNewsLogo($final);
                }
            }
        } */ 
        return 1;
        exit;
        //echo '<pre>'; var_dump($news); die();
    }
	
	
	/************** newsletter mail***************/
	public function FnSendNewsNotificationToMembers()
	{
	    $fivedaysagodate = date('Y-m-d', strtotime('-5 days'));
		$Usermids=$this->model_member->FnFindAllActiveUsers($fivedaysagodate);
		//print_r($Usermids);die();
		if(count($Usermids)>0)
		{
			
			foreach($Usermids as $mid)
			{
				// send email parameters
				$senderfullname=getMemberbusinName($mid['mid']);	
				$emailsent_to=$mid['email'];
			    $subsectorids=fetch_my_subsector($mid['mid']);
				$logobussiness='';
				$profileimage=getProfileImage($mid['mid']);
				if($profileimage=='')
				{
					$logobussiness=logobussinessname($mid['mid']);
				}
				
				
				//$profileimage=($profileimagename=='')?$profileimagename:$logobussiness;
				// print_r($subsectorids);
				 if(count($subsectorids)>0)
				 {
				   $latesnewsid=$this->model_member->FnFindNewsBySubSector($subsectorids);
				   $news_idarr=array();
				  // print_r($latesnewsid);die();
				    foreach($latesnewsid as $news_id)
				   {
					   
					  // $likecount=$this->model_hub->getNewsLike($news_id['id']);
					  // $sharecount=$this->model_hub->getNewsShareCount($news_id['id']);
					  // $commentcount=$this->model_hub->getNewsCommentCount($news_id['id']);
					  // $totalcount=$likecount+$sharecount+$commentcount+$totalcount;
					   //if($totalcount>0)
					  // {
						   $news_idarr[]=$news_id['id'];
					  // }
					   //if(count($news_idarr))
				   } 
			       if(count($news_idarr)>0)
				   {
					  // krsort($news_idarr); 
                      //print_r($news_idarr);
					  //$emailsent_to='lalbhusan2014@gmail';
				       $this->FnSendNewsNotificationEmail($emailsent_to,$senderfullname,$news_idarr,$profileimage,$logobussiness);
				   
				   }
				   
				   
				   
				  
				 }
				 
				 
				 // send email parameters end
				//$this->FnSendnotificationToUsernotification($emailsent_to,$senderfullname);
			}
			 
		}
		
		
		//exit; 
		
	echo 'done';
        exit;
		
	}
	
		function FnSendNewsNotificationEmail($emailsent_to,$senderfullname,$newsidArray,$profileimage,$logobussiness)
{
	$codesection='<span style="background-color: #fcc315;border-radius: 50%;color: #fff;display: inline-block;font-size:40px;height: 100px;line-height: 100px;text-align: center;text-transform: uppercase;width:100px;">'.$logobussiness.'</span>';
	$imagesection='<img src="'.base_url().'uploads/profile_image/'.$profileimage.'" />';
	if($profileimage=='')
	{
		$imagesection=$codesection;
	}
	$counter=0;
	//print_r($newsidArray);die();
	foreach ($newsidArray as $key=>$value) 
	{
	  $newsarr=$this->model_member->FnGetNewsDetails($value);
	 
	  if($counter==0)
	  {
		 $firstrownews='<tr style="background-color:#fff;"> 
            <td width="50%"><img src="'.$newsarr['image_url'].'" width=261px height=232px style="width:100%; display:block;"  alt=""></td>
            <td width="50%" style="padding:20px; background-color:#fff;">
              <h4 style="font-size:22px; line-height:24px; margin:0;">'.str_replace('?','',substr($newsarr['title'],0,35)).'</h4>
              <p>'.str_replace('?','',$newsarr['description']).'</p>
              <a style="background:#357cb7; color:#fff; padding:5px 20px; border-radius: 30px; -moz-border-radius: 30px; -webkit-border-radius: 30px; -ms-border-radius: 30px; text-decoration:none; display:inline-block;" href="'.$newsarr['url'].'">Read More</a>
              
            </td>
          </tr>'; 
	  }
	  else
	  {   
         if($counter==1 || $counter==3)
		  {
			$str .='<tr>';
		  }	 
          
  		 $str .='<td width="50%"> 
             <table border="0" cellpadding="0" cellspacing="0" width="50%">
               <tr>
                 <td style="background-color:#fff;">
                 <img src="'.$newsarr['image_url'].'" alt="" width=266px height=100px style="display:block;">
                  <h4 style="font-size:22px; line-height:24px; margin:0; padding:20px 20px 0;">'.str_replace('?','',substr($newsarr['title'],0,35)).'</h4>
                  <p style="padding:0 20px 0;">'.str_replace('?','',$newsarr['description']).'</p>
                  <a style="background:#357cb7; margin:0 20px 20px; color:#fff; padding:5px 20px; border-radius: 30px; -moz-border-radius: 30px; -webkit-border-radius: 30px; -ms-border-radius: 30px; text-decoration:none; display:inline-block;" href="'.$newsarr['url'].'">Read More</a>
                  
                 </td>
               </tr>
             </table>
            </td>' ;   
			if($counter==2 || $counter==4)
		  {
			$str .='<tr>';
		  }	
		   
	  }
	  $counter++;
    }
//echo $str;die();
	//print_r($newsidArray);die();
	
	
	
	
	$mailBody = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Arlians</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="keywords"  content="" />
	<meta name="Resource-type" content="Document" />
  <meta name="viewport" content="width=device-width, initial-scale=1">


	<link href="fonts/fonts.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">

	<style type="text/css">
  * { box-sizing: border-box;}
   body{margin: 0; font-family: Arial; font-size: 13px; line-height: 20px; color:#515151;}
   ::-moz-placeholder{ color: #357cb7; opacity: 1;}
  
  </style>	

</head>



<body style="background-color:#d1d1d1;">

<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding: 88px 0;">
  <tr>
    <td>
      <table border="0" cellpadding="0" cellspacing="0" style="width: 600px; background-color:#fff; margin: 0 auto;">
        <tr style="background-color: #357cb7;">
          <td style="padding: 35px; color: #fff; width:50%; "><a href="#"><img src="'.base_url().'image/noti-logo.png" alt=""></a></td>      
          <td align="right" style="width:50%; padding: 35px;"><a href="'.base_url().'" style="background:#ffffff; color:#357cb7; padding:5px 20px; border-radius: 30px; -moz-border-radius: 30px; -webkit-border-radius: 30px; -ms-border-radius: 30px; text-decoration:none;">View Online</a></td>
        </tr>
       </table>

       <table border="0" cellpadding="0" cellspacing="0" style="width: 600px; padding:40px 20px; background-color:#f1f1f1; margin: 0 auto;">
         <tr>
           <td align="center">
		   '.$imagesection.'
           <br/>
             <h2 style="font-family: "source_sans_prolight", Arial; font-size:20px; color:#515151;">Hey '.$senderfullname.', here is</h2>
             <h1 style="font-size:33px; color:#357cb7;">this week in the news on Arlians.</h1>
           </td>
         </tr>         
       </table>
        <table border="0" cellpadding="0" cellspacing="0" style="width: 600px; padding:0 20px 30px; background-color:#f1f1f1; margin: 0 auto;">
          '.$firstrownews.'
        </table>
        
        <table border="0" cellpadding="0" cellspacing="0" style="width: 600px; padding:0 20px 30px; background-color:#f1f1f1; margin: 0 auto;">
          '.$str.'
		  <tr>
            <td align="center" colspan="2">
			<img src="'.base_url().'image/noti-logo.png" alt="">
            <p style="margin-top:10px; margin-bottom:0;">Manage email preferences in your settings</p></td>
		  </tr>
		  
        </table>
		 



  

 
</body>
</html> 


';      
        $subject='This week in the news on Arlians.';
       

        $from = "notification@arlians.com";
		//$emailsent_to='lalbhusan2014@gmail.com';
		$this->sentmail($emailsent_to, $mailBody, $subject, $from);  
	
}

	/************** newsletter mail***************/
	
	/*********** been while email ***********/
	
	
	public function FnSendInActiveNotificationToMembers()
	{
		
		$tendaysagodate = date('Y-m-d', strtotime('-10 days'));
		$seventeendaysagodate = date('Y-m-d', strtotime('-17 days'));
		$thitrytwodaysagodate = date('Y-m-d', strtotime('-32 days'));
		$InActiveUsers=$this->model_member->FnFindInActiveUsers($tendaysagodate,$seventeendaysagodate,$thitrytwodaysagodate);
		//print_r($InActiveUsers);die();
		if(count($InActiveUsers)>0)
		{
			foreach($InActiveUsers as $User)
			{
				// update the notification email count in table
				$Last_login_time=$User['last_login_time'];
				$sentmail=0;
				if($Last_login_time==$tendaysagodate)
				{
					$sentmail=1;
				}
				if($Last_login_time==$seventeendaysagodate)
				{
					$sentmail=2;
				}
				if($Last_login_time==$thitrytwodaysagodate)
				{
					$sentmail=3;
				}
				$updatedata=array('notification_email_count'=>$sentmail);
				$this->db->where('mid', $User['mid']);
                $this->db->update('ar_members', $updatedata);
				
				// update the notification email count in table
				
				// send email parameters	
				 $senderfullname=$User['fname'];
				 $emailsent_to=$User['email']; 
				 // send email parameters end
				$this->FnSendnotificationToUsernotification($emailsent_to,$senderfullname);
			}
			 
		}
		
		
		//exit; 
		
	echo 'done';
        exit;
		
	}
	
		function FnSendnotificationToUsernotification($emailsent_to,$senderfullname)
{
	
	$mailBody = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Arlians</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="keywords"  content="" />
	<meta name="Resource-type" content="Document" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

 <link href="fonts/fonts.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">
	
 <style type="text/css">
  * { box-sizing: border-box;}
   body{margin: 0; font-family: Arial; font-size: 13px; line-height: 20px; color:#515151;}
   ::-moz-placeholder{ color: #357cb7; opacity: 1;}
   .main-body{ width: 352px;}

    @media screen and (max-width: 767px) {
    .main-body{ width: 90%;}   
   }
  

  </style>  
	

</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" background="'.base_url().'image/email-bg03.jpg"  style="padding: 88px 0;"> 

  <tr>
    <td>
        <table border="0" cellpadding="0" cellspacing="0" class="main-body" style="width:352px;background-color:#fff; text-align: center; margin: 0 auto;">
          <tr>
            <td style="padding: 25px 40px;">
               <img src="'.base_url().'image/icon-exclm.png" alt="" style="display:inline-block;">
               <h4 style=" font-weight: lighter; font-size:22px; color:#444; line-height:28px; font-family: "source_sans_prolight", Arial;margin-bottom:0px;">Hey '.$senderfullname.',<br/>we&#39;re missing you!</h4>
               <h2 style="font-size:30px; line-height:30px; margin-bottom:0px;margin-top:0px;">It&#39;s been a while !</h2> 
            </td>
          </tr>
           

         

           <tr>
            <td style="border-top:1px solid #eee; padding: 10px 40px 25px 40px ;">
             <span style="font-size:20px; font-family: "source_sans_prolight", Arial; display:block; margin-bottom:10px;">You can Log in here:</span>
           <a href="'.base_url().'" style="background-color: #fe4646 ; border-radius: 30px; color: #fff; display: inline-block; margin: 5px 0;  padding: 8px 30px;  text-decoration: none; width:170px; font-weight: 900;">Login</a>
           <p style="font-size:12px; margin:20px 0 0;">Manage email preferences in your settings</p>       
          
            </td>
          </tr> 
         
        </table> 
         
       <table width="100%" border="0" cellpadding="0" cellspacing="0" style="text-align:center;  font-size: 13px; margin-top: 30px; text-align: center; color:#fff;">
        <tr>
          <td>
            <img src="'.base_url().'image/noti-logo.png" alt="">            
          </td>
        </tr>
      </table>

    </td>
  </tr>
</table>
 
</body>
</html>

';      
        $subject='We’re missing you!';
      // $emailsent_to='sims12781@gmail.com';
	   //$emailsent_to='lalbhusan2014@gmail.com';
        $from = "notification@arlians.com";
		//echo $mailBody;
		$this->sentmail($emailsent_to, $mailBody, $subject, $from);  
	
}


	
	/********** been while email *******************/
	
	 function sentmail($mail, $mailBody, $subject, $from) {



        $this->load->library('email');

        $this->email->from($from, 'Arlians');

        $this->email->to($mail);

        $this->email->set_mailtype('html');



        $this->email->subject($subject);



        $this->email->message($mailBody);

        //echo 'hello';die(); 

        $this->email->send();

        return true;
    }

	


function FnSendBusinessNewsletter()
{
	$subscribedusers=$this->model_member->FnGetSubscribedUser();
	//print_r($subscribedusers);die();
	if(count($subscribedusers)>0)
	{
		foreach($subscribedusers as $usersarr)
		{
		// suggesstion list section
		    $user_id=$usersarr['mid'];
            //$data['is_wiz_two_empty'] = $this->user_manager->isAlreadyDataInWizTwox($user_id);
		//	print_r($data['is_wiz_two_empty']);
            $id = $user_id;
            $data['mid'] = $user_id;
           
            //for suggestion  
            $sids = $this->model_suggestion->suggestionSearchTypes($id);
			//print_r($sids);die();
            $data = array();
            $searchT = array();
            $type = array();
            foreach ($sids as $sid) {
                $s = explode('-', $sid);
                $searchT[] = $s[0];
                $type[] = $s[1];
            }
			
            $user = getParticularUserDetails($id);

            $user_own_type = $user['bussinesstype1']; 
		  
            $pos;
            $pos1;
            if ($user_own_type == 'Buisness') {
                if (in_array('3c', $type)) {
                    $pos = array_search('3c', $type);
                    unset($type[$pos]);
                }
                if (in_array('4b', $type)) {
                    $pos1 = array_search('4b', $type);
                     unset($type[$pos1]);
                }
            }
           
           
            foreach ($type as $ty) {
                $data['last_suggested_id'][$ty] = $this->model_suggestion->getSuggestionByType($id, $ty);
            }
			
            $data['looking_for'] = $searchT;
			
			
            $data['tag'] = $this->model_member->getAllTag();
            
            $chk_sharecode = $this->model_member->shareCodeExist($data['mid']);
			
            if ($chk_sharecode == 'false') {
                $user_name = getMemberName($user_id);
                $ar_members = md5(date("dmY-his") . $user_name);
                $this->model_member->shareCode($data['mid'], $ar_members);
            }

            //$data['is_wiz_two_empty'] = $this->user_manager->isAlreadyDataInWizTwox($user_id);
            $id = $user_id;
            $data['mid'] = $user_id;
                    
            
            $data['suggested_list'] = $this->model_suggestion->suggestionCountbyCustomer($id);
            
            $data['lookingfor_list'] = $this->model_suggestion->suggestionSearchTypes($id);
            
            $lookingfor_list = $data['lookingfor_list']; //$this->model_suggestion->suggestionSearchTypes($id);
		
            $ids = showOnDashboard($user_id);
		
            //--------------------------------------------------
            $user = getParticularUserDetails($user_id);

            $user_own_type = $user['bussinesstype1'];
            $arr_key = array();
            $sugg_ids = array();
            $new_member_ids = array();
            foreach($ids as $id){
                $sugg_ids[$id['searchtype']][] = $id['suggested_id'];
            }   
			
                
			
            $memArr = array();
            foreach($sugg_ids as $su_id){
                foreach($su_id as $id){
                    $memArr[] = $id;
                }
            }
			
            foreach($lookingfor_list as  $looking_for_id ){                             
                foreach($memArr as $suggested_id){
                    $suggested_user_type = userProfType($suggested_id);
                    $looking_for_id=explode('-',$looking_for_id);
                    $looking_for_id = $looking_for_id[0];

                    $arr[] = $looking_for_id[1];

                    $result = checkToShowUserInSearchResult($looking_for_id,$user_own_type);

                    if(strtolower($suggested_user_type) == strtolower($user_own_type) || $result == "BOTH"){
                        $is_score=isScoreNotZero($suggested_id,$user_id);
                        if( $is_score!=0){
                            $new_member_ids[] = $suggested_id;
                        }
                    }
                }
            }
			
            //echo '<pre>'; var_dump($lookingfor_list);
            $new_member_ids=array_unique($new_member_ids);
			
		   //print_r($new_member_ids);DIE();
            $i=0; 
            $showres=array();
            foreach($new_member_ids as $k=>$val){ 
                $id = $user_id;
                $res = suggestionTypeFromId($id,$val);
                $showres[$res][]=$val;
            }
			
			
			// here the mail sent to users
			
			
			//print_r($showres);
		    foreach($showres as $resultarr)
			{
				$businessids=array();
				for($i=0;$i<=count($resultarr)-1;$i++)
				{
					$currentmonth=date("m");
					$suggestedid=$resultarr[$i];
					$profilecompletion=$this->model_memberprofile->profileComplition($resultarr[$i]);
					if($profilecompletion>=90)
					{
						$chebusinesseligibity=$this->model_memberprofile->FnCheckbusinessElibility($user_id,$suggestedid,$currentmonth);
						if($chebusinesseligibity==0)
						{
							$businessids[]=$suggestedid;
						}
						
						
					}
					
					
				}
				array_slice($businessids,3);
				//int_r($businessids);die();
				if(count($businessids)>0)
				{ 
			       
					$businessdetails=$this->model_memberprofile->FnGetBusinessEmail($businessids);
					foreach($businessdetails as $businesssinglearr)
					{
						$profileimage=getProfileImage($businesssinglearr['mid']);
						if($profileimage=='')
						{
							$logobussiness=logobussinessname($businesssinglearr['mid']);
						}
						$codesection='<span style="background-color: #fcc315;border-radius: 50%;color: #fff;display: inline-block;font-size:40px;height: 100px;line-height: 100px;text-align: center;text-transform: uppercase;width:100px;">'.$logobussiness.'</span>';
						$imagesection='<img width="70px" height="70px" src="'.base_url().'uploads/profile_image/'.$profileimage.'" />';
						if($profileimage=='')
						{
							$imagesection=$codesection;
						}
						// send email parameters
				        $senderfullname=getMemberbusinName($businesssinglearr['mid']);	
				        $emailsent_to=$businesssinglearr['email'];
							
						$messagebody .='<tr>
							<td align="center">
							  <table border="0" cellpadding="0" cellspacing="0" class="business_box"  bgcolor="#ffffff">
								<tr>
								  <td align="center" style="padding:20px;">
									'.$imagesection.'
									<h5 style="color:#357cb7; font-size:15px; line-height:15px; margin-top:10px; margin-bottom:20px;">'.$senderfullname.'</h5>
									<p style="color:#444444;" >'.substr(strip_tags($businesssinglearr['bussinessdesc']),0,100)   .'</p>
								  </td>
								</tr>
							  </table>
							</td>
						  </tr>
						  <tr>
							 <td height="25">&nbsp;</td>
						  </tr>
						  ';
						  // Save data to database
						  $emailsentarra['sent_in_month']=date("m");
						  $emailsentarra['mid']=$user_id;
						  $emailsentarra['suggestionid']=$businesssinglearr['mid'];
						  $this->model_memberprofile->FnInsertEmail($emailsentarra);
						  
						  // Save data to database
						  
						  
						  
						  
					}
					
//echo $messagebody; 
					$this->FnSendBusinessNewslettermail($usersarr['email'],$messagebody);
					
					
					
					
					
					
				
				}
				
				
			}
			
			// here mail sent to the users
			
		
           
            
          
       
        // suggesstion list section
		}
	}
	echo 'done';
    exit;
}


	function FnSendBusinessNewslettermail($emailsent_to,$inneremailcontent)
{
	
	
	$mailBody = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Arlians</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="keywords"  content="" />
	<meta name="Resource-type" content="Document" />
  <meta name="viewport" content="width=device-width, initial-scale=1">


	<link href="fonts/fonts.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">

	<style type="text/css">
  * { box-sizing: border-box;}
   body{margin: 0; font-family: Arial; font-size: 13px; line-height: 20px; color:#515151;}
   ::-moz-placeholder{ color: #357cb7; opacity: 1;}
   .main_body{ width: 600px;}
   .business_box{ width: 300px;}
   .copyright {
    display: inline-block;   
    }
    .footer_link {
    display: inline-block;
    float: right;
}
.footer_link td a {
    padding: 0 10px;
}

    @media screen and (max-width: 767px) {
    .main_body{ width: 90%;}   
    .business_box{ width: 90%;}
    .copyright{ width: 100%;}
   }
  
  </style>	

</head>



<body style="background-color:#d1d1d1;">

<table width="100%" border="0" cellpadding="0" cellspacing="0" style="padding:10px 0 88px 0;">
  <tr>
    <td>
      <table border="0" cellpadding="0" cellspacing="0" class="main_body" style="background-color:#fff; margin: 0 auto;">
        <tr><td height="25">&nbsp;</td></tr>
        
        <tr style="background-color: #ffffff;">
          <td align="center"><img width="68" height="auto" src="'.base_url().'image/logo-circle.png" alt="" style="margin-bottom: -38px;"></td>
        </tr>
        <tr>
          <td>
            <table border="0" width="100%" cellpadding="0" cellspacing="0" style="background-color:#357cb7; color:#ffffff; margin: 0 auto;">
              <tr>
                <td style="padding-top:30px;" align="center"><h3 style="font-size:25px; line-height:25px; font-weight:normal;">Here are some amazing businesses</h3></td>
              </tr>
              <tr>
                <td>
                  <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
                    <tbody>
                      <tr>
                        <td width="40%" height="2"></td>
                        <td width="10%" height="2" style=" border-bottom:2px solid #ffffff;"></td>
                        <td width="40%" height="2"></td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td height="25">&nbsp;</td>
              </tr>
              <tr>
                <td align="center"><h4 style="font-size:22px; line-height:22px; margin-top:0; margin-bottom:25px;">you can find on Arlians.</h4></td>
              </tr>
               '.$inneremailcontent.'
              </tr>
                <tr>
                 <td align="center"><a style="background:#ffffff; display:inline-block; color:#357cb7; padding:5px 20px; border-radius: 30px; -moz-border-radius: 30px; -webkit-border-radius: 30px; -ms-border-radius: 30px; text-decoration:none; font-weight:bold;" href="'.base_url().'">View Online</a></td>
              </tr>
                <tr>
                 <td height="25">&nbsp;</td>
              </tr>
              <tr>
                 <td>
                   <table border="0" cellpadding="0" cellspacing="0" width="100%" bgcolor="#ffffff" style="color:#444444; padding:20px;">
                      <tr class="copyright">
                        <td>Copyright © 2016 <a href="'.base_url().'"  style="color:#444; text-decoration:none;">Arlians.</a></td>
                        </tr>
                        <tr class="footer_link">
                          <td>
                            <table>
                              <tr>
                              <td align="center" style="border-right:1px solid #444;"><a href="'.base_url().'" style="color:#444; text-decoration:none;">View in browser</a></td>
                              <td align="center" ><a href="'.base_url().'member/newsletterunsubscribe'.'" style="color:#444; text-decoration:none;">Unsubscribe</a></td>
                                               
                            </tr>
                            </table>
                          </td>
                        </tr>                       
                   </table>
                 </td>
              </tr>
            </table>
          </td>
        </tr>    
      </table>
    </td>
  </tr>
 
</table>


</body>
</html>';    
 
        $subject='Here are some amazing businesses';
        $from = "notification@arlians.com";
		//echo $emailsent_to
		$emailsent_to='lalbhusan2014@gmail.com';
		
		$this->sentmail($emailsent_to, $mailBody, $subject, $from);  
	 
}
    




    
}