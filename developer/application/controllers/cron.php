<?php 
class Cron extends CI_Controller
{
    function __construct(){
		parent::__construct();
		$this->load->model('model_member');
		//$this->load->model('model_message');
		$this->load->library('User_Manager');
		$this->load->helper('common_helper');
		//$this->load->helper('text');
		//$this->load->model('model_othermembersprofile');
		//$this->model_common->SiteSettingsDetails(); // This is mandatory
		$this->user_manager = User_Manager::get_instance();
	}
    
    function setFeatureMail(){
        
        $mails = $this->checkMemberStatus(1); 
        //echo '<pre>'; var_dump($mails); die();
        $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Arlians Welcome</title>
<style type="text/css">
body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
p, a, li, td {-webkit-text-size-adjust:100%}
.ReadMsgBody {width:100%}
.ExternalClass {width:100%}
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
table td {border-collapse:collapse}
br.hideDesktop {display:none !important}
br.hideMobile {display:block !important}
@media only screen and (max-device-width:480px) {
    table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
    table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
    table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
    br[class="hideDesktop"] {display:block !important}
    br[class="hideMobile"] {display:none !important}
}
</style>
</head>
<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="background-color:#e2e2e2" align="center">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="600" align="center">
                            <table width="600" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                <tr>
                                    <td align="center" style="background-color:#ED5664"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:centre">THANKS FOR SIGNING UP TO ARLIANS</span> 
                                    </td>
                                </tr>
                            </table>
                            <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center" style="background-color:#fbfbfb">
                                        <img src="http://arlians.com/assets/images/Arlians-Full-Logo.png" width="300" class="twosixfive" alt="Arlians" style="border:0;display:block" />
                                    </td>
                                </tr>
                            </table>
                            <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                <tr>
                                    <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2">
                                    <span style="font-size:14px;color:#4D575E">Your journey to smarter business networking has just begun. To help you along, we put together some useful tips on how to get the most from Arlians, and skyrocket your business to success
<br />
</span> 
                                        <br/> <span style="color:#00AAA0"><strong>Complete your profile
</strong></span> <br /><br />
<img width="100%" style="border:0;display:block" class="twosixfive" src="'.base_url().'uploads/email_images/profile-screenshot.png">
                                      
                                        <br/> <span style="font-size:14px;color:#4D575E">No social network is complete without some branding. Adding pictures, a description, and your website will not only increase the likelihood of businesses connecting with you, it will also provide you with a platform with which to shout about what you do
 <br />
</span> <br />
<br/> <span style="color:#00AAA0"><strong>Share your product
</strong></span> <br /><br />
<span style="font-size:14px;color:#4D575E">While you’re at it, why not showcase your product? It’s a free and simple way to get your products in front of businesses</span>

                                        <br/>
                                        <br/>
                                        <span style="color:#00AAA0"><strong>See your matches
</strong></span><br /><br />
<img width="100%" style="border:0;display:block" class="twosixfive" src="'.base_url().'uploads/email_images/score-screenshot.png" ><br />
<span style="font-size:14px;color:#4D575E">No more trawling through search engines or Linkedin, unsure whether the business you’ve found would be interested in you. Every match you see on your dashboard has expressed interest in businesses like yours. View and connect with them safe in the knowledge that they’ll want to hear from you
</span><br /><br />

<span style="color:#00AAA0"><strong>Share content
</strong></span><br /><br />
<img width="100%" style="border:0;display:block" class="twosixfive" src="'.base_url().'uploads/email_images/hub-screenshot.png"><br />
<span style="font-size:14px;color:#4D575E">The hub is the perfect place for you to share your industry news, latest thought content, or even your latest promotions. Our unique matching algorithm shares this directly with businesses relevant to your ambitions, meaning the target audience you never knew existed will see and recognise you  
</span><br /><br />
<span style="font-size:14px;color:#4D575E">Any feedback/comments? We’d love to hear from you. Simply hit reply to this email, or contact us on our social media pages: 
</span><br /><br />
<span style="font-size:14px;color:#4D575E">[links to Facebook, Blog, twitter]</span><br /><br />
<span style="font-size:14px;color:#4D575E">The Arlians team</span><br />
                                    </td>
                                </tr>
                            </table>
                            <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                            
                            </table>
                <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" style="background-color:#01A69C">
                            <table border="0" cellspacing="0" cellpadding="10">
                                <tr>
                                    <td width="24px">
                                        <a href="mailto:hello@arlians.com">
                                            <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-forwardtofriend-48.png" style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24"
                                            class="">
                                        </a>
                                    </td>
                                    <td width="24px">
                                        <a href="https://twitter.com/Arlians_">
                                            <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24"
                                            class="">
                                        </a>
                                    </td>
                                    <td width="24px">
                                        <a href="https://www.linkedin.com/company/10075228?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A2-1-4%2CtarId%3A1437656243753%2Ctas%3Aarlians%20limited">
                                            <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-linkedin-48.png" style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24"
                                            class="">
                                        </a>
                                    </td>
                                    <td width="24px">
                                        <a href="https://www.facebook.com/Arlians-1671076353121515">
                                            <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24"
                                            class="">
                                        </a>
                                    </td>
                            </table>
                        </td>
                        </tr>
                </table>
                <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                    <tr>
                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">Copyright © 2015 Arlians, All rights reserved.
                            <br/>You are receiving this email as you signed up to Arlians website.
                            <br/>
                            Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
                        </td>
                    </tr>
                </table>
</body>
</html>';
        $subject = "Getting the most out of business networking";
        $from = "hello@arlians.com";
        foreach($mails as $mail){
            
            $this->sentmail($mail,$mailBody,$subject,$from);
        }
    }
    
    function checkMemberStatus($x){
        $dt = date('Y-m-d',strtotime("-".$x." days"));
        $result = $this->model_member->fetch_yesterdays_member($dt);
        $email_arr = array();

        foreach($result as $res){
            $email_arr[] = $res['email'];
        }
        
        return $email_arr;
    }
    
    function sentmail($mail,$mailBody,$subject,$from){

		$this->load->library('email');
		$this->email->from($from, 'Arlians');
		$this->email->to($mail);
		$this->email->set_mailtype('html');

		$this->email->subject($subject);

		$this->email->message($mailBody);

		$this->email->send();
		return true;

	}
    
    function sentMailOnDayTwo(){
        
        $mails = $this->checkMemberStatus(2); 
        
        $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Arlians Welcome</title>
<style type="text/css">
body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
p, a, li, td {-webkit-text-size-adjust:100%}
.ReadMsgBody {width:100%}
.ExternalClass {width:100%}
.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
table td {border-collapse:collapse}
br.hideDesktop {display:none !important}
br.hideMobile {display:block !important}
@media only screen and (max-device-width:480px) {
    table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
    table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
    table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
    br[class="hideDesktop"] {display:block !important}
    br[class="hideMobile"] {display:none !important}
}
</style>
</head>
<body>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td style="background-color:#e2e2e2" align="center">
                <table border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="600" align="center">
                            <table width="600" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                <tr>
                                    <td align="center" style="background-color:#ED5664"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:centre">THANKS FOR SIGNING UP TO ARLIANS</span> 
                                    </td>
                                </tr>
                            </table>
                            <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="center" style="background-color:#fbfbfb">
                                        <img src="http://arlians.com/assets/images/Arlians-Full-Logo.png" width="300" class="twosixfive" alt="Arlians" style="border:0;display:block" />
                                    </td>
                                </tr>
                            </table>
                            <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                <tr>
                                    <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2">
                                    <br/> <span style="color:#00AAA0"><strong>Content
</strong></span> <br />
                                    <span style="font-size:14px;color:#4D575E; float:left; width:100%; text-align:left;">Dear member
<br /><br />
</span>                                       
                                     <span style="font-size:14px;color:#4D575E; float:left; width:100%; text-align:left;">We hope you’ve enjoyed the past few days of exploring business opportunities with Arlians. 
                                     <br /><br />
                                     As a new member, we’d like to offer you a free trial of our premium service. 

</span> <br /><br /><br /><br />
<span style="color:#00AAA0"><strong>WHAT’s THE CATCH?
</strong></span> <br />
<p style="color:#ee0018; font-size:14px; line-height:18px; text-align:left;">There isn’t one. There is no commitment and you can cancel anytime during the 14-day period without being charged. If on the other hand, you like what you see and choose to keep your premium subscription at the end of 14 days, there is no effort required on your part, you’ll automatically continue with your subscription.  <br />
Even if you change your mind later, there are no minimum contracts and you can cancel anytime. Sound interesting? Wait till you see what you can do with premium
</p>
<span style="font-size:16px; color:#4D575E; text-align:left; width:100%; display:inline-block;">
You have just 28 days left to take advantage of this offer. 
</span><br /><br />
                                    </td>
                                </tr>
                            </table>
                            <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                            
                            </table>
                <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td align="center" style="background-color:#01A69C">
                            <table border="0" cellspacing="0" cellpadding="10">
                                <tr>
                                    <td width="24px">
                                        <a href="mailto:hello@arlians.com">
                                            <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-forwardtofriend-48.png" style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24"
                                            class="">
                                        </a>
                                    </td>
                                    <td width="24px">
                                        <a href="https://twitter.com/Arlians_">
                                            <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-twitter-48.png" style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24"
                                            class="">
                                        </a>
                                    </td>
                                    <td width="24px">
                                        <a href="https://www.linkedin.com/company/10075228?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A2-1-4%2CtarId%3A1437656243753%2Ctas%3Aarlians%20limited">
                                            <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-linkedin-48.png" style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24"
                                            class="">
                                        </a>
                                    </td>
                                    <td width="24px">
                                        <a href="https://www.facebook.com/Arlians-1671076353121515">
                                            <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/color-facebook-48.png" style="display: block;border: 0;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24"
                                            class="">
                                        </a>
                                    </td>
                            </table>
                        </td>
                        </tr>
                </table>
                <table width="600" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                    <tr>
                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">Copyright © 2015 Arlians, All rights reserved.
                            <br/>You are receiving this email as you signed up to Arlians website.
                            <br/>
                            Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
                        </td>
                    </tr>
                </table>
</body>
</html>';
        $subject = "Increase your potential; try premium for FREE today";
        $from = "hello@arlians.com";
        
        foreach($mails as $mail){
            
            $this->sentmail($mail,$mailBody,$subject,$from);
        }
        
    }
    
    function bing()
    {
        $base = $this->url();
        require($base.'bing/BingSearch.php');
            //register for key on windows azure
        $apiKey = 'PoIxFBdtOcp41KuAEFEzeOKOAWV29OfibqQb7cKjndk';
        
        $bing = new BingSearch($apiKey);
        
        # Example 1: simple image search
        /*
        $r = $bing->queryImage('xbox');
        var_dump($r);
        */
        
        # Example 2: advanced image search
        //https://datamarket.azure.com/dataset/bing/search#schema
        //be sure to respect the data types in the values

        $sql = "SELECT `subsector_name` FROM `ar_subsector`";
        $res = $this->db->query($sql);
            foreach($res->result() as $row){

                //$name = stripslashes($row->subsector_name);
            echo $str = stripslashes($row->subsector_name);

           /* $r = $bing->query('News',array(
                'Query'=>"'".$str."'",//string
                'Adult'=>"'Moderate'",//string
                //'ImageFilters'=>"'Size:Small+Aspect:Square'",//string
                //'Latitude'=>47.603450,//double
                //'Longitude'=>-122.329696,//double
                //'Market'=>"'en-US'",//string
                'NewsCategory'=>"'rt_Business'",//string
                'Options'=>"'EnableHighlighting'",//string
                'NewsSortBy'=>"'Relevance'",//string
                
            )); */

                echo '<pre>';
                var_dump($r);
            }
    }

    function url()
    {
      return sprintf(
        "%s://%s%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
        $_SERVER['REQUEST_URI']
      );
    }
	
}