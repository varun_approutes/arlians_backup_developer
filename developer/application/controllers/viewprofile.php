<?php 
class Viewprofile extends CI_Controller
{
    function __construct(){
		parent::__construct();
		
		$this->load->model('model_member');
		$this->load->model('model_message');
		$this->load->library('User_Manager');
		$this->load->helper('common_helper');
		$this->load->helper('text');
		$this->load->model('model_othermembersprofile');
		$this->load->model('model_memberprofile');
		//$this->model_common->SiteSettingsDetails(); // This is mandatory
		$this->user_manager = User_Manager::get_instance();
	}
	/*function profile(){
		$data['whose_profile_viewed'] = $this->uri->segment(3);
		$data['who_viewed'] = $this->session->userdata['logged_in']['id'];
		$data1['whose_profile_viewed'] = $this->uri->segment(3);
		$data1['who_viewed'] = $this->session->userdata['logged_in']['id'];
		$id = $this->uri->segment(3);
		//$data = $this->model_othermembersprofile->getUserDetails($id);
		$data = $this->model_othermembersprofile->getUserDetails($id);
		$getVisibleStatus = $this->model_othermembersprofile->getVisibleStatus($this->session->userdata['logged_in']['id']);		
		if($getVisibleStatus->hide_me_prof_view==1){
			$this->model_othermembersprofile->inser_view($data1);
		}
		$data['member_type'] = $this->model_othermembersprofile->getMemberType($id);
        $data['latest_post'] = $this->model_memberprofile->getUserLatestFivePost($id);
		//echo "<pre>"; print_r($data); exit;
		$this->load->view('viewprofile/viewprofile',$data);
	}*/
	
	public function otherProfileApi(){
		//var_dump($_POST);
		$arr['email'] = $this->input->post('user_email');
		
	    $user_id = getIdByUserEmail($arr['email']);
		$mid = $user_id->mid;
		$othermid = $this->input->post('member_id');
		//var_dump($mid);die();
		$data['whose_profile_viewed'] = $othermid ;
		$data['who_viewed'] = $mid;
		$data1['whose_profile_viewed'] = $othermid ;
		$data1['who_viewed'] = $mid;
		$id = $othermid ;
		//$data = $this->model_othermembersprofile->getUserDetails($id);
		$data = $this->model_memberprofile->getUserDetailsApi($id);
		$getVisibleStatus = $this->model_othermembersprofile->getVisibleStatus($mid);		
		if($getVisibleStatus->hide_me_prof_view==1){
			$this->model_othermembersprofile->inser_view($data1);
		}
		$data['member_type'] = $this->model_othermembersprofile->getMemberType($id);
        $data['latest_post'] = $this->model_memberprofile->getUserLatestFivePost($id);
		$data['isConnected'] = is_connected($mid,$othermid);
		echo json_encode($data);
        exit; 
		//$this->load->view('viewprofile/viewprofile',$data);
	}
}