<?php
require_once('BasicExcel/Reader.php');
\BasicExcel\Reader::registerAutoloader();
class Export extends CI_Controller {
	private $username;
	private $password;
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('system_models/ar_subsector_model');
		$this->load->model('system_models/relevant_keys_model');
		$this->load->model('system_models/ar_request_access_page_model');
		$this->username="1213a261-d453-474d-bd8f-51e95d73802c";
		$this->password="xPm7tN6BhxsA";
    }

    public function subsector_keywords()
    {
		$subsectors=$this->ar_subsector_model->fetchRecord();
		$insertBatch=array();
		foreach($subsectors as $value)
		{
			$isExistsKeywords=$this->relevant_keys_model->fetchRecord(array('subsector'=>$value['subsector_name']));
			if(!$isExistsKeywords)
			{
				$str="";
				/*******************Call Watson and get Keywords of current subsector***************/
				$subsector = trim($value['subsector_name']);	
				$explode=explode('%26',rawurlencode(str_replace(' ','_',$subsector)));
				foreach($explode as $valuexx)
				{
					if(trim($valuexx,'_'))
						$str.='\"/graphs/wikipedia/en-latest/concepts/'.trim($valuexx,'_').'\",';
				}
				if(!empty($str))
					$str=substr($str,0,-1);
				
				$api='curl -u '.$this->username.':'.$this->password.' -G -d "concepts=['.$str.']&level=1&limit=15" "https://gateway.watsonplatform.net/concept-insights/api/v2/graphs/wikipedia/en-latest/related_concepts"';
				//echo $api;die;
				$result = exec($api);
				$keywords = json_decode($result,true);
				/*******************End Call Watson and get Keywords of current subsector***************/
				
				/*******************Prepare Insert Array for relevant_keys table***************/
				if(!empty($keywords['concepts']))
				{
					foreach($keywords['concepts'] as $valuex)
					{
						$data=array(
							'subsector'=>$value['subsector_name'],
							'subsector_id'=>$value['subsector_code'],
							'label'=>$valuex['concept']['label'],
							'concept_id'=>$valuex['concept']['id'],
							'score'=>$valuex['score']
						);
						array_push($insertBatch,$data);
					}
				}
				/*******************End Prepare Insert Array for relevant_keys table***************/
			}
		}
		
		if($insertBatch)
			$this->relevant_keys_model->insert_batch($insertBatch);
		
		$keywords=$this->relevant_keys_model->fetchRecord(array(),array('subsector','ASC'));
		
		/*******************Build CSV Data Array*************/
        $dataXLS=array();
        array_push($dataXLS,array('Subsector Code','Subsector','Keywords','Score','Concept ID'));
		foreach($keywords as $value)
		{
			$data=array(
				$value['subsector_id'],
				$value['subsector'],
				$value['label'],
				$value['score'],
				$value['concept_id'],
			);
			array_push($dataXLS,$data);
		}
		if(count($dataXLS)>1)
		{
		  try {
				/*******************Build CSV file and Download*************/
				$this->array_to_csv_download($dataXLS,(int)time().'.csv');
			 } catch (Exception $e) {
				 echo $e->getMessage();
				 exit;
			 }
		}
    }
	public function export_subsectors()
	{
		$dataXLS=array();
		array_push($dataXLS,array('Subsector Code','Splited Subsector','Main Subsector'));
		$subsectors=$this->ar_subsector_model->fetchRecord();
		/*******************Build CSV Data Array*************/
		foreach($subsectors as $value)
		{
			$subsector = trim($value['subsector_name']);	
			$explode=explode('%26',rawurlencode(str_replace(' ','_',$subsector)));
			foreach($explode as $valuexx)
			{
				if(trim($valuexx,'_'))
				{
					$data=array(
						$value['subsector_code'],
						trim($valuexx,'_'),
						$value['subsector_name'],
					);
					array_push($dataXLS,$data);
				}	
			}
		}
		if(count($dataXLS)>1)
		{
		  try {
				/*******************Build CSV file and Download*************/
				$this->array_to_csv_download($dataXLS,(int)time().'.csv');
			 } catch (Exception $e) {
				 echo $e->getMessage();
				 exit;
			 }
		}
	}
	
	public function description_concepts()
	{
		$dataXLS=array();
        array_push($dataXLS,array('User','Description','Concept','Score'));
		$descriptions=$this->ar_request_access_page_model->fetchRecord(array('bussinessdesc <>'=>''));
		$list=$this->getDocuments();
		foreach($descriptions as $value)
		{
			$key=array_search("user_".$value['mid'],$list['documents']);
			if($key)
			{
				$dataXLS=$this->getConcepts("user_".$value['mid'],$value['mid'],$value['bussinessdesc'],$dataXLS);
			}
			else
			{
				$data=array(
					'name'=>$value['bussinessname'],
					'description'=>$value['bussinessdesc']
				);
				
				$this->createDocuments('user_'.$value['mid'],$data);
				$dataXLS=$this->getConcepts("user_".$value['mid'],$value['mid'],$value['bussinessdesc'],$dataXLS);
			}
		}
		if(count($dataXLS)>1)
		{
		  try {
				/*******************Build CSV file and Download*************/
				$this->array_to_csv_download($dataXLS,(int)time().'.csv');
			 } catch (Exception $e) {
				 echo $e->getMessage();
				 exit;
			 }
		}
		die;
	}
	
	public function description_concepts_db()
	{
		$dataXLS=array();
        array_push($dataXLS,array('User','Description','Concept','Score'));
		$descriptions=$this->ar_request_access_page_model->fetchRecord(array('bussinessdesc <>'=>''));
		$list=$this->getDocuments();
		foreach($descriptions as $value)
		{
			$key=array_search("user_".$value['mid'],$list['documents']);
			if($key)
			{
				$dataXLS=$this->getConcepts("user_".$value['mid'],$value['mid'],$value['bussinessdesc'],$dataXLS);
			}
			else
			{
				$data=array(
					'name'=>$value['bussinessname'],
					'description'=>$value['bussinessdesc']
				);
				
				$this->createDocuments('user_'.$value['mid'],$data);
				$dataXLS=$this->getConcepts("user_".$value['mid'],$value['mid'],$value['bussinessdesc'],$dataXLS);
			}
		}
		if(count($dataXLS)>1)
		{
			$time=(int)time();
			$tempInsert=array();
			$this->db->close();
			$this->db->initialize(); 
			$this->load->model('system_models/user_description_keywords_model');
			foreach($dataXLS as $key=>$value)
			{
				if($key==0)
					continue;
				if(!is_numeric(str_replace('user_','',$value[0])) || !$value[2])
					continue;
				if(empty($tempInsert[str_replace('user_','',$value[0])]))
				{
					$tempInsert[str_replace('user_','',$value[0])]=array();
					$this->user_description_keywords_model->delete(array('user_id'=>str_replace('user_','',$value[0])));
				}
				array_push($tempInsert[str_replace('user_','',$value[0])],array(
					'user_id'=>str_replace('user_','',$value[0]),
					'keyword'=>$value[2],
					'score'=>$value[3],
					'date_of_creation'=>$time,
				));
			}
			foreach($tempInsert as $key=>$value)
			{
				$this->user_description_keywords_model->insert_batch($value);
			}
			
			try {
				/*******************Build CSV file and Download*************/
				$this->array_to_csv_download($dataXLS,$time.'.csv');
			} catch (Exception $e) {
				 echo $e->getMessage();
				 exit;
			}
		}
		die;
	}
	
	private function getConcepts($documentName="",$mid,$description,$dataXLS=array())
	{
		if(!$documentName)
			return $dataXLS;
		$apix='curl -u '.$this->username.':'.$this->password.' -G -d "level=1" "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/ed0tiliprory9/arlians/documents/'.$documentName.'/related_concepts"';
		$resultx = exec($apix);
		$resx = json_decode($resultx,true);
		if(!empty($resx['concepts']))
		{
			foreach($resx['concepts'] as $key=>$value)
			{
				if($value['concept']['label'])
				{
					if($this->hasSpeclChar($value['concept']['label']) || $value['score']<0.89)
						continue;
					if($key==0)
						array_push($dataXLS,array($mid,$description,$value['concept']['label'],$value['score']));
					else
						array_push($dataXLS,array($mid,'',$value['concept']['label'],$value['score']));
				}
			}
		}
		return $dataXLS;
	}
	
	private function hasSpeclChar($string)
	{
		if(preg_match('/[^a-zA-Z\-_ ]/', $string))
			return true;
		else
			return false;
	}
	
	private function createDocuments($documentName="",$dataSet=array())
	{
		$content_type="text/plain";
		$documentName=strtolower(str_replace(' ','',$documentName));
		if(strstr($dataSet['description'],'<'))
			$content_type="text/html";
		$data=array(
			'label'=>$documentName,
			'parts'=>array(
				array(
					'name'=>$dataSet['name'],
					'content-type'=>$content_type,
					'data'=>$dataSet['description']
				),
			)
		);
		$api='curl -X PUT -u '.$this->username.':'.$this->password.' -d \''.json_encode($data).'\' "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/ed0tiliprory9/arlians/documents/'.$documentName.'"';
		$result = exec($api);
		$res = json_decode($result,true);
		return $res;
	}
	private function getDocuments()
	{
		$api='curl -u '.$this->username.':'.$this->password.' "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/ed0tiliprory9/arlians/documents"';
		$result = exec($api);
		$res = json_decode($result,true);
		
		foreach($res['documents'] as $key=>$value)
		{
			$res['documents'][$key]=str_replace('/corpora/ed0tiliprory9/arlians/documents/','',$value);
		}
		return $res;
	}
	
	private function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
       // open raw memory as file so no temp files needed, you might run out of memory though
       $f = fopen('php://memory', 'w'); 
       // loop over the input array
       foreach ($array as $line) { 
           // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter); 
       }
       // reset the file pointer to the start of the file
       fseek($f, 0);
       // tell the browser it's going to be a csv file
       header('Content-Type: application/csv');
       // tell the browser we want to save it instead of displaying it
       header('Content-Disposition: attachment; filename="'.$filename.'";');
       // make php send the generated csv lines to the browser
       fpassthru($f);
       die;
   }
}
