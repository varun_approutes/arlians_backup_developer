<?php 

	/*
	 * sample example code for BingSearch.php class
	 * @author Daniel Boorn info@rapiddigitalllc.com
	 * @license apache 2.0
	 * @bingapiurl https://datamarket.azure.com/dataset/bing/search#schema
	 */


	ini_set('display_errors','1');
	require('BingSearch.php');
	
	//register for key on windows azure
	$apiKey = 'PoIxFBdtOcp41KuAEFEzeOKOAWV29OfibqQb7cKjndk';
	
	$bing = new BingSearch($apiKey);
	
	# Example 1: simple image search
	/*
	$r = $bing->queryImage('xbox');
	var_dump($r);
	*/
	
	# Example 2: advanced image search
	//https://datamarket.azure.com/dataset/bing/search#schema
	//be sure to respect the data types in the values
	$str = $_REQUEST['str'];
	$r = $bing->query('News',array(
		'Query'=>"'".$str."'",//string
		'Adult'=>"'Moderate'",//string
		//'ImageFilters'=>"'Size:Small+Aspect:Square'",//string
		//'Latitude'=>47.603450,//double
		//'Longitude'=>-122.329696,//double
		//'Market'=>"'en-US'",//string
		'NewsCategory'=>"'rt_Business'",//string
		'Options'=>"'EnableHighlighting'",//string
		'NewsSortBy'=>"'Relevance'",//string
		
	));
	echo '<pre>';
	var_dump($r);
	
	
	//repeat examples for other search types...