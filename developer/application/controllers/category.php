<?php

class Category extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('model_common');
        $this->load->model('model_member');
        $this->load->model('model_category');
        $this->load->helper('common_helper');
        $this->model_common->SiteSettingsDetails(); // This is mandatory
    }

    function index() { 
        
    }

    function autocomplete() {
        $q = $_GET['q'];
        $my_data = mysql_real_escape_string($q);
        $data = $this->model_member->auto($my_data);
        return $data;
    }

    function get_top_list() {
        //echo "llll"; exit;
        $data['top_list'] = $this->model_category->top_link_list();
        //print_r($data); exit;
        echo json_encode($data);
        exit;
    }

    function get_sector_list() {
        $top_link_id = $this->input->post('top_link_id');
        //$top_link_id=10001;
        if ($top_link_id != null) {
            $data['sector_list'] = $this->model_category->ar_sector_list($top_link_id);
        } else {
            $data['sector_list'] = $this->model_category->ar_sector_list();
        }
        echo json_encode($data);
        exit;
    }

    function get_subsector_list() {
        $sector_id = $this->input->post('sector_id');
        $top_link_list_id = $this->input->post('top_link_list_id');
        if ($sector_id != null) {
            $data['subsector_list'] = $this->model_category->ar_subsector_list($sector_id, $top_link_list_id);
        } else {
            $data['subsector_list'] = $this->model_category->ar_subsector_list();
        }
        echo json_encode($data);
        exit;
    }

    function get_bussiness_list() {
        $subsector_id = $this->input->post('subsector_id');
        $sector_id = $this->input->post('sector_id');
        $top_link_list_id = $this->input->post('top_link_list_id');
        if ($subsector_id != null) {
            $data['bussiness_list'] = $this->model_category->bussiness_type_list($subsector_id, $sector_id, $top_link_list_id);
        } else {
            $data['bussiness_list'] = $this->model_category->bussiness_type_list();
        }
        echo json_encode($data);
        exit;
    }
	function get_all_bussiness_list() {
        
        $data['bussiness_list'] = $this->model_category->bussiness_type_list();
      
        echo json_encode($data);
        exit;
    }

    function setup_wiz_one_part_two() {
        $d = array();
        $d['mid'] = $this->session->userdata['logged_in']['id'];
        $d['ans_set_cat_id'] = $this->input->post('top_link_id') . '-' . $this->input->post('sector_id') . '-' . $this->input->post('subsector_id') . '-' . $this->input->post('bussiness_id');
        $d['mem_code'] = $this->input->post('top_link_id') . $this->input->post('sector_id') . $this->input->post('subsector_id') . $this->input->post('bussiness_id');

        if ($d['mid'] != '') {
            $result = $this->model_category->insert_wiz_one_p_two($d);
            echo $result;
            /* if($result=='inactive'){
              $this->user_manager->remove_user_token();
              $this->session->sess_destroy();
              redirect('member');
              } */
        } else {
            echo 'not inserted';
        }
        exit;
    }

    function get_looking_for_list() {
        $data['looking_for'] = $this->model_category->looking_for();
        echo json_encode($data);
        exit;
    }

    function get_locality_list() {
        $data['locality'] = $this->model_category->locality();
        echo json_encode($data);
        exit;
    }

    function get_is_service_list() {
        $data['is_service'] = $this->model_category->is_service();
        echo json_encode($data);
        exit;
    }

    function setup_wiz_two() 
    {
        $type = $this->input->post('type');
        $customertype = $this->input->post('type');
        $wiz_two_looking_industry = array();
        //------------------------------------------------------------------
        $data['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data);
        $user_id = $result->mid;
        //------------------------------------------------------------------
        $wiz_two_looking_industry['mid'] = $user_id;
        if ($type != 'insert_new_customer_type') 
		{           
            $wiz_two_looking_industry['industry_code'] = $this->input->post('industry_you_serve');
            $wiz_two_looking_industry['target_sector_code'] = $this->input->post('target_sector');
            $result2 = $this->model_category->wiz_two_looking_industry($wiz_two_looking_industry);            
            if($result2)
                {
                  $arr = array('response' => 'success');  
                }
            else
                {
                   $arr = array('response' => 'fail');   
                }            
            echo json_encode($arr);
        } 
	else
	{
            $looking_for = '';
            foreach (json_decode($this->input->post('looking_for_biz')) as $looking_for_arr) {
                $looking_for .=$looking_for_arr->looking_for_id . ',';
            }

          /*  $locality = '';
            foreach (json_decode($this->input->post('locality_to_explore')) as $locality_arr) {
                $locality .=$locality_arr->locality_id . ',';
            }
			*/
			$locality ='4,';

              $is_service = $this->input->post('services').',';
            /*foreach (json_decode($this->input->post('services')) as $is_service_arr) {
                $is_service .=$is_service_arr->service_id . ',';
            }*/
            if (count(json_decode($this->input->post('looking_service'))>0)) {
                $looking_service = '';
                foreach (json_decode($this->input->post('looking_service')) as $looking_service_arr) {
                    $looking_service .=$looking_service_arr->look_service_id . ',';
                }
            }
            //$ist_time=$this->input->post('first_time');


            $d = array();
            $d['mid'] = $user_id;
            $d['looking_for'] = $looking_for;
            $d['locality'] = $locality;
            $d['is_service'] = $is_service;
            $wiz_two_looking_industry['industry_code'] = $this->input->post('industry_you_serve');
            $wiz_two_looking_industry['target_sector_code'] = $this->input->post('target_sector');
             if (count(json_decode($this->input->post('looking_service'))>0)) {
                $d['looking_service'] = $looking_service;
            } else {

                $d['looking_service'] = '';
            }

            $result1 = $this->model_category->insert_wiz_two($d);
            $result2 = $this->model_category->wiz_two_looking_industry($wiz_two_looking_industry);
            $arr = array();

            if($result1)
            {
              $arr = array('response' => 'success');  
            }
            else
            {
               $arr = array('response' => 'fail');   
            }
            
            echo json_encode($arr);
        }
        exit;
    }
	
	    function setup_wiz_two_api() 
    {
        $type = $this->input->post('type');
        $customertype = $this->input->post('type');
        $wiz_two_looking_industry = array();
        //------------------------------------------------------------------
        $data['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data);
        $user_id = $result->mid;
        //------------------------------------------------------------------
        $wiz_two_looking_industry['mid'] = $user_id;
        if ($type != 'insert_new_customer_type') 
		{           
            $target_sector = $this->input->post('target_sector');
			$target_sector = json_decode($target_sector);
			$result2;
			$flag = array();
			
			foreach($target_sector as $row)
			{
				$wiz_two_looking_industry['industry_code'] = $row->industry_code;
				$wiz_two_looking_industry['target_sector_code'] = $row->tar_sector_code;
				$fl = $this->model_category->isThisCodeAlredyexistinwiztwo($wiz_two_looking_industry);
				
				
				if($fl == 'false')
				{
					$result2 = $this->model_category->wiz_two_looking_industry($wiz_two_looking_industry);
				}
				else
				{
					$flag['industry_code'] = $row->industry_code;
					$flag['tar_sector_code'] = $row->tar_sector_code;
					$flag['status'] = 'duplicate';
				}
				
			}
                        
            if($result2)
                {
                  $arr = array('response' => 'success','status' => $flag);  
                }
            else
                {
                   $arr = array('response' => 'fail','status' => $flag);   
                }            
            echo json_encode($arr);
        } 
	else
	{
            $looking_for = '';
            foreach (json_decode($this->input->post('looking_for_biz')) as $looking_for_arr) {
                $looking_for .=$looking_for_arr->looking_for_id . ',';
            }

          /*  $locality = '';
            foreach (json_decode($this->input->post('locality_to_explore')) as $locality_arr) {
                $locality .=$locality_arr->locality_id . ',';
            }
			*/
			$locality ='4,';

              $is_service = $this->input->post('services').',';
            /*foreach (json_decode($this->input->post('services')) as $is_service_arr) {
                $is_service .=$is_service_arr->service_id . ',';
            }*/
            if (count(json_decode($this->input->post('looking_service'))>0)) {
                $looking_service = '';
                foreach (json_decode($this->input->post('looking_service')) as $looking_service_arr) {
                    $looking_service .=$looking_service_arr->look_service_id . ',';
                }
            }
            //$ist_time=$this->input->post('first_time');


            $d = array();
            $d['mid'] = $user_id;
            $d['looking_for'] = $looking_for;
            $d['locality'] = $locality;
            $d['is_service'] = $is_service;
            $wiz_two_looking_industry['industry_code'] = $this->input->post('industry_you_serve');
            $wiz_two_looking_industry['target_sector_code'] = $this->input->post('target_sector');
             if (count(json_decode($this->input->post('looking_service'))>0)) {
                $d['looking_service'] = $looking_service;
            } else {

                $d['looking_service'] = '';
            }

  
          $result1 = $this->model_category->insert_wiz_two($d);
		  
			$target_sector = $this->input->post('target_sector');
			$target_sector = json_decode($target_sector);
			$result2;
			
			foreach($target_sector as $row)
			{
				$wiz_two_looking_industry['industry_code'] = $row->industry_code;
				$wiz_two_looking_industry['target_sector_code'] = $row->tar_sector_code;
				$result2 = $this->model_category->wiz_two_looking_industry($wiz_two_looking_industry);
			}
            //$result2 = $this->model_category->wiz_two_looking_industry($wiz_two_looking_industry);
            $arr = array();

            if($result1)
            {
              $arr = array('response' => 'success');  
            }
            else
            {
               $arr = array('response' => 'fail');   
            }
            
            echo json_encode($arr);
        }
        exit;
    }

    function get_industry_list() {
        $data['industry_list'] = $this->model_category->industry_list();
        echo json_encode($data);
        exit;
    }

    function get_target_sector_list() {
        $selected_industry_code = $this->input->post('selected_industry_code');
        if ($selected_industry_code != null) {
            $data['target_sector_list'] = $this->model_category->target_sector_list($selected_industry_code);
        } else {
            $data['target_sector_list'] = $this->model_category->target_sector_list();
        }
        echo json_encode($data);
        exit;
    }
	
	function get_target_sector_list_api() 
	{
        $selected_industry_code = $this->input->post('selected_industry_code');
		//var_dump( $selected_industry_code);exit;
		$selected_industry_code = json_decode($selected_industry_code);
		$arr = array();
		 
		foreach($selected_industry_code as $key=>$row)
		{
			$i_code = $row->industry_code;
			$data = $this->model_category->fetchTargetSectorByIndustryCode($i_code);
			$arr[$key]['industry_code'] = $i_code;
			$arr[$key]['industry_name'] = $row->industry_name;
			$arr[$key]['target_data'] = $data;
		}
		//var_dump($data); die();
		echo json_encode($arr);
		exit;
	
    }

    function looking_services_list() {
        //$selected_industry_code=$this->input->post('selected_industry_code');

        $data['target_sector_list'] = $this->model_category->looking_services_list();

        echo json_encode($data);
        exit;
    }

    function getUserOwnIndusNo() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $result = $this->model_category->getUserOwninducstryCodes($data['mid']);
        echo $result;
        exit;
    }

    function getUserOwninducstryCodes() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $result = $this->model_category->getUserOwnIndusNo($data['mid']);
        echo $result;
        exit;
    }

    function getUserOwninducstryCodesMatched() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $top_line_code = $this->input->post('top_link_id');
        $result = $this->model_category->getUserOwninducstryCodesMatched($data['mid'], $top_line_code);
        echo $result;
        exit;
    }

    function getUserTergetinducstryCodes() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $result = $this->model_category->getUserTergetinducstryCodes($data['mid']);
        echo $result;
        exit;
    }

    function getUserTergetIndusNo() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $result = $this->model_category->getUserTergetIndusNo($data['mid']);
        echo $result;
        exit;
    }

    function getUserTergetinducstryCodesMatch() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        //$data['mid']= 50;
        $target_industry_code = $this->input->post('target_industry_code');
        $result = $this->model_category->getUserTergetinducstryCodesMatch($data['mid'], $target_industry_code);
        echo $result;
        exit;
    }

    function readCountry() {
        $keyword = $this->input->post('keyword');
        $result = $this->model_category->readCountry($keyword);
        echo json_encode($result);
        exit;
    }

    function isThisCodeAlredyexistinwiztwo() 
	{
        $data['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data);
        $user_id = $result->mid;
        $data['mid'] = $user_id;
        $code1 = json_decode($this->input->post('code1'));
        $code2 = json_decode($this->input->post('code2'));

		$res = array();
		$i = 0;
		foreach($code2 as $cd)
		{
			$data['code1'] = $cd->industry_code;
			$data['code2'] = $cd->tar_sector_code;
			$result = $this->model_category->isThisCodeAlredyexistinwiztwo($data);
			
			$res[$i]['industry_code'] = $cd->industry_code;
			$res[$i]['tar_sector_code'] = $cd->tar_sector_code;
			$res[$i]['status'] = $result;
			$i++;
		}
        echo json_encode($res);
        exit;
    }
	function  isCodeExist(){
		$data['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data);
        $user_id = $result->mid;
        $result = $this->model_category->isCodeExist($user_id);
        echo json_encode($result);
        exit;
	}

    function wizTwoservice() {
        $field_name = $this->input->post('field_name');
        $mid = $this->session->userdata['logged_in']['id'];
        $result = $this->model_category->wizTwoservice($mid, $field_name);
        echo json_encode($result);
        exit;
    }

    function isThisCodeAlredyexistinwizone() {
        $top_link_id = $this->input->post('top_link_id');
        $mid = $this->session->userdata['logged_in']['id'];
        $result = $this->model_category->isThisCodeAlredyexistinwizone($top_link_id, $mid);
        echo $result;
        exit;
    }

    function subsectorlistToplineWise() {
        $arr = $this->input->post('selected_buisness_top_lines');
        $arrTopLine = array();

        foreach ($arr as $key => $value) {
            $arrTopLine[$key]['top_code'] = $value['top_code'];
            $arrTopLine[$key]['top_name'] = $value['top_name'];
        }

        $res = array();
        $i = 0;
        // print_r($arrTopLine); exit;
        foreach ($arrTopLine as $artop) {

            $data = $this->model_category->getSubCategoryCodeFromToplineCode($artop['top_code']);
            $top_code = $res[$i]['toplinecode'] = $artop['top_code'];
            $top_name = $res[$i]['toplinename'] = $artop['top_name'];

            $arr = array();
            foreach ($data as $row) {
                $arr[] = $row['subsector_code'];
            }
            //$res[$i]['subsector_cod'] = $arr;
            //echo '<pre>'; var_dump($res); die();
            $arr_res = array();
            foreach ($data as $key => $row) {
                $nam = sub_sector_name($row['subsector_code']);
                $arr_res[] = $nam['subsector_name'];
            }
            for ($j = 0; $j < count($arr); $j++) {
                $res[$i]['subsector'][$j]['code'] = $arr[$j];
                $res[$i]['subsector'][$j]['name'] = $arr_res[$j];
                $res[$i]['subsector'][$j]['top_code'] = $top_code;
                $res[$i]['subsector'][$j]['top_name'] = $top_name;
            }
            //$res[$i]['subsector_name'] = $arr_res;
            $i++;
        }
        // echo '<pre>'; var_dump($res); exit;
        echo json_encode($res);
        exit;
    }   
	function subsectorlistToplineWiseObj() {
        $arr = $this->input->post('selected_buisness_top_lines');
		$arr = json_decode($arr);
        $arrTopLine = array();
		
        foreach ($arr as $key => $value) {
            $arrTopLine[$key]['top_code'] = $value->top_code;
            $arrTopLine[$key]['top_name'] = $value->top_name;
        }

        $res = array();
        $i = 0;
        foreach ($arrTopLine as $artop) {

            $data = $this->model_category->getSubCategoryCodeFromToplineCode($artop['top_code']);
            $top_code = $res[$i]['toplinecode'] = $artop['top_code'];
            $top_name = $res[$i]['toplinename'] = $artop['top_name'];

            $arr = array();
            foreach ($data as $row) {
                $arr[] = $row['subsector_code'];
            }
            $arr_res = array();
            foreach ($data as $key => $row) {
                $nam = sub_sector_name($row['subsector_code']);
                $arr_res[] = $nam['subsector_name'];
            }
            for ($j = 0; $j < count($arr); $j++) {
                $res[$i]['subsector'][$j]['code'] = $arr[$j];
                $res[$i]['subsector'][$j]['name'] = $arr_res[$j];
                $res[$i]['subsector'][$j]['top_code'] = $top_code;
                $res[$i]['subsector'][$j]['top_name'] = $top_name;
            }
            $i++;
        }
        echo json_encode($res);
        exit;
    }

    function subsectorlistToplineWiseApi() {
        $arr = $this->input->post('selected_buisness_top_lines');
        $arrTopLine = array();

        foreach ($arr as $key => $value) {
            $arrTopLine[] = $value['top_code'];
        }

        $res = array();

        foreach ($arrTopLine as $artop) {

            $data = $this->model_category->getSubCategoryCodeFromToplineCode($artop);
            $res[$artop] = $data;
            foreach ($data as $key => $row) {
                $nam = sub_sector_name($row['subsector_code']);
                $res[$artop][$key]['subsector_name'] = $nam['subsector_name'];
            }
        }
        //echo '<pre>'; var_dump($res); exit;
        echo json_encode($res);
        exit;
    }

    function sendCombinationCode() {
        //$user_email = $_POST['user_email'];
        //$user_email = $_REQUEST['email'];
        //echo '<pre>'; var_dump($_POST); die();
        $data['user_email'] = $this->input->post('user_email');
        //$data['user_email'] = 'rampin.lorenzo+16@gmail.com';
        $result = $this->model_member->getIdByUserEmail($data);

        $user_id = $result->mid;
        $user_bussiness = $_POST['selected_user_business'];
        $data = $_POST['selected_user_toplines_and_sub_sectors'];
        $user_unique_buisness = $_POST['user_unique_buisness'];

        $fifteen_digit_code = array();
        foreach ($data as $res) {
            $fifteen_digit_code[] = $this->model_category->convertTenDigitToFifteenDigitCode($res['top_code'], $res['code'], $data);
        }

        $twenty_digit_code = array();
        foreach ($fifteen_digit_code as $cd) {
            $twenty_digit_code[$cd[0]] = fifteenToTwentyConversion($cd[0]);
        }

        $re = array();
        foreach ($twenty_digit_code as $tw_code) {
            $d = array();
            $d['mid'] = $user_id;
            $arr = str_split($tw_code);
            $d['ans_set_cat_id'] = $arr[0] . $arr[1] . $arr[2] . $arr[3] . $arr[4] . '-' . $arr[5] . $arr[6] . $arr[7] . $arr[8] . $arr[9] . '-' . $arr[10] . $arr[11] . $arr[12] . $arr[13] . $arr[14] . '-' . $arr[15] . $arr[16] . $arr[17] . $arr[18] . $arr[19];
            $d['mem_code'] = $tw_code;
            //echo '<pre>'; var_dump($d);
            $re[] = $this->model_category->insert_wiz_one_p_two($d);
        }
        if(in_array('true', $re))
        {
            $this->model_category->add_bussiness_name($user_id,$user_unique_buisness);
            echo json_encode(array('success'=>'true'));
        }
        else
        {
            echo json_encode(array('success'=>'false'));
        }
        exit;
    }
	
	function sendCombinationCodeObj() {

        $data['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data);
        $user_id = $result->mid;
		
        $user_bussiness = json_decode($_POST['selected_user_business']);
		
			
		
        $data = json_decode($_POST['selected_user_toplines_and_sub_sectors']);
        $user_unique_buisness = $_POST['user_unique_buisness'];

        $fifteen_digit_code = array();
        foreach ($data as $res) {
            $fifteen_digit_code[] = $this->model_category->convertTenDigitToFifteenDigitCodeObj($res->top_code, $res->code, $user_bussiness);
        }

        $twenty_digit_code = array();
        foreach ($fifteen_digit_code as $cd) {
            $twenty_digit_code[$cd[0]] = fifteenToTwentyConversion($cd[0]);
        }

        $re = array();
		$duplicate = array();
        foreach ($twenty_digit_code as $tw_code) 
		{
            $d = array();
            $d['mid'] = $user_id;
            $arr = str_split($tw_code);
            $d['ans_set_cat_id'] = $arr[0] . $arr[1] . $arr[2] . $arr[3] . $arr[4] . '-' . $arr[5] . $arr[6] . $arr[7] . $arr[8] . $arr[9] . '-' . $arr[10] . $arr[11] . $arr[12] . $arr[13] . $arr[14] . '-' . $arr[15] . $arr[16] . $arr[17] . $arr[18] . $arr[19];
            $d['mem_code'] = $tw_code;
            //echo '<pre>'; var_dump($d);
			$fl = $this->model_category->chkTwentyDigitCode($d);
			if(!$fl)
			{
				$re[] = $this->model_category->insert_wiz_one_p_two($d);
			}
			else
			{
				$duplicate['tw_code'] = $d['mem_code'];
				$duplicate['status'] = 'Duplicate';
			}
            
        }
        if(in_array('true', $re))
        {
            $this->model_category->add_bussiness_name($user_id,$user_unique_buisness);
            echo json_encode(array('success'=>'true','duplicate'=>$duplicate));
        }
        else
        {
            echo json_encode(array('success'=>'false','duplicate'=>$duplicate));
        }
        exit;
    }

    function set_user_type()
    {
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        
        $user_id = $result->mid;

        $bType = $this->input->post('businesstype');

        if($bType == 'false')
        {
			$data['bussinessname'] = $this->input->post('businessname');
            $data['bussinesstype'] = 'Buisness';
			
        }
        else
        {
            $data['bussinessname'] = '';
            $data['bussinesstype'] = 'Yourself, as a self-employed professional';
        }

        $res = $this->model_category->setUserType($data,$user_id);

        $user['fname'] = $this->input->post('fname');
        $user['lname'] = $this->input->post('lname');

        $res1 = $this->model_member->updateUserName($user,$user_id);

        if($res == 1 && $res == 1)
        {
            $r['response'] = 'success';
        }
        else
        {
            $r['response'] = 'fail';
        }

        echo json_encode($r);
        exit;
    }

}
