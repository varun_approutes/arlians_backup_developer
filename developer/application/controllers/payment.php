<?php 
class Payment extends CI_Controller
{
    function __construct(){
		parent::__construct();
		$this->load->model('model_member');
		$this->load->model('model_suggestion');
		$this->load->library('User_Manager');
		$this->load->helper('common_helper');
		$this->load->helper('text');
		$this->load->model('model_hub');
		$this->load->helper('load_controller');
		$this->load->model('model_payment');
		//$this->model_common->SiteSettingsDetails(); // This is mandatory
		$this->user_manager = User_Manager::get_instance();
	}

	function paymentOption(){
		
		$this->load->view('payment/paymentOption');
	}

	function paymentSuccess(){

		$bussinessName = getBusinessNameById($data['mid']);
		$details = getParticularUserDetails($data['mid']);
		

		$mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Thank you for signing up to Arlians</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Thank you for signing up to Arlians.</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;"><br /><br />
                                             <a href="'.base_url().'">
                                                <img title="Arlians logo" src="'.base_url().'/assets/images/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Welcome to Arlians, '.$bussinessName[0]['bussinessname'].'<br />
                                            <br />
                                            <span style="font-size:14px;color:#4D575E;">Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you every step of the way.</span><br /><br />
                                            <span style="color:#00AAA0;"><strong>Spread the word</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">Remember, as the network grows, so do your opportunities. Make the most of Arlians by inviting your customers, suppliers and any other business you\'ve ever worked with.</span>
                                            <br /><br />
                                            <span style="color:#00AAA0"><strong>Be rewarded</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">
                                                You\'ll get the opportunity for exclusive free access to premium features when you recommend someone and they sign up to Arlians. All you have to do to take advantage of this is share your unique code.</span>
                                            <br /><br /> 
                                        </td>
                                    </tr>       
                                </table>
                                
                                
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright © 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                           Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';
$mail = $details['name'];
$subject = 'Subscription mail';
$from = 'subscriptions@arlians.com';

$this->sentmail($mail,$mailBody,$subject,$from);

	$this->load->view('payment/paymentSuccess');
	}
	function paymentCancel(){
		$this->load->view('payment/paymentCancel');
	}
    
    function paymentoptiondetails(){  
        $this->load->view('payment/paymentoptiondetails');
    }
    
    function paynow(){
		$data['is_unsubscribe']=$this->uri->segment(3);
		var_dump($data['is_unsubscribe']);
        $this->load->view('payment/paynow',$data);
    }
    
    function payFromPaypal(){
        $this->load->view('payment/payFromPaypal');
    }
    function payFromFoundationMembership(){
        $this->load->view('payment/payFromFoundationMembership');
    }

	function noOfDaysSinceSignUp(){

		$mid = $this->session->userdata['logged_in']['id'];
		$signup_date = $this->model_payment->getUserSignUpDate($mid); 
		$sn_dt = $signup_date[0]['update_date'];

		$oldtimestamp = strtotime($sn_dt);
		$days = floor((time() - $oldtimestamp) / 86400); 
		return $days;
	}
    
    function cancelPayment(){
        $this->load->view('payment/cancelPayment');
    }
	


    function sentmail($mail,$mailBody,$subject,$from){

		$this->load->library('email');
		$this->email->from($from, 'Arlians');
		$this->email->to($mail);
		$this->email->set_mailtype('html');

		$this->email->subject($subject);

		$this->email->message($mailBody);

		$this->email->send();
		return true;

	}
	
}