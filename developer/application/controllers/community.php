<?php

class Community extends CI_Controller {

    function __construct() {

        parent::__construct();

        $this->load->model('model_community'); 
        $this->load->model('model_memberprofile');  
        $this->load->model('model_member');  		

    }
    
    //Add community
    public function add_community(){
		 //var_dump($_POST);die;
        $data = array();
        $data['com_id']     = $this->model_community->add_community();
        $data['responce']   = "success";
	
        echo json_encode ($data);
		exit;

    }
	
	//bandhu
    public function delete_community(){
		//$id = $com_id ;
		$d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;
		$id = $this->input->post('com_id');
		//echo $user_id ;
		//echo $id ;
		//die ;
        $dat = $this->model_community->delete_community($id,$user_id);
        if($dat == '1'){
          $data['responce']   = "success";  
        }
         if($dat == '0'){
          $data['responce']   = "ERROR";  
        }
        echo json_encode ($data);
  exit;
    }

public function perticepant(){
        $data = array();
        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;
             $id = $this->input->post('com_id');
            
          $data['participate_stattus']   = $this->model_community->participate($id,$user_id);
          $data['participate_count']   = $this->model_community->participatecount($id,$user_id);

             echo json_encode ($data);
  exit;
  
  

    }
    //bandhu
	
	
    
    //Add community comment
    public function add_community_comment(){
         
		
		
        $data = array();
        $data = $this->model_community->add_community_comment();
		
	        
        echo json_encode ($data);
			exit;
    }

    //Add community like
 function add_community_like(){
	 
		 $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;
		$com_id=$this->input->post('com_id');
        $data['com_id'] 	 = $com_id;			
		$data['mid']	 	 = $user_id;
        $data['like_status']   = 1;	
		//bandhu
        $status =  $this->model_community->add_community_like($data,$data['com_id'],$data['mid']);
        if($status == '1'){
			
            $data['msg'] = "Your like has  been placed successfully.";  
        }
        else{
            $data['msg'] = "Your like has not been placed successfully.";  
        }
        

        echo json_encode ($data); 
		exit;	
    }

    //Get community details
public function get_community_details(){

        $data = array();
        $data['result'] = $this->model_community->get_community_details();   
        // abhijit
        $details  = array();
        $temp  = array();
        foreach ($data['result'] as $key=>$value) {
            $data['result'][$key]['logomembername']       = strtoupper(logomembername($value['mid']));
            $img_url                                      = $this->model_community->memberlogo($value['mid']);
            if($img_url != ''){
                $data['result'][$key]['logomembername_url']   = $img_url->image_url;
            }
            
            
        }
        //$data['bussiness_name_initinal'] = $details;  
        // abhijit           
        //echo '<pre>'; print_r($data); die();
        echo json_encode ($data); 
        exit;           
    }


    public function suggestTag()
    {
        $str = $this->input->post('str');
        $result = $this->model_community->getSuggestedTag($str);
        echo json_encode ($result); 
        exit; 
    }

    public function fetchLatestTalk()
    {
        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

        $code = getParticularUserDetails($user_id);

        $myTopline = array();

        //$access = $this->input->post('talk_access');
            $final_tag=array();
           /*  foreach($code['wiz_one_ans'] as $cd)
            {
                $c = explode('-',$cd['ans_set_cat_id']);
                $myTopline[] =  $c[0];
            }
			$myTopline = array_unique($myTopline);
            $myTopline = array_values(array_filter($myTopline));
            $tags = array();

            foreach($myTopline as $topline)
            {
               $tags[$topline] = $this->model_community->getTagsFromTopline($topline); 
            }

            $final_tag = array();

            foreach($tags as $row)
            {
                foreach($row as $r)
                {
                   $final_tag[] = $r['tag1_id']; 
                }
                
            }

            $final_tag = array_unique($final_tag); */
			//print_r($final_tag); die();
            $own_com_ids = $this->model_community->fetchComId($user_id);
            $latest_talk = $this->model_community->fetchLatestTalk($final_tag,$own_com_ids);
			
			//print_r($latest_talk);die();

            foreach($latest_talk as $key=>$row)
            {
				//print_r($row);die();
                $latest_talk[$key]['bussiness_name_initinal']       = strtoupper(logomembername($row['mid']));
                $latest_talk[$key]['bussiness_name']                = getMemberbusinName($row['mid']);
                $latest_talk[$key]['member_name']    	            = getMemberName($row['mid']);
                $latest_talk[$key]['profile_image']                 = getProfileImage($row['mid']);
                $latest_talk[$key]['image_basepath']                = base_url().'uploads/profile_image/';  
                //$latest_talk[$key]['img_url']    		           = base_url().'uploads/profile_image/'.getProfileImage($row['mid']);  
                $latest_talk[$key]['open_comment_box']              = 'false';
                $latest_talk[$key]['show_participate']              = 'false';
				$latest_talk[$key]['show_sub_comment'] 	            =  false;
				$latest_talk[$key]['show_post_button']              = 'false';
				$latest_talk[$key]['show_sub_post_button']          = 'false';
				$latest_talk[$key]['show_latest_talk_section']      = 'false';
				$latest_talk[$key]['com_like'] 			            = $this->model_community->checkLike($row['com_id'],$user_id);
				$latest_talk[$key]['participate_stattus']           = $this->model_community->participate($row['com_id'],$user_id);
				$latest_talk[$key]['participate_user_count']           = $this->model_community->participatedusercount($row['com_id']);
				$latest_talk[$key]['comments']   		            = $this->model_community->get_community_comments($row['com_id']);
				$latest_talk[$key]['my_talk']                       = $this->model_community->get_community_comments($row['com_id']);
                $latest_talk[$key]['logomembername']                = strtoupper(logomembername($row['mid']));
                $img_u;
                if($this->model_community->memberlogo($row['mid']) != NULL) 
                {
                    $in = $this->model_community->memberlogo($row['mid']);
                    $img_u = base_url().'uploads/profile_image/'.$in['image_url'];
                }
                else
                {
                   $img_u = 'NULL'; 
                }
                $latest_talk[$key]['logomembername_url']   = $img_u;
				$latest_talk[$key]['talk_access']   = $row['talk_access'];
				$latest_talk[$key]['like_comid_status']   = $this->model_community->get_community_like_statuslal($row['com_id'],$user_id);
				$latest_talk[$key]['currentsessioninitial']   = strtoupper(logomembername($user_id));
				$currentsessionimg_u;
                if($this->model_community->memberlogo($user_id) != NULL) 
                {
                    $in = $this->model_community->memberlogo($user_id);
                    $currentsessionimg_u = base_url().'uploads/profile_image/'.$in['image_url'];
                }
                else
                {
                   $currentsessionimg_u = 'NULL'; 
                }
				$latest_talk[$key]['currentsessionlogourl']   = $currentsessionimg_u;
				$latest_talk[$key]['access_confirmedusers']   = $this->model_community->get_community_accepted_request($row['com_id']);
				
				
				
            }

           // print_r($latest_talk); die();

            echo json_encode($latest_talk);
            exit();
    }
    public function topTalk()
    {
		$dxxx['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($dxxx);
        $user_id = $result->mid;
		
        $data['toptalk'] = $this->model_community->fetchTopTalks();
        // abhijit
        $details  = array();
        $temp  = array();
        $url = '';
        foreach ($data['toptalk'] as $key=>$value) {
			//print_r($value);die();
			$name	= getBusinessNameById($value['mid']);
			//print_r($name[0]['bussinessname']); 
			
			$data['toptalk'][$key]['logomembername']       	        = strtoupper(logomembername($value['mid']));
			$data['toptalk'][$key]['bussiness_name_initinal']       = strtoupper(logomembername($value['mid']));
			$data['toptalk'][$key]['bussiness_name']                = getMemberbusinName($value['mid']);
			$data['toptalk'][$key]['profile_image']                 = getProfileImage($value['mid']);
			$data['toptalk'][$key]['member_name']    	            = getMemberName($value['mid']);
		    $data['toptalk'][$key]['image_basepath']                = base_url().'uploads/profile_image/';  
			$data['toptalk'][$key]['open_comment_box']              = 'false';
			$data['toptalk'][$key]['show_participate']              = 'false';
			$data['toptalk'][$key]['show_sub_comment'] 	            =  false;
			$data['toptalk'][$key]['show_post_button']              = 'false';
			$data['toptalk'][$key]['show_sub_post_button']          = 'false';
			$data['toptalk'][$key]['show_top_talk_section']      = 'false';
			$data['toptalk'][$key]['com_like'] 			            = $this->model_community->checkLike($value['com_id'],$user_id);
			$data['toptalk'][$key]['comments']   		            = $this->model_community->get_community_comments($value['com_id']);
			$data['toptalk'][$key]['my_talk']                       = $this->model_community->get_community_comments($value['com_id']);
			$data['toptalk'][$key]['participate_stattus']   = $this->model_community->participate($value['com_id'],$user_id);
			$data['toptalk'][$key]['participate_user_count'] = $this->model_community->participatedusercount($value['com_id']);
			$data['toptalk'][$key]['logomembername_url']   	= $this->model_community->memberlogo($value['mid']);
			$data['toptalk'][$key]['fast_last_name']		= $name[0]['bussinessname']; 
			$data['toptalk'][$key]['like_comid_status']   = $this->model_community->get_community_like_statuslal($value['com_id'],$user_id);
			$img_u;
            if($this->model_community->memberlogo($value['mid']) != NULL)
            {
                $in = $this->model_community->memberlogo($value['mid']);
                $img_u = base_url().'uploads/profile_image/'.$in['image_url'];
            }
            else
            {
               $img_u = 'NULL'; 
            }
            $data['toptalk'][$key]['logomembername_url']   = $img_u;
			$data['toptalk'][$key]['talk_access']   = $value['talk_access'];
			
			$data['toptalk'][$key]['currentsessioninitial']   = strtoupper(logomembername($user_id));
			$currentsessionimg_u;
			if($this->model_community->memberlogo($user_id) != NULL) 
			{
				$in = $this->model_community->memberlogo($user_id);
				$currentsessionimg_u = base_url().'uploads/profile_image/'.$in['image_url'];
			}
			else
			{
			   $currentsessionimg_u = 'NULL'; 
			}
			$data['toptalk'][$key]['currentsessionlogourl']   = $currentsessionimg_u;
			$data['toptalk'][$key]['access_confirmedusers']   = $this->model_community->get_community_accepted_request($value['com_id']);
			
            /* $data['toptalk'][$key]['logomembername']       	= strtoupper(logomembername($value['mid']));
            $data['toptalk'][$key]['logomembername_url']   	= $this->model_community->memberlogo($value['mid']);
			$data['toptalk'][$key]['fast_last_name']		= $name[0]['bussinessname'];
			    $data['toptalk'][$key]['open_comment_box']              = 'false';
                $data['toptalk'][$key]['show_participate']              = 'false';
				$data['toptalk'][$key]['show_sub_comment'] 	            =  false;
				$data['toptalk'][$key]['show_post_button']              = 'false';
				$data['toptalk'][$key]['show_sub_post_button']          = 'false';
				$data['toptalk'][$key]['show_latest_talk_section']      = 'false';
            $img_u;
            if($this->model_community->memberlogo($value['mid']) != NULL)
            {
                $in = $this->model_community->memberlogo($value['mid']);
                $img_u = base_url().'uploads/profile_image/'.$in['image_url'];
            }
            else
            {
               $img_u = 'NULL'; 
            }
                $data['toptalk'][$key]['logomembername_url']   = $img_u; */
           // print_r($data);die();
        }
        //$data['bussiness_name_initinal'] = $details;
        // abhijit
		
       //echo "<pre>";print_r($data['toptalk']);die();
        echo json_encode($data);
        exit();
    }

	

    public function deleteComment()
    {
        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

        $data['mid'] = $user_id;
        $data['com_id'] = $this->input->post('com_id');

        $r = $this->model_community->deleteComment($data);
        if($r == true)
            $res['responce'] = 'success';
        else
            $res['responce'] = 'fail';

        echo json_encode($res);
        exit;
    }
	public function get_like_count()
	{
		$com_id = $this->input->post('com_id');
		$data['like_count'] = $this->model_community->get_like_count($com_id);
		$data['msg']='success';
		echo json_encode($data);
		exit;
	}

    public function dislike()
    {
        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;
     	$com_id 	 = $this->input->post('com_id');			
		$data['like_status']   = 0;	

        $r = $this->model_community->dislike($data,$user_id,$com_id);
        if($r == true)
            $res['responce'] = 'success';

        else
            $res['responce'] = 'fail';

        echo json_encode($res);
        exit;
    }

    public function request_access()
    {
        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

        $data['mid']            =   $user_id;
        $data['com_id']         =   $this->input->post('com_id');
        $data['talk_owner_id']  =   $this->input->post('talk_owner_id');
        $data['status']         =   '0';

        $res = $this->model_community->request_access($data);
        $r = array();

        if($res)
        {
          $r['responce'] = 'success';  
        }
        else
        {
            $r['responce'] = 'fail';
        }
        echo json_encode($r);
        exit;
    }

    public function allow_access()
    {
        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

        $com_id = $this->input->post('com_id');
        $mid    = $user_id;

        $res = $this->model_community->allow_access($com_id,$mid);
        $r = array();

        if($res)
        {
          $r['responce'] = 'success';  
        }
        else
        {
            $r['responce'] = 'fail';
        }
        echo json_encode($r);
        exit;

    }

    public function access_check()
    {

        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

        $data['mid']                = $user_id;
        $data['com_id']             = $this->input->post('com_id');  
        $data['talk_owner_id']      = $this->input->post('talk_owner_id'); 


        $res['status'] = $this->model_community->access_check($data);

            if($res['status'] == NULL)
            {
                $res['status'] = 'notApplied';
            }
        echo json_encode($res);
        exit;

    }

    public function insertSubComment()
    {
        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

        $data['mid']                = $user_id;
        $data['com_comm_id']        = $this->input->post('com_comm_id');
        $data['comment']            = $this->input->post('comment');  
//var_dump($data);
        $res = $this->model_community->insertSubComment($data);
        $r = array();

        if($res>0)
        {
          $r['responce'] = 'success';  
		   $r['sub_comment_id'] 		= $this->model_community->fetchsubcomentidmax($data['com_comm_id']);
		    $r['bussiness_name'] 		= getMemberName($data['mid']); 
			$r['profile_image'] 		= getProfileImage($data['mid']); 
			$r['com_comm_id']        	= $data['com_comm_id'];
			$r['currentsessioninitial']   = strtoupper(logomembername($data['mid']));		
			$currentsessionimg_u='';
			if($this->model_community->memberlogo($data['mid']) != NULL) 
			{
				$in = $this->model_community->memberlogo($data['mid']);
				$currentsessionimg_u = base_url().'uploads/profile_image/'.$in['image_url'];
			}
			else
			{
			   $currentsessionimg_u = 'NULL'; 
			}
			$r['currentsessionlogourl']   = $currentsessionimg_u;
			$r['bussiness_name_initinal']       = strtoupper(logomembername($data['mid']));
			$r['bussiness_name']                = getMemberName($data['mid']);
			$r['member_name']    	            = getMemberName($data['mid']);
			
			
			
			
        }
        else
        {
            $r['responce'] = 'fail';
        }
        echo json_encode($r);
        exit;

    }

    public function fetchSubComments()
    {
        $comm_id = $this->input->post('comm_id'); 

        $data['com_comm_id'] = $comm_id;
        $res = $this->model_community->fetchSubComments($data);

        $r = array();

        if($res)
        {
          $r['responce'] = 'success';  
        }
        else
        {
            $r['responce'] = 'fail';
        }
        echo json_encode($r);
        exit; 
    } 
	
	
	public function FnCommunityNotification()
	{
		$d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;
		$fetchnotification=$this->model_community->FnGetCommunityNotification($user_id);
		$fetchnotificationarray=array();
		$fetchnotificationarray['notificationcount']=0;
		if(count($fetchnotification)>0)
		{
			$fetchnotificationarray['notificationcount']=count($fetchnotification);
			foreach($fetchnotification as $key=>$notificationsinglearr)
			{
				$fetchnotificationarray['details'][$key]['bussiness_name_initinal']       = strtoupper(logomembername($notificationsinglearr['mid']));
                $fetchnotificationarray['details'][$key]['bussiness_name']                = getMemberName($notificationsinglearr['mid']);
                $fetchnotificationarray['details'][$key]['member_name']    	            = getMemberName($notificationsinglearr['mid']);
                $fetchnotificationarray['details'][$key]['profile_image']                 = getProfileImage($notificationsinglearr['mid']);
                $fetchnotificationarray['details'][$key]['image_basepath']                = base_url().'uploads/profile_image/';
				$fetchnotificationarray['details'][$key]['logomembername']                = strtoupper(logomembername($fetchnotificationarray['mid']));
                $img_u;
                if($this->model_community->memberlogo($notificationsinglearr['mid']) != NULL) 
                {
                    $in = $this->model_community->memberlogo($notificationsinglearr['mid']);
                    $img_u = base_url().'uploads/profile_image/'.$in['image_url'];
                }
                else
                {
                   $img_u = 'NULL'; 
                }
                $fetchnotificationarray['details'][$key]['logomembername_url']   = $img_u;
				$fetchnotificationarray['details'][$key]['mid']   = $notificationsinglearr['mid'];
				$fetchnotificationarray['details'][$key]['community_id']   = $notificationsinglearr['com_id'];
				$fetchnotificationarray['details'][$key]['talk_owner_id']   = $notificationsinglearr['talk_owner_id'];
				$fetchnotificationarray['details'][$key]['talk_access_id']   = $notificationsinglearr['id'];
				$fetchnotificationarray['details'][$key]['talk_title']   = $this->model_community->FnGetTalkTitle($notificationsinglearr['com_id']);
				
			}
		}
		
		
		echo json_encode($fetchnotificationarray);
        exit; 
	}
	
	
	function accepttalkrequest()
	{
		$data['com_id']=$this->input->post('community_id');
		$data['talk_owner_id']=$this->input->post('talk_owner_id');
		$data['mid']=$this->input->post('mid');
		$res = $this->model_community->accepttalkrequest($data);
        if($res==1)
        {
          $r['success'] = 1;  
        }
        else
        {
            $r['success'] = 'fail';
        }
        echo json_encode($r);
        exit; 

	}
	
	function declinetalkrequest()
	{
		$data['com_id']=$this->input->post('community_id');
		$data['talk_owner_id']=$this->input->post('talk_owner_id');
		$data['mid']=$this->input->post('mid');
		$res = $this->model_community->deletetalkrequest($data);
        if($res==1)
        {
          $r['success'] = 1;  
        }
        else
        {
            $r['success'] = 'fail';
        }
        echo json_encode($r);
        exit; 
	}
	
	function FnRecommendTalk()
	{
		$d['user_email'] =$this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        
		
		$d1['user_email'] = $this->input->post('toemail');
		$toresult = $this->model_member->getIdByUserEmail($d1);
		
		
		$to_mid = $toresult->mid;
		$from_mid = $result->mid;
		$comm_id=$this->input->post('comm_id');
		$message=$this->input->post('message');
		
		
		
		$data['from_mid'] = $from_mid;
        $data['to_mid'] = $to_mid;
		$data['comm_id']=$this->input->post('comm_id');
		$data['message']=$this->input->post('message');
		
		/* send notification to user */
		$data1['whose_profile_viewed'] = $to_mid;
		$data1['who_viewed'] = $from_mid;
		$data1['is_seen'] = '0';
		$data1['noti_type'] = 'recomend_community';
		$data1['message'] = $message;
		$data1['comm_id'] = $comm_id;
		$data1['title'] = 'I have recommended a talk to you';
		$cur_date = date('Y-m-d H:i:s');
		$this->model_member->visited_profile($data1); 
		$res=1;	 
		if($res==1)
        {
          $r['success'] = 1;  
        }
        else
        {
            $r['success'] = 'fail';
        }
        echo json_encode($r);
        exit; 
	}
	
	
	

}
