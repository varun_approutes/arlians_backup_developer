<?php 
class Hub extends CI_Controller
{
    function __construct(){
		parent::__construct();
		$this->load->model('model_member');
		$this->load->model('model_suggestion');

		$this->load->library('User_Manager');
		$this->load->helper('common_helper');
		$this->load->helper('text');
		$this->load->model('model_hub');
		$this->load->model('model_message');
		$this->load->model('model_memberprofile');
		$this->load->model('model_member');
		$this->load->helper('load_controller');
		$this->load->model('model_othermembersprofile');
		//$this->model_common->SiteSettingsDetails(); // This is mandatory
		$this->load->helper('cookie');
		$this->user_manager = User_Manager::get_instance();
	}
        
	function getHubView(){

		$myMid = $this->session->userdata['logged_in']['id'];
		$mids = $this->model_hub->myConnectionList($myMid);
		$my_hub = array();
		array_push($mids,$myMid);

		foreach($mids as $id){
			$my_hub[$id] = $this->model_hub->hubPostedByMember($id);
		}
		
		return $my_hub;
	}

	function getHub(){

		$myMid = $this->session->userdata['logged_in']['id'];
		$tw_digit_codes = $this->model_hub->getTopLineFromMid($myMid);

		$top_line_arr = array();
		// topline code from twenty digit code 
		foreach($tw_digit_codes as $code){
			$cd = $code['mem_code'];
			$top_line_arr[] = substr($cd,0,5);
		}


		// topline code from ten digit code 
		$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($myMid);
//echo '<pre>'; var_dump($ten_digit_code); die();
		foreach($ten_digit_code as $code){
			$cd = $code['memcodetwo'];
			$tw_digit_code = $this->model_suggestion->getTwentyDigitCodeFromTenDigit($cd);
			
			foreach($tw_digit_code as $code){ 
				$cd = $code['wizone_code'];
				$top_line_arr[] = substr($cd,0,5);
			}
		}
		
		// topline code from tag code
		$tag_codes = $this->model_hub->getTagCodes($myMid);
		
		if($tag_codes != ''){
			$tag_arr = explode(',',$tag_codes);
			//echo '<pre>'; var_dump($tag_arr);
			foreach($tag_arr as $code){
				$cd = $this->model_hub->convertingHubIdToTopline($code);
				if($cd!=NULL){
					$top_line_arr[] = $cd;
				}
			}
		}
		
		$top_line_arr = array_unique($top_line_arr);

		$related_hub_ids = $this->model_hub->getHubIdsFromTopLine($top_line_arr);
		//echo '<pre>'; var_dump($related_hub_ids); die(); 
		return $related_hub_ids;
	}
	
	function getHubx($id,$start,$limit){
		
		$myMid = $id;
		$tw_digit_codes = $this->model_hub->getTopLineFromMid($myMid);

		$top_line_arr = array();
		// topline code from twenty digit code 
		foreach($tw_digit_codes as $code){
			$cd = $code['mem_code'];
			//echo $cd.'====';
			$top_line_arr[] = substr($cd,0,5);
		}

       
		$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($myMid);

		foreach($ten_digit_code as $code){
			$cd = $code['memcodetwo'];
			$tw_digit_code = $this->model_suggestion->getTwentyDigitCodeFromTenDigit($cd);
			
			foreach($tw_digit_code as $code){ 
				$cd = $code['wizone_code'];
				$top_line_arr[] = substr($cd,0,5);
			}
		}
		
		// topline code from tag code
		$tag_codes = $this->model_hub->getTagCodes($myMid);
		
		if($tag_codes != ''){
			$tag_arr = explode(',',$tag_codes);
			//echo '<pre>'; var_dump($tag_arr);
			foreach($tag_arr as $code){
				$cd = $this->model_hub->convertingHubIdToTopline($code);
				if($cd!=NULL){
					$top_line_arr[] = $cd;
				}
			}
		}
		
		$top_line_arr = array_unique($top_line_arr);
		//print_r($top_line_arr);die();
		return $top_line_arr;
       /*  $blockedusersids = array();
        $blockedusersids = $this->model_hub->FnFindBlockedHubUserById($myMid); */


//$related_hub_ids = $this->model_hub->getHubIdsFromTopLineWithBlockedId($top_line_arr,$start,$limit,$blockedusersids);
		//print_r()

		//echo '<pre>'; var_dump($related_hub_ids); die(); 
		//return $related_hub_ids;
	}
	function moreHub(){
		
		$data['tag'] = $this->model_member->getAllTag();
		$data['user'] = $this->user_manager->get_looged_in_user();		
            $hub_ids_connects = load_controller('hub', 'getHubView');         
            $hub_ids=array();
            foreach($hub_ids_connects as $ids){
                if($ids != ""){
                   foreach($ids as $id){
                        $hub_ids[] = $id['hid'];
                   }
                }
            }
            $hub_ids_matches = load_controller('hub', 'getHub');
            foreach($hub_ids_matches as $id){
                if($id != ""){
                    $hub_ids[] = $id['hub_id'];
                }
            }
            $hub_ids = array_unique($hub_ids);
            $hub_data = array();
            foreach($hub_ids as $hid){
                $hubs = $this->model_hub->getHubById($hid);
                if($hubs != NULL){
                    $key = $hubs[0]['posted_on'];
                    $hub_data[strtotime($key)] = $hubs[0];
                }                
            }
            krsort($hub_data); 
            $mid=$this->session->userdata['logged_in']['id'];                      
            foreach ($hub_data as $key => $value) {
            	$hud_ind_id = $value['hid'];
            	$like = $this->model_hub->checkHubLike($hud_ind_id,$mid);
            	$comments = $this->model_hub->getComments($hud_ind_id);
            	$hub_data[$key]['comments'] = $comments;
            	$data_for_connection['mid'] = $mid;
            	$data_for_connection['connected_id'] = $value['mid'];
            	$is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection);        	//echo '<pre>'; print_r($comments);
            	$hub_data[$key]['is_connected'] = $is_connected;
            	$likes = $this->model_hub->getTotalLikes($hud_ind_id);
            	$hub_data[$key]['likes'] = $likes;            	
            	$hub_data[$key]['like'] = $like;            	   	
            } 
            //echo"<pre>";
            //print_r($hub_data);exit;
           $data['hubs'] = $hub_data;
         
        $this->load->view('member/morehub',$data);
    }
function isConnected(){
		$mid=$this->input->post('mid');
		$connected_id=$this->input->post('connected_id');
        $is_connected =  $this->model_suggestion->is_connected($mid,$connected_id);   
		if($is_connected==0){
			
			$res = array('response'=>'success');
		
		}else{
			
			$res = array('response'=>'fail');
		}
		echo json_encode($res);		
		exit;
	}
    function moreHubApi_old(){
        //print_r($_POST);die;
    //===========================================================
    $data['user_email'] = $this->input->post('user_emai');
    $result = $this->model_member->getIdByUserEmail($data);
    $user_id = $result->mid; 
     $mid=$user_id; // the userid of the logged in user.
   //die();
    //=============================================================  
    $prof_img;
    $bussinessname;
    $data['tag'] = $this->model_member->getAllTag();
    //$data['user'] = $this->user_manager->get_looged_in_user();    
            //$hub_ids_connects = load_controller('hub', 'getHubView');
			$start=0;
			$limit = 10;
			if($this->input->post('start')>0)
            {
				$start = ($this->input->post('start')*$limit);
			}
     // echo $start;
			
            $hub_ids_connects = $this->getHubViewApi($mid,$start,$limit);
          
            $hub_ids=array();
						
				foreach($hub_ids_connects as $ids)
				{
					if($ids != ""){
					   foreach($ids as $id){
							$hub_ids[] = $id['hid'];
					   }
					}
				}
			
			          
       
            $hub_ids_matches = $this->getHubx($mid,$start,$limit);
			  
            foreach($hub_ids_matches as $id){
                if($id != ""){
                    $hub_ids[] = $id['hub_id'];
                }
            }
          
            $hub_ids = array_unique($hub_ids);
           	
           
            $hub_data = array();
			
            foreach($hub_ids as $hid){
                $hubs = $this->model_hub->getHubById($hid);
				//echo '<pre>'; var_dump($hubs); die();
	             if($hubs != NULL){
                    $key = $hubs[0]['posted_on'];
                    $hub_data[strtotime($key)] = $hubs[0];
			
                    $hub_data[strtotime($key)]['timestamp'] = strtotime($hubs[0]['posted_on']);
                    $hub_title = explode(',',$hubs[0]['tag_id']);
				    $hubTitle = array();
                    foreach($hub_title as $hb){

                      $title = getTagNameById($hb);
                      $hubTitle[$hb] = $title[0]['tag_name'];
                    }
                    $hub_data[strtotime($key)]['tag_title'] = $hubTitle; 
                }                
            }
            krsort($hub_data);
                   //echo '<pre>'; var_dump($hub_data); die();
            foreach ($hub_data as $key => $value) {

              $hud_ind_id = $value['hid'];
              $hud_post_time = $value['posted_on'];
              $like = $this->model_hub->checkHubLike($hud_ind_id,$mid);
              $comments = $this->model_hub->getComments($hud_ind_id);
              //$whoseShare = $this->model_hub->whoseShare($mid,$hud_ind_id,$hud_post_time);
              $whoseShare = $this->model_hub->whoseShare($mid,$hud_ind_id);
			//  var_dump($whoseShare);
			  $userProfType=userProfType($mid);
				if($userProfType=='Buisness'){
					$whose_share=getMemberbusinName($whoseShare); 
				}else{
					
					$whose_share=getMemberName($whoseShare);
				}
			$hub_data[$key]['whose_share'] = $whose_share;
              $hub_data[$key]['whose_share_id'] = $whoseShare;
              $hub_data[$key]['whose_share_logoname'] = logobussinessname($whoseShare);
              $hub_data[$key]['whose_share_prof_img'] = getProfileImage($whoseShare);
              $hub_data[$key]['whose_share_prof_img_path']= base_url().'uploads/profile_image/';
				
             // echo '<pre>'; var_dump($whose_share); 
              $hub_data[$key]['not_in_network_like'] = $this->getUserNameOutsideConnects($hud_ind_id,$mid,$hud_post_time);
              $hub_data[$key]['not_in_network_comment'] = $this->getUserNameOutsideConnectsComment($hud_ind_id,$mid,$hud_post_time);
              

              $hub_data[$key]['comments'] = $comments;
              $hub_data[$key]['share_count'] = $this->model_hub->getTotalShare($hud_ind_id);
              
              foreach($comments as $k=>$val)
              {
                
                //echo '<pre>'; var_dump($val);
                $commented_user_details = getParticularUserDetails($val['mid']);
                $com_name = $commented_user_details['name'];
                $com_name_initial = logobussinessname($val['mid']);
				 $bussinessname_com = $val['bussinessname'];
			   if($bussinessname_com==''){
				   
				   $bussinessname_com=getMemberName($val['mid']);
			   }
			   
                $hub_data[$key]['comments'][$k]['buisnesname'] = $bussinessname_com;
                $hub_data[$key]['comments'][$k]['com_initial_name'] = $com_name_initial;
                $hub_data[$key]['comments'][$k]['name'] = getMemberName($val['mid']); 
                $hub_data[$key]['comments'][$k]['image_path'] = base_url().'uploads/profile_image/';
				 $hub_data[$key]['comments'][$k]['hub_post_time'] = calculateTimeFromPost($hub_data[$key]['posted_on'], date("Y-m-d H:i:s"), "s");
                //echo '<pre>'; var_dump($rsb); die();
              }
              //-----------
			  
			 //if($value['mid']!=''){
              $prof_img=getProfileImage($value['mid']);
              //---------
               $this->db->select('*');
                $this->db->from('ar_request_access_page');
				
				$this->db->where('mid',$value['mid']);
				
                
                $query = $this->db->get();
                $rsb = $query->result_array(); 
               $bussinessname = $rsb[0]['bussinessname'];
			   if($bussinessname==''){
				   
				   $bussinessname=getMemberName($value['mid']);
			   }

                $postHubMembers = $this->model_member->getUserById($value['mid']);
                //echo '<pre>'; var_dump($postHubMembers);
                $em = logobussinessname($value['mid']);
				
                $profile_initional = $em; 


		      $data_for_connection['mid'] = $mid;
              $data_for_connection['connected_id'] = $value['mid'];
			
			
              $is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection);         //echo '<pre>'; print_r($comments);
              $hub_data[$key]['is_connected'] = $is_connected;
              $likes = $this->model_hub->getTotalLikes($hud_ind_id);
			   $total_share = $this->model_hub->getTotalShare($hud_ind_id);
			  if($prof_img!=''){
              $hub_data[$key]['prof_img'] =  base_url().'uploads/profile_image/'.$prof_img;  
			  }else{
				   $hub_data[$key]['prof_img'] =  '';  
			  }			  
          $hub_data[$key]['profile_initional'] = $profile_initional;  
          // var_dump($hub_data[$key]['posted_on']);
          $hub_data[$key]['hub_post_time'] = calculateTimeFromPost($hub_data[$key]['posted_on'], date("Y-m-d H:i:s"), "s");
          $hub_data[$key]['bussinessname'] = $bussinessname;     
          $hub_data[$key]['name'] = getMemberName($mid);          
          $hub_data[$key]['likes'] = $likes; 
          $hub_data[$key]['total_share'] = $total_share; 
          $hub_data[$key]['like'] = $like;
          $hub_data[$key]['type'] = 'hub';  
          $hub_data[$key]['show_comments'] = false; 
          $hub_data[$key]['comment'] = '';			  
          $hub_data[$key]['open_comment_box'] = false;			  
          $hub_data[$key]['comment_count'] = count($comments);			  
          $hub_data[$key]['base_url'] = base_url().'uploads/hub_images/';                
            } 
            //echo '<pre>'; var_dump($hub_data); die();
           $data_api['hubs'] = $hub_data;

           //-----------------Watson ------------
           include_once('memberprofile.php');
           //------------------------------------

           //--------------Bing---------------

           $bing = new memberprofile();
          // $bing->bing_api($mid);
          // $data_api['bing_news'] = $bing->fetchBingNews($mid); 
           // $data_api['bing_news'] = $bing->fetchBingLalNews($mid);

          $nstart = $start/2;
          $nlimit = 2;
          $data_api['bing_news'] = $bing->fetchBingLalNews($mid,$nstart,$nlimit);
           
           //$data_api['bing_news'] = 
           //--------------------------------
           //echo '<pre>'; print_r($data_api['bing_news']); die();
           $i=1;
           $j=0;
           $z=($start * 4);

            $api = array();
            if(!empty($data_api['hubs']))
            {
            foreach($data_api['hubs'] as $hbs)
            {
              if($i%3!=0)
              {
                array_push($api,$hbs);
              }
              else
              {
                array_push($api,'yy');
                array_push($api,$hbs);
                //$api[$i]=$i;
                //array_push($api,'xx','yy');
                $i = 1;
              }
              $i++;
            }
          }

          //echo '<pre>'; var_dump($api); die();
          
          foreach($api as $key=>$value)
          {
            if($value=="xx")
            {
              //if(!empty($data_api['watson_news'][$j]))
                //$api[$key]=$data_api['watson_news'][$j++];
            }
            else if($value=="yy")
            {
              if(!empty($data_api['bing_news'][$z]))
                $api[$key]=$data_api['bing_news'][$z++];
            }
          }
          
          //----------------------------
            $hub_ids_matches = $this->getHubx($mid,$start+1,$limit);
            $news_count = 0;

            if(count($hub_ids_matches) == 0)
            {
          
              for($i=$j;;$i++)
              {
                $news_count++;
                //if(!empty($data_api['watson_news'][$i]))
                 // array_push($api,$data_api['watson_news'][$i]);
                if(!empty($data_api['bing_news'][$z]))
                  array_push($api,$data_api['bing_news'][$z++]);
                //if(empty($data_api['watson_news'][$i]) && empty($data_api['bing_news'][$z]))
                if(empty($data_api['bing_news'][$z]))
                  break;

                if($news_count == 10)
                {
                  break;
                }
              } 
            }
            //---------------------------
            foreach (array_keys($api, 'yy') as $key) 
            {
                unset($api[$key]);
            }
          $api = array_filter($api);
          // removing duplicate news from the array api
          $api = array_map("unserialize", array_unique(array_map("serialize", $api)));
		  //print_r($api);die();
		//  $nextcount=count($api)+1;
		//  $api[$nextcount]=array('nextsearchindex'=>$nextsearchindex);
		 // print_r($api);die();
           //-----------------------------------
         echo json_encode($api);
         exit;
  
    }

    private function loadWatsonAndBing($data_api,$api,$i,$j)
    {
    	if(!empty($data_api['watson_news'][$j]))
    	{
    		$api[$i] = $data_api['watson_news'][$j];
    		//$api[$i++]="Watson";
    	}
    	if(!empty($data_api['bing_news'][$j])){
    		$api[$i+1] = $data_api['bing_news'][$j];
    		//$api[$i+1]="Bing";
    	}
      //echo '<pre>'; var_dump($api); 
    	return $api;
    }
    public function hubEditText(){
    	$data['content'] = $this->input->post('edited_text');
    	$id = $this->input->post('hid');    	
    	$result = $this->model_hub->post_hub_edit($id,$data);
    	echo $result;
    	exit;
    }
    public function hubImgEdit(){
    	$imagename = $this->input->post('imgFile');
    	if($imagename!=''){
			$content = $imagename;
			//print_r($content); exit;
			if($content<>''){
				$parts=explode(',',$content);
				if(count($parts)>1){					
					$product_image_data = base64_decode(str_replace($parts[0].',',"",$content));
					//print_r($product_image_data); exit;
					$image_name = "member".round(microtime(true) * 1000).".png";
					$target_path = FCPATH ."uploads".DS."hub_images".DS.$image_name;

					file_put_contents($target_path,$product_image_data);						
					$data = array("content" => $image_name);	
					$id = $this->input->post('hid');			
					$result = $this->model_hub->post_hub_edit($id,$data);
					echo $result;
				}else{
					$result = false;
				}
				exit;
			}
		}
    }
    function hubDetails($hubid,$hubpostid){
    	if($_POST){
    		print_r($_POST); exit;
    	}
		$mid=$this->session->userdata['logged_in']['id'];
		$hubid=$this->uri->segment(4);
		$hubpostid=$this->uri->segment(3);
		$data['hubid']=$hubid;
		$data['hubpostid']=$hubpostid;
		if($mid==$hubpostid){
			$data['viewtype']='own';
		}else{
			$data['viewtype']='other';
		}
		$hubs = $this->model_hub->getHubById($hubid);
		$data['hub']=$hubs[0];
		$like = $this->model_hub->checkHubLike($hubid,$mid);
        $comments = $this->model_hub->getComments($hubid);
        $data['hub']['comments'] = $comments;
		$data_for_connection['mid'] = $mid;
        $data_for_connection['connected_id'] = $hubpostid;
        $is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection); 
        $data['hub']['is_connected'] = $is_connected;
        $likes = $this->model_hub->getTotalLikes($hubid);
        $data['hub']['likes'] = $likes;            	
        $data['hub']['like'] = $like;   
		
	$this->load->view('member/hubdetails',$data);
	}

     function shared() 
     {
        $data['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data);
        $data['mid'] = $result->mid;
        $data['hub_id'] = $this->input->post('hub_id');
        $posted_id = getHubById($data['hub_id']);
        $data1['whose_profile_viewed'] = $posted_id[0]["mid"];
        $data1['who_viewed'] = $data['mid'];
        $data1['title'] = '';
        $data1['noti_type'] = 'hub_share';
		
      //  $this->model_othermembersprofile->inser_view($data1);
//$res = $this->model_hub->shared($data);
		$hub_id = $this->model_hub->shared($data);
		$hub_ids = array($hub_id);
		$hub_Details=$this->currentHubDetails($hub_ids,$data['mid']);
		$res['hub_details'] = $hub_Details[0];
		$res['response']='success';
        echo json_encode($res);
        exit;
    }

    function shareWithComment() 
     { 
        $data1['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data1);
        $data['mid'] = $result->mid;
		$hbcomment=$this->input->post('hbcomment');
		$hbcomments=explode('~',$hbcomment);
        $data['hub_id'] = $this->input->post('hub_id');
        $data['hbcomment'] =($hbcomment=='' || $hbcomment==null )?null:$hbcomment;
        $posted_id = getHubById($data['hub_id']);
        $data['whose_profile_viewed'] = $posted_id[0]["mid"];
        $data['who_viewed'] = $data['mid'];
        $data['title'] = '';
        $data['noti_type'] = 'hub_share';
		$data['shared_for'] =  $this->input->post('scope');
		$data['POSTFLAG'] =  'SHARE'.$this->input->post('POSTFLAG');
		$data['url_provider'] =  $this->input->post('url_provider');
		/***** check whether the post belongs to another user in share table */	
		$hub_ownermid=$this->model_hub->FnGetOldSharemid($data['hub_id']);
		/***** check whether the post belongs to another user in share table */	
		
	  $posted_id = getHubById($data['hub_id']);
      $data11['whose_profile_viewed'] = $posted_id[0]["mid"];
      $data11['who_viewed'] = $data['mid'];
      $data11['is_seen'] = '0';
      $data11['noti_type'] = 'hub_share';
      $data11['title'] = 'I have shared your post';
	  $cur_date = date('Y-m-d H:i:s');
      $this->model_member->visited_profile($data11);
		
	   // send email parameters	
	  $senderresultarr = $this->model_member->getIdDetailsByUserEmail($data1);
	  $senderfullname=$senderresultarr['fname'].' '.$senderresultarr['lname'];
	  $currentdate=date("dS \of F Y");
	  
	  $hub_id=$this->input->post('hub_id');
	  $hub_idarr = $this->model_member->getHubuserDetailsById($hub_id);

	  $emailsent_to=$hub_idarr['email']; 
	  
	  $posttype=($hub_idarr['hbcomment']!='')?'Comment':'Post';
	  $postcontent=($hub_idarr['hbcomment']!='')?$hub_idarr['hbcomment']:$hub_idarr['hub_title'];
	  
	  $subject = "I have shared your ".$posttype."";
	  $mailinnermessage='<p><strong>Subject:</strong> I have shared your '.$posttype.'. The details are as follows :</p><p><strong>Title:</strong> '.$postcontent.'</p><p><strong>Comment on Share:</strong> '.$hbcomments[0].'</p>';
	  // send email parameters end
	  
	  // unread message notification to user
		/* $message['msg_sub'] = "Your post has been shared";
		$message['msg_body'] = $hbcomments[0];
        $msgid = $this->model_message->insertMessage($message);
		$d['sender_id'] = $result->mid;
        $d['receiver_id'] = $hub_idarr['mid'];
        $d['msg_id'] = $msgid;
		//print_r($d);die();  
        $res = $this->model_member->insertMessageTrans($d); */
	  // unread message notification to user
	  
	  
	  
	 // $this->FnSendnotificationToUsernotification($subject,$emailsent_to,$senderfullname,$mailinnermessage); // send email function to user 
	    $hubid = $this->model_hub->shared($data,$hub_ownermid);
		$hub_ids = array($hubid);
		$hub_Details=$this->currentHubDetails($hub_ids,$result->mid);
		$resp['hub_details'] = $hub_Details[0];
		$resp['hub_details']['POSTFLAG'] = $hub_Details[0]['hub_share_type'];
		$resp['response']='success';
        echo json_encode($resp); 
        exit;
    }

    function postComment(){
    	$user=$this->user_manager->get_looged_in_user();
    	//print_r($user); exit;
    	$data['hub_id'] = $_POST['hub_id'];
    	$data['comments'] = $_POST['comment'];
		$data['mid'] = $this->session->userdata['logged_in']['id'];
		$res_id = $this->model_hub->postComment($data);
		$posted_id=getHubById($data['hub_id']);
		$data1['whose_profile_viewed'] = $posted_id[0]["mid"];
		$data1['who_viewed'] =$data['mid'];
		$data1['is_seen'] = '0';
		$data1['title'] = null;
		$data1['noti_type'] = 'hub_comment';
		$this->model_member->visited_profile($data1);
		$this->hubEditTime($_POST['hub_id']);
		//$this->model_othermembersprofile->inser_view($data1);
		$res = array('id' =>$res_id,'comment'=>checkURLfromString($data['comments']),'user_image'=>$user['fornt_image']);
		echo json_encode($res);		
		exit;
    }


   function postCommentApi(){
		$d['user_email'] = $this->input->post('user_email');        
		$result = $this->model_member->getIdByUserEmail($d);
		$mid = $result->mid;
		$cur_date = date('Y-m-d H:i:s');
        $data['mid'] = $mid;
        $data['hub_id'] = $_POST['hub_id'];
        $data['comments'] = $_POST['comment'];
        $data['comm_date'] = $cur_date;
		$res_id = $this->model_hub->postComment($data);
		$posted_id=getHubById($data['hub_id']);
		//echo '<pre>'; var_dump($posted_id); die();
		$data1['whose_profile_viewed'] = $posted_id[0]["mid"];
		$data1['who_viewed'] =$data['mid'];
		$data1['is_seen'] = '0';
		$data1['title'] = null;
		$data1['noti_type'] = 'hub_comment';
		$this->model_member->visited_profile($data1);
		$this->hubEditTime($data['hub_id'],$cur_date);		
		$resp = array('response'=>'success','id' =>$res_id,'comment'=>checkURLfromString($data['comments']),'user_image'=>getProfileImage($mid),'total_comment_like'=>0,'hub_id'=> $data['hub_id']);
	      // send email parameters
		  $senderresultarr = $this->model_member->getIdDetailsByUserEmail($d);
		  $senderfullname=$senderresultarr['fname'].' '.$senderresultarr['lname'];
		
		  $hub_id=$this->input->post('hub_id');
		  $hub_idarr = $this->model_member->getHubuserDetailsById($hub_id);

		  $emailsent_to=$hub_idarr['email']; 
		  
		  $posttype=($hub_idarr['hbcomment']!='')?'Comment':'Post';
		  $postcontent=($hub_idarr['hbcomment']!='')?$hub_idarr['hbcomment']:$hub_idarr['hub_title'];
	     
		  $subject = "I have commented on your ".$posttype."";
		  $mailinnermessage='<p><strong>Subject:</strong> I have commented on  your '.$posttype.'. The details are as follows :</p><p><strong>Title:</strong> '.$postcontent.'</p><p><strong>Comment:</strong> '.$_POST['comment'].'</p>';
	  
	  // send email parameters end 
	    // unread message notification to user
		//$message['msg_sub'] = "A comment has been posted on your post";
		//$message['msg_body'] = $postcontent;
		//$msgid = $this->model_message->insertMessage($message);
		//$dt['sender_id'] = $mid;
		//$dt['receiver_id'] = $hub_idarr['mid'];
		//$dt['msg_id'] = $msgid;
		//print_r($d);die();  
		//$res = $this->model_member->insertMessageTrans($dt);
		// unread message notification to user
	  
	  
	  
	  
     
		//$this->FnSendnotificationToUsernotification($subject,$emailsent_to,$senderfullname,$mailinnermessage); // send email function to user  
       
		 
		echo json_encode($resp);		
		exit;
    }
	function conceptInsights(){		
		$this->load->view('member/conceptInsights');
	}
	function firstInsightSearch(){
		$key_word = rawurlencode($this->input->post('keyword'));
		//$key_word =$_REQUEST['q'];
		$curlSession = curl_init();
		$url='https://concept-insights-demo.mybluemix.net/api/labelSearch?query='.$key_word.'&limit=7&concept_fields={%22abstract%22%3A1}';
		curl_setopt($curlSession, CURLOPT_URL, $url);
		curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

		$jsonData = json_decode(curl_exec($curlSession));
		curl_close($curlSession);
		//echo '<pre>';
		foreach($jsonData as  $key => $jdata){
			if($key=='matches'){
				$matches = $jdata;
			}
		}		
		//print_r($matches);
		$suggested_array=array();
		foreach($matches as $key => $data){
			foreach($data as $keydata => $data_value){
				if($keydata == 'label'){
					$obj = (object) array('lable' => $data_value);
					array_push($suggested_array,$obj);
				}
			}			
		}
		echo json_encode($suggested_array);	
		//print_r ($suggested_array);
		exit;
		
	}
	function finalInsightSearch(){
		$key_word = rawurlencode($this->input->post('keyword'));
		//$key_word =$_REQUEST['q'];
		$curlSession = curl_init();
		$url='https://concept-insights-demo.mybluemix.net/api/conceptualSearch?ids%5B%5D=%2Fgraphs%2Fwikipedia%2Fen-20120601%2Fconcepts%2F'.$key_word.'&limit=10&document_fields=%7B%22user_fields%22%3A1%7D';
		curl_setopt($curlSession, CURLOPT_URL, $url);
		curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

		$jsonData = json_decode(curl_exec($curlSession));
		$jsonDat = curl_exec($curlSession);
		curl_close($curlSession);
		//$api='curl -u 1213a261-d453-474d-bd8f-51e95d73802c:xPm7tN6BhxsA "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/public/ibmresearcher/conceptual_search?ids%5B%5D=%2Fgraphs%2Fwikipedia%2Fen-20120601%2Fconcepts%2F'.$key_word.'&limit=10&document_fields=%7B%22user_fields%22%3A1%7D"';
		//echo $api;
		//$jsonDat = exec($api);
		echo $jsonDat;
		//exit;
		foreach($jsonData as  $key => $jdata){
			if($key=='matches'){
				$matches = $jdata;
			}
		}		
		//print_r($matches);
		$suggested_array=array();
		foreach($matches as $key => $data){
			foreach($data as $keydata => $data_value){
				if($keydata == 'label'){
					$obj = (object) array('lable' => $data_value);
					array_push($suggested_array,$obj);
				}
			}			
		}
		echo json_encode($suggested_array);	
		//print_r ($suggested_array);
		exit;
		
	}
	function postLike(){
		$hid = $_POST['hub_id'];
		$mid = $this->session->userdata['logged_in']['id'];
		$posted_id=getHubById($hid);
		$data1['whose_profile_viewed'] = $posted_id[0]["mid"];
		$data1['who_viewed'] =$mid;
		$data1['title'] = '';
		$data1['noti_type'] = 'hub_like';
		//var_dump($data1);die();
		$this->model_othermembersprofile->inser_view($data1);
		echo $this->model_hub->postLike($hid,$mid);
		exit;
	}

	function checkLike(){
		$hid = $_POST['hub_id'];
		$mid = $this->session->userdata['logged_in']['id'];		
		echo $res = $this->model_hub->checkHubLike($hid,$mid);
		exit;
	}

	function hubPublish(){
		//My publish post
		$id = $this->session->userdata['logged_in']['id'];
		$data['my_publish'] = $this->model_hub->getMyPublish($id);
		$data['tag'] = $this->model_member->getAllTag();
		$this->load->view('member/hub_publish',$data);
	}

	function hubPublishApi(){
		//My publish post
		$arr['email'] = $this->input->post('user_email');
		$user_id = getIdByUserEmail($arr['email']);

		$id = $user_id->mid;

		$data['my_publish'] = $this->model_hub->getMyPublish($id);
		$data['tag'] = $this->model_member->getAllTag();
												
		echo json_encode($array=array('msg'=>'Success'));	
		//print_r ($suggested_array);
		exit;

	}
	
	function hubPublishEdit(){
		$hid=$this->uri->segment(3);
		$hub_details=$this->model_hub->getHubById($hid);
		
		$data['hid']=$hub_details[0]['hid'];
		$data['mid']=$hub_details[0]['mid'];
		$data['hub_title']=$hub_details[0]['hub_title'];
		$data['content']=$hub_details[0]['content'];
		$data['link']=$hub_details[0]['link'];
		$data['tag_id']=$hub_details[0]['tag_id'];
		
		$id = $this->session->userdata['logged_in']['id'];
		$data['my_publish'] = $this->model_hub->getMyPublish($id);
		$data['tag'] = $this->model_member->getAllTag();
		
		$this->load->view('member/hub_publish_edit',$data);
	}

	function hubPublishEditApi(){
		$hid=$this->uri->segment(3);
		$hub_details=$this->model_hub->getHubById($hid);
		
		$data['hid']=$hub_details[0]['hid'];
		$data['mid']=$hub_details[0]['mid'];
		$data['hub_title']=$hub_details[0]['hub_title'];
		$data['content']=$hub_details[0]['content'];
		$data['link']=$hub_details[0]['link'];
		$data['tag_id']=$hub_details[0]['tag_id'];
            //---------------------

            $arr['email'] = $this->input->post('user_email');
            $user_id = getIdByUserEmail($arr['email']);

            $id = $user_id->mid;

            //---------------------
		$data['my_publish'] = $this->model_hub->getMyPublish($id);
		$data['tag'] = $this->model_member->getAllTag();
		
		$this->load->view('member/hub_publish_edit',$data);
	}
	
	function hub_edit_post(){
		
			$tag_arr = '';
			if($_POST['tag'] != ''){
				$tag_arr = implode(',',$_POST['tag']);
			}
			
			//$filename = "";
			//echo '<pre>';print_r($_POST);print_r($_FILES);die;
			/* if(!empty($_FILES['file_hub_post']['name'])){
				//echo "TEST";die;
				$prev_img_path = FCPATH."/uploads/hub_images/".$this->input->post('prev_img');
				unlink($prev_img_path);
				
				$path = $_FILES['file_hub_post']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				
				$filename = uniqid('HubPublish', true).".".$ext;
				
				$tmp_file = $_FILES['file_hub_post']['tmp_name'];
				$uploads_dir = FCPATH.'/uploads/hub_images/'.$filename;

				move_uploaded_file($tmp_file, $uploads_dir);
			}else{
				$filename=$this->input->post('prev_img');
			} */
            //---------------------
			
			$imagename = $this->input->post('imgFile');
			$image_name;
			//echo '<pre>'; var_dump($imagename); die();
			if ($imagename != '')
			{
				$content = $imagename;
				if ($content <> '') 
				{
					$parts = explode(',', $content);
	                if (count($parts) > 1) 
	                {
	                    $product_image_data = base64_decode(str_replace($parts[0] . ',', "", $content));
	                    $image_name = "HubPublish" . round(microtime(true) * 1000) . ".png";
	                    $target_path = FCPATH . "uploads" . DS . "hub_images" . DS . $image_name; 
	                    file_put_contents($target_path, $product_image_data);
						$ext = pathinfo($image_name, PATHINFO_EXTENSION);
						$data['link'] = $image_name;
					}
				}
			} 
			
			
			
			
			

            $arr['email'] = $this->input->post('user_email');
            $user_id = getIdByUserEmail($arr['email']);

            $mid = $user_id->mid;

            //---------------------
			$data['hub_title'] = $_POST['title'];
			$data['content'] = $_POST['comments'];
			
			$data['shared_for'] = 4;
			$data['tag_id'] = $tag_arr;
			$hub_id = $_POST['hid'];
			$this->model_hub->post_hub_edit($hub_id,$data);
			
			$tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid);
			$code = $tw_digit_code[0]['mem_code'];
			$code = substr($code,0,5);
			
			$dt['hub_id'] = $hub_id;
			$dt['topline_code'] = $code;

			$res = $this->model_hub->hubPostDetails($dt);

			if($tag_arr != ''){
				$tag_arr = $_POST['tag'];
				//var_dump($tag_arr); die();
				$this->model_hub->del_hub_details($hub_id);
				foreach($tag_arr as $tag){
					$top_line_code = $this->model_hub->convertingHubIdToTopline($tag);
					
					if($top_line_code != NULL){
						$d['hub_id'] = $hub_id;
						$d['topline_code'] = $top_line_code;
						$res = $this->model_hub->hubPostDetails($d);
					} 
				} 
			}
			
		$response = array('response'=>'success','hub_id'=>$hub_id,'ext'=>$ext,'imagename'=>$image_name);
			echo json_encode($response);
			exit;
		//redirect('hub/hubPublish');
	}
	
	function hub_post_api(){
			
			$tag_arr = '';
			if($_POST['tag'] != '')
			{
				$tag_arr = implode(',',$_POST['tag']);
			}			
			$imagename = $this->input->post('imgFile');
			$image_name;
			//echo '<pre>'; var_dump($imagename); die();
			$data['hub_share_type'] = 'PUBLISHHUB';
			if ($imagename != '')
			{
				$content = $imagename;
				if ($content <> '') 
				{
					$parts = explode(',', $content);
	                if (count($parts) > 1) 
	                {
	                    $product_image_data = base64_decode(str_replace($parts[0] . ',', "", $content));
	                    $image_name = "HubPublish" . round(microtime(true) * 1000) . ".png";
	                    $target_path = FCPATH . "uploads" . DS . "hub_images" . DS . $image_name; 
	                    file_put_contents($target_path, $product_image_data);
						
					}
				}
			} 
			
			$ext = pathinfo($image_name, PATHINFO_EXTENSION);
			//--------------------------------------------------
			//---------------------

            $arr['email'] = $this->input->post('user_email');
            $user_id = getIdByUserEmail($arr['email']);

            $data['mid'] = $user_id->mid;

            //---------------------
			$data['hub_title'] = $this->input->post('title');
			$data['content'] = $this->input->post('publish_text');
			$data['link'] = $image_name;

			$data['shared_for'] = 4;
			$data['tag_id'] = $tag_arr;
			
			$hub_id = $this->model_member->post_hub($data);
			
			$tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
			$code = $tw_digit_code[0]['mem_code'];
			$code = substr($code,0,5);

			$dt['hub_id'] = $hub_id;
			$dt['topline_code'] = $code;

			$res = $this->model_hub->hubPostDetails($dt);

			if($tag_arr != ''){
				$tag_arr = $_POST['tag'];
				foreach($tag_arr as $tag){
					$top_line_code = $this->model_hub->convertingHubIdToTopline($tag);
					
					if($top_line_code != NULL){
						$d['hub_id'] = $hub_id;
						$d['topline_code'] = $top_line_code;
						$res = $this->model_hub->hubPostDetails($d);
					} 
				} 
			}
			$hub_ids = array($hub_id);
			$hub_Details=$this->currentHubDetails($hub_ids,$mid);
			$hub_Details[0]['POSTFLAG']=$hub_Details[0]['hub_share_type'];
			$hub_Details[0]['likeconnectedtouser']=false;
			$hub_Details[0]['commentconnectedtouser']=false;
			$res = array('response'=>'success','hub_details'=>$hub_Details[0],'ext'=>$ext,'imagename'=>$image_name);
			echo json_encode($res); exit;
		//redirect('hub/hubPublish');
	}

	function hub_post(){
			
			$tag_arr = '';
			if($_POST['tag'] != ''){
				$tag_arr = implode(',',$_POST['tag']);
			}			
			$filename = "";
			if(isset($_FILES)){
				$path = $_FILES['file_hub_post']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				
				$filename = uniqid('HubPublish', true).".".$ext;
				
				$tmp_file = $_FILES['file_hub_post']['tmp_name'];
				$uploads_dir = FCPATH.'/uploads/hub_images/'.$filename;

				 move_uploaded_file($tmp_file, $uploads_dir);
			}
			$data['mid'] =  $this->session->userdata['logged_in']['id'];
			$data['hub_title'] = $_POST['title'];
			$data['content'] = $_POST['comments'];
			$data['link'] = $filename;
			$data['shared_for'] = 4;
			$data['tag_id'] = $tag_arr;
			
			$hub_id = $this->model_member->post_hub($data);
			
			$tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
			$code = $tw_digit_code[0]['mem_code'];
			$code = substr($code,0,5);

			$dt['hub_id'] = $hub_id;
			$dt['topline_code'] = $code;

			$res = $this->model_hub->hubPostDetails($dt);

			if($tag_arr != ''){
				$tag_arr = $_POST['tag'];
				foreach($tag_arr as $tag){
					$top_line_code = $this->model_hub->convertingHubIdToTopline($tag);
					
					if($top_line_code != NULL){
						$d['hub_id'] = $hub_id;
						$d['topline_code'] = $top_line_code;
						$res = $this->model_hub->hubPostDetails($d);
					} 
				} 
			}

		redirect('hub/hubPublish');
	}

	function deleteComment()
	{
		$data['user_email'] = $this->input->post('user_email');        
		$result = $this->model_member->getIdByUserEmail($data);

		$data['mid']=$result->mid;
    	$data['comm_id']=$this->input->post('comment_id');		
		
		$res = $this->model_hub->deleteComment($data);
		
		$result_data = array();
		if($res==1)
            $result_data['response'] = "success";
        else
            $result_data['response'] = "false";
   
        echo json_encode($result_data);
        exit;
    }

    function deletePost(){

    	$data['user_email'] = $this->input->post('user_emai');        
		$result = $this->model_member->getIdByUserEmail($data);

		$data['mid']=$result->mid;
    	$data['hub_id']=$this->input->post('hub_id');		
		
		echo $res = $this->model_hub->deletePost($data);
		exit;
    }

	function deleteLike(){
		//$mid = $this->session->userdata['logged_in']['id'];
		$data['user_email'] = $this->input->post('user_emai');        
		$result = $this->model_member->getIdByUserEmail($data);
		$mid = $result->mid;
		$hid = $_POST['hub_id'];
		echo $res = $this->model_hub->deleteHub($hid,$mid);
		exit;

	}

	function reportHub(){
		$data['hub_id']=$this->input->post('hub_id');
		$data['reports']=$this->input->post('reason_report');
		$data['mid']=$this->session->userdata['logged_in']['id'];
		echo $res = $this->model_hub->reportHub($data);
		exit;
	}
	function hub_delete(){
		$id = $_POST['id'];
		$res = $this->model_hub->hub_delete($id);
		if($res==1)
            $result_data['response'] = "success";
        else
            $result_data['response'] = "false";
        echo json_encode($result_data);
        exit;
	}

   // Phase 2 API //
    function userDeleteLike() 
    {
        $hid = $this->input->post('hub_id');
        $data['user_email'] = $this->input->post('user_email');        
        $result = $this->model_member->getIdByUserEmail($data);
        $mid = $result->mid;
        $result_data = array();        
        $res = $this->model_hub->deleteHub($hid, $mid);
		 // send email parameters start
		  $senderresultarr = $this->model_member->getIdDetailsByUserEmail($data);
		  $senderfullname=$senderresultarr['fname'].' '.$senderresultarr['lname'];
		
		  
		  $hub_id=$this->input->post('hub_id');
		  $hub_idarr = $this->model_member->getHubuserDetailsById($hub_id);
	
		  $emailsent_to=$hub_idarr['email']; 
		  
		  $posttype=($hub_idarr['hbcomment']!='')?'Comment':'Post';
		  $postcontent=($hub_idarr['hbcomment']!='')?$hub_idarr['hbcomment']:$hub_idarr['hub_title'];
	     // send email parameters start
		 $subject = "I have disliked your ".$posttype."";
		 
		  $mailinnermessage='<p><strong>Subject:</strong> I have disliked your '.$posttype.'. The details are as follows :</p><p><strong>Title:</strong> '.$postcontent.'</p>';
		 
        if($res==1)
		{
			
			$posted_id = getHubById($hid);
			/* $data1['whose_profile_viewed'] = $hub_idarr['mid'];
			$data1['who_viewed'] = $mid;
			$data1['is_seen'] = '0';
			$data1['noti_type'] = 'hub_unlike';
			$data1['title'] = 'I have unliked your post';
			$cur_date = date('Y-m-d H:i:s');
			$this->model_member->visited_profile($data1); */
			
			
			// unread message notification to user
			/* $message['msg_sub'] = "Your post has has been unliked";
			$message['msg_body'] = $postcontent;
			$msgid = $this->model_message->insertMessage($message);
			$d['sender_id'] = $mid;
			$d['receiver_id'] = $hub_idarr['mid'];
			$d['msg_id'] = $msgid;
			//print_r($d);die();  
			$res = $this->model_member->insertMessageTrans($d); */
			// unread message notification to user
			
			//$this->FnSendnotificationToUsernotification($subject,$emailsent_to,$senderfullname,$mailinnermessage); // send email function to user 
		 	$result_data['response'] = "success";
		}
            
        else
		{
			$result_data['response'] = "false";
		}
            
        echo json_encode($result_data);
        exit;
    }
	
	 function sentmail($mail, $mailBody, $subject, $from) {



        $this->load->library('email');

        $this->email->from($from, 'Arlians');

        $this->email->to($mail);

        $this->email->set_mailtype('html');



        $this->email->subject($subject);



        $this->email->message($mailBody);

        //echo 'hello';die(); 

        $this->email->send();

        return true;
    }



     function userPostLike()
    {
      $hid = $this->input->post('hub_id');
      $data['user_email'] = $this->input->post('user_email');        
      $result = $this->model_member->getIdByUserEmail($data);
      $mid = $result->mid;
      $result_data = array(); 
      $posted_id = getHubById($hid);
      $data1['whose_profile_viewed'] = $posted_id[0]["mid"];
      $data1['who_viewed'] = $mid;
      $data1['is_seen'] = '0';
      $data1['noti_type'] = 'hub_like';
      $data1['title'] = 'I have liked your post';
	  $cur_date = date('Y-m-d H:i:s');
      $this->model_member->visited_profile($data1);
      $res = $this->model_hub->postLike($hid,$mid,$cur_date);
      $this->hubEditTime($hid,$cur_date);
	  
	  // send email parameters start
	  $senderresultarr = $this->model_member->getIdDetailsByUserEmail($data);
	  $senderfullname=$senderresultarr['fname'].' '.$senderresultarr['lname'];
	  $currentdate=date("dS \of F Y");
	  
	  $hub_id=$this->input->post('hub_id');
	  $hub_idarr = $this->model_member->getHubuserDetailsById($hub_id);

	  $emailsent_to=$hub_idarr['email']; 
	  
	  $posttype=($hub_idarr['hbcomment']!='')?'Comment':'Post';
	  $postcontent=($hub_idarr['hbcomment']!='')?$hub_idarr['hbcomment']:$hub_idarr['hub_title'];
	  
	   $subject = "I have liked your ".$posttype."";
	   $mailinnermessage='<p><strong>Subject:</strong> I have liked your '.$posttype.'. The details are as follows :</p><p><strong>Title:</strong> '.$postcontent.'</p>';
	  // $subject = "Your post has received a like";
	  // send email parameters end
      if($res==1)
	  {
		// unread message notification to user
		 /* $message['msg_sub'] = "Your post has received a like";
		$message['msg_body'] = $postcontent;
        $msgid = $this->model_message->insertMessage($message);
		$d['sender_id'] = $mid;
        $d['receiver_id'] = $hub_idarr['mid'];
        $d['msg_id'] = $msgid;
		//print_r($d);die();  
        $res = $this->model_member->insertMessageTrans($d);  */
		// unread message notification to user
		
		//$this->FnSendnotificationToUsernotification($subject,$emailsent_to,$senderfullname,$mailinnermessage); // send email function to user  
		$result_data['response'] = "success";
	  }
          
      else
	  {
		  $result_data['response'] = "false";
	  }
          
      echo json_encode($result_data);
      exit;
  	}

    //Hub Report
    function hubReport()
    {           

      $result_data = $user = $data = array(); 

      $user['user_email'] = $this->input->post('user_email');        
      $result = $this->model_member->getIdByUserEmail($user);
      $mid = $result->mid;

      $data['hub_id']       = $this->input->post('hub_id');
      $data['mid']          = $mid;               
      //$data['reports']      = $this->input->post('reports');
      $data['reports']      = '';
      $data['reports_date'] = date('Y-m-d H:i:s');      
      
      $res = $this->model_hub->addHubReport($data);
 
      if($res)
          $result_data['response'] = "success";
      else
          $result_data['response'] = "false";
 
      echo json_encode($result_data);
      exit;
    
    }

    //Hub Delete
    function hubDelete()
    {         

      $result_data = array();
      $hid = $this->input->post('hub_id');
      
      $data['user_email'] = $this->input->post('user_email');        
      $result = $this->model_member->getIdByUserEmail($data);
      $mid = $result->mid;

      $res = $this->model_hub->hubDeleteByOwner($hid, $mid);
 
      if($res==1)
          $result_data['response'] = "success";
      else
          $result_data['response'] = "false";
 
      echo json_encode($result_data);
      exit;
    
    }  

    function hubNewsComments()
    {     
      $data['user_email'] = $this->input->post('user_email');        
      $result = $this->model_member->getIdByUserEmail($data);
      $mid = $result->mid;

      $res['hub_id'] = 0;
      $res['mid'] = $mid; 
      $res['comments'] = $this->input->post('comment');   
      $res['comm_date'] = date('Y-m-d H:i:s');
      $res['hub_news_id'] = $this->input->post('newsId'); 
    	$res_id = $this->model_hub->insertHubNewsComments($res);
     if($res_id > 0){
          $result_data = array('id' =>$res_id,'comment'=>checkURLfromString($res['comments']),'response'=>"success");
      }else{
          $result_data= array('response'=> "false");
	  }
      echo json_encode($result_data);
      exit;
    } 

    function hubNewsLike()
    {     
      $data['user_email'] = $this->input->post('user_email');        
      $result = $this->model_member->getIdByUserEmail($data);
      $mid = $result->mid;

      $res['hub_id'] = 0;
      $res['mid'] = $mid; 
      $res['news_id'] = $this->input->post('newsId'); 

      $result = $this->model_hub->insertHubNewsLike($res);

      if($result==1)
          $result_data['response'] = "success";
      else
          $result_data['response'] = "false";
 
      echo json_encode($result_data);
      exit;
    } 

    function saveNewsAsHub()
    {
      $data['user_email'] = $this->input->post('user_email');        
      $result = $this->model_member->getIdByUserEmail($data);
      $mid = $result->mid;
	  $hbcomment=$this->input->post('hbcomment');
		if($hbcomment!='' || $hbcomment!=null){
			$hbcomment=$hbcomment;
			
		}else{
			
			$hbcomment=null;
		}
      $res['mid'] = $mid;
      $res['hub_title'] = $this->input->post('hub_title'); 
      $res['type_of_post'] = $this->input->post('type_of_post'); 
      $res['content'] = $this->input->post('content'); 
	  $res['hbcomment'] = $hbcomment;	  
      $res['news_link'] = $this->input->post('news_link');
      $res['link'] = $this->input->post('link');
      $res['shared_for'] = '1';
	  $res['provider'] = $this->input->post('source');  
	  $res['shared_bing'] = 'bing'; 
	  $res['hub_share_type'] = 'SHARE'.$this->input->post('POSTFLAG');  
	  

      $last_hub_id = $this->model_hub->post_hub($res); 

      if($last_hub_id != 0)
      {
        //---------Insert into hub share----------------

        $hub_share['owner_mid'] = 0;
        $hub_share['mid'] = $mid;
        $hub_share['hub_id_old'] = 0;
        $hub_share['hub_id_new'] = $last_hub_id;
        $this->model_hub->post_hubShare($hub_share);

        //----------------------------------------------

        $res1['hub_id'] = $last_hub_id;
        $res1['news_id'] = $this->input->post('news_id');

        $result1 = $this->model_hub->insertHubRelation($res1);
		//print_r($result);
		
		
		//-----------------Insert in Hub Top Details-----------------
		
		$tw_digit_codes = $this->model_hub->getTopLineFromMid($mid);

		$top_line_arr = array();
		// topline code from twenty digit code 
		foreach($tw_digit_codes as $code){
			$cd = $code['mem_code'];
			$top_line_arr[] = substr($cd,0,5);
		}
		
		$res2['hub_id'] = $last_hub_id;
		$res2['topline_code'] = $top_line_arr[0];
		
		$res = $this->model_hub->hubPostDetails($res2);
		
		//-----------------------------------------------------------

        if($result1>0){
            $result_data['response'] = "success";

             $result_data['hid'] = $res;


           // $result_data['hid'] = $res;
			$hub_ids = array($last_hub_id);
			$hub_Details=$this->currentHubDetails($hub_ids,$mid); 
			$result_data['hub_details'] = $hub_Details[0];
            $result_data['hub_details']['POSTFLAG'] = $hub_Details[0]['hub_share_type'];			
        }else{
            $result_data['response'] = "false";
		}
        echo json_encode($result_data);
        exit;
      }

    }

	function dislikeNews()
	{
		$data['user_email'] = $this->input->post('user_email');        
		$result = $this->model_member->getIdByUserEmail($data);
		$mid = $result->mid;
		
		$newsId = $this->input->post('news_id'); 
		
		$res = $this->model_hub->dislikeNews($mid,$newsId);
		
		$result_data = array();
		if($res==1)
            $result_data['response'] = "success";
        else
            $result_data['response'] = "false";
   
        echo json_encode($result_data);
        exit;
	}
	
	function newsBlockFromUser(){
		$data['user_email'] = $this->input->post('user_email');        
		$result = $this->model_member->getIdByUserEmail($data);
		$mid = $result->mid;
		$newsId = $this->input->post('news_id'); 
		$res = $this->model_hub->newsBlockFromUser($mid,$newsId);
		
		$result_data = array();
		if($res >0 )
            $result_data['response'] = "success";
        else
            $result_data['response'] = "false";
   
        echo json_encode($result_data);
        exit;
		
		
	}
	
	public function hubEditTime($hid,$posted_on){
    	$data['posted_on'] = $posted_on;
    	$id = $hid;    	
    	$result = $this->model_hub->post_hub_edit($id,$data);
    	return $result;
    	
    }

    public function getUserNameOutsideConnects($hid,$user_id,$cur_date)
    {
      $data = $this->model_hub->getUserNameOutsideConnects($hid,$cur_date);
	   if(count($data)>0){
			  $mid  = $data[0]['mid'];
			  $name;
			  $details = array();
			 /*  if(isset($mid))
			  {
				$res = $this->model_suggestion->is_connected($user_id,$mid);
				if($res == 1)
				{
				  $details['name'] = getMemberbusinName($mid);
				}
				else
				{
				  $details['name']= getMemberbusinName($mid);
				}
			  } */
			  $details['name'] = getMemberbusinName($mid);
			  $details['liked_mid']= $mid;
			  $details['liked_profile_img']= getProfileImage($mid);
			  $details['liked_profile_logo']= logobussinessname($mid);
	   }
      //var_dump($name);
      return $details;
    }
	
	public function getUserNameOutsideConnectsComment($hid,$user_id,$cur_date)
    {
      $data = $this->model_hub->getUserNameOutsideConnectsComment($hid,$cur_date);
	  if(count($data)>0){
		  $mid  = $data[0]['mid'];
     	  $details = array();
      /* if(isset($mid))
      {
        $res = $this->model_suggestion->is_connected($user_id,$mid);
        if($res == 1)
        {
          $details['name'] = getMemberbusinName($mid);
        }
        else
        {
          $details['name'] = getMemberbusinName($mid);
        }
      } */
	    $details['name'] = getMemberbusinName($mid);
		$details['comment_mid']= $mid;
		$details['comment_profile_img']= getProfileImage($mid);
		$details['comment_profile_logo']= logobussinessname($mid);  
	  }
     return $details;
    }


//  function to block the hub user.

public function FnBlockHubUser()
{

  $data['user_email'] = $this->input->post('user_email');        
    $result = $this->model_member->getIdByUserEmail($data);
    $login_id = $result->mid;
    $hub_owner_id=trim($_POST['hub_owner_id']);
  
  $inserthubblock = $this->model_hub->FnBlockHubUser($login_id,$hub_owner_id);
  if($inserthubblock>0)
  {
     $res['responce'] = 'success';
  }
  else
  {
     $res['responce'] = 'fail';
  }
  echo json_encode($res); exit;
  

}


function FnSendnotificationToUsernotification($subject,$emailsent_to,$senderfullname,$mailinnermessage)
{
	$currentdate=date("dS \of F Y");
	$mailBody = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Arlians</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="keywords"  content="" />
	<meta name="Resource-type" content="Document" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
	
  <link href="fonts/fonts.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">

  <style type="text/css">
  * { box-sizing: border-box;}
   body{margin: 0; font-family: Arial; font-size: 13px; line-height: 20px; color:#515151;}
   ::-moz-placeholder{ color: #357cb7; opacity: 1;}
   .main-body{ width: 352px;}
   .responsive_main{padding: 35px 27px 27px;}

    @media screen and (max-width: 767px) {
    .main-body{ width: 90%;} 
    .responsive_main{padding: 35px 15px 27px;}  
   }

  </style>  
	
</head>
<body>
 
 <table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-color:#eeebeb; padding: 88px 0;">
    <tr>
      <td>
         <table border="0" cellpadding="0" cellspacing="0" class="main-body" style="background-color:#fff; width:352px; margin: 0 auto;">
           <tr>
             <td class="responsive_main">
               <table width="100%" border="0" cellpadding="0" cellspacing="0">
                 <tr>
                   <td style="text-align:center;">
                     <span>'.$currentdate.'</span>
                      <h2 style="font-size: 43px; margin: 30px 0; line-height:46px;">Notification...</h2>
                   </td>
                 </tr>
                 <tr>
                   <td style="background-color: #357cb7; color:#fff; border-radius: 10px; -moz-border-radius: 10px; -webkit-border-radius: 10px; padding: 15px; text-align:center;">
                      <span style="font-size: 24px; margin-bottom: 15px; display: block;">... from '.$senderfullname.'</span>'.$mailinnermessage.'
	  
                   <br/>
                   <img src="'.base_url().'image/noti-logo.png" alt="">
                   

                   </td>
                 </tr>
               </table>
             </td>
           </tr>
         </table>
      </td>
    </tr>

 </table>

</body>
</html>
';

        $from = "notification@arlians.com";
		$this->sentmail($emailsent_to, $mailBody, $subject, $from);
	
}


function moreHubApi(){
       		$data['user_email'] = $this->input->post('user_emai');
			$result = $this->model_member->getIdByUserEmail($data);
			$user_id = $result->mid; 
			$mid=$user_id; 
			$start=0;
			$limit = 2;
			$poststart=$this->input->post('start');
			if($poststart)
            {
				$start = ($poststart*$limit);
			}
			$checking_sector = $this->getHubx($mid,$start,$limit);
			$hub_ids_connects = $this->getHubViewApi($mid,$start,$limit,$checking_sector);
			$hub_ids=array();
			if(count($hub_ids_connects)>0)
			{
				foreach($hub_ids_connects as $ids)
				{
					foreach($ids as $id)
					{
						//print_r($id);
						//echo $id;
							$hub_ids[] = $id;
							
					}
					
				}
			
			}	
			     
           /*  $hub_ids_matches = $this->getHubx($mid,$start,$limit);
			
			if(count($hub_ids_matches)>0)
			{
				foreach($hub_ids_matches as $id)
				{
					if($id != "")
					{
						$hub_ids[] = $id['hub_id'];
						$hub_idmatches[] = $id['hub_id'];
					}
				}
			}
			       */ 
            $hub_ids = array_unique($hub_ids);
			//print_r($hub_ids);die();
			$hub_data = array();
			if(count($hub_ids)>0)
			{   
				 foreach($hub_ids as $hid)
				 {
					$hubs = $this->model_hub->getHubById($hid);
					if($hubs != NULL)
					 {
						$key = $hubs[0]['posted_on'];
						$hub_data[strtotime($key)] = $hubs[0];
				
						$hub_data[strtotime($key)]['timestamp'] = strtotime($hubs[0]['posted_on']);
						$hub_title = explode(',',$hubs[0]['tag_id']);
						$hubTitle = array();
						foreach($hub_title as $hb){

						  $title = getTagNameById($hb);
						  $hubTitle[$hb] = $title[0]['tag_name'];
						}
						$hub_data[strtotime($key)]['tag_title'] = $hubTitle;  
					  }                
				 }
				 
				foreach ($hub_data as $key => $value) 
				{

				  $hud_ind_id = $value['hid'];
				  $hud_post_time = calculateTimeFromPost($value['posted_on'], date("Y-m-d H:i:s"), "s");
				  $like = $this->model_hub->checkHubLike($hud_ind_id,$mid);
				  $comments = $this->model_hub->getComments($hud_ind_id);
				  $whoseShareid = $this->model_hub->whoseShare($mid,$hud_ind_id);				  $hub_data[$key]['sharetagline'] = $this->model_memberprofile->FnGetUserTagLine($whoseShareid); 
				  $hub_data[$key]['tagline']='';
				  $hub_data[$key]['hub_post_time'] = calculateTimeFromPost($hub_data[$key]['posted_on'], date("Y-m-d H:i:s"), "s");
				  if($value['shared_for']==4)
				  {
					 $hub_data[$key]['tagline'] = $this->model_memberprofile->FnGetUserTagLine($value['mid']); 
				  }
				  $userProfType=userProfType($mid);
					if($userProfType=='Buisness'){
						$whose_share=getMemberbusinName($whoseShareid);
					}else{
						
						$whose_share=getMemberName($whoseShareid);
					}
										
				  $hub_data[$key]['likeconnectedtouser']= $this->model_hub->FnUserConnectedtolikeduser($value['hid'],$user_id,$value['mid']);
				  $hub_data[$key]['likedpersondetails']= $this->model_hub->FnLikednotconnectedperson($value['hid'],$user_id,$value['mid']);
				  $hub_data[$key]['commentconnectedtouser']= $this->model_hub->FnUserConnectedtoCommentuser($value['hid'],$user_id,$value['mid']);
				  $hub_data[$key]['commentpersondetails']= $this->model_hub->FnCommentnotconnectedperson($value['hid'],$user_id,$value['mid']);
				  
				  
				 // $hub_data[$key]['shareconnectedtouser']= $this->model_hub->FnUserConnectedtoshareuser($value['hid'],$user_id,$value['mid']);
				  $hub_data[$key]['POSTFLAG'] = $value['hub_share_type'];
				  $hub_data[$key]['whose_share'] = $whose_share;
				  $hub_data[$key]['whose_share_id'] = $whoseShareid;
				  $hub_data[$key]['whose_share_logoname'] = logobussinessname($whoseShareid);
				  $hub_data[$key]['whose_share_prof_img'] = getProfileImage($whoseShareid);
				  $hub_data[$key]['whose_share_prof_img_path']= base_url().'uploads/profile_image/';
				  $hub_data[$key]['not_in_network_like'] = $this->getUserNameOutsideConnects($hud_ind_id,$mid,$hud_post_time);
				  $hub_data[$key]['not_in_network_comment'] = $this->getUserNameOutsideConnectsComment($hud_ind_id,$mid,$hud_post_time);
				  $hub_data[$key]['comments'] = $comments;
				  $hub_data[$key]['shared_bing'] = $value['shared_bing'];
				  $hub_data[$key]['share_count'] = $this->model_hub->getTotalShare($hud_ind_id);
				  foreach($comments as $k=>$val)
				  {
					$commented_user_details = getParticularUserDetails($val['mid']);
					$com_name = $commented_user_details['name'];
					$com_name_initial = logobussinessname($val['mid']);
					$bussinessname_com = getMemberbusinName($val['mid']);
					if($bussinessname_com=='')
                    {
						$bussinessname_com= getMemberName($val['mid']);
					}
				   	$hub_data[$key]['comments'][$k]['buisnesname'] = $bussinessname_com;
					$hub_data[$key]['comments'][$k]['com_initial_name'] = $com_name_initial;
					$hub_data[$key]['comments'][$k]['name'] = getMemberName($val['mid']); 
					$hub_data[$key]['comments'][$k]['image_path'] = base_url().'uploads/profile_image/';
					$hub_data[$key]['comments'][$k]['comment_post_time'] = calculateTimeFromPost($val['comment_date'], date("Y-m-d H:i:s"), "s");
					$comprof_img=getProfileImage($val['mid']);
					if($comprof_img!='')
					{
					  $hub_data[$key]['comments'][$k]['comment_prof_img'] =  base_url().'uploads/profile_image/'.$comprof_img;  
					}else
					{
						$hub_data[$key]['comments'][$k]['comment_prof_img'] =  '';  
				    }	
					$likedata=array('hub_comment_id'=>$val['comm_id']);
					$hub_data[$key]['comments'][$k]['total_comment_like'] = $this->model_hub->FnTotalCommentCount($likedata);
					$userlikedata=array('hub_comment_id'=>$val['comm_id'],'mid'=>$user_id);
					$hub_data[$key]['comments'][$k]['user_comment_like_status']=$this->model_hub->Fnuser_comment_like_status($userlikedata);
				  }
				  //-----------
				  
				 //if($value['mid']!=''){
				  $prof_img=getProfileImage($value['mid']);
				  $this->db->select('*');
				  $this->db->from('ar_request_access_page');
				  $this->db->where('mid',$value['mid']);
				  $query = $this->db->get();
				  $rsb = $query->result_array(); 
				  $bussinessname = $rsb[0]['bussinessname'];
				  if($bussinessname=='')
				  {
					$bussinessname=getMemberName($value['mid']);
				  }
       			  $postHubMembers = $this->model_member->getUserById($value['mid']);
				  $em = logobussinessname($value['mid']);
				  $profile_initional = $em; 
    			  $data_for_connection['mid'] = $mid;
				  $data_for_connection['connected_id'] = $value['mid'];
				  $is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection);         
				  $hub_data[$key]['is_connected'] = $is_connected;
				  $likes = $this->model_hub->getTotalLikes($hud_ind_id);
				  $total_share = $this->model_hub->getTotalShare($hud_ind_id);
				  if($prof_img!='')
				  {
				  $hub_data[$key]['prof_img'] =  base_url().'uploads/profile_image/'.$prof_img;  
				  }
				  else
				  {
					   $hub_data[$key]['prof_img'] =  '';  
				  }			  
			      $hub_data[$key]['profile_initional'] = $profile_initional;  
			 	  $hub_data[$key]['bussinessname'] = $bussinessname;     
				  $hub_data[$key]['name'] = getMemberName($mid);          
				  $hub_data[$key]['likes'] = $likes; 
				  $hub_data[$key]['total_share'] = $total_share; 
				  $hub_data[$key]['like'] = $like;
				  $hub_data[$key]['type'] = 'hub';  
				  $hub_data[$key]['show_comments'] = false; 
				  $hub_data[$key]['comment'] = '';			  
				  $hub_data[$key]['open_comment_box'] = false;			  
				  $hub_data[$key]['comment_count'] = (int) count($comments);			  
				  $hub_data[$key]['base_url'] = base_url().'uploads/hub_images/';                
				} 
            
			}
			          
           $data_api['hubs'] = $hub_data;
           include_once('memberprofile.php');
           $bing = new memberprofile();
		   $nstart = $poststart;
		   $nlimit = 1;
		   if(count($hub_ids)<2)
		   {
			  $currenthubcount=count($hub_ids);
			  $nlimit = 5-$currenthubcount; 
			  $nstart = ($poststart*$nlimit);
		   }
		   $data_api['bing_news'] = $bing->fetchBingLalNews($mid,$nstart,$nlimit);
	       $api = array();
           if(!empty($data_api['hubs']))
           {
				foreach($data_api['hubs'] as $hbs)
				{
				  array_push($api,$hbs);
				}
		   }
		   if(count($data_api['bing_news'])>0)
		   {
			 foreach($data_api['bing_news'] as $newskey=>$newsarrbing)
			 {
				array_push($api,$newsarrbing); 
			 }	 
		   }
           $api = array_filter($api);
           $api = array_map("unserialize", array_unique(array_map("serialize", $api)));
		   echo json_encode($api);
		   exit;
  
    }
	
	

	
	function currentHubDetails($hub_ids,$mid){
			  $hub_ids = array_unique($hub_ids);
           	//print_r($hub_ids);die();

            $hub_data = array();
			if(count($hub_ids)>0)
			{
				 foreach($hub_ids as $hid)
				 {
					$hubs = $this->model_hub->getHubById($hid);
					//echo '<pre>'; var_dump($hubs); die();
					 if($hubs != NULL)
					 {
						$key = 'hub_Details';
						$hub_data[strtotime($key)] = $hubs[0];
				
						$hub_data[strtotime($key)]['timestamp'] = strtotime($hubs[0]['posted_on']);
						$hub_title = explode(',',$hubs[0]['tag_id']);
						$hubTitle = array();
						foreach($hub_title as $hb){

						  $title = getTagNameById($hb);
						  $hubTitle[$hb] = $title[0]['tag_name'];
						}
						$hub_data[strtotime($key)]['tag_title'] = $hubTitle; 
					  }                
				 }
				 krsort($hub_data);
                // print_r($hub_data);die();
				foreach ($hub_data as $key => $value) 
				{
//print_r($value);
				  $hud_ind_id = $value['hid'];
				  $hud_post_time = $value['posted_on'];
				  $like = $this->model_hub->checkHubLike($hud_ind_id,$mid);
				  $comments = $this->model_hub->getComments($hud_ind_id);
				  //$whoseShare = $this->model_hub->whoseShare($mid,$hud_ind_id,$hud_post_time);
				  $whoseShare = $this->model_hub->whoseShare($mid,$hud_ind_id);
				//  var_dump($whoseShare);
				  $userProfType=userProfType($mid);
					if($userProfType=='Buisness'){
						$whose_share=getMemberbusinName($whoseShare);
					}else{
						
						$whose_share=getMemberName($whoseShare);
					}
				  $hub_data[$key]['whose_share'] = $whose_share;
				  $hub_data[$key]['whose_share_id'] = $whoseShare;
				  $hub_data[$key]['whose_share_logoname'] = logobussinessname($whoseShare);
				  $hub_data[$key]['whose_share_prof_img'] = getProfileImage($whoseShare);
				  $hub_data[$key]['whose_share_prof_img_path']= base_url().'uploads/profile_image/';
					
				 // echo '<pre>'; var_dump($whose_share); 
				  $hub_data[$key]['not_in_network_like'] = $this->getUserNameOutsideConnects($hud_ind_id,$mid,$hud_post_time);
				  $hub_data[$key]['not_in_network_comment'] = $this->getUserNameOutsideConnectsComment($hud_ind_id,$mid,$hud_post_time);
				  

				  $hub_data[$key]['comments'] = $comments;
				  $hub_data[$key]['share_count'] = $this->model_hub->getTotalShare($hud_ind_id);
				  
				  foreach($comments as $k=>$val)
				  {
					
					//echo '<pre>'; var_dump($val);
					$commented_user_details = getParticularUserDetails($val['mid']);
					$com_name = $commented_user_details['name'];
					$com_name_initial = logobussinessname($val['mid']);
					 $bussinessname_com = $val['bussinessname'];
				   if($bussinessname_com==''){
					   
					   $bussinessname_com=getMemberName($val['mid']);
				   }
				   
					$hub_data[$key]['comments'][$k]['buisnesname'] = $bussinessname_com;
					$hub_data[$key]['comments'][$k]['com_initial_name'] = $com_name_initial;
					$hub_data[$key]['comments'][$k]['name'] = getMemberName($val['mid']); 
					$hub_data[$key]['comments'][$k]['image_path'] = base_url().'uploads/profile_image/';
					 $hub_data[$key]['comments'][$k]['hub_post_time'] = calculateTimeFromPost($hub_data[$key]['posted_on'], date("Y-m-d H:i:s"), "s");
					
				  }
				  //-----------
				  
				 //if($value['mid']!=''){
				  $prof_img=getProfileImage($value['mid']);
				  //---------
				   $this->db->select('*');
					$this->db->from('ar_request_access_page');
					
					$this->db->where('mid',$value['mid']);
					
					
					$query = $this->db->get();
					$rsb = $query->result_array(); 
				   $bussinessname = $rsb[0]['bussinessname'];
				   if($bussinessname==''){
					   
					   $bussinessname=getMemberName($value['mid']);
				   }

					$postHubMembers = $this->model_member->getUserById($value['mid']);
					//echo '<pre>'; var_dump($postHubMembers);
					$em = logobussinessname($value['mid']);
					
					$profile_initional = $em; 


				  $data_for_connection['mid'] = $mid;
				  $data_for_connection['connected_id'] = $value['mid'];
				
				
				  $is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection);         //echo '<pre>'; print_r($comments);
				  $hub_data[$key]['is_connected'] = $is_connected;
				  $likes = $this->model_hub->getTotalLikes($hud_ind_id);
				   $total_share = $this->model_hub->getTotalShare($hud_ind_id);
				  if($prof_img!=''){
				  $hub_data[$key]['prof_img'] =  base_url().'uploads/profile_image/'.$prof_img;  
				  }else{
					   $hub_data[$key]['prof_img'] =  '';  
				  }			  
			  $hub_data[$key]['profile_initional'] = $profile_initional;  
			  // var_dump($hub_data[$key]['posted_on']);
			  $hub_data[$key]['hub_post_time'] = calculateTimeFromPost($hub_data[$key]['posted_on'], date("Y-m-d H:i:s"), "s");
			  $hub_data[$key]['bussinessname'] = $bussinessname;     
			  $hub_data[$key]['name'] = getMemberName($mid);          
			  $hub_data[$key]['likes'] = $likes; 
			  $hub_data[$key]['total_share'] = $total_share; 
			  $hub_data[$key]['like'] = $like;
			  $hub_data[$key]['type'] = 'hub';  
			  $hub_data[$key]['show_comments'] = false; 
			  $hub_data[$key]['comment'] = '';			  
			  $hub_data[$key]['open_comment_box'] = false;			  
			  $hub_data[$key]['comment_count'] = count($comments);			  
			  $hub_data[$key]['base_url'] = base_url().'uploads/hub_images/';                
				} 
            
			}
			return $hub_data;
			}

			
	
function getHubViewApi($id,$start,$limit,$checking_sector)
	{
       // print_r($checking_sector);die();
		$myMid = $id;
		$mids=array();
		
		$connection_list=array();
		$connection_list = $this->model_hub->myConnectionList($myMid);
		
		if(count($connection_list)>0)
		{
			foreach($connection_list as $con_list)
			{
				if($con_list->mid == $myMid)
				{
					$mids[]=$con_list->connected_id;
				}
				elseif($con_list->connected_id ==$myMid)
				{
					$mids[]=$con_list->mid;					
				}
			}
		}	
		//print_r($mids);die();
    $blockedusersids = $this->model_hub->FnFindBlockedHubUserById($myMid); // user blocked for the user
	
	$my_hub = array();
    if(count($mids) > 0)
    {
	  $allhids=array();
	 
      array_push($mids,$myMid);
      if(count($blockedusersids)>0)
      {
          $mids=array_diff($mids, $blockedusersids);   
      }
	  $likeids = $this->model_hub->FnFindLikesHidsInConnection($mids,$start,$limit);
	  $commenthids = $this->model_hub->FnFindCommentsHidsInConnection($mids,$start,$limit);
	  $allhids=array_merge($likeids,$commenthids);
	  if(count($allhids)>0)
	  {
		 arsort($allhids);
		 $uniquehids=array_unique($allhids); 
	  }
     // print_r($uniquehids);
	  $my_hub[] = $this->model_hub->hubPostedByMembertestlal($mids,$start,$limit,$uniquehids,$checking_sector); 
     
    }
	  
		return $my_hub;
	}
	
	
	public function FnCommentLike()
	{
		$data['hub_id']=$this->input->post('hub_id');
	    $data['hub_comment_id']=$this->input->post('comm_id');//$_POST['hub_comment_id'];
		$data['mid']=$this->input->post('mid');//$_POST['m_id'];
				
		if($_POST['hub_id']==null)
		{
			$data['news_id']=$this->input->post('news_id');//$_POST['news_id'];
			$data['hub_id']=null; 
		}
		
		$data['like_date']=date('Y-m-d h:i:s');
      	$last_insert_id = $this->model_hub->FnLikeHubComment($data); 
		if($last_insert_id>0)
		{
			$commentlikecount=$this->model_hub->FnTotalCommentCount($data);
			$res = array('response'=>'success','comment_like_count'=>$commentlikecount,'commentlikestatus'=>1);
		
		}else{
			
			$res = array('response'=>'fail');
		}
		echo json_encode($res);		
		exit;
	}
	
	public function FnCommentDisLike()
	{
		
		$data['hub_id']=$this->input->post('hub_id');
	    $data['hub_comment_id']=$this->input->post('comm_id');
		$data['mid']=$this->input->post('mid');
		if($_POST['hub_id']==null)
		{
			$data['news_id']=$this->input->post('news_id');
			$data['hub_id']=null; 
		}
		$delres = $this->model_hub->FnDislikeLikeHubComment($data); 
		$commentlikecount=$this->model_hub->FnTotalCommentCount($data);
		$res = array('response'=>'success','comment_like_count'=>$commentlikecount,'commentlikestatus'=>0);
		echo json_encode($res);		
		exit;
	}
	
	public function FnDeleteComment()
	{
		
		$comment_id=$this->input->post('comm_id');
	    $delres = $this->model_hub->FnDeleteCommentandCommentLike($comment_id); 
		$res = array('response'=>'success');
		echo json_encode($res);		
		exit;
	}
	
	


}