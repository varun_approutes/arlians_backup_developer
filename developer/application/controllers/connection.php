<?php

class Connection extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('model_common');
        $this->load->model('model_member');
        $this->load->model('model_hub');
        $this->load->model('model_message');
        $this->load->library('User_Manager');
        $this->load->model('model_suggestion');
		 $this->load->model('model_memberprofile');
        $this->load->helper('common_helper');
        $this->load->helper('text');
        $this->model_common->SiteSettingsDetails(); // This is mandatory
        $this->load->model('model_othermembersprofile');
        $this->load->model('model_category');
        $this->user_manager = User_Manager::get_instance();
        $this->load->helper('load_controller');
    }

    function index() {
        $this->load->view('member/connectionlist');
        
    }

   
	function connectionList(){
		//$d['user_email'] = 'arlians2015@gmail.com';
		$d['user_email'] = $this->input->post('user_email');
        $res = $this->model_member->getIdByUserEmail($d);
        $user_id = $res->mid;
        $connect = $this->model_memberprofile->getConnectedMemberListAll($user_id);
		//print_r($connect);
        $cont = array();
		
        for($i=0;$i<=count($connect)-1;$i++) {
            $cont[$i]['bussiness_name'] = getMemberbusinName($connect[$i]);
            $em = logobussinessname($connect[$i]);
      	    $cont[$i]['logobussinessname'] = $em; 
            $cont[$i]['image_basepath'] = base_url() . 'uploads/profile_image/';
            $cont[$i]['mid'] = $connect[$i];
            $cont[$i]['back_image'] = $this->model_member->BackImage($connect[$i]);
            $cont[$i]['profile_image'] = $this->model_member->ProfileImage($connect[$i]);
            $cont[$i]['location'] = $this->model_member->userLocation($connect[$i]);
        }
        $result['connection'] = $cont;
        //$result['pending'] = $this->model_memberprofile->getNetworkConnectionRecievedList($user_id);
       // $result['recieved'] = $this->model_memberprofile->getConnectedMemberListAllre($user_id);
        echo json_encode($result);
        exit;
		//$this->load->view('member/connectionlist');
		
	}
	function connectionListPage(){
		$this->load->view('member/connectionlist');
		
	}
	
}
