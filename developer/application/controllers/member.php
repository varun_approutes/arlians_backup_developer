<?php

class Member extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('model_common');
        $this->load->model('model_member');
        $this->load->model('model_hub');
        $this->load->model('model_message');
        $this->load->library('User_Manager');
        $this->load->model('model_suggestion');
		 $this->load->model('model_memberprofile');
        $this->load->helper('common_helper');
        $this->load->helper('text');
        $this->model_common->SiteSettingsDetails(); // This is mandatory
        $this->load->model('model_othermembersprofile');
        $this->load->model('model_category');
        $this->user_manager = User_Manager::get_instance();
        $this->load->helper('load_controller');
    }

    function index() {
        if (count($_POST) > 0) {
            $this->load->model('model_member');
            $id = $this->model_member->insertmemberdata($_POST['data']);
            if ($id != 'FALSE') {
                //=========================Welcome Mail==========================
                $mail = $_POST['data']['hemail'];
                //$mailBody = "Hi, welcome to Arlians";

 $mailBody = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Arlians</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="keywords"  content="" />
	<meta name="Resource-type" content="Document" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Oxygen:400,700" rel="stylesheet" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet" type="text/css">
	
 <style type="text/css">
  * { box-sizing: border-box;}
   body{margin: 0; font-family: Arial; font-size: 13px; line-height: 20px; color:#515151;}
   ::-moz-placeholder{ color: #357cb7; opacity: 1;}
    .main-body{ width: 352px;}

    @media screen and (max-width: 767px) {
    .main-body{ width: 90%;}   
   }

  </style>  
	

</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image: url('.base_url().'email-template/images/email-bg02.jpg); background-repeat: no-repeat; background-position: center top;  background-size: cover;  padding: 88px 0;">
    <tr>
      <td style="text-align:center;">
        <h1 style="font-family: \'source_sans_prolight\', Arial; font-size: 67px; color: #fff; margin: 0; line-height:67px;">Hello!</h1>
        <span style="display: block; margin: 30px 0; text-transform: uppercase; font-size: 22px; color: #fff; line-height:25px;">Welcome to your profile!</span>
      </td>
    </tr>
  <tr>
    <td>
        <table border="0" cellpadding="0" cellspacing="0" class="main-body" style="background: #357cb7; padding: 25px 40px; color: #fff; text-align: center; margin: 0 auto;">
          <tr>
            <td>
               <span style="background: #383838 ; border-radius: 50%;  height: 100px;  margin: 0 auto 20px; width: 100px; display:inline-block;"><img style="width:100%;" src="images/logo-circle.png" alt=""></span>
            </td>
          </tr>  
          <tr>
            <td>
               <h2 style="font-size: 34px; margin: 0 0 20px; line-height: 40px; font-family: \'Source Sans Pro\', sans-serif;font-weight:300;">'.$mail.'</h2>
               <p>Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you  every step of the way.</p>
               <a href="'.base_url().'" style="background-color: #fff ; text-decoration:none; border-radius: 4px; color: #383838; display: inline-block; font-weight: bold;  padding: 15px 40px; font-size: 16px;">Customize Profile</a>
               <!--<p>Refer Arlians <a href="javascript:void(0)" style=" color: #fff; text-decoration: underline;">here</a></p>-->
            </td>
          </tr>  
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" style="text-align:center;  font-size: 13px; margin-top: 30px; text-align: center; color:#fff;">
        <tr>
          <td>
            <img src="'.base_url().'email-template/images/noti-logo.png" alt="">
            <p style="margin-top:10px; margin-bottom:0;">Manage email preferences in your settings</p>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
 
</body>
</html>';



                $subject = "Welcome to Arlians";
                $this->sentmail($mail, $mailBody, $subject);

                //=========================Activation Mail ========================
                //$activationCode = md5($_POST['data']['hemail']);				
                $sess_array = array(
                    'id' => $id,
                    'username' => $this->input->post('hemail'),
                    'status' => '1'
                );
                $this->session->set_userdata('logged_in', $sess_array);
                $subject = "Activation Mail for Arlians";
                //$mailBody = "Hi, please activate your account from this link.<a href='".base_url()."member/setup_wiz_one/". $id."'>Activate Now</a>";

                $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Arlians Welcome</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Thank you for signing up to Arlians.</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;">
                                            <a href="' . base_url() . '">
                                                <img title="Arlians logo" src="' . base_url() . '/assets/images/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Thank you for signing up ' . $mail . '<br />
                                            <br />
                                            <span style="font-size:14px;color:#4D575E;">Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you every step of the way.</span><br /><br />
                                            <span style="color:#00AAA0;"><strong>Spread the word</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">Remember, as the network grows, so do your opportunities. Make the most of Arlians by inviting your customers, suppliers and any other business you\'ve ever worked with.</span>
                                            <br /><br />
                                            <span style="color:#00AAA0"><strong>Be rewarded</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">
                                                You\'ll get the opportunity for exclusive free access to premium features when you recommend someone and they sign up to Arlians. All you have to do to take advantage of this is share your unique code.</span>
                                            <br /><br /> 
                                        </td>
                                    </tr>       
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                    <td align="center" style="background-color:#01A69C;font-family:Calibri, Arial, sans-serif;font-size:16px;color:#fff;line-height:1.2;">
                                    <span style="color:#fff; background-color:transparent; font-family:arial; vertical-align:baseline; white-space:pre-wrap">Your activation link is </span><br /><strong><a style="color:#fff;" href="' . base_url() . 'member/setup_wiz_one/' . $id . '/' . md5($mail) . '" target="_blank">Activate your account</a></strong>
										
										</td>
										</tr>
                                </table>
                               
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright &copy; 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                            Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';
//------------------------ Stopping activation mail------------------
               // $this->sentmail($mail, $mailBody, $subject);
//-------------------------------------------------------------------
                $this->load->view('member/setup_wiz');
            }
        } else {
            $user = $this->user_manager->get_looged_in_user();

            if ($user != null) {
                $this->afterLogin();
            }
            $this->load->view('member/member');
        }
    }

    function afterLogin() {
        $data = $this->user_manager->get_looged_in_user();
        $chk_wiz_one = $this->model_member->data_exists_wiz_one($data['mid']);
        if ($chk_wiz_one == "TRUE" && $data['status'] == 1) {
            redirect('member/dashboard');
        } elseif ($chk_wiz_one == "TRUE" && $data['status'] == 0) {
            $data['error_message'] = 'You are not activated. please active your account from mail link';
            $this->load->view('member/login', $data);
        } else {
            $this->load->view('member/setup_wiz');
        }
    }

    function resendActivationRequestLink() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $result = $this->model_member->getUserById($data['mid']);
        $mail = $result->email;
        $subject = "Activation Mail for Arlians";
        $mailBody = "Hi, please activate your account from this link.<a href='" . base_url() . "member/activateUser/" . $data['mid'] . "'>Activate Now</a>";
        $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Arlians Welcome</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Thank you for signing up to Arlians.</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;">
                                            <a href="' . base_url() . '">
                                                <img title="Arlians logo" src="' . base_url() . '/assets/images/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Thank you for signing up ' . $mail . '<br />
                                            <br />
                                            <span style="font-size:14px;color:#4D575E;">Your journey to finding new sources of growth has just begun. Whatever growth means to you, from getting a new customer to finding a new partner, we promise to be with you every step of the way.</span><br /><br />
                                            <span style="color:#00AAA0;"><strong>Spread the word</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">Remember, as the network grows, so do your opportunities. Make the most of Arlians by inviting your customers, suppliers and any other business you\'ve ever worked with.</span>
                                            <br /><br />
                                            <span style="color:#00AAA0"><strong>Be rewarded</strong></span><br /><br />
                                            <span style="font-size:14px;color:#4D575E;">
                                                You\'ll get the opportunity for exclusive free access to premium features when you recommend someone and they sign up to Arlians. All you have to do to take advantage of this is share your unique code.</span>
                                            <br /><br /> 
                                        </td>
                                    </tr>       
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                    <td align="center" style="background-color:#01A69C;font-family:Calibri, Arial, sans-serif;font-size:16px;color:#fff;line-height:1.2;">
                                    <span style="color:#fff; background-color:transparent; font-family:arial; vertical-align:baseline; white-space:pre-wrap">Your activation link is </span><br /><strong><a style="color:#fff;" href="' . base_url() . 'member/activateUser/' . $data['mid'] . '/' . md5($mail) . '">Activate your account</a></strong>
                                    

                                </table>
                               
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright &copy; 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                            Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';
        $result = $this->sentmail($mail, $mailBody, $subject);
        return $result;
        exit();
        //print_r($result->email); exit;
    }

    function validatemember() {
        $this->load->model('model_member');
        $email = $_POST['data'];
        $res = $this->model_member->membervalidate($email);
        echo $res;
        exit;
    }

    function login() {
        $data = array();

        if (count($_POST) > 0) {
            $this->load->model('model_member');

            $d['email'] = $_POST['data']['email'];
            $d['pass'] = $_POST['data']['pass'];
            $chk = $this->model_member->logincheck($d);

            if ($chk == "TRUE") {
                $data['mid'] = $this->session->userdata['logged_in']['id'];
                $data['status'] = $this->session->userdata['logged_in']['status'];
                $this->user_manager->set_user_token($d['email']);
                $chk_wiz_one = $this->model_member->data_exists_wiz_one($data['mid']);

                if ($chk_wiz_one == "TRUE" && $data['status'] == 1) {
                    redirect('member/dashboard');
                } elseif ($chk_wiz_one == "TRUE" && $data['status'] == 0) {
                    $data['error_message'] = 'You are not activated. please active your account from mail link';
                    $this->load->view('member/login', $data);
                } else {
                    $this->load->view('member/setup_wiz');
                }
            } else {

                $data['error_message'] = 'Inavlid Login Credentials';
                $this->load->view('member/login', $data);
            }
        } else {
            $user = $this->user_manager->get_looged_in_user();
            if ($user != null) {
                $this->afterLogin();
            }
            redirect('explore');
        }
    }
     function dashboard() {
         $this->load->view('member/dashboard');
     }
     
    function dashboardOld() {

        //$user_email = $_REQUEST['email'];
        $data['user_email'] = $this->input->post('user_email');
        //$data['user_email'] = 'rampin.lorenzo+16@gmail.com';
        $result = $this->model_member->getIdByUserEmail($data);
        
        $user_id = $result->mid;

         $this->makeSuggestedListForloginuserDashboard($user_id);

        if ($user_id) {
            //$data['is_wiz_two_empty'] = $this->user_manager->isAlreadyDataInWizTwox($user_id);
            $id = $user_id;
            $data['mid'] = $user_id;

            //for suggestion  
            $sids = $this->model_suggestion->suggestionSearchTypes($id);
            $data = array();
            $searchT = array();
            $type = array();
            foreach ($sids as $sid) {
                $s = explode('-', $sid);
                $searchT[] = $s[0];
                $type[] = $s[1];
            }
            $user = getParticularUserDetails($id);
//echo '<pre>'; var_dump($type);
            $user_own_type = $user['bussinesstype1']; 
            $pos;
            $pos1;
            if ($user_own_type == 'Buisness') {
                if (in_array('3c', $type)) {
                    $pos = array_search('3c', $type);
                    unset($type[$pos]);
                }
                if (in_array('4b', $type)) {
                    $pos1 = array_search('4b', $type);
                     unset($type[$pos1]);
                }
            }
            //echo '<pre>'; var_dump($type);
           
            foreach ($type as $ty) {
                $data['last_suggested_id'][$ty] = $this->model_suggestion->getSuggestionByType($id, $ty);
            }
            $data['looking_for'] = $searchT;
            //end suggestion
            $data['tag'] = $this->model_member->getAllTag();
            //var_dump($data); die();
            //$data['user_data'] = $this->user_manager->get_looged_in_user();
            $chk_sharecode = $this->model_member->shareCodeExist($data['mid']);
            if ($chk_sharecode == 'false') {
                $user_name = getMemberName($user_id);
                $ar_members = md5(date("dmY-his") . $user_name);
                $this->model_member->shareCode($data['mid'], $ar_members);
            }

           // $data['is_wiz_two_empty'] = $this->user_manager->isAlreadyDataInWizTwox($user_id);
            $id = $user_id;
            $data['mid'] = $user_id;
            
            //$data['is_wiz_two_empty']=$this->user_manager->isAlreadyDataInWizTwo();
            
           
            
            $data['suggested_list'] = $this->model_suggestion->suggestionCountbyCustomer($id);
            //echo '<pre>'; var_dump($data); die();
            $data['lookingfor_list'] = $this->model_suggestion->suggestionSearchTypes($id);

            $lookingfor_list = $this->model_suggestion->suggestionSearchTypes($id);
            
           // $data['msg_count'] = $this->message_count($id, 'dash');
            //echo '===='; var_dump($data['msg_count']);die();
           // $data['rec_msg'] = $this->model_message->inbox($id);

            //---------------- Suggestion Count--------------------
            //echo $user_id; die();
            $ids = showOnDashboard($user_id);
            
            //--------If no data is available against the user_id----------------
            
            if(in_array(0,$ids)){
                $data_api['suggestion_count'] = 0;
                $data_api['suggestionCountTypeWise']= 0;
                echo json_encode($data_api);
                exit;
            }
            
            //--------------------------------------------------
            $user = getParticularUserDetails($user_id);

            $user_own_type = $user['bussinesstype1'];
            $arr_key = array();
            $sugg_ids = array();
            $new_member_ids = array();
            foreach($ids as $id){
                $sugg_ids[$id['searchtype']][] = $id['suggested_id'];
            }   
                $arr_key = array_keys($sugg_ids);   
                $ct = 0;
                if(!empty($arr_key)){
                  foreach($arr_key as $key){
                    $ct += count($sugg_ids[$key]);
                  }
                }

            $memArr = array();
            foreach($sugg_ids as $su_id){
                foreach($su_id as $id){
                    $memArr[] = $id;
                }
            }
            foreach($lookingfor_list as  $looking_for_id ){                             
                foreach($memArr as $suggested_id){
                    $suggested_user_type = userProfType($suggested_id);
                    $looking_for_id=explode('-',$looking_for_id);
                    $looking_for_id = $looking_for_id[0];

                    $arr[] = $looking_for_id[1];

                    $result = checkToShowUserInSearchResult($looking_for_id,$user_own_type);

                    if(strtolower($suggested_user_type) == strtolower($user_own_type) || $result == "BOTH"){
                        $is_score=isScoreNotZero($suggested_id,$user_id);
                        if( $is_score!=0){
                            $new_member_ids[] = $suggested_id;
                        }
                    }
                }
            }
            //echo '<pre>'; var_dump($lookingfor_list);
            $new_member_ids=array_unique($new_member_ids);

            $i=0; 
            $showres=array();
            foreach($new_member_ids as $k=>$val){ 
                $id = $user_id;
                $res = suggestionTypeFromId($id,$val);
                $showres[$res][]=$val;
            }
            //echo '<pre>'; var_dump($showres); die();
            $suggestionCountTypeWise = array();
                foreach($showres as $key=>$val){
                    if($key=='5a'){$text='strategic alliances';}
                    elseif($key=='5b'){$text='M&A opportunities';}
                    elseif($key=='2'){$text='suppliers';}
                    elseif($key=='1'){$text='customers';}
                    elseif($key=='4a'){$text='Invester in businesses';}
                    elseif($key=='4b'){$text='invester';}
                    elseif($key=='3a'){$text='non-executive directors';}
                    elseif($key=='3b'){$text='non-executive directors positions';}
                    elseif($key=='3c'){$text='freelance professionals';}
                    //$html .= '<li>'. count($val).'-'.getLookingForByType($key) .'</li>';
                    //$html .= '<li>'. count($val).' '.$text .'</li>';
                    //echo $user_own_type; die();
                    if($user_own_type == 'Buisness')
                    {
                        if($key != '3c' && $key != '4b')
                        {
                            //echo $key; echo '<br/>';
                            $suggestionCountTypeWise[$i++] = array('type'=>$text,'type_key'=>$key,'type_count'=>count($val));
                        }
                        
                    }
                    else
                    {
                        $suggestionCountTypeWise[$i++] = array('type'=>$text,'type_key'=>$key,'type_count'=>count($val));
                    }
                    
                }                       
            $data_api['suggestionCountTypeWise'] = $suggestionCountTypeWise;
            //echo '<pre>'; var_dump($data_api);
            $sug_ct = 0;
            foreach($data_api['suggestionCountTypeWise'] as $row)
            {
                $sug_ct = $sug_ct + $row['type_count'];
            }

            $data_api['suggestion_count'] = $sug_ct;
            //--------------------------------------------------------
           //var_dump($data_api); die();
            echo json_encode($data_api);
            exit;
        }
    }

	
	
    function autocomplete() {
        $q = $_REQUEST['q'];
        //$mid = $this->session->userdata['logged_in']['id'];
        $data['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data);
        $user_id = $result->mid; 
        $mid=$user_id;
        //$menberIds = $this->getMemberIdsFromBussinessCode($buis_id);
        $my_data = mysql_real_escape_string($q);
        $data = $this->model_member->autofetchMemberId($my_data, $mid);
        echo $data;
        exit;
    }

    function sentmail($mail, $mailBody, $subject) {

        $this->load->library('email');
        $this->email->from('admin@arlians.com', 'Arlians');
        $this->email->to($mail);
        $this->email->set_mailtype('html');

        $this->email->subject($subject);

        $this->email->message($mailBody);

        $this->email->send();
        return true;
    }

    function activateUser() {
        $id = $this->uri->segment(3);
        $activate_code = $this->uri->segment(4);
        $chk = $this->model_member->update_activation_code($id, $activate_code);
        //$chk = $this->model_member->update_activation_code($id);
        $this->load->view('member/greetings_afetr_activation');
    }

    function setup_wiz_one() {

        $id = $this->uri->segment(3);
        $activate_code = $this->uri->segment(4);
        $this->model_member->update_activation_code($id, $activate_code);
        $chk = $this->model_member->data_exists_wiz_one($id);       
        if ($chk == "TRUE") {
            redirect('member/login');
        } else {
            $this->load->view('member/setup_wiz');
        }
    }

    function get_country() {
        $data['country'] = $this->model_member->fetch_country_list();
        echo json_encode($data);
        exit;
    }

    function setup_wiz_one_part_one() {
        $d = array();
        $d['bussinesstype'] = $this->input->post('bussinesstype');
        $d['bussinessname'] = $this->input->post('bussinessname');
        $data_for_member['fname'] = $this->input->post('first_name');
        $data_for_member['lname'] = $this->input->post('last_name');
        $d['office_name'] = $this->input->post('office_name');
        $d['country'] = $this->input->post('country');
        $d['city'] = $this->input->post('city');
        $d['zipcode'] = $this->input->post('zipcode');
        $url = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . $d['zipcode']);
        $address = json_decode($url);
        if ($address->status == 'OK') {
            $d['Latitude'] = $address->results[0]->geometry->location->lat;
            $d['Longitude'] = $address->results[0]->geometry->location->lng;
        }

        $d['mid'] = $this->session->userdata['logged_in']['id'];
        $data_for_member['sharecode'] = md5(date("dmY-his") . $data_for_member['fname']);
        $data_for_member['mid'] = $d['mid'];
        $result_update = $this->model_member->update_member($data_for_member);
        $result = $this->model_member->insert_wiz_one_p_one($d);
        echo $result;
        exit;
    }

    function updatelatlan() {
        $ok = $this->model_member->updatelatlanall();
        echo $ok;
        die();
    }

    function changepassword() {
        $email = $this->input->post('data');
        $chk = $this->model_member->retriveMemberPassword($email);
        if ($chk != 0) {
            $result = '';
            //$mailBody = 'Please click on the link to change password. <a href="'.base_url().'member/retrivepass/'.$chk.'">Click Here</a>';
            $mailBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Arlians</title>
        <style type="text/css">
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;background-color:#e2e2e2} 
            p, a, li, td {-webkit-text-size-adjust:100%}
            .ReadMsgBody {width:100%}
            .ExternalClass {width:100%}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height:100%} 
            table td {border-collapse:collapse}
            br.hideDesktop {display:none !important}
            br.hideMobile {display:block !important}
            @media only screen and (max-device-width:480px) {
                table[class="threetwofive"], td[class="threetwofive"], img[class="threetwofive"] {width:325px !important}
            table[class="twosixfive"], td[class="twosixfive"], img[class="twosixfive"] {width:265px !important}
            table[class="hide"], td[class="hide"], img[class="hide"] {display:none !important}
            br[class="hideDesktop"] {display:block !important}
            br[class="hideMobile"] {display:none !important}
            }
        </style>
    </head>
    <body>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:#e2e2e2" align="center">
                    <table border="0" cellspacing="0" cellpadding="0" style="max-width: 600px;">
                        <tr>
                            <td width="100%" align="center">     
                                <table width="100%" class="threetwofive" cellspacing="0" cellpadding="5" border="0">
                                    <tr>
                                        <td align="center" style="background-color:#ED5664;"><span style="font-family:Calibri, Arial, sans-serif;font-size:11px;color:#fff; text-align:left">Reset your password</span>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;"><br /><br />
                                            <a href="http://www.arlians.com">
                                                <img src="http://www.arlians.com/img/Arlians-Full-Logo.png" width="200" class="twosixfive" alt="Arlians Logo" style="border:0;display:block" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="center" style="background-color:#fbfbfb;font-family:Calibri, Arial, sans-serif;font-size:26px;font-weight:bold;color:#4D575E;line-height:1.2;">Your new Arlians password awaits
<br />
                                            <br />
                                            <span style="font-size:14px;color:#4D575E;">You recently requested to reset your password. Please click the button below to start the password reset process.
<br /><br />
If you did not request a password change, you may ignore this message and your password will remain unchanged.

</span><br /><br />
                                            
                                        </td>
                                    </tr>       
                                </table>
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                    <td align="center" style="background-color:#01A69C;font-family:Calibri, Arial, sans-serif;font-size:16px;color:#fff;line-height:1.2;">
                                    <strong><a style="color:#fff;" href="' . base_url() . 'member/retrivepass/' . $chk . '">Reset Password</a></strong>
                                    

                                </table>
                                
                                <table width="100%" class="threetwofive" border="0" cellspacing="0" cellpadding="30">
                                    <tr>
                                        <td align="left" style="background-color:#4D575E;font-family:Calibri, Arial, sans-serif;font-size:11px;color:#cccccc">
                                            Copyright &copy; 2015 Arlians, All rights reserved.<br />
                                            You are receiving this email as you signed up to Arlians website.<br /><br />
                                            Our mailing address is:<br />
													Arlians Limited<br />
													88 Wood Street<br />
													London, EC2V 7RS<br />
													United Kingdom<br />
                                        </td>
                                    </tr>
                                </table>      
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>                              
    </body>
</html>';
            $subject = "Change Password";
            $result = $this->sentmail($email, $mailBody, $subject);
            echo $result;
            exit;
        } else {
            return 'false';
        }
    }   

    function retrivepass() {
        $id = $this->uri->segment(3);
        $data['id'] = $id;
        $this->load->view('member/passwordchange', $data);
    }

    function resetPassword() {
        $result = 0;
        $id = $this->input->post('id');
        $pass = md5($this->input->post('pass'));
        $result = $this->model_member->update_pass($id, $pass);
        echo $result;
        exit;
    }

    function getMemberIdsFromBussinessCode($code) {
        $mids = $this->model_member->fetchMemberId($code);
        return $mids;
    }

    function logout() {
        $this->user_manager->remove_user_token();
        $this->session->sess_destroy();
        redirect('explore');
    }

    function message_count($id, $type = null) {
        $count = $this->model_message->message_count_q($id, $type);
        return $count;
    }

    function fetchWhoViewedMyProfile() {
		$arr['email'] = $this->input->post('user_email');
	    $user_id = getIdByUserEmail($arr['email']);
		$id = $user_id->mid;
       // $id = $this->session->userdata['logged_in']['id'];
        $res['notification_list'] = $this->model_member->fetchWhoViewedMyProfile($id);
        //$res['notification_unseen'] = $this->model_member->fetchWhoViewedMyProfileUnseen($id);
		if(count($res['notification_unseen'])==0){
			 $res['notification_unseen'] =0;
		}
        //print_r($res); exit;
        echo json_encode($res);
        exit;
    }

    function updateNotificationData() {
        $request_list = json_decode($this->input->post('list'));
        $result = $this->model_member->updateNotificationData($request_list);
        if($result == true){
			$res_api['response'] = "success";
			
		}else{
			$res_api['response'] = "fail";
		}
        echo json_encode($res_api);
        exit;
    }

    function reqestForConnection() {
        $data['connected_id'] = $this->input->post('id');
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $data['is_seen'] = '0';
        $data['is_accepted'] = '0';
        //print_r($data); exit;
        $result = $this->model_othermembersprofile->reqestForConnection($data);
        echo $result;
        exit;
    }

    function reqestForDisconnection() {
        $data['connected_id'] = $this->input->post('id');
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        //print_r($data); exit;
        $result = $this->model_othermembersprofile->reqestForDisconnection($data);
        echo $result;
        exit;
    }

    function getConnectRequestDetail() {
        $data['connected_id'] = $this->input->post('id');
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $result = $this->model_othermembersprofile->getConnectRequestDetail($data);
        echo json_encode($result);
        exit;
    }

    function requestConnectList() {
        $id = $this->session->userdata['logged_in']['id'];
        $res = $this->model_member->requestConnectList($id);
        echo json_encode($res);
        exit;
    }

    function acceptRequestForConnection() {
        $id = $this->session->userdata['logged_in']['id'];
        $cid = $this->input->post('cid');
        $result_request = $this->model_member->acceptRequestForConnection($cid);
        echo $result_request;
        exit;
    }

    function requestConnectionSeen() {
        $cid = $this->input->post('cid');
        $result_request = $this->model_member->requestConnectionSeen($cid);
        echo $result_request;
        exit;
    }

    function getUnreadMessage() {
        $id = $this->session->userdata['logged_in']['id'];
        $result_request = $this->model_member->getUnreadMessage($id);
        //echo $result_request;
        echo json_encode($result_request);
        exit;
    }

    function getReadMessage() {
        $id = $this->session->userdata['logged_in']['id'];
        $count = $this->input->post('count');
        echo $id; exit;
        $result_request = $this->model_member->getReadMessage($id, $count);
        //print_r($result_request);
        echo json_encode($result_request);
        exit;
    }

    function getProfileViewedByYouList() {
        $id = $this->session->userdata['logged_in']['id'];
        $result_list = $this->model_member->iviewednotification($id);
        echo json_encode($result_list);
        exit;
    }

    function getYoursProfileViewedList() {
        $id = $this->session->userdata['logged_in']['id'];
        $result_list = $this->model_member->meviewednotification($id);
        echo json_encode($result_list);
        exit;
    }

    function getSuggestionList() {
        $id = $this->session->userdata['logged_in']['id'];
        $result_list = $this->model_member->getSuggestionList($id);
        echo json_encode($result_list);
        exit;
    }

    function deleteSuggestion() {
		$arr['email'] = $this->input->post('user_email');
        $user_id = getIdByUserEmail($arr['email']);
        $mid = $user_id->mid;
        $cid = $this->input->post('connected_id');
        $result_request = $this->model_member->deleteConnection($mid,$cid);
		if($result_request == true){
			$res_api['response'] = "success";
			
		}else{
			$res_api['response'] = "fail";
		}
        echo json_encode($res_api);
        exit;
    }

    function blockFromSuggestion() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $data['blocked_mid'] = $this->input->post('cid');
        //print_r($data); exit;
        $result_request = $this->model_suggestion->blockFromSuggestion($data);
        echo $result_request;
        exit;
    }

    function validateSignUpMember() {
        $data = array();
        $result = array();
        $results = array();
        $d['email'] = $this->input->post('email');
        $d['pass'] = $this->input->post('password');
        $chk = $this->model_member->logincheck($d);
        if ($chk == "TRUE") {
            $result['validate'] = true;
			$da['user_email'] = $this->input->post('email');
			$results = $this->model_member->getIdByUserEmail($da);
			$data['mid'] = $results->mid;
            $data['status'] = $this->model_member->memberStatus($d);
			$this->user_manager->set_user_token($d['email']);
            $chk_wiz_one = $this->model_member->data_exists_wiz_one($data['mid']);
			
			//$updatelogintime=$this->model_member->FnUpdateLoginTime();
			
			
			
			
                      
            if ($chk_wiz_one == "TRUE" && $data['status'] == '1') {
                $result['status'] = 1;
                $result['token'] = $d['email'];
                $result['redirect'] = 'member/dashboard';
            } elseif ($chk_wiz_one == "TRUE" && $data['status'] == '0') {
                $result['status'] = 0;
                $result['error_message'] = 'You are not activated. please active your account from mail link';
            } elseif ($chk_wiz_one == "TRUE" && $data['status'] == '3') {
                $result['status'] = 3;
                $result['error_message'] = 'You already deleted your account.Please register again';
            } elseif ($chk_wiz_one == "TRUE" && $data['status'] == '2') {
                $result['status'] = 2;
                $result['redirect'] = 'memberprofile/reactivate';
            } else {
                $result['status'] = null;
                $result['redirect'] = 'member/setup_wiz_one';
            }
        } else {
            $result['validate'] = false;
        }
		echo json_encode($result);
        exit;
    }

    function makeSuggestedListForloginuserDashboard($user_id) {
        //$sids = $_POST['looking_for'];
        $mid = $user_id;
        $sids = $this->model_suggestion->suggestionSearchTypes($mid);

        $searchT = array();

        foreach ($sids as $sid) {
            $s = explode('-', $sid);
            $searchT[] = $s[0];
        }
		
		//print_r($searchT);die();


        $result = $this->model_suggestion->deleteMidFromSuggestion($mid);

        foreach ($sids as $sid) 
		{
			
            $sid = explode('-', $sid);

            $mainMemberId = array();

            $id = array();
            $code;
            unset($uniqueMemberids);
            unset($uniqueMembers);
            $uniqueMemberids = array();
            $uniqueMembers = array();
            $uniqueIds2 = array();
            $mainMid;
            $remove_arr = array();

            $mid = $user_id;
            $rowSType = $sid[1];
            $mainMid = $mid;
            //=== check whether there is a search type for this member id or not ===
            $ch = $rowSType;



            if ($ch != '') {

                switch ($ch) {
                    case "1":
                        $user_type = $this->model_suggestion->userProfType($mid);

                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('4'), $user_type);


                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);



                            if (!empty($twenty_digit_code)) {
                                $i = 0;
                                foreach ($twenty_digit_code as $tw_code) {
                                    $i_code = $tw_code['industry_code'];
                                    $s_code = $tw_code['target_sector_code'];

                                    $codeArr = $this->model_suggestion->suggestionPossibleTwentyDigitCode($i_code, $s_code);

                                    foreach ($members_ids_to_filter as $suggested_id) {
                                        $flag = 0;
                                        $flag2 = 0;

                                        foreach ($codeArr as $code) {
                                            $codeTypeOne = substr($code, 0, 10);

                                            $match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id, $code, $codeTypeOne);
                                            $uniqueMemberids[$suggested_id][] = $match_type;
                                        }
                                    }
                                }

                                //----------------------------------

                                $newArr = $uniqueMemberids;
                                $finalArr = array();

                                foreach ($newArr as $key => $value) {

                                    $k = array_search('M', $value);

                                    if (isset($k)) {

                                        $finalArr[$key] = $value[$k];
                                    } else {

                                        $ke = array_search('PM', $value);

                                        if (isset($ke)) {
                                            $finalArr[$key] = $value[$ke];
                                        } else {
                                            $finalArr[$key] = 'X';
                                        }
                                    }
                                }
                                unset($uniqueMemberids);
                                $uniqueMemberids = $finalArr;
                                //----------------------------------

                                $ids = array_keys($uniqueMemberids);

                                $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);

                                foreach ($uniqueMemberids as $key => $id) {
                                    $location = $this->model_suggestion->locationMatchbyUserId($key, $mid);
                                    $this->model_suggestion->match1UserScore($sug_id, $mainMid, $key, $id, $location);
                                }
                            }
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        }
                        unset($uniqueMemberids);
                        break;
//=====================For 2================================
                    case "2":
                        $user_type = $this->model_suggestion->userProfType($mid);

                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('3'), $user_type);


                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid);

                            if (!empty($twenty_digit_code)) {
                                $i = 0;
                                foreach ($twenty_digit_code as $tw_code) {
                                    $tw_digit_code = $tw_code['mem_code'];

                                    $related_ten_digit_code = $this->model_suggestion->suggestionPossibleTenDigitCode($tw_digit_code);

                                    foreach ($members_ids_to_filter as $suggested_id) {
                                        if (!empty($related_ten_digit_code)) {
                                        foreach ($related_ten_digit_code as $tn_code) {

                                            $target_industry_code = substr($tn_code, 0, 5);
                                            $target_sector_code = substr($tn_code, 5, 10);

                                            $match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id, $target_industry_code, $target_sector_code);
                                            $uniqueMemberids[$suggested_id][] = $match_type;
                                        }
                                        }
                                    }
                                }



                                $newArr = $uniqueMemberids;
                                $finalArr = array();

                                foreach ($newArr as $key => $value) {

                                    $k = array_search('M', $value);

                                    if (isset($k)) {

                                        $finalArr[$key] = $value[$k];
                                    } else {

                                        $ke = array_search('PM', $value);

                                        if (isset($ke)) {
                                            $finalArr[$key] = $value[$ke];
                                        } else {
                                            $finalArr[$key] = 'X';
                                        }
                                    }
                                }

                                unset($uniqueMemberids);
                                $uniqueMemberids = $finalArr;


                                //----------------------------------

                                $ids = array_keys($uniqueMemberids);

                                $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);



                                foreach ($uniqueMemberids as $key => $id) {
                                    $location = $this->model_suggestion->locationMatchbyUserId($key, $mid);

                                    $this->model_suggestion->match1UserScore($sug_id, $mainMid, $key, $id, $location);
                                }
                            }
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        }
                        unset($uniqueMemberids);
                        break;

//==============================================================================================================================
                    //=====================For 3a================================
                    case "3a":
                        $uniqueIdsForTenDigitMatch = array();

                        $user_type = $this->model_suggestion->userProfType($mid);

                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('7'), $user_type);

                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid);

                            // For matching 20 digit code.
                            $match_type = array();
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($twenty_digit_code as $tw_code) {
                                    $flag = 0;
                                    $flag2 = 0;

                                    $code = $tw_code['mem_code'];
                                    $codeTypeOne = substr($code, 0, 10);
                                    $match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id, $code, $codeTypeOne);


                                    $uniqueMemberids[$suggested_id][] = $match_type;
                                }
                            }


                            $newArr = $uniqueMemberids;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueMemberids);
                            $uniqueMemberids = $finalArr;

                            //For matching 10 digit code.
                            $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($ten_digit_code as $tn_code) {
                                    $flag = 0;
                                    $flag2 = 0;
                                    $match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id, $tn_code['industry_code'], $tn_code['target_sector_code']);


                                    $uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
                                }
                            }

                            $newArr = $uniqueIdsForTenDigitMatch;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueIdsForTenDigitMatch);
                            $uniqueIdsForTenDigitMatch = $finalArr;

                            $merged_arr = $this->array_merge_new($uniqueMemberids, $uniqueIdsForTenDigitMatch);


                            $ids = array_keys($merged_arr);

                            $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);

                            foreach ($merged_arr as $key => $value) {
                                $value['location'] = $this->model_suggestion->locationMatchbyUserId($key, $mainMid);
                                $str = $value['own_industry'] . "-" . $value['location'] . "-" . $value['target_industry'];
                                $this->model_suggestion->match3UserScore($sug_id, $mainMid, $key, $str);
                            }
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        }

                        break;
                    //=================================================
                    //====================== 3b=========================

                    case "3b":
                        $uniqueIdsForTenDigitMatch = array();

                        $user_type = $this->model_suggestion->userProfType($mid);

                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('8'), $user_type);

                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid);

                            // For matching 20 digit code.
                            $match_type = array();
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($twenty_digit_code as $tw_code) {
                                    $flag = 0;
                                    $flag2 = 0;

                                    $code = $tw_code['mem_code'];
                                    $codeTypeOne = substr($code, 0, 10);
                                    $match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id, $code, $codeTypeOne);


                                    $uniqueMemberids[$suggested_id][] = $match_type;
                                }
                            }


                            $newArr = $uniqueMemberids;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueMemberids);
                            $uniqueMemberids = $finalArr;

                            //For matching 10 digit code.
                            $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($ten_digit_code as $tn_code) {
                                    $flag = 0;
                                    $flag2 = 0;
                                    $match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id, $tn_code['industry_code'], $tn_code['target_sector_code']);


                                    $uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
                                }
                            }

                            $newArr = $uniqueIdsForTenDigitMatch;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueIdsForTenDigitMatch);
                            $uniqueIdsForTenDigitMatch = $finalArr;

                            $merged_arr = $this->array_merge_new($uniqueMemberids, $uniqueIdsForTenDigitMatch);

                            $ids = array_keys($merged_arr);

                            $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);

                            foreach ($merged_arr as $key => $value) {
                                $value['location'] = $this->model_suggestion->locationMatchbyUserId($key, $mainMid);
                                $str = $value['own_industry'] . "-" . $value['location'] . "-" . $value['target_industry'];
                                $this->model_suggestion->match3UserScore($sug_id, $mainMid, $key, $str);
                            }
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        }

                        break;
                    //=================================================
                    //====================== 3c=========================

                    case "3c":
                        $uniqueIdsForTenDigitMatch = array();

                        $user_type = $this->model_suggestion->userProfType($mid);

                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('9'), $user_type);

                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid);

                            // For matching 20 digit code.
                            $match_type = array();
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($twenty_digit_code as $tw_code) {
                                    $flag = 0;
                                    $flag2 = 0;

                                    $code = $tw_code['mem_code'];
                                    $codeTypeOne = substr($code, 0, 10);
                                    $match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id, $code, $codeTypeOne);


                                    $uniqueMemberids[$suggested_id][] = $match_type;
                                }
                            }

                            $newArr = $uniqueMemberids;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueMemberids);
                            $uniqueMemberids = $finalArr;

                            //For matching 10 digit code.
                            $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($ten_digit_code as $tn_code) {
                                    $flag = 0;
                                    $flag2 = 0;
                                    $match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id, $tn_code['industry_code'], $tn_code['target_sector_code']);
                                    $uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
                                }
                            }

                            $newArr = $uniqueIdsForTenDigitMatch;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueIdsForTenDigitMatch);
                            $uniqueIdsForTenDigitMatch = $finalArr;

                            $merged_arr = $this->array_merge_new($uniqueMemberids, $uniqueIdsForTenDigitMatch);


                            $ids = array_keys($merged_arr);

                            $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);

                            foreach ($merged_arr as $key => $value) {
                                $value['location'] = $this->model_suggestion->locationMatchbyUserId($key, $mainMid);
                                $str = $value['own_industry'] . "-" . $value['location'] . "-" . $value['target_industry'];
                                $this->model_suggestion->match3UserScore($sug_id, $mainMid, $key, $str);
                            }
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                        }

                        break;

                    //=====================================
                    //=====================For 4a================================
                    case "4a":

                        $user_type = $this->model_suggestion->userProfType($mid);

                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('5'), $user_type);


                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);


                            if (!empty($twenty_digit_code)) {
                                $i = 0;
                                foreach ($twenty_digit_code as $tw_code) {
                                    $i_code = $tw_code['industry_code'];
                                    $s_code = $tw_code['target_sector_code'];

                                    $codeArr = $this->model_suggestion->suggestionPossibleTwentyDigitCode($i_code, $s_code);

                                    foreach ($members_ids_to_filter as $suggested_id) {
                                        $flag = 0;
                                        $flag2 = 0;

                                        foreach ($codeArr as $code) {
                                            $codeTypeOne = substr($code, 0, 10);

                                            $match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id, $code, $codeTypeOne);
                                            $uniqueMemberids[$suggested_id][] = $match_type;
                                        }
                                    }
                                }

                                //----------------------------------

                                $newArr = $uniqueMemberids;
                                $finalArr = array();

                                foreach ($newArr as $key => $value) {

                                    $k = array_search('M', $value);

                                    if (isset($k)) {

                                        $finalArr[$key] = $value[$k];
                                    } else {

                                        $ke = array_search('PM', $value);

                                        if (isset($ke)) {
                                            $finalArr[$key] = $value[$ke];
                                        } else {
                                            $finalArr[$key] = 'X';
                                        }
                                    }
                                }
                                unset($uniqueMemberids);
                                $uniqueMemberids = $finalArr;
                                //----------------------------------

                                $ids = array_keys($uniqueMemberids);

                                $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);

                                foreach ($uniqueMemberids as $key => $id) {
                                    $location = $this->model_suggestion->locationMatchbyUserId($key, $mid);

                                    $this->model_suggestion->match1UserScore($sug_id, $mainMid, $key, $id, $location);
                                    //$this->model_suggestion->match1UserScore($sug_id,$mainMid,$key,$id);
                                }
                            }
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                            //var_dump($data); die();
                        }
                        break;

//=====================
                    //=====================================4B
                    case "4b":

                        $user_type = $this->model_suggestion->userProfType($mid);

                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('6'), $user_type);


                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);


                            if (!empty($twenty_digit_code)) {
                                $i = 0;
                                foreach ($twenty_digit_code as $tw_code) {
                                    $i_code = $tw_code['industry_code'];
                                    $s_code = $tw_code['target_sector_code'];

                                    $codeArr = $this->model_suggestion->suggestionPossibleTwentyDigitCode($i_code, $s_code);

                                    foreach ($members_ids_to_filter as $suggested_id) {
                                        $flag = 0;
                                        $flag2 = 0;

                                        foreach ($codeArr as $code) {
                                            $codeTypeOne = substr($code, 0, 10);

                                            $match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id, $code, $codeTypeOne);
                                            $uniqueMemberids[$suggested_id][] = $match_type;
                                        }
                                    }
                                }

                                //----------------------------------

                                $newArr = $uniqueMemberids;
                                $finalArr = array();

                                foreach ($newArr as $key => $value) {

                                    $k = array_search('M', $value);

                                    if (isset($k)) {

                                        $finalArr[$key] = $value[$k];
                                    } else {

                                        $ke = array_search('PM', $value);

                                        if (isset($ke)) {
                                            $finalArr[$key] = $value[$ke];
                                        } else {
                                            $finalArr[$key] = 'X';
                                        }
                                    }
                                }
                                unset($uniqueMemberids);
                                $uniqueMemberids = $finalArr;
                                //----------------------------------

                                $ids = array_keys($uniqueMemberids);

                                $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);

                                foreach ($uniqueMemberids as $key => $id) {
                                    $location = $this->model_suggestion->locationMatchbyUserId($key, $mid);
                                    $this->model_suggestion->match1UserScore($sug_id, $mainMid, $key, $id, $location);
                                    //$this->model_suggestion->match1UserScore($sug_id,$mainMid,$key,$id);
                                }
                            }
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                            //var_dump($data); die();
                        }
                        break;
//==================================
//================================ 5a=======================
                    case "5a":

                        $uniqueIdsForTenDigitMatch = array();

                        $user_type = $this->model_suggestion->userProfType($mid);

                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('1'), $user_type);

                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid);

                            // For matching 20 digit code.
                            $match_type = array();
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($twenty_digit_code as $tw_code) {
                                    $flag = 0;
                                    $flag2 = 0;

                                    $code = $tw_code['mem_code'];
                                    $codeTypeOne = substr($code, 0, 10);
                                    $match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id, $code, $codeTypeOne);



                                    $uniqueMemberids[$suggested_id][] = $match_type;
                                }
                            }

                            $newArr = $uniqueMemberids;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueMemberids);
                            $uniqueMemberids = $finalArr;

                            //For matching 10 digit code.
                            $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
                            //echo '<pre>'; var_dump($ten_digit_code); die();
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($ten_digit_code as $tn_code) {
                                    $flag = 0;
                                    $flag2 = 0;
                                    $match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id, $tn_code['industry_code'], $tn_code['target_sector_code']);

                                    $uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
                                }
                            }

                            $newArr = $uniqueIdsForTenDigitMatch;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueIdsForTenDigitMatch);
                            $uniqueIdsForTenDigitMatch = $finalArr;

                            $merged_arr = $this->array_merge_new($uniqueMemberids, $uniqueIdsForTenDigitMatch);

                            //Looking Service 
                            $lookingService = $this->model_suggestion->getlookingservice($mainMid);

                            $lookingService = trim($lookingService, ',');
                            $lookingServiceArray = explode(',', $lookingService);

                            $last_suggested_ids = array_keys($merged_arr);

                            foreach ($last_suggested_ids as $id) {
                                $merged_arr[$id]['looking_service'] = $this->model_suggestion->getlookingservicemid($lookingServiceArray, $id);
                            }

                            $ids = array_keys($merged_arr);

                            $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);
                            // echo '<pre>'; var_dump($merged_arr);

                            foreach ($merged_arr as $key => $value) {
                                $value['location'] = $this->model_suggestion->locationMatchbyUserId($key, $mainMid);
                                $str = $value['own_industry'] . "-" . $value['location'] . "-" . $value['target_industry'] . "-" . $value['looking_service'];
                                $this->model_suggestion->match5UserScore($sug_id, $mainMid, $key, $str);
                            }
                            //echo '<pre>'; var_dump($merged_arr); die();
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                            //var_dump($data); die();
                        }
                        unset($merged_arr);
                        unset($uniqueIdsForTenDigitMatch);
                        break;
                    //=======================================
                    //================================ 5b=======================
                    case "5b":

                        $uniqueIdsForTenDigitMatch = array();

                        $user_type = $this->model_suggestion->userProfType($mid);
                        //echo '<pre>';
                        //var_dump($searchT); 
                        //echo $user_type;
                        // die();
                        $members_ids_to_filter = $this->model_suggestion->filterSuggestionLookingFor(array('2'), $user_type);
                        // sort($members_ids_to_filter);
                        //echo '<pre>'; var_dump($members_ids_to_filter); die();

                        if (!empty($members_ids_to_filter)) {
                            //Getting 20 digit code of the particuler mid.
                            $twenty_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($mid);


                            // For matching 20 digit code.
                            $match_type = array();
                            foreach ($members_ids_to_filter as $suggested_id) {
                                foreach ($twenty_digit_code as $tw_code) {
                                    $flag = 0;
                                    $flag2 = 0;

                                    $code = $tw_code['mem_code'];
                                    $codeTypeOne = substr($code, 0, 10);
                                    $match_type = $this->model_suggestion->suggestiontwentydigitmatchScore($suggested_id, $code, $codeTypeOne);

                                    $uniqueMemberids[$suggested_id][] = $match_type;
                                }
                            }

                            $newArr = $uniqueMemberids;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueMemberids);
                            $uniqueMemberids = $finalArr;


                            //For matching 10 digit code.
                            $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($mid);
                            //echo '<pre>'; var_dump($ten_digit_code); die();
                            foreach ($members_ids_to_filter as $suggested_id) {

                                foreach ($ten_digit_code as $tn_code) {


                                    $match_type = $this->model_suggestion->suggestiontendigitmatchScore($suggested_id, $tn_code['industry_code'], $tn_code['target_sector_code']);

                                    $uniqueIdsForTenDigitMatch[$suggested_id][] = $match_type;
                                }
                            }



                            $newArr = $uniqueIdsForTenDigitMatch;
                            $finalArr = array();

                            foreach ($newArr as $key => $value) {

                                $k = array_search('M', $value);

                                if (isset($k)) {

                                    $finalArr[$key] = $value[$k];
                                } else {

                                    $ke = array_search('PM', $value);

                                    if (isset($ke)) {
                                        $finalArr[$key] = $value[$ke];
                                    } else {
                                        $finalArr[$key] = 'X';
                                    }
                                }
                            }
                            unset($uniqueIdsForTenDigitMatch);
                            $uniqueIdsForTenDigitMatch = $finalArr;

                            //echo '<pre>'; var_dump($uniqueIdsForTenDigitMatch); die();

                            $merged_arr = $this->array_merge_new($uniqueMemberids, $uniqueIdsForTenDigitMatch);
                            //echo '<pre>'; var_dump($merged_arr); die();
                            //Looking Service 
                            $lookingService = $this->model_suggestion->getlookingservice($mainMid);

                            $lookingService = trim($lookingService, ',');
                            $lookingServiceArray = explode(',', $lookingService);

                            $last_suggested_ids = array_keys($merged_arr);

                            foreach ($last_suggested_ids as $id) {
                                $merged_arr[$id]['looking_service'] = $this->model_suggestion->getlookingservicemid($lookingServiceArray, $id);
                            }

                            $ids = array_keys($merged_arr);

                            $sug_id = $this->model_suggestion->insertTypeTwo($mainMid, $ids, $ch);
                            // echo '<pre>'; var_dump($merged_arr); die();

                            foreach ($merged_arr as $key => $value) {
                                $value['location'] = $this->model_suggestion->locationMatchbyUserId($key, $mainMid);
                                $str = $value['own_industry'] . "-" . $value['location'] . "-" . $value['target_industry'] . "-" . $value['looking_service'];
                                $this->model_suggestion->match5UserScore($sug_id, $mainMid, $key, $str);
                            }
                            //echo '<pre>'; var_dump($merged_arr); die();
                        } else {
                            $data['error_msg'] = '<li> <p class="date">No new result found. maybe they are already added in your connects.</p></li>';
                            //var_dump($data); die();
                        }
                        unset($merged_arr);
                        unset($uniqueIdsForTenDigitMatch);
                        break;
                    //=======================================
                }
            }
        }

        //=============== Deleting unwanted ids from ar_score table================

        $this->model_suggestion->deletedUnwantedIds();

        //=========================================================================

    }

    function array_merge_new($arr1, $arr2) {
        $arr3 = array();

        foreach ($arr1 as $key => $value) {

            $arr3[$key]['own_industry'] = $value;
            $arr3[$key]['location'] = "M";
            $arr3[$key]['target_industry'] = $arr2[$key];
        }
        //echo '<pre>'; var_dump($arr3); die();
        return $arr3;
    }

    function hub_post() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $data['content'] = $_POST['pm'];
        $tag_arr = $_POST['tag'];

        if ($_POST['tag'] != '') {
            $data['tag_id'] = implode(",", $_POST['tag']);
        }
        $data['shared_for'] = $_POST['st'];

        $hub_id = $this->model_member->post_hub($data);
        $ch = $_POST['st'];
        switch ($ch) {
            case 1:
                $tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
                $code = $tw_digit_code[0]['mem_code'];
                $code = substr($code, 0, 5);

                $dt['hub_id'] = $hub_id;
                $dt['topline_code'] = $code;
                $res = $this->model_hub->hubPostDetails($dt);
                break;
            case 2:
                //-----Own top line-----------
                $tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);

                $own_topline = array();

                foreach ($tw_digit_code as $code) {
                    $cd = $code['mem_code'];
                    $own_topline[] = substr($cd, 0, 5);
                }
                //---------------------------------	
                // topline code from ten digit code 
                $top_line_arr = array();
                $tw_digit_code = array();
                $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($data['mid']);
                foreach ($ten_digit_code as $code) {
                    $cd = $code['memcodetwo'];
                    $tw_digit_code = $this->model_suggestion->getTwentyDigitCodeFromTenDigit($cd);

                    foreach ($tw_digit_code as $c) {
                        $cd = $c['wizone_code'];
                        $top_line_arr[] = substr($cd, 0, 5);
                    }
                }
                //-----------------------------
                $top_line_arr = array_unique($top_line_arr);
                $final_topline = array_merge($own_topline, $top_line_arr);
                $final_topline = array_unique($final_topline);

                foreach ($final_topline as $topline) {

                    $dt['hub_id'] = $hub_id;
                    $dt['topline_code'] = $topline;
                    $res = $this->model_hub->hubPostDetails($dt);
                }
                break;

            case 3:
                //-----Own top line-----------
                $tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);

                $own_topline = array();

                foreach ($tw_digit_code as $code) {
                    $cd = $code['mem_code'];
                    $own_topline[] = substr($cd, 0, 5);
                }
                //---------------------------------	
                // topline code from ten digit code 
                $top_line_arr = array();
                $tw_digit_code = array();
                $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($data['mid']);
                foreach ($ten_digit_code as $code) {
                    $cd = $code['memcodetwo'];
                    $tw_digit_code = $this->model_suggestion->getTwentyDigitCodeFromTenDigit($cd);

                    foreach ($tw_digit_code as $c) {
                        $cd = $c['wizone_code'];
                        $top_line_arr[] = substr($cd, 0, 5);
                    }
                }
                //-----------------------------
                $top_line_arr = array_unique($top_line_arr);
                $final_topline = array_merge($own_topline, $top_line_arr);
                $final_topline = array_unique($final_topline);

                foreach ($final_topline as $topline) {

                    $dt['hub_id'] = $hub_id;
                    $dt['topline_code'] = $topline;
                    $res = $this->model_hub->hubPostDetails($dt);
                }
                break;
        }

        if ($tag_arr != '') {
            foreach ($tag_arr as $tag) {
                $top_line_code = $this->model_hub->convertingHubIdToTopline($tag);

                if ($top_line_code != NULL) {
                    $d['hub_id'] = $hub_id;
                    $d['topline_code'] = $top_line_code;
                    $res = $this->model_hub->hubPostDetails($d);
                }
            }
        }
        echo $res;
        exit;
    }

    function hub_post_api() {
		//var_dump($_FILES);
		$flag=0;
        if(count($_FILES)>0){
		   $validextensions = array("jpeg", "jpg", "png","gif","bmp","mp4");
		   $ext = explode('.', basename($_FILES[0]['name']));//explode file name from dot(.) 
			  
		        $file_extension = end($ext); //store extensions in the variable
		        $file_name = 'Hub'.date('Ymdhis').'.'.$file_extension;//set the target path with a new name of image
		        $target_path = FCPATH ."uploads".DS."hub_images".DS.$file_name;
		   
		 if (in_array($file_extension, $validextensions)) {
		            if (move_uploaded_file($_FILES[0]['tmp_name'], $target_path)) {//if file moved to uploads folder
		               $flag=1;
		            } else {//if file was not moved.
		                $flag=0;
		            }
					
		        } else {
					$flag=2;
		        }
		}
		
		//var_dump(count($_FILES).'>0 &&'. $flag.'==2');exit;
	if(count($_FILES[0])>0 && $flag==2){
		$res_api['response'] = "Fail";	
		 $res_api['message'] = 'File format not supported.';
	}else{
        $arr['email'] = $_SERVER['HTTP_AUTH'];
        $user_id = getIdByUserEmail($arr['email']);
				if($flag==1){
					 $data['link'] = $file_name;
				}elseif(json_decode($_SERVER['HTTP_MESSAGE'])->src!=''){
					$data['link'] = base64_decode(json_decode($_SERVER['HTTP_MESSAGE'])->src);
				}
				if(json_decode($_SERVER['HTTP_MESSAGE'])->title !=''){
					
						$data['hub_title'] = base64_decode(json_decode($_SERVER['HTTP_MESSAGE'])->title);
					
				}
				if(json_decode($_SERVER['HTTP_MESSAGE'])->desc !=''){
						$data['content'] = base64_decode(json_decode($_SERVER['HTTP_MESSAGE'])->desc);
					
				}else{
					$data['content'] = null;
				}
					$string =json_decode($_SERVER['HTTP_MESSAGE'])->message;
					$regex = "@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?).*$)@";
					$message= preg_replace($regex, ' ', $string);
					$data['hbcomment'] = ($message=='0')?'':$message;
				if(base64_decode(json_decode($_SERVER['HTTP_MESSAGE'])->news_url) !=''){
					$data['news_link'] =  base64_decode(json_decode($_SERVER['HTTP_MESSAGE'])->news_url);
				}else{
					$data['news_link'] =  null;
				}
				
				
				
        $data['mid'] = $user_id->mid;

       
       //$ct = json_encode($_SERVER['HTTP_MESSAGE']);
	  // echo '<pre>'; var_dump($data); die();
       
		
        $tag_arr = $_POST['tag'];
        $res_api = array();

        if ($_POST['tag'] != '') {
            $data['tag_id'] = implode(",", $_POST['tag']);
        }
        $data['shared_for'] = $_SERVER['HTTP_SCOPE'];
        $data['type_of_post'] = $_SERVER['HTTP_TYPE'];
        $data['url_provider'] = (json_decode($_SERVER['HTTP_MESSAGE'])->provider);
		 $data['hub_share_type'] = (json_decode($_SERVER['HTTP_MESSAGE'])->POSTFLAG);
		//print_r(json_decode($ct));die;
//print_r($data);exit;
        $hub_id = $this->model_member->post_hub($data);
        $ch = $_SERVER['HTTP_SCOPE'];
        switch ($ch) {
            case 1:
                $tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
                $code = $tw_digit_code[0]['mem_code'];
                $code = substr($code, 0, 5);

                $dt['hub_id'] = $hub_id;
                $dt['topline_code'] = $code;
                $res = $this->model_hub->hubPostDetails($dt);
                break;
            case 2:
                //-----Own top line-----------
                $tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);

                $own_topline = array();

                foreach ($tw_digit_code as $code) {
                    $cd = $code['mem_code'];
                    $own_topline[] = substr($cd, 0, 5);
                }
                //--------------------------------- 
                // topline code from ten digit code 
                $top_line_arr = array();
                $tw_digit_code = array();
                $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($data['mid']);
                foreach ($ten_digit_code as $code) {
                    $cd = $code['memcodetwo'];
                    $tw_digit_code = $this->model_suggestion->getTwentyDigitCodeFromTenDigit($cd);

                    foreach ($tw_digit_code as $c) {
                        $cd = $c['wizone_code'];
                        $top_line_arr[] = substr($cd, 0, 5);
                    }
                }
                //-----------------------------
                $top_line_arr = array_unique($top_line_arr);
                $final_topline = array_merge($own_topline, $top_line_arr);
                $final_topline = array_unique($final_topline);

                foreach ($final_topline as $topline) {

                    $dt['hub_id'] = $hub_id;
                    $dt['topline_code'] = $topline;
                    $res = $this->model_hub->hubPostDetails($dt);
                }
                break;

            case 3:
                //-----Own top line-----------
                $tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);

                $own_topline = array();

                foreach ($tw_digit_code as $code) {
                    $cd = $code['mem_code'];
                    $own_topline[] = substr($cd, 0, 5);
                }
                //--------------------------------- 
                // topline code from ten digit code 
                $top_line_arr = array();
                $tw_digit_code = array();
                $ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($data['mid']);
                foreach ($ten_digit_code as $code) {
                    $cd = $code['memcodetwo'];
                    $tw_digit_code = $this->model_suggestion->getTwentyDigitCodeFromTenDigit($cd);

                    foreach ($tw_digit_code as $c) {
                        $cd = $c['wizone_code'];
                        $top_line_arr[] = substr($cd, 0, 5);
                    }
                }
                //-----------------------------
                $top_line_arr = array_unique($top_line_arr);
                $final_topline = array_merge($own_topline, $top_line_arr);
                $final_topline = array_unique($final_topline);

                foreach ($final_topline as $topline) {

                    $dt['hub_id'] = $hub_id;
                    $dt['topline_code'] = $topline;
                    $res = $this->model_hub->hubPostDetails($dt);
                }
                break;
        }

        if ($tag_arr != '') {
            foreach ($tag_arr as $tag) {
                $top_line_code = $this->model_hub->convertingHubIdToTopline($tag);

                if ($top_line_code != NULL) {
                    $d['hub_id'] = $hub_id;
                    $d['topline_code'] = $top_line_code;
                    $res = $this->model_hub->hubPostDetails($d);
                }
            }
        }
        //$res = array();
        if($res >0)
        {
            $res_api['response'] = "success";
            
			$hub_ids = array($hub_id);
			  include_once('hub.php');
			 $hub = new hub();
			$hub_Details=$hub->currentHubDetails($hub_ids,$mid);
			$res_api['hub_details'] = $hub_Details[0];
			$res_api['hub_details']['POSTFLAG'] = $hub_Details[0]['hub_share_type'];
			$res_api['hub_details']['likeconnectedtouser'] = false;
			$res_api['hub_details']['commentconnectedtouser'] = false;
				
			if($flag==1){
				 $res_api['link'] =  base_url() . "uploads/hub_images/".$file_name;
				 $ext = explode('.', basename($file_name));//explode file name from dot(.) 
				$file_extension = end($ext); 
				 $res_api['ext'] = $file_extension;
				 $res_api['ext'] = $file_extension;
				 $res_api['ext'] = $file_extension;
					
			}else{
				$res_api['link'] = '';
			}
        }
        else
        {
           $res_api['response'] = "Fail";
        }
	} 
    echo json_encode($res_api);
    exit;
}

    function postImage() {

        $imagename = $this->input->post('imgFile');
        $share = $this->input->post('share');

        if ($_POST['tag'] != '') {
            $tag = implode(',', $this->input->post('tag'));
        }
        $about_the_img = $this->input->post('about_the_img');

        if ($imagename != '') {
            $content = $imagename;
            if ($content <> '') {
                $parts = explode(',', $content);
                if (count($parts) > 1) {
                    $product_image_data = base64_decode(str_replace($parts[0] . ',', "", $content));
                    $image_name = "member" . round(microtime(true) * 1000) . ".png";
                    $target_path = FCPATH . "uploads" . DS . "hub_images" . DS . $image_name;
                    file_put_contents($target_path, $product_image_data);

                    $data['mid'] = $this->session->userdata['logged_in']['id'];
                    $data['content'] = $about_the_img;

                    if ($_POST['tag'] != '') {
                        $data['tag_id'] = $tag;
                    }

                    $data['link'] = $image_name;
                    $data['shared_for'] = $share;

                    $hub_id = $this->model_member->post_hub($data);
                    $tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
                    $code = $tw_digit_code[0]['mem_code'];
                    $code = substr($code, 0, 5);

                    $dt['hub_id'] = $hub_id;
                    $dt['topline_code'] = $code;

                    $res = $this->model_hub->hubPostDetails($dt);
                    if ($_POST['tag'] != '') {
                        $tag_arr = $_POST['tag'];
                        foreach ($tag_arr as $tg) {
                            $top_line_code = $this->model_hub->convertingHubIdToTopline($tg);

                            if ($top_line_code != NULL) {
                                $d['hub_id'] = $hub_id;
                                $d['topline_code'] = $top_line_code;
                                $res = $this->model_hub->hubPostDetails($d);
                            }
                        }
                    }
                    echo $res;
                } else {
                    $result = false;
                }
                exit;
            }
        }
    }

    function postImageApi() {

        $imagename = $this->input->post('imgFile');
        $share = $this->input->post('share');

        if ($_POST['tag'] != '') {
            $tag = implode(',', $this->input->post('tag'));
        }
        $about_the_img = $this->input->post('about_the_img');

        if ($imagename != '') {
            $content = $imagename;
            if ($content <> '') {
                $parts = explode(',', $content);
                if (count($parts) > 1) {
                    $product_image_data = base64_decode(str_replace($parts[0] . ',', "", $content));
                    $image_name = "member" . round(microtime(true) * 1000) . ".png";
                    $target_path = FCPATH . "uploads" . DS . "hub_images" . DS . $image_name;
                    file_put_contents($target_path, $product_image_data);
                    //---------------------

                    $arr['email'] = $this->input->post('user_email');
                    $user_id = getIdByUserEmail($arr['email']);

                    $data['mid'] = $user_id->mid;

                    //--------------------
                    //$data['mid'] = $this->session->userdata['logged_in']['id'];
                    $data['content'] = $about_the_img;

                    if ($_POST['tag'] != '') {
                        $data['tag_id'] = $tag;
                    }

                    $data['link'] = $image_name;
                    $data['shared_for'] = $share;

                    $hub_id = $this->model_member->post_hub($data);
                    $tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
                    $code = $tw_digit_code[0]['mem_code'];
                    $code = substr($code, 0, 5);

                    $dt['hub_id'] = $hub_id;
                    $dt['topline_code'] = $code;

                    $res = $this->model_hub->hubPostDetails($dt);
                    if ($_POST['tag'] != '') {
                        $tag_arr = $_POST['tag'];
                        foreach ($tag_arr as $tg) {
                            $top_line_code = $this->model_hub->convertingHubIdToTopline($tg);

                            if ($top_line_code != NULL) {
                                $d['hub_id'] = $hub_id;
                                $d['topline_code'] = $top_line_code;
                                $res = $this->model_hub->hubPostDetails($d);
                            }
                        }
                    }
                    echo $res;
                } else {
                    $result = false;
                }
                exit;
            }
        }
    }

    function getUserOwnIndusNo() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $result = $this->model_category->getUserOwnIndusNo($data['mid']);
        echo $result;
        exit;
    }

    function getUserTergetIndusNo() {
        $data['mid'] = $this->session->userdata['logged_in']['id'];
        $result = $this->model_category->getUserTergetIndusNo($data['mid']);
        echo $result;
        exit;
    }

    function updateMessageSeenTable() {
        //$data['mid'] = $this->session->userdata['logged_in']['id'];mt_id
        $data['mt_id'] = $this->input->post('mt_id');
        $data['msg_read'] = '1';
        $result = $this->model_member->updateMessageSeenTable($data);
        echo $result;
        exit;
    }

    function deleteMyAccount() {
        $this->load->view('member/deleteMyAccount');
    }

    function moreHub() {
        $this->load->view('member/morehub');
    }

    function fetch_country_list() {
        $result = $this->model_member->fetch_country_list();
        echo json_encode($result);
        exit;
    }
    /**PHASE 2 API***/
    function getUserDetails(){
        $data['user_email'] = $this->input->post('user_token');
        $user['user_info'] = $this->model_member->MemberDetails($data['user_email']);

        $info = getIdByUserEmail($data['user_email']);
        $img = $this->model_member->ProfileImage($info->mid);
//var_dump($user['user_info']);die();
        if($img != NULL || $img != '')
        {    $em = logobussinessname($info->mid);
            $user['profile_image'] = $img;
            $user['logobussinessname'] = $em;
        }
        else
        {
			
			$em = logobussinessname($info->mid);
      	

          $user['profile_image'] = $img; 
          $user['logobussinessname'] = $em; 
        }
		
		$user['country_name'] = $this->model_member->fetch_country_byid($user['user_info']['country']);
		$user['userProfType'] = userProfType($info->mid);
        $user['chk_wiz_one']= $this->model_member->data_exists_wiz_one($info->mid);
        echo json_encode($user);
        exit;
    }
    function getUserUnreadMessage() {
        $data['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($data);
        $result_request = $this->model_member->getUnreadMessage($result->mid);
        //echo $result_request;
        echo json_encode($result_request);
        exit;
    }
    function getUserReadMessage() {
        $data['user_email'] = $this->input->post('user_email');       
        $count = $this->input->post('count');
        $result = $this->model_member->getIdByUserEmail($data);    
        $result_request = $this->model_member->getReadMessage($result->mid, $count);
        echo json_encode($result_request);
        exit;
    }
    function userRequestConnectList() {
        $data['user_email'] = $this->input->post('user_email');  
        $result = $this->model_member->getIdByUserEmail($data);    
        $res = $this->model_member->requestConnectList($result->mid);
        echo json_encode($res);
        exit;
    }
    function userAcceptRequestForConnection() {        
        $cid = $this->input->post('cid');
        $result_request = $this->model_member->acceptRequestForConnection($cid);
        if($result_request==true){
            $result = array('success'=>true);
        }else{
            $result = array('success'=>false);
        }
        echo json_encode($result);
        exit;
    }
    function  userRequestConnectionSeen() {
        $cid = $this->input->post('cid');
        $result_request = $this->model_member->requestConnectionSeen($cid);
        if($result_request==true){
            $result = array('success'=>true);
        }else{
            $result = array('success'=>false);
        }
        echo json_encode($result);
        exit;;
    }
    function validateMemberEmail() {       
        $email = $this->input->post('user_email');  
        $res = $this->model_member->membervalidate($email);
        if($res=="TRUE"){
            $result = array('success'=>true);
        }else{
            $result = array('success'=>false);
        }
        echo json_encode($result);
        exit;
    }
    function generate_password($length = 20){
      $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789`-=~!@#$%^&*()_+,./<>?;:[]{}\|';
		$str = '';
		$max = strlen($chars) - 1;
		for ($i=0; $i < $length; $i++)
			$str .= $chars[mt_rand(0, $max)];
		return $str;
}
    function forgetCredential() {
        $user['user_email'] = $this->input->post('data');
        $email = $this->input->post('data');
        $chk = $this->model_member->retriveMemberPassword($email);
		$result = $this->model_member->getIdByUserEmail($user);    
		$user_name=getMemberbusinName($result->mid);
		$password=$this->generate_password(8);
		$this->model_member->update_pass($result->mid, md5($password));
		
        if ($chk != 0) {
            $result = '';
           
 $mailBody = '<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Arlians</title>
	<meta name="author" content="" />
	<meta name="description" content="" />
	<meta name="keywords"  content="" />
	<meta name="Resource-type" content="Document" />
  <meta name="viewport" content="width=device-width, initial-scale=1">


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Oxygen:400,700" rel="stylesheet" type="text/css">

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400" rel="stylesheet" type="text/css">

	<style type="text/css">
  * { box-sizing: border-box;}   
   ::-moz-placeholder{ color: #357cb7; opacity: 1;}
   .main-body{ width: 352px;}
   .input-box{ width: 241px;}



   @media screen and (max-width: 767px) {
    .main-body{ width: 90%;}
    .input-box{ width:82%;}
   }


  </style>	

</head>



<body style="margin: 0; font-family: Arial; font-size: 13px; line-height: 20px; color:#515151;">

<table width="100%" border="0" cellpadding="0" cellspacing="0" style="background-image: url('.base_url().'email-template/images/email-bg01.jpg); background-repeat: no-repeat; background-position: center top;  background-size: cover;  padding: 88px 0;">
  <tr>
    <td>
      <table border="0" cellpadding="0" cellspacing="0" style="width: 352px; background-color:#fff; margin: 0 auto;">
        <tr>
          <td style="background-color: #357cb7; padding: 35px; color: #fff; text-align: center;">
             <h2 style="font-size: 40px; line-height: 46px; margin: 0 0 15px;">Did you forget something?</h2>
              <span style="font-size: 30px; text-transform: uppercase; font-weight: bold;">Here it is!</span>
          </td>
        </tr>
        
        <tr>
          <td style="padding: 35px;">
              <h2 style="font-size: 37px; color: #515151; line-height: 46px; color:#515151; margin: 0 0 20px; text-align: center; font-family: \'Source Sans Pro\', sans-serif;font-weight:300;">Hi, '.$user_name.'!</h2> 
              <p style="margin: 0 38px; text-align: center; color:#515151;">Here are your login details. You can change your email address and password in your settings.</p>
          </td>
        </tr>

         <tr style="margin-top: 50px;">
          <td  style="padding: 35px;">
             <form>
              <table width="100%" border="0" cellpadding="0" cellspacing="0">
                 <tr>
                   <td>
                      <span style="border-radius: 0; background-color: #357cb7; color: #fff; display: table-cell;   vertical-align: middle; white-space: nowrap;  font-size: 14px; font-weight: normal; line-height: 1; padding: 6px 12px; text-align: center; width: 20px; height:40px; line-height:30px; float:left;"><img src="'.base_url().'email-template/images/user-icon.png" alt=""></span>

<a href="#" style=" width: 218px; background-color: #f5f7f7; border: medium none; border-radius: 0; color: #357cb7;  font-size: 16px; min-height: 52px; line-height:52px; display:inline-block; padding: 0 10px;">
'.$email.'</a>
                   </td>
                 </tr>
                 <tr>
                   <td style="padding-top: 3px;" >
                      <span style="border-radius: 0; background-color: #357cb7; color: #fff; display: table-cell;   vertical-align: middle; white-space: nowrap;  font-size: 14px; font-weight: normal; line-height: 1; padding: 6px 12px; text-align: center; width: 20px; height:40px; line-height:30px; float:left;"><img src="'.base_url().'email-template/images/password-icon.png" alt=""></span>

            <a href="#" style=" width: 218px; background-color: #f5f7f7; border: medium none; border-radius: 0; color: #357cb7;  font-size: 16px; min-height: 52px; line-height:52px; display:inline-block; padding: 0 10px;">
'.$password.'</a> 
                   </td>
                 </tr>
                   <tr>
                   <td style="padding-top: 30px;" >
                       <!--<button type="submit" style="background-color: #357cb7 ; border: none; color: #fff; display: block; font-size: 18px; font-weight: bold; padding: 8px; width: 100%; cursor: pointer;">Login</button>-->
					   <a href="'.base_url().'" style="background-color: #357cb7 ; border: none; color: #fff; display: block; font-size: 18px; font-weight: bold; padding: 8px 0; width: 100%; text-align:center; cursor: pointer;display:block;text-decoration:none">Login</a>
                   </td>
                 </tr>
              </table>
              </form>

          </td>
        </tr>

      </table>
      <table width="100%" border="0" cellpadding="0" cellspacing="0" style="text-align:center;  font-size: 13px; margin-top: 30px; text-align: center; color:#fff;">
        <tr>
          <td>
            <img src="'.base_url().'email-template/images/noti-logo.png" alt="">
            <p style="margin-top:10px; margin-bottom:0;">Manage email preferences in your settings</p>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>   

 
</body>
</html>';
            $subject = "Forgot your password";
            $result = $this->sentmail($email, $mailBody, $subject);
            if($result == true){
                $return_data = array('success' => true);
            }else{
                $return_data = array('success' => false);
            }
        } else {
           $return_data = array('success' => false);
        }
        echo json_encode($return_data);
        exit;
    }
	
	function isBuisnessNameExist(){
		$data['user_email'] = $this->input->post('user_email');
        $info = getIdByUserEmail($data['user_email']);
        $response = $this->model_member->isBuisnessNameExist($info->mid);
		 if($response == 1){
                $return_data = array('success' => 'true');
          }else{
                $return_data = array('success' => 'false');
         }
		
		echo json_encode($return_data);
        exit;
	}
	function isBusinessNameuniqe(){
		$user_buisnessname = $this->input->post('user_buisnessname');
		$response = $this->model_member->isBusinessNameuniqe($user_buisnessname);
		if($response == 1){
                $return_data = array('success' => 'false');
          }else{
                
				$return_data = array('success' => 'true');
         }
		 echo json_encode($return_data);
        exit;
		
	}
	
		function ownSubSector(){
		$data['user_email'] = $this->input->post('user_email');
        $info = getIdByUserEmail($data['user_email']);
		$response = fetch_my_sector_pro($info->mid);
		echo json_encode($response);
        exit;
			  
		  }
		function targetSubSector(){
			$data['user_email'] = $this->input->post('user_email');
			$info = getIdByUserEmail($data['user_email']);
			$response = fetch_target_sector($info->mid);
			echo json_encode($response);
			exit;
		  }
		
	function updateProfile(){
		$data['user_email'] = $this->input->post('user_email');
		$info = getIdByUserEmail($data['user_email']);
		$datap['mid']=$info->mid;
		$datap['password'] = $this->input->post('password');
		$resultp = $this->model_memberprofile->resetPassword($datap);
		$datae['mid']=$info->mid;
		$datae['email'] = $this->input->post('email');
		$resulte = $this->model_memberprofile->editProfileEmail($datae);
		
		 if($resultp == 'success' && $resulte == 'success'){
                $return_data = array('success' => 'true');
          }else{
                $return_data = array('success' => 'false');
         }
		  
		echo json_encode($return_data);
        exit;
	}
	
	function newsletterunsubscribe()
	{
		$this->load->view('member/newsletterunsubscribe');
	}
	
	function FnUnsubscribe()
	{
		$user_email = $this->input->post('email');
		$status=$this->model_member->FnUnSubscribeUser($user_email);
	    if($status == 'success')
		{
                $return_data = array('success' => 'true');
        }else
		{
                $return_data = array('success' => 'false');
        }
		
		echo json_encode($return_data);
        exit;
	}
	function connectionList(){
		//$d['user_email'] = 'arlians2015@gmail.com';
		$d['user_email'] = $this->input->post('user_email');
        $res = $this->model_member->getIdByUserEmail($d);
        $user_id = $res->mid;
        $connect = $this->model_memberprofile->getConnectedMemberListAll($user_id);
		//print_r($connect);
        $cont = array();
		
        for($i=0;$i<=count($connect)-1;$i++) {
            $cont[$i]['bussiness_name'] = getMemberbusinName($connect[$i]);
            $em = logobussinessname($connect[$i]);
      	    $cont[$i]['logobussinessname'] = $em; 
            $cont[$i]['image_basepath'] = base_url() . 'uploads/profile_image/';
            $cont[$i]['mid'] = $connect[$i];
            $cont[$i]['back_image'] = $this->model_member->BackImage($connect[$i]);
            $cont[$i]['profile_image'] = $this->model_member->ProfileImage($connect[$i]);
            $cont[$i]['location'] = $this->model_member->userLocation($connect[$i]);
        }
        $result['connection'] = $cont;
        //$result['pending'] = $this->model_memberprofile->getNetworkConnectionRecievedList($user_id);
       // $result['recieved'] = $this->model_memberprofile->getConnectedMemberListAllre($user_id);
        echo json_encode($result);
        exit;
		//$this->load->view('member/connectionlist');
		
	}
	function connectionListPage(){
		$this->load->view('member/connectionlist');
		
	}
	
}
