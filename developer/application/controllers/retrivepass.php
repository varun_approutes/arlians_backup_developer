<?php
class Retrivepass extends CI_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('model_common');
        $this->load->model('model_member');
        $this->load->model('model_hub');
        $this->load->model('model_message');
        $this->load->library('User_Manager');
        $this->load->model('model_suggestion');
        $this->load->helper('common_helper');
        $this->load->helper('text');
        $this->model_common->SiteSettingsDetails(); // This is mandatory
        $this->load->model('model_othermembersprofile');
        $this->load->model('model_category');
        $this->user_manager = User_Manager::get_instance();
        $this->load->helper('load_controller');
    }
    function index() {
        $id = $this->uri->segment(3);
        $data['id'] = $id;
        $this->load->view('member/passwordchange', $data);
    }
}
?>