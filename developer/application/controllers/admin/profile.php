<?php 
class Profile extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('admin/modeladmin');
		$this->load->model('admin/modelprofile');
		$this->modeladmin->AllSiteSettings();
	}
	
	function index()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
			
		if($this->session->userdata('admin_user_data')=='')
		{
			redirect('admin');
		}
		else
		{
			$result['profile']=$this->modelprofile->GetProfileDetails();
			$this->load->view('admin/profile',$result);
		}
		
	}
//--------------- Start function to edit profile ----------------------------//
	function edit()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
			
		if($this->session->userdata('admin_user_data')=='')
		{
			redirect('admin');
		}
		else
		{
			$result['profile']=$this->modelprofile->GetProfileDetails();
			$this->load->view('admin/edit_profile',$result);
		}
	}
//----------------- End fuinction to load the edit profile page -----------------------//
//------------------- Start function to check email existance -------------------------//
	function check_email()
	{
		$userid=$_REQUEST['userid'];
		$usertype=$_REQUEST['usertype'];
		$email=$_REQUEST['email'];
		//echo $userid.'@@@'.$usertype.'@@@'.$email;die;
		if(($usertype=='Super_Admin') || ($usertype=='Admin'))
		{
			$this->db->where('id !=',$userid);
			$this->db->where('email',$email);
			$q=$this->db->get('admin');
		}
		if($q->num_rows>0)
		{
			echo "FALSE";
		}
		else
		{
			echo "TRUE";
		}
	}
//--------------------- End function to check email existance --------------------------//
//------------------ Start function to update profile details ---------------------------//
	function update()
	{
		$result=$this->modelprofile->update();
		redirect('admin/profile/edit');
	}
//-------------------- End function to update profile details ----------------------------//       
}