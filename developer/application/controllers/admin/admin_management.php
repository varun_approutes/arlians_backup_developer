<?php 
class Admin_management extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('admin/modeladmin');
		$this->modeladmin->AllSiteSettings();
		$this->load->model('admin/model_admin_management');
		/*if($this->session->userdata('admin_user_data')=='')
		{
			redirect('admin');
		}*/
	}
	
	function index()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
			
		
		$result['rows'] = $this->model_admin_management->GetAllList();
		//echo '<pre>';print_r($result['rows']);die;
		$this->load->view('admin/admin_management/list',$result);
		
		
	}

	function add()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
		$this->load->view('admin/school/add',$result);
		
	}
//---------------------- Start function to check username existance --------------------------//
	function check_username_exist()
	{
		$username=$_REQUEST['username'];
		$userid=$_REQUEST['userid'];
		$this->db->where('username',$username);
		if($userid!=0)
		{
			$this->db->where('id !=',$userid);
		}
		$q=$this->db->get('admin');
		if($q->num_rows>0)
		{
			$return="False";
		}
		else
		{
			$return="True";
		}
		echo $return;
	}
//------------------------ End function to check username existance ---------------------------//
//---------------------- Start function to check email existance --------------------------//
	function check_email_exist()
	{
		$email=$_REQUEST['email'];
		$userid=$_REQUEST['userid'];
		$this->db->where('email',$email);
		if($userid!=0)
		{
			$this->db->where('id !=',$userid);
		}
		$q=$this->db->get('admin');
		if($q->num_rows>0)
		{
			$return="False";
		}
		else
		{
			$return="True";
		}
		echo $return;
	}
//------------------------ End function to check email existance ---------------------------//



	//------------------------ Start function to check phone number existance in database ---------------------------//
	function check_phone_exist()
	{
		$phone=$_REQUEST['phone'];
		$userid=$_REQUEST['userid'];
		$this->db->where('phone_no',$phone);
		$this->db->where('id !=',$userid);
	
		$q=$this->db->get('admin');
		//echo $this->db->last_query();die;
		if($q->num_rows>0)
		{
			echo "False";
		}
		else
		{
			echo "True";
		}
	}
//------------------------- End function to check phone number existance in database ------------------------------//	


//--------------- Start function to load the edit page ---------------------------//
	function edit()
	{
		//echo $this->input->post('edit_id');die;
		if($this->input->post('edit_id')!='')
		{
			$result['success_msg'] 	= $this->session->userdata('success_msg');
			$result['error_msg'] 	= $this->session->userdata('error_msg');
		
			$this->session->set_userdata('success_msg', "");
			$this->session->set_userdata('error_msg', "");
			$result['rows']=$this->model_admin_management->edit($this->input->post('edit_id'));
			$this->load->view('admin/admin_management/edit',$result);
		}
		else
		{
			redirect('admin/school');
		}
	}
//----------------- End function to load the edit page ----------------------------//

//------------------ Start function to update school details ---------------------------//
	function update()
	{
		//echo "JAYATISH";die;
		$return=$this->model_admin_management->update();
		redirect('admin/admin_management');
	}
//-------------------- ENd function to update school details ---------------------------//

//--------------------- Start function to delete school details permanenetly -------------------------//
	function delete()
	{
		$result=$this->model_school->delete();
		redirect('admin/school');
	}
//---------------------- End function to delete school details permanently --------------------------//

//------------------------ Start function to update status of school -------------------------//
	function update_status()
	{
		$result=$this->model_admin_management->update_status();
		redirect('admin/admin_management');
	}
//-------------------------- End function to update status of school --------------------------//

	function pdf()
		{
			if($this->input->post('edit_id')!=''){
			$data['result'] = $this->model_school->download_pdf($this->input->post('edit_id'));
			//echo '<pre>';print_r($data['result']);die;
			//print_r($data['result'] [0]->school_id);die;
			    //$root = $this->config->item('base_url');				 		
		    	$this->load->helper('pdf_helper');
		    	$data1['result'].='<div style="text-align:center; margin:0 auto;width:100%"><img src="'.base_url().'uploads/logo/thumbs/'.LOGO.'" alt="My image" /></div><br/><br/>';
			   	$data1['result'].='<table width="100%">';
			   	$data1['result'].='<thead>';
            	$data1['result'].='<tr>';
                $data1['result'].='<th style="text-decoration:underline; font-weight:bold;width:29%; padding:0 2%">Name</th>';
                $data1['result'].='<th style="text-decoration:underline; font-weight:bold;">Phone</th>'; 
                $data1['result'].='<th style="text-decoration:underline; font-weight:bold;">Principal Name</th>'; 
                $data1['result'].='<th style="text-decoration:underline; font-weight:bold;">Teachers Name</th>'; 
                $data1['result'].='<th style="text-decoration:underline; font-weight:bold;">Address</th>';                           
            	$data1['result'].='</tr>';
            	$data1['result'].='</thead>';
            	$data1['result'].='<tr>';
				$data1['result'].='<td style="width:29%; padding:0 2%">'.$data['result'][0]->school_name.'</td>';
				$data1['result'].='<td>'.$data['result'][0]->school_phone.'</td>';
				if($data['result'][0]->type == '0'){
				$data1['result'].='<td>'.$data['result'][0]->full_name.'</td>';
				}else{
					$data1['result'].='<td>'.$data['result'][0].'</td>';
				}
			
				if(count($data['result'])>0)
				{
					$data1['result'].='<td>';
					foreach($data['result'] as $teacher)
					{
						//echo $teacher->type.'<br/>';
						if($teacher->type=='1')
						{
							//$data1['result'].='<td>'.$teacher->full_name.'</td>';
							//echo "TEST<br/>";
							$data1['result'].=$teacher->full_name."<br/>";
						}
					}
					$data1['result'].='</td>';
				}
				else
				{
					$data1['result'].='<td>'.$data['result'][0] .'</td>';
				}		
				$data1['result'].='<td>'.$data['result'][0]->school_address.'</td>';					
				$data1['result'].='</tr>';
            	$data1['result'].='<tbody>'; 
                $data1['result'].='</tbody>';
				$data1['result'].='</table>';
							
				
		       	$this->load->view('admin/school/pdfreport', $data1);
		       }else{
		       	redirect('admin/school/');
		       }
		   		
		}
}