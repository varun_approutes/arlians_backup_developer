<?php 
class Dashboard extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('admin/modeladmin');
		$this->modeladmin->AllSiteSettings();
	}
	
	function index()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");

		$result['admin_details'] = $this->modeladmin->User_Details();
		//var_dump($result);die();
		$this->load->view('admin/dashboard',$result);
	}
    
	
	
	
        
}