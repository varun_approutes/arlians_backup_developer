<?php 
class Cms extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('admin/modeladmin');
		$this->modeladmin->AllSiteSettings();
		$this->load->model('admin/model_cms');
		if($this->session->userdata('admin_user_data')=='')
		{
			redirect('admin');
		}
	}
	
	function index()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
			
		
		$result['rows'] = $this->model_cms->GetAllList();
		//echo '<pre>';print_r($result['rows']);die;
		$this->load->view('admin/cms/list',$result);
		
		
	}

	function add()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
		$this->load->view('admin/cms/add',$result);
		
	}

//---------------------- End function to show college / school list --------------------------//
//--------------- Start function to insert school details ------------------------//
	function insert()
	{
		$result=$this->model_cms->insert();
		redirect('admin/cms');
	}
//----------------- End function to insert  details -------------------------//

//------------------------ Start function to check email existance in database ---------------------------//
	function check_email_exist()
	{
		$email=$_REQUEST['email'];
		$userid=$_REQUEST['userid'];
		$this->db->where('email',$email);
		if($userid!='0')
		{
			$this->db->where('id !=',$userid);
		}
		$q=$this->db->get('student');
		if($q->num_rows>0)
		{
			echo "False";
		}
		else
		{
			echo "True";
		}
	}
//------------------------- End function to check email existance in database ------------------------------//



//--------------- Start function to load the edit page ---------------------------//
	function edit()
	{
		
		if($this->input->post('edit_id')!='')
		{
			$result['success_msg'] 	= $this->session->userdata('success_msg');
			$result['error_msg'] 	= $this->session->userdata('error_msg');
		
			$this->session->set_userdata('success_msg', "");
			$this->session->set_userdata('error_msg', "");
			$result['rows']=$this->model_cms->edit($this->input->post('edit_id'));
			$this->load->view('admin/cms/edit',$result);
		}
		else
		{
			redirect('admin/cms');
		}
	}
//----------------- End function to load the edit page ----------------------------//

//------------------ Start function to update  details ---------------------------//
	function update()
	{
		
		$return=$this->model_cms->update();
		redirect('admin/cms');
	}
//-------------------- ENd function to update  details ---------------------------//

//--------------------- Start function to delete  details permanenetly -------------------------//
	function delete()
	{
		$result=$this->model_cms->delete();
		redirect('admin/cms');
	}
//---------------------- End function to delete  details permanently --------------------------//

//------------------------ Start function to update status of  -------------------------//
	function update_status()
	{
		$result=$this->model_cms->update_status();
		redirect('admin/cms');
	}
//-------------------------- End function to update status of  --------------------------//
//-------------------- Start function to show class year section -------------------------------//
	function show_class_year()
	{
		$data='';
		$value=$_REQUEST['value'];
		$type_name=$_REQUEST['type_name'];
		if($type_name==0)
		{
			$data.='<div class="form-group">
		            <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Class</label>
		                <div class="col-lg-7">	                            
	                        <select class="form-control" id="class" name="class">
	                            <option value="">Select Class</option>';
	                            for($i=1;$i<=12;$i++)
	                            {
	                            $data.='<option value="'.$i.'">Class "'.$i.'</option>';
	                            }
	                $data.='</select>
	                        <p style="color:#A94442" id="class_error"></p>                               
		                </div>
		            </div>';
		}
		else
		{
			$get_year=$this->model_student->GetYear($value);
			$data.='<div class="form-group">
		            <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Years</label>
		                <div class="col-lg-7">	                            
	                        <select class="form-control" id="class" name="class">
	                            <option value="">Select Years</option>';
	                            for($years=1;$years<=$get_year;$years++)
	                            {
	                            $data.='<option value="'.$years.'">'.$years.' Years</option>';
	                            }
	                $data.='</select>
	                        <p style="color:#A94442" id="class_error"></p>                               
		                </div>
		            </div>';
		}
		echo $data;
	}
//---------------------- End function to show class year section -------------------------------//



}