<?php
class AppController extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
    }
    protected function header_footer($opta=array())
    {
        $heading="";
        $title="";
        if(!empty($opta['title']))
            $title=$opta['title'];

        else
            if(!empty($opta['title']))
                $heading=$opta['title'];
        $include['title']=$title;
        $include['heading']=$heading;
        $include['header']=$this->load->view('admin/header',$include,true);
        $include['footer']=$this->load->view('admin/footer','',true);
        return $include;
    }
}