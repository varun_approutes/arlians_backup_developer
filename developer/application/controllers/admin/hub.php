<?php

class Hub extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/modeladmin');
        $this->modeladmin->AllSiteSettings();
        $this->load->model('admin/model_cms');
        $this->load->model('admin/model_hub');
        if ($this->session->userdata('admin_user_data') == '') {
            redirect('admin');
        }
    }

    function updateBingStr() {
        $row = 1;
        $res = array();

        if (($handle = fopen("data/test.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                for ($c = 0; $c < $num; $c++) {
                    //echo $data[$c] . "<br />\n";
                    $res[$data[0]] = $data[1];
                }
            }
            fclose($handle);
            
        }

        $this->model_hub->saveBing($res);
    }

    function reportHub() {
        $data['res'] = $this->model_hub->fetchReportedHub();
        //echo '<pre>'; var_dump($data['res']); 
        // die ;
        $this->load->view('admin/report/list', $data);
    }

    function stupdt($hid) {
        //  echo "hello stupdt controller";
        //   print_r($hid) ;
        // exit ;
        $id = $hid;

        //  $data['res'] = $this->model_hub->statusupdt($id);
        $this->model_hub->statusupdt($id);
        $data1['res'] = $this->model_hub->fetchReportedHub();
        $this->load->view('admin/report/list', $data1);
    }

    function subsector_view(){
       // echo "hello";
      //  exit;
      $data['res'] = $this->model_hub->subsectorview();  
      
        $this->load->view('admin/report/subsector_view', $data);
        
    }
    function subsector_edit($subsector_code){
        $code = $subsector_code ;
     //   print_r($code);
      // echo "hello";
      // exit; 
        $data['res'] = $this->model_hub->subsectoreditview($code);
         $this->load->view('admin/report/subsector_edit', $data);
    }
    function  subsectorupdt(){
       //  echo "hello";
     
        $subsector_code = $this->input->post('subsector_code') ;
        $subsector_name = $this->input->post('subsector_name') ;
        $bing_query_str = $this->input->post('bing_query_str') ;
        
        $data = array(
       'subsector_code' => $this->input->post('subsector_code') ,
       'subsector_name' => $this->input->post('subsector_name') ,
       'bing_query_str' => $this->input->post('bing_query_str') 
          ); 
        // print_r($subsector_code);
        // print_r($data);
        // exit; 
          $this->model_hub->subsectorpdt($data,$subsector_code) ; 
        $dat['res'] = $this->model_hub->subsectorview();  
      
        $this->load->view('admin/report/subsector_view', $dat);
        
    }
    
    
    function bussnissview(){
        
      //  echo "hello";
        
        $data['res'] = $this->model_hub->bussniss_view();  
      
        $this->load->view('admin/report/ar_bussiness_view', $data); 
        
    }
    
    function bussnissedit($bussiness_code){
       // echo "hello";
       $code = $bussiness_code ;
       // print_r($code) ;
      //  exit ;
         $data['res'] = $this->model_hub->bussniss_edit($code);  
      
        $this->load->view('admin/report/ar_bussiness_edit', $data); 
        
    }
    
    function bussnissupdt(){
        //echo "hello updt";
        
         $bussiness_code = $this->input->post('bussiness_code') ;
    //    $bussiness_name = $this->input->post('bussiness_name') ;
        
         $data = array(
       'bussiness_code' => $this->input->post('bussiness_code') ,
       'bussiness_name' => $this->input->post('bussiness_name')       
          );
      //   print_r($bussiness_code);
     //    print_r($data);
     //    exit; 
         $this->model_hub->bussnisspdt($data,$bussiness_code) ; 
         $dat['res'] = $this->model_hub->bussniss_view(); 
      
        $this->load->view('admin/report/ar_bussiness_view', $dat);
         
    }
    function industry_view (){
        
          // echo "hello ";
           
         $data['res'] = $this->model_hub->industryview();  
      
        $this->load->view('admin/report/ar_industry_view', $data);   
           
    }
    
    function industry_edit($industry_code){
        $code = $industry_code ;
        // echo "hello ";
       // print_r($code);
       // exit ;
        
        $data['res'] = $this->model_hub->industryedit($code);  
      
       $this->load->view('admin/report/ar_industry_edit', $data);            
        
    }
    function industryupdt(){
        
       // echo "hello updt";
        $industry_code = $this->input->post('industry_code') ;
      //  $industry_name = $this->input->post('industry_name') ;
      //  $profile_image = $this->input->post('profile_image') ;
        
         $data = array(
       'industry_code' => $this->input->post('industry_code') ,
       'industry_name' => $this->input->post('industry_name') , 
       'profile_image' => $this->input->post('profile_image')
          );
        //  print_r($industry_code);
      //  print_r($data);
      //  exit;
       $this->model_hub->industrypdt($data,$industry_code) ;  
       $dat['res'] = $this->model_hub->industryview();  
      
        $this->load->view('admin/report/ar_industry_view', $dat);
       
       
       
    }
    
    
    
}
