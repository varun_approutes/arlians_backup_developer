<?php 
class Site_settings extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		
		$this->load->model('admin/modeladmin'); // This is mandatory
		$this->load->model('admin/modelsite_settings');
		//$this->modeladmin->SiteSettingsDetails(); // This is mandatory
		$this->modeladmin->AllSiteSettings();
	}
	
	function index()
	{
		 $result['success_msg'] 	= $this->session->userdata('success_msg');
		 $result['error_msg'] 	= $this->session->userdata('error_msg');
	
		 $this->session->set_userdata('success_msg', "");
		 $this->session->set_userdata('error_msg', "");
			
		 $result['result']= $this->modelsite_settings->GetAllSettings();
		 $this->load->view('admin/site_settings/index',$result);
		
	}
//----------------------- Start function to load main add user section ---------------------------//    
	function update_settings()
	{
			//echo "PAPOAi";die;
		$return=$this->modelsite_settings->update_settings();
		redirect('admin/site_settings');
	}
//----------------------- End function to load main add user section --------------------------//
	
  
}
