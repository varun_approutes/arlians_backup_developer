<?php 
class Calendar extends CI_Controller
{
   function __construct()
	{
		parent::__construct();
		$this->load->model('admin/modeladmin');
		$this->modeladmin->AllSiteSettings();
		$this->load->model('admin/model_calendar');
		if($this->session->userdata('admin_user_data')=='')
		{
			redirect('admin');
		}
		
	}
	function index()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
		$this->load->view('admin/calendar/index',$result);
	}


	function add_holiday()
	{
		$result=$this->model_calendar->holiday_insert();

		//$this->load->view('admin/calendar/index',$result);
		redirect('admin/calendar');
	}
//----------------------- Start function to get Holiday List details --------------------------------//
	function getHolidayDetails()
	{
		$response='';
		$value=$_REQUEST['value'];
		$this->db->where('id',$value);
		$q=$this->db->get('holidays');
		//echo $this->db->last_query();die;
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$response=$result->id.'[DIGITAL_APTECH]'.$result->title.'[DIGITAL_APTECH]'.$result->description;
			}
		}
		echo $response;
	}
//------------------------- End function to get Holiday List details -------------------------------//
//----------------- Start function to update holiday details ------------------------------//
	function update_holiday()
	{
		$return=$this->model_calendar->UpdateHoliday();
		redirect('admin/calendar');
	}
//-------------------- End function to update holiday details -----------------------------//
//-------------------------- Start function to show the class topic details --------------------------//
	function getClassTopicDetails()
	{
		$class_name=$class_value=$topic_name=$topic_description=$class_date=$start_time=$end_time=$school_details='';
		$value=$_REQUEST['value'];
		$school_id=$this->session->userdata('admin_school_id');
		$school_type=$this->session->userdata('admin_school_type');
		$this->db->where('school_id',$school_id);
		$this->db->where('id',$value);
		$q=$this->db->get('class_topic');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				if($school_type==0)
				{
					$class_name="Class";
					$class_value="Class ".$result->class;
				}
				if($school_type==1)
				{
					$class_name="Years";
					$class_value=$result->class." Years";
				}
				$topic_name=$result->topic_name;
				$topic_description=$result->topic_description;
				$class_date=date('m-d-Y',strtotime($result->start_time));
				$start_time=date('h:i A',strtotime($result->start_time));
				$end_time=date('h:i A',strtotime($result->end_time));

				$school_details.='<select class="form-control" id="teacher_id" name="teacher_id">
			                                    <option value="">Select Teacher</option>';
			    $allTeacher=$this->model_calendar->GetAllTeacher($school_id);
                foreach($allTeacher as $teacher_result)
                {
                	if($teacher_result->id==$result->assigned_teacher)
                	{
                		$selected_teacher="selected='selected'";
                	}
                	else
                	{
                		$selected_teacher="";
                	}
                	$school_details.='<option value="'.$teacher_result->id.'" '.$selected_teacher.'>'.$teacher_result->full_name.'</option>';
                }
                $school_details.='</select><p style="color:#A94442" id="teacher_id_error"></p>';
                $topic_id=$result->id;
                $class_school_id=$result->school_id ;
			}
		}
		$return=$class_name.'[DIGITAL_APTECH]'.$class_value.'[DIGITAL_APTECH]'.$topic_name.'[DIGITAL_APTECH]'.$topic_description.'[DIGITAL_APTECH]'.$class_date.'[DIGITAL_APTECH]'.$start_time.'[DIGITAL_APTECH]'.$end_time.'[DIGITAL_APTECH]'.$school_details.'[DIGITAL_APTECH]'.$topic_id.'[DIGITAL_APTECH]'.$class_school_id;
		echo $return;
	}
//---------------------------- End function to show the class topic details --------------------------//

	function update_class_module()
	{
		$return=$this->model_calendar->update_class_module();
		redirect('admin/calendar');
	}
}