<?php 
class Change_password extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('admin/modeladmin');
		$this->modeladmin->AllSiteSettings();
	}
	
	function index()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
			
		if($this->session->userdata('admin_user_data')=='')
		{
			redirect('admin');
		}
		else
		{
			//$result['admin_details'] = $this->modeladmin->User_Details();
			$this->load->view('admin/change_password',$result);
		}
		
	}
//------------------- Start function to check wheather the entered password is right or wrong ------------------------------//
	function check_password()
	{
		$userid=$_REQUEST['userid'];
		$usertype=$_REQUEST['usertype'];
		$password=$_REQUEST['password'];
		//echo $userid.'@@@'.$usertype;die;
		if(($usertype=='Super_Admin') || ($usertype=='Admin'))
		{
			$this->db->where('password',md5($password));
			$this->db->where('id',$userid);
			$q=$this->db->get('admin');
		}
		else
		{
			$this->db->where('password',md5($password));
			$this->db->where('id',$userid);
			$q=$this->db->get('principal_teacher');
		}
		if($q->num_rows>0)
		{
			echo "True";
		}
		else
		{
			echo "False";
		}
	}
//-------------------- End function to check wheather the entered password is right or wrong -------------------------------//

//-------------------- Start function to update password -------------------------------//
	function update_password()
	{
		$return=$this->modeladmin->update_password();
		redirect('admin/change_password');
		
	}
//----------------------- End function to update password ---------------------------------//
        
}