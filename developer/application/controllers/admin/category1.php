<?php 
class Category extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->model('admin/modeladmin');
		$this->modeladmin->AllSiteSettings();
		$this->load->model('admin/model_member');
		$this->load->model('admin/model_category');
		$this->load->helper('common_helper');
		if($this->session->userdata('admin_user_data')=='')
		{
			redirect('admin');
		}
	}
	
	function index()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
		$result['rows'] = $this->model_category->GetAllList();
		$this->load->view('admin/category/list',$result);
			
	}
	function sectorlist()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
		$result['toplineid']=$this->uri->segment(4); 
		$result['rows'] = $this->model_category->GetAllSectorList($result['toplineid']);
		$this->load->view('admin/category/sectorlist',$result);
			
	}
	function subsectorlist()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
		$result['sector_id']=$this->uri->segment(4); 
		$result['rows'] = $this->model_category->GetAllSectorList($result['sector_id']);
		$this->load->view('admin/category/sectorlist',$result);
			
	}

	function add()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
		$this->load->view('admin/student/add',$result);
		
	}
//-------------------- Start function to show college / school list -------------------------//
	function show_type()
	{
		$data='';
		$type=$_REQUEST['type'];
		if($type==0)
		{
			$school_name='School';
		}
		else
		{
			$school_name='College';
		}
		/*$this->db->where('status','1');
		$this->db->where('type',$type);
		$list=$this->db->get('school');*/

		$this->db->select('school.*,principal_teacher.full_name');
		$this->db->from('school');
		$this->db->from('principal_teacher');
		$this->db->where('school.type',$type);
		$this->db->where('principal_teacher.status','1');
		$this->db->where('principal_teacher.type','0');
		$this->db->where('principal_teacher.school_id = school.id');
		$this->db->where('school.status','1');
		$list=$this->db->get();

		$data.='<div class="form-group">
		            <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*'.$school_name.' Name</label>';
		if($list->num_rows>0)
		{
			$data.='<div class="col-lg-7">
                        <select class="form-control" id="school_name" name="school_name" onchange="Show_Class_Year(this.value);">
                            <option value="">Select '.$school_name.'</option>';
			foreach($list->result() as $result)
			{
				$data.='<option value='.$result->id.'>'.$result->school_name.'</option>';
			}
			$data.='</select><p style="color:#A94442" id="school_name_error"></p></div>';
		}
		else
		{
			$data.='<div class="col-lg-7">
	                    <p class="form-control-static">No '.$school_name.' available</p> 
	                    <p style="color:#A94442" id="school_name_error"></p>  
	                </div>';
		}
		$data.='</div>[DIGITAL_APTECH]'.$list->num_rows;
		echo $data;
	}
//---------------------- End function to show college / school list --------------------------//
//--------------- Start function to insert school details ------------------------//
	function insert()
	{
		$result=$this->model_student->insert();
		redirect('admin/student');
	}
//----------------- End function to insert  details -------------------------//

//------------------------ Start function to check email existance in database ---------------------------//
	function check_email_exist()
	{
		$email=$_REQUEST['email'];
		$userid=$_REQUEST['userid'];
		$this->db->where('email',$email);
		if($userid!='0')
		{
			$this->db->where('id !=',$userid);
		}
		$q=$this->db->get('student');
		if($q->num_rows>0)
		{
			echo "False";
		}
		else
		{
			echo "True";
		}
	}
//------------------------- End function to check email existance in database ------------------------------//

//------------------------ Start function to check phone number existance in database ---------------------------//
	function check_phone_exist()
	{
		$phone=$_REQUEST['phone'];
		$userid=$_REQUEST['userid'];
		$this->db->where('phone_no',$phone);
		if($userid!='0')
		{
			$this->db->where('id !=',$userid);
		}
		$q=$this->db->get('student');
		//echo $this->db->last_query();die;
		if($q->num_rows>0)
		{
			echo "False";
		}
		else
		{
			echo "True";
		}
	}
//------------------------- End function to check phone number existance in database ------------------------------//	

//--------------- Start function to load the edit page ---------------------------//
	function edit()
	{
		
		if($this->input->post('edit_id')!='')
		{
			$result['success_msg'] 	= $this->session->userdata('success_msg');
			$result['error_msg'] 	= $this->session->userdata('error_msg');
		
			$this->session->set_userdata('success_msg', "");
			$this->session->set_userdata('error_msg', "");
			$result['rows']=$this->model_category->edit($this->input->post('edit_id'));
			
			
			$this->load->view('admin/category/edit',$result);
		}
		else
		{
			redirect('admin/member');
		}
	}
	function sectoredit()
	{
		
		if($this->input->post('edit_id')!='')
		{
			$result['success_msg'] 	= $this->session->userdata('success_msg');
			$result['error_msg'] 	= $this->session->userdata('error_msg');
		
			$this->session->set_userdata('success_msg', "");
			$this->session->set_userdata('error_msg', "");
			
			$result['rows']=$this->model_category->sectoredit($this->input->post('edit_id'));
			$result['toplineid']=$this->input->post('topline_id');
			$this->load->view('admin/category/sectoredit',$result);
		}
		else
		{
			redirect('admin/member');
		}
	}
	
//----------------- End function to load the edit page ----------------------------//

//------------------ Start function to update  details ---------------------------//
	function update()
	{
		
		$return=$this->model_category->update();
		redirect('admin/category/');
	}
	function sectorupdate()
	{
		$sector_id= $this->input->post('toplineid');
		$return=$this->model_category->sectorupdate();
		redirect('admin/category/sectorlist/'.$sector_id);
	}
//-------------------- ENd function to update  details ---------------------------//

//--------------------- Start function to delete  details permanenetly -------------------------//
	function delete()
	{
		$result=$this->model_member->delete();
		redirect('admin/member');
	}
//---------------------- End function to delete  details permanently --------------------------//

//------------------------ Start function to update status of  -------------------------//
	function update_status()
	{
		$result=$this->model_member->update_status();
		redirect('admin/member');
	}
//-------------------------- End function to update status of  --------------------------//
//-------------------- Start function to show class year section -------------------------------//
	function show_class_year()
	{
		$data='';
		$value=$_REQUEST['value'];
		$type_name=$_REQUEST['type_name'];
		if($type_name==0)
		{
			$data.='<div class="form-group">
		            <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Class</label>
		                <div class="col-lg-7">	                            
	                        <select class="form-control" id="class" name="class">
	                            <option value="">Select Class</option>';
	                            for($i=1;$i<=12;$i++)
	                            {
	                            $data.='<option value="'.$i.'">Class "'.$i.'</option>';
	                            }
	                $data.='</select>
	                        <p style="color:#A94442" id="class_error"></p>                               
		                </div>
		            </div>';
		}
		else
		{
			$get_year=$this->model_student->GetYear($value);
			$data.='<div class="form-group">
		            <label for="inputEmail1" class="col-lg-3 col-sm-2 control-label">*Years</label>
		                <div class="col-lg-7">	                            
	                        <select class="form-control" id="class" name="class">
	                            <option value="">Select Years</option>';
	                            for($years=1;$years<=$get_year;$years++)
	                            {
	                            $data.='<option value="'.$years.'">'.$years.' Years</option>';
	                            }
	                $data.='</select>
	                        <p style="color:#A94442" id="class_error"></p>                               
		                </div>
		            </div>';
		}
		echo $data;
	}
//---------------------- End function to show class year section -------------------------------//

	function addCorpusKey()
	{
		echo $api='curl -u 1213a261-d453-474d-bd8f-51e95d73802c:xPm7tN6BhxsA "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/public/ibmresearcher/label_search?query=Art&amp;amp;prefix=true&amp;amp;limit=40&amp;amp;concepts=true"';
		echo exec($api_doc_insert);
	}



}