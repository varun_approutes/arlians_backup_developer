<?php
require_once('AppController.php');
class Export extends AppController {
    private $username;
	private $password;
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('system_models/ar_subsector_model');
		$this->load->model('system_models/relevant_keys_model');
		$this->load->model('system_models/ar_request_access_page_model');
		$this->load->model('system_models/user_description_keywords_model');
		$this->username="1213a261-d453-474d-bd8f-51e95d73802c";
		$this->password="xPm7tN6BhxsA";
		$this->load->model('admin/modeladmin');
		$this->modeladmin->AllSiteSettings();
		$this->load->model('admin/model_member');
		$this->load->library('utility');
		if($this->session->userdata('admin_user_data')=='')
		{
			redirect('admin');
		}
    }
    public function index($slug='')
    {
        
    }
	public function delete_description_keyword($id="", $frmSecutity="")
	{
		if($this->utility->getSecurity()!=$frmSecutity)
		{
			$this->utility->setMsg('Invalid Security token','ERROR');
			redirect(base_url().'admin/export/description_concepts');
		}
		$id=base64_decode($id);
		if(!$id || !is_numeric($id))
		{
			$this->utility->setMsg('Invalid keyword selected','ERROR');
			redirect(base_url().'admin/export/description_concepts');
		}
		$isExist=$this->user_description_keywords_model->fetchRow(array('id'=>$id));
		if(!$isExist)
		{
			$this->utility->setMsg('Selected Keyword not found','ERROR');
			redirect(base_url().'admin/export/description_concepts');
		}
		$this->user_description_keywords_model->delete(array('id'=>$id));
		$this->utility->setMsg($isExist['keyword'].' has been Deleted','SUCCESS');
		redirect(base_url().'admin/export/description_concepts/view/'.base64_encode($isExist['user_id']));
	}
	public function description_concepts($action="",$user_id="",$frmSecutity="")
	{
		if($action)
		{
			if($action=="view")
			{
				$user_id=base64_decode($user_id);
				if(!$user_id || !is_numeric($user_id))
				{
					$this->utility->setMsg('Invalid user selected','ERROR');
					redirect(base_url().'admin/export/description_concepts');
				}
				$keywords=$this->user_description_keywords_model->fetchRecord(array('user_id'=>$user_id));
				$data=$this->header_footer(array(
					'title'=>  "USER-".$user_id." Keywords"
				));
				$data['user_id']=base64_encode($user_id);
				$data['rows']=$keywords;
				$this->load->view('admin/export/user_description_keywords',$data);
			}
			else if($action=="delete")
			{
				if($this->utility->getSecurity()!=$frmSecutity)
				{
					$this->utility->setMsg('Invalid Security token','ERROR');
					redirect(base_url().'admin/export/description_concepts');
				}
				$user_id=base64_decode($user_id);
				if(!$user_id || !is_numeric($user_id))
				{
					$this->utility->setMsg('Invalid user selected','ERROR');
					redirect(base_url().'admin/export/description_concepts');
				}
				$this->user_description_keywords_model->delete(array('user_id'=>$user_id));
				$this->utility->setMsg('User Keywords Deleted','SUCCESS');
				redirect(base_url().'admin/export/description_concepts');
			}
			else
			{}
		}
		else
		{
			$data=$this->header_footer(array(
				'title'=>  "Export Description Keywords"
			));
			$userKeywords=$this->ar_request_access_page_model->find();
			if(empty($userKeywords['ar_request_access_page']))
				$userKeywords['ar_request_access_page']=array();
			$data['rows']=$userKeywords['ar_request_access_page'];
			$this->load->view('admin/export/description_keywords',$data);
		}
	}
	public function description_concepts_db()
	{
		$dataXLS=array();
        array_push($dataXLS,array('User','Description','Concept','Score'));
		$descriptions=$this->ar_request_access_page_model->fetchRecord(array('bussinessdesc <>'=>''));
		$list=$this->getDocuments();
		foreach($descriptions as $value)
		{
			$key=array_search("user_".$value['mid'],$list['documents']);
			if($key)
			{
				$dataXLS=$this->getConcepts("user_".$value['mid'],$value['mid'],$value['bussinessdesc'],$dataXLS);
			}
			else
			{
				$data=array(
					'name'=>$value['bussinessname'],
					'description'=>$value['bussinessdesc']
				);
				
				$this->createDocuments('user_'.$value['mid'],$data);
				$dataXLS=$this->getConcepts("user_".$value['mid'],$value['mid'],$value['bussinessdesc'],$dataXLS);
			}
		}
		if(count($dataXLS)>1)
		{
			$time=(int)time();
			$tempInsert=array();
			$this->db->close();
			$this->db->initialize(); 
			$this->load->model('system_models/user_description_keywords_model');
			foreach($dataXLS as $key=>$value)
			{
				if($key==0)
					continue;
				if(!is_numeric(str_replace('user_','',$value[0])) || !$value[2])
					continue;
				if(empty($tempInsert[str_replace('user_','',$value[0])]))
				{
					$tempInsert[str_replace('user_','',$value[0])]=array();
					$this->user_description_keywords_model->delete(array('user_id'=>str_replace('user_','',$value[0])));
				}
				array_push($tempInsert[str_replace('user_','',$value[0])],array(
					'user_id'=>str_replace('user_','',$value[0]),
					'keyword'=>$value[2],
					'score'=>$value[3],
					'date_of_creation'=>$time,
				));
			}
			foreach($tempInsert as $key=>$value)
			{
				$this->user_description_keywords_model->insert_batch($value);
			}
			redirect(base_url().'admin/export/description_concepts');
			/*******************Build CSV file and Download*************
			try {
				
				$this->array_to_csv_download($dataXLS,$time.'.csv');
			} catch (Exception $e) {
				 echo $e->getMessage();
				 exit;
			}
			*/
			
		}
		die;
	}
	private function getConcepts($documentName="",$mid,$description,$dataXLS=array())
	{
		if(!$documentName)
			return $dataXLS;
		$apix='curl -u '.$this->username.':'.$this->password.' -G -d "level=1" "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/ed0tiliprory9/arlians/documents/'.$documentName.'/related_concepts"';
		$resultx = exec($apix);
		$resx = json_decode($resultx,true);
		if(!empty($resx['concepts']))
		{
			foreach($resx['concepts'] as $key=>$value)
			{
				if($value['concept']['label'])
				{
					if($this->hasSpeclChar($value['concept']['label']) || $value['score']<0.89)
						continue;
					if($key==0)
						array_push($dataXLS,array($mid,$description,$value['concept']['label'],$value['score']));
					else
						array_push($dataXLS,array($mid,'',$value['concept']['label'],$value['score']));
				}
			}
		}
		return $dataXLS;
	}
	
	private function hasSpeclChar($string)
	{
		if(preg_match('/[^a-zA-Z\-_ ]/', $string))
			return true;
		else
			return false;
	}
	
	private function createDocuments($documentName="",$dataSet=array())
	{
		$content_type="text/plain";
		$documentName=strtolower(str_replace(' ','',$documentName));
		if(strstr($dataSet['description'],'<'))
			$content_type="text/html";
		$data=array(
			'label'=>$documentName,
			'parts'=>array(
				array(
					'name'=>$dataSet['name'],
					'content-type'=>$content_type,
					'data'=>$dataSet['description']
				),
			)
		);
		$api='curl -X PUT -u '.$this->username.':'.$this->password.' -d \''.json_encode($data).'\' "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/ed0tiliprory9/arlians/documents/'.$documentName.'"';
		$result = exec($api);
		$res = json_decode($result,true);
		return $res;
	}
	private function getDocuments()
	{
		$api='curl -u '.$this->username.':'.$this->password.' "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/ed0tiliprory9/arlians/documents"';
		$result = exec($api);
		$res = json_decode($result,true);
		
		foreach($res['documents'] as $key=>$value)
		{
			$res['documents'][$key]=str_replace('/corpora/ed0tiliprory9/arlians/documents/','',$value);
		}
		return $res;
	}
	
	private function array_to_csv_download($array, $filename = "export.csv", $delimiter=";") {
       // open raw memory as file so no temp files needed, you might run out of memory though
       $f = fopen('php://memory', 'w'); 
       // loop over the input array
       foreach ($array as $line) { 
           // generate csv lines from the inner arrays
            fputcsv($f, $line, $delimiter); 
       }
       // reset the file pointer to the start of the file
       fseek($f, 0);
       // tell the browser it's going to be a csv file
       header('Content-Type: application/csv');
       // tell the browser we want to save it instead of displaying it
       header('Content-Disposition: attachment; filename="'.$filename.'";');
       // make php send the generated csv lines to the browser
       fpassthru($f);
       die;
   }
}
