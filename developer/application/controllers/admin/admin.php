<?php 
class Admin extends CI_Controller
{
    function __construct()
	{
		parent::__construct();
		$this->load->helper('cookie'); 
		$this->load->model('admin/modeladmin'); 
		$this->load->model('model_memberprofile');
		$this->load->model('model_category');
		$this->load->model('model_hub');
		$this->load->library('bingsearch');
		$this->modeladmin->AllSiteSettings();
		
	}
	
	function index()
	{
		$result['success_msg'] 	= $this->session->userdata('success_msg');
		$result['error_msg'] 	= $this->session->userdata('error_msg');
	
		$this->session->set_userdata('success_msg', "");
		$this->session->set_userdata('error_msg', "");
			
		if($this->session->userdata('admin_user_data')=='')
		{
			$this->load->view('admin/login',$result);
		}
		else
		{
			redirect('admin/dashboard');
		}
		
	}
    
	function authenticate()
	{
		
		if($this->input->post('remember_me')!='')
		{
			
			setcookie('admin_cookie_value',1,time()+60*60*60*24*365,'/');
			setcookie('admin_cookie_usertype',$this->input->post('usertype'),time()+60*60*60*24*365,'/');
			setcookie('admin_cookie_username',$this->input->post('username'),time()+60*60*60*24*365,'/');
			setcookie('admin_cookie_password',$this->input->post('password'),time()+60*60*60*24*365,'/');
		}
		else
		{
			
			setcookie('admin_cookie_value','',1,'/');
			setcookie('admin_cookie_usertype','',1,'/');
			setcookie('admin_cookie_username','',1,'/');
			setcookie('admin_cookie_password','',1,'/');
		}
		
		$result=$this->modeladmin->UserAuthenticate();
		
		if($result==1)
		{
			redirect('admin/dashboard');
		}
		else
		{
			$this->session->set_userdata('error_msg','Authentication problem. Please try again.');
			redirect('admin');
		}
	}
//----------------- Start function to logout in admin panel -------------------------------//
	function logout()
	{
		$array_items = array('admin_user_data' => '', 'admin_user_name' => '','admin_user_type' => '','admin_school_id' => '','admin_school_type' => '');

		$this->session->unset_userdata($array_items);
		redirect('admin');
	}
//------------------- End function to logout in admin panel -------------------------------//
	function clean($string) 
	{
	   return preg_replace("/[^a-zA-Z0-9\s]/", "", $string);
	}


	function insertNewsImage()
	{
		$d = $this->modeladmin->ar_subsector_list();
		$own_subsector = array();
		//echo '<pre>'; var_dump($d); die();
		foreach($d as $row)
		{
			$own_subsector[] = $row['sub_sector_id'];
		}
		//echo '<pre>'; var_dump($own_subsector); die();
		$news = array();
		$j = 0;
		foreach($own_subsector as $code)
        {
        	$bingData = $this->model_memberprofile->check_news_by_sub_sector_old($code);
			//echo '<pre>'; var_dump($bingData);
			$z = 0;
        	foreach($bingData as $bnData)
        	{
        		$data = json_decode($bnData['jsondata']);
        		$res = $data->d->results; 
			//echo '<pre>'; var_dump($res); die();
	        	if($res != NULL)
	        	{
		        	$i=0;
		        	foreach($res as $r) 
		        	{
		        		$news[$j][$z][$i]['id'] = $r->ID;
		        		//$news[$j][$z][$i]['title'] = $this->clean($r->Title);
		        		$news[$j][$z][$i]['url'] = $r->Url;
		        		//$news[$j][$z][$i]['newsimg'] = '';
		        		//$news[$j][$z][$i]['source'] = $r->Source;
		        		//$news[$j][$z][$i]['desc'] = $this->clean($r->Description);
						//$news[$j][$z][$i]['date'] = calculateTimeFromPost($r->Date, date("Y-m-d H:i:s"), "s");
		        		$news[$j][$z][$i]['type'] = 'bing';
		        		//$news[$j][$z][$i]['expand'] = false;
                        //$news[$j][$z][$i]['share_count'] = $this->model_hub->getNewsShareCount($r->ID);  
                        //$news[$j][$z][$i]['comment_count'] = $this->model_hub->getNewsCommentCount($r->ID);   
						//$news[$j][$z][$i]['show_comments'] = false;
						//$news[$j][$z][$i]['open_comment_box'] = false;


                        //$newsId = $r->ID;
                        //$news[$j][$z][$i]['like_count'] = $this->model_hub->getNewsLike($newsId);

                        //$newsComment = $this->model_hub->getNewsCommentsDetails($newsId);
						
						//$news[$j][$z][$i]['like'] = $this->model_hub->checkNewsLike($r->ID,$user_id);

                        //$news[$j][$z][$i]['comments'] = $newsComment;
						//$news[$j][$z][$i]['comment'] = '';

		        	}
		        }
		        else
		        {
		        	$i=0;
		        	foreach($data as $r)
		        	{
		        		$news[$j][$z][$i]['id'] = $r->id;
		        		//$news[$j][$z][$i]['title'] = $this->clean($r->title);
		        		$news[$j][$z][$i]['url'] = $r->url;
						//$news[$j][$z][$i]['newsimg'] = getImageFromUrl($r->Url);
						//$news[$j][$z][$i]['newsimg'] = '';
		        		//$news[$j][$z][$i]['source'] = $r->concept;
		        		//$news[$j][$z][$i]['desc'] = $this->clean($r->text);
		        		//$news[$j][$z][$i]['date'] = '1 day';
		        		$news[$j][$z][$i]['type'] = 'bing';
						//$news[$j][$z][$i]['expand'] = false;
						//$news[$j][$z][$i]['show_comments'] = false;
						//$news[$j][$z][$i]['open_comment_box'] = false;

                        //$newsId = $r->id;

                        //$news[$j][$z][$i]['like_count'] = $this->model_hub->getNewsLike($newsId);

                        //$newsComment = $this->model_hub->getNewsComments($newsId);

		        	}
		        }

		        $z++;
		    }
	        $j++;
        }
		
        $ns = array();
        foreach($news as $row)
        {
        	foreach($row as $r)
        	{
        		foreach($r as $fi)
        		{
        			$ns[] = $fi;
        		}
        	}
        }
//echo '<pre>'; var_dump($ns); die();
        foreach($ns as $row)
        {
        	if($row['id'] != NULL)
        	{
	        	$final['news_id'] = $row['id'];
	        	$final['image_url'] = getImageFromUrl($row['url']);

	        	$chk = $this->modeladmin->checkImageExist($row['id']);
	        	if(!$chk)
	        	{
	        		echo $this->modeladmin->insertNewsImage($final);
	        		echo $row['id'];
	        		echo '<br>';
	        	}
	        	
        	}
        }
	}

	function insertNewsImageNew()
	{
		$result = $this->modeladmin->getAllBingNews();
		$news = array();

		foreach($result as $key=>$row)
		{
			$data = $row['jsondata'];
			$data = json_decode($data);

			$res = $data->d->results;
			if($res != NULL)
			{
				foreach($res as $k=>$r)
				{
					$news[$key][$k]['id'] = $r->ID;
					$news[$key][$k]['url'] = $r->Url;
				}
			}
			else
			{
				foreach($res as $k=>$r)
				{
					$news[$key][$k]['id'] = $r->id;
					$news[$key][$k]['url'] = $r->url;
				}

			}
		}
//echo '<pre>'; var_dump($news);
		foreach($news as $row)
		{
			foreach($row as $r)
			{
	        	$final['news_id'] 	= $r['id'];
	        	$final['image_url'] = getImageFromUrl($r['url']);
	        	//$final['logo'] 		= @getFavicon($r['url']);
	        	//echo '<pre>'; var_dump($final); die();
	        	$chk = $this->modeladmin->checkImageExist($r['id']);
	        	
	        	if(!$chk)
	        	{
	        		//pr($final,false);
	        		$this->modeladmin->insertNewsImage($final);
	        		
	        		echo $r['id']."<br />";
	        	}
	        	else
	        	{
	        		$this->modeladmin->updateNewsLogo($final);
	        	}
			}
		}
		return 1;
		exit;
		//echo '<pre>'; var_dump($news); die();
	}

	public function insertNewsCron()
	{
        $activeSubSectors = $this->model_memberprofile->fetchActiveSubSectorCode();

        foreach($activeSubSectors as $cd)
        {
        	$cd = explode('-', $cd['ans_set_cat_id']);
        	$code = $cd[2];

        	$d = $this->model_memberprofile->check_news_by_sub_sector($code);

        	    $current_date = date('Y-m-d'); 
        		$result = $this->model_memberprofile->checkNewsAvailable($current_date,$code);

        	if($d[0]['c'] < 7 && $result)
        	{
                $str = get_bing_str($code);
                $string = $str['bing_query_str'];

	        	$r = $this->bingsearch->query('News',array(
	                'Query'=>"'".$string."'",//string
	                'Adult'=>"'Moderate'",//string
	                //'ImageFilters'=>"'Size:Small+Aspect:Square'",//string
	                //'Latitude'=>47.603450,//double
	                //'Longitude'=>-122.329696,//double
	                'Market'=>"'en-US'",//string
	                'NewsCategory'=>"'rt_Business'",//string
	                'Options'=>"'EnableHighlighting'",//string
	                'NewsSortBy'=>"'Relevance'",//string
	                
	            )); 

	            $data['mid'] = 0;
	            $data['jsondata']= json_encode($r);
	            $data['sub_sector_id'] = $code;
	            $data['news_from'] = 'bing';
	            $data['on_date'] = date('Y-m-d');

	        	$res = $this->model_category->insertBingNewsRespectToSubsector($data);
        	}
        	else
        	{
        		$current_date = date('Y-m-d'); 
        		$res = $this->model_memberprofile->get_lastest_new_id($code);
        		$result = $this->model_memberprofile->checkNewsAvailable($current_date,$code);

        		if($result)
        		{
	        		$str = sub_sector_name($code);
	        		$string = $str['subsector_name'];

		        	$r = $this->bingsearch->query('News',array(
		                'Query'=>"'".$string."'",//string
		                'Adult'=>"'Moderate'",//string
		                //'ImageFilters'=>"'Size:Small+Aspect:Square'",//string
		                //'Latitude'=>47.603450,//double
		                //'Longitude'=>-122.329696,//double
		                'Market'=>"'en-US'",//string
		                'NewsCategory'=>"'rt_Business'",//string
		                'Options'=>"'EnableHighlighting'",//string
		                'NewsSortBy'=>"'Relevance'",//string
		                
		            ));

		            $data['mid'] = 0;
		            $data['jsondata']= json_encode($r);
		            $data['sub_sector_id'] = $code;
		            $data['news_from'] = 'bing';
		            $data['on_date'] = date('Y-m-d'); 
		           // $data['al_id'] =  $res[0]['al_id'];
		            //$data['type'] =  'bing'; 

		        	$res = $this->model_category->updateBingNewsRespectToSubsector($data);
        		}
        	}
        }

        return 1;
        exit;
	}
	
	function cronTest(){
			//echo getcwd();
			//exit;
			$to      = 'rityan@digitalaptech.com';
			$subject = 'test arlians cron';
			$message = 'hello';
			//$headers = 'From: webmaster@example.com' . "\r\n" .
				//'Reply-To: webmaster@example.com' . "\r\n" .
				//'X-Mailer: PHP/' . phpversion();
			//mail($to, $subject, $message, $headers);
		 $from = "subscriptions@arlians.com";
        $this->sentmail($to, $message, $subject, $from);
	}
     function sentmail($mail, $mailBody, $subject, $from) {

        $this->load->library('email');
        $this->email->from($from, 'Arlians');
        $this->email->to($mail);
        $this->email->set_mailtype('html');

        $this->email->subject($subject);

        $this->email->message($mailBody);

        $this->email->send();
        return true;
    }

    function insertNewslogo()
	{
		$result = $this->modeladmin->getAllBingNews();
		$news = array();

		foreach($result as $key=>$row)
		{
			$data = $row['jsondata'];
			$data = json_decode($data);

			$res = $data->d->results;
			if($res != NULL)
			{
				foreach($res as $k=>$r)
				{
					$news[$key][$k]['id'] = $r->ID;
					$news[$key][$k]['url'] = $r->Url;
				}
			}
			else
			{
				foreach($res as $k=>$r)
				{
					$news[$key][$k]['id'] = $r->id;
					$news[$key][$k]['url'] = $r->url;
				}

			}
		}
//echo '<pre>'; var_dump($news);
		foreach($news as $row)
		{
			foreach($row as $r)
			{
	        	$final['news_id'] 	= $r['id'];
	        	$final['logo'] 		= getFavicon($r['url']);

	        	$chk = $this->modeladmin->checkImageExist($r['id']);
	        	
	        	if(!$chk)
	        	{
	        		$this->modeladmin->insertNewsLogo($final);
	        		
	        		echo $r['id']."<br />";
	        	}
	        	else
	        	{
	        		$this->modeladmin->updateNewsLogo($final);
	        	}
			}
		}
		return 1;
		exit;
	}

	public function startCron()
	{
		$result = $this->insertNewsCron();
		$res;
		$r;
		$array = array();

		if($result == 1)
		{
			$res = $this->insertNewsImageNew();
		}
		if($res == 1)
		{
			$r = $this->insertNewslogo();
		}

		if($r == 1)
		{
			$arr['responce'] = 'success';
		}
		else
		{
			$arr['responce'] = 'fail';
		}

		echo json_encode($arr);
	}
}