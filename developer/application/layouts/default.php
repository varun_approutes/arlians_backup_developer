<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Arlians</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/bootstrap.css" rel="stylesheet">
        <!-- Bootstrap theme -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/bootstrap-theme.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_phase2/wizard/css/jquery.pagepiling.css" />

        <!-- Owl Carousel Assets -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/owl.theme.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_phase2/wizard/css/component.css" />
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/modernizr.custom.js"></script>

        <!-- scrollbar -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/jquery.mCustomScrollbar.min.css" rel="stylesheet">   



        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/dots.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/fonts/fonts.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/responsive.css" rel="stylesheet">  
        <!-- CSS -->
        <link href="<?php echo base_url(); ?>assets_phase2/css/alertify.css" rel="stylesheet"> 
        <!-- Default theme -->
        <link href="<?php echo base_url(); ?>assets_phase2/css/default.css" rel="stylesheet"> 
		
<?php  if(strpos($_SERVER['HTTP_HOST'],'www')===false)
    {
        $config['base_url'] = 'http://arlians.com/developer/';
    }
    else
    {
        $config['base_url'] = 'http://www.arlians.com/developer/';
    }
?>
<script>
   
    var CORE_URL = '<?php echo $config['base_url']?>';
		
</script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url(); ?>assets_phase2/js/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/jquery.fullPage.js"></script> 
        <!--Alertify-->
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.core.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.default.css">-->
        <!--Knockout JS-->
        <script src="<?php echo base_url(); ?>assets/js/knockout-3.2.0.js"></script>
        <!-- UserProxy JS -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/UserProxy.js"></script>
        <!-- common-helper JS -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/common-helper.js"></script>
        <!-- JavaScript -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/alertify.js"></script>
		



 <!--        <script src="<?php echo base_url(); ?>assets/js/alertify.js"></script>-->
    </head>

    <body class="signup_page step1 step2" >
        <div id="WizardCtrl" style="display:none">
            <div id="pagepiling" >

                <div id="signup" class="section">
                    <header class="top_header">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <a href="landing.html" class="logo"><img src="<?php echo base_url(); ?>assets_phase2/wizard/images/logo.png" alt=""></a>
                                </div>

                            </div>
                        </div>
                    </header>
                    <div class="container" data-bind="visible:step_one_selection">                    
                        <div class="signup_cont mCustomScrollbar">
                            <div>My business
                                <!-- ko foreach:user_selected_buisness -->
                                <span  class="selecte-biz"> <span data-bind="text:buisness_name+','"></span><i data-bind="click:$root.removeUserBuizness" class="fa fa-times-circle-o"></i></span>
                                <!-- /ko -->
                               
                                <select  class="cs-select cs-skin-border" data-bind="options: buisness_lists,
                                        optionsText: 'bussiness_name',
                                        optionsValue: 'bussiness_code',
                                        value:selected_Buisness">
                                </select>

                            </div> 

                            <span>in the field(s) of 
                                <span data-bind="visible:user_selected_sub_sector().length>0">
                                    <!-- ko foreach:user_selected_sub_sector -->
                                    <span  class="selecte-biz"><span data-bind="text:name+','"> </span><i class="fa fa-times-circle-o" data-bind="click:$root.removeUserSelectedSubSector "></i></span>
                                    <!-- /ko -->
                                    <a href="javascript:void(0)" data-bind="click:selectionTopLines" class="selected-sub_sec"></a></span> 
                            </span>
                            <a href="javascript:void(0)" data-bind="click:selectionTopLines,visible:user_selected_sub_sector().length==0" class="signup_nextlink"></a></span><br> 
                            <span>Simply put, my business is a</span><br>
                            <textarea type="text" value="" data-bind="value:user_unique_buisness" class="business_input" placeholder="e.g. Coffee Shop"></textarea>
                        </div>
                        <div class="steplast signup_buttons">
                            <a href="javascript:void(0);" class="white_signupbtn" data-bind="click:sendCombinationCode,visible:user_selected_buisness().length>0 && user_selected_sub_sector().length>0">Save</a>
                        </div>
                    </div> 
                    <div class="container" data-bind="visible:step_two_selection">
                        <div class="signup_step2">
                            <div class="sections">
                                <div class="section__content clearfix">
                                    <div class="mCustomScrollbar" data-bind="foreach:buisness_top_lists">
                                        <div class="card effect__click" onclick="animationBuisnessTopLines(this)" data-bind="click:$root.selectionBuisnessTopLines">
                                            <div class="card__front">
                                                <div class="card_img">
                                                    <img data-bind="attr:{src:path+'/'+profile_image}" alt="">
                                                </div>
                                                <span class="card__text" data-bind="text:top_name">Art &amp; Culture</span>
                                            </div>
                                            <div class="card__back">
                                                <div class="card_img">
                                                    <i class="fa fa-close close_card"></i>
                                                    <div class="selected">Selected</div>

                                                    <img data-bind="attr:{src:path+'/'+profile_image}" alt="">
                                                </div>
                                                <span class="card__text" data-bind="text:top_name">Art &amp; Culture</span>
                                            </div>
                                        </div> 
                                    </div>
                                </div> 
                            </div> 
                        </div>
                        <div class="signup_buttons">
                            <a href="javascript:void(0)" data-bind="click:selectionSectionOne" class="white_signupbtn">Back</a>
                            <a href="javascript:void(0)" data-bind="click:showSubSectorSection" class="white_signupbtn">Next</a>
                        </div>
                    </div> 
                    <div class="container" data-bind="visible:step_three_selection">
                        <div class="signup_step3">
                            <div class="step3_cont">
                                <div class="mCustomScrollbar">
                                    <!-- ko foreach: sub_sector_list -->
                                    <div class="art_culture">
                                        <h1 data-bind="text:toplinename">Art &amp; Culture </h1>
                                        <ul class="subtag_select" data-bind="foreach:subsector">
                                            <li onclick="animatedSubSector(this)" data-bind="text:name,click:$root.setUserSelectedSubSector">Tag name sub</li> 
                                        </ul>
                                    </div>
                                    <!-- /ko -->
                                </div>
                            </div>
                        </div>
                        <div class="signup_buttons">
                            <a href="javascript:void(0);" data-bind="click:selectionTopLines" class="white_signupbtn">Back</a>
                            <a href="javascript:void(0)" data-bind="click:setTheFirstView" class="white_signupbtn">Next</a>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- flipcard js -->
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/flip.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/wizard.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/selectstyle.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/selectFx.js"></script>
        <!-- input effect -->
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/classie.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/dots.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/jquery.dlmenu.js"></script>

        <script>
            addLanguageScript = function(lang,callback) {
                //debugger;
                var head = document.getElementsByTagName("head")[0],
                script = document.createElement('script');
                script.type = 'text/javascript'
                script.src = lang + '.js'
                head.appendChild(script);
                //callback(true);
            };
            $(window).load(function() {
                //setTimeout(function(){
                if (screen.width > 767) {  
                    $('#pagepiling').pagepiling({
                        menu: '#menu',
                            anchors: ['page1', 'page2', 'page3', 'page4'],
                            navigation: {
                            'textColor': '#f2f2f2',
                                    'bulletsColor': '#ccc',
                                    'position': 'left',
                                    'tooltips': ['Page 1', 'Page 2', 'Page 3', 'Page 4', 'Page 5']
                            }
                    });
                }
                                                    //},2000);
                $('#pagepiling').show();
            });
            $(function () {
                if (screen.width > 767) {                                            
                    addLanguageScript('<?php echo base_url(); ?>assets_phase2/landing/js/jquery.pagepiling');
                }                
            });
            [].slice.call(document.querySelectorAll('.dotstyle > ul')).forEach(function (nav) {
                new DotNav(nav, {
                    callback: function (idx) {
                        //console.log( idx )
                    }
                });
            });
            (function () {
                // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
                if (!String.prototype.trim) {
                    (function () {
                        // Make sure we trim BOM and NBSP
                        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                        String.prototype.trim = function () {
                            return this.replace(rtrim, '');
                        };
                    })();
                }
                [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
                    // in case the input is already filled..
                    if (inputEl.value.trim() !== '') {
                        classie.add(inputEl.parentNode, 'input--filled');
                    }
                    // events:
                    inputEl.addEventListener('focus', onInputFocus);
                    inputEl.addEventListener('blur', onInputBlur);
                });
                function onInputFocus(ev) {
                    classie.add(ev.target.parentNode, 'input--filled');
                }

                function onInputBlur(ev) {
                    if (ev.target.value.trim() === '') {
                        classie.remove(ev.target.parentNode, 'input--filled');
                    }
                }
            })();
        </script>
    </body>
</html>
