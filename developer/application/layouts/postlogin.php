<!doctype html>
<html lang="en" class="no-js">
    <head>  
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/reset.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.core.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.default.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/loader.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/select.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/menu-toggle.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.treeview.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/screen.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.tokenize.css" />
        <!-- CSS reset -->    
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.11.3.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.validate.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/knockout-3.2.0.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/alertify.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/kinetic.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.final-countdown.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.treeview.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.treeview.sortable.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.treeview.async.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.treeview.edit.js"></script>
        <!--FancyBox-->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.fancybox.css">
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.fancybox.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/cropbox.js"></script>
        <!--FancyBox-->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/select.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/masonry-docs.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.tokenize.js"></script> 
        <!--<script src="<?php echo base_url(); ?>assets/js/countdown.js" type="text/javascript"></script>-->
        <script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- Modernizr -->  
        <script type="text/javascript">
            var notification_list = [];
            $(window).load(function () {
                $('#menu_close').html('<i class="fa fa-times"></i>');
            });
            jQuery(function () {

                jQuery("#more-drop-down").hover(function () {
                    jQuery(".sub").slideToggle(400);
                },
                        function () {
                            jQuery(".sub").hide();
                        }
                );
                jQuery(function () {
                    jQuery("#more-drop-down2").hover(
                            function () {
                                jQuery(".sub").slideToggle(400);
                            },
                            function () {
                                jQuery(".sub").hide();
                            }
                    );
                    /*Menu Toggle*/
                    $('.slideout-menu-toggle').on('click', function (event) {

                        event.preventDefault();
                        // create menu variables
                        var slideoutMenu = $('.slideout-menu');
                        var slideoutMenuWidth = $('.slideout-menu').width();

                        // toggle open class
                        slideoutMenu.toggleClass("open");

                        // slide menu
                        if (slideoutMenu.hasClass("open")) {
                            slideoutMenu.animate({
                                right: "0px"
                            });
                        } else {
                            slideoutMenu.animate({
                                right: -slideoutMenuWidth
                            }, 250);
                        }
                        return false;
                    });
                    /*Menu Toggle*/
                });

                $(function () {
                    $('.notify-open').hover(function () {
                        $('.notify-open-down').fadeIn(100);
                        try {
                            for (var i = 0; i < vmNotification.notification_unseen().length; i++) {
                                notification_list.push(vmNotification.notification_unseen()[i].id)
                            }
                            $.post('<?php echo base_url(); ?>member/updateNotificationData', {list: notification_list}, function (result) {
                                if (result == 1) {
                                    vmNotification.notification_count(false);
                                }
                            });
                        } catch (e) {
                            alert(e);
                        }
                    }, function () {
                        $('.notify-open-down').fadeOut(100)
                    });
                });

                jQuery(function () {
                    jQuery('.message-open').hover(function () {
                        jQuery('.setting-message-down').fadeIn(100)
                    }, function () {
                        jQuery('.setting-message-down').fadeOut(100)
                    })
                })

                jQuery(function () {
                    jQuery('.people-open').hover(function () {
                        jQuery('.people-open-down').fadeIn(100)
                    }, function () {
                        jQuery('.people-open-down').fadeOut(100)
                    })
                });

                jQuery(function () {
                    jQuery("#setting-open").hover(function () {
                        jQuery(".setting-drop-down-open").slideToggle("fast", function () {
                        });
                    }
                    );
                });

            });
        </script>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                /* Updated November 2012 by Kris Rybak */
                jQuery('.mobile-nav').click(function () {
                    jQuery('.dashboard-nav').slideToggle('slow');
                });
                jQuery(window).resize(function () {
                    var currentWidth = $(window).width();
                    if (currentWidth >= 768) {
                        jQuery('.dashboard-nav').show();
                        jQuery('.dashboard-nav').css("display", "");
                    } else {
                        if (jQuery('.dashboard-nav').css("display") == 'none') {
                            jQuery('.dashboard-nav').hide();
                        }
                    }
                });
            });

            //---------------------- looking for dropdown-----------------
            jQuery(document).ready(function () {

                var url = '<?php echo base_url(); ?>category/get_looking_for_list';
                jQuery.post(url, {}, function (res) {
                    var value = JSON.parse(res);
                    var html1;
                    var get_selected_cookie_looking_for_selected_id = readCookie('set_selected_looking_for');
                    jQuery.each(value, function (ind, val) {

                        var lfid = val.looking_for_id;
                        var lfname = val.looking_for_name;
                        var stype = val.search_type;
                        var id = lfid + "-" + stype;
                        if (get_selected_cookie_looking_for_selected_id == id) {
                            var html = "<option value='" + id + "' selected='selected'>" + lfname + "</option>";
                        } else {
                            var html = "<option value='" + id + "'>" + lfname + "</option>";
                        }
                        jQuery('#select').append(html);
                    });

                    $(".select_looking_for_id").select2({
                        placeholder: "I'm looking to find...",
                        allowClear: true
                    });
                });
            });
   //-----------------------------------------------------------------
        </script>
        <title>Message board</title>
    </head>
    <!-- hijacking: on/off - animation: none/scaleDown/rotate/gallery/catch/opacity/fixed/parallax -->
    <?php
    $manager = new User_Manager();
    $user = $manager->get_looged_in_user();
    //print_r($user); exit();
    $is_wiz_two_empty = $manager->isAlreadyDataInWizTwo();
    if (empty($user)) {
        redirect(base_url() . "member/login");
    }
    ?>
    <body style="background-color:#F4F4F4">
        <header class="dashboard-header">
            <div>
                <div class="logo">
                    <a href="<?php echo base_url(); ?>member/dashboard">
                        <img src="<?php echo base_url(); ?>assets/images/Arlians-Full-Logo.png" title="Arlians logo" />
                    </a>
                </div>
                <?php
                $mid = $user['mid'];
                $user_type = check_member_payment_status($mid);
                ?>
                    <?php if ($user['status'] == 1) { ?> 
                    <div class="top-memeber-head">        
                        <div class="premium-member">Beta</div>
                        <?php if ($user_type[0]['user_type'] == "Free") { ?>
                            <a href="<?php echo base_url(); ?>payment/paynow" ><div class="premium-member" style="margin-left:5px">Upgrade</div></a>
    <?php } else {
        if (isFoundingMember($mid) == true) {
            ?>

                                <a href="javascript:void(0)" ><div class="premium-member" style="margin-left:5px">Founding</div></a>

                            <?php } else { ?>


                                <a href="javascript:void(0)" ><div class="premium-member" style="margin-left:5px">Premium</div></a>
        <?php }
    } ?>
                    </div>
<?php } ?>
                <div class="header-top-right">
                    <ul id="notification_section">
<?php if ($user['status'] == 1) { ?>
                            <li class="notification" >
                                <div class="notify-open" id="notify-open" >
                                    <a href="<?php echo base_url() ?>memberprofile/notification"><span data-bind="visible:notification_unseen().length>0,text:notification_unseen().length"></span>			  
                                        <i class="fa fa-bell-o fa-1"></i>
                                    </a>
                                    <div id="notify-open-down" class="notify-open-down" data-bind="visible:notification_count">
                                        <div id="content-1" data-bind="attr:{class:notification_list().length>3?'custom-scollar-wrap content':''}">
                                            <ul class="inbox-message-list right-inbox-message-list" data-bind="foreach:notification_list">            
                                                <li>
                                                    <div class="inbox-img">
                                                        <img alt="Profile Image" data-bind="attr:{src:image_url!=null?'<?php echo base_url(); ?>uploads/profile_image/'+image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}" >
                                                    </div>
                                                    <div class="inbox-content">
                                                        <h4 data-bind="text:bussinessname">Name of business/individual</h4>
                                                        <p class="date"><span data-bind="text:bussinesstype !=null ?bussinesstype:'N/A'">Type of business</span>, <span data-bind="text:noti_type=='prof_view'?'viewed your profile':noti_type=='hub_like'?'like your post':noti_type=='hub_share'?'share your post':'comment on your post'"></span></p>
                                                    </div>
                                                    <!--<div class="right-inbox-icon">
                                                        <a class="icon-border" href="javascript:void(0);"><i class="fa fa-times fa-1"></i></a>
                                                    </div>-->
                                                </li>					  
                                            </ul>
                                            <!--</div data-bind="visible:notification_list().length>4">-->
                                        </div>
                                    </div>

                                </div>
                            </li>
                            <li class="People">
                                <div class="people-open" id="people-open">
                                    <a href="<?php echo base_url() ?>memberprofile/connection"><span data-bind="visible:connection_request_count()>0,text:connection_request_count">22</span>        
                                        <i class="fa fa-user-plus fa-1"></i>         
                                    </a>
                                    <div class="people-open-down" id="people-open-down" data-bind="visible:connection_request_count>0">
                                        <ul class="inbox-message-list right-inbox-message-list" data-bind="foreach:connection_request_list">
                                            <li>
                                                <div class="inbox-img" data-bind="click:$root.redirectToMyNetwork">
                                                    <img alt="Profile Image" data-bind="attr:{src:image_url!=null?'<?php echo base_url(); ?>uploads/profile_image/'+image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}" >
                                                </div>
                                                <div class="inbox-content">
                                                    <h4 data-bind="text:bussinessname">Name of business/individual</h4>
                                                    <p class="date"><span data-bind="text:tag_line !=null ?tag_line:''">Type of business</span></p>
                                                </div>
                                                <div class="right-inbox-icon">
                                                    <a href="javascript:void(0);" class="icon-border" data-bind="click:$root.seenRequest">
                                                        <i class="fa fa-times fa-1"></i>
                                                    </a>
                                                    <a href="javascript:void(0);" class="icon-border"  data-bind="click:$root.acceptRequest">
                                                        <i class="fa fa-check fa-1"></i>
                                                    </a>
                                                </div>
                                            </li>                  
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li class="message">
                                <div class="message-open" id="message-open">
                                    <a href="<?php echo base_url(); ?>message/msglist/cm"><span data-bind="text:count_unread_message().length,visible:count_unread_message().length>0">8</span><i class="fa fa-envelope-o fa-1"></i></a>              
                                    <div class="setting-message-down" id="setting-message-down">
                                        <ul class="inbox-message-list right-inbox-message-list">
                                            <!--ko foreach:count_unread_message-->
                                            <li data-bind="visible: $index() < 5">
                                                <div class="inbox-img">
                                                    <img alt="Profile Image" data-bind="attr:{src:image_url!=null?'<?php echo base_url(); ?>uploads/profile_image/'+image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}" >
                                                </div>
                                                <div class="inbox-content">
                                                    <h4 data-bind="text:bussinessname">Name of business/individual</h4>
                                                    <p class="date" data-bind="click:$root.redirectToMessage"><span data-bind="trimText:msg_body,trimTextLength:10">Type of business</span></p>
                                                </div>
                                                <div>
                                                    <a class="reply" href="#send_message_modal" id="send_message_modal_link" href="javascript:void(0)" data-bind="click:$root.quickSendMessage">Send Message</a>
                                                </div>
                                            </li> 
                                            <!--/ko--> 
                                            <!--ko if:count_unread_message().length==0-->
                                            <li class="unread-message">No unread messages</li>
                                            <!--/ko-->
                                            <!--ko foreach:count_read_message-->
                                            <li data-bind="visible: $index() < 5">
                                                <div class="inbox-img">
                                                    <img alt="Profile Image" data-bind="attr:{src:image_url!=null?'<?php echo base_url(); ?>uploads/profile_image/'+image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}" >
                                                </div>
                                                <div class="inbox-content">
                                                    <h4 data-bind="text:bussinessname">Name of business/individual</h4>
                                                    <p class="date" data-bind="click:$root.redirectToMessage"><span data-bind="trimText:msg_body,trimTextLength:10">Type of business</span></p>
                                                </div>
                                                <div>
                                                    <a class="reply" href="#send_message_modal" id="send_message_modal_link" href="javascript:void(0)" data-bind="click:$root.quickSendMessage">Send Message</a>
                                                </div>
                                            </li> 
                                            <!--/ko-->                 
                                        </ul>
                                    </div>
                                </div>
                            </li>
<?php } ?>
<?php //print_r($user); exit; ?>
                        <li class="setting">
                            <div class="setting-open" id="setting-open">
                                <span>Settings</span>
                                <!--<a href="#" class="button slideout-menu-toggle"><img src="<?php echo base_url(); ?>assets/images/setting-icon.png" alt=""/></a>-->
                                <a href="javascript:void(0);" class="button slideout-menu-toggle"><i class="fa fa-cog"></i></a>
                            </div>
                            <div class="slideout-menu">
                                <h3>Menu <a href="#" class="slideout-menu-toggle" id="menu_close"></a></h3>
                                <!--<h3>Menu <a href="javascript:void(0);" class="slideout-menu-toggle"><i class="fa fa-times"></i></a></h3>-->
                                <ul>
                                    <?php if ($user['status'] == 1) { ?>
                                        <li><?php echo $user['bussinessname'] == '' ? '' : $user['bussinessname']; ?> </li>
                                        <li><a class="signout" href="<?php echo base_url() ?>memberprofile"> Manage profile </a></li>
                                        <!--<li><a class="signout" href="<?php echo base_url() ?>memberprofile/settings">Settings</a></li>-->
                                        <?php
                                        if ($user_type[0]['user_type'] == 'Premium') {

                                            if (premiumType($mid) == 1) {
                                                $text = 'Founding';
                                            } else {
                                                $text = 'premium';
                                            }
                                        } else {
                                            $text = 'Beta';
                                        }
                                        ?>
                                        <li>Account: <?php echo $text; ?></li>
                                        <li><a href="mailto:hello@arlians.com?Subject=Help%20center" target="_top">Help center</a></li>
                                        <?php if ($user_type[0]['user_type'] == "Free") { ?>

                                            <?php
                                            $date = getUserRegistrationDate($mid);
                                            $now = time(); // or your date as well
                                            $your_date = strtotime($date);
                                            $datediff = $now - $your_date;
                                            $day_diff = floor($datediff / (60 * 60 * 24));
                                            if ($day_diff <= 30) {
                                                ?>
                                                <?php
                                                $date = explode(' ', getUserRegistrationDate($mid));
                                                $date = $date[0];
                                                //	var_dump($your_date);
                                                $add_days = 30;
                                                $date = date('Y-m-d', strtotime($date) + (24 * 3600 * $add_days)); //my preferred method
                                                $end_date = strtotime($date);
                                                $current_date = strtotime(date('Y-m-d H:i:s'));
                                                ?>
                                                <li class="countdown-wrap upgrade-payment"><a class="signout" href="<?php echo base_url() ?>payment/paymentoptiondetails">UPGRADE - Limited offer</a>

                                                    <script type="text/javascript">
                                                        $('document').ready(function () {
                                                            'use strict';

                                                            $('.countdown').final_countdown({
                                                                'start': <?php echo $your_date; ?>,
                                                                'end': <?php echo $end_date; ?>,
                                                                'now': <?php echo $current_date; ?>
                                                            });
                                                        });
                                                    </script>
                                                    <div class="countdown">
                                                        <div class="clock row">
                                                            <div class="clock-item clock-days">
                                                                <div class="wrap">
                                                                    <div class="inner">
                                                                        <div id="canvas-days" class="clock-canvas"></div>

                                                                        <div class="text">
                                                                            <p class="val">0</p>

                                                                        </div><!-- /.text -->
                                                                        <p class="type-days type-time">DAYS</p>
                                                                    </div><!-- /.inner -->
                                                                </div><!-- /.wrap -->
                                                            </div><!-- /.clock-item -->

                                                            <div class="clock-item clock-hours">
                                                                <div class="wrap">
                                                                    <div class="inner">
                                                                        <div id="canvas-hours" class="clock-canvas"></div>

                                                                        <div class="text">
                                                                            <p class="val">0</p>                        
                                                                        </div><!-- /.text -->
                                                                        <p class="type-hours type-time">HOURS</p>
                                                                    </div><!-- /.inner -->
                                                                </div><!-- /.wrap -->
                                                            </div><!-- /.clock-item -->

                                                            <div class="clock-item clock-minutes">
                                                                <div class="wrap">
                                                                    <div class="inner">
                                                                        <div id="canvas-minutes" class="clock-canvas"></div>

                                                                        <div class="text">
                                                                            <p class="val">0</p>                        
                                                                        </div><!-- /.text -->
                                                                        <p class="type-minutes type-time">MINS</p>
                                                                    </div><!-- /.inner -->
                                                                </div><!-- /.wrap -->
                                                            </div><!-- /.clock-item -->

                                                            <div class="clock-item clock-seconds">
                                                                <div class="wrap">
                                                                    <div class="inner">
                                                                        <div id="canvas-seconds" class="clock-canvas"></div>

                                                                        <div class="text">
                                                                            <p class="val">0</p>                        
                                                                        </div><!-- /.text -->
                                                                        <p class="type-seconds type-time">SEC</p>
                                                                    </div><!-- /.inner -->
                                                                </div><!-- /.wrap -->
                                                            </div><!-- /.clock-item -->
                                                        </div><!-- /.clock -->
                                                    </div><!-- /.countdown-wrapper -->

                                                </li>
                                            <?php } elseif ($day_diff > 30 && $day_diff < 35) { ?>
                                                <?php
                                                $date = explode(' ', getUserRegistrationDate($mid));
                                                $date = $date[0];
                                                //	var_dump($your_date);
                                                $add_days = 36;
                                                $date = date('Y-m-d', strtotime($date) + (24 * 3600 * $add_days)); //my preferred method
                                                $end_date = strtotime($date);
                                                $current_date = strtotime(date('Y-m-d H:i:s'));
                                                ?>
                                                <li class="countdown-wrap upgrade-payment"><a class="signout" href="<?php echo base_url() ?>payment/paymentoptiondetails">UPGRADE - ONE OFF EXTENSION OFFER</a>	
                                                    <script type="text/javascript">
                                                        $('document').ready(function () {
                                                            'use strict';

                                                            $('.countdown').final_countdown({
                                                                'start': <?php echo $your_date; ?>,
                                                                'end': <?php echo $end_date; ?>,
                                                                'now': <?php echo $current_date; ?>
                                                            });
                                                        });
                                                    </script>


                                                    <div class="countdown">
                                                        <div class="clock row">
                                                            <div class="clock-item clock-days">
                                                                <div class="wrap">
                                                                    <div class="inner">
                                                                        <div id="canvas-days" class="clock-canvas"></div>

                                                                        <div class="text">
                                                                            <p class="val">0</p>

                                                                        </div><!-- /.text -->
                                                                        <p class="type-days type-time">DAYS</p>
                                                                    </div><!-- /.inner -->
                                                                </div><!-- /.wrap -->
                                                            </div><!-- /.clock-item -->

                                                            <div class="clock-item clock-hours">
                                                                <div class="wrap">
                                                                    <div class="inner">
                                                                        <div id="canvas-hours" class="clock-canvas"></div>

                                                                        <div class="text">
                                                                            <p class="val">0</p>                        
                                                                        </div><!-- /.text -->
                                                                        <p class="type-hours type-time">HOURS</p>
                                                                    </div><!-- /.inner -->
                                                                </div><!-- /.wrap -->
                                                            </div><!-- /.clock-item -->

                                                            <div class="clock-item clock-minutes">
                                                                <div class="wrap">
                                                                    <div class="inner">
                                                                        <div id="canvas-minutes" class="clock-canvas"></div>

                                                                        <div class="text">
                                                                            <p class="val">0</p>                        
                                                                        </div><!-- /.text -->
                                                                        <p class="type-minutes type-time">MINS</p>
                                                                    </div><!-- /.inner -->
                                                                </div><!-- /.wrap -->
                                                            </div><!-- /.clock-item -->

                                                            <div class="clock-item clock-seconds">
                                                                <div class="wrap">
                                                                    <div class="inner">
                                                                        <div id="canvas-seconds" class="clock-canvas"></div>

                                                                        <div class="text">
                                                                            <p class="val">0</p>                        
                                                                        </div><!-- /.text -->
                                                                        <p class="type-seconds type-time">SEC</p>
                                                                    </div><!-- /.inner -->
                                                                </div><!-- /.wrap -->
                                                            </div><!-- /.clock-item -->
                                                        </div><!-- /.clock -->
                                                    </div><!-- /.countdown-wrapper -->

                                                </li>		
                                            <?php } else { ?>
                                                <li class="countdown-wrap upgrade-payment"><a class="signout" href="<?php echo base_url() ?>payment/paymentoptiondetails">UPGRADE</a></li>
                                            <?php } ?>
                                            <li class="become-founding-member"><a class="signout" href="<?php echo base_url() ?>payment/paymentOption">Become a founding member</a></li>
                                        <?php } else { ?>
        <?php if ($user_type[0]['user_type'] == 'Premium') { ?>
                                                <li class="become-founding-member"><a class="signout" href="<?php echo base_url() ?>payment/paymentOption">Become a founding member</a></li>
                                            <?php } ?>	
                                            <?php if (isFoundingMember($mid) != true) { ?>
                                                <li><a class="signout" href="<?php echo base_url() ?>payment/cancelPayment">Unsubscribe from premium membership</a></li>
        <?php } ?>

    <?php } ?>
                                        <!--<li>Privacy & Settings</li>-->

                                        <li><a href="<?php echo base_url() ?>memberprofile/accountStatus">Deactivate account</a></li>
<?php } ?>
                                    <li><a class="signout" href="<?php echo base_url() ?>member/logout"> Sign out</a> </li>
                                </ul>
                            </div>
                        </li>
                        <a class="reply" href="#send_message_modal_from_notification" id="send_message_modal_from_notification_link" style="display:none"></a>
                        <div id="send_message_modal_from_notification" style="width:600px; display:none;">
                            <div class="popup-box-part-center">
                                <form id="send_message_form_from_notification" method="post">
                                    <h2>Send Message</h2>
                                    <div class="business-img-listing-wrap cust-fancy-label-input">
                                        <div class="cust-fancy-label-input">
                                            <label>Subject</label>
                                            <div data-bind="text:message_sub"></div>
                                            <!--<input type="text" name="message_sub" id="message_sub" data-bind="value:message_sub"> -->
                                        </div>
                                        <div class="cust-fancy-label-input">
                                            <label>Message</label>
                                            <div data-bind="text:message_display"></div>
                                        </div>                         
                                        <div class="cust-fancy-label-input"> 
                                            <label>Reply</label>
                                            <textarea data-bind="value:message" name="message"></textarea>
                                            <!--<input type="text" name="last_name" id="edit_last_name" data-bind="value:last_name"> -->
                                        </div>   
                                        <button type="submit" class="button" id="send_message_submit_button">Send</button> 
                                    </div>      
                                </form>    
                            </div>
                        </div>


                    </ul>
                    </ul>
                </div>
                </li>
                </ul>
            </div>  
        </div>
    </header>
    <section class="dashboard-section">
        <div class="greybox">
            <div class="row">       
                <div class="cover-wrap">             
                    <form id="suggestion-search" method="post" action="<?php echo base_url(); ?>search/advsearch">
<?php if ($user['status'] == 1) { ?>
                            <div class="cover-header">
                                <div class="cover-header-left">
                                    <ul>
                                        <li class="looking-find">
                                            <label data-bind="click:showHideFunctionalitySearchBar">I am looking to find</label>
                                            <!-- <input type="text" class="search-text" name="text_search" id="text_search" placeholder="Near to ... ( Enter Location )">
                                            <input type="hidden" name="search_type" id="search_type" value="2">
                                            <input type="hidden" name="country" id="country" value="1"> -->
                                            <div class="search-open-form" data-bind="visible:search_form_section">
                                                <ul>
                                                    <li>
                                                        <input type="radio" class="ad_search_type" value="1" onclick="checkBoxCheck(this, 'hasBuisness');" name="search_type"  id="search_by_buisness_radio">
                                                        <input type="text" name="text" placeholder="Type buisness name here" id="search_by_buisness" data-bind="enable: hasBuisness">
                                                    </li>
                                                    <li>                                              
                                                        <input type="radio" class="ad_search_type" value="2" name="search_type" onclick="checkBoxCheck(this, 'hasIndustry');"  id="search_by_industry_radio">
                                                        <input type="text" name="text" placeholder="Type industry name here" id="search_by_industry" data-bind="enable: hasIndustry">
                                                        <div id="suggesstion-box" data-bind="visible:search_industry_list().lenght>0">
                                                            <ul id="country-list" data-bind="foreach:search_industry_list">
                                                                <li data-bind="text:name,click:$root.selectIndustry">industry name</li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li class="enter-loaction">
                                            <!--<input type="text" class="fld" name="" id="" placeholder="Near to ... ( Enter Location )">-->
                                            <select id="select1" name="selected_country" 
                                                    data-bind="options: country_list,
                                    optionsText: 'country_name',
                                    optionsValue: 'id',
                                    value: selected_country,
                                    optionsCaption: 'All countries'"></select>
                                        </li>                              
                                    </ul>
                                </div>
                                <div class="cover-header-right">
                                    <a class="advance-search" href="<?php echo base_url() ?>search/advanceSearch">Advanced search</a>
                                      <!--<input type="button" onclick="validateSuggestionBeforeSubmit();" value="Search" class="dashboard-search">-->
                                    <input type="button" onclick="validateSearchFormBeforeSubmit();"  value="Search" class="dashboard-search">
                                </div>                      
                            </div>
<?php } ?> 
                    </form>

                    <br />
       <!-- <form id="suggestion-search" method="post" action="<?php echo base_url(); ?>suggestion/makeSuggestedListForloginuser">
        <div class="cover-header">
          <div class="cover-header-left">
            <ul>
              <li>
                <select class="select_looking_for_id" id="select" name="looking_for[]">                   
                  
                </select>
              </li>
            
            </ul>
          </div>
          <div class="cover-header-right">
               <input type="button" onclick="validateSuggestionBeforeSubmit();" value="Search" class="dashboard-search">
          </div>
        </div>
        </form>-->
                        <?php
                        if (empty($user)) {
                            redirect(base_url() . "member/login");
                        }
                        //echo '<pre>'; var_dump($user); die();
                        ?>
                    <div class="cover-img">
<?php if (!empty($user['back_image'])) { ?>
                            <!--<img id="profile_cover_pic" src="<?php echo base_url() . 'assets/images/cover-images.png' ?>"  alt=""/>-->
                            <img id="profile_cover_pic" src="<?php echo base_url() . 'uploads/profile_image/' . $user['back_image']; ?>" height="190" width="100%"  alt=""/>
                        <?php } else { ?>
                            <img id="profile_cover_pic" src="<?php echo base_url() . 'assets/images/cover-images.png' ?>" height="190" width="100%"  alt=""/>
<?php } ?>
<?php if ($user['status'] == 1) { ?>
    <?php if (strpos($_SERVER['REQUEST_URI'], "viewprofile") != true) { ?>
                                <div class="update-cover-photo">
                                    <i class="fa fa-camera fa-2"></i>
                                    <a  href="#imgdiv1" class="fancybox" id="back_image"><p>Update cover photo </p></a>
                                </div>
    <?php } ?>
                            <?php } ?>    
                    </div>
                </div>
                <div class="main-container"> 
                    <div class="profile-wrap">
                        <div class="profile-img-left">
                            <?php if (!empty($user['fornt_image'])) { ?>
                                <img id="profile_pic" src="<?php echo base_url() . 'uploads/profile_image/' . $user['fornt_image']; ?>" alt=""/>
                            <?php } else { ?>
                                <img id="profile_pic" src="<?php echo base_url() . 'assets/images/profile-img.png' ?>" alt=""/>

                            <?php } ?>
                            <?php if ($user['status'] == 1) { ?>
                                <?php if (strpos($_SERVER['REQUEST_URI'], "viewprofile") != true) { ?>
                                    <div class="upload-sec">
                                        <a href="#imgdiv" class="fancybox" id="img_upload"><i class="fa fa-camera fa-2"></i><p>Update profile <br> picture</p></a>
                                    </div>
                                        <?php } ?>
                                    <?php } ?>
                        </div>
                        <div class="profile-cont-left">
                                            <?php //print_r($user); exit; ?>
                                            <?php if (!empty($user['bussinessname'])) { ?>
                                <h3><?php echo $user['bussinessname'] ?></h3>
                                            <?php } else { ?>
                                <h3><?php echo $user['fname'] . ' ' . $user['lname'] ?><h3>
                                            <?php } ?>
                                    <p><?php echo $user['tag_line']; ?></p>              
                                            <?php if ($user['status'] == 1) { ?>      
                                        <a class="mobile-nav"><i class="fa fa-bars fa-3"></i></a> 
                                        <nav class="dashboard-nav">
                                            <ul>
                                                <?php if (strpos($_SERVER['REQUEST_URI'], "viewprofile") == true) { ?>
                                                    <li class="active"><a href="javascript:void(0);">Profile</a></li> 
                                        <?php } else if (strpos($_SERVER['REQUEST_URI'], "hubdetails") == true) { ?>
                                            <?php if ($manager->get_current_user_id_while_visting_other_user_profile() != $mid) { ?>						
                                                        <li class="active"><a href="javascript:void(0);">Profile</a></li>
        <?php } else { ?>
            <?php echo Menu_Helper::get_main_menu_html("dashboard") ?> 
        <?php } ?>
    <?php } else { ?>
                                            <?php echo Menu_Helper::get_main_menu_html("dashboard") ?>                                               
                                        <?php } ?>
                                            </ul>
                                        </nav>
<?php } ?>  
                                    </div>
                                    </div> 
                                    <div id="loader-wrapper" style="display:none">
                                        <!-- loader start -->
                                        <div class="loader"></div>
                                    </div>
<?php echo $content; ?>
                                    </div>
                                    </div>
                                    </div>
                                    </section>  
                                    <footer class="dashboard-footer">
                                        <div class="main-container">
                                            <div class="dashboard-footer-left">
                                                <a href="#"><img src="<?php echo base_url(); ?>assets/images/footer-logo.png" alt=""/></a>
                                            </div>
                                            <div class="dashboard-footer-right">
                                                <ul class="dashboard-footer-list">
                                                    <li><p>&copy; 2015 arlians.com</p></li>              
                                                    <li><a href="<?php echo base_url() ?>company/termsAndConditions">Terms & Conditions</a></li>
                                                    <li><a href="<?php echo base_url() ?>company/privacyPolicy">Privacy Policy</a></li>
                                                    <li><a href="mailto:info@arlians.com?Subject=Help%20center" target="_top">Support</a></li>
                                                </ul>
                                                <ul class="dashboard-footer-list">
                                                    <li><a href="<?php echo base_url() ?>company/aboutArlians">About Arlians</a></li>    
                                                    <!-- <li><a href="<?php echo base_url() ?>memberprofile/comingSoon">Business Services</a></li> -->
                                                    <li><a href="mailto:hello@arlians.com?Subject=Help%20center" target="_top">Contact Us</a></li>
                                                    <li><a href="<?php echo base_url() ?>memberprofile/comingSoon">FAQS</a></li>
                                                </ul>
                                                <ul class="dashboard-footer-list social-icon">
                                                    <li><p>Find Us</p><br><br>
                                                        <div class="social-icon-right">
                                                            <ul>
                                                                <li><a target="_blank" href="https://www.facebook.com/pages/Arlians/1678298732393309"><img src="<?php echo base_url(); ?>assets/images/facebook.png"></a></li>
                                                                <li><a target="_blank" href="https://twitter.com/Arlians_"><img src="<?php echo base_url(); ?>assets/images/twiiter.png"></a></li>
                                                                <li><a target="_blank" href="https://www.linkedin.com/company/10075228?trk=tyah&trkInfo=clickedVertical%3Acompany%2Cidx%3A2-1-4%2CtarId%3A1437656243753%2Ctas%3Aarlians%20limited"><img src="<?php echo base_url(); ?>assets/images/linkdin.png"></a></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </footer>
                                    </body>
                                    <div id="improve_match" style="width:700px; display:none;">
                                        <div class="popup-wrap">
                                            <div data-bind="visible:part_one">
                                                <div class="popup-box-part-one">
                                                    <h2>Kick-off your Arlians experience with these tips</h2>
                                                    <ul>
                                                        <li>
                                                            1) <a href="javascript:void(0);" data-bind="click:showPartTwo"> Complete the setup wizard </a> to see what businesses Arlians suggest you share             mutual characteristics with
                                                        </li>
                                                        <li>
                                                            2) Tell the world about your business by personalising your profile <a href="<? echo base_url()?>memberprofile">here</a>
                                                        </li>
                                                        <!--<li>3) Get rewarded for adding your contacts in <a href="javascript:void(0);">here</a></li>-->
                                                    </ul>
                                                </div>
                                            </div>
                                            <div data-bind="visible:part_two">
                                                <div class="popup-box-part-center">
                                                    <h2>First, tell us what you’re looking for</h2>
                                                    <div class="business_tool-tips">
                                                        <div class="tool-tips-block">This helps us to match you with business that want the same thing</div>
                                                    </div>
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-img-listing looking_for_list" data-bind="foreach:looking_for_list">          
                                                            <li onclick="setActive(this)" data-bind="click:$root.setLookingForList">
                                                                <a href="javascript:void(0);" data-bind="value:looking_for_id"><img style="opacity:0" src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>
                                                                    <div class="business-listing-heading">
                                                                        <h4 data-bind="text:looking_for_name">Capital Market</h4>
                                                                    </div>
                                                                </a>
                                                            </li>                   
                                                        </ul>
                                                    </div>
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzard(2)">Next</button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-bind="visible:part_three">
                                                <div class="popup-box-part-center">
                                                    <h2>Where do you want to explore?</h2>
                                                    <div class="business_tool-tips">
                                                        <div class="tool-tips-block">This helps us to match you with business that want the same thing</div>
                                                    </div>
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-content-listing locality_list" data-bind="foreach:locality_list">          
                                                            <li onclick="setActive(this)" data-bind="click:$root.selLocalityList"><a href="javascript:void(0);" data-bind="value:locality_id,text:locality_type"> #1Lorem ipsum</a></li>
                                                            <!--<li data-bind="value:locality_id,text:locality_type">#1Lorem ipsum</li>-->
                                                        </ul>  
                                                    </div>              
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzard(3)">Next</button>
                                                            </li>
                                                        </ul>
                                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(3)">Back to Previous Question</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-bind="visible:part_four">
                                                <div class="popup-box-part-center">
                                                    <h2>Do you service…</h2>
                                                    <div class="business_tool-tips">
                                                        <div class="tool-tips-block">This helps us to understand what sort of businesses may be interested in you</div>
                                                    </div>
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-content-listing service_list" data-bind="foreach:service_list">          
                                                            <li onclick="setActive(this)" data-bind="click:$root.setService"><a href="javascript:void(0);" data-bind="text:service_name"> #1Lorem ipsum</a><input type="hidden" data-bind="value:service_id"></li>                                        
                                                        </ul>                
                                                    </div>
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzard(4)">Next</button>
                                                            </li>
                                                        </ul>
                                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(4)">Back to Previous Question</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-bind="visible:part_five">
                                                <div class="popup-box-part-center">
                                                    <h2>What sort of businesses do you service?</h2>
                                                    <div class="business_tool-tips">
                                                        <div class="tool-tips-block">This helps us to understand what sort of businesses may be interested in you</div>
                                                    </div>
                                                    <div class="business-listing-wrap">
                                                        <div class="business-img-listing-wrap">
                                                            <ul class="business-img-listing industry_list" data-bind="foreach:industry_list">          
                                                                <li onclick="setIndActive(this, 'industry_list')">
                                                                    <a href="javascript:void(0);" data-bind="value:industry_code,click:$root.getSectorChildList">
                                                                        <img data-bind="attr:{src:profile_image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+profile_image:'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                                        <div class="business-listing-heading">
                                                                            <h4 data-bind="text:industry_name">Capital Market</h4>
                                                                        </div>
                                                                    </a>
                                                                </li>                   
                                                            </ul>
                                                        </div>
                                                        <p class="business-message">We know that some people work in more than one industry, so we are working on adding in the functionality to do this.</p>
                                                    </div>
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzard(5)">Next</button>
                                                            </li>
                                                        </ul>
                                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(5)">Back to Previous Question</a></div>
                                                    </div>
                                                </div>
                                            </div>             
                                            <div data-bind="visible:part_six">
                                                <div class="popup-box-part-center">
                                                    <h2>What best describes your target sector?</h2>
                                                    <div class="business_tool-tips">
                                                        <div class="tool-tips-block">This helps us to understand what sort of businesses may be interested in you</div>
                                                    </div>
                                                    <div class="business-listing-wrap">
                                                        <div class="business-img-listing-wrap">
                                                            <ul class="business-img-listing child_sec_list" data-bind="foreach:child_sector_list">          
                                                                <li onclick="setIndActive(this, 'child_sec_list')">
                                                                    <a href="javascript:void(0);" data-bind="value:tar_sector_code,click:$root.setSector">
                                                                        <img data-bind="attr:{src:profile_image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+profile_image:'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                                        <div class="business-listing-heading">
                                                                            <h4 data-bind="text:tar_sector_name">Capital Market</h4>
                                                                        </div>
                                                                    </a>
                                                                </li>                   
                                                            </ul>
                                                        </div>
                                                        <p class="business-message">We know that some people target more than one industry, so we are working on adding in the functionality to do this.</p>
                                                    </div>
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzard(6)">Next</button>
                                                            </li>
                                                        </ul>
                                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(6)">Back to Previous Question</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-bind="visible:part_seven">
                                                <div class="popup-box-part-center">
                                                    <h2>What sort of buisnesses do your service?</h2>
                                                    <div class="business_tool-tips">
                                                        <div class="tool-tips-block">This helps us to understand what sort of businesses may be interested in you</div>
                                                    </div>
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-content-listing biz_serve_list" data-bind="foreach:biz_serve_list">          
                                                            <li onclick="setActive(this)" data-bind="click:$root.setBizServe"><a href="javascript:void(0);" data-bind="text:look_service_name"> #1Lorem ipsum</a></li>                                        
                                                        </ul>                
                                                    </div>
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzard(7)">Next</button>
                                                            </li>
                                                        </ul>
                                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzart(7)">Back to Previous Question</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-bind="visible:part_eight">
                                                <h2 class="first">Congratulations!</h2>
                                                <div class="form-min-hgt-wrap">
                                                    <p>You have now set up your matching. See who Arlians suggests you connect with today, using the my suggestions section
                                                        Would you like to add another area in which you work, or add another target customer type?</p>
                                                </div>
                                            </div>
                                        </div>          
                                    </div>

                                    <div id="add_new_customer" style="width:700px; display:none;">
                                        <div class="popup-wrap">

                                            <div class="popup-box-part-center" data-bind="visible:add_new_wiz_two_part_one">
                                                <h2>What sort of businesses do you service?</h2>
                                                <div class="business_tool-tips">
                                                    <div class="tool-tips-block">This helps us to understand what sort of businesses may be interested in you</div>
                                                </div>
                                                <div class="business-listing-wrap">
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-img-listing industry_list" data-bind="foreach:industry_list">          
                                                            <li onclick="setIndActive(this, 'industry_list')">
                                                                <a href="javascript:void(0);" data-bind="value:industry_code,click:$root.getSectorChildList">
                                                                    <img data-bind="attr:{src:profile_image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+profile_image:'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                                    <div class="business-listing-heading">
                                                                        <h4 data-bind="text:industry_name">Capital Market</h4>
                                                                    </div>
                                                                </a>
                                                            </li>                   
                                                        </ul>
                                                    </div>
                                                    <p class="business-message">We know that some people work in more than one industry. We are working on adding in this functionality at the moment and you will be able to add more shortly.</p>
                                                </div>
                                                <div class="middle-signup-container">
                                                    <ul>
                                                        <li><button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzardAddNewWizTwo(1)">Next</button></li>
                                                    </ul>              
                                                </div>
                                            </div>
                                            <div class="popup-box-part-center" data-bind="visible:add_new_wiz_two_part_two">
                                                <h2>What best describes your target sector?</h2>
                                                <div class="business_tool-tips">
                                                    <div class="tool-tips-block">This helps us to understand what sort of businesses may be interested in you</div>
                                                </div>
                                                <div class="business-listing-wrap">
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-img-listing child_sec_list" data-bind="foreach:child_sector_list">          
                                                            <li onclick="setIndActive(this, 'child_sec_list')">
                                                                <a href="javascript:void(0);" data-bind="value:tar_sector_code,click:$root.setSector">
                                                                    <img data-bind="attr:{src:profile_image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+profile_image:'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                                    <div class="business-listing-heading">
                                                                        <h4 data-bind="text:tar_sector_name">Capital Market</h4>
                                                                    </div>
                                                                </a>
                                                            </li>                   
                                                        </ul>
                                                    </div>
                                                    <p class="business-message">We know that some people work in more than one industry. We are working on adding in this functionality at the moment and you will be able to add more shortly.</p>
                                                </div>
                                                <div class="middle-signup-container">
                                                    <ul>
                                                        <li>
                                                            <button type="button" class="button" id="a_SignUpSubmit" data-bind="click:addAnotherCustomerType">Save</button>
                                                        </li>
                                                    </ul>
                                                    <div class="back-previous"><a href="javascript:void(0);" data-bind="click:showPartOneIndustryList">Back to Previous Question</a></div>
                                                </div>
                                            </div>
                                            <div class="popup-box-part-center" data-bind="visible:add_new_wiz_two_part_three">
                                                <h2 class="first">Congratulations!</h2>
                                                <div class="form-min-hgt-wrap">
                                                    <p>You have now set up your matching. See who Arlians suggests you connect with today, using the my suggestions section.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="add_info_wiz_one" style="width:700px; display:none;">
                                        <div class="popup-wrap">      
                                            <div data-bind="visible:wiz_one_part_one">
                                                <div class="popup-box-part-center">
                                                    <h2>What sort of business are you?</h2>
                                                    <div class="business_tool-tips">
                                                        <div class="tool-tips-block">This helps us to understand what sort of businesses may be interested in you</div>
                                                    </div>
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-img-listing top_list" data-bind="foreach:top_list">          
                                                            <li onclick="setIndActive(this, 'top_list')">
                                                                <a href="javascript:void(0);" data-bind="value:top_code,click:$root.getChildList">
                                                                  <!--<img src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>-->
                                                                    <img data-bind="attr:{src:profile_image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+profile_image:'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                                    <div class="business-listing-heading">
                                                                        <h4 data-bind="text:top_name">Capital Market</h4>
                                                                    </div>
                                                                </a>
                                                            </li>                   
                                                        </ul>
                                                    </div>
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzardOne(1)">Next</button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div data-bind="visible:wiz_one_part_two">
                                                <div class="popup-box-part-center">
                                                    <h2>What best describes your sector?</h2>
                                                    <div class="business-select-wrap">
                                                        <div class="business-img-left active">
                                                            <a href="javascript:void(0)">
                                                                <!--<img src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>-->
                                                                <img data-bind="attr:{src:label_sel_sector_img()!=null?'<?php echo base_url(); ?>uploads/profile_image/'+label_sel_sector_img():'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                                <div class="business-listing-heading"> 
                                                                    <h4 data-bind="text:selected_top">Consumer Durables</h4>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="business-select-text">
                                                            You have selected <span data-bind="text:label_sel_sector"></span> in previous selection.  Please select your business sector classifications from below.
                                                        </div>
                                                    </div>
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-img-listing biz_child_list" data-bind="foreach:child_top_list">          
                                                            <li onclick="setIndActive(this, 'biz_child_list')">
                                                                <a href="javascript:void(0);" data-bind="value:sector_code,click:$root.selectSetorCode">
                                                                    <img data-bind="attr:{src:profile_image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+profile_image:'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                                    <!--<img src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>-->
                                                                    <div class="business-listing-heading">
                                                                        <h4 data-bind="text:sector_name">Capital Market</h4>
                                                                    </div>
                                                                </a>
                                                            </li>                   
                                                        </ul>
                                                    </div>
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzardOne(2)">Next</button>
                                                            </li>
                                                        </ul>
                                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzartOne(2)">Back to previous question</a></div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div data-bind="visible:wiz_one_part_three">
                                                <div class="popup-box-part-center">
                                                    <h2>What best describes your sub-sector?</h2>
                                                    <div class="business-select-wrap">
                                                        <div class="business-img-left active">
                                                            <a href="">
                                                                <!--<img src="<?php echo base_url(); ?>assets/images/business-img-one.png" alt=""/>-->
                                                                <img data-bind="attr:{src:label_sel_sub_sector_img()!=null?'<?php echo base_url(); ?>uploads/profile_image/'+label_sel_sub_sector_img():'<?php echo base_url(); ?>assets/images/business-img-one.png'}" alt=""/>
                                                                <div class="business-listing-heading"> 
                                                                    <h4 data-bind="text:selected_sector_name">Consumer Durables</h4>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="business-select-text">
                                                            You have selected <span data-bind="text:label_sel_sub_sector"></span> in previous selection.  Your  have selected ##1business sector. Please click to select the item which best describes your sub-sector.
                                                        </div>
                                                        <div class="business-img-listing-wrap">
                                                            <ul class="business-content-listing sub_sector_list" data-bind="foreach:sub_sector_list">          
                                                                <li onclick="setIndActive(this, 'sub_sector_list')"><a href="javascript:void(0);" data-bind="value:subsector_code,text:subsector_name,click:$root.getBuisnessList"> #1Lorem ipsum</a></li>                                        
                                                            </ul>                
                                                        </div>
                                                        <div class="middle-signup-container">
                                                            <ul>
                                                                <li>
                                                                    <button type="button" class="button" id="a_SignUpSubmit" onclick="nextWidzardOne(3)">Next</button>
                                                                </li>
                                                            </ul>
                                                            <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzartOne(3)">Back to previous question</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div data-bind="visible:wiz_one_part_four">
                                                <div class="popup-box-part-center">
                                                    <h2>What best describes your business type?</h2>
                                                    <div class="business_tool-tips">
                                                        <div class="tool-tips-block">You have selected consumer durables in previous selection. You have selected ##1business sector. You have selected #4business sector. Please click to select the item which best describes your business class</div>
                                                    </div>
                                                    <div class="business-img-listing-wrap">
                                                        <ul class="business-content-listing biz_serve_list" data-bind="foreach:buisness_list">          
                                                            <li onclick="setIndActive(this, 'biz_serve_list')" data-bind="click:$root.setBuisDetail"><a href="javascript:void(0);" data-bind="value:bussiness_code,text:bussiness_name"> #1Lorem ipsum</a></li>                                        
                                                        </ul>                
                                                    </div>
                                                    <div class="middle-signup-container">
                                                        <ul>
                                                            <li>
                                                                <button type="button" class="button" id="a_SignUpSubmit" data-bind="click:saveWizSecond">Next</button>
                                                            </li>
                                                        </ul>
                                                        <div class="back-previous"><a href="javascript:void(0);" onclick="previousWidzartOne(4)">Back to previous question</a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div data-bind="visible:wiz_one_part_five">
                                                <div class="popup-box-part-center">
                                                    <h2 class="first">Congratulations!</h2>
                                                    <div class="form-min-hgt-wrap">
                                                        <p>You have now set up your matching preferences. See who Arlians has suggested you connect with using the ‘My Suggestions’ module. We know that some people work in more than one industry, so we are working on adding in the functionality to do this.</p>
                                                    </div>
                                                </div>  
                                            </div>
                                        </div>       
                                    </div>
                                    </div>
                                    </div>  
                                    <!-- -------------fancy box div start-------------------- -->  
                                    <div id="imgdiv"  class="upload-cover-photo upload-profile-photo" style="width:700px; display:none;">
                                        <h3>Upload Profile Image</h3>
                                        <div class="container-crop">
                                            <div class="imageBox">
                                                <div class="thumbBox"></div>
                                                <div class="spinner" style="display: none">Loading...</div>
                                            </div>
                                            <div class="action">
                                                <input type="file" id="file" style="float:left; width: 250px">
                                                <!--<input type="button" id="uploadImg" style="float: right" value="Upload"/>-->
                                                <input type="button" id="btnCrop" value="Upload" style="float: right">
                                                <input type="button" id="btnZoomIn" value="+" style="float: right">
                                                <input type="button" id="btnZoomOut" value="-" style="float: right">
                                            </div>
                                            <div class="cropped"></div>
                                        </div>
                                    </div>

                                    <div id="imgdiv1" class="upload-cover-photo upload-profile-photo" style="width:1000px; display:none;">
                                        <h3>Upload Cover Image</h3>
                                        <div class="container-crop">
                                            <div class="imageBox1">
                                                <div class="thumbBox1"></div>
                                                <div class="spinner1" style="display: none">Loading...</div>
                                            </div>
                                            <div class="action">
                                                <input type="file" id="file1" style="float:left; width: 250px">
                                                <!--<input type="button" id="uploadCoverImg" style="float: right" value="Upload"/>-->
                                                <input type="button" id="btnCrop1" value="Upload" style="float: right">
                                                <input type="button" id="btnZoomIn1" value="+" style="float: right">
                                                <input type="button" id="btnZoomOut1" value="-" style="float: right">

                                            </div>

                                        </div>
                                    </div>
                                    <!-- -------------fancy box div end-------------------- -->
                                    <script type="text/javascript">
                                        var selected_industry = '';
                                        var add_new_selected_industry = null;
                                        var add_new_selected_sector = null;
                                        var selected_sector = '';
                                        var selected_list = null;
                                        var sub_sector = null;
                                        var sel_buiz = null;
                                        var buisness_det = null;
                                        var check_wiz_two = "<?php echo $is_wiz_two_empty; ?>";
                                        var vmAdvanceSearch = {
                                            search_form_section: ko.observable(false),
                                            hasIndustry: ko.observable(false),
                                            country_list: ko.observableArray(),
                                            hasBuisness: ko.observable(false),
                                            selected_country: ko.observable(),
                                            search_industry_list: ko.observableArray(),
                                            showHideFunctionalitySearchBar: function () {
                                                if (vmAdvanceSearch.search_form_section() == false) {
                                                    vmAdvanceSearch.search_form_section(true);
                                                } else {
                                                    vmAdvanceSearch.search_form_section(false)
                                                }
                                            }, selectIndustry: function () {
                                                $("#search_by_industry").val(this.name);
                                                $("#suggesstion-box").hide();
                                            }
                                        };
                                        function checkBoxCheck(curObj, type) {
                                            if (type == 'hasIndustry') {
                                                vmAdvanceSearch.hasIndustry(true);
                                                vmAdvanceSearch.hasBuisness(false);
                                            } else {
                                                vmAdvanceSearch.hasBuisness(true);
                                                vmAdvanceSearch.hasIndustry(false);
                                            }
                                        }
                                        function validateSearchFormBeforeSubmit() {
                                            if ($(".ad_search_type:checked").length == 0) {
                                                alertify.error('Please select search type');
                                                return false;
                                            }
                                            if ($('#search_by_industry').is(':disabled')) {
                                                if ($('#search_by_buisness').val() == '') {
                                                    alertify.error('Please enter buisness text to search');
                                                    return false;
                                                }
                                            } else {
                                                if ($('#search_by_industry').val() == '') {
                                                    alertify.error('Please enter industry text to search');
                                                    return false;
                                                }
                                            }
                                            $("#suggestion-search").submit();
                                        }
                                        vmWidzardOneUpdate = {
                                            top_list: ko.observableArray(),
                                            child_top_list: ko.observableArray(),
                                            wiz_one_part_one: ko.observable(true),
                                            label_sel_sector_img: ko.observable(''),
                                            wiz_one_part_two: ko.observable(false),
                                            wiz_one_part_three: ko.observable(false),
                                            wiz_one_part_four: ko.observable(false),
                                            wiz_one_part_five: ko.observable(false),
                                            skip: ko.observable(''),
                                            selected_top: ko.observable(),
                                            selected_sector_id: ko.observable(''),
                                            selected_sector_name: ko.observable(''),
                                            selected_subsector_id: ko.observable(''),
                                            selected_subsector_name: ko.observable(''),
                                            label_sel_sector: ko.observable(''),
                                            label_sel_sub_sector: ko.observable(''),
                                            label_sel_sub_sector_img: ko.observable(''),
                                            sub_sector_list: ko.observableArray(),
                                            buisness_list: ko.observableArray(),
                                            getChildList: function () {
                                                selected_list = this;
                                                vmWidzardOneUpdate.selected_top(selected_list.top_name);
                                                $.post('<?php echo base_url(); ?>category/get_sector_list', {top_link_id: selected_list.top_code}, function (result_child_list) {
                                                    result_child_list = eval("(" + result_child_list + ")");
                                                    vmWidzardOneUpdate.child_top_list(result_child_list);
                                                    if (vmWidzardOneUpdate.child_top_list().length == 1) {
                                                        $('.biz_child_list>li>a').trigger("click");
                                                        vmWidzardOneUpdate.skip(2);
                                                    }
                                                });
                                            },
                                            selectSetorCode: function () {
                                                sub_sector = this;
                                                vmWidzardOneUpdate.selected_sector_id(this.sector_code);
                                                vmWidzardOneUpdate.selected_sector_name(this.sector_name)
                                                $.post('<?php echo base_url(); ?>category/get_subsector_list', {sector_id: sub_sector.sector_code, top_link_list_id: selected_list.top_code}, function (result_subchild_list) {
                                                    result_subchild_list = eval("(" + result_subchild_list + ")");
                                                    vmWidzardOneUpdate.sub_sector_list(result_subchild_list);
                                                    if (vmWidzardOneUpdate.sub_sector_list().length == 1) {
                                                        $('.sub_sector_list>li>a').trigger("click");
                                                        vmWidzardOneUpdate.skip(3);
                                                    }
                                                });
                                            },
                                            getBuisnessList: function () {
                                                sel_buiz = this;
                                                vmWidzardOneUpdate.selected_subsector_id(this.subsector_code);
                                                vmWidzardOneUpdate.selected_subsector_name(this.subsector_name);
                                                $.post('<?php echo base_url(); ?>category/get_bussiness_list', {subsector_id: vmWidzardOneUpdate.selected_subsector_id(), sector_id: sub_sector.sector_code, top_link_list_id: selected_list.top_code}, function (result_subbiz_list) {
                                                    result_subbiz_list = eval("(" + result_subbiz_list + ")");
                                                    vmWidzardOneUpdate.buisness_list(result_subbiz_list);
                                                    if (vmWidzardOneUpdate.buisness_list().length == 1) {
                                                        $('.biz_serve_list>li>a').trigger("click");
                                                        vmWidzardOneUpdate.skip(4);
                                                        //vmWidzardOneUpdate.saveWizSecond();
                                                    }
                                                });
                                            },
                                            setBuisDetail: function () {
                                                buisness_det = this;
                                                //console.log(buisness_det);
                                            },
                                            saveWizSecond: function () {
                                                if (buisness_det != null) {
                                                    $.post('<?php echo base_url(); ?>category/isThisCodeAlredyexistinwizone', {top_link_id: selected_list.top_code}, function (result) {
                                                        if (result == 'false') {
                                                            $.post('<?php echo base_url(); ?>category/getUserOwninducstryCodes', {}, function (result) {
                                                                if (result <= 3) {
                                                                    $.post('<?php echo base_url(); ?>category/getUserOwnIndusNo', {}, function (result_data) {
                                                                        if (result_data < 10) {
                                                                            $.post('<?php echo base_url(); ?>category/setup_wiz_one_part_two', {top_link_id: selected_list.top_code, sector_id: sub_sector.sector_code, subsector_id: vmWidzardOneUpdate.selected_subsector_id(), bussiness_id: buisness_det.bussiness_code}, function (result_data) {
                                                                                if (result_data == 'active') {
                                                                                    vmWidzardOneUpdate.wiz_one_part_five(true);
                                                                                    vmWidzardOneUpdate.wiz_one_part_four(false);
                                                                                    vmWidzardOneUpdate.wiz_one_part_three(false);
                                                                                    vmWidzardOneUpdate.wiz_one_part_two(false);
                                                                                    vmWidzardOneUpdate.wiz_one_part_one(false);
                                                                                    selected_list = null;
                                                                                    sub_sector = null;
                                                                                    sel_buiz = null;
                                                                                    location.reload();
                                                                                } else if (result_data == 'exist') {
                                                                                    alertify.alert("This is the same business codes that you set up previously!");
                                                                                } else {
                                                                                    alertify.error("This is embrassing.Something error occured.Please try again!");
                                                                                }
                                                                            });
                                                                        } else {
                                                                            alertify.alert("You can only add 10 target industry");
                                                                        }
                                                                    });
                                                                } else {
                                                                    alertify.alert("You have already selected three types of top line.");
                                                                }
                                                            });
                                                        } else {
                                                            alertify.alert("This is the same business codes that you set up previously!");
                                                        }
                                                    });
                                                    /*$.post('<?php echo base_url(); ?>category/getUserOwninducstryCodes',{},function(result){
                                                     if(result<=3){
                                                     $.post('<?php echo base_url(); ?>category/setup_wiz_one_part_two',{top_link_id:selected_list.top_code,sector_id:sub_sector.sector_code,subsector_id:vmWidzardOneUpdate.selected_subsector_id(),bussiness_id:buisness_det.bussiness_code},function(result_data){
                                                     if(result_data=='active'){                    
                                                     vmWidzardOneUpdate.wiz_one_part_five(true);
                                                     vmWidzardOneUpdate.wiz_one_part_four(false);
                                                     vmWidzardOneUpdate.wiz_one_part_three(false);
                                                     vmWidzardOneUpdate.wiz_one_part_two(false);
                                                     vmWidzardOneUpdate.wiz_one_part_one(false);
                                                     selected_list=null;
                                                     sub_sector= null;
                                                     sel_buiz=null;
                                                     location.reload();
                                                     }else if(result_data=='exist'){
                                                     alertify.alert("This is the same business codes that you set up previously!");
                                                     }else{
                                                     alertify.error("This is embrassing.Something error occured.Please try again!");  
                                                     }
                                                     });
                                                     }else{
                                                     $.post('<?php echo base_url(); ?>category/getUserOwninducstryCodesMatched',{top_link_id:selected_list.top_code},function(result){
                                                     if(result=='true'){
                                                     $.post('<?php echo base_url(); ?>category/setup_wiz_one_part_two',{top_link_id:selected_list.top_code,sector_id:sub_sector.sector_code,subsector_id:vmWidzardOneUpdate.selected_subsector_id(),bussiness_id:buisness_det.bussiness_code},function(result_data){
                                                     if(result_data=='active'){                    
                                                     vmWidzardOneUpdate.wiz_one_part_five(true);
                                                     vmWidzardOneUpdate.wiz_one_part_four(false);
                                                     vmWidzardOneUpdate.wiz_one_part_three(false);
                                                     vmWidzardOneUpdate.wiz_one_part_two(false);
                                                     vmWidzardOneUpdate.wiz_one_part_one(false);
                                                     selected_list=null;
                                                     sub_sector= null;
                                                     sel_buiz=null;
                                                     }else if(result_data=='exist'){
                                                     alertify.alert("This is the same business codes that you set up previously.!");
                                                     }else{
                                                     alertify.error("This is embrassing.Something error occured.Please try again!");  
                                                     }
                                                     });
                                                     }else{
                                                     alertify.error("You have already selected three types of top line.");
                                                     }
                                                     });
                                                     }
                                                     });*/
                                                } else {
                                                    alertify.error("Please select your buisness type.!");
                                                }
                                            }
                                        };
                                        vmWidzardTwoAddNew = {
                                            industry_list: ko.observableArray(),
                                            child_sector_list: ko.observableArray(),
                                            add_new_wiz_two_part_one: ko.observable(true),
                                            add_new_wiz_two_part_two: ko.observable(false),
                                            add_new_wiz_two_part_three: ko.observable(false),
                                            getSectorChildList: function () {
                                                add_new_selected_industry = this;
                                                $.post('<?php echo base_url(); ?>category/get_target_sector_list', {selected_industry_code: add_new_selected_industry.industry_code}, function (result_child_list) {
                                                    result_child_list = eval("(" + result_child_list + ")");
                                                    vmWidzardTwoAddNew.child_sector_list(result_child_list.target_sector_list);
                                                });
                                            },
                                            setSector: function () {
                                                add_new_selected_sector = this;
                                            },
                                            showPartOneIndustryList: function () {
                                                vmWidzardTwoAddNew.add_new_wiz_two_part_one(true);
                                                vmWidzardTwoAddNew.add_new_wiz_two_part_two(false);
                                            },
                                            addAnotherCustomerType: function () {
                                                if (add_new_selected_sector != null) {
                                                    $.post('<?php echo base_url(); ?>category/setup_wiz_two', {looking_for_biz: '', locality_to_explore: '', services: '', industry_you_serve: add_new_selected_industry.industry_code, target_sector: add_new_selected_sector.tar_sector_code, looking_service: '', type: 'insert_new_customer_type'}, function (result_data) {
                                                        console.log(result_data);
                                                        if (result_data == 'true') {
                                                            vmWidzardTwoAddNew.add_new_wiz_two_part_two(false);
                                                            vmWidzardTwoAddNew.add_new_wiz_two_part_three(true);
                                                            add_new_selected_industry = null;
                                                            add_new_selected_sector = null;
                                                        } else if (result_data == 'exist') {
                                                            alert("Same target buisness code already exit!");
                                                        } else {
                                                            alert("This is embrassing.Something error occured.Please try again!");
                                                        }
                                                    });
                                                } else {
                                                    alert("Please select your sector");
                                                    return false;
                                                }
                                            }
                                        }
                                        vmWidzardSecond = {
                                            part_one: ko.observable(true),
                                            part_two: ko.observable(false),
                                            part_three: ko.observable(false),
                                            part_four: ko.observable(false),
                                            part_five: ko.observable(false),
                                            part_six: ko.observable(false),
                                            part_seven: ko.observable(false),
                                            part_eight: ko.observable(false),
                                            looking_for_list: ko.observableArray(),
                                            selected_looking_for_list: ko.observableArray(),
                                            industry_list: ko.observableArray(),
                                            locality_list: ko.observableArray(),
                                            selected_locality_list: ko.observableArray(),
                                            selected_service: ko.observableArray(),
                                            service_list: ko.observableArray(),
                                            child_sector_list: ko.observableArray(),
                                            biz_serve_list: ko.observableArray(),
                                            sel_biz_serve_list: ko.observableArray(),
                                            setLookingForList: function () {
                                                console.log(this);
                                                var sel_looking_item = this;
                                                var is_removed = 0;
                                                if (vmWidzardSecond.selected_looking_for_list().length > 0) {
                                                    for (var i = 0; i < vmWidzardSecond.selected_looking_for_list().length; i++) {
                                                        if (vmWidzardSecond.selected_looking_for_list()[i].looking_for_id == sel_looking_item.looking_for_id) {
                                                            vmWidzardSecond.selected_looking_for_list.remove(function (item) {
                                                                if (sel_looking_item.looking_for_id == item.looking_for_id) {
                                                                    is_removed++;
                                                                    return true;
                                                                }
                                                            });
                                                        }
                                                    }
                                                    if (is_removed == 0)
                                                        vmWidzardSecond.selected_looking_for_list.push(sel_looking_item);
                                                } else {
                                                    vmWidzardSecond.selected_looking_for_list.push(sel_looking_item);
                                                }
                                            },
                                            selLocalityList: function () {
                                                //debugger;
                                                var sel_locality_item = this;
                                                var is_removed = 0;
                                                if (vmWidzardSecond.selected_locality_list().length > 0) {
                                                    for (var i = 0; i < vmWidzardSecond.selected_locality_list().length; i++) {
                                                        if (vmWidzardSecond.selected_locality_list()[i].locality_id == sel_locality_item.locality_id) {
                                                            vmWidzardSecond.selected_locality_list.remove(function (item) {
                                                                if (sel_locality_item.locality_id == item.locality_id) {
                                                                    is_removed++;
                                                                    return true;
                                                                }
                                                            });
                                                        }
                                                    }
                                                    if (is_removed == 0)
                                                        vmWidzardSecond.selected_locality_list.push(sel_locality_item);
                                                } else {
                                                    vmWidzardSecond.selected_locality_list.push(sel_locality_item);
                                                }
                                            },
                                            setBizServe: function () {
                                                var sel_biz_serve_item = this;
                                                var is_removed = 0;
                                                if (vmWidzardSecond.sel_biz_serve_list().length > 0) {
                                                    for (var i = 0; i < vmWidzardSecond.sel_biz_serve_list().length; i++) {
                                                        if (vmWidzardSecond.sel_biz_serve_list()[i].look_service_id == sel_biz_serve_item.look_service_id) {
                                                            vmWidzardSecond.sel_biz_serve_list.remove(function (item) {
                                                                if (sel_biz_serve_item.look_service_id == item.look_service_id) {
                                                                    is_removed++;
                                                                    return true;
                                                                }
                                                            });
                                                        }
                                                    }
                                                    if (is_removed == 0)
                                                        vmWidzardSecond.sel_biz_serve_list.push(sel_biz_serve_item);
                                                } else {
                                                    vmWidzardSecond.sel_biz_serve_list.push(sel_biz_serve_item);
                                                }
                                            },
                                            showPartTwo: function () {
                                                vmWidzardSecond.part_one(false);
                                                vmWidzardSecond.part_two(true);
                                            },
                                            setService: function () {
                                                var sel_ser = this;
                                                var is_removed = 0;
                                                if (vmWidzardSecond.selected_service().length > 0) {
                                                    for (var i = 0; i < vmWidzardSecond.selected_service().length; i++) {
                                                        if (vmWidzardSecond.selected_service()[i].service_id == sel_ser.service_id) {
                                                            vmWidzardSecond.selected_service.remove(function (item) {
                                                                if (sel_ser.service_id == item.service_id) {
                                                                    is_removed++;
                                                                    return true;
                                                                }
                                                            });
                                                        }
                                                    }
                                                    if (is_removed == 0)
                                                        vmWidzardSecond.selected_service.push(sel_ser);
                                                } else {
                                                    vmWidzardSecond.selected_service.push(this);
                                                }
                                            },
                                            getSectorChildList: function () {
                                                selected_industry = this;
                                                $.post('<?php echo base_url(); ?>category/get_target_sector_list', {selected_industry_code: selected_industry.industry_code}, function (result_child_list) {
                                                    result_child_list = eval("(" + result_child_list + ")");
                                                    vmWidzardSecond.child_sector_list(result_child_list.target_sector_list);
                                                });
                                            },
                                            setSector: function () {
                                                selected_sector = this;
                                            }
                                        }
                                        var vmNotification = {
                                            notification_list: ko.observableArray(),
                                            notification_unseen: ko.observableArray(),
                                            connection_request_list: ko.observableArray(),
                                            connection_request_count: ko.observable(),
                                            message_sub: ko.observable(''),
                                            message_display: ko.observable(''),
                                            message: ko.observable(''),
                                            send_msg_user_id: ko.observable(''),
                                            count_unread_message: ko.observable(0),
                                            count_read_message: ko.observable(0),
                                            quickSendMessage: function () {
                                                //console.log(this);
                                                vmNotification.send_msg_user_id(this.sender_id);
                                                vmNotification.message_display(this.msg_body);
                                                vmNotification.message_sub(this.msg_sub);
                                                var message_id = this.sender_id;
                                                $('#send_message_modal_from_notification_link').trigger('click');
                                                try {
                                                    $.post('<?php echo base_url(); ?>member/updateMessageSeenTable', {mt_id: message_id}, function (result_request) {
                                                        if (result_request == 1) {
                                                            $.post('<?php echo base_url(); ?>member/getUnreadMessage', {}, function (result_unreadmessage) {
                                                                result_unreadmessage = eval("(" + result_unreadmessage + ")");
                                                                //console.log(result_unreadmessage);
                                                                vmNotification.count_unread_message(result_unreadmessage);
                                                            });
                                                            //vmNotification.connection_request_list.remove(function (item) {
                                                        }
                                                    });
                                                } catch (e) {
                                                    alertify.alert(e);
                                                }
                                                return false;
                                            },
                                            notification_count: ko.observable(false),
                                            redirectToMyNetwork: function () {
                                                window.location.replace("<?php echo base_url() ?>memberprofile/connection");
                                            },
                                            redirectToMessage: function () {
                                                window.location.replace("<?php echo base_url() ?>message/msglist/cm");
                                            },
                                            acceptRequest: function () {
                                                var sel_obj = this;
                                                $.post('<?php echo base_url(); ?>member/acceptRequestForConnection', {cid: sel_obj.cid}, function (result_request) {
                                                    if (result_request == 1) {
                                                        vmNotification.connection_request_list.remove(function (item) {
                                                            if (sel_obj.cid == item.cid) {
                                                                return true;
                                                            }
                                                        });
                                                    } else {
                                                        alert('Some error occured');
                                                    }
                                                });
                                            },
                                            seenRequest: function () {
                                                $.post('<?php echo base_url(); ?>member/requestConnectionSeen', {cid: this.cid}, function (result_request) {
                                                    if (result_request == 1) {
                                                        vmNotification.connection_request_count(vmNotification.connection_request_count() - 1);
                                                    } else {
                                                        alert('Some error occured');
                                                    }
                                                });
                                            }
                                        }
                                        function createCookie(cname, cvalue, exdays, path) {
                                            if (!path) {
                                                path = '/';
                                            }
                                            var d = new Date();
                                            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
                                            var expires = "expires=" + d.toUTCString();
                                            document.cookie = cname + "=" + cvalue + "; " + expires + "; path=" + path;
                                        }

                                        function readCookie(name) {
                                            var nameEQ = name + "=";
                                            var ca = document.cookie.split(';');
                                            for (var i = 0; i < ca.length; i++) {
                                                var c = ca[i];
                                                while (c.charAt(0) == ' ')
                                                    c = c.substring(1, c.length);
                                                if (c.indexOf(nameEQ) == 0)
                                                    return c.substring(nameEQ.length, c.length);
                                            }
                                            return null;
                                        }
                                        function validateSuggestionBeforeSubmit() {
                                            //debugger;
                                            if (check_wiz_two == "false") {
                                                $.post('<?php echo base_url(); ?>member/checkDbWizTwoInput', {}, function (result_request) {
                                                    if (result_request == 'true') {
                                                        if ($("#select option:selected").length > 0) {
                                                            createCookie('set_selected_looking_for', $('#select').val());
                                                            $("#suggestion-search").submit();
                                                        }
                                                        else {
                                                            alertify.error("Please sected a option for suggestion");
                                                        }
                                                    } else {
                                                        alertify.error('Please fill up Wizard Two');
                                                    }
                                                });
                                            } else {
                                                if ($("#select option:selected").length > 0) {
                                                    createCookie('set_selected_looking_for', $('#select').val());
                                                    $("#suggestion-search").submit();
                                                }
                                                else {
                                                    alertify.error("Please sected a option for suggestion");
                                                }
                                            }

                                        }
                                        function OpenModalForAddAreaOfWork() {
                                            $.post('<?php echo base_url(); ?>member/getUserOwnIndusNo', {}, function (result_request) {
                                                if (result_request <= 10) {
                                                    $('.top_list>li').removeClass('active');
                                                    vmWidzardOneUpdate.wiz_one_part_one(true);
                                                    vmWidzardOneUpdate.wiz_one_part_two(false);
                                                    vmWidzardOneUpdate.wiz_one_part_three(false);
                                                    vmWidzardOneUpdate.wiz_one_part_four(false);
                                                    vmWidzardOneUpdate.wiz_one_part_five(false);
                                                } else {
                                                    $('.fancybox-close').trigger('click');
                                                    alertify.error("You have already added 10 codes");
                                                }
                                            });
                                        }
                                        function OpenModalForAddNewCustomer() {
                                            $('.industry_list>li').removeClass('active');
                                            vmWidzardTwoAddNew.add_new_wiz_two_part_one(true);
                                            vmWidzardTwoAddNew.add_new_wiz_two_part_two(false);
                                            vmWidzardTwoAddNew.add_new_wiz_two_part_three(false);
                                            add_new_selected_industry = null;
                                            add_new_selected_sector = null;
                                        }
                                        $("#search_by_industry").keyup(function () {
                                            $("#loader-wrapper").show();
                                            $.post('<?php echo base_url(); ?>category/readCountry', {keyword: $(this).val()}, function (result) {
                                                $("#loader-wrapper").hide();
                                                result = eval("(" + result + ")");
                                                vmAdvanceSearch.search_industry_list(result);
                                                $("#suggesstion-box").show();
                                                $("#search-box").css("background", "#FFF");
                                            });
                                        });

                                        $(function () {
                                            ko.bindingHandlers.trimText = {
                                                init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
                                                    var trimmedText = ko.computed(function () {
                                                        var untrimmedText = ko.utils.unwrapObservable(valueAccessor());
                                                        var defaultMaxLength = 20;
                                                        var minLength = 5;
                                                        var maxLength = ko.utils.unwrapObservable(allBindingsAccessor().trimTextLength) || defaultMaxLength;
                                                        if (maxLength < minLength)
                                                            maxLength = minLength;
                                                        var text = untrimmedText.length > maxLength ? untrimmedText.substring(0, maxLength - 1) + '...' : untrimmedText;
                                                        return text;
                                                    });
                                                    ko.applyBindingsToNode(element, {
                                                        text: trimmedText
                                                    }, viewModel);

                                                    return {
                                                        controlsDescendantBindings: true
                                                    };
                                                }
                                            };
                                            try {
                                                $.post('<?php echo base_url(); ?>member/get_country', {}, function (result) {
                                                    result = eval("(" + result + ")");
                                                    //console.log(JSON.parse(result));
                                                    vmAdvanceSearch.country_list(result.country);
                                                    //$("#select1").select2({                 
                                                    //allowClear: true
                                                    //});
                                                });
                                            } catch (e) {
                                                alertify.alert(e);
                                            }
                                            try {
                                                $.post('<?php echo base_url(); ?>category/get_looking_for_list', {}, function (result) {
                                                    result = eval("(" + result + ")");
                                                    //console.log(JSON.parse(result));
                                                    vmWidzardSecond.looking_for_list(result);
                                                });
                                            } catch (e) {
                                                alert(e);
                                            }
                                            try {
                                                $.post('<?php echo base_url(); ?>category/get_locality_list', {}, function (result) {
                                                    result = eval("(" + result + ")");
                                                    //console.log(JSON.parse(result));
                                                    vmWidzardSecond.locality_list(result);
                                                });
                                            } catch (e) {
                                                alert(e);
                                            }
                                            try {
                                                $.post('<?php echo base_url(); ?>category/get_is_service_list', {}, function (result) {
                                                    result = eval("(" + result + ")");
                                                    //console.log(JSON.parse(result));
                                                    vmWidzardSecond.service_list(result.is_service);
                                                });
                                            } catch (e) {
                                                alert(e);
                                            }
                                            try {
                                                $.post('<?php echo base_url(); ?>category/get_industry_list', {}, function (result) {
                                                    result = eval("(" + result + ")");
                                                    //console.log(JSON.parse(result));
                                                    vmWidzardSecond.industry_list(result.industry_list);
                                                    vmWidzardTwoAddNew.industry_list(result.industry_list);
                                                });
                                            } catch (e) {
                                                alert(e);
                                            }
                                            try {
                                                $.post('<?php echo base_url(); ?>category/looking_services_list', {}, function (result) {
                                                    result = eval("(" + result + ")");
                                                    vmWidzardSecond.biz_serve_list(result.target_sector_list);

                                                });
                                            } catch (e) {
                                                alert(e);
                                            }
                                            try {
                                                $.post('<?php echo base_url(); ?>category/get_top_list', {}, function (result) {
                                                    result = eval("(" + result + ")");
                                                    //console.log(JSON.parse(result));
                                                    vmWidzardOneUpdate.top_list(result);
                                                });
                                            } catch (e) {
                                                alert(e);
                                            }
                                            try {
                                                $("#send_message_form_from_notification").validate({
                                                    rules: {
                                                        message: {required: true}
                                                    },
                                                    messages: {
                                                        message: {required: "Please enter the message"}
                                                    },
                                                    submitHandler: function (form) {
                                                        $.post('<?php echo base_url(); ?>message/sendMessage', {reciever_id: vmNotification.send_msg_user_id(), subject: vmNotification.message_sub(), message: vmNotification.message()}, function (result) {
                                                            $('#loader-wrapper').hide();
                                                            if (result == 1) {
                                                                alertify.success("Message Send");
                                                                $('.fancybox-close').trigger('click');
                                                            } else {
                                                                alertify.success("Some error occured.Please try again");
                                                            }
                                                        });
                                                        return false;
                                                    }
                                                });
                                            } catch (e) {
                                                alert(e);
                                            }
                                            try {
                                                $.post('<?php echo base_url(); ?>member/fetchWhoViewedMyProfile', {}, function (result_notification) {
                                                    var result_list = eval("(" + result_notification + ")");//JSON.parse(result_notification);
                                                    vmNotification.notification_list(result_list.notification_list);
                                                    vmNotification.notification_unseen(result_list.notification_unseen);
                                                    if (vmNotification.notification_list().length > 0) {
                                                        vmNotification.notification_count(true);
                                                        $('#notify-open-down').addClass('notify-open-down');
                                                    } else {
                                                        $('#notify-open-down').removeClass('notify-open-down');
                                                    }
                                                    $(".content").mCustomScrollbar({
                                                        autoHideScrollbar: true,
                                                        theme: "rounded"
                                                    });
                                                });
                                                $.post('<?php echo base_url(); ?>member/requestConnectList', {}, function (result_connection) {
                                                    var result_list = eval("(" + result_connection + ")");//JSON.parse(result_notification);
                                                    //console.log(result_list);
                                                    var count = 0;
                                                    vmNotification.connection_request_list(result_list);

                                                    //console.log(vmNotification.connection_request_list());
                                                    for (var i = 0; i < result_list.length; i++) {
                                                        if (result_list[i].is_seen == 0) {
                                                            count++
                                                        }
                                                    }
                                                    if (vmNotification.connection_request_list().length > 0) {
                                                        vmNotification.connection_request_count(count);
                                                        $('#people-open-down').addClass('people-open-down');
                                                    } else {
                                                        $('#people-open-down').removeClass('people-open-down');
                                                    }
                                                });
                                                getUnreadMessage();
                                            } catch (e) {
                                                alert(e);
                                            }
                                            $("#send_message_modal_from_notification_link").fancybox();
                                            $("#img_upload").fancybox();//popup modal for upload image
                                            $("#back_image").fancybox();//popup modal for upload image  
                                            $("#add_info_wiz_one_link").fancybox();
                                            $("#add_new_customer_link").fancybox();
                                            $('#improve_match_link').fancybox();
                                            // Change title type, overlay closing speed
                                            $(".fancybox-effects-a").fancybox({
                                                helpers: {
                                                    title: {
                                                        type: 'outside'
                                                    },
                                                    overlay: {
                                                        speedOut: 0
                                                    }
                                                }
                                            });
                                            // Disable opening and closing animations, change title type
                                            $(".fancybox-effects-b").fancybox({
                                                openEffect: 'none',
                                                closeEffect: 'none',
                                                helpers: {
                                                    title: {
                                                        type: 'over'
                                                    }
                                                }
                                            });
                                            // Set custom style, close if clicked, change title type and overlay color
                                            $(".fancybox-effects-c").fancybox({
                                                wrapCSS: 'fancybox-custom',
                                                closeClick: true,
                                                openEffect: 'none',
                                                helpers: {
                                                    title: {
                                                        type: 'inside'
                                                    },
                                                    overlay: {
                                                        css: {
                                                            'background': 'rgba(238,238,238,0.85)'
                                                        }
                                                    }
                                                }
                                            });
                                            // Remove padding, set opening and closing animations, close if clicked and disable overlay
                                            $(".fancybox-effects-d").fancybox({
                                                padding: 0,
                                                openEffect: 'elastic',
                                                openSpeed: 150,
                                                closeEffect: 'elastic',
                                                closeSpeed: 150,
                                                closeClick: true,
                                                helpers: {
                                                    overlay: null
                                                }
                                            });
                                            //------------------------- crop image --------------------

                                            if (check_wiz_two == "false") {
                                                $("#improve_match_link").trigger("click");
                                            }
                                            var options =
                                                    {
                                                        imageBox: '.imageBox',
                                                        thumbBox: '.thumbBox',
                                                        spinner: '.spinner',
                                                        imgSrc: 'avatar.png'
                                                    }
                                            var cropper;
                                            document.querySelector('#file').addEventListener('change', function () {
                                                var reader = new FileReader();
                                                reader.onload = function (e) {
                                                    options.imgSrc = e.target.result;
                                                    cropper = new cropbox(options);
                                                }
                                                reader.readAsDataURL(this.files[0]);
                                                this.files = [];
                                            })
                                            document.querySelector('#btnCrop').addEventListener('click', function () {
                                                try {
                                                    var img = cropper.getDataURL();

                                                    //var v = $('#profile_pic').attr('src');
                                                    //var x = img.split('/');
                                                    //if(x[7] == 'profile-img.png'){
                                                    //$('#error_img').html('<p>Please select a picture to upload</p>');
                                                    // }else{
                                                    $("#loader-wrapper").show();
                                                    $.post('<?php echo base_url(); ?>memberprofile/uploadImage', {'imgFile': img}, function (result) {
                                                        $("#loader-wrapper").hide();
                                                        if (result == true) {
                                                            $('#profile_pic').attr('src', img);
                                                            $('.fancybox-item').trigger('click');
                                                        } else {
                                                            alert("Please Crop Image to Upload");
                                                        }
                                                    });
                                                    //}
                                                } catch (e) {
                                                    alertify.error('Please select a image');
                                                }

                                                //document.querySelector('.cropped').innerHTML += '<img src="'+img+'">';

                                            })
                                            document.querySelector('#btnZoomIn').addEventListener('click', function () {
                                                cropper.zoomIn();
                                            })
                                            document.querySelector('#btnZoomOut').addEventListener('click', function () {
                                                cropper.zoomOut();
                                            });
                                            //------------------------- crop image --------------------

                                            //------------------------- crop image 2--------------------
                                            var options1 =
                                                    {
                                                        imageBox: '.imageBox1',
                                                        thumbBox: '.thumbBox1',
                                                        spinner: '.spinner1',
                                                        imgSrc: 'avatar.png'
                                                    }
                                            var cropper;
                                            document.querySelector('#file1').addEventListener('change', function () {
                                                var reader = new FileReader();
                                                reader.onload = function (e) {
                                                    options1.imgSrc = e.target.result;
                                                    cropper = new cropbox(options1);
                                                }
                                                reader.readAsDataURL(this.files[0]);
                                                this.files = [];
                                            })
                                            document.querySelector('#btnCrop1').addEventListener('click', function () {
                                                try {
                                                    var img = cropper.getDataURL()
                                                    //document.querySelector('.cropped').innerHTML += '<img src="'+img+'">';            
                                                    //var v = $('#profile_cover_pic').attr('src');
                                                    //var x = v.split('/');
                                                    //if(x[7] == 'profile-img.png'){
                                                    //$('#error_img1').html('<p>Please select a picture to upload</p>');
                                                    //}else{
                                                    $("#loader-wrapper").show();
                                                    $.post('<?php echo base_url(); ?>memberprofile/uploadBackImage', {'imgFile': img}, function (result) {
                                                        $("#loader-wrapper").hide();
                                                        if (result == true) {
                                                            $('.fancybox-close').trigger('click');
                                                            $('#profile_cover_pic').attr('src', img);
                                                        } else {
                                                            alertify.error('Some error occurd please try again')
                                                        }
                                                    });
                                                    //}
                                                } catch (e) {
                                                    alertify.error('Please select a image');
                                                }
                                            })
                                            document.querySelector('#btnZoomIn1').addEventListener('click', function () {
                                                cropper.zoomIn();
                                            })
                                            document.querySelector('#btnZoomOut1').addEventListener('click', function () {
                                                cropper.zoomOut();
                                            });
                                            //------------------------- crop image 2--------------------

                                        });
                                        function getUnreadMessage() {
                                            $.post('<?php echo base_url(); ?>member/getUnreadMessage', {}, function (result_unreadmessage) {
                                                result_unreadmessage = eval("(" + result_unreadmessage + ")");
                                                //console.log(result_unreadmessage);
                                                vmNotification.count_unread_message(result_unreadmessage);
                                                //debugger;
                                                //alert(vmNotification.count_unread_message().length);
                                                var count_read_message_display_from_4 = 4 - vmNotification.count_unread_message().length;
                                                if (count_read_message_display_from_4 > 0) {
                                                    getReadMessage(count_read_message_display_from_4);
                                                }
                                            });
                                        }
                                        function getReadMessage(count) {
                                            $.post('<?php echo base_url(); ?>member/getReadMessage', {count: count}, function (result_readmessage) {
                                                result_readmessage = eval("(" + result_readmessage + ")");
                                                vmNotification.count_read_message(result_readmessage);
                                            });
                                        }
                                        function nextWidzardAddNewWizTwo(part) {
                                            if (part == 1) {
                                                if (add_new_selected_industry != null) {
                                                    vmWidzardTwoAddNew.add_new_wiz_two_part_two(true);
                                                    vmWidzardTwoAddNew.add_new_wiz_two_part_one(false);
                                                } else {
                                                    alert('Please select your industry');
                                                }
                                            }
                                        }
                                        function nextWidzard(part) {
                                            if (part == 2) {
                                                if (vmWidzardSecond.selected_looking_for_list().length > 0) {
                                                    vmWidzardSecond.part_three(true);
                                                    vmWidzardSecond.part_two(false);
                                                } else {
                                                    alert('Please select category to proceed');
                                                }
                                            }
                                            if (part == 3) {
                                                if (vmWidzardSecond.selected_locality_list().length > 0) {
                                                    vmWidzardSecond.part_two(false);
                                                    vmWidzardSecond.part_four(true);
                                                    vmWidzardSecond.part_three(false);
                                                } else {
                                                    alert('Please select nationality to proceed');
                                                }
                                            }
                                            if (part == 4) {
                                                if (vmWidzardSecond.selected_service().length > 0) {
                                                    //$("#improve_match").css("width",'1100');
                                                    //console.log(vmWidzardSecond.selected_service());
                                                    if (vmWidzardSecond.selected_service().length == 1) {
                                                        if (vmWidzardSecond.selected_service()[0].service_id == 1) {
                                                            $.post('<?php echo base_url(); ?>category/setup_wiz_two', {looking_for_biz: vmWidzardSecond.selected_looking_for_list(), locality_to_explore: vmWidzardSecond.selected_locality_list(), services: vmWidzardSecond.selected_service(), type: 'consumer'}, function (result_data) {
                                                                //console.log(result_data);   
                                                                if (result_data == 'true') {
                                                                    vmWidzardSecond.part_two(false);
                                                                    vmWidzardSecond.part_eight(true);
                                                                    vmWidzardSecond.part_four(false);
                                                                } else {
                                                                    alert("This is embrassing.Something error occured.Please try again!");
                                                                }
                                                            });
                                                        } else {
                                                            vmWidzardSecond.part_two(false);
                                                            vmWidzardSecond.part_five(true);
                                                            vmWidzardSecond.part_four(false);
                                                            //$("#improve_match_link").trigger("click");
                                                        }
                                                    } else {
                                                        vmWidzardSecond.part_two(false);
                                                        vmWidzardSecond.part_five(true);
                                                        vmWidzardSecond.part_four(false);
                                                    }
                                                    $(".fancybox-close").trigger("click");
                                                    setTimeout(function () {
                                                        $("#improve_match_link").trigger("click");
                                                        vmWidzardSecond.part_two(false);
                                                    }, 500);
                                                } else {
                                                    alert('Please select service to proceed');
                                                }
                                            }
                                            if (part == 5) {
                                                if (selected_industry != '') {
                                                    vmWidzardSecond.part_one(false);
                                                    vmWidzardSecond.part_six(true);
                                                    vmWidzardSecond.part_five(false);
                                                } else {
                                                    alert('Please select your industry');
                                                }
                                            }
                                            if (part == 6) {
                                                if (selected_sector != '') {
                                                    if (check_wiz_two == 'true') {
                                                        //console.log(selected_industry); return false;
                                                        $.post('<?php echo base_url(); ?>category/isThisCodeAlredyexistinwiztwo', {code1: selected_industry.industry_code, code2: selected_sector.tar_sector_code}, function (result) {
                                                            if (result == 'false') {
                                                                $.post('<?php echo base_url(); ?>category/getUserTergetinducstryCodes', {}, function (result) {
                                                                    if (result <= 3) {
                                                                        $.post('<?php echo base_url(); ?>category/getUserTergetIndusNo', {target_industry_code: selected_industry.industry_code}, function (result) {
                                                                            //debugger;
                                                                            if (result < 10) {
                                                                                $.post('<?php echo base_url(); ?>category/setup_wiz_two', {looking_for_biz: vmWidzardSecond.selected_looking_for_list(), locality_to_explore: vmWidzardSecond.selected_locality_list(), services: vmWidzardSecond.selected_service(), industry_you_serve: selected_industry.industry_code, target_sector: selected_sector.tar_sector_code, looking_service: vmWidzardSecond.sel_biz_serve_list(), type: 'full'}, function (result_data) {
                                                                                    //console.log(result_data);   
                                                                                    if (result_data == 'true') {
                                                                                        vmWidzardSecond.part_two(false);
                                                                                        vmWidzardSecond.part_eight(true);
                                                                                        vmWidzardSecond.part_six(false);
                                                                                        if (window.location.href.indexOf("memberprofile") > -1) {
                                                                                            location.reload();
                                                                                        }
                                                                                    } else {
                                                                                        alert("This is embrassing.Something error occured.Please try again!");
                                                                                    }
                                                                                });
                                                                            } else {
                                                                                alertify.alert("You can only add 10 target industry.");
                                                                            }
                                                                        });
                                                                    } else {
                                                                        alertify.alert("You have already selected three types of top line.");
                                                                    }
                                                                });
                                                            } else {
                                                                alertify.alert("This is the same business codes that you set up previously!");
                                                            }
                                                        });
                                                        /*$.post('<?php echo base_url(); ?>category/getUserTergetinducstryCodes',{},function(result){
                                                         //alert(result);              
                                                         if(result<=3){
                                                         $.post('<?php echo base_url(); ?>category/getUserTergetinducstryCodesMatch',{target_industry_code:selected_industry.industry_code},function(result){
                                                         if(result=='true'){
                                                         $.post('<?php echo base_url(); ?>category/setup_wiz_two',{looking_for_biz:vmWidzardSecond.selected_looking_for_list(),locality_to_explore:vmWidzardSecond.selected_locality_list(),services:vmWidzardSecond.selected_service(),industry_you_serve:selected_industry.industry_code,target_sector:selected_sector.tar_sector_code,looking_service:vmWidzardSecond.sel_biz_serve_list(),type:'full'},function(result_data){                  
                                                         //console.log(result_data);   
                                                         if(result_data=='true'){
                                                         vmWidzardSecond.part_two(false);
                                                         vmWidzardSecond.part_eight(true);
                                                         vmWidzardSecond.part_six(false);
                                                         if(window.location.href.indexOf("memberprofile") > -1) {
                                                         location.reload();
                                                         }
                                                         }else{
                                                         alert("This is embrassing.Something error occured.Please try again!");
                                                         }      
                                                         });
                                                         }else{
                                                         alertify.alert("You can only add 10 target industry.");
                                                         }
                                                         });
                                                         }else{
                                                         alertify.alert("You have already selected three types of top line.");
                                                         }
                                                         });*/
                                                    } else {
                                                        vmWidzardSecond.part_two(false);
                                                        vmWidzardSecond.part_six(false);
                                                        vmWidzardSecond.part_seven(true);
                                                    }
                                                } else {
                                                    alert('Please select your sector');
                                                }
                                            }
                                            if (part == 7) {
                                                if (vmWidzardSecond.sel_biz_serve_list().length > 0) {
                                                    $.post('<?php echo base_url(); ?>category/setup_wiz_two', {looking_for_biz: vmWidzardSecond.selected_looking_for_list(), locality_to_explore: vmWidzardSecond.selected_locality_list(), services: vmWidzardSecond.selected_service(), industry_you_serve: selected_industry.industry_code, target_sector: selected_sector.tar_sector_code, looking_service: vmWidzardSecond.sel_biz_serve_list(), type: 'full'}, function (result_data) {
                                                        //console.log(result_data);   
                                                        if (result_data == 'true') {
                                                            vmWidzardSecond.part_two(false);
                                                            vmWidzardSecond.part_eight(true);
                                                            vmWidzardSecond.part_seven(false);
                                                            if (window.location.href.indexOf("memberprofile") > -1) {
                                                                location.reload();
                                                            }
                                                        } else {
                                                            alert("This is embrassing.Something error occured.Please try again!");
                                                        }
                                                    });
                                                } else {
                                                    alert("Please select the buisnesses that you serve");
                                                }
                                            }
                                            //$("#improve_match_link").trigger("click");
                                        }
                                        function nextWidzardOne(part) {
                                            //debugger;
                                            part = vmWidzardOneUpdate.skip() != '' ? vmWidzardOneUpdate.skip() : part;
                                            if (part == 1) {
                                                if (selected_list == null) {
                                                    alert('Please selected a buisness');
                                                } else {
                                                    console.log(selected_list);
                                                    vmWidzardOneUpdate.label_sel_sector(selected_list.top_name);
                                                    vmWidzardOneUpdate.label_sel_sector_img(selected_list.profile_image);
                                                    vmWidzardOneUpdate.wiz_one_part_one(false);
                                                    vmWidzardOneUpdate.wiz_one_part_two(true);
                                                }
                                            }
                                            if (part == 2) {
                                                if (sub_sector == null) {
                                                    alert('Please selected a sub sector of your buisness');
                                                } else {
                                                    console.log(sub_sector);
                                                    vmWidzardOneUpdate.label_sel_sub_sector(sub_sector.sector_name);
                                                    vmWidzardOneUpdate.label_sel_sub_sector_img(sub_sector.profile_image);
                                                    vmWidzardOneUpdate.wiz_one_part_one(false);
                                                    vmWidzardOneUpdate.wiz_one_part_two(false);
                                                    vmWidzardOneUpdate.wiz_one_part_three(true);
                                                    vmWidzardOneUpdate.wiz_one_part_four(false);
                                                }
                                            }
                                            if (part == 3) {
                                                if (sel_buiz == null) {
                                                    alert('Please selected a buisness');
                                                } else {
                                                    vmWidzardOneUpdate.wiz_one_part_one(false);
                                                    vmWidzardOneUpdate.wiz_one_part_two(false);
                                                    vmWidzardOneUpdate.wiz_one_part_three(false);
                                                    vmWidzardOneUpdate.wiz_one_part_four(true);
                                                }
                                            }
                                            if (part == 4) {
                                                vmWidzardOneUpdate.saveWizSecond();
                                            }
                                        }
                                        function previousWidzartOne(part) {
                                            if (part == 2) {
                                                vmWidzardOneUpdate.wiz_one_part_one(true);
                                                vmWidzardOneUpdate.wiz_one_part_two(false);
                                            }
                                            if (part == 3) {
                                                vmWidzardOneUpdate.wiz_one_part_two(true);
                                                vmWidzardOneUpdate.wiz_one_part_three(false);
                                            }
                                            if (part == 4) {
                                                vmWidzardOneUpdate.wiz_one_part_three(true);
                                                vmWidzardOneUpdate.wiz_one_part_four(false);
                                            }
                                        }
                                        function previousWidzart(part) {
                                            if (part == 2) {
                                                vmWidzardSecond.part_one(true);
                                                vmWidzardSecond.part_two(false);
                                            }
                                            if (part == 3) {
                                                vmWidzardSecond.part_two(true);
                                                vmWidzardSecond.part_three(false);
                                            }
                                            if (part == 4) {
                                                vmWidzardSecond.part_three(true);
                                                vmWidzardSecond.part_four(false);
                                            }
                                            if (part == 5) {
                                                // $("#improve_match").css("width",'700');
                                                vmWidzardSecond.part_four(true);
                                                vmWidzardSecond.part_five(false);
                                            }
                                            if (part == 6) {
                                                //debugger;
                                                //$(".fancybox-close").trigger("click");          
                                                //$("#improve_match_link").trigger("click");
                                                vmWidzardSecond.part_six(false);
                                                vmWidzardSecond.part_five(true);
                                            }
                                            if (part == 7) {
                                                //$("#improve_match").css("width",'1100')
                                                vmWidzardSecond.part_six(true);
                                                vmWidzardSecond.part_seven(false);
                                            }
                                            //$("#improve_match_link").trigger("click");
                                        }
                                        function setActive(curObj) {
                                            if ($(curObj).hasClass('active')) {
                                                $(curObj).removeClass('active');
                                            } else {
                                                $(curObj).addClass('active');
                                            }
                                        }
                                        function setIndActive(curObj, cur_class) {
                                            $("." + cur_class + ">li").removeClass('active');
                                            $(curObj).addClass('active');
                                        }
                                        /*IMAGE UPLOAD*/
                                        $('#uploadImg').click(function () {
                                            var v = $('#profile_pic').attr('src');
                                            var x = v.split('/');
                                            if (x[7] == 'profile-img.png') {
                                                $('#error_img').html('<p>Please select a picture to upload</p>');
                                            } else {
                                                $.post('<?php echo base_url(); ?>memberprofile/uploadImage', {'imgFile': v}, function (result) {
                                                    if (result == true) {
                                                        $('.fancybox-item').trigger('click');
                                                    } else {
                                                        alert("Please Crop Image to Upload");
                                                    }
                                                });
                                            }
                                        });
                                        $('#uploadCoverImg').click(function () {
                                            var v = $('#profile_cover_pic').attr('src');
                                            var x = v.split('/');
                                            if (x[7] == 'profile-img.png') {
                                                $('#error_img1').html('<p>Please select a picture to upload</p>');
                                            } else {
                                                $.post('<?php echo base_url(); ?>memberprofile/uploadBackImage', {'imgFile': v}, function (result) {
                                                    if (result == true)
                                                        $('.fancybox-close').trigger('click');
                                                    else {
                                                        $('.fancybox-item').trigger('click');
                                                    }
                                                });
                                            }
                                        });
                                        function readURL(input) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();
                                                reader.onload = function (e) {
                                                    $('#profile_pic')
                                                            .attr('src', e.target.result)
                                                            .width(152)
                                                            .height(152);
                                                };
                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }
                                        function readBackURL(input) {
                                            if (input.files && input.files[0]) {
                                                var reader = new FileReader();
                                                reader.onload = function (e) {
                                                    $('#profile_cover_pic')
                                                            .attr('src', e.target.result)
                                                            .width(1200)
                                                            .height(190);
                                                };
                                                reader.readAsDataURL(input.files[0]);
                                            }
                                        }
                                        /*IMAGE UPLOAD*/
                                        $(".inbox-img").click(function () {
                                            var device_width = $(window).width();
                                            if (device_width < 480) {
                                                alertify.error("On mobile you can’t visit someone’s profile");
                                                return false;
                                            }
                                        });
                                        ko.applyBindings(vmAdvanceSearch, $("#suggestion-search")[0]);
                                        ko.applyBindings(vmNotification, $("#notification_section")[0]);
                                        ko.applyBindings(vmWidzardSecond, $("#improve_match")[0]);
                                        ko.applyBindings(vmWidzardOneUpdate, $("#add_info_wiz_one")[0]);
                                        ko.applyBindings(vmWidzardTwoAddNew, $("#add_new_customer")[0]);
                                    </script>  
                                    </html>
