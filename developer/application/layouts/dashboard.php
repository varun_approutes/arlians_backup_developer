<div class="row" id="dashboard_display" style="display:none">
    <div class="col-md-9">
        <div class="col-md-6 col-sm-6">
            <div class="row" ng-controller="DashboardHub as hub">
                <div class="col-md-12" ng-show="show_default_hub">
                    <div class="box post_update">
                        <div class="post-header">
                            <ul class="post_link">
                                <li><a href="javascript:void(0)"><i class="fa fa-edit"></i> Status</a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-photo" ng-click="showPublishSection()"></i>Photos / <i class="fa fa-video-camera"></i> Videos</a></li>
                                <!--<li><a href="<?php echo base_url() ?>hub/hubPublish"><i class="fa fa-sign-out"></i> Publish</a></li>-->
                                <li><a href="javascript:void(0);"  ng-click="showPublishSection()"><i class="fa fa-sign-out"></i> Publish</a></li>
                            </ul>
                        </div>
                        <form ng-submit="sharePost()"> 
                            <div class="box-body">

                                <textarea class="form-control post_field" placeholder="What’s up in your mind?" ng-model="text_in_mind"></textarea>                    

                            </div>
                            <div class="post_footer clearfix">
                                <span style="color:#fff" ng-bind="show_selected">Hello world</span>
                                <!--<ul class="post_footerlink">
                                    <li><a href="#"><i class="fa fa-user-plus"></i></a></li>
                                    <li><a href="#"><i class="fa fa-video-camera"></i></a></li>
                                    <li><a href="#"><i class="fa fa-photo"></i></a></li>
                                </ul>-->
                                <div class="post_footebtn pull-right">
                                    <div class="btn-group post_footerdrpbtn">
                                        <i class="fa fa-gears pull-left"></i>
                                        <button aria-expanded="false" type="button" data-toggle="dropdown" class="btn  btn-sm dropdown-toggle">Share with</button>
                                        <ul role="menu" class="dropdown-menu pull-right">                                        
                                            <li><a href="javascript:void(0);" ng-click="select(list)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>
                                            <li><a href="#">Clear events</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#">View calendar</a></li>
                                        </ul>
                                        <!--                                        <div class="share_btn">
                                                                                    <div class="select-style">
                                                                                        <select ng-options="opt.shareId as opt.name for opt in share_with_list">
                                                                                        </select>
                                                                                    </div>
                                                                                </div>-->
                                    </div>
                                    <button type="button" class="btn post_btn" ng-click='sharePost()'>Post</button>
                                    <!--                                    <a href="javascript:void(0)" class="btn post_btn" ng-click='sharePost()'>Post</a>-->

                                </div>

                            </div>
                        </form>
                    </div>
                    <!-- <div class="box post_update">
                        <div class="post-header">
                            <div class="btn-group confidentiality pull-right">
                                <button class="btn  btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Confidentiality  <i class="fa fa-chevron-down"></i></button>
                                <ul class="dropdown-menu pull-right" role="menu">
                                    <li><a href="javascript:void(0)">Add new event</a></li>
                                    <li><a href="javascript:void(0)">Clear events</a></li>
                                    <li class="divider"></li>
                                    <li><a href="javascript:void(0)">View calendar</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="box-body">
                            <form>
                                <div class="user_pic">
                                    <img alt="" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg">
                                </div>
    
                                <textarea class="form-control post_field" placeholder="What’s up in your mind?"></textarea>
                                <div aria-label="..." role="group" class="btn-group btn-group-justified post_btngrp">
                                    <div role="group" class="btn-group">
                                        <button class="btn btn-primary" type="button"><i class="fa fa-edit"></i> Update</button>
                                    </div>
                                    <div role="group" class="btn-group">
                                        <button class="btn btn-primary" type="button"><i class="fa fa-photo"></i> Media</button>
                                    </div>
                                    <div role="group" class="btn-group">
                                        <button class="btn btn-primary" type="button"><i class="fa fa-sign-out"></i> Publish</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>-->
                </div>
                <!-- Modal -->
                <div class="modal fade" id="wiz_one_last_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div role="document" class="modal-dialog welcome_popup">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                                <h4 id="myModalLabel" class="modal-title">Welcome to your Hub!</h4>
                            </div>
                            <div class="modal-body">
                                <input type="text" class="welcominput" ng-model="user_buisnessname" placeholder="enter a business name">
                                <p>or click here to signup as a professional <input type="checkbox" ng-click="toggleIsProfrssional()" ng-model="is_profrssional" class="welcome_check"></p>
                                <input type="text" ng-model="user_firstname" class="welcominput seconinput" placeholder="First Name">
                                <input type="text" ng-model="user_lastname" class="welcominput lastinput" placeholder="Last Name">
                            </div>
                            <div class="modal-footer">

                                <button class="btn welcome_yellow" ng-click="welcomeWizard()" type="button">Let's go</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- post block -->     
                <div ng-show="show_default_hub" >
                    <div ng-repeat="current_publish in current_post_publish_list">
                        <div ng-if="current_publish.post == false">
                            <div class="col-md-12">
                                <div class="box latest_talk">
                                    <div class="box-body">
                                        <div class="image default_image">
                                            <span ng-show="current_publish.image == ''" ng-bind="user_initial"></span>
                                            <img class="sub_img" alt="" ng-show="current_publish.image != ''" ng-src="{{current_publish.path + current_publish.image}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg" src="<?php echo base_url(); ?>assets_phase2/images/Alchemy-icon.png">
                                        </div>
                                        <div class="latest_content">

                                            <div class="latest_header clearfix">
                                            <h6><avi ng-bind="current_publish.buisname">Mike Johns</avi> <!--<span>author</span>--></h6>
                                                <!--                                                <div class="btn-group confidentiality pull-right">
                                                                                                    <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                                                                    <ul role="menu" class="dropdown-menu pull-right">
                                                                                                        <li><a href="javascript:void(0)">Add new event</a></li>
                                                                                                        <li><a href="javascript:void(0)">Clear events</a></li>
                                                                                                        <li class="divider"></li>
                                                                                                        <li><a href="javascript:void(0)">View calendar</a></li>
                                                                                                    </ul>
                                                                                                </div>-->
                                            </div>

<!-- <span class="post_time">9 Min ago</span>  -->
<!--                            <p>Watch these adorable robots play follow the leader:</p>                  -->      
                                        </div>
                                    </div>
                                    <div class="post_innercont clearfix">
                                        <!--                                <a  target="_balnk" >-->
                                        <img class="post_img" alt="" ng-if="current_publish.image_post != ''" ng-src="{{current_publish.image_post}}"  src=""/>
                                        <!--                                </a>-->
                                        <div>
                                            <!--<div class="post_innertxt" ng-if="news.author ==''" class = "full-width">-->
                                            <!--                                    <a  target="_balnk"><h5>The Future of Flying Robots For Suistanable Farming</h5></a>-->

                                            <p ng-bind-html="current_publish.html_text | limitTo: 300">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>
                                            <a href="javascript:void(0)" ng-show="current_publish.hub_title != ''" ng-click="showHubDetails(current_publish, 'current_post')">...</a>
        <!--                                    <p>{{current_publish.html_text | strLimit: 120}}</p>-->
                                        </div>
                                    </div>
                                    <div class="box-footer post_bottom clearfix">
                                        <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
            <!--                                    <li><a href="javascript:void(0)"><i class="fa fa-share-alt"></i> Share</a></li>-->
                                                <li><a href="javascript:void(0)" ng-click="open_comment_Animation(current_publish)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                <li><a href="javascript:void(0)" ng-class="{'like': current_publish.like}" ng-click="feedback(current_publish)"><i ng-class="{'fa fa-heart' :current_publish.like, 'fa fa-heart-o' :!current_publish.like}" class="fa fa-heart-o"></i> Like</a></li>
                                            </ul>

                                            <ul class="latest_footerlink_right pull-right">
            <!--                                    <li><a href="javascript:void(0)">43 <i class="fa fa-share-alt"></i></a></li>-->
                                                <li><a href="javascript:void(0)" ng-click="commentsAnimation(current_publish)"><span ng-bind="current_publish.comments_count">45 </span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span ng-bind="current_publish.like_count"></span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="comment_box" ng-show="current_publish.open_comment_box">
                                            <div class="comment_default"> 
                                                <span ng-show="current_publish.image == ''" ng-bind="user_initial"></span>
                                                <img class="comment_img" ng-show="current_publish.image != ''" ng-src="{{current_publish.path + current_publish.image}}" />
                                            </div>
                                            <input type="text" ng-model="current_publish.comment" class="form-control"  placeholder="Write your comment">
                                            <button class="btn btn_publish" ng-click="postComments(current_publish)">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-11 col-md-offset-1" ng-show="current_publish.show_comments" ng-repeat="comment in current_publish.comments">
                                <div class="box latest_talk comment_reply">
                                    <div class="box-body">
                                        <div class="image default_image">
                                            <span ng-show="current_publish.image == ''" ng-bind="user_initial"></span>
                                            <img ng-show="comment.image != ''" ng-src="{{comment.image}}" src="images/post-userimg.jpg" alt="" class="sub_img">
                                        </div>
                                        <div class="latest_content">
                                            <div class="mCustomScrollbar">
                                                <div class="latest_header clearfix">
    <!--                                                <h6>Mike Johns <span>verified talker</span></h6>-->
                                                    <h6 ng-bind="comment.buisnesname">Mike Johns</h6>
                                                </div>
                                                <!--<span class="post_time">9 Min ago</span>-->  
                                                <p ng-bind="comment.comment_view">Thats a really nice job cant wait to see it </p>
                                            </div>                  
                                        </div>
                                    </div>

                                    <!--<div class="box-footer post_bottom clearfix">
                                        <ul class="latest_footerlink pull-left">
                                            <li><a href="#"><i class="fa fa-share-alt"></i> <span>Share</span></a></li>
                                            <li><a href="#"><i class="fa  fa-comment-o"></i> <span>Comments</span></a></li>
                                            <li><a href="#"><i class="fa fa-heart-o"></i> <span>Like</span></a></li>
                                        </ul>

                                        <ul class="latest_footerlink_right pull-right">
                                            <li><a href="#">43 <i class="fa fa-share-alt"></i></a></li>
                                            <li><a href="#">45 <i class="fa  fa-comment-o"></i> </a></li>
                                            <li><a href="#">23 <i class="fa  fa-heart-o"></i></a></li>
                                        </ul>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <div ng-if="current_publish.post == true">
                            <div class="col-md-12">
                                <div class="box latest_talk">
                                    <div class="box-body">   
                                        <div class="image default_image">
                                            <span ng-show="current_publish.image == ''" ng-bind="user_initial"></span>
                                            <img ng-show="current_publish.image != ''" ng-src="{{current_publish.path + current_publish.image}}" class="sub_img" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image">
                                        </div>
                                        <div class="latest_content">
                                            <div class="mCustomScrollbar">
                                                <div class="latest_header clearfix">
                                                    <h6 ng-bind="current_publish.buisname">Mike Johns <span>verified talker</span></h6>
                                                    <!--                                                    <div class="btn-group confidentiality pull-right">
                                                                                                            <button class="btn  btn-sm dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-chevron-down"></i></button>
                                                                                                            <ul class="dropdown-menu pull-right" role="menu">
                                                                                                                <li><a href="javascript:void(0)">Add new event</a></li>
                                                                                                                <li><a href="javascript:void(0)">Clear events</a></li>
                                                                                                                <li class="divider"></li>
                                                                                                                <li><a href="javascript:void(0)">View calendar</a></li>
                                                                                                            </ul>
                                                                                                        </div>-->
                                                </div>                                        
                                                <p ng-bind="current_publish.text">At his lab at the University of Pennsylvania, Vijay Kumar and his team develop a crazy AlgorithmWe are really looking forward to see it see live on <a href="javascript:void(0)"> www.liveteam.co.uk.</a> </p>                  
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer post_bottom clearfix">
                                        <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a href="javascript:void(0)" ng-click="open_comment_Animation(current_publish)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                <li><a href="javascript:void(0)" ng-class="{'like': current_publish.like}" ng-click="feedbackHub(current_publish)"><i ng-class="{'fa fa-heart' :current_publish.like, 'fa fa-heart-o' :!current_publish.like}" class="fa fa-heart-o"></i> Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)" ng-click="commentsAnimation(current_publish)"><span ng-bind="current_publish.comments_count">45 </span><i class="fa fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span ng-bind="current_publish.like_count">46</span> <i ng-class="{'fa fa-heart' :current_publish.like_count > 0, 'fa fa-heart-o' :current_publish.like_count == 0}" class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="comment_box" ng-show="current_publish.open_comment_box">
                                            <div class="comment_default"> 
                                                <span ng-show="current_publish.image == ''" ng-bind="user_initial"></span>
                                                <img class="comment_img" ng-show="current_publish.image != ''" ng-src="{{current_publish.path + current_publish.image}}" />
                                            </div>
                                            <input type="text" ng-model="current_publish.comment" class="form-control"  placeholder="Write your comment">
                                            <button class="btn btn_publish" ng-click="postComments(current_publish)">Post</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-11 col-md-offset-1" ng-show="current_publish.show_comments" ng-repeat="comment in current_publish.comments">
                                <div class="box latest_talk comment_reply">
                                    <div class="box-body">
                                        <div class="image default_image">
                                            <span ng-show="current_publish.image == ''" ng-bind="user_initial"></span>
                                            <img ng-show="comment.image != ''" ng-src="{{comment.image}}" src="images/post-userimg.jpg" alt="" class="sub_img">
                                        </div>
                                        <div class="latest_content">
                                            <div class="mCustomScrollbar">
                                                <div class="latest_header clearfix">
    <!--                                                <h6>Mike Johns <span>verified talker</span></h6>-->
                                                    <h6 ng-bind="comment.buisnesname">Mike Johns</h6>
                                                </div>
                                                <!--<span class="post_time">9 Min ago</span>-->  
                                                <p ng-bind="comment.comment_view">Thats a really nice job cant wait to see it </p>                  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12" ng-repeat="hub in hub_list" ng-show="show_default_hub">
                    <div ng-if="hub.type == 'hub'">
                        <div class="box latest_talk">
                            <div class="box-body">
                                <div class="image default_image">
                                    <span ng-show="hub.prof_img == ''" ng-bind="hub.profile_initional"></span>
                                    <img class="latest_talkimg" alt="" ng-show="hub.prof_img != ''" ng-src="{{hub.prof_img}}" />
                                </div>
                                <!--<img class="latest_talkimg" alt="" ng-src="{{hub.user_image != '' && '<?php echo base_url() ?>/uploads/profile_image/'+hub.user_image || +'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg" />-->

                                <div class="latest_content">

                                    <div class="latest_header clearfix">
                                    <h6><avi ng-bind="hub.bussinessname">Mike Johns</avi> <!--<span>author</span>--></h6>
                                        <!--                                        <div class="btn-group confidentiality pull-right">
                                                                                    <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                                                    <ul role="menu" class="dropdown-menu pull-right">
                                                                                        <li><a href="javascript:void(0)">Add new event</a></li>
                                                                                        <li><a href="javascript:void(0)">Clear events</a></li>
                                                                                        <li class="divider"></li>
                                                                                        <li><a href="javascript:void(0)">View calendar</a></li>
                                                                                    </ul>
                                                                                </div>-->
                                    </div>

                                </div>
                            </div>
                            <div class="post_innercont clearfix">
                                <div>
                                    <a href="javascript:void(0)" target="_balnk" ng-if="hub.link != null">
                                        <img class="post_img" alt="" ng-src="{{hub.base_url + hub.link}}" src="<?php echo base_url(); ?>assets_phase2/images/post-img.jpg">
                                    </a>
                                    <div ng-class="{'post_innertxt' : hub.image != ''}">
                                        <!--<div class="post_innertxt" ng-if="news.author ==''" class = "full-width">-->
                                        <span ng-if="hub.hub_title != ''">
                                            <a ng-href="javascript:void(0)" target="_balnk"><h5 ng-bind="hub.hub_title">The Future of Flying Robots For Suistanable Farming</h5></a></span>

                                        <p ng-bind-html="hub.content | limitTo: 300">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>
                                        <a href="javascript:void(0)" ng-show="hub.hub_title != null" ng-click="showHubDetails(hub, 'hub')">...</a>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer post_bottom clearfix">
                                <div class="comment_links clearfix">
                                    <ul class="latest_footerlink pull-left">
        <!--                                <li><a href="javascript:void(0)"><i class="fa fa-share-alt"></i> Share</a></li>-->
                                        <li><a href="javascript:void(0)" ng-click="open_comment_Animation(hub)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                        <li><a href="javascript:void(0)" ng-class="{'like': hub.like != 0}" ng-click="feedback(hub)"><i ng-class="{'fa fa-heart' :hub.like != 0, 'fa fa-heart-o' :hub.like == 0}" class="fa"></i> Like</a></li>
                                    </ul>
                                    <ul class="latest_footerlink_right pull-right">
                                        <!--<li><a href="javascript:void(0)">43 <i class="fa fa-share-alt"></i></a></li>-->
                                        <li><a href="javascript:void(0)" ng-click="commentsAnimation(hub)"><span ng-bind="hub.comment_count">45</span> <i class="fa  fa-comment-o"></i> </a></li>
                                        <li><a href="javascript:void(0)"><span ng-bind="hub.likes">23</span> <i class="fa  fa-heart-o"></i></a></li>
                                    </ul>
                                </div>
                                <div class="comment_box" ng-show="hub.open_comment_box">
                                    <div class="comment_default"> 
                                        <span ng-show="front_img_name == ''" ng-bind="user_initial"></span>
                                        <img class="comment_img" ng-show="front_img_name != ''" ng-src="{{path + front_img_name}}" />
                                    </div>
<!--                                    <img class="comment_img" ng-src="{{hub.prof_img != '' && hub.prof_img|| 'http://arlians.com/assets/images/profile-img.png'}}" />-->
                                    <input type="text" ng-model="hub.comment" class="form-control"  placeholder="Write your comment">
                                    <button class="btn btn_publish" ng-click="postHubComments(hub)">Post</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11 col-md-offset-1" ng-show="hub.show_comments" ng-repeat="comment in hub.comments">
                            <div class="box latest_talk comment_reply">
                                <div class="box-body">
                                    <div class="image default_image">
                                        <span ng-show="comment.image == null" ng-bind="comment.com_initial_name"></span>
                                        <img ng-show="comment.image != null" ng-src="{{comment.image_path + comment.image}}" src="images/post-userimg.jpg" alt="" class="sub_img">
                                    </div>
                                    <div class="latest_content">
                                        <div class="mCustomScrollbar">
                                            <div class="latest_header clearfix">
        <!--                                                <h6>Mike Johns <span>verified talker</span></h6>-->
                                                <h6 ng-bind="comment.buisnesname">Mike Johns</h6>
                                            </div>
                                            <!--<span class="post_time">9 Min ago</span>-->  
                                            <p ng-bind="comment.comment_view">Thats a really nice job cant wait to see it </p>                  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box latest_talk"  ng-if="hub.type == 'watson'">
                        <div class="box-body">
                            <!--                            <a href="javascript:void(0)" ng-href="hub.url">
                                                            <img class="latest_talkimg" alt="" ng-if="hub.image != '' || hub.image != undefined" ng-bind="hub.image" src="images/post-userimg.jpg">
                                                        </a>-->
                            <div class="latest_content">
                                <div class="mCustomScrollbar">
                                    <div class="latest_header clearfix">
                                        <h6 ng-if="hub.author != ''"><a href="javascript:void(0)" ng-href="hub.url"><span ng-bind="hub.author">Mike Johns</span></a></h6>
                                        <!--                                    <div class="btn-group confidentiality pull-right">
                                                                                <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                                                <ul role="menu" class="dropdown-menu pull-right">
                                                                                    <li><a href="#">Add new event</a></li>
                                                                                    <li><a href="#">Clear events</a></li>
                                                                                    <li class="divider"></li>
                                                                                    <li><a href="#">View calendar</a></li>
                                                                                </ul>
                                                                            </div>-->
                                    </div>
    <!--                                <span class="post_time">9 Min ago</span>  -->
    <!--                                <p ng-bind="hub.">Watch these adorable robots play follow the leader:</p>                  -->
                                </div>
                            </div>
                        </div>
                        <div class="post_innercont clearfix">
                            <a href="javascript:void(0)" target="_blank" ng-href="hub.url">
                                <img class="post_img" alt="" ng-show="hub.image != '' || hub.image != undefined" ng-src="{{hub.image}}" src="images/post-userimg.jpg">
                            </a>
                            <div class="post_innertxt">
                                <h5 ng-bind="hub.title">The Future of Flying Robots For Suistanable Farming</h5>
                                <p ng-bind-html="hub.text | limitTo: 300">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>
                                <a href="javascript:void(0)" ng-click="showHubDetails(hub, 'watson')">...</a>
                            </div>
                        </div>
                        <!--                        <div class="box-footer post_bottom clearfix">
                                                    <ul class="latest_footerlink pull-left">
                                                        <li><a href="#"><i class="fa fa-share-alt"></i> Share</a></li>
                                                        <li><a href="#"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                        <li><a href="#"><i class="fa fa-heart-o"></i> Like</a></li>
                                                    </ul>
                        
                                                    <ul class="latest_footerlink_right pull-right">
                                                        <li><a href="#">43 <i class="fa fa-share-alt"></i></a></li>
                                                        <li><a href="#">45 <i class="fa  fa-comment-o"></i> </a></li>
                                                        <li><a href="#">23 <i class="fa  fa-heart-o"></i></a></li>
                                                    </ul>
                                                </div> -->
                    </div>
                    <div class="box latest_talk"  ng-if="hub.type == 'bing'">
                        <div class="box-body">
<!--                            <img class="latest_talkimg" alt="" src="images/post-userimg.jpg">-->
                            <div class="latest_content">
                                <div class="mCustomScrollbar">
                                    <div class="latest_header clearfix">
                                        <h6><a href="javascript:void(0);" ng-href="hub.url"><span ng-bind="hub.source">Mike Johns</span></a> <span>data sourse</span></h6>
                                        <!--                                    <div class="btn-group confidentiality pull-right">
                                                                                <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                                                <ul role="menu" class="dropdown-menu pull-right">
                                                                                    <li><a href="#">Add new event</a></li>
                                                                                    <li><a href="#">Clear events</a></li>
                                                                                    <li class="divider"></li>
                                                                                    <li><a href="#">View calendar</a></li>
                                                                                </ul>
                                                                            </div>-->
                                    </div>
    <!--                                <span class="post_time">9 Min ago</span>  -->
    <!--                                <p>Watch these adorable robots play follow the leader:</p>                  -->
                                </div>
                            </div>
                        </div>
                        <div class="post_innercont clearfix">
<!--                            <img class="post_img" alt="" src="images/post-img.jpg">-->
                            <div class="post_innertxt">
                                <a target="_blank" ng-href="hub.url"><h5 ng-bind="hub.title">The Future of Flying Robots For Suistanable Farming</h5></a>
                                <p ng-bind-html="hub.desc | limitTo: 300">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>
                                <a href="javascript:void(0)" ng-click="showHubDetails(hub, 'bing')">...</a>
                            </div>
                        </div>
                        <!--                        <div class="box-footer post_bottom clearfix">
                                                    <ul class="latest_footerlink pull-left">
                                                        <li><a href="#"><i class="fa fa-share-alt"></i> Share</a></li>
                                                        <li><a href="#"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                        <li><a href="#"><i class="fa fa-heart-o"></i> Like</a></li>
                                                    </ul>
                        
                                                    <ul class="latest_footerlink_right pull-right">
                                                        <li><a href="#">43 <i class="fa fa-share-alt"></i></a></li>
                                                        <li><a href="#">45 <i class="fa  fa-comment-o"></i> </a></li>
                                                        <li><a href="#">23 <i class="fa  fa-heart-o"></i></a></li>
                                                    </ul>
                                                </div> -->
                    </div>
                </div>
                <!--<div class="col-md-11 col-md-offset-1">
                    <div class="box latest_talk comment_reply">
                        <div class="box-body">
                            <img src="<?php echo base_url(); ?>assets_phase2/images/post-userimg.jpg" alt="" class="latest_talkimg">
                            <div class="latest_content">
                                <div class="latest_header clearfix">
                                    <h6>Mike Johns <span>verified talker</span></h6>

                                </div>
                                <span class="post_time">9 Min ago</span>  
                                <p>Thats a really nice job cant wait to see it </p>                  
                            </div>
                        </div>

                        <div class="box-footer post_bottom clearfix">
                            <ul class="latest_footerlink pull-left">
                                <li><a href="javascript:void(0)"><i class="fa fa-share-alt"></i> <span>Share</span></a></li>
                                <li><a href="javascript:void(0)"><i class="fa  fa-comment-o"></i> <span>Comments</span></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-heart-o"></i> <span>Like</span></a></li>
                            </ul>

                            <ul class="latest_footerlink_right pull-right">
                                <li><a href="javascript:void(0)">43 <i class="fa fa-share-alt"></i></a></li>
                                <li><a href="javascript:void(0)">45 <i class="fa  fa-comment-o"></i> </a></li>
                                <li><a href="javascript:void(0)">23 <i class="fa  fa-heart-o"></i></a></li>
                            </ul>
                        </div> 
                    </div>
                </div>-->
                <div class="publish_second" ng-show="show_publish_details">
                    <div class="box publish_section clearfix" ng-if = "hub_det_type == 'current_post'">
                        <div class="publish_toppart">
                            <a target="_blank" ng-href="hub.url"><img ng-show="show_hub_det.image_post != ''" ng-src="{{show_hub_det.image_post}}" src="images/publish_img.jpg" alt=""></a>
                            <div class="publish_namecontainer">
                                <div class="">
                                    <span ng-show="show_hub_det.image == ''" ng-bind="show_hub_det.user_initial"></span>
                                    <img ng-show="show_hub_det.image != ''" ng-src="{{show_hub_det.path + show_hub_det.image}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image">
                                </div>
    <!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                                <div class="pulish_heading">
                                    <span class="publis_name" ng-bind="show_hub_det.buisname">Digitalweb</span>
    <!--                                <p class="publish_subtxt">Production of modern website</p>-->
                                </div>
                            </div>
                        </div>                        
                        <a target="_blank" ng-href="hub.url"><h5 ng-bind="show_hub_det.hub_title">Write your headline</h5></a>
                        <div class="publish_editor_txt box-boby" ng-bind-html="show_hub_det.html_text">
                        </div>
                        <button class="btn btn_publish pull-right" ng-click="backToHub()">Back</button>
                    </div>
                    <div class="box publish_section clearfix" ng-if = "hub_det_type == 'hub'">
                        <div class="publish_toppart">

                            <img ng-show="show_hub_det.link != ''" ng-src="{{show_hub_det.base_url + show_hub_det.link}}" src="images/publish_img.jpg" alt="">
                            <div class="publish_namecontainer">
                                <div>
                                    <span ng-show="show_hub_det.prof_img == ''" ng-bind="show_hub_det.user_initial"></span>                                   
                                    <img ng-show="show_hub_det.prof_img != ''" ng-src="{{show_hub_det.prof_img}}" class="publish_avator" alt="User Image">
                                </div>
    <!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                                <div class="pulish_heading">
                                    <span class="publis_name" ng-bind="show_hub_det.bussinessname">Digitalweb</span>
    <!--                                <p class="publish_subtxt">Production of modern website</p>-->
                                </div>
                            </div>
                        </div>
                        <h5 ng-bind="show_hub_det.hub_title">Write your headline</h5>
                        <div class="publish_editor_txt  box-body" ng-bind-html="show_hub_det.content">
                        </div>
                        <button class="btn btn_publish pull-right" ng-click="backToHub()">Back</button>
                    </div>
                    <div class="box publish_section clearfix" ng-if = "hub_det_type == 'watson'">
                        <div class="publish_toppart">
                            <a href="javascript:void(0)" target="_blank" ng-href="show_hub_det.url">
                                <img ng-show="hub_det_type.image != '' || hub_det_type.image != undefined" ng-src="{{hub_det_type.image}}" src="images/publish_img.jpg" alt="">
                            </a>
                        </div>
                        <h5 ng-bind="show_hub_det.title">Write your headline</h5>
                        <span ng-bind="show_hub_det.author"></span>
                        <div class="publish_editor_txt" ng-bind-html="show_hub_det.text">
                        </div>
                        <button class="btn btn_publish pull-right" ng-click="backToHub()">Back</button>
                    </div>
                    <div class="box publish_section clearfix" ng-if = "hub_det_type == 'bing'">
                        <span ng-bind="show_hub_det.source"></span>
                        <h5 ng-bind="show_hub_det.title">Write your headline</h5>
                        <div class="publish_editor_txt" ng-bind-html="show_hub_det.desc">

                        </div>
                        <button class="btn btn_publish pull-right" ng-click="backToHub()">Back</button>
                    </div>
                </div>
                <div class="box publish_section clearfix" ng-show="show_publish">
                    <div class="publish_toppart">
                        <!--                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                          Launch demo modal
                        </button>-->

                        <!-- Modal Crop Image-->
                        <div class="modal fade" id="crop_publish_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Upload an Image</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-crop">
                                            <div class="imageBox">
                                                <div class="thumbBox"></div>
                                                <div class="spinner" style="display: none">Loading...</div>
                                            </div>
                                            <div class="action">

<!--<input type="button" id="uploadImg" style="float: right" value="Upload"/>
<input type="button" id="btnCrop" value="Upload" style="float: right">
<input type="button" id="btnZoomIn" value="+" style="float: right">
<input type="button" id="btnZoomOut" value="-" style="float: right">-->
                                            </div>
                                            <div class="cropped"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="note">image size 450 x 250  for best result</span>
                                        <input type="file" id="file">                                               
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                        <button type="button" id="btnZoomIn" class="btn btn-primary">+</button>
                                        <button type="button" id="btnZoomOut" class="btn btn-primary">-</button>
                                        <button type="button" id="btnCrop" class="btn btn-primary">Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        <input type="file" id="upload_img_for_post" onchange="readURLPostImage(this);"/>-->
                        <i class="fa fa-camera change_photos" style="cursor:pointer" onclick="open_publish_crop_img_modal()"></i>
<!--                        <input type="file" ng-model="image" ng-change="readURLPostImage(this)"><i class="fa fa-camera change_photos"></i></input>-->

                        <div class="publish_namecontainer" style="display:none">
                            <img ng-src="{{front_img_name != '' && path + front_img_name|| 'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image">
<!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                            <div class="pulish_heading">
                                <span class="publis_name" ng-bind="bussinessname">Digitalweb</span>
                                <p class="publish_subtxt">Production of modern website</p>
                            </div>
                        </div>
                        <!--                        <div></div>-->
                        <img id="hub_publish_default_img" src="<?php echo base_url(); ?>assets/images/publish_img.jpg" alt="">
                        <img id="hub_publish_preview_img" class="hub_publish_preview_img" src="" alt="">
                    </div>



                    <input type="text" ng-model="publish_header" class="write_headline" placeholder="Write your headline" />

                    <!--                    <h5>Write your headline</h5>-->

                    <div class="publish_editor">
                        <textarea id="txtEditor"></textarea> 
                    </div>
<!--                    <div class="post-header add_tags"><i class="fa fa-tag"></i>Add tags</div>   
                    <div class="bs-example">
                        <input type="text" value="tag name" data-role="tagsinput" />
                    </div>-->
                    <!--<div class="clearfix">
                        <span style="color:#345672" ng-bind="show_post_selected">Hello world</span>
                        <div class="btn-group post_footerdrpbtn">
                            <i class="fa fa-gears pull-left"></i>
                            <button aria-expanded="false" data-toggle="dropdown" class="btn  btn-sm dropdown-toggle">Share with</button>
                            <ul role="menu" class="dropdown-menu pull-right">                                        
                                <li><a href="javascript:void(0);" ng-click="select_post(list)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                
                            </ul>
                        </div>
                    </div>-->

                    <div class="editor_btn">
                        <button class="btn btn_publish pull-right" ng-click="showDefault()">Back</button>
                        <button class="btn btn_publish pull-right" ng-click="savePublish()">Publish</button>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 tab-sticky-scroll">
            <img  src="<?php echo base_url(); ?>assets_phase2/images/tab-img.jpg" alt="">
            <!--            <div class="details_tab">
                            <div id="parentHorizontalTab">
                                <ul class="resp-tabs-list hor_1 clearfix">
                                    <li><i class="fa  fa-refresh"></i><br>Latest Talks</li>
                                    <li><i class="fa  fa-cloud-download"></i><br>My Talks</li>
                                    <li><i class="fa  fa-th"></i><br>Top Talks</li>
                                    <li><i class="fa  fa-users"></i><br>Start a Talk</li>
                                    <li><i class="fa  fa-list"></i><br>More</li>
                                </ul>
                                <div class="resp-tabs-container hor_1">
                                    <div>
                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image">
                                                    <img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" alt="" class="latest_talkimg img-circle">
                                                    <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6>Mike Johns <span>verified talker</span></h6>
                                                        <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                    </div>
                                                    <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)">Participate</a></li>
                                                    <li><a href="javascript:void(0)">Like </a></li>
                                                    <li><a href="javascript:void(0)">Recommend</a></li>
                                                </ul>
            
                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                                </ul>
                                            </div> 
                                        </div>
            
                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image">
                                                    <img src="<?php echo base_url(); ?>assets_phase2/images/user7-128x128.jpg" alt="" class="latest_talkimg img-circle">
                                                    <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6>Mike Johns <span>verified talker</span></h6>
                                                        <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                    </div>
                                                    <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)">Participate</a></li>
                                                    <li><a href="javascript:void(0)">Like </a></li>
                                                    <li><a href="javascript:void(0)">Recommend</a></li>
                                                </ul>
            
                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                                </ul>
                                            </div> 
                                        </div>
            
                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image">
                                                    <img src="<?php echo base_url(); ?>assets_phase2/images/user5-128x128.jpg" alt="" class="latest_talkimg img-circle">
                                                    <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6>Mike Johns <span>verified talker</span></h6>
                                                        <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                    </div>
                                                    <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)">Participate</a></li>
                                                    <li><a href="javascript:void(0)">Like </a></li>
                                                    <li><a href="javascript:void(0)">Recommend</a></li>
                                                </ul>
            
                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                                </ul>
                                            </div> 
                                        </div>
            
                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image">
                                                    <img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" alt="" class="latest_talkimg img-circle">
                                                    <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6>Mike Johns <span>verified talker</span></h6>
                                                        <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                    </div>
                                                    <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)">Participate</a></li>
                                                    <li><a href="javascript:void(0)">Like </a></li>
                                                    <li><a href="javascript:void(0)">Recommend</a></li>
                                                </ul>
            
                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                                </ul>
                                            </div> 
                                        </div>
                                    </div>
                                </div>                  
                                <div class="resp-tabs-container hor_1">
                                    <div>
                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image">
                                                    <img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" alt="" class="latest_talkimg img-circle">
                                                    <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6>Mike Johns <span>verified talker</span></h6>
                                                        <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                    </div>
                                                    <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)">Participate</a></li>
                                                    <li><a href="javascript:void(0)">Like </a></li>
                                                    <li><a href="javascript:void(0)">Recommend</a></li>
                                                </ul>
            
                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                                </ul>
                                            </div> 
                                        </div>
                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image">
                                                    <img src="<?php echo base_url(); ?>assets_phase2/images/user7-128x128.jpg" alt="" class="latest_talkimg img-circle">
                                                    <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6>Mike Johns <span>verified talker</span></h6>
                                                        <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                    </div>
                                                    <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)">Participate</a></li>
                                                    <li><a href="javascript:void(0)">Like </a></li>
                                                    <li><a href="javascript:void(0)">Recommend</a></li>
                                                </ul>
                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                                </ul>
                                            </div> 
                                        </div>
                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image">
                                                    <img src="<?php echo base_url(); ?>assets_phase2/images/user5-128x128.jpg" alt="" class="latest_talkimg img-circle">
                                                    <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6>Mike Johns <span>verified talker</span></h6>
                                                        <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                    </div>
                                                    <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)">Participate</a></li>
                                                    <li><a href="javascript:void(0)">Like </a></li>
                                                    <li><a href="javascript:void(0)">Recommend</a></li>
                                                </ul>
            
                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                                </ul>
                                            </div> 
                                        </div>
                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image">
                                                    <img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" alt="" class="latest_talkimg img-circle">
                                                    <a class="online-status" href="javascript:void(0)"><i class="fa fa-circle text-success"></i></a>
                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6>Mike Johns <span>verified talker</span></h6>
                                                        <a href="javascript:void(0)" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                    </div>
                                                    <p><strong>How food compagnies can enter this new competitive market?</strong></p> 
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus, odio vitae gravida mattis, nunc est viverra turpis, ac turpis duis.</p>                     
                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)">Participate</a></li>
                                                    <li><a href="javascript:void(0)">Like </a></li>
                                                    <li><a href="javascript:void(0)">Recommend</a></li>
                                                </ul>
                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)">43 <i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)">45 <i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li><a href="javascript:void(0)"><i class="fa  fa-gear"></i></a></li>
                                                </ul>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>-->
        </div>
    </div>
    <div class="col-md-3 col-sm-12 left-block" ng-controller="DashboardSuggestionCtrl as suggestion">
        <div class="row">   

            <div class="col-sm-6 col-md-12" ng-show="search_module">
                <div class="view_result_box fixd_box1">
                    <div class="view_box_top"><h4>See All Matching <!--<small><i class="fa fa-search"></i></small>--></h4></div>
                    <div class="view_box_body">
                        <div class="match_result">
                            <span class="mathc_txt">MATCHING <br> RESULTS</span><br>
                            <span class="mathc_resulttxt" ng-bind="total_suggestion_count">132</span>
                        </div>
                        <div class="result_bottom clearfix">
                            <div id="result-circle" class="owl-carousel">
                                <div class="item" ng-repeat="list in detail_suggestion_list">
                                    <div class="result_numb">
                                        <div class="result_numbin" ng-bind="list.type_count">69</div>
                                    </div>
                                    <span class="result_name" ><a ng-bind="list.type" href="javascript:void(0)" ng-click="getDetailSuggestion(list)">Customers</a></span>
                                </div>                                 
                            </div>
                        </div>
                    </div>
                    <div class="view_box_bottom">
                        <a href="javascript:void(0);" class="view_menu" ng-click="openWizardTwoModal()"><i class="fa fa-list"></i></a>
<!--                        <a href="javascript:void(0);" class="view_menu" data-toggle="modal" data-target="#myModal"><i class="fa fa-list"></i></a>-->
<!--                        <p class="match_text">Improve your match</p>-->
                        <!-- Modal -->
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_wizard_notice">Let's set up your business matching engine</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_industry_list">Who are your customers?</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_target_sector">in the field of…..</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_looking_for">Finally, tell us what you are looking for.</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_locality">Your target locality</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_services">Do you service</h4>
                                        <h4 class="modal-title" id="myModalLabel" ng-show="show_target_list">and tell us about the people …..</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="step3" ng-show="show_wizard_notice">
                                            <div class="art_culture"> 
                                                <h3>Let’s set up your business matching engine</h3>
                                                <p>We will ask you 4 questions and it should take less than a minute. Don't worry though, we won't ask any thing sensitive and your answer won't be visible to other members</p>
                                            </div>
                                        </div>
                                        <div class="mCustomScrollbar" ng-show="show_industry_list">
                                            <div class="section__content clearfix">
                                                <div class="card effect__click industry_list" onclick="animationCard(this, '.industry_list')" ng-click="setIndustryCode(industry)" ng-repeat="industry in industry_list_for_wizard_two">
                                                    <div class="card__front">
                                                        <div class="card_img">
                                                            <img src="<?php echo base_url() ?>uploads/profile_image/{{industry.profile_image}}" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="industry.industry_name">Art &amp; Culture</span>
                                                    </div>
                                                    <div class="card__back">
                                                        <div class="card_img">
                                                            <i class="fa fa-close close_card"></i>
                                                            <div class="selected">Selected</div>
                                                            <img src="<?php echo base_url() ?>uploads/profile_image/{{industry.profile_image}}" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="industry.industry_name">Art &amp; Culture</span>
                                                    </div>
                                                </div>
                                                <div class="card effect__click industry_list wiz_2_hid_card"></div>
                                                <div class="card effect__click industry_list" onclick="animationCard(this, '.industry_list')">
                                                    <div class="card__front">
                                                        <div class="card_img">
                                                            <img src="<?php echo base_url() ?>uploads/profile_image/consumer.jpg" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="industry.industry_name">Art &amp; Culture</span>
                                                    </div>
                                                    <div class="card__back">
                                                        <div class="card_img">
                                                            <i class="fa fa-close close_card"></i>
                                                            <div class="selected">Selected</div>
                                                            <img src="<?php echo base_url() ?>uploads/profile_image/consumer.jpg" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="industry.industry_name">Art &amp; Culture</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="step3" class="mCustomScrollbar" ng-show="show_target_sector">
                                            <div class="section__content clearfix">
                                                <div class="card effect__click sub_sector" onclick="animationCard(this, '.sub_sector')" ng-click="setSubSectorCode(sub_sector)" ng-repeat="sub_sector in sub_sector_list">

                                                    <div class="card__front">
                                                        <div class="card_img">
                                                            <img src="<?php echo base_url() ?>uploads/profile_image/{{sub_sector.profile_image}}" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="sub_sector.tar_sector_name">Art &amp; Culture</span>
                                                    </div>
                                                    <div class="card__back">
                                                        <div class="card_img">
                                                            <i class="fa fa-close close_card"></i>
                                                            <div class="selected">Selected</div>
                                                            <img src="<?php echo base_url() ?>uploads/profile_image/{{sub_sector.profile_image}}" alt="">
                                                        </div>
                                                        <span class="card__text" ng-bind="sub_sector.tar_sector_name">Art &amp; Culture</span>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>


                                        <div class="step3" ng-show="show_looking_for">
                                            <div class="art_culture">                                              
                                                <ul class="subtag_select">
                                                    <li onclick="animatedSubSector(this)" ng-repeat="look in looking_for_list" ng-click="setLookingFor(look)" ng-bind="look.looking_for_name">Tag name sub</li>                                                        
                                                </ul>
                                            </div>
                                        </div>
                                        <!--                                        <div class="step3" ng-show="show_locality">
                                                                                    <div class="art_culture">                                              
                                                                                        <ul class="subtag_select">
                                                                                            <li onclick="animatedSubSector(this)" ng-repeat="locality in locality_list" ng-click="setLocality(locality)" ng-bind="locality.locality_type">Tag name sub</li>                                                        
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>-->
                                        <div class="step3" ng-show="show_services">
                                            <div class="art_culture">                                              
                                                <ul class="subtag_select">
                                                    <li onclick="animatedSubSector(this)" ng-repeat="service in services_list" ng-click="setService(service)" ng-bind="service.service_name">Tag name sub</li>                                                        
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="step3" ng-show="show_target_list">
                                            <div class="art_culture">                                              
                                                <ul class="subtag_select">
                                                    <li onclick="animatedSubSector(this)" ng-repeat="target in target_sector_list" ng-click="setTarget(target)" ng-bind="target.look_service_name">Tag name sub</li>                                                        
                                                </ul>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="modal-footer" ng-show="show_wizard_notice">
                                        <p class="wiz_2_note">We know that some people work in more than one industry. We are working on adding in this functionality at the moment and you will be able to add more shortly.</p>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="openWizardTwoForNewUser()">Next</button>
                                    </div>
                                    <div class="modal-footer" ng-show="show_industry_list">
                                        <p class="wiz_2_note">We know that some people work in more than one industry. We are working on adding in this functionality at the moment and you will be able to add more shortly.</p>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="getTargetSectorByIndustry()">Next</button>
                                    </div>
                                    <div class="modal-footer" ng-show="show_target_sector">
                                        <p class="wiz_2_note">We know that some people work in more than one industry. We are working on adding in this functionality at the moment and you will be able to add more shortly.</p>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('one')">Back</button>
                                        <button type="button" class="btn btn-primary" ng-click="checkBusinessCodeExitForUser()">Next</button>
                                    </div>

                                    <div class="modal-footer" ng-show="show_target_list">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('five')">Back</button>
                                        <!--                                        <button type="button" class="btn btn-primary" ng-click="saveDataForWizard()">Next</button>-->
                                        <button type="button" class="btn btn-primary" ng-click="fetchServices()">Next</button>
                                    </div>


                                    <!--                                    <div class="modal-footer" ng-show="show_locality">
                                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('three')">Back</button>
                                                                            <button type="button" class="btn btn-primary" ng-click="fetchServices()">Next</button>
                                                                        </div>-->
                                    <div class="modal-footer" ng-show="show_services">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('three')">Back</button>
                                        <button type="button" class="btn btn-primary" ng-click="checkServices()">Next</button>
                                    </div>
                                    <div class="modal-footer" ng-show="show_looking_for">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" ng-click="setWizardBackTo('two')">Back</button>
                                        <!--                                        <button type="button" class="btn btn-primary" ng-click="fetchServices()">Next</button>-->
                                        <button type="button" class="btn btn-primary" ng-click="saveDataForWizard()">Next</button>
                                        <!--                                        <button type="button" class="btn btn-primary" ng-click="fetchLocalityList()">Next</button>-->
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!--                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">What sort of businesses do you service?</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                </div> 
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                <button type="button" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                        </div>
                        
                                                    </div>
                                                </div>-->
                    </div>                      


                </div>
            </div>

            <div class="col-sm-6 col-md-12"  ng-show="show_detail" id="detail_suggestion">
                <div class="view_result_box fixd_box1" ng-repeat="detail in details_suggestion">
                    <div class="view_box_top1 matching clearfix"><h4 ng-bind="search_for_suggestion">Customers</h4>
                        <div class="result_numb1">
                            <div class="result_numbin1" ng-bind="total_count">69</div>
                        </div>
                    </div>
                    <div>
                        <div class="view_box_body1">
                            <div class="matchign_cont">
                                <a href="javascript:void(0)" class="matchig_logo">
                                    <span  ng-show="detail.image == ''" ng-bind="detail.logobussinessname"></span>
                                    <img ng-show="detail.image != ''" ng-src="{{detail.image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+detail.image:'<?php echo base_url(); ?>assets/images/inbox-img.png'}}" alt="" >                                    
                                </a>
                                <h5 ng-bind="detail.bussinessname">Company Name</h5>
                                <p class="match_detail" ng-bind="detail.desc">Excel at enterprise, mobile and web development with Java, Scala, Groovy and Kotlin, with all the latest modern technologies and frameworks available out of the box. </p>
                            </div>
                            <img src="<?php echo base_url() ?>assets_phase2/images/match_enginebg.jpg" alt="">
                        </div>
                        <div class="view_box_body1 text-center clearfix">
                            <a href="javascript:void(0);" class="result_numb3" ng-click="individual_suggestion_user_feedback(detail, 0)">
                                <div class="result_numbin3"><i class="fa fa-minus"></i></div>
                            </a>
                            <a href="javascript:void(0);" class="result_numb4" ng-click="individual_suggestion_user_feedback(detail, 1)">
                                <div class="result_numbin4"><i class="fa fa-plus"></i></div>
                            </a>
                            <div class="clearfix match_score">
                                <div class="result_numb2">
                                    <div class="result_numbin2" ng-bind="detail.score">69</div>
                                </div>
                                <span class="score_txt" ng-bind="detail.matched_text">Match Score</span>
                            </div>
                        </div>
                    </div>
                    <div class="view_box_bottom">
                        <span class="backto_result" >
                            <a href="javascript:void(0);" ng-click="backToSuggestionList()">Back to matching results</a>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-12">
                <img  src="<?php echo base_url(); ?>assets_phase2/images/p-d.jpg" alt="">
                <!--                <div class="view_result_box portfolio">
                                    <div class="view_box_top"><h4>Portfolio Documents</h4></div>
                                    <div class="view_box_body clearfix">
                
                                        <div class="portfolio_box white_box">
                                            <i class="fa fa-pencil"></i>
                                        </div>
                
                                        <div class="portfolio_box normal_box">
                                            <span class="portbox_txt">Notes</span><br>
                                            <span class="port_inncircle">12</span>  
                                        </div>
                
                                        <div class="portfolio_box  normal_box ">
                                            <span class="portbox_txt">Documents</span><br>
                                            <span class="port_inncircle">12</span> 
                
                                        </div>
                
                                        <div class="portfolio_box white_box ">
                                            <i class="fa  fa-folder"></i>    
                                        </div>
                
                                        <div class="portfolio_box white_box">
                                            <i class="fa fa-comment"></i>
                                        </div>
                
                                        <div class="portfolio_box normal_box">
                                            <span class="portbox_txt">Talks</span><br>
                                            <span class="port_inncircle">12</span>  
                                        </div>
                
                                    </div>
                                    <div class="view_box_bottom">
                                        <a href="javascript:void(0)" class="view_menu"><i class="fa fa-list"></i></a>
                                    </div>
                                </div>-->
            </div>
        </div>

    </div>

</div>            
</div>

<!-- common-helper JS -->
<script src="<?php echo base_url(); ?>assets_phase2/js/dashboard_page.js"></script>
<script>
                                        $(function(){
                                        $('#hub_publish_default_img').show();
                                                $('#hub_publish_preview_img').hide();
                                                CKEDITOR.replace('txtEditor');
                                                $('.left-block, .tab-sticky-scroll')
                                                .theiaStickySidebar({
                                                additionalMarginTop: 50
                                                });
                                        });</script>
<script>
            $(document).ready(function() {

    });
            (function($){
            $(window).load(function(){

            $("#content-1").mCustomScrollbar({
            axis:"yx", //set both axis scrollbars
                    advanced:{autoExpandHorizontalScroll:true}, //auto-expand content to accommodate floated elements
                    // change mouse-wheel axis on-the-fly 
                    callbacks:{
                    onOverflowY:function(){
                    var opt = $(this).data("mCS").opt;
                            if (opt.mouseWheel.axis !== "y") opt.mouseWheel.axis = "y";
                    },
                            onOverflowX:function(){
                            var opt = $(this).data("mCS").opt;
                                    if (opt.mouseWheel.axis !== "x") opt.mouseWheel.axis = "x";
                            },
                    }
            });
            });
            })(jQuery);
</script>