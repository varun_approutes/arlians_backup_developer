<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
         
		 
        <title>Arlians</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="icon" href="<?php echo base_url(); ?>assets_phase2/images/favicon.ico">     
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/fonts/fonts.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/font-awesome.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/AdminLTE.css" type="text/css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/skins.css">
        <!-- Owl Carousel Assets -->
        <!-- CSS -->
        <link href="<?php echo base_url(); ?>assets_phase2/css/alertify.css" rel="stylesheet"> 
        <!-- Default theme -->
        <link href="<?php echo base_url(); ?>assets_phase2/css/default.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/css/owl.theme.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/css/responsive.css" rel="stylesheet">
        <!-- Loader Assets -->
<!--        <link href="<?php echo base_url(); ?>assets_phase2/css/loader.css" rel="stylesheet">-->
        <!-- Text Editor -->
        <link href="<?php echo base_url(); ?>assets_phase2/css/editor.css" type="text/css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets_phase2/css/jquery.mCustomScrollbar.min.css" type="text/css" rel="stylesheet"/>
        <!-- Tag Editor -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/bootstrap-tagsinput.css">    
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/textAngular.css">
        <link href="<?php echo base_url(); ?>assets_phase2/css/bootstrap-slider.css" rel="stylesheet"> 
        <link href="<?php echo base_url(); ?>assets_phase2/css/mobile_menu.css" rel="stylesheet">  

        <!-- 05-04-16 -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_phase2/css/fadeanimation.css"> 
        <!-- datetime  -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/pickmeup.css" type="text/css" />
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/bootstrap-toggle.css" type="text/css" /> -->

<?php  if(strpos($_SERVER['HTTP_HOST'],'www')===false)
    {
        $config['base_url'] = 'http://arlians.com/developer/';
    }
    else
    {
        $config['base_url'] = 'http://www.arlians.com/developer/';
    }
?>
<script>
    //alert('is login ? :'+is_login+' user id : '+user_id+' user_type : '+user_type);
    var CORE_URL = '<?php echo $config['base_url']?>';
</script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!--nilanjan add for toggle edit start -->
        <!-- <link href="<?php echo base_url(); ?>assets_phase2/edittogglenrc/css/font-awesome.min.css" rel="stylesheet" /> -->
		<!--<link rel="stylesheet" href="<?php echo base_url(); ?>assets_phase2/css/style.css" />-->
		  <!--nilanjan add for toggle edit end -->
		<!--<script type="text/javascript" src="<?php echo base_url(); ?>assets_phase2/js/site_<?php //require('combine.php'); ?>.js"></script>-->
        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/jQuery-2.1.4.min.js"></script>
       
        <script src="<?php echo base_url(); ?>assets_phase2/js/angular.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/angular-sanitize.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/textAngular.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/textAngular-rangy.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/textAngular-sanitize.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/textAngularSetup.js"></script>

        
        <script src="<?php echo base_url(); ?>assets_phase2/js/UserProxy.js"></script>
       
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/flip.js"></script>
       
        <script src="<?php echo base_url(); ?>assets_phase2/js/common-helper.js"></script>
       
        <script src="<?php echo base_url(); ?>assets_phase2/js/theia-sticky-sidebar.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/alertify.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/theia-sticky-sidebar.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/angular-toggle-switch.min.js"></script>
        
       

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/cropbox.js"></script> 

       <!--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.2.1/angular-animate.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/readmore.min.js"></script> -->        
        <!-- Dashboard JS -->

<!--        <script type="text/javascript" src="<?php echo base_url(); ?>assets_phase2/js/jquery.pickmeup.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_phase2/js/demo.js"></script>    
        <script type="text/javascript" src="<?php echo base_url(); ?>assets_phase2/js/community.js"></script> -->

    </head>
    <body class="hold-transition skin-blue sidebar-mini sidebar-collapse"  ng-app="arliansApp" ng-controller="DashboardCtrl as dashboard">
        <div class="wrapper">
            <div >
                <header class="main-header">
                    <!-- Logo -->
                    <a href="<?php echo base_url(); ?>member/dashboard" class="logo">
                        <img src="<?php echo base_url(); ?>assets_phase2/images/logo.png" alt="logo">
                    </a>
                    <!-- Header Navbar: style can be found in header.less -->
                    <nav class="navbar " role="navigation">
                        <!-- Sidebar toggle button-->
                        <a href="javascript:void(0)" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                            <span class="sr-only">Toggle navigation</span>
                        </a>
                        <div id="dl-menu5" class="dl-menuwrapper">
                            <button class="dl-trigger" id="mobmenu">
                                <span></span>
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>

                            <ul class="dl-menu" id="menuul">
                                <li class="header"></li>
                                <li class="active treeview">
                                    <a href="javascript:void(0)" >
                                        <i class="fa fa-th-list"></i> 
                                        <span ng-click="activeHub();">The Hub</span>
                                    </a>

                                </li>
                                <li class="treeview">
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-align-justify"></i>
                                        <span ng-click="activeProfile();">Profile</span>                
                                    </a>

                                </li>
                                
                                <li class="treeview">
                                    <!-- <a href="<?php echo base_url(); ?>member/dashboard"> -->
                                    <a href="javascript:void(0)">
                                        <i class="fa fa-users"></i>
                                        <span ng-click="activeMatch();">Matching</span>

                                    </a>              
                                </li>
                                <li class="treeview">
                                    <a href="javascript:void(0)" ng-click="activeProf();">
                                        <i class="fa  fa-comments"></i>
                                        <span>Community</span>

                                    </a>

                                </li>
                                <li class="treeview">
                                    <a href="javascript:void(0)" ng-click="activeSettings();">
                                        <i class="fa fa-gears"></i>
                                        <span>Settings</span>

                                    </a>

                                </li>
                                <!-- <li class="treeview">
                                    <a href="javascript:void(0)" onclick="return alertify.alert('Section is under construction')">
                                        <i class="fa fa-eye"></i> <span>Profile Views</span>
    
                                    </a>
    
                                </li> 
                                <li class="treeview">
                                    <a href="javascript:void(0)" onclick="return alertify.alert('Section is under construction')">
                                        <i class="fa  fa-gears"></i> <span>Settings</span>
    
                                    </a>
    
                                </li>
                                <li>
                                    <a href="javascript:void(0)" onclick="return alertify.alert('Section is under construction')">
                                        <i class="fa fa-credit-card"></i> <span>Wallet</span>
    
                                    </a>
                                </li>-->

                            </ul>
                        </div>
                        <!-- search form -->
                        <!--                        <form action="javascript:void(0)" method="get" class="sidebar-form top_search">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
                                                        </span>
                                                        <input type="text" name="q" class="form-control" placeholder="Search...">
                        
                                                    </div>
                                                </form>-->
                        <!-- /.search form -->
                        <!-- Navbar Right Menu -->
                        <div class="navbar-custom-menu">
                            <ul class="nav navbar-nav">
							    <li class="dropdown messages-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-comments"></i>
                                        <span class="label" ng-show="show_talk_notification_count > 0" ng-bind="show_talk_notification_count"></span> 
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">You have <span ng-bind="show_talk_notification_count"></span> unread talk request</li>
                                        <li>
                                            <!-- inner menu: contains the actual data -->
                                            <ul class="menu" ng-show="show_talk_notification.length > 0">
                                                <li ng-repeat="talknotification in show_talk_notification" id="{{talknotification.talk_access_id}}" >
                                                <!-- start message -->
                                                   <a href="javascript:void(0)">
                                                       <div class="image default_image">
                                                        <!-- <img alt="User Image" class="img-circle" src="dist/img/user2-160x160.jpg"> -->


                                                        <span ng-show="talknotification.logomembername_url=='NULL'" ng-bind="talknotification.bussiness_name_initinal"> </span>
                                                        <img ng-show="talknotification.logomembername_url!='NULL'" ng-src="{{talknotification.logomembername_url}}" src="" class="latest_talkimg" alt="User Image">

                                                      </div>

                                                       <h4 id="b_name"> <span ng-bind="talknotification.bussiness_name"> </span></h4>
													   <h4><small>wants to participate in the discussion:"{{talknotification.talk_title}}"</small></h4>
													   
                                                    <div class="menu_actionbtn">   
														<button type="button" class="btn btn-primary btn-sm"  ng-click="talkRequestAccepted(talknotification)">Accept</button>
														<button type="button" class="btn btn-default btn-sm"  ng-click="talkRequestDecline(talknotification)">Decline</button>
													</div>
                                                   </a>
                                                 <!-- end message -->   
                                                </li>

                                            </ul>
                                        </li>
                                      
                                    </ul>
                                </li>
                                
                                <li class="dropdown messages-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-users"></i>
                                        <span class="label" ng-show="connection_request_count > 0" ng-bind="connection_request_count"></span> 
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">You have <span ng-bind="connection_request_count"></span> request</li>
                                        <li>
                                            <!-- inner menu: contains the actual data -->
                                            <ul class="menu" ng-show="connection_request_list.length > 0">
                                                <li ng-repeat="request in connection_request_list" ng-hide="request.bussinessname=='' && request.tag_line==''"><!-- start message -->
                                                    <a href="javascript:void(0)">
                                                       <!-- <div class="pull-left">
                                                            <a href="<?php echo base_url() ?>memberprofile/profile/{{request.bussinessname + '/' + request.mid}}">-->
                                                                <div class="image default_image">
                                                                    <span ng-show="request.image_url==null" ng-bind="request.bussinessname | capitalize:true"> </span>
                                                                    <img ng-show="request.image_url!=null" ng-src="{{'<?php echo base_url(); ?>uploads/profile_image/'+request.image_url}}" src="" class="img-circle" alt="User Image">
                                                                </div>
                                                           <!-- </a>                                                            
                                                        </div>-->
                                                        <h4>
                                                            <span ng-bind="request.bussinessname">Support Team </span>
                                                        </h4>
                                                        <p ng-bind="request.tag_line !=null ?request.tag_line:'N / A'">Buisnesstype</p>                                                        
                                                        <!--<div class="btn-group btn-group-xs cust-float-right" role="group" aria-label="Extra-small button group" ng-show="request.is_accepted == '0'">-->
                                                        <div class="menu_actionbtn">
                                                            <button type="button" class="btn btn-primary btn-sm" ng-click="friendRequestAccepted(request)">Accept</button> 
                                                            <button type="button" class="btn btn-default btn-sm" ng-click="friendRequestDecline(request)">Decline</button>
                                                        </div>
                                                        <div class="bs-example cust-float-right" data-example-id="label-variants" ng-show="request.is_accepted == '1'">
                                                            <span class="label label-info">Connected</span> 
                                                        </div>                                                                                                              
                                                    </a>
                                                </li><!-- end message -->

                                            </ul>
                                        </li>
                                        <!--<li class="footer"><a href="<?php //echo base_url() ?>memberprofile/notification">See All Notifications</a></li> -->
                                    </ul>
                                </li>
                                <!-- Messages: style can be found in dropdown.less-->
                                <li class="dropdown messages-menu">
                                    <a href="javascript:void(0)" ng-click="updateNotification()" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bell"></i>
                                        <span class="label" ng-show="notification_list_count > 0" ng-bind="notification_list_count"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">You have <span ng-bind="notification_list_count"></span> unread notifications</li>
                                        <li>
                                            <!-- inner menu: contains the actual data -->
                                            <ul class="menu" ng-show="notification_list.length > 0">
                                                <li ng-repeat="notify in notification_list"><!-- start message -->
                                                    <a href="javascript:void(0)">
                                                        <div class="image default_image">
                                                            
                                                               <span ng-show="notify.image_url==null" ng-bind="notify.bussinessname | capitalize:true"> </span>
                                                                <img ng-show="notify.image_url!=null" ng-src="{{'<?php echo base_url(); ?>uploads/profile_image/'+notify.image_url}}" src="" class="img-circle" alt="User Image">
                                                                                                                   
                                                        </div>
                                                        <h4>
                                                            <span ng-bind="notify.bussinessname">Support Team </span>
															<small ng-bind="notify.noti_type=='hub_like'?'liked your post':''"></small>
															<small ng-bind="notify.noti_type=='hub_share'?'shared your post':''"></small>
															<small ng-bind="notify.noti_type=='hub_comment'?'commented on your post':''"></small>
															<small ng-bind="notify.noti_type=='prof_view'?'viewed your post':''"></small>
															<small ng-bind="notify.noti_type=='recomend_community'?'recommended you to participate':''"></small> 
                                                            
                                                        </h4>
                                                       <!-- <p ng-bind="notify.bussinesstype !=null ?notify.bussinesstype:'N / A'">Buisnesstype</p>
                                                        <p ng-bind="noti_type=='prof_view'?'viewed your profile':noti_type == 'hub_like'?'like your post':noti_type=='hub_share'?'share your post':'comment on your post'"></p>
                                                        <p ng-bind="notify.noti_type==='prof_view'?'viewed your profile':notify.noti_type === 'hub_like'?'like your post':notify.noti_type==='hub_share'?'share your post':notify.noti_type==='hub_unlike'?'Unlike your post'"></p>-->
														
														<small class="msg_time"><i class="fa fa-clock-o"></i><span  ng-bind="notify.on_date"> 5 mins</span></small>
                                                    </a>
                                                </li><!-- end message -->

                                            </ul>
                                        </li>
                                        <!--<li class="footer"><a href="<?php echo base_url() ?>memberprofile/notification">See All Notifications</a></li>-->
                                    </ul>
                                </li>
                                <li class="dropdown messages-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa  fa-envelope"></i>
                                        <span class="label" ng-show="count_unread_message.length > 0" ng-bind="count_unread_message.length"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">You have <span ng-bind="count_unread_message.length > 0 ? count_unread_message.length:0"></span> unread messages</li>
                                        <li>
                                            <!-- inner menu: contains the actual data -->
                                            <ul class="menu">
                                                <li ng-repeat="mail in show_unread_mail_notification"><!-- start message -->
                                                    <a href="<?php echo base_url(); ?>message">
                                                        <div class="image default_image">
                                                          
                                                              <span ng-show="mail.image_url==null" ng-bind="mail.bussinessname | capitalize:true"> </span>                                                                
                                                              <img ng-show="mail.image_url!=null" ng-src="{{'<?php echo base_url(); ?>uploads/profile_image/'+mail.image_url}}" class="img-circle" alt="User Image">
                                                        </div>
                                                        <h4>
                                                            <span ng-bind="mail.bussinessname">Support Team</span>
                                                         </h4>
														 <p>{{ mail.msg_body | strLimit: 40 }}</p>
														<small class="msg_time"><i class="fa fa-clock-o"></i><span ng-bind="mail.msg_time"> 5 mins</span></small>
														
                                                    </a>
                                                </li>
                                                <li ng-repeat="mail in show_read_mail_notification"><!-- start message -->
                                                    <a href="<?php echo base_url(); ?>message">
                                                        <div class="image default_image">
                                                            <!--<img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" class="img-circle" alt="User Image">-->
                                                            <img ng-src="{{mail.image_url!=null?'<?php echo base_url(); ?>uploads/profile_image/'+mail.image_url:'<?php echo base_url(); ?>assets/images/inbox-img.png'}}" class="img-circle" alt="User Image">
                                                        </div>
                                                        <h4>
                                                            <span ng-bind="mail.bussinessname">Support Team</span>
                                                            
                                                        </h4>
                                                        <p>{{ mail.msg_body | strLimit: 40 }}</p>
                                                        <small class="msg_time"><i class="fa fa-clock-o"></i><span ng-bind="mail.msg_time"> 5 mins</span></small>
                                                    </a>
                                                </li><!-- end message -->                                                
                                            </ul>
                                        </li>
                                        <li class="footer"><a href="<?php echo base_url(); ?>message">See All Messages</a></li>
                                    </ul>
                                </li>
                                <!-- User Account: style can be found in dropdown.less -->
                                <li class="dropdown user user-menu">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                                        <div class="bg_yellow_sm">
                                            <span ng-show="sign_image == ''" ng-bind="user_initial"></span>
                                            <img ng-show="sign_image != ''"  ng-src="{{path + sign_image}}" src="<?php echo base_url(); ?>assets_phase2/images/default_pic.jpg" class="user-image" alt="User Image">
                                        </div>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <!-- User image -->
                                        <li class="user-header">
                                            <!--<img src="<?php echo base_url(); ?>assets_phase2/images/user2-160x160.jpg" class="img-circle" alt="User Image">-->
                                            <div class="bg_yellow profile_img" ng-click="openModalForSignatureImg()">
                                                    <!-- Modal Crop Image
                                                    <div class="modal fade" id="crop_profile_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="myModalLabel">Upload an Busines Logo</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <div class="container-crop">
                                                                        <div class="imageBox">
                                                                            <div class="thumbBox"></div>
                                                                            <div class="spinner" style="display: none">Loading...</div>
                                                                        </div>                                            
                                                                        <div class="cropped"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <input type="file" id="file">                                               
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                                                    <button type="button" id="btnZoomIn" class="btn btn-primary">+</button>
                                                                    <button type="button" id="btnZoomOut" class="btn btn-primary">-</button>
                                                                    <button type="button" ng-click="uploadProfileImage()" id="btnCrop" class="btn btn-primary">Done</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>-->

                                               <i class="fa fa-camera change_photos2" ></i> <!-- ng-show="show_if_own_profile" style="cursor:pointer" ng-show="toggle_edit" onclick="open_profile_crop_img_modal()" -->
                                                <span ng-show="sign_image == ''" ng-bind="user_initial"></span>
                                                <img ng-show="sign_image != ''" ng-src="{{path + sign_image}}" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image">
                                            </div>
                                            <p>
                                                <!-- <span ng-bind="bussinessname">Alexander Pierce - Web Developer</span> -->
                                                <small><span>{{profile_first_name}}&nbsp;{{profile_last_name}},<br />{{profile_bussinessname}} </span></small>
                                                <small>Member since <span ng-bind="reg_date">Nov. 2012</span></small>
                                            </p>
                                        </li>
                                        <!-- Menu Body -->
                                        <li class="user-body">
                                            <!-- <div class="col-xs-4 text-center">
                                                <a href="javascript:void(0)">Followers(0)</a>
                                            </div>ConnectionNo -->
                                            <div class="text-center">
                                                <a href="http://arlians.com/developer/connection">Connections(<span ng-bind="connection_count">0</span>)</a>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <!-- <a href="javascript:void(0)">Connections(0)</a>-->
                                            </div>
                                            <!--<div class="col-xs-4 text-center">
                                                <a href="javascript:void(0)">Engagement score(0)</a>
                                            </div>-->
                                        </li>
                                        <!-- Menu Footer-->
                                        <li class="user-footer">
                                            <div class="pull-left">
                                                <a href="<?php echo base_url() ?>memberprofile/viewMemberProfileApi" class="btn btn-default btn-flat">Profile</a>
                                            </div>
                                            <div class="pull-right">
                                                <!--<a href="<?php echo base_url() ?>member/logout" class="btn btn-default btn-flat">Sign out</a>-->
                                                <a href="javascript:void(0)" onclick="signOut()" class="btn btn-default btn-flat">Sign out</a>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>                        
                    </nav>


                </header>
                <div class="modal fade" id="crop_signature_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Upload a personal profile picture</h4>
                            </div>
                            <div class="modal-body">
                                <div class="container-crop">
                                    <div class="imageBox_signature">
                                        <div class="thumbBox_signature"></div>
                                        <div class="spinner" style="display: none">Loading...</div>
                                    </div>
                                    <div class="action">
<!--<input type="button" id="uploadImg" style="float: right" value="Upload"/>
<input type="button" id="btnCrop" value="Upload" style="float: right">
<input type="button" id="btnZoomIn" value="+" style="float: right">
<input type="button" id="btnZoomOut" value="-" style="float: right">-->
                                    </div>
                                    <div class="cropped"></div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="file" id="filesignature">                                               
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                <button type="button" id="btnZoomInc" class="btn btn-primary">+</button>
                                <button type="button" id="btnZoomOutc" class="btn btn-primary">-</button>
                                <button type="button" id="btnCropc" ng-click="uploadSignatureImage()" class="btn btn-primary">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
                <script src="<?php echo base_url(); ?>assets_phase2/js/dashboard.js"></script>
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="main-sidebar sticky-scroll-menu">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel class="bg_yellow_sm"-->
                        <div class="user-panel">
                            <div class="image">
                                <div  ng-class="{'bg_yellow_sm': front_img_name == ''}">
                                    <span ng-show="front_img_name == ''" ng-bind="user_initial"></span>
                                    <img ng-show="front_img_name != ''"  ng-src="{{path + front_img_name}}" src="<?php echo base_url(); ?>assets_phase2/images/default_pic.jpg" class="user-image" alt="User Image">
                                </div>

                                <!-- <a href="javascript:void(0)" class="online-status"><i class="fa fa-circle text-success"></i></a> -->
                            </div>
                            <div class="sidebar_profile_details">
                                <p class="user_name" ng-bind="bussinessname">Smarttech.Ltd</p>
<!--                                <span class="admin text-capitalize">administrator</span><br>-->
                               <!-- <a href="javascript:void(0)" ng-click="editProfile()"  class="edit_profile">Edit Profile</a>-->
                                <a href="<?php echo base_url() ?>memberprofile/viewMemberProfileApi" class="edit_profile">Edit Profile</a>
                            </div>
                        </div>

                        <!-- sidebar menu: : style can be found in sidebar.less -->
                        <ul class="sidebar-menu">
                            <li class="header"></li>
                            <?php echo Menu_Helper::get_main_menu_html("arlians_menu") ?>


                        </ul>
                    </section>
                    <!-- /.sidebar -->
                </aside>
                <div class="modal fade" id="wiz_one_last_modal" tabindex="-1"  data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
                    <div role="document" class="modal-dialog welcome_popup">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button aria-label="Close" data-dismiss="modal" class="close" type="button"><span aria-hidden="true">×</span></button>
                                <h4 id="myModalLabel" class="modal-title">Welcome to your Hub!</h4>
                            </div>
                            <div class="modal-body">
                                <input type="text" class="welcominput" ng-model="user_buisnessname" ng-disabled="is_profrssional" placeholder="enter a business name">
                                <p>or click here to signup as a professional <input type="checkbox" ng-click="toggleIsProfrssional()" ng-model="is_profrssional" class="welcome_check"></p>
                                <input type="text" ng-model="user_firstname" class="welcominput seconinput" placeholder="first name">
                                <input type="text" ng-model="user_lastname" class="welcominput lastinput" placeholder="last name">
                            </div>
                            <div class="modal-footer">
                                
                                <button class="btn first_open welcome_yellow" ng-click="welcomeWizard()" type="button">Let's go</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">

                <!-- Main content -->
                <section class="content">
                    <?php echo $content; ?>
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->
            <section  class="mobile_footer">
                <div class="col-sm-10 col-xs-10">
                    <div class="box-body">
                      <div class="image default_image">                           
                        <img src="<?php echo base_url(); ?>uploads/profile_image/default.png" ng-src="{{path + back_image}}" alt="">                                         
                    </div>

                    <div class="latest_content">
                        <div class="latest_header">
                           <a href="#" ><h6 ng-bind="profile_bussinessname">Bob Pinta</h6></a>                                                                                   
                        </div>
                        <p class="post_normaltxt" ><span ng-bind="cityuser"></span> ,<span  ng-bind="country_name"></span></p>
                    </div>
                </div>
                </div>

                 <div class="col-sm-2 col-xs-2">
                     <i class="fa  fa-power-off signout" ng-click="signOut();"></i>
                 </div>
            </section>

            <footer class="main-footer">

                <strong>Copyright &copy; 2016 <a href="javascript:void(0)">Arlians.</a>
            </footer>


            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>

        </div><!-- ./wrapper -->
        <div id="loader-wrapper">
            <div id="loading-center">
                <div id="loading-center-absolute">
                    <div class="object" id="object_one"></div>
                    <div class="object" id="object_two"></div>
                    <div class="object" id="object_three"></div>
                </div>
            </div>
        </div>
		<!--<div id="circularG" style="display:none;">
			  <div id="circularG_1" class="circularG"></div>
			  <div id="circularG_2" class="circularG"></div>
			  <div id="circularG_3" class="circularG"></div>
			  <div id="circularG_4" class="circularG"></div>
			  <div id="circularG_5" class="circularG"></div>
			  <div id="circularG_6" class="circularG"></div>
			  <div id="circularG_7" class="circularG"></div>
			  <div id="circularG_8" class="circularG"></div>
		</div>-->
        <!-- Bootstrap 3.3.5 -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/bootstrap.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/app.min.js"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/jquery.slimscroll.js"></script>
        <!--Plug-in Tab-->
        <script src="<?php echo base_url(); ?>assets_phase2/js/easyResponsiveTabs.js"></script> 
        <script src="<?php echo base_url(); ?>assets_phase2/js/owl.carousel.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/bootstrap-tagsinput.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/editor.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/modernizr.custom.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/js/jquery.dlmenu.js"></script>


        <script>
                                            $(function() {
                                            $('#dl-menu5').dlmenu();
                                            });</script>

        <script type="text/javascript">









                    function signOut(){
                    UserProxy.unsetAccessToken();
                            window.location = "<?php echo base_url() ?>member/logout";
                    }
            $(document).ready(function () {
            $('.prevent_default').click(function(event){
				alert("hi");
            event.stopPropagation();
            });
                    //Horizontal Tab
                    $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
                    width: 'auto', //auto or any width like 600px
                    fit: true, // 100% fit in a container
                    tabidentify: 'hor_1', // The tab groups identifier
                    activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                            var $info = $('#nested-tabInfo');
                            var $name = $('span', $info);
                            $name.text($tab.text());
                            $info.show();
                    }
            });
                    // Child Tab
                    $('#ChildVerticalTab_1').easyResponsiveTabs({
					type: 'vertical',
                    width: 'auto',
                    fit: true,
                    tabidentify: 'ver_1', // The tab groups identifier
                    activetab_bg: '#fff', // background color for active tabs in this group
                    inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
                    active_border_color: '#c1c1c1', // border color for active tabs heads in this group
                    active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
            });
                    //Vertical Tab
                    $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
                    width: 'auto', //auto or any width like 600px
                    fit: true, // 100% fit in a container
                    closed: 'accordion', // Start closed if in accordion view
                    tabidentify: 'hor_1', // The tab groups identifier
                    activate: function (event) { // Callback function if tab is switched
                    var $tab = $(this);
                            var $info = $('#nested-tabInfo2');
                            var $name = $('span', $info);
                            $name.text($tab.text());
                            $info.show();
                    }
            });
            });
                    var looper;
                    var degrees = 0;
                    function rotateAnimation(el, speed){
                    var elem = document.getElementById(el);
                            if (navigator.userAgent.match("Chrome")){
                    elem.style.WebkitTransform = "rotate(" + degrees + "deg)";
                    } else if (navigator.userAgent.match("Firefox")){
                    elem.style.MozTransform = "rotate(" + degrees + "deg)";
                    } else if (navigator.userAgent.match("MSIE")){
                    elem.style.msTransform = "rotate(" + degrees + "deg)";
                    } else if (navigator.userAgent.match("Opera")){
                    elem.style.OTransform = "rotate(" + degrees + "deg)";
                    } else {
                    elem.style.transform = "rotate(" + degrees + "deg)";
                    }
                    looper = setTimeout('rotateAnimation(\'' + el + '\',' + speed + ')', speed);
                            degrees++;
                            if (degrees > 359){
                    degrees = 1;
                    }
                    document.getElementById("status").innerHTML = "rotate(" + degrees + "deg)";
                    }
        </script>
        <script>
            (function(i, s, o, g, r, a, m){i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function(){
            (i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date(); a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                    ga('create', 'UA-65677143-1', 'auto');
                    ga('send', 'pageview');

        </script>


    <!--<script>rotateAnimation("load_img");</script>-->
    </body>
</html>