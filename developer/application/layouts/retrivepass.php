<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">

        <title>Arlians</title>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/bootstrap.css" rel="stylesheet">
        <!-- Bootstrap theme -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/bootstrap-theme.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_phase2/wizard/css/jquery.pagepiling.css" />

        <!-- Owl Carousel Assets -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/owl.carousel.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/owl.theme.css" rel="stylesheet">

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_phase2/wizard/css/component.css" />
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/modernizr.custom.js"></script>

        <!-- scrollbar -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/jquery.mCustomScrollbar.min.css" rel="stylesheet">   



        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/dots.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/fonts/fonts.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets_phase2/wizard/css/responsive.css" rel="stylesheet">  
        <!-- CSS -->
        <link href="<?php echo base_url(); ?>assets_phase2/css/alertify.css" rel="stylesheet"> 
        <!-- Default theme -->
        <link href="<?php echo base_url(); ?>assets_phase2/css/default.css" rel="stylesheet"> 

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url(); ?>assets_phase2/js/jQuery-2.1.4.min.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/jquery.fullPage.js"></script> 
        <!--Alertify-->
<!--        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.core.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.default.css">-->
        <!--Knockout JS-->
        <!--<script src="<?php echo base_url(); ?>assets/js/knockout-3.2.0.js"></script>-->
		<!-- Angular JS -->
		<script src="<?php echo base_url(); ?>assets_phase2/js/angular.js"></script>
        <!-- UserProxy JS -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/UserProxy.js"></script>
        <!-- common-helper JS -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/common-helper.js"></script>
        <!-- JavaScript -->
        <script src="<?php echo base_url(); ?>assets_phase2/js/alertify.js"></script>




 <!--        <script src="<?php echo base_url(); ?>assets/js/alertify.js"></script>-->
    </head>

    <body class="signup_page step1 step2">

        <div id="pagepiling" >

            <div id="signup" class="section">
                <header class="top_header">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <a href="landing.html" class="logo"><img src="<?php echo base_url(); ?>assets_phase2/wizard/images/logo.png" alt=""></a>
                            </div>

                        </div>
                    </div>
                </header>
                <div class="container" >                    
                    <?php echo $content;?>
                </div> 
               
            </div> 
        </div>
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- flipcard js -->
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/flip.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/jquery.mCustomScrollbar.concat.min.js"></script>        
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/selectstyle.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/selectFx.js"></script>
        <!-- input effect -->
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/classie.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/dots.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/wizard/js/jquery.dlmenu.js"></script>

        <script>
            addLanguageScript = function(lang,callback) {
                //debugger;
                var head = document.getElementsByTagName("head")[0],
                script = document.createElement('script');
                script.type = 'text/javascript'
                script.src = lang + '.js'
                head.appendChild(script);
                //callback(true);
            };
            
            $(function () {
                if (screen.width > 767) {                                            
                    addLanguageScript('<?php echo base_url(); ?>assets_phase2/landing/js/jquery.pagepiling');
                }                
            });
            [].slice.call(document.querySelectorAll('.dotstyle > ul')).forEach(function (nav) {
                new DotNav(nav, {
                    callback: function (idx) {
                        //console.log( idx )
                    }
                });
            });
            (function () {
                // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
                if (!String.prototype.trim) {
                    (function () {
                        // Make sure we trim BOM and NBSP
                        var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                        String.prototype.trim = function () {
                            return this.replace(rtrim, '');
                        };
                    })();
                }
                [].slice.call(document.querySelectorAll('input.input__field')).forEach(function (inputEl) {
                    // in case the input is already filled..
                    if (inputEl.value.trim() !== '') {
                        classie.add(inputEl.parentNode, 'input--filled');
                    }
                    // events:
                    inputEl.addEventListener('focus', onInputFocus);
                    inputEl.addEventListener('blur', onInputBlur);
                });
                function onInputFocus(ev) {
                    classie.add(ev.target.parentNode, 'input--filled');
                }

                function onInputBlur(ev) {
                    if (ev.target.value.trim() === '') {
                        classie.remove(ev.target.parentNode, 'input--filled');
                    }
                }
            })();
        </script>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65677143-1', 'auto');
  ga('send', 'pageview');

</script>
    </body>
</html>
