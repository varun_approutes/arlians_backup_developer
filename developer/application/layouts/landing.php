<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Arlians</title>
        <meta name="author" content="Alvaro Trigo Lopez" />
        <meta name="description" content="pagePiling.js plugin by Alvaro Trigo." />
        <meta name="keywords"  content="pile,piling,piling.js,stack,pages,scrolling,stacking,touch,fullpile,scroll,plugin,jquery" />
        <meta name="Resource-type" content="Document" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_phase2/landing/css/jquery.pagepiling.css" />
            <!-- Added Css -->
            <!-- Bootstrap core CSS -->
            <link href="<?php echo base_url(); ?>assets_phase2/landing/css/bootstrap.css" rel="stylesheet"/>
            <!-- Bootstrap theme -->
            <link href="<?php echo base_url(); ?>assets_phase2/landing/css/bootstrap-theme.css" rel="stylesheet"/>  

            <!-- Owl Carousel Assets -->
            <link href="<?php echo base_url(); ?>assets_phase2/landing/css/owl.carousel.css" rel="stylesheet"/>
            <link href="<?php echo base_url(); ?>assets_phase2/landing/css/owl.theme.css" rel="stylesheet"/>

            <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_phase2/landing/css/component.css" />
            <script src="<?php echo base_url(); ?>assets_phase2/landing/js/modernizr.custom.js"></script>



            <!-- Custom styles for this template -->
            <link href="<?php echo base_url(); ?>assets_phase2/landing/css/style.css" rel="stylesheet" />
            <link href="<?php echo base_url(); ?>assets_phase2/landing/css/dots.css" rel="stylesheet"/>
            <link href="<?php echo base_url(); ?>assets_phase2/landing/fonts/fonts.css" rel="stylesheet"/>
            <link href="<?php echo base_url(); ?>assets_phase2/landing/css/font-awesome.css" rel="stylesheet"/>



            <!--Alertify-->
            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.core.css"/>
            <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/alertify.default.css"/>
            <link href="<?php echo base_url(); ?>assets_phase2/landing/css/responsive.css" rel="stylesheet"/> 
			
					<?php  if(strpos($_SERVER['HTTP_HOST'],'www')===false)
    {
        $config['base_url'] = 'http://arlians.com/developer/';
    }
    else
    {
        $config['base_url'] = 'http://www.arlians.com/developer/';
    }
?>
<script>
    //alert('is login ? :'+is_login+' user id : '+user_id+' user_type : '+user_type);
    var CORE_URL = '<?php echo $config['base_url']?>';
</script>
            <!-- jQuery 2.1.4 -->
            <script src="<?php echo base_url(); ?>assets_phase2/js/jQuery-2.1.4.min.js"></script>
             <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
            <!-- Angular JS -->
            <script src="<?php echo base_url(); ?>assets_phase2/js/angular.js"></script>
            <!-- Dashboard JS -->
            <script src="<?php echo base_url(); ?>assets_phase2/landing/js/landing.js"></script>
            <!-- UserProxy JS -->
            <script src="<?php echo base_url(); ?>assets_phase2/js/UserProxy.js"></script>
            <!-- common-helper JS -->
            <script src="<?php echo base_url(); ?>assets_phase2/js/common-helper.js"></script>
            <script src="<?php echo base_url(); ?>assets_phase2/js/google-plus-signin.js"></script>
            <script src="<?php echo base_url(); ?>assets_phase2/landing/js/modernizr.custom.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/alertify.js"></script>


    </head>
    <body class="land-page" ng-app="arliansApp" ng-controller="LandingCtrl as landing">
        <div id="pagepiling" style="display:none">
            <div class="section" id="section1">
                <div class="section_holder">
                    <header class="top_header">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <a href="<?php echo base_url(); ?>" class="logo"><img src="<?php echo base_url(); ?>assets_phase2/landing/images/logo.png" alt=""></a>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <div class="top_action_btn">
                                        <a ng-repeat="filter in filters" ng-click="select($index)" ng-class="{active: $index == selected}" href="javascript:void(0)" class="top_btn" ng-bind="filter.name">Sign up</a> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div class="firstpage_cont" ng-show="login_section">
                        <div class="email_validate" ng-show="user_login_section">
                            <h2>Welcome Back.</h2>
                            <!--ng-submit="validateUser(user_login)"ng-submit="validateUser(user_login)"-->
                            <form novalidate class="form-inline" name="loginUser"  ng-submit="validateUser(user_login)" >
                                <!--  -->                                          
                                <span class="input input--haruki animated fadeInUp d1">
                                    <input class="input__field input__field--haruki"  ng-model="user_login.user_email" ng-class="{'has-error': loginUser.user_email.$error.pattern(user_login.user_email)}" ng-pattern="/^\S+@\S+\.\S+$/i" required autocomplete="off" type="email" name="user_email" />                                    
                                    <span ng-show="loginUser.user_email.$dirty && loginUser.user_email.$error.required" class="validation_error">Required</span>
                                    <span ng-show="!loginUser.user_email.$error.required && loginUser.user_email.$dirty && loginUser.user_email.$error.email" class="validation_error">Enter correct email format</span>   
                                    <label class="input__label input__label--haruki" for="user_email">
                                        <span class="input__label-content input__label-content--haruki">Enter your Email</span>
                                    </label>                                    
                                </span>                                 
                                <span class="input input--haruki animated fadeInUp d2">
                                    <input class="input__field input__field--haruki" ng-model="user_login.user_password" autocomplete="off" type="password" name="user_password" required/>
                                    <span ng-show="loginUser.user_password.$dirty && loginUser.user_password.$error.required" class="validation_error">Required</span>
<!--                                    <span ng-show="loginUser.user_password.$dirty && loginUser.user_password.$error.minlength" style="color:red">Too short!!!</span> 
                                    <span ng-show="loginUser.user_password.$dirty && loginUser.user_password.$error.maxlength" style="color:green">Strong</span> -->
                                    <label class="input__label input__label--haruki" for="user_password">
                                        <span class="input__label-content input__label-content--haruki">Password</span>
                                    </label>
                                </span> 
                                <button type="submit" class="btn get_started animated fadeInUp d3" >Log in</button>
                            </form>
                            <div class="line"></div>
                            <!-- <p class="signup_media">log in with <a class="link_yellow" href="javascript:void(0)"> Linkedin</a>, <a class="link_yellow" href="javascript:void(0)">Twitter</a> or <a class="link_yellow" href="javascript:void(0)">Gmail</a></p> -->
                            <p class="signup_media">Forgot your user name or password? <a class="link_yellow" href="javascript:void(0)" ng-click="showForgetCrendentialsSection()"> click here</a></p>
                        </div>
                        <div class="email_validate" ng-show="user_forget_credentials_section">
                            <h2>Find your Account.</h2>
                            <form novalidate class="form-inline" name="userForgetCredentials" ng-submit="forgetCrendentials(user_forget_credentials)">
                                <span class="input input--haruki animated fadeInUp d1">
                                    <input class="input__field input__field--haruki"  ng-model="user_forget_credentials.user_email" ng-class="{'has-error': userForgetCredentials.user_email.$error.pattern}" ng-pattern="/^\S+@\S+\.\S+$/i" required autocomplete="off" type="email" name="user_email"/> 
                                    <span ng-show="userForgetCredentials.user_email.$dirty && userForgetCredentials.user_email.$error.required" class="validation_error">Required</span> 
                                    <span ng-show="!userForgetCredentials.user_email.$error.required && userForgetCredentials.user_email.$dirty && userForgetCredentials.user_email.$error.email" class="validation_error">Enter correct email format</span>                                     
                                    <label class="input__label input__label--haruki" for="user_email">
                                        <span class="input__label-content input__label-content--haruki">Enter your Email</span>
                                    </label>                                    
                                </span>                                 
                                <button type="submit" class="btn get_started animated fadeInUp d3">Search</button>
                            </form>
                            <div class="line"></div>
                            <!-- <p class="signup_media">Or sign up with <a class="link_yellow" href="javascript:void(0)"> Linkedin</a>, <a class="link_yellow" href="javascript:void(0)">Twitter</a> or <a class="link_yellow" href="javascript:void(0)">Gmail</a></p> -->
                            <p class="signup_media">Back to Login<a class="link_yellow" href="javascript:void(0)" ng-click="showLoginSection()"> click here</a></p>

                        </div>
                    </div>
                    <div class="container"  ng-show="sign_in_section">
                        <div class="firstpage_cont">
                            <div class="fade-in one">
                                <h1 class="cd-headline letters rotate-3">Your personal <br>business 
                                        <div class="word_disappear">
                                            <span class="quotes">GENIUS</span>
                                            <span class="quotes">NEWS</span>
                                            <span class="quotes">COMMUNITY</span>
                                            <span class="quotes">RESOURCES</span>
                                            <span class="quotes">Q&amp;A</span>
                                            <span class="quotes">SEARCH ENGINE</span>
                                            <span class="quotes">NETWORK</span>


                                        </div> 
                                </h1>
                                <p class="firstpage_details">Business news perfectly curated, an insightful community of business owners answering important questions and the most advanced business search engine yet.</p>
                            </div>
                            <div class="email_validate" ng-show="user_email_section">
                                <h2>Stay on top of business.</h2>
                                <form novalidate class="form-inline" name="userMail" ng-submit="validateUserEmail(user_email)">
                                    <span class="input input--haruki animated fadeInUp d1 sign_up_animation">
                                        <input class="input__field input__field--haruki" id="input-1" ng-model="user_email.user_email" ng-class="{'has-error': userEmail.user_email.$error.pattern}" ng-pattern="/^\S+@\S+\.\S+$/i" required autocomplete="off" type="email" name="user_email"  />
                                        <span ng-show="userMail.user_email.$dirty && userMail.user_email.$error.required" class="validation_error">Required</span>
                                        <span ng-show="!userMail.user_email.$error.required && userMail.user_email.$dirty && userMail.user_email.$error.email" class="validation_error">Enter correct email format</span>   
                                        <label class="input__label input__label--haruki" for="input-1">
                                            <span class="input__label-content input__label-content--haruki">Enter your Email</span>
                                        </label>
                                    </span>
                                    <button type="submit" class="btn get_started animated fadeInUp d3 sign_up_animation">Sign up free</button>
                                </form>
                                <div class="line"></div>
                                <!-- <p class="signup_media">Or sign up with <a class="link_yellow" href="#"> Linkedin</a>, <a class="link_yellow" href="#">Twitter</a> or <a class="link_yellow" href="#">Gmail</a></p> -->
                                 <p>By clicking Sign Up you accept Arlians' 
                    <a type="button" class="link_yellow" style="cursor: pointer; cursor: hand;" data-toggle="modal" data-target="#myModal">User Agreement</a>, 
                                <!--<a href="#" class="link_yellow">Privacy Policy</a> -->
                                <a type="button" class="link_yellow" style="cursor: pointer; cursor: hand;" data-toggle="modal" data-target="#myModal_privacy">Privacy and Cookies Policy</a>
                                </p> 
                            </div>
                            <div class="email_validate" ng-show="user_validat_email_and_password_section">
                                <h2>Lets validate your email.</h2>
                                <form class="form-inline" novalidate name="signInUserVal" ng-submit="registeredUser(sign_in_user, user_email)">
                                    <span class="input input--haruki animated fadeInUp d1">
                                        <input class="input__field input__field--haruki" ng-model="sign_in_user.confirm_user_email" ng-class="{'has-error': sign_in_user.confirm_user_email.$error.pattern}" ng-pattern="/^\S+@\S+\.\S+$/i" required autocomplete="off" type="email" name="user_confirm_email" id="input-2" />
                                        <span ng-show="signInUserVal.user_confirm_email.$error.required && signInUserVal.user_confirm_email.$dirty" class="validation_error">Required</span>
                                        <span ng-show="!signInUserVal.user_confirm_email.$error.required && signInUserVal.user_confirm_email.$dirty && signInUserVal.user_confirm_email.$error.noMatch" class="validation_error"> Email do not match</span> 
                                        <label class="input__label input__label--haruki" for="input-2">
                                            <span class="input__label-content input__label-content--haruki">Re enter your Email</span>
                                        </label>
                                    </span>
                                    <span class="input input--haruki animated fadeInUp d2">
                                        <input class="input__field input__field--haruki" type="password" ng-model="sign_in_user.user_password" autocomplete="off" type="password" name="user_password" required ng-minlength="3" ng-maxlength="10" id="input-3"/>
                                        <span ng-show="signInUserVal.user_password.$error.required && signInUserVal.user_password.$dirty" class="validation_error">Required</span>
                                        <span ng-show="!signInUserVal.user_password.$error.required && signInUserVal.user_password.$dirty && (signInUserVal.user_password.$error.minlength || signInUserVal.user_password.$error.maxlength)" class="validation_error">Length should be 4 to 9 character long</span>
                                        <label class="input__label input__label--haruki" for="input-3">
                                            <span class="input__label-content input__label-content--haruki">Password</span>
                                        </label>
                                    </span>                           

                                    <button type="submit" class="btn get_started animated fadeInUp d3">Lets get started</button>
                                </form>
                                <div class="line"></div>
                                <!-- <p class="signup_media">Or sign up with <a class="link_yellow" href="#"> Linkedin</a>, <a class="link_yellow" href="#">Twitter</a> or <a class="link_yellow" href="#">Gmail</a></p> -->
                            </div>
                        </div>   

                        <div class="social_links">
                            <ul>
                                <!-- <li id="gConnect">
                                    <google-plus-signin clientid="745751287917-b6dasmtph0uff1k64nitof9is5dk3a76.apps.googleusercontent.com">                                        
                                    </google-plus-signin>
                                </li> -->
                                <li><a href="javascript:void(0);" onclick="googleLogin()"><i class="fa fa-location-arrow"></i></a></li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- <a class="down-page" href="#">
                        learn more<br>
                            <img src="<?php echo base_url(); ?>assets_phase2/landing/images/top-down-arrow.png" alt=""/>
                    </a> -->
                </div>
            </div>


            <div class="section" id="section2">
                <div class="section_holder">
                    <div class="container">
                        <div class="row">
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                                <ul class="dl-menu">
                                    <li><a href="#page1" >Home</a></li>
                                    <li><a href="#page1" ng-click="showPgeOneSignUp()">Sign up</a></li>
                                    <li><a href="#page1" ng-click="showPgeOneLogin()">Login</a></li>              
                                </ul>
                            </div>
                        </div>
                        <div  class="phone_img">
                           

                            <img src="<?php echo base_url(); ?>assets_phase2/landing/images/phone.png" alt=""/>
                        </div>

                        <div class="slider_part">
                            <h2>A world of business opportunities</h2>
                            <div id="slider" class="owl-carousel">                                
                                <div class="item">                                    
                                    <p class="slider_bluetxt">Join the founders circle. A community of business owners answering the important questions.</p>
                                    <p class="slider_graytxt">Arlians community lets you interact with other business owners, share your knowledge and learn from other founders.</p>
                                </div>

                                <div class="item">
                                    <p class="slider_bluetxt">Stay ahead with intelligently curated news perfectly tailored to your business.</p>
                                    <p class="slider_graytxt">In your business, time is too valuable to read irrelevant news. With Arlians you only need a few minutes to stay up to date with news and articles close to your business and that of your customers.</p>
                                </div>

                                <div class="item">
                                    <p class="slider_bluetxt">Easily find businesses, make connections and form partnerships.</p>
                                    <p class="slider_graytxt">Finding potential partners for your business is all about timing. Arlians’ powerful matching technology allows you to switch on and off searches for possible partners and easily find business matches that match your characteristics.</p>
                                </div>

                                <div class="item">
                                    <p class="slider_bluetxt">Effortless lead generation. Let your business appear in front of customers that want your products.</p>
                                    <p class="slider_graytxt">Arlians’ matching technology helps you showcase your business profile to potential customers. Advertising does not get more effortless than that.</p>
                                </div>
                            </div>
                            <div class="text-center m_t30">
                                <a href="#" class="btn_yellow">Try it</a>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>


            <div class="section" id="section3">
                <div class="section_holder video_section">	    		
                    <img src="<?php echo base_url(); ?>assets_phase2/landing/images/cloud.png" id="cloud_animation" alt=""/>
                    <img src="<?php echo base_url(); ?>assets_phase2/landing/images/cloud.png" id="cloud_animation2" alt=""/>
                    <img src="<?php echo base_url(); ?>assets_phase2/landing/images/break_line.png" id="breakline_animation1" alt=""/>
                    <img src="<?php echo base_url(); ?>assets_phase2/landing/images/break_line.png" id="breakline_animation2" alt=""/>
                    <img src="<?php echo base_url(); ?>assets_phase2/landing/images/plus.png" id="spinner" alt=""/>
                    <img src="<?php echo base_url(); ?>assets_phase2/landing/images/plus.png" id="spinner1" alt=""/>
                    <!-- opportunity -->
                    <div class="opportunity_section">
                        <div class="container">
                            <div class="white_menu">
                                <div id="dl-menu1" class="dl-menuwrapper">
                                    <button class="dl-trigger">
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                        <span></span>
                                    </button>
                                    <ul class="dl-menu">
                                        <li><a href="javascript:void(0);">Home</a></li>
                                        <li><a href="javascript:void(0);" ng-click="showThirdPageSignIn()">Sign up</a></li>
                                        <li><a href="javascript:void(0);" ng-click="showThirdPageLogin()">Login</a></li>              
                                    </ul>
                                </div>
                            </div>

                            <div class="opportunity_inner">
                                <h3 ng-show="third_page_signin_section">Never Miss an Oportunity</h3>
                                <h3 ng-show="third_page_validate_sign_section">Verification</h3>
                                <h3 ng-show="third_page_login_section">Welcome</h3>
                                <form novalidate class="form-inline" ng-show="third_page_signin_section" ng-submit="validateUserEmail(user_email)">  
                                    <div class="form-group">   
                                        <input  ng-model="user_email.user_email" ng-pattern="/^\S+@\S+\.\S+$/i" required autocomplete="off" type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address"/>
                                    </div>
                                    <button type="submit" class="btn get_started">Sign up free</button>
                                </form>
                                <form novalidate class="form-inline" ng-submit="registeredUser(sign_in_user, user_email)" ng-show="third_page_validate_sign_section">  
                                    <div class="form-group">   
                                        <input type="email" ng-model="sign_in_user.confirm_user_email" class="form-control"  placeholder="Email address "/>
                                        <input type="password" ng-model="sign_in_user.user_password" class="form-control"  placeholder="Password " required ng-minlength="3" ng-maxlength="10"/>
                                        <span ng-show="signInUserVal.user_password.$error.required && signInUserVal.user_password.$dirty" class="validation_error">Required</span>
                                        <span ng-show="!signInUserVal.user_password.$error.required && signInUserVal.user_password.$dirty && (signInUserVal.user_password.$error.minlength || signInUserVal.user_password.$error.maxlength)" class="validation_error">Length should be 4 to 9 character long</span>
                                    </div>
                                    <button type="submit" class="btn get_started">Sign up free</button>
                                </form>
                                <form novalidate class="form-inline" ng-show="third_page_login_section" ng-submit="validateUser(user_login)">  
                                    <div class="form-group">   
                                        <input type="email" ng-model="user_login.user_email" class="form-control"  placeholder="Email address "/>
                                        <input type="password" ng-model="user_login.user_password" class="form-control" placeholder="Password" />
                                    </div>
                                    <button type="submit" class="btn get_started">Login</button>
                                </form>

                            </div>
                        </div>
                    </div>

                    <h2 class="heading text-center">Watch Gary and Dave’s story</h2>
                    <div class="video">
                        <iframe src="https://player.vimeo.com/video/133324592?title=0&byline=0&portrait=0" ></iframe>
                    </div>
                </div>
            </div>

            <div class="section" id="section4">
                <div class="section_holder top_business">
                    <div class="container">
                        <div class="white_menu clearfix">
                            <div id="dl-menu2" class="dl-menuwrapper">
                                <button class="dl-trigger"> 
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </button>
                                <ul class="dl-menu">
                                    <li><a href="#page1">Home</a></li>
                                    <li><a href="#page1" ng-click="showPgeOneSignUp()">Sign up</a></li>
                                    <li><a href="#page1" ng-click="showPgeOneLogin()">Login</a></li>              
                                </ul>
                            </div>
                        </div>
                        <h3 ng-show="user_email_section">Stay on top of business.</h3>
                        <div class="email_validate1" ng-show="user_email_section">   
                            <div class="avator">
                                <img src="<?php echo base_url(); ?>assets_phase2/landing/images/avator.jpg" alt="" class="avator_img"/>
                                <span class="signup">Sign up today</span>
                                <p><em>enter your email address</em></p>
                            </div>                     
                            <form novalidate name="userEmail" ng-submit="validateUserEmail(user_email)">
                                <span class="input input--haruki animated fadeInUp d1">
                                    <input class="input__field input__field--haruki"  ng-model="user_email.user_email" ng-class="{'has-error': userEmail.user_email.$error.pattern}" ng-pattern="/^\S+@\S+\.\S+$/i" required autocomplete="off" type="email" name="user_email" />
                                    <span ng-show="userEmail.user_email.$dirty && userEmail.user_email.$error.required" class="validation_error">Required</span>
                                    <span ng-show="!userEmail.user_email.$error.required && userEmail.user_email.$dirty && userEmail.user_email.$error.email" class="validation_error">Enter correct email format</span>   
                                    <!--<input class="input__field input__field--haruki"  ng-model="user_email.user_email" required autocomplete="off" type="email" id="user_email" />-->
                                    <label class="input__label input__label--haruki" for="user_email">
                                        <span class="input__label-content input__label-content--haruki">Enter your Email</span>
                                    </label>
                                    <!--<div class="help-block" ng-show="user_email.user_email.$error.pattern || user_email.user_email.$error.required">
                                        <p ng-message="required">This field is required</p>
                                        <p ng-message="email">This needs to be a valid email</p>
                                    </div>
                                    -->
                                </span>  
                                <button class="btn get_started animated fadeInUp" type="submit">Sign up free</button>
                            </form>
                            <div class="line"></div>
                            <p class="signup_media">By clicking Sign Up you accept Arlians' 
                                <!-- <a href="javascript:void(0);" class="link_yellow" data-toggle="modal" data-target="#myModal">User Agreement</a>, --> 
                                <a type="button" class="link_yellow" style="cursor: pointer; cursor: hand;" data-toggle="modal" data-target="#myModal">User Agreement</a>, 
                                <!--<a href="#" class="link_yellow">Privacy Policy</a> -->
                                <a type="button" class="link_yellow" style="cursor: pointer; cursor: hand;" data-toggle="modal" data-target="#myModal_privacy">Privacy and Cookies Policy</a>
                               
                                <!-- <a href="#" class="link_yellow">Cookies Policy</a></p> -->
                            <!-- <p class="signup_media">Also sign up with <a href="#" class="link_yellow"> Linkedin</a>, <a href="#" class="link_yellow">Twitter</a> or <a href="#" class="link_yellow">Gmail</a></p> -->
                        </div>
                        <h3 ng-show="user_validat_email_and_password_section">Lets validate your email.</h3>
                        <div class="email_validate1" ng-show="user_validat_email_and_password_section">
                            <div class="avator">
                                <img src="<?php echo base_url(); ?>assets_phase2/landing/images/avator.jpg" alt="" class="avator_img"/>
                                <span class="signup">Sign up today</span>
                                <p><em>validate your email</em></p>
                            </div>  
                            <form class="form-inline" name="signInUser" ng-submit="registeredUser(sign_in_user, user_email)"  >
                                <span class="input input--haruki animated fadeInUp d1">
                                    <input class="input__field input__field--haruki" ng-model="sign_in_user.confirm_user_email" ng-class="{'has-error': sign_in_user.confirm_user_email.$error.pattern}" ng-pattern="/^\S+@\S+\.\S+$/i" required autocomplete="off" type="email" name="user_confirm_email"/>
                                    <span ng-show="signInUser.user_confirm_email.$dirty && signInUser.user_confirm_email.$error.required" class="validation_error">Required</span>
                                   <!-- <span ng-show="!signInUser.user_email.$error.required && signInUser.user_email.$dirty && signInUser.user_email.$error.email" style="color:red">Enter correct email format</span> -->
                                    <span ng-show="!signInUser.user_confirm_email.$error.required && signInUser.user_confirm_email.$dirty && signInUser.user_confirm_email.$error.noMatch" class="validation_error"> Email do not match</span>   
                                 <!--********my now<span ng-show=>   sign_in_user.confirm_user_email != user_email.user_email-->
                                    <label class="input__label input__label--haruki" for="user_confirm_email">
                                        <span class="input__label-content input__label-content--haruki">Re enter your Email</span>
                                    </label>
                                </span>
                                <span class="input input--haruki animated fadeInUp d2">
                                    <input class="input__field input__field--haruki" ng-model="sign_in_user.user_password" autocomplete="off" type="password" name="user_password" required ng-minlength="3" ng-maxlength="10"/>
                                    <span ng-show="signInUser.user_password.$dirty && signInUser.user_password.$error.required" class="validation_error">Required</span>
                                    <span ng-show="signInUser.user_password.$dirty && !signInUser.user_password.$error.required && (signInUser.user_password.$error.minlength || signInUser.user_password.$error.maxlength)" class="validation_error">Length should be 4 to 9 character long</span>

                                    <!--(signInUser.user_password.$error.minlength||signInUser.user_password.$error.maxlength)-->

                                    <label class="input__label input__label--haruki" for="user_password">
                                        <span class="input__label-content input__label-content--haruki">Password</span>
                                    </label>
                                </span>                           

                                <button type="submit"  class="btn get_started animated fadeInUp d3" >Lets get started</button>
                            </form>
                            <div class="line"></div>
                            <p class="signup_media">Also sign up with <a href="#" class="link_yellow"> Linkedin</a>, <a href="#" class="link_yellow">Twitter</a> or <a href="#" class="link_yellow">Gmail</a></p>
                        </div>
                    </div>
                    <!-- Footer -->
                    <footer>
                        <div class="container">
                            <div class="row">
                                <div class="col-md-2 col-sm-2">
                                    <a href="<?php echo base_url(); ?>" class="footer_logo"><img src="<?php echo base_url(); ?>assets_phase2/images/logo.png" alt=""></a>
                                </div>

                                <div class="col-md-3 col-sm-3">
                                    <p class="copyright"> Copyright ©  Arlians Ltd. 2016 - All Rights Reserved</p>
                                </div>  
                                <div class="col-md-5 col-sm-4">
                                    <ul class="footer_link">                          
                                        <li><a href="#page1">Home</a></li>
                                        <?php /* <li><a href="#">Terms &amp; Conditions</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                        <li><a href="#">Pricing</a></li>
                                        <li><a href="#">Career</a></li>
                                        <li><a href="#">Developers</a></li>
                                        <li><a href="#">Advertise</a></li> */ ?>
                                        <li><a target="_blank" href="https://arlianscom.wordpress.com/">Blog</a></li>
                                        <li><a href="mailto:hello@arlians.com">Contact us</a></li>
                                    </ul>
                                </div>

                                <div class="col-md-2 col-sm-3">
                                    <ul class="footer_social_link">
                                        <li><a href="#"><i class="fa fa-location-arrow"></i></a></li>
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    </ul>
                                </div>  

                            </div>
                        </div>
                    </footer>
                </div>
            </div>
        </div>

        <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Terms</h4>
      </div>
      <div class="modal-body">
       Terms
1. Introduction

1.1 Arlians is the dating site of businesses. It is designed to help businesses network, communicate, and collaborate across the globe
1.2 The Arlians website is operated by Arlians Limited ("we", “our”, “us”). We are registered in England and Wales under company number 09447080 and have our registered office at Level 3, 207 Regent Street, London, W1B 3HH. Our main trading address is Unit 3.05 Clerkenwell Workshops, 31 Clerkenwell Close, London, EC1R 0AT. We are a limited company.
1.2 By accessing, using and/or purchasing a subscription to the Arlians website you are agreeing to enter into a binding contract with us on these terms and conditions, including our Website Terms of Use and our Privacy Policy.
1.3 If you do not agree to these terms and conditions or Website Terms of Use or Privacy Policy please do not access, use or subscribe to the Arlians website.
2. Your status

The Arlians website is directed exclusively at commercial enterprises and individuals acting in a business capacity. You agree that you will be using the Arlians website for your own business purposes only.

3. Your right to use the Arlians website

3.1 Subject to clauses 3.2 and 3.3 below and your compliance with these terms and conditions and the Website Terms of Use we grant to you a non-exclusive, non-transferable right to use the Arlians website solely for your internal business operations.
3.2 You acknowledge that certain features of the Arlians website (“Restricted Features”) will only be available to you pursuant to clause 3.1 if you have purchased a subscription to the Arlians website (“Subscription”).
3.3 If have purchased a Subscription, you shall be permitted to use the Restricted Features for the duration of the Subscription Period (as defined in clause 11 below).
3.4 We do not guarantee the availability of the Arlians website at any time or that the Arlians website will be un-interrupted or error free.
3.5 The Arlians website is directed to people residing in the United Kingdom. We do not represent that content available on or through the Arlians website is appropriate or available in other locations. We may limit the availability of the Arlians website to any person or geographic area at any time. If you choose to access our site from outside the United Kingdom, you do so at your own risk.
3.6 In addition to these terms and conditions, in using the Arlians website you agree to comply with our Website Terms of Use
4.Subscription Fee

4.1 If you wish to purchase a Subscription you agree to pay the applicable fees and taxes from time to time as set out on the Arlians website (“Subscription Fee”). If you fail to pay such fees and taxes, we may terminate your Subscription and your right to access and use the Restricted Features will terminate.
4.2 The Subscription Fee is payable by the methods set out on the Arlians website and is due on the first day of each Subscription Period. You authorise us to store your payment method (e.g. credit card details) in order to avoid interruptions to the Restricted Features and to facilitate the renewal of the Subscription Periods.
4.3 If we have not received payment of the Subscription Fee by the relevant due date (whether due to incorrect payment details being entered or otherwise), and without prejudice to any of our other rights and remedies then we may, without liability to you, disable your access to the Restricted Features and we shall be under no obligation to provide any or all of the Restricted Features while the Subscription Fee remains unpaid.
4.4 The Subscription Fee: 

4.4.1 is non-cancellable and non-refundable; and
4.4.2 is exclusive of any applicable taxes (such as value added tax), which shall be added to the Subscription Fee at the appropriate rate.
4.5 The Subscription Fee you pay may be subject to foreign exchange fees or differences in prices based on location (e.g. exchange rates) and you shall be solely responsible for these.
4.6 We shall be entitled to increase the Subscription Fee at the start of each Subscription Period.
5.Your Data

5.1 As between you and us, you own all right, title and interest in and to all data, information and content that you submit to the Arlians website (“Your Data”) and shall have sole responsibility for the legality, reliability, integrity, accuracy and quality of Your Data.
5.2 You acknowledge that as the Arlians website is not a storage service, we have no obligation to store, maintain or provide you with a copy of Your Data except to the extent required by law. Accordingly you will be solely responsible for securing and backing up Your Data.
5.3 Subject to clause 5.4 below, by submitting Your Data to the Arlians website you grant to us a non-exclusive, worldwide, transferrable and sub-licensable right to copy, modify, distribute, publish and process Your Data without providing you with any further notice or obtaining from you any further consent. 
You also agree that other users of the Arlians website may access and share Your Data.
5.4 To the extent that you submit any personal data to the Arlians website we shall treat such personal data in accordance with the Privacy Policy as such document may be amended from time to time by us in our sole discretion.
5.5 You agree that Your Data will be accurate and that you have right to submit Your Data to the Arlians website.
5.6 We reserve the right to remove Your Data from the Arlians website at any time and at our discretion.
5.7 We have the right to disclose your identity to any third party who is claiming that any of Your Data constitutes a violation of their intellectual property rights, or of their right to privacy.
6.Third parties

6.1 You acknowledge that the Arlians website may enable or assist you to access the website content of, correspond with, and purchase products and services from, third parties. Any such activity carried out by you done so solely at its own risk.
6.2 We make no representation or commitment and shall have no liability or obligation whatsoever in relation to the content or use of, or correspondence with, any such third-party or any such third party website, or any transactions completed, and any contract entered into by you, with any such third party. Any contract entered into and any transaction completed with a third party is between you and the relevant third party, and not us.
6.3 We do not endorse or approve any third party, any third party’s products or services nor the content of any of the third party website made available via the Arlians website.
6.4 The views expressed by others on the Arlians website do not represent our views.
7. Proprietary rights

You acknowledge and agree that we and/or our licensors own all intellectual property rights in the Arlians website. Except as provided above you do not have any rights to, or in, patents, copyright, database right, trade secrets, trade names, trade marks (whether registered or unregistered), or any other rights or licences in respect of the Arlians website.

8. No reliance on information

8.1 The content on the Arlians website is provided for general information only. It is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on the Arlians website.
8.2 Although we make reasonable efforts to update the information the Arlians website, we make no representations, warranties or guarantees, whether express or implied, that the content on our site is accurate, complete or up-to-date.
9. Indemnity

You agree to defend, indemnify and hold us harmless against claims, actions, proceedings, losses, damages, expenses and costs (including without limitation court costs and reasonable legal fees) arising out of or in connection with your use of the Arlians website including but not limited to any breach by you of the Website Terms of Use.

10. Limitation of Liability

10.1 This clause 10 sets out the entire financial liability of us (including any liability for the acts or omissions of our employees, agents and sub-contractors) to you: 

10.1.1 arising under or in connection with these terms and conditions;
10.1.2 in respect of any use made by you of the Arlians website; and
10.1.3 in respect of any representation, statement or tortious act or omission (including negligence) arising under or in connection with the Arlians website.
10.2 Except as expressly and specifically provided in these terms and conditions: 

10.2.1 you assume sole responsibility for the results obtained from your use of the Arlians website and for conclusions drawn from such use. We shall have no liability for any damage caused by errors or omissions in any information, instructions or scripts provided to us by you;
10.2.2 all warranties, representations, conditions and all other terms of any kind whatsoever implied by statute or common law are, to the fullest extent permitted by applicable law, excluded from the agreement between you and us; and
10.2.3 the Arlians website is provided to you on an "as is" basis.
10.3 Nothing in these terms and conditions excludes our liability: 

10.3.1 for death or personal injury caused by our negligence; or
10.3.2 for fraud or fraudulent misrepresentation.
10.4 Subject to clause 10.2 and clause 10.3: 

10.4.1 we shall not be liable to you whether in tort (including for negligence or breach of statutory duty), contract, misrepresentation, restitution or otherwise for any loss of profits, loss of business, depletion of goodwill and/or similar losses or loss or corruption of data or information, or pure economic loss, or for any special, indirect or consequential loss, costs, damages, charges or expenses however arising in connection with the Arlians website; and
10.4.2 our total aggregate liability to you in, tort (including negligence or breach of statutory duty), misrepresentation, restitution or otherwise, arising in connection with the Arlians website shall be limited to the total Subscription Fee paid during the 12 months immediately preceding the date on which the claim arose.
11. Term and Termination

11.1 If you have purchased a Subscription, your Subscription shall commence on the date you purchased the Subscription and shall continue for one month and, thereafter, the Subscription shall be automatically renewed for successive periods of one month (each month a “Subscription Period”).
11.2 We may terminate your Subscription immediately if you are in material breach of any of the terms and conditions including if you breach any of our Website Terms of Use. You may terminate your Subscription at any time on 30 days’ notice in writing to us at subscriptions@arlians.com .
11.3 Any rights that have accrued to either party at the date of termination will remain enforceable after termination.
General

12.1 If any provision (or part of a provision) of these terms and conditions is found by any court or administrative body of competent jurisdiction to be invalid, unenforceable or illegal, the other provisions shall remain in force.
12.2 We reserve the right at any time without notice to revise the content of our site (including the services offered by us) and these terms and conditions. Any changes to these terms and conditions will be posted on our site and by continuing to use our site following any such change you will signify that you agree to be bound by the revised terms and conditions of use.
12.3 These terms and conditions (and any dispute, controversy, proceedings or claim of whatever nature arising out of or in any way relating to them or their formation) shall be governed by and interpreted in accordance with the laws of England and Wales and, for these purposes, the parties submit to the exclusive jurisdiction of the courts of England and Wales.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- End -->  

        <!-- Modal -->
<div id="myModal_privacy" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Privacy and Cookies Policy</h4>
      </div>
      <div class="modal-body">
Privacy and Cookies Policy
Our mission at Arlians Limited ("we") is to help businesses network, communicate, and collaborate across the globe. Our “members” share relevant information with us and communicate with other such “members”. We believe that collecting such information provides great benefit to users of Arlians. In doing so, we make your privacy one of our top priorities, this policy is intended to set out what we may do with collected information in order to provide the best service, and adhere to industry standards. This policy sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By visiting any of www.arlians.com, www.arlians.net, www.arlians.org, www.arlians.co.uk (“the Arlians website”) you are accepting and consenting to the practices described in this policy. 

For the purpose of the Data Protection Act 1998 (the “Act”), the data controller is Arlians Limited of Level 3, 207 Regent Street, London, W1B 3HH.

Information we may collect from you

We may collect and process the following data about you:

Information you give us. You may give us information about you by filling in forms on the Arlians website (including by creating a profile) or by corresponding with us by phone, e-mail or otherwise. This includes information you provide when you register to use our site, subscribe to the Arlians website and when you report a problem with our site. The information you give us may include your name, address, e-mail address and phone number, financial and credit card information, personal description and photograph.


Information we collect about you. With regard to each of your visits to our site (as is normal) we may automatically collect the following information: technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;
information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from our site (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our customer service number.
Information we receive from other sources. We may receive information about you if you use any of the other websites we operate or the other services we provide. We are also working closely with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, credit reference agencies) and may receive information about you from them.

Cookies

The Arlians website uses cookies to distinguish you from other users of the Arlians Website. This helps us to provide you with a good experience when you browse the Arlians Website and also allows us to improve it. More information about the cookies we use and how you can block them is available from our Cookies Policy

Uses made of the information

We use information held about you in the following ways:


Information you give to us. We will use this information:

to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and services that you request from us;
to provide you with information about other goods and services we offer that are similar to those that you have already purchased or enquired about; to notify you about changes to our service;
to ensure that content from the Arlians website is presented in the most effective manner for you and for your computer.
Information we collect about you. We will use this information:

to administer the Arlians website and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;
to improve the Arlians website to ensure that content is presented in the most effective manner for you and for your computer;
to allow you to participate in features of our service, when you choose to do so, for example, by placing information on the Arlians website;
as part of our efforts to keep the Arlians website safe and secure;
to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you;
to make suggestions and recommendations to you and other users of [website name to be inserted] about goods or services that may interest you or them.
Information we receive from other sources. We may combine this information with information you give to us and information we collect about you. We may use this information and the combined information for the purposes set out above (depending on the types of information we receive).

Disclosure of your information

We may share your information with selected third parties including:

business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you;
analytics and search engine providers that assist us in the improvement and optimisation of the Arlians website.
We may disclose your personal information to third parties:

in the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets;
If Arlians Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.
If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce any agreement we have with you; or to protect the rights, property, or safety of Arlians, our customers, or others.
By entering your own information on the Arlians website, you acknowledge that other users of the Arlians website will have access to that information and that we have no control over the use of that information by third parties. Do not place on the Arlians website any information that you are not comfortable sharing with other users of the Arlians website.
Where we store your personal data

The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area ("EEA"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. 

Where we have given you (or where you have chosen) a password which enables you to access certain parts of our site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone. 

Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to our site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access. 

Your rights

You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at support@arlians.com. 

The Arlians website may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.

Access to Information

The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of £10 to meet our costs in providing you with details of the information we hold about you.

Changes to our Privacy Policy

Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.

Contact

Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to hello@arlians.com.
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- End -->       
        <script src="<?php echo base_url(); ?>assets_phase2/landing/js/headline.js"></script> 
        <script src="<?php echo base_url(); ?>assets_phase2/landing/js/dots.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/landing/js/jquery.dlmenu.js"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/landing/js/classie.js"></script>
        <script src="https://apis.google.com/js/client:platform.js?onload=startApp"></script>
        <script src="<?php echo base_url(); ?>assets_phase2/landing/js/owl.carousel.js"></script>
        <script>
                                                addLanguageScript = function(lang, callback) {
                                                //debugger;
                                                var head = document.getElementsByTagName("head")[0],
                                                        script = document.createElement('script');
                                                        script.type = 'text/javascript'
                                                        script.src = lang + '.js'
                                                        head.appendChild(script);
                                                        //callback(true);
                                                };
                                               $(window).load(function() {
                                        //setTimeout(function(){
                                        if (screen.width > 767) {
                                        $('#pagepiling').pagepiling({
                                        menu: '#menu',
                                                anchors: ['page1', 'page2', 'page3', 'page4'],
                                                navigation: {
                                                'textColor': '#f2f2f2',
                                                        'bulletsColor': '008000',
                                                        'position': 'left',
                                                        'tooltips': ['Page 1', 'Page 2', 'Page 3', 'Page 4', 'Page 5']
                                                }
                                        });
                                        }
                                        //},2000);
                                        $('#pagepiling').show();
                                        });
                                                (function() {

                                                var quotes = $(".quotes");
                                                        var quoteIndex = - 1;
                                                        function showNextQuote() {
                                                        ++quoteIndex;
                                                                quotes.eq(quoteIndex % quotes.length)
                                                                .fadeIn(2000)
                                                                .delay(2000)
                                                                .fadeOut(2000, showNextQuote);
                                                        }

                                                showNextQuote();
                                                })();
                                                $(document).ready(function() {
												$("#slider").owlCarousel({
												navigation : false,
                                                slideSpeed : 300,
                                                paginationSpeed : 400,
                                                singleItem : true,
                                                autoPlay : true

                                                // "singleItem:true" is a shortcut for:
                                                // items : 1, 
                                                // itemsDesktop : false,
                                                // itemsDesktopSmall : false,
                                                // itemsTablet: false,
                                                // itemsMobile : false
                                        });
                                        });
                                                if (screen.width > 767) {
                                        addLanguageScript('<?php echo base_url(); ?>assets_phase2/landing/js/jquery.pagepiling');
                                                //$('#pagging').remove();
                                        }



                                        $(function() {
                                        $('#dl-menu').dlmenu();
                                                $('#dl-menu1').dlmenu();
                                                $('#dl-menu2').dlmenu();
                                        });
                                                [].slice.call(document.querySelectorAll('.dotstyle > ul')).forEach(function(nav) {
                                        new DotNav(nav, {
                                        callback : function(idx) {
                                        //console.log( idx )
                                        }
                                        });
                                        });
                                                (function() {
                                                // trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
                                                if (!String.prototype.trim) {
                                                (function() {
                                                // Make sure we trim BOM and NBSP
                                                var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
                                                        String.prototype.trim = function() {
                                                        return this.replace(rtrim, '');
                                                        };
                                                })();
                                                }

                                                [].slice.call(document.querySelectorAll('input.input__field')).forEach(function(inputEl) {
                                                // in case the input is already filled..
                                                if (inputEl.value.trim() !== '') {
                                                classie.add(inputEl.parentNode, 'input--filled');
                                                }
                                                // events:
                                                inputEl.addEventListener('focus', onInputFocus);
                                                        inputEl.addEventListener('blur', onInputBlur);
                                                });
                                                        function onInputFocus(ev) {
                                                        classie.add(ev.target.parentNode, 'input--filled');
                                                        }

                                                function onInputBlur(ev) {
                                                if (ev.target.value.trim() === '') {
                                                classie.remove(ev.target.parentNode, 'input--filled');
                                                }
                                                }
                                                })();
        </script>
    </body>
</html>
