<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_othermembersprofile extends CI_model
{
 
function __construct(){

		parent::__construct();
		
	}
function getUserDetails($id){
	
	//$id = $this->session->userdata['logged_in']['id'];
	
	$this->db->select('*');
	$this->db->from('ar_members');
	$this->db->where('mid',$id);
	$query = $this->db->get();
	
	$rs = $query->result_array();
	
	//$data['name'] = $rs[0]['email'];
	$data['fname'] = $rs[0]['fname'];
	$data['lname'] = $rs[0]['lname'];
	$data['email'] = $rs[0]['email'];
	$data['sharecode'] = $rs[0]['sharecode'];

	$this->db->select('*');
	$this->db->from('ar_request_access_page');
	$this->db->where('mid',$id);
	$query = $this->db->get();
	
	$rsb = $query->result_array(); //echo '<pre>'; var_dump($rsb); die();
	
	$data['bussinessname'] = $rsb[0]['bussinessname'];
	$data['tag_line'] = $rsb[0]['tag_line'];
	$data['city'] = $rsb[0]['city'];
	$data['country'] = $rsb[0]['country'];
	$data['bussinessdesc'] = $rsb[0]['bussinessdesc'];
	$data['bussinesstype1'] = $rsb[0]['bussinesstype'];
	$data['about_me'] = $rsb[0]['about_me'];

	$this->db->select('ans_set_cat_id');
	$this->db->from('ar_member_data_wiz_one');
	$this->db->where('mid',$id);
	$query = $this->db->get();
	
	$rsb1 = $query->result_array();
	
	$data['wiz_one_ans'] = $rsb1;
	
	$this->db->select('*');
	$this->db->from('member_image');
	$this->db->where('mid',$id);
	$this->db->where('type','profile_img');
	$query = $this->db->get();
	
	$rsb2 = $query->result_array();
	
	if($rsb2 == NULL){
		$imgValue = "profile_image.png";
	}else{
		$imgValue = $rsb2[0]['image_url'];
	}
	$data['profile_image'] = $imgValue;
	
	$this->db->select('*');
	$this->db->from('member_image');
	$this->db->where('mid',$id);
	$this->db->where('type','back_img');
	$query = $this->db->get();
	
	$rsb3 = $query->result_array();
	
	if($rsb3 == NULL){
		$imgValue1 = "profile_image.png";
	}else{
		$imgValue1 = $rsb3[0]['image_url'];
	}
	$data['background_image'] = $imgValue1;
	
	$data['questionone'] = $this->getMemberLookingForData($id);
	
	$data['questionTwo'] = $this->getMemberLocalityData($id);
	
	$data['questionThree'] = $this->getMemberServiceData($id);
	
	$data['questionFour'] = $this->getMemberIndustryData($id);
	
	$data['questionFive'] = $this->getMemberSectorData($id);
	
	$data['questionSix'] = $this->getMemberlookingForBussinessData($id);
	
	$data['connection_list'] = $this->getCOnnectionDetails($id);

	$data['wiz_two_quiz_one_and_six_selected_biz_ids'] = $this->wizTwoQuizOneAndSixSelectedBizIds($id);
	$data['wiz_two_quiz_four_and_five_selected_biz_ids'] = $this->wizTwoQuizFourAndFiveSelectedBizIds($id);
	$data['locality_to_explore'] = $this->wizTwoQuizLocalityToExplore($id);
	$data['customer_type'] = $this->wizTwoQuizCustomerType($id);
	$data['looking_service'] = $this->wizTwoQuizLookingService($id);
	$data['latest_post'] = $this->getUserLatestFivePost($id);
	$data['doc_uploaded'] = $this->getUserDocUploaded($id);
	$data['has_founding_member'] = $this->paymentType($id);
	return $data;
	
}
function getUserDocUploaded($id){
	$this->db->select('*');
	$this->db->from('ar_user_doc');
	$this->db->where('mid',$id);
	$this->db->order_by("doc_id", "desc");		
	$query = $this->db->get();			 
	$result = $query->result_array();
	return $result;	
}
function paymentType($mid){
	$this->db->from('ar_transaction');
	$this->db->where('mid', $mid);
	$this->db->order_by('mid desc'); 
	$this->db->limit(0,1); 
    $query = $this->db->get();
	$res = $query->row();
	return $res->subscription_type;
}
function wizTwoQuizOneAndSixSelectedBizIds($id){
	$this->db->select('looking_for,looking_service');
	$this->db->from('ar_member_data_wiz_two');
	$this->db->where('mid',$id);
	$query = $this->db->get();	
	$result = $query->row();

	$interested_looking_for_ids = explode(',', $result->looking_for);
	$interested_services_ids = explode(',', $result->looking_service);
	$this->db->select('look_service_name');
	$this->db->from('ar_looking_services');
	$this->db->where_in('look_service_id', $interested_services_ids);
	$query_interested_services = $this->db->get();	
	$data['interested_services_result'] = $query_interested_services->result_array();

	$this->db->select('looking_for_name');
	$this->db->from('ar_looking_for');
	$this->db->where_in('looking_for_id', $interested_looking_for_ids);
	$query_interested_services = $this->db->get();
	$data['interested_looking_for_result'] = $query_interested_services->result_array();
	//print_r($data); exit;
	return $data;
}
function wizTwoQuizCustomerType($id){
	$this->db->select('is_service');
	$this->db->from('ar_member_data_wiz_two');
	$this->db->where('mid',$id);
	$query = $this->db->get();	
	$result = $query->row();		
	$result = trim($result->is_service,',');
	$result = explode(',', $result);
	$this->db->select('*');
	$this->db->from('ar_is_service');
	$this->db->where_in('service_id',$result);		
	$query = $this->db->get();	
	$data = $query->result();	
	return $data;
}
function wizTwoQuizLookingService($id){
	$this->db->select('looking_service');
	$this->db->from('ar_member_data_wiz_two');
	$this->db->where('mid',$id);
	$query = $this->db->get();	
	$result = $query->row();	
	//print_r($result); exit;	
	$result = trim($result->looking_service,',');
	$result = explode(',', $result);
	$this->db->select('*');
	$this->db->from('ar_looking_services');
	$this->db->where_in('look_service_id',$result);		
	$query = $this->db->get();	
	$data = $query->result();	
	//print_r($data); exit;
	return $data;
}
function getUserLatestFivePost($id){
	$this->db->select('*');
	$this->db->from('ar_hub');
	$this->db->where('mid',$id);
	$this->db->order_by("hid", "desc");
	$this->db->limit(6,0);
	$query = $this->db->get();			 
	$result = $query->result_array();
	return $result;	
}
function wizTwoQuizFourAndFiveSelectedBizIds($id){
	$this->db->select('look.industry_code,look.target_sector_code,ar_industry.industry_name,ar_industry.profile_image as industry_img,ar_target_sector.tar_sector_name,ar_target_sector.profile_image as sector_img');
	$this->db->from('ar_wiz_two_looking_industry as look');
	$this->db->join('ar_industry','ar_industry.industry_code = look.industry_code');
	$this->db->join('ar_target_sector','ar_target_sector.tar_sector_code = look.target_sector_code');
	$this->db->where('look.mid',$id);
	$query = $this->db->get();	
	$result = $query->result_array();
	return $result;
	//print_r($result); exit;
}
function wizTwoQuizLocalityToExplore($id){
	$this->db->select('locality');
	$this->db->from('ar_member_data_wiz_two');
	$this->db->where('mid',$id);
	$query = $this->db->get();	
	$result = $query->row();		
	$result = trim($result->locality,',');
	$result = explode(',', $result);
	$this->db->select('*');
	$this->db->from('ar_locality');
	$this->db->where_in('locality_id',$result);		
	$query = $this->db->get();	
	$data = $query->result();	
	//echo $this->db->last_query(); exit;
	return $data;
}
function getCOnnectionDetails($id){
	$this->db->select('*');
	$this->db->join('ar_request_access_page', 'ar_request_access_page.mid = connected.mid', 'left');
	$this->db->join('member_image', 'member_image.mid = connected.mid and member_image.type = "profile_img"', 'left');
	$this->db->from('connected');
	$this->db->where('connected.connected_id',$id);
	$this->db->where('connected.is_accepted','1');
	$query = $this->db->get();
	$res = $query->result_array();		
	return $res;
}

function getMemberLookingForData($id){
	
	$this->db->select('looking_for');
	$this->db->from('ar_member_data_wiz_two');
	$this->db->where('mid', $id);
	$query = $this->db->get();
		if ($query->num_rows() > 0){
		$result = $query->result_array();


		$x = $result[0]['looking_for'];
		$arr = explode(',',$x);
		
		$answerOne = array();
		array_pop($arr);
		foreach($arr as $row){
			
			if($row == NULL) continue;
			
			$this->db->select('looking_for_name');
			$this->db->from('ar_looking_for');
			$this->db->where('looking_for_id',$row);
			$query1 = $this->db->get();
			$res = $query1->result_array();
			$answerOne[] = $res[0]['looking_for_name'];
		}
		return $answerOne;
	} else{
		$arr = array();
		return $arr;
	}

}

function getMemberLocalityData($id){
	
	$this->db->select('looking_for');
	$this->db->from('ar_member_data_wiz_two');
	$this->db->where('mid', $id);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	$result = $query->result_array();
	
	$x = $result[0]['looking_for'];
	$arr = explode(',',$x);
	
		$answerTwo = array();
	
	//echo '<pre>'; var_dump($arr); die();
		foreach($arr as $row){
			
			if($row == NULL || count($row) == 1) continue;
				
			$this->db->select('locality_type');
			$this->db->from('ar_locality');
			$this->db->where('locality_id',$row);
			$query1 = $this->db->get();
			$res = $query1->result_array();
			 
			//echo isset($row);
			$answerTwo[] = $res[0]['locality_type'];
		}
	return $answerTwo;
		} else{
		$arr = array();
		return $arr;
	}
}

function getMemberServiceData($id){
	
	$this->db->select('is_service');
	$this->db->from('ar_member_data_wiz_two');
	$this->db->where('mid', $id);
	$query = $this->db->get();
	if ($query->num_rows() > 0){
	$result = $query->result_array();
	
	$x = $result[0]['is_service'];
	$arr = explode(',',$x);
	
		$answerThree = array();
		
	foreach($arr as $row){
		
		if($row == NULL || count($row) == 1) continue;
		
		$this->db->select('service_name');
		$this->db->from('ar_is_service');
		$this->db->where('service_id',$row);
		$query1 = $this->db->get();
		$res = $query1->result_array();
		$answerThree[] = $res[0]['service_name'];
	}
	return $answerThree;
		} else{
		$arr = array();
		return $arr;
	}
}

function getMemberlookingForBussinessData($id){
	
	$this->db->select('looking_service');
	$this->db->from('ar_member_data_wiz_two');
	$this->db->where('mid', $id);
	$query = $this->db->get();
	
	if ($query->num_rows() > 0){
	$result = $query->result_array();
	
	$x = $result[0]['looking_service'];
	$arr = explode(',',$x);
	
		$answerSix = array();
		
	foreach($arr as $row){
		
		if($row == NULL || count($row) == 1) continue;
		
		$this->db->select('looking_for_name');
		$this->db->from('ar_looking_for');
		$this->db->where('looking_for_id`',$row);
		$query1 = $this->db->get();
		$res = $query1->result_array();
		$answerSix[] = $res[0]['looking_for_name'];
	}
	return $answerSix;
			} else{
		$arr = array();
		return $arr;
	}
}

function getMemberIndustryData($id){
	
	$this->db->select('industry_code');
	$this->db->from('ar_wiz_two_looking_industry');
	$this->db->where('mid',$id);
	$query1 = $this->db->get();

	if ($query1->num_rows() > 0){
	$res = $query1->result_array();
	//echo '<pre>'; var_dump($res); die();
	$answerFour = array();

	foreach($res as $row){
		if($row == NULL || count($row) == 1) continue;
		
		$this->db->select('industry_name');
		$this->db->from('ar_industry');
		$this->db->where('industry_code`',$row['industry_code']);
		$query1 = $this->db->get();
		$res = $query1->result_array();
		$answerFour[] = $res[0]['industry_name'];
	}
	return $answerFour;
			} else{
		$arr = array();
		return $arr;
	}
}

function getMemberSectorData($id){
	
	$this->db->select('target_sector_code');
	$this->db->from('ar_wiz_two_looking_industry');
	$this->db->where('mid',$id);
	$query1 = $this->db->get();
	if ($query1->num_rows() > 0){
	$res = $query1->result_array();
	//echo '<pre>'; var_dump($res); die();
	$answerFive = array();
	
	foreach($res as $row){
		if($row == NULL || count($row) == 1) continue;
		
		$this->db->select('tar_sector_name');
		$this->db->from('ar_target_sector');
		$this->db->where('tar_sector_code`',$row['target_sector_code']);
		$query1 = $this->db->get();
		$res = $query1->result_array();
		$answerFive[] = $res[0]['tar_sector_name'];
	}
	return $answerFive;
				} else{
		$arr = array();
		return $arr;
	}
}

	function reqestForConnection($data){
		$this->db->insert('connected', $data);
		$insert_id = $this->db->insert_id();
		return $insert_id;
	}
	function getConnectRequestDetail($data){
		$this->db->select('*');
		$this->db->from('connected');
		$this->db->where('connected_id`',$data['connected_id']);
		$this->db->where('mid`',$data['mid']);
		$query = $this->db->get();
		$res = $query->row();
		return $res;
	}
	function inser_view($data){
		$this->db->insert('notification', $data);	 
	}
	function getVisibleStatus($id){
		$this->db->select('hide_me_prof_view');
		$this->db->from('ar_settings');
		$this->db->where('mid`',$id);
		$query = $this->db->get();
		$res = $query->row();
		return $res;
	}
	function getMemberType($id){
		$this->db->select('user_type');
        $this->db->from('ar_members');
        $this->db->where('mid', $id);
        $query = $this->db->get();
		$res = $query->result_array();		
		return $res;
	}
	function reqestForDisconnection($id){
		$this->db->where('mid', $id);
        $this->db->or_where('connected_id', $id);
		$this->db->delete('connected'); 
        if ( $this->db->affected_rows() == '1' ){
				return TRUE;
			}
			else{
				return FALSE;
			}
	}
	
	
 } //end
