<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_common extends CI_model
{
 
function __construct()
	{
		parent::__construct();
	}
	
	function add_subscribe_form()
	{
		$insert_data = array(
		                    'user_name' => $this->input->post('subscrib_name'),
							'email' => $this->input->post('subscrib_email')
						    );                   
		$this->db->insert('newsletter',$insert_data);
		return true;
	}
	
	
	
	function chk_login_data()
	{
			$this->load->library('encrypt');
			$email   = $this->input->post('email');
        	$password 	= $this->input->post('password');
			
			$sql = "select 	id,password,status from people where email = '".$email."' AND host_name = 'Mortgage'";
			//echo $sql;die;
			$query = $this->db->query($sql);
			$rs = $query->result_array();
	
			if(count($rs) > 0)
			{
				$decrypt_password=$this->encrypt->decode($rs[0]['password']);
				
				//echo $decrypt_password.'@@@'.$password;die;
				if($decrypt_password!=$password)
				{
				//echo '1';
					$this->session->set_userdata('err_msg',"Invalid username / password!");
					$return=0;
				}
				else
				{
					if($rs[0]['status']==0)
					{
						//echo '2';
						$this->session->set_userdata('err_msg',"Your profile has not been activated.");
						$return=0;
					}
					else
					{
						//echo '3';
						$this->session->set_userdata('userId', $rs[0]['id']);
						$return=1;
					}
				}
				
			}	
			else
			{
				$this->session->set_userdata('err_msg',"Invalid username / password!");
				$return=0;
			}
			return $return;
   	}
//--------------------------- Start function to get Site Settings details -----------------------//
	function SiteSettingsDetails()
	{
		$query = $this->db->get('site_settings');
        $query2=$query->num_rows();
        if($query->num_rows>0)
        {
            foreach($query->result() as $result_row)
            {
                define(strtoupper($result_row->field_name),$result_row->field_value);
            }
        }
	}
//--------------------------- End function to get Site Settings details ------------------------//	
//----------------------------- Start function to add referral ---------------------------------------------//
	function add_referral()
	{
		$insert_value=array(
		'user_id' => $this->session->userdata('userId'),
		'ref_email' => $this->input->post('ref_email'),
		'ref_date' => date('Y-m-d')
		);
		$insert=$this->db->insert('user_referrals',$insert_value);
		if($insert)
		{
//---------------------------- Start function to get user name ------------------------------------------------//
			$this->db->where('id',$this->session->userdata('userId'));
			$get=$this->db->get('people');
			if($get->num_rows>0)
			{
				foreach($get->result() as $result)
				{
					$username=$result->first_name.' '.$result->last_name;
				}
			}
//---------------------------- End function to get user name ---------------------------------------------------//

//------------------------------- Start section to send mail after add referral --------------------------------------------//
			$data['username']=$username;
			$data['link']=base_url().'home/registration';
			$view_page=$this->load->view('admin/email_template/send_referral',$data,TRUE);
			//echo $view_page;die;
			$this->load->library('email');
			$this->email->from(ADMIN_EMAIL, 'Admin');
			$this->email->to($this->input->post('ref_email')); 
			$this->email->subject('Refer a friend');
			$this->email->message($view_page);
			$this->email->send();
//---------------------------------- End function to send mail after add referral -------------------------------------------//
			$return=1;
		}
		else
		{
			$return=0;
		}
		return $return;
	}
//--------------------------------- End function to add referral --------------------------------------------//	
//-------------------------------------- Start function to generate alpha neumeric random number ------------------------------------------------------------//

	function GetAlphaneumericCode($length) 
	{
		$random= "";
		srand((double)microtime()*1000000);
		//$data = "AbcDE123IJKLMN67QRSTUVWXYZ"; 
		$data = "aBCdefghiJKLMN123opq45rs67tuv89wxyz"; 
		//$data .= "0FGH45OP89";
		for($i = 0; $i < $length; $i++) 
		{ 
			$random .= substr($data, (rand()%(strlen($data))), 1); 
		}
		
		return $random; 
	}
//-------------------------------------- End function to generate alpha neumeric random number ------------------------------------------------------------//

//----------------------------------------- Start function to get User Existance in Free Assessment ----------------------------------------------------------//
	function GetUserExistance()
	{
		$this->db->where('user_id',$this->session->userdata('userId'));
		$get=$this->db->get('user_loan');
		if($get->num_rows>0)
		{
			$return=1;
		}
		else
		{
			$return=0;
		}
		return $return;
	}
//-------------------------------------------- End function to get User Existance in Free Assessment -----------------------------------------------------------//

//-------------------------------------------- Start function to get the Free Assessment Id --------------------------------------------------------------------//
	function free_assessment_id()
	{
		$assessment_id='';
		$this->db->where('type','F');
		$get=$this->db->get('application_form');
		if($get->num_rows>0)
		{
			foreach($get->result() as $result)
			{
				$assessment_id=$result->id;
			}
		}
		return $assessment_id;
	}
//------------------------------------------------- End function to get the Free Assessment Id ----------------------------------------------------------------------//
//------------------------------------------------ Start function to get Logged IN User Name -----------------------------------------------------//
	function getUserName($user_id)
	{
		$data='';
		$this->db->where('id',$user_id);
		$q=$this->db->get('people');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data=$result->first_name.' '.$result->last_name;
			}
		}
		return $data;
	}
//---------------------------------------------------- End function to get Logged In User Name ------------------------------------------------------//
//--------------------------------------- Start function to get Application Name ----------------------------------------------------------//
	function ApplicationName($application_id)
	{
		$data='';
		$this->db->where('id',$application_id);
		$q=$this->db->get('application_form');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data=$result->name;
			}
		}
		return $data;
	}
//------------------------------------------- End function to get Application Name ----------------------------------------------------------//
//--------------------------- Start function to get the loan amount ---------------------------------------------------------//
	function GetLoanAmount($loan_id)
	{
		$data='';
		$this->db->where('id',$loan_id);
		$q=$this->db->get('application_form');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data=$result->amount;
			}
		}
		return $data;
	}
//------------------------------ End function to get the loan amount --------------------------------------------------------// 
//--------------------------- Start function to get the reward percent ---------------------------------------------------------//
	function GetRewardPoint($type)
	{
		$data='';
		$this->db->where('type',$type);
		$q=$this->db->get('reward_point');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data=$result->reward_percent;
			}
		}
		return $data;
	}
//------------------------------ End function to get the reward percent --------------------------------------------------------//  
//------------------------------ Start function to generate rand number -------------------------------------------------------//
	function GenerateNumber()
	{
		$rand_number=mt_rand(10000,99999);
		$this->db->where('transaction_id',$rand_number);
		$q=$this->db->get('user_payment');
		if($q->num_rows>0)
		{
			return GenerateNumber();
		}
		else
		{
			return $rand_number;
		}
	}
//---------------------------------- End function to generate rand_number ----------------------------------------------------//

//------------------------------ Start function to generate rand number -------------------------------------------------------//
	function UpadateUserPointBalance($user_id)
	{
		$this->db->select_sum('credit_balance');
		$this->db->select_sum('credit_point');
		$this->db->select_sum('debit_balance');
		$this->db->select_sum('debit_point');
		$this->db->select_sum('encash_point');
		$this->db->select_sum('encash_amount');
		$this->db->where('user_id',$user_id);
		$q=$this->db->get('user_point_history');
		//echo $this->db->last_query();die;
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$total_credit_balance=$result->credit_balance;
				$total_credit_point=$result->credit_point;
				$total_debit_balance=$result->debit_balance;
				$total_debit_point=$result->debit_point;
				$total_encash_point=$result->encash_point;
				$total_encash_amount=$result->encash_amount;
			}
		}
		//echo $total_credit_balance.'++'.$total_credit_point.'++'.$total_debit_balance.'++'.$total_encash_point.'++'.$total_encash_amount.'<br/>';
		$available_point=$total_credit_point-($total_encash_point+$total_debit_point);
		$available_balance=($total_credit_balance+$total_encash_amount)-$total_debit_balance;
		//echo $available_point.'##'.$available_balance;die;
		$update_people=array(
			'available_balance' => $available_balance,
			'available_points' => $available_point
		);
		$this->db->where('id',$user_id);
		$update_people=$this->db->update('people',$update_people);
		if($update_people)
		{
			$return=1;
		}
		else
		{
			$return=0;
		}
		return $return;
	}
//---------------------------------- End function to generate rand_number ----------------------------------------------------//
//------------------------------ Start function to get User's All Details ----------------------------------------//
	function UserDetail($user_id)
	{
		$data=array();
		$this->db->where('id',$user_id);
		$q=$this->db->get('people');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//--------------------------------- End function to get User's All Details --------------------------------------------//
//----------------------------------- Start function to get Per Point Value ------------------------------------------//
	function GetPerPointValue($type)
	{
		$data='';
		$this->db->where('type',$type);
		$this->db->where('host_name','Mortgage');
		$q=$this->db->get('reward_point');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data=$result->reward_percent;
			}
		}
		return $data;
	}
//--------------------------------------- End function to get Per Point Value ------------------------------------------//
//-------------------------------------------- Start function to Get User Total History -------------------------------------//
	function GetUserTotalHistory($user_id,$column_name)
	{
		$data='';
		$this->db->select_sum($column_name);
		$this->db->where('user_id',$user_id);
		$q=$this->db->get('user_point_history');
		//echo $this->db->last_query();die;
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data=$result->$column_name;
			}
		}
		return $data;
	}
//--------------------------------------------- End function to Get User Total History -------------------------------------//
//---------------------------------------- Start Function to get all the Other Loan details ----------------------------------------//
	function other_loans()
	{
		$data=array();
		$this->db->where('type','OL');
		$q=$this->db->get('application_form');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//---------------------------------------- End Function to get all the Other Loan details ----------------------------------------//
//-------------------------------------------- Start function to get Free Assessment Id ---------------------------------------//
	function GetFreeAssessmentId()
	{
		$data='';
		$this->db->where('type','F');
		$q=$this->db->get('application_form');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data=$result->id;
			}
		}
		return $data;
	}
//------------------------------------------------ End function to get Free Assessment Id ---------------------------------------//
//------------------------------------------ Start function to get the recent status ---------------------------------------//
	function GetStatus($email,$type)
	{
		$show_status='';
		$this->db->where('email',$email);
		$q=$this->db->get('people');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$user_id=$result->id;
			}
			
			if($type=='referral')
			{
				$this->db->where('user_id',$user_id);
				$this->db->where('payment_status','S');
				$this->db->order_by('pament_id','DESC');
				$get_value=$this->db->get('user_payment');
				if($get_value->num_rows>0)
				{
					$show_status='Joined and minimum transaction is done. You got $100 in your account.';
				}
				else
				{
					$show_status='Joined';
				}
			}
			if($type=='invite')
			{
				$this->db->where('user_id',$user_id);
				$this->db->where('status','S');
				$this->db->order_by('id','DESC');
				$get_value=$this->db->get('user_loan');
				if($get_value->num_rows>0)
				{
					$show_status='Joined and atleast one loan is settled.';
				}
				else
				{
					$show_status='Joined';
				}
			}
		}
		else
		{
			$show_status='Not joined';
		}
		return $show_status;
	}
//----------------------------------------------- End function to get the recent status -------------------------------------------//
//-------------------------------------- Start function to count User Loan -------------------------------------------//
	function UserLoanCount($user_id)
	{
		$this->db->where('user_id',$user_id);
		$q=$this->db->get('user_loan');
		return $q->num_rows();
	}
//---------------------------------------- End function to count User Loan -------------------------------------------//
//---------------------------------------- Start function to enter the point into user point history -------------------------------------//
	function GetUserPoint($user_email,$loan_id)
	{
		$parent_id='';
		$user_id=$this->session->userdata('userId');
		$referral_status=$this->GetUserReferralStatus($user_email);
		if($referral_status!='')
		{
			$explode_user_value=explode('###',$referral_status);
			if($explode_user_value[0]=='R')
			{
				$reason_for_type='Referral';
			}
			else
			{
				$reason_for_type='Invite';
			}
			$parent_id=$explode_user_value[1];
			//echo $parent_id;die;
			$this->db->where('user_id',$parent_id);
			$this->db->where('referral_id',$user_id);
			$this->db->where('reason_for',$reason_for_type);
			$get_value=$this->db->get('user_point_history');
			if($get_value->num_rows>0)
			{
				
				
			}
			else
			{
				$insert_user_point=array(
					'user_id' => $parent_id,
					'reason_for' => $reason_for_type,
					'credit_balance' => '100',
					'referral_id' => $user_id,
					'loan_id' => $loan_id,
					'host_name' => 'Mortgage',
					'add_date' => date('Y-m-d')
				);
				//echo '<pre>';print_r($insert_user_point);die;
				$insert=$this->db->insert('user_point_history',$insert_user_point);
				
			}
		}
		$update_user_point_balance=$this->model_common->UpadateUserPointBalance($parent_id);
		return true;
	}
//-------------------------------------------- End function to enter the point into user point history ----------------------------------------//
	function GetUserReferralStatus($user_email)
	{
		$user_status='';
		$this->db->where('ref_email',$user_email);
		$q=$this->db->get('user_referrals');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$user_status=$result->status.'###'.$result->user_id;
			}
			
		}
		return $user_status;
	}
//---------------------------- Start funcion to get All Loans ----------------------------------------//
	function GetAllLoan()
	{
		$data=array();
		$this->db->where('type !=','F');
		$q=$this->db->get('application_form');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//-------------------------------- End function to get All Loans ----------------------------------------//
//-------------------- Start function to check wheather the user exist in point table ---------------------------//
	function GetUserExistanceInUserPoint($user_id)
	{
		$this->db->where('referral_id',$user_id);
		$q=$this->db->get('user_point_history');
		return $q->num_rows;
	}
//---------------------- End function to check wheather the user exist in point table ----------------------------//
//---------------------------- Start function to get Other Loan Details ----------------------------------------//
	function GetOtherLoanList()
	{
		$data=array();
		$this->db->where('type','OL');
		$q=$this->db->get('application_form');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
	
	function get_rate($application_id)
	{
	  $data =array();
	  $sql = "SELECT rate,loan_type,term from interest_rate where application_id='".$application_id."'";
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0)
		{
			foreach($query->result() as $result)
			{
				$data[]=$result;
			}
		
		}
		return $data;
	}
	
	function application_fromname($application_id)
	{
	  $data =array();
	   $sql = "SELECT name from application_form where id='".$application_id."'"; 
	   $query = $this->db->query($sql);
	   if($query->num_rows()>0)
		{
			foreach($query->result() as $result)
			{
				$data[]=$result;
			}
		
		}
		return $data;
	}
	
//-------------------------------- End function to get Other Loan Details ----------------------------------------//
//-------------------------- Start function to get Admin Id ----------------------------------//
	function GetAdminID()
	{
		$admin_id='';
		$this->db->where("type = 'SA'");
		$q=$this->db->get('admin');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$admin_id=$result->id;
			}
		}
		return $admin_id;
	}
//----------------------------- End function to get Admin Id ----------------------------------//
//----------------------------- Start function to get total product list ---------------------------------//
	function GetAllProductsCountData()
	{
		$data=array();
		$this->db->where('status','1');
		$this->db->where("stock > '0'");
		$q=$this->db->get('product');
		return $q->num_rows;
	}
//---------------------------- End function to get total product list ----------------------------------//
//----------------------------- Start function to get all product list details ---------------------------------//
	function GetAllProducts($limit,$start)
	{
		$data=array();
		$this->db->where('status','1');
		$this->db->where("stock > '0'");
		$this->db->limit($limit,$start);
		$q=$this->db->get('product');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//---------------------------- End funciton to get all product list details ----------------------------------//
//------------------------- Start function to update product count -----------------------------------//
	function UpdateProductCount($product_id)
	{
		$this->db->where('product_id',$product_id);
		$q=$this->db->get('product');
		//echo $this->db->last_query().'<br/>';
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$product_count=$result->stock;
			}
		}
		$int = (int)$product_count;
		$update_product_count=$product_count-1;
		//echo $update_product_count.'<br/>';
		$update_value=array(
			'stock' => $update_product_count
		);
		$this->db->where('product_id',$product_id);
		$updated=$this->db->update('product',$update_value);
		//echo $this->db->last_query().'<br/>';die;
		if($updated)
		{
			$return=1;
		}
		else
		{
			$return=0;
		}
		return $return;
	}
//---------------------------- End function to update product count ------------------------------------//


function get_rest_pages($page_id,$position)
    {
		$this->db->select('*');
		$this->db->from('cms_content');
		$this->db->where("menu_id",$page_id);
		$this->db->where("position",$position);
		$this->db->where("status",'1');
		$query=$this->db->get(); 
		return $query->result();
    }
//----------------------------- Start function to send mail after apply loan ------------------------------------//
	function send_mail_after_submit_loan($loan_id)
	{
		$details='';
		$this->db->select('people.first_name,people.last_name,people.email,people.phone_no,people.address,application_form.name,application_form.type,user_loan.*');
		$this->db->from('people');
		$this->db->from('user_loan');
		$this->db->from('application_form');
		$this->db->where('user_loan.id',$loan_id);
		$this->db->where('user_loan.user_id = people.id');
		$this->db->where('user_loan.application_id = application_form.id');
		$q=$this->db->get();
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data['username']=$result->first_name.' '.$result->last_name;
				$message_body='';
				$message_body.='<h3>Hello Admin,</h3>
				<p style="margin-bottom: 30px;">'.$data['username'].' apply for loan on '.date('d-m-Y').'. His apply loan details is given below.</p>
				<h3>Loan Details</h3>
				<table style="margin-left:-10px;">
				<tr><th style="width: 50%;">Loan for</th><td style="width: 50%;">'.$result->name.'</td></tr>
				<tr><th style="width: 50%;">I am looking to</th><td style="width: 50%;">'.$result->our_needs.'</td></tr>
				<tr><th style="width: 50%;">Estimate price</th><td style="width: 50%;">'.$result->estimated_property_price.'</td></tr>
				<tr><th style="width: 50%;">Loan Amount</th><td style="width: 50%;">'.$result->loan_amount.'</td></tr>';
				if($result->amount_on_prop!='')
				{
					$message_body.='<tr><th style="width: 50%;">Amount you owe on the property</th><td style="width: 50%;">'.$result->amount_on_prop.'</td></tr>';
				}
				if($result->postcode_property!='')
				{
					$message_body.='<tr><th style="width: 50%;">Suburb or postcode of the property</th><td style="width: 50%;">'.$result->postcode_property.'</td></tr>';
				}
				if($result->own_property!='')
				{
					$message_body.='<tr><th style="width: 50%;">Do you already own a property?</th><td style="width: 50%;">'.$result->own_property.'</td></tr>';
				}
				if($result->first_home_buyer!='')
				{
					$message_body.='<tr><th style="width: 50%;">Are you a first home buyer?</th><td style="width: 50%;">'.$result->first_home_buyer.'</td></tr>';
				}
				if($result->savings_fund_property!='')
				{
					$message_body.='<tr><th style="width: 50%;">Do you have any savings to help fund the property?</th><td style="width: 50%;">'.$result->savings_fund_property.'</td></tr>';
				}
				if($result->saving_available!='')
				{
					$message_body.='<tr><th style="width: 50%;">How much savings do you have available?</th><td style="width: 50%;">'.$result->saving_available.'</td></tr>';
				}
				if($result->guarantor_loan!='')
				{
					$message_body.='<tr><th style="width: 50%;">Are you looking for 100% guarantor loan?</th><td style="width: 50%;">'.$result->guarantor_loan.'</td></tr>';
				}
				if($result->employment!='')
				{
					$message_body.='<tr><th style="width: 50%;">Employment</th><td style="width: 50%;">'.$result->employment.'</td></tr>';
				}
				if($result->provide_documents!='')
				{
					$message_body.='<tr><th style="width: 50%;">Which of these documents can you provide?</th><td style="width: 50%;">'.$result->provide_documents.'</td></tr>';
				}
				if($result->clear_credit_history!='')
				{
					$message_body.='<tr><th style="width: 50%;">Do you have a clear credit history?</th><td style="width: 50%;">'.$result->clear_credit_history.'</td></tr>';
				}
				if($result->soon_need_loan!='')
				{
					$message_body.='<tr><th style="width: 50%;">How soon do you need a loan?</th><td style="width: 50%;">'.$result->soon_need_loan.'</td></tr>';
				}
				if($result->email!='')
				{
					$message_body.='<tr><th style="width: 50%;">Email</th><td style="width: 50%;">'.$result->email.'</td></tr>';
				}
				if($result->phone_no!='')
				{
					$message_body.='<tr><th style="width: 50%;">Phone Number</th><td style="width: 50%;">'.$result->phone_no.'</td></tr>';
				}
				if($result->address!='')
				{
					$message_body.='<tr><th style="width: 50%;">Address</th><td style="width: 50%;">'.$result->address.'</td></tr>';
				}
				if($result->best_time!='')
				{
					$message_body.='<tr><th style="width: 50%;">The best time to call</th><td style="width: 50%;">'.$result->best_time.'</td></tr>';
				}
				$message_body.='<tr><th style="width: 50%;">Comment</th><td style="width: 50%;">'.$result->comment.'</td></tr></table><br/><br/>';
				$from=ADMIN_EMAIL;
				$type='ADMIN';
				$to=ADMIN_EMAIL;
				$subject='Apply Loan Details';
				$send_mail=$this->model_common->send_email($from,$type,$to,$subject,$message_body);
//---------------------------------------	End section to send mail to Admin after submit loan -------------------------------------------------//
//-------------------------------------- Start section to send mail to user after submit loan --------------------------------------------------//
				/*$user_data['username']=$result->first_name.' '.$result->last_name;;
				$user_data['details']=$result->name;
				$user_message_body='';
				$user_message_body.='<h3>Hello '.$user_data['username'].',</h3>
				<p style="margin-bottom: 30px;">Thank you for your application for loan.<br/>We are checking your details and will get in touch with you after complete the whole process.<br/>All the best with your '.$user_data['details'].'!</p>';
				$user_from=ADMIN_EMAIL;
				$user_type='ADMIN';
				$user_to=$result->email;
				$user_subject=SITE_NAME.' received your loan.';
				$user_send_mail=$this->model_common->send_email($user_from,$user_type,$user_to,$user_subject,$user_message_body);*/
//----------------------------------------- End section to send mail to user after submit loan --------------------------------------------------//
			}
		}
		return 1;
	}
//------------------------------- End function to send mail after applu loan -----------------------------------//
//-------------------------- Start function to get user payment status ------------------------------------//
	function GetUserPaymentStatus($loan_id)
	{
		$payment_id='';
		$payment_status='';
		$user_id=$this->session->userdata('userId');
		$this->db->where('loan_id',$loan_id);
		$this->db->where('user_id',$user_id);
		$this->db->where("payment_status != 'S'");
		$q=$this->db->get('user_payment');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				//$payment_id=$result->pament_id;
				if($result->payment_status=='F')
				{
					$payment_status=$result->payment_status;
				}
				else
				{
					$payment_status=$result->payment_status.'###'.$result->pament_id;
				}
			}
		}
		return $payment_status;
	}
//---------------------------- End function to get user payment status-------------------------------------//
//------------------------------ Start function to update loan details -------------------------------------//
	function update_loan_details($transaction_id,$payment_id,$response_code)
	{
		if($response_code=='00')
		{
			$payment_status='S';
		}
		else
		{
			$payment_status='F';
		}
		$update_payment=array(
			'transaction_id' => $transaction_id,
			'payment_status' => $payment_status,
		);
		$this->db->where('pament_id',$payment_id);
		$update=$this->db->update('user_payment',$update_payment);
		if($payment_status=='S')
		{
			$this->db->where('pament_id',$payment_id);
			$payment_details=$this->db->get('user_payment');
			if($payment_details->num_rows>0)
			{
				foreach($payment_details->result() as $payment_result)
				{
					$get_loan_amount=$payment_result->amount;
					$loan_id=$payment_result->loan_id;
				}
				
				$get_reward_point=$this->GetRewardPoint('L');
				$calculate_credit_point=($get_loan_amount*$get_reward_point)/100;
//---------------------- Start section to insert user point history ---------------------//
				$insert_user_history=array(
					'user_id' => $this->session->userdata('userId'),
					'reason_for' => 'Transaction',
					'credit_point' => $calculate_credit_point,
					'host_name' => 'Mortgage',
					'add_date' => date('Y-m-d')
				);
				$insert_user_point_history=$this->db->insert('user_point_history',$insert_user_history);
//---------------------- End section to insert user point history ---------------------//
				$update_user_point_balance=$this->UpadateUserPointBalance($this->session->userdata('userId'));
				$user_count_loan=$this->GetUserExistanceInUserPoint($this->session->userdata('userId'));
				if($user_count_loan==0)
				{
					$user_email=$this->UserDetail($this->session->userdata('userId'));
					$this->GetUserPoint($user_email[0]->email,$loan_id);
				}	
				$model_to_send_mail=$this->send_mail_after_submit_loan($loan_id);	
			}
		}
		if($update)
		{
			$this->session->set_userdata('succ_msg','Your payment successfully completed.');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('err_msg','Sorry! There is an error to update payment.');
			$return=0;
		}
		return $return;
	}
//------------------------------ End function to update loan details -----------------------------------------//

//------------------------- Start function to get banner ---------------------------//
	function get_active_banner()
    {
		$this->db->where("status",'1');
		$this->db->limit(1, 0);
		$query=$this->db->get('banner');
		if($query->num_rows>0)
		{
			foreach($query->result() as $result)
			{
				$banner_image=$result->image;
			}
		} 
		return $banner_image;
    }
//---------------------------- End function to get banner ------------------------------//

//---------------------------- Start function to get About Mortgage details -----------------------------//
	function about_details($page_id,$subpage_id)
    {
		$this->db->where("menu_id",$page_id);
		$this->db->where("submenu_id",$subpage_id);
		$query=$this->db->get('cms_content');
		//echo $this->db->last_query();
		if($query->num_rows>0)
		{
			foreach($query->result() as $result)
			{
				$data=$result->long_description;
			}
		} 
		return $data;
    }
//------------------------------- End function to get About Mortgage details -------------------------------//

//-------------------------- Start function to send email ----------------------------------------------//
	function send_email($from,$type,$to,$subject,$message_body)
	{
		$banner=$this->get_active_banner();
		$about_mortgage=$this->about_details('5','49');
		$data['body']=$message_body;
		$data['about_mortgage']=$about_mortgage;
		$data['banner_image']=$banner;
		//print_r($data);die;
		$view_page=$this->load->view('email_template',$data,TRUE);
		/*echo $from.'<br/>';
		echo $to.'<br/>';
		echo $subject.'<br/>';*/
		//echo $view_page;die;
		$this->load->library('email');

		$this->email->from($from,$type);
		$this->email->to($to); 
		
		$this->email->subject($subject);
		$this->email->message($view_page);
		if(!$this->email->send())
		{
			$return=0;
		}
		else
		{
			$return=1;
		}
		return $return;
	}
//----------------------------- End function to send email -----------------------------------------------//
//-------------------------------- Start function to get all coupon details ----------------------------------//
	function GetAllCardDetails()
	{
		$data=array();
		$this->db->where('status !=','D');
		$get=$this->db->get('card_details');
		if($get->num_rows>0)
		{
			foreach($get->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------------------------- End function to get all coupon details -------------------------------------//
 } //end