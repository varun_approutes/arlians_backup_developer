<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_message extends CI_model
{
 
function __construct(){

		parent::__construct();
	}

function recive($mid){
	$data=array();
			$sql = "select m.*,mt.*,p.image_url,ar.fname,ar.lname,ar.email,req.bussinessname from ar_message as m left join ar_message_trans as mt on m.msg_id=mt.msg_id left join ar_members ar ON ar.mid = mt.sender_id LEFT JOIN ar_request_access_page req ON req.mid = mt.sender_id left join member_image p on p.mid=mt.sender_id and p.type='profile_img' where mt.receiver_id = ".$mid." and  mt.receiver_delete ='0' and mt.sender_id!=0 and ar.status='1' group by m.msg_sub ORDER BY m.msg_id DESC ";
			
			
			//echo $mid;
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				$i=0;
			foreach ($rs as $result) 
			{	
				$data[$i]=$result;
				
					$em = logobussinessname($result["sender_id"]);
					$data[$i]['logobussinessname'] = $em;
					$data[$i]['status'] = 'Received';
				$data[$i]['base_url'] = base_url().'uploads/profile_image/';
				$data[$i]['msgdate'] = date("d/m/Y \A\T H:i:s a",strtotime($result["msg_time"]));
				$data[$i]['posttime'] = date("d/m/Y \A\T H:i:s a",strtotime($result["msg_time"]));
				$i++;
			}
			
		}
		//var_dump( $data);
		
		return $data;
}

function sent($mid){
	$data=array();
			$sql = "select m.*,mt.*,p.image_url,ar.fname,ar.lname,ar.email,req.bussinessname from ar_message as m  left join ar_message_trans as mt on m.msg_id=mt.msg_id left join ar_members ar ON ar.mid = mt.receiver_id LEFT JOIN ar_request_access_page req ON req.mid = mt.receiver_id left join member_image p on p.mid=mt.receiver_id and p.type='profile_img' where mt.sender_id = ".$mid." and  mt.sender_delete ='0' and mt.receiver_id!=0 and ar.status='1' group by m.msg_sub ORDER BY m.msg_id DESC";
			//echo $sql;die;
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				$i=0;
				foreach ($rs as $result) 
				{	
					$data[$i]=$result;
					$em = logobussinessname($result["receiver_id"]);
					$data[$i]['logobussinessname'] = $em;
					$data[$i]['base_url'] = base_url().'uploads/profile_image/';
					$data[$i]['msgdate'] = date("d/m/Y \A\T H:i:s a",strtotime($result["msg_time"]));
					$data[$i]['status'] = 'Sent';
					$data[$i]['posttime'] = date("d/m/Y \A\T H:i:s a",strtotime($result["msg_time"]));
					
					$i++;
				}
			}
		return $data;
}
function inbox($mid){
	$recive=$this->recive($mid);
	$sent=$this->sent($mid);
	
	$all=array_merge($recive,$sent);

	foreach ($all as $key => $row) {
		$msgdate[$key]  = $row['msgdate'];
		
	}
	//array_multisort($msgdate, SORT_DESC, $all);
	
	return $all;
}

function Fninbox($mid)
{
  $sql = "select m.*,mt.*,p.image_url,ar.fname,ar.lname,ar.email,req.bussinessname from ar_message as m  left join ar_message_trans as mt on m.msg_id=mt.msg_id left join ar_members ar ON ar.mid = mt.receiver_id LEFT  JOIN ar_request_access_page req ON req.mid = mt.receiver_id left join member_image p on p.mid=mt.receiver_id and p.type='profile_img' where (mt.sender_id = '".$mid."' or mt.receiver_id='".$mid."') and  mt.sender_delete ='0' and mt.receiver_id!=0 and ar.status='1' group by m.msg_sub  ORDER BY m.msg_id DESC"; 
 $query = $this->db->query($sql); 
			$rs = $query->result_array();    $data=array();
	if(count($rs) > 0){
		$i=0;
		foreach ($rs as $result) 
		{	
		    if($result["receiver_id"]==$mid)
			{
				$sent_id=$result["sender_id"];
				$data[$i]['status'] = 'Received';
			}
			else
			{
				$sent_id=$result["receiver_id"];
				$data[$i]['status'] = 'Sent';
			}
			$data[$i]=$result;
			$em = logobussinessname($sent_id);
			$userProfType=userProfType($sent_id);
			if($userProfType=='Buisness'){
				$data[$i]['inboxname']=getMemberbusinName($sent_id);
			}else{
				
				$data[$i]['inboxname']=getMemberName($sent_id);
			}
			$data[$i]['logobussinessname'] = $em;
			$data[$i]['inboxid'] = $sent_id;
			$data[$i]['base_url'] = base_url().'uploads/profile_image/';
			$data[$i]['msgdate'] = date("d/m/Y \A\T H:i:s a",strtotime($result["msg_time"]));
			
			$data[$i]['posttime'] = date("d/m/Y \A\T H:i:s a",strtotime($result["msg_time"]));
			
			$i++;
		}
	}
return $data;
}


function inbox_editpage($mid){
	$recive=$this->recive($mid);
	$sent=$this->sent($mid);
	
	$all=array_merge($recive,$sent);
    $i=0;
	foreach ($all as $key => $row) {
		if($i<5)
		{
			$msgdate[$key]  = $row['msgdate'];
		}
		
		
		
	}
	array_multisort($msgdate, SORT_DESC, $all);
	
	return $all;
}
function create_msg($from,$to_list,$sub,$body){
	
	$insert_array_msg=array(
				'msg_sub' => $this->input->post('title'),
				'msg_body' => htmlentities($this->input->post('content')),	
			);
			$insert=$this->db->insert('ar_message',$insert_array_msg);
			$msg_id = $this->db->insert_id();
			$insert_array_msg_tr=array(
				'sender_id' => $this->input->post('title'),
				'receiver_id' => htmlentities($this->input->post('content')),	
				'msg_id' => $msg_id,	
				'msg_read' => 0,	
			);
			$insert=$this->db->insert('ar_message',$insert_array_msg);
			$msg_id = $this->db->insert_id();

			if($insert)
			{

				$this->session->set_userdata('success_msg','Data insert successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to insert data.Please try again.');
				$return=0;
			}
		
		return $return;
	
}

function insertMessage($data){
	$this->db->insert('ar_message', $data); 
	$insert_id = $this->db->insert_id();
	return  $insert_id;
}

function getId($e){
	$sql = "select ar_members.mid from ar_members left join ar_request_access_page on ar_members.mid=ar_request_access_page.mid where CONCAT(ar_members.fname,' ',ar_members.lname)='".$e."' 
or ar_members.email='".$e."' or ar_request_access_page.bussinessname='".$e."'";
$query = $this->db->query($sql);
$res = $query->result_array();	
return $res;	
			
	/* $this->db->select('mid');
	$this->db->from('ar_request_access_page');
	$this->db->where('bussinessname', $e); 	
	$query = $this->db->get();
	$res = $query->result_array();	
	return $res; */
}

function deleteMessage($message_id,$user_id,$message_status){
	//if($message_status=='Sent'){		
		$data['sender_delete'] = '1';
	//}else{
		$data['receiver_delete'] = '1';
	//}	
	$this->db->where('msg_id', $message_id);
	$this->db->update('ar_message_trans', $data);
	return true;
}

function message_count_q($id,$type=null){
	
	if($type!='sent' && $type!='dash'){
		$this->db->where('receiver_id',$id);
		$this->db->where('msg_read','0');
		$num_rows = $this->db->count_all_results('ar_message_trans');
		
	}elseif($type=='dash'){
		$this->db->where('receiver_id',$id);
		$num_rows = $this->db->count_all_results('ar_message_trans');
		
	}else{
		$this->db->where('sender_id',$id);
		$num_rows = $this->db->count_all_results('ar_message_trans');		
	}
	//echo $num_rows;die();
	return $num_rows;	
}
	function requestConnectionSeen($mt_id){
		$data['msg_read'] = '1';
		$this->db->where('mt_id', $mt_id);
		$this->db->update('ar_message_trans', $data);
		return true;
	}
	
	function getUserById($mid) {
		if($mid!=''){
			$sql = "SELECT * FROM ar_members  WHERE ar_members.mid=" . $mid;
			$query = $this->db->query($sql);
			$rs = $query->row();
			//print_r($rs);
			return $rs;
		}
    }
	
	
	function FnGetMessageDetails($message_id,$user_id,$message_subject,$inboxid,$session_user_id)
	{
		$data=array();
		$sql="SELECT * FROM (`ar_message` as message) INNER JOIN `ar_message_trans` as tran ON `tran`.`msg_id`=`message`.`msg_id` WHERE `message`.`msg_sub` =  '".$message_subject."' AND (`tran`.`sender_id` =  '".$inboxid."' OR `tran`.`receiver_id` =  '".$inboxid."') ORDER BY `message`.`msg_id` DESC ";
		$query = $this->db->query($sql);
		$data=array();
		$data['0']['body_content']='';
		if($query->num_rows>=1)
		{
			$record=$query->result_array();
			//$data['0']['subject']=$record['msg_sub'];
			
			//print_r($record);die();
			$i=0;
			foreach($record as $testarr)
			{
				$styleclass='';
				if($i>0) 
				{
					$styleclass='style="background:  none ;"';
				}
				if($testarr['msg_type']!='reply')
				{
					$data['0']['body_content'] .='<div class="col-sm-12">                
                      <div class="message_box" '.$styleclass.'>
                        <span class="msg_send">Message sent '.$testarr['msg_time'].'</span>
                        '.$testarr['msg_body'].'
                      </div>
                      </div> ';
				} 
				else
				{
					$data['0']['body_content'] .='<div class="col-sm-11 col-sm-offset-1"> 
                      <div class="message_box1" '.$styleclass.'>
                        <span class="msg_send">Message sent '.$testarr['msg_time'].'</span>
                         '.$testarr['msg_body'].'
                      </div>
                      </div>';
				}
				$i++;
			}
			$receiverid=$record['receiver_id'];
			$sender_id=$record['sender_id'];
			$emailnowsent=$sender_id;
			if($sender_id==$user_id)
			{
				$emailnowsent=$receiverid;
			}
			$em = getMemberbusinName($emailnowsent);
			$data['0']['replybusinessname']=$em;
			$emailaddressdet=$this->getUserById($emailnowsent);
			//print_r($emailaddressdet);die();
			$data['0']['replyemailaddress']=$emailaddressdet->email;
			$data['0']['replyemaildate']=$record['msg_time'];
			
		}
		return $data;
		
		
	}
	
	
	
	
} //end