<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_payment extends CI_model
{
 
	function __construct(){
		parent::__construct();			 
	}
	function insert_transaction($data){
		$result = $this->db->insert('ar_transaction',$data);
		return $result;
	}

	function getUserSignUpDate($id){

		$this->db->select('update_date');
		$this->db->from('ar_members');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}

	function paymentType($mid){
		
		
    	$this->db->from('ar_transaction');
    	$this->db->where('mid', $mid);
		$this->db->order_by('mid desc'); 
		$this->db->limit(1,0); 
        $query = $this->db->get();
		$res = $query->row();
		return $res->subscription_type;
	}

 } //end
