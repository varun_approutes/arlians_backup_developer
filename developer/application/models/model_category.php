<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_category extends CI_model
{
 
function __construct(){

		parent::__construct();
	}

function top_link_list(){

	$data=array();
			$sql = "select 	* from ar_top_lines";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			
			
			if(count($rs) > 0)
			{
				$i = 0;
				foreach ($rs as $result) 
			{	
				$rs[$i++]['path'] = base_url().'uploads/profile_image/'; 
				
			}
				
				
				foreach ($rs as $result) 
			{	
				$data[]=$result;
				
			}
			
		}
		return $data;
}

function ar_sector_list($top_link_list_id=null){
	$data=array();
			$sql = "select 	distinct(ar_sector.sector_name),ar_sector.sector_code,ar_sector.profile_image from ar_sector";
			if($top_link_list_id!=null){
			$sql .= " left join ar_relation on ar_sector.sector_code = ar_relation.sector_code where ar_relation.top_code=".$top_link_list_id;
			}
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		$json_response = json_encode($data);
		echo $json_response;
		exit;
}

function ar_subsector_list($sector_id=null,$top_link_list_id=null){
	$data=array();
			$sql = "select 	distinct(ar_subsector.subsector_name),ar_subsector.subsector_code from ar_subsector";
			if($sector_id!=null){
			$sql .= " left join ar_relation on ar_subsector.subsector_code = ar_relation.subsector_code where ar_relation.sector_code=".$sector_id;
			$sql .= " and  ar_relation.top_code=".$top_link_list_id;
			}
			
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		$json_response = json_encode($data);
		echo $json_response;
		exit;
}
function subsectorListByUser($sector_id, $user_id){
	
	
			
			$sql = "select * from ar_member_data_wiz_one  where mem_code REGEXP '".$sector_id."' and mid=".$user_id;
			
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				$i=0;
				foreach ($rs as $result) 
			{	
				$data[$i]=$result;
				$ans_set_cat_id=explode('-',$result['ans_set_cat_id']);
				$data[$i]['topline_name']=topline_name($ans_set_cat_id[0])['top_name'];
				$data[$i]['sector_name']=sector_name($ans_set_cat_id[1])['sector_name'];
				$data[$i]['sub_sector_name']=sub_sector_name($ans_set_cat_id[2])['subsector_name'];
				$data[$i]['bussiness_name']=bussiness_name($ans_set_cat_id[3])['bussiness_name'];
				$i++;
			}
		}
		$json_response = json_encode($data);
		echo $json_response;
		exit;
	
}
function bussiness_type_list($subsector_code=null,$sector_id=null,$top_link_list_id=null){
	 
	 $data=array();
			$sql = "select 	distinct(ar_bussiness.bussiness_name),ar_bussiness.bussiness_code from ar_bussiness";
			if($subsector_code!=null){
			$sql .= " left join ar_relation on ar_bussiness.bussiness_code = ar_relation.bussiness_code where ar_relation.subsector_code=".$subsector_code;
			$sql .= " and  ar_relation.top_code=".$top_link_list_id." and ar_relation.sector_code=".$sector_id;
			}
			$sql .= " order by ar_bussiness.bussiness_name";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		$json_response = json_encode($data);
		echo $json_response;
		exit;
 }

/*function ar_subsector_list($sector_id=null){
	$data=array();
			$sql = "select 	distinct(ar_subsector.subsector_name),ar_subsector.subsector_code from ar_subsector";
			if($sector_id!=null){
			$sql .= " left join ar_relation on ar_subsector.subsector_code = ar_relation.subsector_code where ar_relation.sector_code=".$sector_id;
			}
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		$json_response = json_encode($data);
		echo $json_response;
		exit;
}*/
 /*function bussiness_type_list($subsector_code=null){
	 
	 $data=array();
			$sql = "select 	distinct(ar_bussiness.bussiness_name),ar_bussiness.bussiness_code from ar_bussiness";
			if($subsector_code!=null){
			$sql .= " left join ar_relation on ar_bussiness.bussiness_code = ar_relation.bussiness_code where ar_relation.subsector_code=".$subsector_code;
			}
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		$json_response = json_encode($data);
		echo $json_response;
		exit;
	 
	 
 }*/

   function is_member_active($mid){
	  		
			$sql = "select 	* from ar_members where status='1' and mid=".$mid;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				return 'true';
			}else{
				return 'false';
			}
	 
 }
 
 function getBussinessCode(){
	  
		$sql = 'SELECT * FROM ar_bussiness';
		$query = $this->db->query($sql);
		$rs = $query->result_array();
			
	 return $query;
 }

 function insert_wiz_one_p_two($d)
 {	 
	$mid=$d['mid'];
	$code=$d['mem_code'];
	//$is_already = $this->isThisCodeAlredyexistinwizone($code,$mid);
	//if($is_already=='false'){
			$this->db->insert('ar_member_data_wiz_one', $d); 
			$lid=$this->db->insert_id();
			if($lid)
			{
				/*$active=$this->is_member_active($mid);
				if($active=='true'){
					return 'active';
				}else{
					return 'inactive';
				} */
				return 'true';
			}
			else
			{
				return 'false';
			}
	 
 }
 
 function chkTwentyDigitCode($d)
 {
	$mid=$d['mid'];
	$code=$d['mem_code'];
	
	$this->db->select('*');
	$this->db->from('ar_member_data_wiz_one');
	$this->db->where('mid',$mid);
	$this->db->where('mem_code',$code);
	$query = $this->db->get();
	
		if ($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	
 }
 function looking_for(){
	 $sql = "select * from ar_looking_for";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$data[]=$result;
				}
			}
		$json_response = json_encode($data);
		echo $json_response;
		exit;
	 
 }
 function locality(){
	  $sql = "select * from ar_locality";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$data[]=$result;
				}
			}
		$json_response = json_encode($data);
		echo $json_response;
		exit;
 }
 

 
 function is_service(){
	  $sql = "select * from ar_is_service";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$data[]=$result;
				}
			}
		return $data;
	 
 }
 function industry_list(){
	 
	  $sql = "select * from ar_industry";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$data[]=$result;
				}
			}
			return $data;
 }
 function target_sector_list($industry_code=null){
	 
	$data=array();
			$sql = "select 	distinct(ar_target_sector.tar_sector_name),ar_target_sector.tar_sector_code,ar_target_sector.profile_image from ar_target_sector";
			if($industry_code!=null){
			$sql .= " left join ar_target_relation on ar_target_sector.tar_sector_code = ar_target_relation.tar_sector_code where ar_target_relation.industry_code=".$industry_code;
			}
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
 }
 
  function looking_services_list(){
 
	  $sql = "select * from ar_looking_services";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$data[]=$result;
				}
			}
			return $data;
 }
 
  function country_name($id){
if($id!=''){
	 $sql = "select * from countries where id=".$id;

	 $query = $this->db->query($sql);

	 $rs = $query->result_array();
	
	 return $rs[0]['country_name'];
}else{
	return 'Please insert country name';
}

 }
 
 function bussiness_name($code){
if($code!=''){
	  $sql = "select * from ar_bussiness where bussiness_code=".$code;

	 $query = $this->db->query($sql);

	 $rs = $query->result_array();

	 return $rs[0]['bussiness_name'];
}else{
	return 'Please insert business name';
}
	 

 }
 function topline_name($code){
if($code!=''){
	  $sql = "select * from ar_top_lines where top_code=".$code;

	 $query = $this->db->query($sql);

	 $rs = $query->result_array();

	 return $rs[0]['top_name'];
}else{
	return 'Please insert Industry name';
}
 } 
 function watsonKeyWord($code){
	if($code!=''){
		  $sql = "select * from alchemy_keywords_relation left join alchemy_keywords on alchemy_keywords_relation.watson_code=alchemy_keywords.code  WHERE alchemy_keywords_relation.arlians_code ='".$code."'";
		 $query = $this->db->query($sql);
		 $rs = $query->result_array();
		 //$arr_two_level=array();
		foreach ($rs as $watsoncode){
			 $arr_two_level[]=$watsoncode["watson_code"];
			
		 }
		 if(!empty($arr_two_level))
		 $arr_two_level= array_unique($arr_two_level);

		 return $arr_two_level;
	}else{
		return 'Please insert Industry name';
	}	 
 
 }
 function topline_alchemyName($code){
	if($code!=''){
		$sql = "select * from  alchemy_keywords WHERE code ='".$code."'";
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		return $rs[0]['keyword'];
	}else{
		return 'Please insert Industry name';
	}
 }
	function isRecordAlchemyDataExit($data=''){
		$sql = "select 	jsondata from ar_apinews where on_date='".$data['on_date']."' and mid=".$data['mid'];
		$query = $this->db->query($sql);
		$rs = $query->row();
		return $rs;
		/*if ($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}*/
	}
	function updateAlchemyNewsRespectToUser($data){
		$flag = false;
		$sql = "select al_id from ar_apinews where mid=".$data['mid'];
		$query = $this->db->query($sql);
		$rs = $query->row();
		
		if(!empty($rs)){
			$this->db->where('mid',$data['mid']);				
			$this->db->update('ar_apinews',$data); 
		}else{
			$this->db->insert('ar_apinews',$data);
		}
		if ($this->db->affected_rows() >= 0) {
			$flag = true; // your code
		}
		return $flag; // your code
		
	}

	function insertBingNewsRespectToSubsector($data){

		$this->db->insert('ar_apinews',$data);
		$flag;
			if ($this->db->affected_rows() >= 0) {
			$flag = true; // your code
			}
		return $flag; // your code
		
	}

	function updateBingNewsRespectToSubsector($data)
	{
		$this->db->where('al_id', $data['al_id']);
		$this->db->update('ar_apinews', $data); 
		$flag;
			if ($this->db->affected_rows() >= 0) {
				$flag = true; // your code
			}
		return $flag; // your code
	}
 
  function sector_name($code){
	if($code!=''){
		$sql = "select * from ar_sector where sector_code=".$code;
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		return $rs[0]['sector_name'];
	}else{
		return 'Please insert sector name';
	}
 }
 function sub_sector_name($code){
	if($code!=''){
		  $sql = "select * from ar_subsector where subsector_code=".$code;
		 $query = $this->db->query($sql);
		 $rs = $query->result_array();
		 return $rs[0]['subsector_name'];
	}else{
		return 'Please insert subsector  name';
	}
 }

  function get_bing_str($code){
	if($code!=''){
		  $sql = "select * from ar_subsector where subsector_code=".$code;
		 $query = $this->db->query($sql);
		 $rs = $query->result_array();
		 return $rs[0]['bing_query_str'];
	}else{
		return 'Please insert subsector  name';
	}
 }

  function fetch_sub_sector_name()
	{
		$sql = "SELECT `subsector_name` FROM `ar_subsector`";
		$query = $this->db->query($sql);
		$rs = $query->result_array();

		return $rs;
	}



 function insert_wiz_two($d){	 
	$mid=$d['mid'];
	$this->db->insert('ar_member_data_wiz_two', $d); 
	$lid=$this->db->insert_id();
	if($lid){
	return 'true';
		
	}else{
		return 'false';
	}
	 
 }
 function isCodeExist($mid){
	 $sql="select wiz_two_id from ar_member_data_wiz_two where mid=".$mid;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
				return 'true';
			} else{
			return 'false';
		}
	 
 }
 function wiz_two_looking_industry($d)
 {	 
	$mid=$d['mid'];
	$code1=$d['industry_code'];
	$code2=$d['target_sector_code'];
	//foreach()
	//{
		$is_already_exist = $this->isThisCodeAlredyexistinwiztwo($code1,$code2,$mid);
		if($is_already_exist=='false')
		{
			$this->db->insert('ar_wiz_two_looking_industry', $d); 
			$lid=$this->db->insert_id();
			if($lid)
			{
				return 'true';
				
			}
			else
			{
				return 'false';
			}
		}
		else
		{
			return 'exist';
		}
//	}
	 
 }
 	function isThisCodeAlredyexistinwizone($code,$mid){
		$sql = "SELECT * FROM ar_member_data_wiz_one WHERE mid='".$mid."' and mem_code='".$code."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
				return 'true';
			} else{
			return 'false';
		}
		
	}

	function isThisCodeAlredyexistinwiztwo($data)
	{
		$sql = "SELECT * FROM ar_wiz_two_looking_industry WHERE mid='".$data['mid']."' and industry_code='".$data['code1']."' and target_sector_code='".$data['code2']."'";

		$query = $this->db->query($sql);
		if ($query->num_rows() > 0)
		{
			return 'true';
		} 
		else
		{
			return 'false';
		}
	
	}
	
	 function get_topline_image($code){
		 
		if($code!=''){
			  $sql = "select * from ar_top_lines where top_code=".$code;

			 $query = $this->db->query($sql);

			 $rs = $query->result_array();

			 return $rs;
		} else{
			return false;
		}
	}
	
	function getUserOwnIndusNo($mid){
		$sql="select count(*) as code_Count from ar_member_data_wiz_one where mid=".$mid;
		$query = $this->db->query($sql);
		 $rs = $query->row();
		return $rs->code_Count;		
		
	}
		function getUserTergetIndusNo($mid){
		$sql="select count(*) as code_Count from  ar_wiz_two_looking_industry where mid=".$mid;
		$query = $this->db->query($sql);
		 $rs = $query->row();
		return $rs->code_Count;
		
		
	}
	function getUserOwninducstryCodes($mid){
		$sql="select * from ar_member_data_wiz_one where mid=".$mid;
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		foreach($rs as $result){
			//var_dump($result);
			$top_code=explode('-',$result['ans_set_cat_id']);
			$data[]=$top_code[0];
			
		}
		$data=array_unique($data);
		$data= count($data);
		return $data;
		
	}
	function getUserOwninducstryCodesMatched($mid,$code){
		$sql="select * from ar_member_data_wiz_one where mid=".$mid;
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		foreach($rs as $result){
			//var_dump($result);
			$top_code=explode('-',$result['ans_set_cat_id']);
			$data[]=$top_code[0];
			
		}
		$data=array_unique($data);
		
		foreach($data as $data_match){
			if($data_match==$code){
				return 'true';
			}else{
				return 'false';
			}			
		}
	}
	
	
	function getUserTergetinducstryCodes($mid){
		$data=array();
		$sql="select * from ar_wiz_two_looking_industry where mid=".$mid;
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		foreach($rs as $result){
			//var_dump($result);
			$top_code=$result['industry_code'];
			$data[]=$top_code;
		}
		if(!empty($data)){
			$data=array_unique($data);
			$data= count($data);
			return $data;
		}else{
			return 0;
		}
	}
	
	function getUserOwninducstryCodebyuser($mid){
		$sql="select * from ar_member_data_wiz_one where mid=".$mid." limit 0,1";
		$query = $this->db->query($sql);
		$rs = $query->row();
		$top_code=explode('-',$rs->ans_set_cat_id);
		return $top_code[0];
	}
		function getUserTergetinducstryCodesMatch($mid,$code){
			$data=array();
		$sql="select * from ar_wiz_two_looking_industry where mid=".$mid;
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		foreach($rs as $result){
			//var_dump($result);
			$top_code=$result['industry_code'];
			$data[]=$top_code;
		}
		//print_r($data);
		if(!empty($data)){
			$data=array_unique($data);
			foreach($data as $data_match){
				//echo "data_match = ".$data_match ."---  Code = ".$code;
				if($data_match==$code){
					return 'true';
				}else{
					return 'false';
				}			
			}
		}else{
			return 'true';

		}
		
	}
	
	function readCountry($searchStr){

		$sql = "(select top_name as name from ar_top_lines  where top_name like '".$searchStr."%')
		UNION
		(select sector_name as name from ar_sector  where sector_name like '".$searchStr."%')
		UNION
		(select subsector_name as name from ar_subsector  where subsector_name like '".$searchStr."%')
		UNION
		(select bussiness_name as name from ar_bussiness  where bussiness_name like '".$searchStr."%')
		UNION
		(select industry_name as name from ar_industry  where industry_name like '".$searchStr."%')
		UNION 
		(select tar_sector_name as name from ar_target_sector  where tar_sector_name like '".$searchStr."%')
		";
		//echo $sql; exit;
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		return $rs;
	}
	
	function wizTwoservice($mid,$field_name){
		$data=array();
		$sql="select * from ar_member_data_wiz_two where mid=".$mid;
		$query = $this->db->query($sql);
		$result = $query->row();
		$result = trim($result->$field_name,',');
		$result = explode(',', $result);
		$this->db->select('*');
		if($field_name=='is_service'){	
			$this->db->from('ar_is_service');
			$this->db->where_in('service_id',$result);	
		}else{
			$this->db->from('ar_looking_services');
			$this->db->where_in('look_service_id',$result);				
		}
		$query = $this->db->get();	
		$data = $query->result();	
		return $data;
		
	}
	function fifteenToTwentyConversion($code){
		
		$sql="select * from ar_wizone_wiztwo_rel where fifteen_wizone_code='".$code."'";
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		return $rs[0]["wizone_code"];
	}

	function getSubCategoryCodeFromToplineCode($code)
	{
		$this->db->distinct();
		$this->db->select('subsector_code');
		$this->db->from('ar_relation');
		$this->db->where('top_code', $code);
		$query = $this->db->get();	
		$data = $query->result_array();	
		return $data;
	}
	function convertTenDigitToFifteenDigitCode($topline,$subsector,$bussinessArr)
	{
		$this->db->select('bussiness_code');
		$this->db->from('ar_relation');
		$this->db->where('top_code',$topline);
		$this->db->where('subsector_code',$subsector);
		$query = $this->db->get();	
		$data = $query->result_array();

		$bussinessCodeFetch = array();
		$res = array();

		foreach($data as $dt)
		{
			$bussinessCodeFetch[] = $dt['bussiness_code'];
		}

		/* 
		If selected bussiness code matches then 15 digit code 
		is genarated else code is genarated with the 1st matched code.
		*/
		foreach($bussinessArr as $bussi)
		{
			if (in_array($bussi,$bussinessCodeFetch))
			{
				$res[] = $topline.$subsector.$bussi;
			}
			else
			{
				$res[] = $topline.$subsector.$bussinessCodeFetch[0];
			}
		}
		$res = array_unique($res);	
		//echo '<pre>'; var_dump($res); die();
		return $res;
	}
	
function convertTenDigitToFifteenDigitCodeObj($topline,$subsector,$bussinessArr)
	{
		$this->db->select('bussiness_code');
		$this->db->from('ar_relation');
		$this->db->where('top_code',$topline);
		$this->db->where('subsector_code',$subsector);
		$query = $this->db->get();	
		$data = $query->result_array();

		$bussinessCodeFetch = array();
		$res = array();

		foreach($data as $dt)
		{
			$bussinessCodeFetch[] = $dt['bussiness_code'];
		}

		/* 
		If selected bussiness code matches then 15 digit code 
		is genarated else code is genarated with the 1st matched code.
		*/
		foreach($bussinessArr as $bussi)
		{
			if (in_array($bussi,$bussinessCodeFetch))
			{
				$res[] = $topline.$subsector.$bussi;
			}
			else
			{
				$res[] = $topline.$subsector.$bussinessCodeFetch[0];
			}
		}
		$res = array_unique($res);	
		//echo '<pre>'; var_dump($res); die();
		return $res;
	}

	function add_bussiness_name($id,$bussi_desc)
	{
		$data['tag_line'] = $bussi_desc;
	//	var_dump($data);exit;
		$this->db->where('mid', $id);
		$this->db->update('ar_request_access_page',$data);
		return 1;
	}

	function setUserType($data,$user_id)
	{
		$this->db->where('mid',$user_id);
		$res = $this->db->update('ar_request_access_page',$data);
		return $res;
	}
	
	function fetchTargetSectorByIndustryCode($code)
	{
		$this->db->select('*');
		$this->db->from('ar_target_relation');
		$this->db->join('ar_target_sector','ar_target_sector.tar_sector_code = ar_target_relation.tar_sector_code');
		$this->db->where('ar_target_relation.industry_code',$code);
		$query = $this->db->get();	
		$data = $query->result_array();	
		return $data;
	}
	
	function newsImage($id){
		$this->db->from('news_image');
		$this->db->where('news_id',$id);
		$query = $this->db->get();	
		$data = $query->row();	
		return $data->image_url;
		
		
	}

}
 
//end
