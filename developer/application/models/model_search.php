<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_search extends CI_model
{
 
	function __construct(){

			parent::__construct();
			 
	}
	
	
function advSearch($searchStr,$location='',$type){
	$data=array();
	$data_codes=array();
	$data_new=array();

	if($type==1){
	$sql = "select * from ar_request_access_page  where bussinessname like '%".$searchStr."%'";
	$query = $this->db->query($sql);
	$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$data_new[]=$result;
				}
			}
			foreach($data_new as $dt){
			$data[]=$dt['mid'];
			
		}
	}else{
		
		$sql = "(select top_code as code from ar_top_lines  where top_name = '".$searchStr."')
		UNION
		(select sector_code as code from ar_sector  where sector_name = '".$searchStr."')
		UNION
		(select subsector_code as code from ar_subsector  where subsector_name = '".$searchStr."')
		UNION
		(select bussiness_code as code from ar_bussiness  where bussiness_name = '".$searchStr."')
		UNION
		(select industry_code as code from ar_industry  where industry_name = '".$searchStr."')
		UNION
		(select tar_sector_code as code from ar_target_sector  where tar_sector_name = '".$searchStr."')
		";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$data_codes[]=$result;
				}
			}
			
		foreach($data_codes as $data_code){
			//var_dump($data_code['code']);
			$sql1 = "(select mid from ar_member_data_wiz_one  where mem_code REGEXP '".$data_code['code']."')			
				    UNION (select mid from ar_wiz_two_looking_industry  where industry_code ='".$data_code['code']."')
					UNION(select mid from ar_wiz_two_looking_industry  where target_sector_code ='".$data_code['code']."')";
				
			$query1 = $this->db->query($sql1);
			$rs1 = $query1->result_array();
		
			if(count($rs1) > 0){
				foreach ($rs1 as $result1){	
					$data_new[]=$result1;
				}
			}		
						
		}
		foreach($data_new as $dt){
			$data[]=$dt['mid'];
			
		}
		
		
	}
	
	return	array_unique($data);
}

function ajaxAdvanceSearch($data){
	$rs=array();
	$data_new=array();
	$rs['selected_industries']=$data['selected_industries'];
	$rs['selected_sector']=$data['selected_sector'];
	$rs['selected_sub_sector']=$data['selected_sub_sector'];
	if(count($rs) > 0){
				foreach ($rs as $result){	
					$data_codes[]=$result;
				}
			}
			//var_dump($data_codes);
	foreach($data_codes as $data_code){
			foreach($data_code as $code){
					$sql1 = "select mid from ar_member_data_wiz_one  where mem_code REGEXP '".$code."'";
					$query1 = $this->db->query($sql1);
					$rs1 = $query1->result_array();
					if(count($rs1) > 0){
					foreach ($rs1 as $result1){	
						$data_new[]=$result1;
					}
				}	
			}
					
		}
		foreach($data_new as $dt){
			$mid[]=(int)$dt['mid'];
		}
		$result1 = array_unique($mid) ;
		
			$midCountry=array();
			$data_Country=array();
		if($data['selected_country']!=false){
			foreach ($result1 as $mid){
				$user = getParticularUserDetails($mid);
				foreach ($data['selected_country'] as $country){
					if($user['country']==$country){
						$midCountry[]=(int)$mid;
					}
				}
			}
			
			$result = array_unique($midCountry);
		}else{		
			$result=$result1;
		}
		//var_dump($result);
		return array_unique($result);
		exit;
	}
	
	function getTargetLookingForType($searchtypetarget){
			$searchtypetargetarr=array();
					$sql="select * from ar_looking_for_rel where  looking_for_id=".$searchtypetarget;
					$query = $this->db->query($sql);
					if ($query->num_rows() > 0){
						$row = $query->result_array();
						foreach($row as $value){
						  $searchtypetargetarr[]=getLookingForById($value['target_looking_for_id']);
					  }
					}
					return array_unique($searchtypetargetarr);
	}
 } //end
