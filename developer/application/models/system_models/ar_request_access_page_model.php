<?php
class Ar_request_access_page_model extends Core {
    public function __construct()
	{
		parent::__construct('ar_request_access_page');
	}
	public function find($opta=array())
    {
        if(empty($opta['join'])){
                $opta['join']=array(
                        array(
                                'table'=>'user_description_keywords',
                                'on'=>'user_description_keywords.user_id=ar_request_access_page.mid'
                        )
                );
        }
        return $this->buildResult(parent::find($opta));
    }
	public function buildResult($result=array())
    {
            if(!empty($result['ar_request_access_page']))
            {
                    foreach($result['ar_request_access_page'] as $key=>$value)
                    {
                            if(!empty($result['user_description_keywords']))
                            {
                                    foreach($result['user_description_keywords'] as $keyx=>$valuex)
                                    {
                                            if($value['mid']==$valuex['user_id'])
                                            {
                                                    if(empty($result['ar_request_access_page'][$key]['user_description_keywords']))
                                                            $result['ar_request_access_page'][$key]['user_description_keywords']=array();
                                                    array_push($result['ar_request_access_page'][$key]['user_description_keywords'],$valuex);
                                            }
                                    }
                            }
                    }
            }
            return $result;
    }
}