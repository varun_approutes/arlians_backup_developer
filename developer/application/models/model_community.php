<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_community extends CI_model{
 
	function __construct(){

		parent::__construct();
		$this->load->model('model_member'); 
		$this->load->helper('common_helper');
	}

	//Add community
	function add_community(){

		$data 	= array();
		$data1 	= array();		

		//---------------------------------------


        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

		//---------------------------------------
		$tags='';
		//ar_community table data 
		$data['mid'] 		 = $user_id;
		$data['title'] 		 = $this->input->post('title');
		$data['content']	 = $this->input->post('content');
		$data['talk_access'] = $this->input->post('talk_access');
		if($this->input->post('tags')!='')
		{
			$tags = $this->input->post('tags');
		}
		
		//$tags = array("70001","70002");

		$this->db->insert('ar_community',$data);
		   $com_id = $this->db->insert_id();
		

		//ar_comm_tag table 
		
		if($tags!='')
		{
			$i=0;
			foreach($tags as $key=>$value)
			{
			$data1[$i]['com_id'] = $com_id;
			$data1[$i]['tag_id'] = $value['tag_code']; 
			$i++;	
		    }
			$this->db->insert_batch('ar_comm_tag',$data1);
		}
		
		//echo '<pre>'; var_dump($data); var_dump($data1); die();
		
		return $com_id;
	
	}
	
	function get_community_like_statuslal($com_id,$m_id)
	{
		$this->db->select('*');
        $this->db->where('com_id',$com_id);
		$this->db->where('mid',$m_id);
		$this->db->where('like_status','1');
        $this->db->from('ar_comm_like');
        $query = $this->db->get();
		//echo $this->db->last_query();
		return $query->num_rows();
	}
	function participatedusercount($com_id)
	{
		$sql="select count(*) from ar_comm_comment  where com_id='".$com_id."' group by mid";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	
	
	
	//bandhu
	 function delete_community($comid,$m_id){
               $res = '0';
			   $allow =  false;
  $data  = array();
  $data1  = array();  

                $com_id =  $comid  ;
                $mid = $m_id ;
				
				//echo $com_id ;
				//echo $mid ;
				//die ;
				
				$this->db->select('mid');
        $this->db->where('com_id',$com_id);
        
        $this->db->from('ar_community');
        $query = $this->db->get();
        $num = $query->row_array();
		//echo $num['mid'] ;
		//die ;
		$mm_id = $num['mid'];
		//echo $mid ;
		//echo "hello";
		//echo $mm_id ;
		//die ;
		
		if ($mm_id == $mid){
			$allow = true;
			
		}
		//echo $allow ;
		//die ;
                  if($allow ){
				
                 //delete from ar_community
                  
                    $this->db->where('com_id', $com_id);
                    $this->db->delete('ar_community');
					//echo $this->db->last_query();
					//die ;
                    if($this->db->affected_rows() > '0'){
						
                        $res = '1';
                    }
                //delete from ar_comm_tag
                $this->db->where('com_id', $com_id);
                $this->db->delete('ar_comm_tag');
                 if($this->db->affected_rows() > '0'){
                      //  $res = '1';
                    }
                //delete from ar_comm_comment
                $this->db->select('com_comm_id');
     $this->db->from('ar_comm_comment'); 
            $this->db->where('com_id',$com_id);
             
               $query = $this->db->get();
                 $id = $query->result_array();
                
                  $this->db->where('com_id', $com_id);
                $this->db->delete('ar_comm_comment'); 
                 if($this->db->affected_rows() > '0'){
                       // $res = '1';
                    }
                //delete from ar_comm_subcomment
                foreach ($id as $value) {
                    $com_comm_id = $value['com_comm_id'];
                      $this->db->where('com_comm_id', $com_comm_id);
                     $this->db->delete('ar_comm_subcomment');
                      if($this->db->affected_rows() > '0'){
                      //  $res = '1';
                    }
                    
                }
                //delete from ar_comm_like
                 foreach ($id as $value) {
                    $com_comm_id = $value['com_comm_id'];
                      $this->db->where('com_id', $com_comm_id);
                     $this->db->delete('ar_comm_like');
                      if($this->db->affected_rows() > '0'){
                     //   $res = '1';
                    }
                    
                }
				  }
               return $res;
        
        
        
        }


        //bandhu
		
		
 	//Add community comment
	function add_community_comment(){

		$data = array();

		//---------------------------------------


        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

		//---------------------------------------

		$data['com_id'] 	 = $this->input->post('com_id');
		$data['comment'] 	 = $this->input->post('comment');
		$data['mid']	 	 = $user_id;				
		
		$this->db->insert('ar_comm_comment',$data);
		$insert_id = $this->db->insert_id();

		$arr['com_id'] 		= $this->input->post('com_id');
		$arr['comment']  	= $this->input->post('comment');
		$arr['com_comm_id'] = $insert_id;
		$arr['mid']	 		= $user_id;
		$arr['msg'] 		= "Your comment has been added successfully.";
		$arr['currentsessioninitial']   = strtoupper(logomembername($user_id));		
		$currentsessionimg_u='';
		if($this->model_community->memberlogo($user_id) != NULL) 
		{
			$in = $this->model_community->memberlogo($user_id);
			$currentsessionimg_u = base_url().'uploads/profile_image/'.$in['image_url'];
		}
		else
		{
		   $currentsessionimg_u = 'NULL'; 
		}
		$arr['currentsessionlogourl']   = $currentsessionimg_u;
        $arr['bussiness_name_initinal']       = strtoupper(logomembername($user_id));
		$arr['bussiness_name']                = getMemberName($user_id);
		$arr['member_name']    	            = getMemberName($user_id);		
		

		return $arr;
	
	}
	//Add community like
	function add_community_like_bandhu($user_id,$com_id){

		//---------------------------------------
          $status = '0';
		//---------------------------------------
		$data = array();
		
		$data['com_id'] 	 = $com_id;			
		$data['mid']	 	 = $user_id;
        $data['like_status']   = 1;	
                  //bandhu
                $this->db->select('*');
                $this->db->from('ar_comm_like');
                $this->db->where('mid', $data['mid']);
                $this->db->where('com_id', $data['com_id']);
                $query = $this->db->get();
				//echo $this->db->last_query();
                 $res = $query->row_array();
	
                if($query->num_rows() > 0){
                    
                    if($res['like_status'] == 0){
                      $data['like_status']  = 1;
                       $status = '1' ;
					   
                    }
                    if($res['like_status'] == 1){
                      $data['like_status']  = 0;
                       $status = '0' ;
                    }
                // $this->db->set($data); 
                 $this->db->where("mid", $data['mid']); 
                 $this->db->where("com_id", $data['com_id'] ); 
                 $this->db->update("ar_comm_like",$data); 
				//$status = $this->db->last_query();
                // $check = $this->db->affected_rows() ;
				// var_dump($check);exit;
                  //if($check > 0){   
                  //    $status = '2' ;
                //  }
				
                    
                }
                
                else{
                  $this->db->insert('ar_comm_like',$data);  
                    $status = '1' ;
                    
                }
                
                return $status;
                //bandhu		

		//$this->db->insert('ar_comm_like',$data);
		//return true;
	
	}
//bandhu 

    // lal
		function add_community_like($data,$com_id,$mid)
		{
			$this->db->select('com_like_id');
			$this->db->from('ar_comm_like');
			$this->db->where('mid', $mid);
			$this->db->where('com_id', $com_id);
			$query = $this->db->get();
			$rescount = $query->num_rows();
			if($rescount>0)
			{
				$data=array();
				$data['like_status']  = 1;
				$this->db->where('mid', $mid);
			    $this->db->where('com_id', $com_id);
                $this->db->update("ar_comm_like",$data); 
			}
			else
			{
				$this->db->insert('ar_comm_like',$data);  
                
			}
			$status = '1' ;
			
			
			
            
		    return $status;
        }

	
	//lal


	 function participate($comid,$m_id){
            
              
             $status =  false;
    

                $com_id =  $comid  ;
                $mid = $m_id ;
                
                 $this->db->select('*');
                 $this->db->where('com_id',$com_id);
                 $this->db->where('mid',$mid);
                  $this->db->from('ar_comm_comment');
                   $query = $this->db->get();
                   $num = $query->num_rows();
				   
                   
                   if($num>0){
                     $status =  true;  
                       
                   }
        
                return $status;

            
        }
        
        
        function participatecount($comid,$m_id){
            
            
                $count = '0';

                $com_id =  $comid  ;
                $mi_d = $m_id ;
                
                $this->db->distinct();
                 $this->db->select('mid');
                 $this->db->where('com_id',$com_id);
                 
                  $this->db->from('ar_comm_comment');
                   $query = $this->db->get();
				//echo    $this->db->last_query ;
				//   die ;
                   $num = $query->num_rows();
                  // $count = $num + 1 ;
				    $count = $num  ;
                  return $count;
        }
		//bandhu


  //Get community details
 function get_community_details(){

  //---------------------------------------

        $d['user_email'] = $this->input->post('user_email');
        $result = $this->model_member->getIdByUserEmail($d);
        $user_id = $result->mid;

  //---------------------------------------

  $data  = array();

  $commentedIds = $this->getCommentedIds($user_id);

  $mid = $user_id;
 
		  $this->db->select('*');
		  $this->db->where('mid',$mid);
		  if(count($commentedIds) > 0)
		  {
			$this->db->or_where_in('com_id', $commentedIds);  
		  }
		  
		  $this->db->order_by('update_date','DESC');
		  $this->db->from('ar_community');
        $query = $this->db->get();
       //echo $this->db->last_query(); die();

  if ($query->num_rows() > 0){
   $i = 0;
         foreach($query->result_array() as $row){

				$data[$i]['com_id'] 		 	= $row['com_id'];
				$data[$i]['mid'] 		 		= $row['mid'];
				$data[$i]['title'] 		 		= $row['title'];
				$data[$i]['content']	 		= $row['content'];
				$data[$i]['tags'] 		 		= $this->get_community_tags($row['com_id']);
				$data[$i]['total_like']	 		= $row['total_like'];
				$data[$i]['total_comment']	 		= $row['total_comment'];
				
				
				$bName = getBusinessNameById($row['mid']);
				
				$data[$i]['bussiness_name']     = $bName['bussinessname'];
				$img          					= base_url().'uploads/profile_image/';
				$data[$i]['member_name']        = getMemberName($row['mid']);
				$data[$i]['img']				= $img.getProfileImage($row['mid']);
				$data[$i]['update_date']   		= $this->seconds_to_words($row['update_date']);   
    			$data[$i]['com_like']   		= $this->checkLike($row['com_id'] , $row['mid']);


			    //Comments & likes

			    $data[$i]['comments']   = $this->get_community_comments($row['com_id']);
			    $data[$i]['comments_count'] = $this->get_comment_count($row['com_id']);
			    $data[$i]['show_comments']  = false;
			    $data[$i]['likes']    = $this->get_community_likes($row['com_id']); 
			    $data[$i]['like_count']  = $this->get_like_count($row['com_id']);
				$data[$i]['bussiness_name_initinal']       = strtoupper(logomembername($row['mid']));
                $data[$i]['profile_image']                 = getProfileImage($row['mid']);
                $data[$i]['image_basepath']                = base_url().'uploads/profile_image/';  
                //$latest_talk[$key]['img_url']    		           = base_url().'uploads/profile_image/'.getProfileImage($row['mid']);  
                $data[$i]['open_comment_box']              = 'false';
                $data[$i]['show_participate']              = 'false';
				$data[$i]['show_sub_comment'] 	            =  false;
				$data[$i]['show_post_button']              = 'false';
				$data[$i]['show_sub_post_button']          = 'false';
				$data[$i]['show_my_talk_section']      = 'false';
				$data[$i]['com_like'] 			            = $this->checkLike($row['com_id'],$user_id);
				$data[$i]['participate_stattus']           = $this->participate($row['com_id'],$user_id);
				$data[$i]['participate_user_count']           = $this->model_community->participatedusercount($row['com_id']);
				$data[$i]['talk_access']   = $row['talk_access'];
				$data[$i]['currentsessioninitial']   = strtoupper(logomembername($user_id));
				$data[$i]['like_comid_status'] 			= $this->get_community_like_statuslal($row['com_id'],$user_id);
				$currentsessionimg_u;
                if($this->model_community->memberlogo($user_id) != NULL) 
                {
                    $in = $this->model_community->memberlogo($user_id);
                    $currentsessionimg_u = base_url().'uploads/profile_image/'.$in['image_url'];
                }
                else
                {
                   $currentsessionimg_u = 'NULL'; 
                }
				$data[$i]['currentsessionlogourl']   = $currentsessionimg_u;
				$data[$i]['access_confirmedusers']   = $this->model_community->get_community_accepted_request($row['com_id']);
				
				
				
				
				
				
				
				
				
				

			    $i++;
         	}

        } 
  return $data;
 }
	function get_comment_count($com_id)
	{
		$this->db->select('count(*) as ct');
		$this->db->from('ar_comm_comment');
		$this->db->where('com_id',$com_id);
		$query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['ct'];
	}

	function get_like_count($com_id)
	{
		$this->db->select('count(*) as ct');
		$this->db->from('ar_comm_like');
		$this->db->where('com_id',$com_id);
		$query = $this->db->get();
        $res = $query->result_array();
        return $res[0]['ct'];
	}
	//Get community tags
	function get_community_tags($com_id){

		$data  = array();
	
		$this->db->select('*');
		$this->db->where('com_id',$com_id);
		$this->db->from('ar_comm_tag');
        $query = $this->db->get();

		if ($query->num_rows() > 0){
        	foreach($query->result_array() as $row){
				
				$data[]	= $row->tag_id;		
        	}
        } 
				
		return $data;
	} 
	//Get community likes.
	function get_community_likes($com_id){

		$data  = array();
	
		$this->db->select('*');
		$this->db->where('com_id',$com_id);
		$this->db->from('ar_comm_like');
        $query = $this->db->get();

		if ($query->num_rows() > 0){
        	foreach($query->result_array() as $row){
				
				$data[]['mid']	= $row->mid;		
        	}
        } 
				
		return $data;
	}	
	
	function get_community_like_status($com_id){

		$data  = array();
	
		$this->db->select('like_status');
		$this->db->where('com_id',$com_id);
		$this->db->from('ar_comm_like');
        $query = $this->db->get();
		$status=$query->row_array();
		if($status['like_status']==1){
			
			$st=1;
		}else{
			$st=0;
		}
		return $st;
	}	
	//Get community comments
	function get_community_comments($com_id){
		$data  = array();		
	
		$this->db->select('*');
		$this->db->where('com_id',$com_id);
		$this->db->order_by('comm_date','DESC');
		$this->db->from('ar_comm_comment');
        $query = $this->db->get();
        //print_r($query->result_array());die();
		if ($query->num_rows() > 0){
			$i = 0;
        	foreach($query->result_array() as $row){
				
				$data[$i]['com_comm_id']        = $row['com_comm_id'];
				$data[$i]['com_id'] 	 		= $row['com_id'];
				$data[$i]['comment'] 	 		= $row['comment'];
				$data[$i]['mid']	 	 		= $row['mid'];	
				$data[$i]['show_sub_post_button'] = 'false';
				$data[$i]['show_sub_comment'] = 'false';
				$data[$i]['bussiness_name'] 	= getMemberName($row['mid']);
				$data[$i]['profile_image']  	= getProfileImage($row['mid']);
				$data[$i]['image_basepath'] 	= base_url().'uploads/profile_image/';	
				$data[$i]['comm_date'] 			= calculateTimeFromPost($row['comm_date'], date("Y-m-d H:i:s"), "s"); 
				$data[$i]['subcomment_count'] 	= $this->subcomencount($row['com_comm_id']);
				$data[$i]['comment_like_status']= $this->get_community_like_status($row['com_id']);
				$data[$i]['comment_like_count'] = $this->comentlikecount($row['com_id'],$row['comment']);
				$data[$i]['commentlogomembername']                = strtoupper(logomembername($row['mid']));
				$img_u;
                if($this->model_community->memberlogo($row['mid']) != NULL) 
                {
                    $in = $this->model_community->memberlogo($row['mid']);
                    $img_u = base_url().'uploads/profile_image/'.$in['image_url'];
                }
                else
                {
                   $img_u = 'NULL'; 
                }
                $data[$i]['commentlogomembername_url']   = $img_u;
				
				
				
				 //bandhu palash
                                $details  = array();
                               
							      $subcom = $row['com_comm_id'] ;
                                //$ressubcoment = $this->model_community->fetchSubComments($subcom);
                               // $ressubcoment = $this->fetchSubComments($subcom);
								$ressubcoment = $this->fetchSubCommentsnew($row['com_comm_id']);
                             foreach ($ressubcoment as $value) {
									$temp['sub_comment_id'] =  $value['sub_comment_id'] ;  
									$temp['comment_id'] =  $value['com_comm_id'] ;
									$temp['sub_comment'] =  $value['comment'] ;
									$temp['sub_mid'] =  $value['mid'] ;
									$temp['bussiness_name'] = getMemberName($value['mid']);
									$temp['profile_image'] = getProfileImage($value['mid']);
									$temp['sub_comm_date'] =  calculateTimeFromPost($value['comm_date'], date("Y-m-d H:i:s"), "s");
									$temp['subcommentlogomembername']                = strtoupper(logomembername($value['mid']));
									$subimg_u;
									if($this->model_community->memberlogo($value['mid']) != NULL) 
									{
										$in = $this->model_community->memberlogo($value['mid']);
										$subimg_u = base_url().'uploads/profile_image/'.$in['image_url'];
									}
									else
									{
									   $subimg_u = 'NULL'; 
									}
									$temp['subcommentlogomembername_url']   = $subimg_u;
														
									
									
									
									
									
									
							
									array_push($details, $temp);
                                         
                  }
                              $data[$i]['subcomment'] = $details ;
                                
                                //bandhu palash
				$i++;
        	}
        }
		//print_r($data);die();
		return $data;
	}
	
	public function getmytalk($com_id){
		
		$this->db->select('my_talk');
		$this->db->where('com_id',$com_id);
		$this->db->from('ar_comm_comment');
        $query = $this->db->get();
		$res = $query->result_array();
		
		$mytalk = $res[0]['my_talk'];
		
		return $mytalk;
		
	}
	



	//Get time in proper format
    public function seconds_to_words($seconds){ 

        $ret = "";
        $days = intval(intval($seconds) / (3600*24));               

        if($days<1){

	        /*** get the days ***/
	        //$days = intval(intval($seconds) / (3600*24));
	        if($days> 0){
	            $ret .= "$days days ";
	        }

	        /*** get the hours ***/
	        $hours = (intval($seconds) / 3600) % 24;
	        if($hours > 0){
	            $ret .= "$hours hours ";
	        }

	        /*** get the minutes ***/
	        $minutes = (intval($seconds) / 60) % 60;
	        if($minutes > 0){
	            $ret .= "$minutes minutes ";
	        }

	        /*** get the seconds ***/
	        $seconds = intval($seconds) % 60;
	        if ($seconds>=0){
	            $ret .= "$seconds seconds";
	        }

	       	$ret .= " ago";	

	    }else{

	    	$seconds = time() - $seconds;
	    	$ret = date('d/m/y h:ia', $seconds);
	    }       

        return $ret;
    }

    function getSuggestedTag($str)
    {
    	$this->db->select('*');
    	$this->db->from('ar_tag');
    	$this->db->like('tag_name', $str, 'after');
		$this->db->limit(5,0);
    	$query = $this->db->get();
        $res = $query->result_array();
        return $res;
    } 

    function getTagsFromTopline($topLine)
    {
    	$sql = 'SELECT `tag1_id` FROM `ar_top_tag_rel` WHERE `top_code` = '.$topLine.' UNION SELECT `tag2_id` FROM `ar_top_tag_rel` WHERE `top_code` = '.$topLine.' UNION SELECT `tag3_id` FROM `ar_top_tag_rel` WHERE `top_code` = '.$topLine;
    	$query = $this->db->query($sql);
        $res = $query->result_array();
        return $res;
    } 

    function fetchLatestTalk($tags,$own_com_ids)
    {
    	$this->db->select('*');
    	$this->db->from('ar_community');
    	//$this->db->join('ar_comm_tag','ar_community.com_id = ar_comm_tag.com_id','LEFT');
		$this->db->join('ar_members','ar_members.mid = ar_community.mid','INNER');
    	$this->db->where('ar_community.status','1');
    	//if(count($own_com_ids) > 0)
    	//{
    		//$this->db->where_not_in('ar_community.com_id', $own_com_ids);
    	//}
    	//$this->db->where_in('ar_comm_tag.tag_id', $tags); // commented on 27th june by lal
		$this->db->where_in('ar_members.status', '1');
    	$this->db->group_by('ar_community.com_id');
    	$this->db->order_by('ar_community.update_date','desc');

    	$query = $this->db->get();
    	//echo $this->db->last_query(); die();
        $res = $query->result_array();
        return $res;
    } 

    function fetchConnectedTalk($connects)
    {
    	$this->db->select('*');
    	$this->db->from('ar_community');
    	$this->db->where_in('mid',$connects);
    	$this->db->where('talk_access','0');
    	$this->db->order_by('update_date','desc');

    	$query = $this->db->get();
        $res = $query->result_array();
        return $res;

    }
    function fetchTopTalks() 
    {
    	$sql = 'SELECT * from ar_community as arc INNER JOIN (SELECT newtable.com_id,count(com_id) as dis_comm FROM (SELECT * FROM ar_comm_comment group by mid,com_id ) as newtable group by newtable.com_id order by dis_comm desc) AS arcc on arc.com_id = arcc.com_id   order by dis_comm DESC LIMIT 0,15';
    	$query = $this->db->query($sql);
        $res = $query->result_array();
		
		foreach($res as $key=>$re)
		{
			$res[$key]['com_like'] 			= $this->checkLike($re['com_id'],$re['mid']);
			$res[$key]['comments'] 	        = array();
			$res[$key]['show_sub_comment'] 	= false;
			$res[$key]['show_post_button']  = false;
		}
		//echo '<pre>'; var_dump($res); die();
        return $res;
    }

    function deleteComment($data)
    {
    	$res = $this->db->delete('ar_comm_comment', $data);
    	return $res;
    }
    
    function dislike_old($data)
    {
		
    	$res = $this->db->delete('ar_comm_like', $data);
    	return $res;
    }
	// lal
	 function dislike($data,$mid,$com_id)
    {
		 $this->db->where("mid", $mid); 
         $this->db->where("com_id", $com_id ); 
         $res =$this->db->update("ar_comm_like",$data);
		 return $res;
    }
	// lal

	function checkLike($com_id,$mid)
    {
     $this->db->select('count(*) as ct');
     $this->db->from('ar_comm_like');
     $this->db->where('com_id',$com_id);
     $this->db->where('mid',$mid);

     $query = $this->db->get();


    
        $res = $query->result_array();
        return $res[0]['ct'];
    }

    function getCommentedIds($mid)
    {
    	$this->db->select('com_id');
    	$this->db->from('ar_comm_comment');
    	$this->db->where('mid',$mid);
    	$query = $this->db->get();
    	$res = $query->result_array();

    	$arr = array();

    	foreach($res as $r)
    	{
    		$arr[] = $r['com_id'];
    	}

    	$arr = array_unique($arr);
    	return $arr;
    }

    function request_access($data)
    {
    	$res = $this->db->insert('talk_access',$data);
    	return $res;
    }

    function allow_access($com_id,$mid)
    {
    	$where['com_id'] = $com_id;
    	$where['mid'] = $mid;

    	$this->db->where($where);
    	$res = $this->db->update('status','1');
    	return $res;
    }

    function access_check($data)
    {
    	$this->db->select('status');
    	$this->db->where($data);
    	$this->db->from('talk_access');

    	$query = $this->db->get();
    
        $res = $query->result_array();
        return $res[0]['status'];
    }

    function insertSubComment($data)
    {
    	$res = $this->db->insert('ar_comm_subcomment',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
    	
    }

    function fetchSubComments($data)
    {
    	$this->db->select('*');
    	$this->db->from('ar_comm_subcomment');
    	$this->db->where($data);
    	$query = $this->db->get();
    
        $res = $query->result_array();

        return $res;
    }
	
	//bandhu
    function fetchSubCommentsnew($data)
    {
     $this->db->select('*');
     $this->db->from('ar_comm_subcomment');
     $this->db->where('com_comm_id', $data);
     $query = $this->db->get();
    
        $res = $query->result_array();

        return $res;
    }
	function fetchsubcomentidmax($data){
      $this->db->select_max('sub_comment_id');
     $this->db->from('ar_comm_subcomment');
     $this->db->where('com_comm_id', $data);
     $query = $this->db->get();
    
        $res = $query->result_array();
        
        $id = $res['0']['sub_comment_id'];
       return $id; 
      //  return $res;
        
        
    }
    function subcomencount($data){
        
       $this->db->select('*');
     $this->db->from('ar_comm_subcomment');
     $this->db->where('com_comm_id', $data);
     $query = $this->db->get();
    
        $num = $query->result_array();
      //  $num = $query->num_rows() ;
     //   $res = 	$num + 1 ;	
          $res = $query->num_rows() ;
        return $res; 
        
    }
	
	function comentlikecount($com_id,$comment){
         $status = '1';
        $comunity_id = $com_id ;
        $community_comment = $comment ;
        $comment_id =$this->commentid($comunity_id,$community_comment);
        $this->db->select('like_status');
        $this->db->where('com_id',$comment_id);
         $this->db->where('like_status',$status);
        $this->db->from('ar_comm_like');
        $query = $this->db->get();
        $num = $query->num_rows() ;
        $likecount = $num + 1 ;
        
        return $likecount;
    }
    
    function commentid($com_id,$comment){
        
		 $data  = array();  
		// $comment = $this->get_community_comments($com_id);
		$this->db->select('com_comm_id');
		$this->db->where('com_id',$com_id);
		$this->db->where('comment',$comment);
		$this->db->from('ar_comm_comment');
		$query = $this->db->get();
		$id = $query->result_array();
		$res = $id['com_comm_id'];
               
        return $res; 
    }
	
	function memberlogo($id)
	{
		$this->db->select('image_url');
		$this->db->from('member_image');
		$this->db->where('mid',$id);
		$this->db->where('type','sign_img');
		$query = $this->db->get();
        $res = $query->result_array();
        return $res[0];
	}

	function fetchComId($user_id)
	{
		$commentedIds = $this->getCommentedIds($user_id);
		$commentedIds = array_unique($commentedIds);

		$this->db->select('com_id');
		$this->db->from('ar_community');
		$this->db->where('mid',$user_id);
		$query = $this->db->get();
        $res = $query->result_array();

        $arr = array();
	      foreach($res as $r)
	      {
	      	$arr[] = $r['com_id'];
	      }

	    $result = array_merge($arr, $commentedIds);
	    return $result;
	}
	
	function FnGetCommunityNotification($user_id)
	{
		$this->db->select('talk.*');
		$this->db->from('talk_access as talk');
		$this->db->join('ar_members','ar_members.mid = talk.mid','INNER');
		$this->db->where('talk.talk_owner_id',$user_id);
		$this->db->where('ar_members.status','1');
		$this->db->where('talk.status','0');
		$query = $this->db->get();
		//echo $this->db->last_query();
        $res = $query->result_array();
		return $res;
	}
	
	function accepttalkrequest($data)
	{
		$sql = "UPDATE `talk_access` SET  `status` = '1' WHERE `mid` =  '".$data['mid']."'  AND `com_id` =  '".$data['com_id']."' AND `talk_owner_id` =  '".$data['talk_owner_id']."'";
    	$query = $this->db->query($sql);
		return 1;
	}
	
	function deletetalkrequest($data)
	{
		$sql = "Delete from `talk_access`  WHERE `mid` =  '".$data['mid']."'  AND `com_id` =  '".$data['com_id']."' AND `talk_owner_id` =  '".$data['talk_owner_id']."'";
    	$query = $this->db->query($sql);
		return 1;
	}
function get_mycommunity_details($mid){

  //---------------------------------------

        
        $user_id = $mid;

  //---------------------------------------

  $data  = array();

  $commentedIds = $this->getCommentedIds($user_id);

  $mid = $user_id;
 
		  $this->db->select('*');
		  $this->db->where('mid',$mid);
		  if(count($commentedIds) > 0)
		  {
			$this->db->or_where_in('com_id', $commentedIds);  
		  }
		  
		  $this->db->order_by('update_date','DESC');
		 
		  $this->db->from('ar_community');
        $query = $this->db->get();
       //echo $this->db->last_query(); die();

  if ($query->num_rows() > 0){
   $i = 0;
         foreach($query->result_array() as $row){

				$data[$i]['com_id'] 		 	= $row['com_id'];
				$data[$i]['mid'] 		 		= $row['mid'];
				$data[$i]['title'] 		 		= $row['title'];
				$data[$i]['content']	 		= $row['content'];
				$data[$i]['tags'] 		 		= $this->get_community_tags($row['com_id']);
				
				$bName = getBusinessNameById($row['mid']);
				
				$data[$i]['bussiness_name']     = $bName['bussinessname'];
				$img          					= base_url().'uploads/profile_image/';
				$data[$i]['member_name']        = getMemberName($row['mid']);
				$data[$i]['img']				= $img.getProfileImage($row['mid']);
				$data[$i]['update_date']   		= $this->seconds_to_words($row['update_date']);   
    			//$data[$i]['com_like']   		= $this->checkLike($row['com_id'] , $row['mid']);


			    //Comments & likes

			   // $data[$i]['comments']   = $this->get_community_comments($row['com_id']);
			   // $data[$i]['comments_count'] = $this->get_comment_count($row['com_id']);
			   // $data[$i]['show_comments']  = false;
			   // $data[$i]['likes']    = $this->get_community_likes($row['com_id']); 
			   // $data[$i]['like_count']  = $this->get_like_count($row['com_id']);

			    $i++;
         	}

        } 
  return $data;
 }


function get_mycommunity_detailslimit($mid){

  //---------------------------------------

        
        $user_id = $mid;

  //---------------------------------------

  $data  = array();

  $commentedIds = $this->getCommentedIds($user_id);

  $mid = $user_id;
 
		  $this->db->select('*');
		  $this->db->where('mid',$mid);
		  if(count($commentedIds) > 0)
		  {
			$this->db->or_where_in('com_id', $commentedIds);  
		  }
		  
		  $this->db->order_by('update_date','DESC');
		  $this->db->limit(5,0);
		  $this->db->from('ar_community');
        $query = $this->db->get();
       //echo $this->db->last_query(); die();

  if ($query->num_rows() > 0){
   $i = 0;
         foreach($query->result_array() as $row){

				$data[$i]['com_id'] 		 	= $row['com_id'];
				$data[$i]['mid'] 		 		= $row['mid'];
				$data[$i]['title'] 		 		= $row['title'];
				$data[$i]['content']	 		= $row['content'];
				$data[$i]['tags'] 		 		= $this->get_community_tags($row['com_id']);
				
				$bName = getBusinessNameById($row['mid']);
				
				$data[$i]['bussiness_name']     = $bName['bussinessname'];
				$img          					= base_url().'uploads/profile_image/';
				$data[$i]['member_name']        = getMemberName($row['mid']);
				$data[$i]['img']				= $img.getProfileImage($row['mid']);
				$data[$i]['update_date']   		= $row['update_date'];   
    			//$data[$i]['com_like']   		= $this->checkLike($row['com_id'] , $row['mid']);


			    //Comments & likes

			   // $data[$i]['comments']   = $this->get_community_comments($row['com_id']);
			   // $data[$i]['comments_count'] = $this->get_comment_count($row['com_id']);
			   // $data[$i]['show_comments']  = false;
			   // $data[$i]['likes']    = $this->get_community_likes($row['com_id']); 
			   // $data[$i]['like_count']  = $this->get_like_count($row['com_id']);

			    $i++;
         	}

        } 
  return $data;
 }
 
 function get_community_accepted_request($com_id)
 {
	  $this->db->select('mid');
	  $this->db->where('com_id',$com_id);
	  $this->db->where('status','1');	  
	  $this->db->from('talk_access');
	  $query = $this->db->get();
	 // echo $this->db->last_query();
	  $returndata=array();
	  if($query->num_rows() >0)
	  {
		  foreach($query->result_array() as $row)
		  {
			   $returndata[]['mid']=$row['mid'];
		  }
		 
	  }		
	  //print_r($returndata);
      return $returndata;	  
 }
 
 function FnAddRecommendCommunity($data)
 {
	//$this->db->insert('ar_community',$data);
	//return $this->db->insert_id();
 }
 
 function FnGetTalkTitle($titleid)
 {
	  $returndata='';
	  $this->db->select('title');
	  $this->db->where('com_id',$titleid);
	  $this->db->from('ar_community');
	  $query = $this->db->get();
	  $returndata=array();
	  if($query->num_rows()==1)
	  {
		  $row=$query->row_array();
		 $returndata=$row['title'];
		 
		 
	  }		
	  
      return $returndata;	
 }

 
}
?>