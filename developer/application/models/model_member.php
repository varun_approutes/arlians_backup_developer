<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Model_member extends CI_model {

    function __construct() {

        parent::__construct();
    }

    function membervalidate($email) {
        $this->db->select('*');
        $this->db->where('email', $email);        
        $query = $this->db->get('ar_members');
        //echo $this->db->last_query(); exit;
        if ($query->num_rows() > 0) {
            return "TRUE";
        } else {
            return "FALSE";
        }
    }

    function getReferalData($referal_code = '') {
        $this->db->select('mid');
        $this->db->from('ar_members');
        $this->db->where('sharecode', $referal_code);
        $query = $this->db->get();
        $res = $query->row();
        return $res;
    }

    function insertmemberdata($data) {
        $result = $this->membervalidate($data['hemail']);
        if ($result != 'TRUE') {
            $d['email'] = $data['hemail'];
            $d['password'] = md5($data['pass']);
            $d['status'] = '1';
            //$d['verification_code'] = md5($data['hemail']);
            $this->db->insert('ar_members', $d);
            $id = $this->db->insert_id();
            $m['mid'] = $id;
            $this->db->insert('ar_request_access_page', $m);
            if ($data['referal'] != '0') {
                $referals_data = $this->getReferalData($data['referal']);
                $insert_data['connected_id'] = $referals_data->mid;
                $insert_data['mid'] = $id;
                $insert_data['is_seen'] = 1;
                $insert_data['is_accepted'] = 1;
                $this->db->insert('connected', $insert_data);

                $referal_data['mid'] = $insert_data['connected_id'];
                $referal_data['ref_mid'] = $insert_data['mid'];
                $referal_data['ref_code'] = $data['referal'];
                $this->db->insert('ar_referral', $referal_data);
            }
            return $id;
        } else {
            return "FALSE";
        }
    }

    function update_member($data) {
        $this->db->where('mid', $data['mid']);
        $this->db->update('ar_members', $data);
        if ($this->db->affected_rows() >= 0) {
            return true; // your code
        } else {
            return false; // your code
        }
    }

    function logincheck($data) {
        $email = $data['email'];
        $pass = md5($data['pass']);
        $sql = "SELECT * FROM ar_members WHERE email='" . $email . "' AND password='" . $pass . "'";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $sess_array = array();
            foreach ($query->result() as $row) {
                $sess_array = array(
                    'id' => $row->mid,
                    'username' => $row->email,
                    'status' => $row->status
                );
				$currentdate=date("Y-m-d"); 
				$updatedata=array('last_login_time'=>$currentdate,'notification_email_count'=>0);
				//print_r($updatedata);die();
				$this->db->where('mid', $row->mid);
                $this->db->update('ar_members', $updatedata);
                $this->session->set_userdata('logged_in', $sess_array); 
            }
            return "TRUE";
        } else {
            // $this->form_validation->set_message('check_database', 'Invalid username or password');
            return "FALSE";
        }
    }
	function memberStatus($data){
		$email = $data['email'];
        $pass = md5($data['pass']);
        $sql = "SELECT * FROM ar_members WHERE email='" . $email . "' AND password='" . $pass . "' limit 0,1";
        $query = $this->db->query($sql);
		$rs = $query->row();
		return $rs->status;
	}

    function autofetchMemberId($str, $mid) {
        $data = array();
        $sql = "select ar_members.mid,ar_members.email,ar_request_access_page.bussinessname from ar_members LEFT JOIN ar_request_access_page ON ar_members.mid =  ar_request_access_page.mid where ar_members.email like '" . $str . "%' OR ar_request_access_page.bussinessname LIKE '" . $str . "%' limit 0,5"; 
		
		/*  $sql = "select ar_members.mid,ar_members.email,ar_request_access_page.bussinessname from ar_members LEFT JOIN ar_request_access_page ON ar_members.mid =  ar_request_access_page.mid where (ar_members.mid in(select mid from connected where connected_id=" . $mid . ") or  ar_members.mid in(select connected_id from connected where connected.mid=" . $mid . ")) and  ar_members.email like '" . $str . "%' OR ar_request_access_page.bussinessname LIKE '" . $str . "%' limit 0,5"; */
		
		
        $arr = array();
        $rs = mysql_query($sql);
        while ($obj = mysql_fetch_object($rs)) {
            $arr[] = $obj;
        }
        $json_response = json_encode($arr);
        return $json_response;
    }

    function update_activation_code($id, $activation_code = null) {
        if ($activation_code != '' && $id != '') {
            $data['verification_code'] = NULL;
            $data['status'] = "1";
            $this->db->where('mid', $id);
            $this->db->where('verification_code', $activation_code);
            $this->db->update('ar_members', $data);
            $res = $this->db->affected_rows();
            if ($res == 1) {
                return "TRUE";
            } else {
                return "FALSE";
            }
        } else {
            return "FALSE";
        }
    }

    function data_exists_wiz_one($id) {
        if ($id != '') {
            $sql = 'SELECT * FROM ar_member_data_wiz_one WHERE mid=' . $id;
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) {
                return "TRUE";
            } else {
                return "FALSE";
            }
        } else {

            return "FALSE";
        }
    }

    function fetch_country_list() {
        $sql = 'SELECT * FROM `countries` order by country_name';
		$query = $this->db->query($sql);
        $arr = $query->result_array();
		
       /*  $rs = mysql_query($sql);
        while ($obj = mysql_fetch_object($rs)) {

            $arr[] = $obj;
        } */

        return $arr;
    }
	
	function fetch_country_byid($country_id) {
        $sql = 'SELECT * FROM `countries` where id="'.$country_id.'"';
		$query = $this->db->query($sql);
		$countryname='';
        if($query->num_rows()==1)
		{  
	        $arr = $query->row_array();
			$countryname=$arr['country_name'];
		}
		return $countryname;
    }

    function insert_wiz_one_p_one($d) {
        $this->db->where('mid', $d['mid']);
        $this->db->update('ar_request_access_page', $d);
        //$this->db->last_query();exit;
        $lid = $this->db->insert_id();
        if ($lid) {
            return true;
        } else {
            return false;
        }
    }

    function retriveMemberPassword($email) {
        $sql = "SELECT * FROM ar_members WHERE email='" . $email . "'";
        $query = $this->db->query($sql);
        $rs = $query->result_array();
        if ($query->num_rows() > 0) {
            //var_dump($rs[0]['mid']);
            return $rs[0]['mid'];
        } else {
            return 0;
        }
    }

    function update_pass($id, $pass) {
        $data['password'] = $pass;
        $this->db->where('mid', $id);
        $this->db->update('ar_members', $data);
        return 1;
    }

    function fetchMemberId($c) {
        if ($c == "ALL") {
            $sql = "SELECT * FROM ar_member_data_wiz_one";
        } else {
            $sql = "SELECT * FROM ar_member_data_wiz_one WHERE ans_set_cat_id LIKE '%" . $c . "'";
        }
        $query = $this->db->query($sql);

        $result = $query->result_array();
        $mids = array();
        //var_dump($result); die();
        foreach ($result as $row) {
            $mids[] = $row['mid'];
        }
        $mids = array_unique($mids);
        return $mids;
    }

    function insertMessageTrans($d) {
        $res = $this->db->insert('ar_message_trans', $d);
		return $res;
    }

    function BackImage($id) {
        if ($id != 0) {
            $sql = "SELECT * FROM member_image where type='back_img' and mid='" . $id . "'";
            $query = $this->db->query($sql);
            $rs = $query->result_array();
            if ($query->num_rows() > 0) {
                //var_dump($rs[0]['mid']);
                return $rs[0]['image_url'];
            } else {
                //return '';
                $this->db->select('ans_set_cat_id');
                $this->db->from('ar_member_data_wiz_one');
                $this->db->where('mid', $id);
                $query = $this->db->get();
                if ($query->num_rows() > 0) {
                    $res = $query->result_array();
                    $codeSet = $res[0]['ans_set_cat_id'];
                    $topLineCode = explode('-', $codeSet);

                    //echo $topLineCode[0];

                    if ($topLineCode[0] != NULL) {
                        $this->db->select('background_image');
                        $this->db->from('ar_top_lines');
                        $this->db->where('top_code', $topLineCode[0]);
                        $query1 = $this->db->get();
                        $result = $query1->result_array();

                        return $result[0]['background_image'];
                    }
                    //echo '<pre>'; var_dump($result); die();
                } else {
                    return '';
                }
            }
        }
    }

    function ProfileImage($id) {
        $sql = "SELECT * FROM member_image WHERE type='profile_img' and mid='" . $id . "'";
        $query = $this->db->query($sql);
        $rs = $query->result_array();
        if ($query->num_rows() > 0) {
            //var_dump($rs[0]['mid']);
            return $rs[0]['image_url'];
        } else {
            return '';
        }
    }
function signImage($id) {
        $sql = "SELECT * FROM member_image WHERE type='sign_img' and mid='" . $id . "'";
        $query = $this->db->query($sql);
        $rs = $query->result_array();
        if ($query->num_rows() > 0) {
            //var_dump($rs[0]['mid']);
            return $rs[0]['image_url'];
        } else {
            return '';
        }
    }
    function MemberDetails($email) {
        $sql = "SELECT * FROM ar_members left join ar_request_access_page on ar_members.mid=ar_request_access_page.mid WHERE ar_members.email='" . $email . "'";
        $query = $this->db->query($sql);
        $rs = $query->result_array();
        $data = $rs[0];
        $id = $rs[0]['mid'];
        $data['bussinessname'] = getMemberbusinName($id);
        $data['back_image'] = $this->BackImage($id);
        $data['fornt_image'] = $this->ProfileImage($id);
		 $data['sign_image'] = $this->signImage($id);
        $data['topline_name'] = $this->toplineofMember($id);
        $data['path'] = base_url() . 'uploads/profile_image/';
		
		$time = strtotime($this->getUserRegistrationDate($id));
		 $data['reg_date'] =  date("M,Y", $time);
		//$date = new DateTime($this->getUserRegistrationDate($id));
       // $data['reg_date'] =  $date->format('M,Y');
        $data['connection_count'] =  $this->connectioncount($id);
        return $data;
    }

	function connectioncount($id){
	if($id!=0){
	//$this->db->where('connected_id',$id);
	//$this->db->or_where('mid',$id);
	//$this->db->where('is_accepted','1');
	//$this->db->from('connected');
	//$num_rows = $this->db->count_all_results('connected');
	//return $num_rows;
	$sql = " SELECT * FROM (`connected`)  WHERE `is_accepted` = '1' and (`mid` = ".$id." OR `connected_id` = ".$id." ) ORDER BY `mid` DESC";
	$query = $this->db->query($sql);
	
	
	
	
	/* $this->db->where('connected_id',$id);
	$this->db->or_where('mid',$id);
	$this->db->where('is_accepted','1'); 
	//$this->db->from('connected');
	echo $num_rows = $this->db->count_all_results('connected'); die();*/
	return $query->num_rows();
	}else{
		
		return 0;
	}
}
    function isAlreadyDataInWizTwo($mid) {
        $sql = "SELECT * FROM ar_member_data_wiz_two WHERE mid=" . $mid;
        $query = $this->db->query($sql);
        $rs = $query->result_array();
        if ($query->num_rows() > 0) {
            return 'true';
        } else {
            return 'false';
        }
    }

    function member_bussiness_name_message($mid) {
		if($mid!=''){
        $sql = "SELECT * FROM ar_request_access_page WHERE mid=" . $mid;
        $query = $this->db->query($sql);
        $rs = $query->result_array();
        if ($query->num_rows() > 0) {
            //var_dump($rs[0]['mid']);
            return $rs[0]['bussinessname'];
        } else {
            return '';
        }
	}else{
		return '';
		
	}
}

    function getUserById($mid) {
		if($mid!=''){
			$sql = "SELECT * FROM ar_members  WHERE ar_members.mid=" . $mid;
			$query = $this->db->query($sql);
			$rs = $query->row();
			return $rs;
		}
    }

    function visited_profile($d) {
        //echo "<pre>"; var_dump($d); die();
        $this->db->insert('notification', $d);
    }

    function fetchWhoViewedMyProfile($id) {
		if($id!=''){
			$sql = "SELECT * FROM (`notification`) LEFT JOIN `ar_request_access_page` ON `ar_request_access_page`.`mid` = `notification`.`who_viewed` Inner Join `ar_members` on `ar_members`.`mid`=`ar_request_access_page`.`mid` LEFT JOIN `member_image` ON `member_image`.`mid` = `notification`.`who_viewed` and member_image.type = 'profile_img' WHERE (`notification`.`whose_profile_viewed` = " . $id . " and  `notification`.`who_viewed` != " . $id . ") and `notification`.`is_seen` = '0' and `ar_members`.`status`='1'   ORDER BY `id` desc ";
			$query = $this->db->query($sql); // group by `notification`.`who_viewed`
			$res = $query->result_array();
			 $i=0;
			$sug_member=array();
			foreach ($res as $key=>$value){
				foreach ($value as $k=>$val){
					$sug_member[$i][$k]=$val;
				}
				 $sug_member[$i]['on_date']=  calculateTimeFromPost( $sug_member[$i]['on_date'], date("Y-m-d H:i:s"), "s");
				//
			$i++;	
			} 
			
			//echo $this->db->last_query(); exit;   
			return $sug_member;
			//return $res;
		}else{
			
			return '';
		}
    }

    function fetchNotification($id, $start, $limit) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = notification.who_viewed', 'left');
        $this->db->join('member_image', 'member_image.mid = notification.who_viewed and member_image.type = "profile_img"', 'left');
        $this->db->from('notification');
        $this->db->where('notification.whose_profile_viewed', $id);
        $this->db->order_by("id", "desc");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        $res = $query->result_array();
       $i=0;
		$sug_member=array();
		foreach ($res as $key=>$value){
			foreach ($value as $k=>$val){
				$sug_member[$i][$k]=$val;
			}
			 $sug_member[$i]['on_date']=  calculateTimeFromPost( $sug_member[$i]['on_date'], date("Y-m-d H:i:s"), "s");
			//
		$i++;	
		}
		
        //echo $this->db->last_query(); exit;   
        return $sug_member;
    }
	
	
	function getHubuserDetailsById($hub_id) {
        $this->db->select('hub.hub_title,hub.hbcomment,members.fname,members.lname,members.email,hub.mid');
        $this->db->join('ar_members as members', 'members.mid = hub.mid', 'Inner');
        $this->db->from('ar_hub as hub');
        $this->db->where('hub.hid', $hub_id);
        $query = $this->db->get();
		//echo $this->db->last_query();die();
        $res = $query->row_array();
        return $res;
    }
	
	
	

    function fetchTotalWhoViewedMyProfile($id, $start, $limit) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = notification.who_viewed', 'left');
        $this->db->join('member_image', 'member_image.mid = notification.who_viewed and member_image.type = "profile_img"', 'left');
        $this->db->from('notification');
        $this->db->where('notification.whose_profile_viewed', $id);
        $this->db->order_by("id", "desc");
        //$this->db->limit($limit,$start);      
        $query = $this->db->get();
        $total = $query->num_rows();
        //echo $total;
        return ($total / $limit);
    }

    function fetchWhoViewedMyProfileUnseen($id) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = notification.who_viewed', 'left');
        $this->db->join('member_image', 'member_image.mid = notification.who_viewed and member_image.type = "profile_img"', 'left');
        $this->db->from('notification');
        $this->db->where('notification.whose_profile_viewed', $id);
        $this->db->where('notification.is_seen', '0');
        $query = $this->db->get();
        $res = $query->result_array();
       /* $i=0;
		$sug_member=array();
		foreach ($res as $key=>$value){
			foreach ($value as $k=>$val){
				$sug_member[$i][$k]=$val;
			}
			 $sug_member[$i]['on_date']=  calculateTimeFromPost( $sug_member[$i]['on_date'], date("Y-m-d H:i:s"), "s");
			//
		$i++;	
		} */
		
        //echo $this->db->last_query(); exit;   
       // return $sug_member;
        return $res;
       
    }

    function updateNotificationData($list) {
		//var_dump($list);
		foreach($list as $value){
			$data['is_seen'] = '1';
            $this->db->where('id', (int)$value->id);
            $this->db->update('notification', $data);
			//echo $this->db->last_query(); exit;   
		}
        return true;
    }

    function updateNotificationTable($id) {
        $data['is_seen'] = '1';
        $this->db->where('id', $ids);
        $this->db->update('notification', $data);
        return true;
    }

    function requestConnectList($id) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = connected.mid', 'left');
		$this->db->join('member_image', 'member_image.mid = connected.mid and member_image.type = "profile_img"', 'left');
		$this->db->join('ar_members', 'ar_request_access_page.mid = ar_members.mid', 'Inner');
       
        $this->db->from('connected');
       $this->db->where('ar_members.status', '1');
		 $this->db->where('connected.connected_id', $id);
        $this->db->where('connected.is_accepted', '0');
        $query = $this->db->get();
	//echo 	$this->db->last_query();
        $res = $query->result_array();
        return $res;
    }

    function acceptRequestForConnection($cid = '') {
        $data['is_accepted'] = '1';
        $this->db->where('cid', $cid);
        $this->db->update('connected', $data);
        return true;
    }

    function requestConnectionSeen($cid = '') {
        $data['is_seen'] = '1';
        $this->db->where('cid', $cid);
        $this->db->update('connected', $data);
        return true;
    }

    function getUnreadMessage($mid) {
        $this->db->select('*');
        $this->db->join('ar_message', 'ar_message.msg_id = ar_message_trans.msg_id', 'left');
		$this->db->join('member_image', 'member_image.mid = ar_message_trans.sender_id and member_image.type = "profile_img"', 'left');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = ar_message_trans.sender_id', 'left');
		
        $this->db->from('ar_message_trans');
        $this->db->where('ar_message_trans.msg_read', '0');
        $this->db->where('ar_message_trans.receiver_id', $mid);
		//$this->db->where('ar_message_trans.sender_id != null');
		$this->db->where('ar_message_trans.receiver_delete', '0');
		$this->db->order_by("msg_time", "desc");
		$this->db->limit(5,0);
		
        $query = $this->db->get();
	//	echo $this->db->last_query();
       $res= $query->result_array();
	$i=0;
		$sug_member=array();
		foreach ($res as $key=>$value){
			foreach ($value as $k=>$val){
				$sug_member[$i][$k]=$val;
			}
			 $sug_member[$i]['msg_time']=  calculateTimeFromPost( $sug_member[$i]['msg_time'], date("Y-m-d H:i:s"), "s");
			//
		$i++;	
		}
        //echo $this->db->last_query(); exit;   
       return $sug_member;
	 //  print_r($res);die();
       // return $res;
    }

    function getReadMessage($mid, $count) {
        $this->db->select('*');
        $this->db->join('ar_message', 'ar_message.msg_id = ar_message_trans.msg_id', 'left');
		$this->db->join('member_image', 'member_image.mid = ar_message_trans.sender_id and member_image.type = "profile_img"', 'left');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = ar_message_trans.sender_id', 'left');
		 $this->db->join('ar_members', 'ar_request_access_page.mid = ar_members.mid', 'Inner');
        
        $this->db->from('ar_message_trans');
        $this->db->where('msg_read', '1');
		 $this->db->where('ar_members.status', '1');
        $this->db->where('receiver_id', $mid);
        $this->db->order_by("msg_time", "desc");
        $this->db->limit($count);
        $query = $this->db->get();
       $res= $query->result_array();
		/* $i=0;
		$sug_member=array();
		foreach ($res as $key=>$value){
			foreach ($value as $k=>$val){
				$sug_member[$i][$k]=$val;
			}
			 $sug_member[$i]['msg_time']=  calculateTimeFromPost( $sug_member[$i]['msg_time'], date("Y-m-d H:i:s"), "s");
			//
		$i++;	
		}*/
		
        //echo $this->db->last_query(); exit;   
        //return $sug_member;
		return $res;
    }

    function iviewednotification($id) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = notification.whose_profile_viewed', 'left');
        $this->db->join('member_image', 'member_image.mid = notification.whose_profile_viewed and member_image.type = "profile_img"', 'left');
        $this->db->from('notification');
        $this->db->where('notification.who_viewed', $id);
        $this->db->group_by('notification.whose_profile_viewed');
        $this->db->order_by('notification.id', 'desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function meviewednotification($id) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = notification.who_viewed', 'left');
        $this->db->join('member_image', 'member_image.mid = notification.who_viewed and member_image.type = "profile_img"', 'left');
        $this->db->from('notification');
        $this->db->where('notification.whose_profile_viewed', $id);
        $this->db->group_by('notification.who_viewed');
        $this->db->order_by('notification.id', 'desc');
        $this->db->limit(5);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function loadMoreWhoViewedList($id) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = notification.who_viewed', 'left');
        $this->db->join('member_image', 'member_image.mid = notification.who_viewed and member_image.type = "profile_img"', 'left');
        $this->db->from('notification');
        $this->db->where('notification.whose_profile_viewed', $id);
        $this->db->group_by('notification.who_viewed');
        $this->db->order_by('notification.id', 'desc');
        $this->db->limit(50);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function loadMoreIViewedList($id) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = notification.whose_profile_viewed', 'left');
        $this->db->join('member_image', 'member_image.mid = notification.whose_profile_viewed and member_image.type = "profile_img"', 'left');
        $this->db->from('notification');
        $this->db->where('notification.who_viewed', $id);
        $this->db->group_by('notification.whose_profile_viewed');
        $this->db->order_by('notification.id', 'desc');
        $this->db->limit(50);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function memberdetailswithnotification($mid) {
        if ($mid != null) {
            $sql = "SELECT * FROM ar_members left join ar_request_access_page on ar_members.mid=ar_request_access_page.mid WHERE ar_members.mid='" . $mid . "'";
            $query = $this->db->query($sql);
            $rs = $query->result_array();
            $data = $rs[0];
            $id = $rs[0]['mid'];
            $data['back_image'] = $this->BackImage($id);
            $data['fornt_image'] = $this->ProfileImage($id);
            $data['iviewed'] = $this->iviewednotification($id);
            $data['meviewed'] = $this->meviewednotification($id);
            return $data;
        }
    }

    function getSuggestionList($id) {
        $this->db->select('*');
        $this->db->join('ar_request_access_page', 'ar_request_access_page.mid = connected.mid', 'left');
        $this->db->join('member_image', 'member_image.mid = connected.mid and member_image.type = "profile_img"', 'left');
        $this->db->from('connected');
        $this->db->where('connected.connected_id', $id);
        $this->db->where('connected.is_accepted', '1');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function deleteSuggestion($mid,$cid) {
        $this->db->where('cid', $cid);
        $this->db->where('mid', $mid);
        $this->db->delete('connected');
        return true;
    }
	
	function deleteConnection($mid,$cid) {
        //$this->db->where('connected_id', $cid);
        //$this->db->where('mid', $mid);
		$this->db->where('(mid='.$mid.' and connected_id='.$cid.')');
		$this->db->or_where('(mid='.$cid.' and connected_id='.$mid.')');
        $this->db->delete('connected');
        return true;
    }
	
    function toplineofMember($id) {
		if($id!=''){
			$this->load->helper('common_helper');
			$sql = 'select * from ar_member_data_wiz_one  WHERE mid=' . $id;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) {
				$rs = $query->result_array();
				return topline_name(substr($rs[0]['mem_code'], 0, 5));
			}
		}
    }

    function getAllTag() {
        $this->db->select('*');
        $this->db->from('ar_tag');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function post_hub($data) {
        $this->db->insert('ar_hub', $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    function check_member_payment_status($mid) {
        $this->db->select('user_type');
        $this->db->from('ar_members');
        $this->db->where('mid', $mid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function check_member_type() {
        $mid = $this->session->userdata['logged_in']['id'];
        $this->db->select('user_type');
        $this->db->from('ar_members');
        $this->db->where('mid', $mid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function getBusinessNameById($mid) {

        $this->db->select('bussinessname');
        $this->db->from('ar_request_access_page');
        $this->db->where('mid', $mid);
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function shareCodeExist($mid) {
        $this->db->select('sharecode');
        $this->db->from('ar_members');
        $this->db->where('mid', $mid);
        $query = $this->db->get();
        $res = $query->row();
        if ($res->sharecode != '') {
            return 'true';
        } else {
            return 'false';
        }
    }

    function shareCode($mid, $code) {
        $data['sharecode'] = $code;
        $this->db->where('mid', $mid);
        $this->db->update('ar_members', $data);
        return true;
    }

    function updateMemberStatus($id) {
        $data['user_type'] = 'Premium';
        $this->db->where('mid', $id);
        $this->db->update('ar_members', $data);
        //$this->db->last_query(); 
        return true;
    }

    function getUserRegistrationDate($mid) {
        $sql = "SELECT * FROM ar_members where mid='" . $mid . "'";
        $query = $this->db->query($sql);
        $rs = $query->result_array();
        if ($query->num_rows() > 0) {

            return $rs[0]['update_date'];
        }
    }

    function fetch_yesterdays_member($dt) {
        $lwd = $dt . " 00:00:00";
        $upd = $dt . " 23:59:59";

        $this->db->select('*');
        $this->db->from('ar_members');
        $this->db->where('update_date >', $lwd);
        $this->db->where('update_date <', $upd);
        $this->db->where('status', '0');
        $query = $this->db->get();
        $res = $query->result_array();
        return $res;
    }

    function isFoundingMember($mid) {
        $this->db->select('subscription_type');
        $this->db->from('ar_transaction');
        $this->db->where('subscription_type', 1);
        $this->db->where('mid', $mid);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->row();
        return $res;
    }

    function updateMessageSeenTable($data) {
        $this->db->where('mt_id', $data['mt_id']);
        $this->db->update('ar_message_trans', $data);
        $res = $this->db->affected_rows();
        if ($res == 1) {
            return true;
        } else {
            return false;
        }
    }

    function userUpgrationStatus($mid) {

        $this->db->select('subscription_type');
        $this->db->from('ar_transaction');
        //$this->db->where('subscription_type',1);
        $this->db->where('mid', $mid);
        $this->db->order_by('id', 'desc');
        $this->db->limit(1);
        $query = $this->db->get();
        $res = $query->row();
        return $res;
    }

    function updatelatlanall() {
        $this->db->select('zipcode,mid');
        $this->db->from('ar_request_access_page');
        $this->db->where('zipcode !=', ' ');
        $query = $this->db->get();
        $res = $query->result_array();
        $d = array();
        echo count($res);
        foreach ($res as $zipcode) {
            $path = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $zipcode['zipcode'];
            $url = file_get_contents($path);
            $address = json_decode($url);
            if ($address->status == 'OK') {
                echo '<br />' . $path;
                echo '<br />' . $address->status;
                $d['Latitude'] = $address->results[0]->geometry->location->lat;
                $d['Longitude'] = $address->results[0]->geometry->location->lng;
                //$d['mid']=$zipcode['mid'];
                $this->db->where('mid', $zipcode['mid']);
                $this->db->update('ar_request_access_page', $d);
            }
        }
        return 1;
    }
    function getIdByUserEmail($data){

        $this->db->select('mid');
        $this->db->where('email', $data["user_email"]);
        $query = $this->db->get('ar_members');
        $result = $query->row();
        return $result;
    }   
	
	 function getIdDetailsByUserEmail($data){

        $this->db->select('fname,lname,mid');
        $this->db->where('email', $data["user_email"]);
        $query = $this->db->get('ar_members');
		//echo $this->db->last_query();die();
        $result = $query->row_array();
        return $result;
    }   
	
	 function getDetailsById($mid){

        $this->db->select('*');
        $this->db->where('mid', $mid);
        $query = $this->db->get('ar_members');
		//echo $this->db->last_query();die();
        $result = $query->row_array();
        return $result;
    }   
    
    //Social Login account check
    function socialUserValidation($data=array()){
               
        //print_r($data);exit();
        $rs = '';
        
        $this->db->select('mid');
        $this->db->from('ar_members');
        $this->db->where('email', $data['email']);
        $this->db->where('social_id', $data['social_id']);
        $query = $this->db->get();

        //echo $this->db->last_query();exit();

        if ($query->num_rows() > 0) {
            $row = $query->row();
            $rs  = $row->mid;
        }

        return $rs;
    }
    //Add Social Member Data
    function insertSocialMemberData($data=array()) {
        
        $rs = '';
        $d= array();

        $result = $this->membervalidate($data['email']);
        if ($result != 'TRUE') {
           
            $d['email']             = $data['email'];           
            $d['social_id']         = $data['social_id'];
            $d['type']              = $data['type'];
            $d['verification_code'] = md5($data['email']);

            //print_r($d);exit();

            $this->db->insert('ar_members', $d);            
            $id = $this->db->insert_id();
            $m['mid'] = $id;
           
            $this->db->insert('ar_request_access_page', $m);
            if ($data['referal'] != '0'){
                $referals_data = $this->getReferalData($data['referal']);
                $insert_data['connected_id']    = $referals_data->mid;
                $insert_data['mid']             = $id;
                $insert_data['is_seen']         = 1;
                $insert_data['is_accepted']     = 1;
                $this->db->insert('connected', $insert_data);

                $referal_data['mid']            = $insert_data['connected_id'];
                $referal_data['ref_mid']        = $insert_data['mid'];
                $referal_data['ref_code']       = $data['referal'];
                $this->db->insert('ar_referral', $referal_data);
            }
                $rs = $id;
        } 

        return $rs; 
    }

	function isBuisnessNameExist($mid)
	{
	   $this->db->select('fname');
	   $this->db->from('ar_members');
       $this->db->where('mid', $mid);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $bname  = $row->fname;
			
			if($bname!='')
			{
				return 1;
			}
			else
			{
				return 0;
			}
        }
		else
		{
			return 0;
		}

    }
    function updateUserName($data,$user_id)
    {
        $this->db->where('mid',$user_id);
        $res = $this->db->update('ar_members',$data);
        return $res;
    }
	
	function getMemberStatus($id){
		  $this->db->select('status');
		  $this->db->from('ar_members');
		  $this->db->where('mid',$id);
		  $query = $this->db->get();
		  $res = $query->result_array();  
		  return $res[0]['status'];
	}
	function isBusinessNameuniqe($str){
		$this->db->from('ar_request_access_page');
		$this->db->where('bussinessname',$str);
		$query = $this->db->get();
        if ($query->num_rows() > 0) {
			return 1;
		}else{
			return 0;
		}
	}
	
	function FnFindInActiveUsers($tendaysagodate,$seventeendaysagodate,$thitrytwodaysagodate)
	{
		$this->db->select('fname,email,last_login_time,mid');
        $this->db->from('ar_members');
		//$this->db->where('status',1);
        $this->db->where('last_login_time', $tendaysagodate);
        $this->db->or_where('last_login_time', $seventeendaysagodate);
		$this->db->or_where('last_login_time', $thitrytwodaysagodate);	
		$this->db->order_by("mid", "desc");
        $query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
	}
	
	
	function FnFindAllActiveUsers($fivedaysagodate)
	{
		$this->db->select('mid,email');
        $this->db->from('ar_members');
		$this->db->where('last_login_time <=',$fivedaysagodate);
		$this->db->where('weekly_bing_news_sub_status',1);
//$this->db->limit(3,0);
		$this->db->order_by("mid", "desc");
		
        $query = $this->db->get();
//echo $this->db->last_query(); die();
		return $query->result_array();
	}
	
	function FnFindNewsBySubSector($subsectorsarr)
	{
		$this->db->select('news.title,news.url,news.id,image.image_url');
       	$this->db->join('news_image as image','image.news_id=news.id','Inner');
		$this->db->from('ar_apinews as news');
		$this->db->where_in('news.sub_sector_id',$subsectorsarr);
		$this->db->where('image.image_url !=','');
		$this->db->limit(5,0);
        $this->db->order_by("news.on_date", "desc");
        $query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result_array();
	}
	
	function FnGetNewsDetails($news_id)
	{
		$this->db->select('news.title,news.url,news.id,image.image_url,news.description');
       	$this->db->join('news_image as image','image.news_id=news.id','Inner');
		$this->db->from('ar_apinews as news');
		$this->db->where('image.news_id',$news_id);
		
        $query = $this->db->get();
		//echo $this->db->last_query();
		return $query->row_array();
	}
	
	function FnGetSubscribedUser()
	{
		$this->db->select('mid,email');
		$this->db->where('subscription_status',1);
		$this->db->where('status','1');
        $this->db->from('ar_members');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function FnUnSubscribeUser($email)
	{
		$this->db->select('mid');
		$this->db->where('status','1');
		$this->db->where('email',$email);
        $this->db->from('ar_members');
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		$num_rows= $query->num_rows();
		if($num_rows==1)
		{
			 $sql = "Update ar_members Set subscription_status='0' where email='".$email."'";
             $query = $this->db->query($sql);
			 return 'success';
		}
		else
		{
			return 'failure';
		}
	}
	
	function userLocation($id){
		
		$this->db->select('*');
		$this->db->from('ar_request_access_page');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rsb = $query->result_array(); 
		return $rsb[0]['city'].','.countryName($rsb[0]['country']);
		//$data['city'] = $rsb[0]['city'];
		//$data['country'] = $rsb[0]['country'];
		
	}
	
	

}

//end