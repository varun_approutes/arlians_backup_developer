<?php
class model_category extends CI_Model 
{
	
	
	function __construct()
	{
		parent::__construct();
	}
//----------------- Start function to get all principal list -----------------------------//
	function GetAllList()
	{
		$data=array();
		//if($this->session->userdata('admin_user_type')=="Super_Admin")
		//{
			$data=array();
			$sql = "select 	* from ar_top_lines";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	
		//}
		//else
		//{
			/*$school_id=$this->session->userdata('admin_school_id');
			$this->db->select('school.school_name,school.type,student.*');
			$this->db->from('school');
			$this->db->from('student');
			$this->db->where('student.school_id',$school_id);
			$this->db->where('student.school_id = school.id');
			$this->db->where('school.status','1');
			$q=$this->db->get();*/
		//}
		
	}
	function GetAllSectorList($toplineid)
	{
		$data=array();
			$sql = "select 	distinct(ar_sector.sector_name),ar_sector.sector_code,ar_sector.profile_image from ar_sector";
			if($toplineid!=null){
			$sql .= " left join ar_relation on ar_sector.sector_code = ar_relation.sector_code where ar_relation.top_code=".$toplineid;
			}
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	
		//}
		//else
		//{
			/*$school_id=$this->session->userdata('admin_school_id');
			$this->db->select('school.school_name,school.type,student.*');
			$this->db->from('school');
			$this->db->from('student');
			$this->db->where('student.school_id',$school_id);
			$this->db->where('student.school_id = school.id');
			$this->db->where('school.status','1');
			$q=$this->db->get();*/
		//}
		
	}
//------------------ End function to get all principal list ------------------------------//
//------------------ Start function to get school Id ---------------------------------//

	function GetSchoolList()
	{
		$data=array();
		$this->db->where('status','1');
		$q=$this->db->get('school');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------ End function to to get school Id ------------------------------//

//--------------------- Start section to get Student's Section Details ----------------------------//
	function GetSection()
	{
		$data=array();
		$this->db->where('status','1');
		$q=$this->db->get('section');
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}
//----------------------- End section to get Student's Section Details -----------------------------//

//------------------ Start function to insert school details ---------------------------------//
	function insert()
	{
		$error=0;
		if($this->input->post('email')!='')
		{
			$this->db->where('email',$this->input->post('email'));
			$exist_email=$this->db->get('student');
			if($exist_email->num_rows>0)
			{
				$this->session->set_userdata('error_msg','This email is already exist. Please try another.');
				$return=0;
				$error=1;
			}
		}
		if($this->input->post('phone')!='')
		{
			$this->db->where('phone_no',$this->input->post('phone'));
			$exist_phone=$this->db->get('student');
			if($exist_phone->num_rows>0)
			{
				$this->session->set_userdata('error_msg','This phone number is already exist. Please try another.');
				$return=0;
				$error=1;
			}
		}
		if($error==0)
		{
			$image_name='';
			if($this->input->post('school_id')!=0)
			{
				$school_id=$this->input->post('school_id');
			}
			else
			{
				$school_id=$this->input->post('school_name');
			}
			$registration_number=$this->model_student->getRegistrationNumber($this->input->post('school_id'));
			//echo $registration_number.'<br/>';
			$admission_date = date('Y-m-d',strtotime($this->input->post('admission_date')));
			$this->db->where('school_id',$this->input->post('school_id'));
			$this->db->where('class',$this->input->post('class'));
			$this->db->order_by('roll_no','DESC');
			$this->db->limit(1,0);
			$get_roll=$this->db->get('student');
			if($get_roll->num_rows>0)
			{
				foreach($get_roll->result() as $roll)
				{
					$new_roll=$roll->roll_no+1;
				}
			}
			else
			{
				$new_roll=1;
			}
			//echo $new_roll;die;
			if($_FILES['image']['name']!='')
			{
				$image_name="image";
				$config['upload_path'] = 'uploads/student_image/'; 
				$config['allowed_types'] = 'gif|jpg|jpeg|png'; 
				$config['max_size'] = '1000'; 
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $config); 
				if(!$this->upload->do_upload($image_name)) 
				{
				 	//echo $this->upload->display_errors();die; 
				 	$this->session->set_userdata('error_msg',$this->upload->display_errors());
				 	$return=0;
				 	return $return;
				}
				else 
				{ 
					$fInfo = $this->upload->data();
					$image_name=$fInfo['file_name'];
					$config1 = array(
						  'source_image' => 'uploads/student_image/'.$fInfo['file_name'], //get original image
						  'new_image' => 'uploads/student_image/thumb/', //save as new image //need to create thumbs first
						  'maintain_ratio' => true,
						  'width' => 150,
						  'height' => 150
						);
					$this->load->library('image_lib', $config1); //load library
					$this->image_lib->resize(); //generating thumb
					//echo $this->image_lib->display_errors(); die;
				}
			}
			$insert_array=array(
				'school_id' => $school_id,			
				'name' => $this->input->post('name'),
				'class' => $this->input->post('class'),
				'roll_no' => $new_roll,
				'reg_no' => $registration_number,
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password')),
				'address' => $this->input->post('address'),
				'phone_no' => $this->input->post('phone'),
				'admission_date' => $admission_date,
				'student_image' => $image_name,
				'status' => $this->input->post('status'),
			);
			$insert=$this->db->insert('student',$insert_array);

			if($insert)
			{

				$this->session->set_userdata('success_msg','Data insert successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to insert data.Please try again.');
				$return=0;
			}
		}
		return $return;
	}
//-------------------- End function to insert school details ---------------------------------//
//------------------- Start function to get auto generated registration number -----------------------------//
	function getRegistrationNumber($school_id)
	{
		$six_digit_random_number = mt_rand(100000, 999999);
		$get_number='SC'.$school_id.'-'.$six_digit_random_number;
		$this->db->where('reg_no',$get_number);
		$get=$this->db->get('student');
		if($get->num_rows>0)
		{
			$this->model_student->getRegistrationNumber($school_id);
		}
		else
		{
			return $get_number;
		}
	}
//---------------------- End function to get auto generated registration number ---------------------------//
//------------------------ Start function to get details of student in edit ---------------------------------//

	function edit($id)
	{
		$data=array();
		
		$this->db->from('ar_top_lines');
		$this->db->where('top_code',$id);
		$q=$this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{
				$data[]=$result;
			}
		}
		return $data;
	}
	function sectoredit($id)
	{
		$data=array();
		
		$this->db->from('ar_sector');
		$this->db->where('sector_code',$id);
		$q=$this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------------- End function to get details of student in edit -----------------------------------//

//------------------------ Start function to update Student Details ----------------------------------//

function update()
	{
		$error=0;
		$toplineid=$this->input->post('toplineid');

		if($error==0)
		{
			$image_name='';
			//echo $new_roll;die;
			if($_FILES['image']['name']!='')
			{
				$image_name="image";
				$config['upload_path'] = 'uploads/profile_image/'; 
				$config['allowed_types'] = 'gif|jpg|jpeg|png'; 
				$config['max_size'] = '1000'; 
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $config); 
				if(!$this->upload->do_upload($image_name)) 
				{
				 	//echo $this->upload->display_errors();die; 
				 	$this->session->set_userdata('error_msg',$this->upload->display_errors());
				 	$return=0;
				 	return $return;
				}
				else 
				{ 
					$fInfo = $this->upload->data();
					$image_name=$fInfo['file_name'];
					$config1 = array(
						  'source_image' => 'uploads/profile_image/'.$fInfo['file_name'], //get original image
						  'new_image' => 'uploads/profile_image', //save as new image //need to create thumbs first
						  'maintain_ratio' => true,
						  'width' => 109,
						  'height' => 86,
						  'chmod' => 0777
						);
					$this->load->library('image_lib', $config1); //load library
					$this->image_lib->resize(); //generating thumb
					
					//echo $this->image_lib->display_errors(); die;
					$update_image=array('profile_image' => $image_name);
					$this->db->where('top_code',$toplineid);
					$this->db->update('ar_top_lines',$update_image);
					if($this->input->post('exist_image')!='')
					{
						$image_path='uploads/profile_image/'.$this->input->post('exist_image');
						
						if(file_exists($image_path))
						{
							unlink($image_path);
						}
						
					}
				}
			}
			$update_array=array(
				
				'top_name' => $this->input->post('name')
				
			);
			$this->db->where('top_code',$toplineid);
			$update=$this->db->update('ar_top_lines',$update_array);

			if($update)
			{

				$this->session->set_userdata('success_msg','Data update successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to update data.Please try again.');
				$return=0;
			}
		}
		return $return;
	}
	
	
	function sectorupdate()
	{
		$error=0;
		$sectorid=$this->input->post('sectorid');

		if($error==0)
		{
			$image_name='';
			//echo $new_roll;die;
			if($_FILES['image']['name']!='')
			{
				$image_name="image";
				$config['upload_path'] = 'uploads/profile_image/'; 
				$config['allowed_types'] = 'gif|jpg|jpeg|png'; 
				$config['max_size'] = '1000'; 
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $config); 
				if(!$this->upload->do_upload($image_name)) 
				{
				 	//echo $this->upload->display_errors();die; 
				 	$this->session->set_userdata('error_msg',$this->upload->display_errors());
				 	$return=0;
				 	return $return;
				}
				else 
				{ 
					$fInfo = $this->upload->data();
					$image_name=$fInfo['file_name'];
					$config1 = array(
						  'source_image' => 'uploads/profile_image/'.$fInfo['file_name'], //get original image
						  'new_image' => 'uploads/profile_image', //save as new image //need to create thumbs first
						  'maintain_ratio' => true,
						  'width' => 109,
						  'height' => 86,
						  'chmod' => 0777
						);
					$this->load->library('image_lib', $config1); //load library
					$this->image_lib->resize(); //generating thumb
					
					//echo $this->image_lib->display_errors(); die;
					$update_image=array('profile_image' => $image_name);
					$this->db->where('sector_code',$sectorid);
					$this->db->update('ar_sector',$update_image);
					if($this->input->post('exist_image')!='')
					{
						$image_path='uploads/profile_image/'.$this->input->post('exist_image');
						
						if(file_exists($image_path))
						{
							unlink($image_path);
						}
						
					}
				}
			}
			$update_array=array(
				
				'sector_name' => $this->input->post('name')
				
			);
			$this->db->where('sector_code',$sectorid);
			$update=$this->db->update('ar_sector',$update_array);

			if($update)
			{

				$this->session->set_userdata('success_msg','Data update successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to update data.Please try again.');
				$return=0;
			}
		}
		return $return;
	}
//-------------------------- End function to update Student Details ----------------------------------//
//------------------------ Start function to change status in Student Management ------------------------------//
	function update_status()
	{
		if($this->input->post('status')==1)
		{
			$updated_status='0';
		}
		if($this->input->post('status')==0)
		{
			$updated_status='1';
		}
		$status_change=array('status' => $updated_status);
		$this->db->where('mid',$this->input->post('edit_id'));
		$update=$this->db->update('ar_members',$status_change);
		if($update==1)
		{
			$this->session->set_userdata('success_msg','Status update successfully.');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Failed to update status.Please try again.');
			$return=0;
		}
		return $return;
	}
//-------------------------- End function to change status in Student Management ------------------------------//

//---------------------- Start function to delete school details permanently -------------------------------//
	function delete()
	{
		$image_name='';
		$this->db->where('id',$this->input->post('edit_id'));
		$get=$this->db->get('student');
		if($gqt->num_rows>0)
		{
			foreach ($get->result() as $result) 
			{	
				$image_name=$result->student_image;
			}
		}
		if($image_name!='')
		{
			$unlink_main_path='uploads/student_image/'.$image_name;
			$unlink_thumb_path='uploads/student_image/thumb/'.$image_name;
			if(file_exists($unlink_main_path))
			{
				unlink($unlink_main_path);
			}
			if(file_exists($unlink_thumb_path))
			{
				unlink($unlink_thumb_path);
			}
		}
		$this->db->where('id',$this->input->post('edit_id'));
		$delete=$this->db->delete('student');
		if($delete)
		{
			$this->session->set_userdata('success_msg','Data deleted successfully');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Failed to delete data. Please try again.');
			$return=0;
		}
		return $return;
	}
//-------------------------- ENd function to delete school details permanently ----------------------------------//
//------------------------- Start function to get no of years -----------------------------//
	function GetYear($school_id)
	{
		$years='';
		$this->db->where('id',$school_id);
		$q=$this->db->get('school');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$years=$result->no_of_years;
			}
		}
		return $years;
	}
//--------------------------- End function to get no of years ----------------------------//

	 function topline_name($code){
	if($code!=''){
		  $sql = "select * from ar_top_lines where top_code=".$code;

		 $query = $this->db->query($sql);

		 $rs = $query->row();
		// var_dump($rs->top_name);
		 return $rs->top_name;
		}else{
			return 'Please insert Industry name';
		}
	 }
}
?>