<?php
class model_calendar extends CI_Model 
{
	
	
	function __construct()
	{
		parent::__construct();
	}
//------------------------ Start section to see the total class list ------------------------------------//
	function AllClass()
	{
		$data=array();
		$school_id=$this->session->userdata('admin_school_id');
		$this->db->where('school_id',$school_id);
		$this->db->where('status','1');
		$q=$this->db->get('class_topic');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//--------------------------- End function to see the total class list -----------------------------------//
//--------------------------- hoday list start-----------------------------------//
	function holiday_list()
	{
		//$school_id=$this->session->userdata('admin_school_id');
		$data=array();
		$school_id=$this->session->userdata('admin_school_id');
		$this->db->where('school_id',$school_id);		
		$q=$this->db->get('holidays');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}	
//--------------------------- hoday list end -----------------------------------//
	function holiday_insert()
	{
	    $date = date_create($this->input->post('holiday_date'));
	    $formated_date = date_format($date,"Y-m-d");
		$insert_array=array(
				'school_id' => $this->input->post('school_id'),			
				'title' => $this->input->post('title'),				
				'description' => $this->input->post('description'),
				'date' => $formated_date,				
			);
			$insert=$this->db->insert('holidays',$insert_array);
			if($insert)
			{

				$this->session->set_userdata('success_msg','Data insert successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to insert data.Please try again.');
				$return=0;
			}
		return $return;

	}
//-------------------------- Start function to update holiday details --------------------------------//
	function UpdateHoliday()
	{
		$update_array=array(
			'title' => $this->input->post('edit_holiday_title'),
			'description' => $this->input->post('edit_holiday_description')
		);
		$this->db->where('id',$this->input->post('holiday_id'));
		$update=$this->db->update('holidays',$update_array);
		if($update)
		{
			$this->session->set_userdata('success_msg','Holiday details updated successfully.');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Sorry! There is an error to update holiday details.');
			$return=0;
		}
		return $return;
	}
//---------------------------- End function to update holiday details ----------------------------------//
//-------------------- Start function to get assign teacher list -------------------------------//
	function GetAllTeacher($school_id)
	{
		$data=array();
		$this->db->where('school_id',$school_id);
		$this->db->where('type','1');
		$this->db->where('status','1');
		$q=$this->db->get('principal_teacher');
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//---------------------- End function to get assign teacher list -------------------------------//
//---------------------- update Class topic -------------------------------//
function update_class_module()
	{
		$error=0;
		$edit_id=$this->input->post('topic_id');
		$get_start_date_time=str_replace('-','/',$this->input->post('select_date')).' '.$this->input->post('start_time');
		$get_end_date_time=str_replace('-','/',$this->input->post('select_date')).' '.$this->input->post('end_time');
		$new_start_time=date('Y-m-d H:i:s',strtotime($get_start_date_time));
		$new_end_time=date('Y-m-d H:i:s',strtotime($get_end_date_time));

	if(($get_start_date_time!='') && ($get_end_date_time!=''))
		{
			$this->db->where('school_id',$this->input->post('class_school_id'));
			//$this->db->where('id',$this->input->post('topic_id'));
			$this->db->where("((`start_time` BETWEEN '".$new_start_time."' AND '".$new_end_time."') OR (`end_time` BETWEEN '".$new_start_time."' AND '".$new_end_time."'))");
			$this->db->where('id =',$edit_id);
			$q=$this->db->get('class_topic');
			if($q->num_rows>0)
			{
				$this->session->set_userdata('error_msg','This class is already scheduled within this time range.');
				$error=1;
				$return=0;
				return $return;
			}
			
		}

		if($this->input->post('select_date')!='')
		{
			$select_date = str_replace('-','/',$this->input->post('select_date'));			
			$new_date_format = date("Y-m-d",strtotime($select_date));			
			$this->db->where('date =',$new_date_format);
			$holiday_available=$this->db->get('holidays');
			if($holiday_available->num_rows>0)
			{
				$this->session->set_userdata('error_msg','This Date Already Assign As A Holidays.');
				$error=1;
				$return=0;
				return $return;
			}
			
		}

		if($error==0)
		{
			$update_array=array(				
				'topic_name' => $this->input->post('topic_name'),
				'topic_description' => $this->input->post('class_topic_description'),			
				'start_time' => $new_start_time,
				'end_time' => $new_end_time,
				'assigned_teacher' => $this->input->post('teacher_id')				
			);
			$this->db->where('id',$edit_id);
			$update=$this->db->update('class_topic',$update_array);
			//echo $this->db->last_query();die;
			if($update)
			{
				$this->session->set_userdata('success_msg','Class topic updated successfully.');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to update class topic. Please try again.');
				$return=0;
			}
			return $return;
		}
   }
//---------------------- update Class topic end -------------------------------//
}
?>