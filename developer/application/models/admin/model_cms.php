<?php
class model_cms extends CI_Model 
{
	
	
	function __construct()
	{
		parent::__construct();
	}
//----------------- Start function to get all principal list -----------------------------//
	function GetAllList()
	{
		$data=array();
		//if($this->session->userdata('admin_user_type')=="Super_Admin")
		//{
			
			$this->db->from('ar_cms');
			$q=$this->db->get();
		/*}
		else
		{
			$school_id=$this->session->userdata('admin_school_id');
			$this->db->select('school.school_name,school.type,student.*');
			$this->db->from('school');
			$this->db->from('student');
			$this->db->where('student.school_id',$school_id);
			$this->db->where('student.school_id = school.id');
			$this->db->where('school.status','1');
			$q=$this->db->get();
		}*/
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------ End function to get all principal list ------------------------------//
//------------------ Start function to get school Id ---------------------------------//
 function text_clean($string) {
	$string = strtolower(trim($string));
   $string = str_replace(' ', '_', $string); // Replaces all spaces with hyphens.
   return preg_replace('/[^A-Za-z0-9_\-]/', '', $string); // Removes special chars.
}
	function insert()
	{
		//$error=0;
		
		
		//if($error==0)
		//{
			
			$insert_array=array(
					
				'cms_title' => $this->input->post('title'),
				'cms_containt' => htmlentities($this->input->post('content')),
				'cms_url' =>  $this->text_clean($this->input->post('title')),
				'cms_status' => $this->input->post('status'),
				
				
			);
			$insert=$this->db->insert('ar_cms',$insert_array);

			if($insert)
			{

				$this->session->set_userdata('success_msg','Data insert successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to insert data.Please try again.');
				$return=0;
			}
		
		return $return;
	}
	
//---------------------- End function to get auto generated registration number ---------------------------//
//------------------------ Start function to get details of student in edit ---------------------------------//

	function edit($mid)
	{
		$data=array();
		$this->db->from('ar_cms');
		$this->db->where('cms_id',$mid);
		$q=$this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------------- End function to get details of student in edit -----------------------------------//

//------------------------ Start function to update Student Details ----------------------------------//
	function update()
	{
		$error=0;
		$cms_id=$this->input->post('cms_id');
		
		
		//if($error==0)
		//{
			
			
			//echo $new_roll;die;

			$update_array=array(
				
				'cms_title' => $this->input->post('title'),
				'cms_containt' => htmlentities($this->input->post('content')),
				'cms_status' => $this->input->post('status'),
			);
			$this->db->where('cms_id',$cms_id);
			$update=$this->db->update('ar_cms',$update_array);

			if($update)
			{

				$this->session->set_userdata('success_msg','Data update successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to update data.Please try again.');
				$return=0;
			}
		//}
		return $return;
	}
//-------------------------- End function to update Student Details ----------------------------------//
//------------------------ Start function to change status in Student Management ------------------------------//
	function update_status()
	{
		if($this->input->post('status')==1)
		{
			$updated_status='0';
		}
		if($this->input->post('status')==0)
		{
			$updated_status='1';
		}
		$status_change=array('cms_status' => $updated_status);
		$this->db->where('cms_id',$this->input->post('edit_id'));
		$update=$this->db->update('ar_cms',$status_change);
		if($update==1)
		{
			$this->session->set_userdata('success_msg','Status update successfully.');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Failed to update status.Please try again.');
			$return=0;
		}
		return $return;
	}
//-------------------------- End function to change status in Student Management ------------------------------//

//---------------------- Start function to delete school details permanently -------------------------------//
	function delete()
	{
		$image_name='';
		$this->db->where('id',$this->input->post('edit_id'));
		$get=$this->db->get('ar_cms');
		if($gqt->num_rows>0)
		{
			foreach ($get->result() as $result) 
			{	
				$image_name=$result->student_image;
			}
		}
		if($image_name!='')
		{
			$unlink_main_path='uploads/student_image/'.$image_name;
			$unlink_thumb_path='uploads/student_image/thumb/'.$image_name;
			if(file_exists($unlink_main_path))
			{
				unlink($unlink_main_path);
			}
			if(file_exists($unlink_thumb_path))
			{
				unlink($unlink_thumb_path);
			}
		}
		$this->db->where('id',$this->input->post('edit_id'));
		$delete=$this->db->delete('student');
		if($delete)
		{
			$this->session->set_userdata('success_msg','Data deleted successfully');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Failed to delete data. Please try again.');
			$return=0;
		}
		return $return;
	}

}
?>