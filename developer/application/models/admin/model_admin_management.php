<?php
class model_admin_management extends CI_Model 
{
	
	
	function __construct()
	{
		parent::__construct();
	}
//----------------- Start function to get all school list -----------------------------//
	function GetAllList()
	{
		$data=array();
		//$this->db->where('status','1');
		//$this->db->get('school');
		//$this->db->get('admin');
		$this->db->select('school.school_name,school.school_address,admin.*');
		//$this->db->where('school_id',$this->input->post('edit_id'));
		$this->db->from('school');
		$this->db->from('admin');
		//$this->db->where('school.id',$this->input->post('edit_id'));
		$this->db->where('admin.id = school.admin_id');
		$q = $this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------ End function to get all school list ------------------------------//



//---------------------- Start function to get the all details in Edit Section -----------------------------//
	function edit($admin_id)
	{
		$data=array();
		/*$this->db->where('id',$school_id);
		$q=$this->db->get('school');
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) {
				$data[]=$result;
			}
		}
		return $data;*/
		$this->db->select('school.school_name,admin.*');
		//$this->db->where('school_id',$this->input->post('edit_id'));
		$this->db->from('school');
		$this->db->from('admin');
		$this->db->where('school.admin_id',$admin_id);
		$this->db->where('admin.id = school.admin_id');
		$q = $this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------------- End function to get the all details in Edit Section ----------------------------//

//----------------------------- Start function to update admin details --------------------------------//
	function update()
	{
		$image_name='';
		$admin_id=$this->input->post('edit_id');
		$exist_image=$this->input->post('exist_image');
		//echo "JAYATISH";die;
		//$admin_id=$this->input->post('admin_id');
		if($_FILES['logo']['name']!='')
		{
			$image_name="logo";
			$config['upload_path'] = 'uploads/user_image/'; 
			$config['allowed_types'] = 'gif|jpg|jpeg|png'; 
			$config['max_size'] = '1000'; 
			$config['encrypt_name'] = TRUE;
			
			$this->load->library('upload', $config); 
			if(!$this->upload->do_upload($image_name)) 
			{
			 	//echo $this->upload->display_errors();die; 
			 	$this->session->set_userdata('error_msg',$this->upload->display_errors());
			 	$return=0;
			 	return $return;
			}
			else 
			{ 
				$fInfo = $this->upload->data();
				$image_name=$fInfo['file_name'];
				$config1 = array(
				  'source_image' => 'uploads/user_image/'.$fInfo['file_name'], //get original image
				  'new_image' => 'uploads/user_image/thumb/', //save as new image //need to create thumbs first
				  'maintain_ratio' => true,
				  'width' => 150,
				  'height' => 150
				);
				$this->load->library('image_lib', $config1); //load library
				$this->image_lib->resize(); //generating thumb
				//echo $this->image_lib->display_errors(); die;
				$update_image=array('user_image' => $image_name);
				$this->db->where('id',$admin_id);
				$this->db->update('admin',$update_image);

				if($exist_image!='')
				{
					$unlink_main_path='uploads/user_image/'.$exist_image;
					$unlink_thumb_path='uploads/user_image/thumb/'.$exist_image;
					if(file_exists($unlink_main_path))
					{
						unlink($unlink_main_path);
					}
					if(file_exists($unlink_thumb_path))
					{
						unlink($unlink_thumb_path);
					}
				}

			}
		}


	if($this->input->post('password')!=''){
				$update_array=array(
					'password' => md5($this->input->post('password')),
					'phone_no' => $this->input->post('phone_no'),
					'description' => $this->input->post('description'),			
					'status' => $this->input->post('status')
				);
			}
		else{
				$update_array=array(					
					'phone_no' => $this->input->post('phone_no'),
					'description' => $this->input->post('description'),			
					'status' => $this->input->post('status')
				);
			}
		$this->db->where('id',$admin_id);
		$update=$this->db->update('admin',$update_array);

		if($update)
		{

			$this->session->set_userdata('success_msg','Data update successfully');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Failed to update data.Please try again.');
			$return=0;
		}
		return $return;
	}
//------------------------------- End function to update admin details --------------------------------//

//------------------------ Start function to update status of admin -----------------------------------//
	function update_status()
	{
		if($this->input->post('status')==1)
		{
			$updated_status='0';
		}
		if($this->input->post('status')==0)
		{
			$updated_status='1';
		}
		$update_status=array('status' => $updated_status);
		$this->db->where('id',$this->input->post('edit_id'));
		$update=$this->db->update('admin',$update_status);
		if($update)
		{
			$this->session->set_userdata('success_msg','Status updated successfully.');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Failed to update status. Please try again.');
			$return=0;
		}
		return $return;
	}

//-------------------------- End function to update status of admin ----------------------------------//

	function download_pdf()
	{
		//die('hh');
		$data=array();
		//$this->db->where('id',$this->input->post('edit_id'));
		$this->db->select('school.*,principal_teacher.*');
		//$this->db->where('school_id',$this->input->post('edit_id'));
		$this->db->from('school');
		$this->db->from('principal_teacher');
		$this->db->where('school.id',$this->input->post('edit_id'));
		$this->db->where('principal_teacher.school_id = school.id');
		$get=$this->db->get();
		//echo $this->db->last_query();die;
		if($get->num_rows>0)
		{
			foreach ($get->result() as $result) 
			{	
				//$image_name=$result->school_name;
				$data[]=$result;
				//$data[]='erfdsg';
			}//die;
			return $data;
			//print_r($data);die();
		}
	}

}
?>