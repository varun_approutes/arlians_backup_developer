<?php
class modeladmin extends CI_Model 
{
	
	
	function __construct()
	{
		parent::__construct();
	}
	
//-------------------- Start function to get all site settings details ------------------------------//
	function AllSiteSettings()
	{
		$q=$this->db->get('site_settings');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				define(strtoupper($result->field_name), $result->field_value);
			}
		}
	}
//---------------------- End function to get all site settings details ------------------------------//
//------------------ Start function to authenticate with admin login credentials ----------------------------//
	function UserAuthenticate()
	{
		$admin_user_data='';
		$admin_user_name='';
		$admin_user_type='';
		$school_type='';
		$school_id=0;
		if(($this->input->post('usertype')==0) || ($this->input->post('usertype')==1))
		{
			$this->db->where('username',$this->input->post('username'));
			$this->db->where('password',md5($this->input->post('password')));
			$this->db->where('type',$this->input->post('usertype'));
			$this->db->where('status','1');
			$q=$this->db->get('admin');
			//echo $this->db->last_query();die;
		}
		else
		{
			$this->db->where('username',$this->input->post('username'));
			$this->db->where('password',md5($this->input->post('password')));
			if($this->input->post('usertype')==2)
			{
				$this->db->where('type','0');
			}
			if($this->input->post('usertype')==3)
			{
				$this->db->where('type','1');
			}
			$this->db->where('status','1');
			$q=$this->db->get('principal_teacher');
		}
		//echo $q->num_rows;die;
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{
				$admin_user_data=$result->id;
				$admin_user_name=$result->username;
				//echo $admin_user_data.'<br/>'.$admin_user_name;die;
				if(($this->input->post('usertype')==0) || ($this->input->post('usertype')==1))
				{
					if($result->type==0)
					{
						$admin_user_type="Super_Admin";	
					}
					else
					{
						$admin_user_type="Admin";
						$this->db->where('admin_id',$admin_user_data);
						$school=$this->db->get('school');
						//echo $this->db->last_query();die;
						if($school->num_rows>0)
						{
							foreach($school->result() as $result_school)
							{
								$school_id=$result_school->id;
								$school_type=$result_school->type;
							}
						}
					}
				}
				else
				{
					if($this->input->post('usertype')==2)
					{
						$admin_user_type="Principal";	
					}
					if($this->input->post('usertype')==3)
					{
						$admin_user_type="Teacher";	
					}
					$this->db->select('school.type as school_type,principal_teacher.*');
					$this->db->from('school');
					$this->db->from('principal_teacher');
					$this->db->where('principal_teacher.id',$admin_user_data);
					$this->db->where('principal_teacher.school_id = school.id');
					$school=$this->db->get();
					//echo $this->db->last_query();die;
					if($school->num_rows>0)
					{
						foreach($school->result() as $result_school)
						{
							$school_id=$result_school->school_id;
							$school_type=$result_school->school_type;
						}
					}
				}
				
			}
			//echo $this->db->last_query();die;
			//echo $school_id.'<br/>'.$school_type;die;
			$message=array(
				'admin_user_data' => $admin_user_data,
				'admin_user_name' => $admin_user_name,
				'admin_user_type' => $admin_user_type,
				'admin_school_id' => $school_id,
				'admin_school_type' => $school_type,
			);
			//echo '<pre>';print_r($message);die;
			$this->session->set_userdata($message);
			$return=1;
		}
		else
		{
			$return=0;
		}
		return $return;
	}
//------------------ End function to authenticate with admin login credentials ----------------------------//

//------------------ Start function to get user details -----------------------------------//
	function User_Details()
	{
		$userid=$this->session->userdata('admin_user_data');
		$usertype=$this->session->userdata('admin_user_type');
		//echo $usertype;die;
		if(($usertype=='Super_Admin') || ($usertype=='Admin'))
		{
			$this->db->where('id',$userid);
			$q=$this->db->get('admin');
		}
		if(($usertype=='Principal') || ($usertype=='Teacher'))
		{
			$this->db->where('id',$userid);
			$q=$this->db->get('principal_teacher');
		}
		//echo $this->db->last_query();die;
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				//if(($usertype=='Super_Admin') || ($usertype=='Admin'))
				//{
					if($result->user_image!='')
					{
						$image_path='uploads/user_image/thumb/'.$result->user_image;
						if(file_exists($image_path))
						{
							$user_image=base_url().'uploads/user_image/thumb/'.$result->user_image;
						}
						else
						{
							$user_image=base_url().'assets/images/no-user.png';
						}
					}
					else
					{
						$user_image=base_url().'assets/images/no-user.png';
					}
					$username=$result->full_name;
				//}
				
				$data=array(
					'admin_image' => $user_image,
					'admin_name' => $username
				);
			}
		}
		return $data;
	}
//-------------------- End function to get user details ------------------------------------//
//-------------------- Start function to update password -------------------------------//
	function update_password()
	{
		$usertype=$this->input->post('usertype');
		$userid=$this->input->post('userid');
		$new_password=$this->input->post('new_password');
		if(($usertype=='Super_Admin') || ($usertype=='Admin'))
		{
			$update_array=array('password' => md5($new_password));
			$this->db->where('id',$userid);
			$q=$this->db->update('admin',$update_array);
		}
		else
		{
			$update_array=array('password' => md5($new_password));
			$this->db->where('id',$userid);
			$q=$this->db->update('principal_teacher',$update_array);
		}
		if($q)
		{
			$this->session->set_userdata('success_msg','Your password update successfully.');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','There is an error to update password. Please try again.');
			$return=0;
		}
		return $return;
	}
//----------------------- End function to update password ---------------------------------//

		//----------------- Start function to get all parent list by school_id-----------------------------//
	function GetList($school_id)
	{
		$data=array();
		$this->db->select('student.*');
		$this->db->from('student');
		//$this->db->from('parent');
		$this->db->where('student.school_id',$school_id);
		//$this->db->where('parent.student_id = student.id');
		$this->db->where('student.status','1');
		//echo $this->db->last_query();die;

		$q=$this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------ End function to get all parent list by school_id ------------------------------//
//---------------------- Start function to get All Attendance Count---------------------------//
	function GetTotalAttendance($school_id)
	{
		$data=array();
		$sql=$this->db->query("SELECT COUNT(`student_id`) as `student_count`,`attendance_status` FROM `attendance` WHERE `school_id` = '".$school_id."' GROUP BY `attendance_status`");
		if($sql->num_rows>0)
		{
			foreach($sql->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------------ End function to get All Attendance Count --------------------------// 
//---------------------- Start function to get attendance per class ---------------------------//
	function GetUniqueClass($school_id)
	{
		$data=array();
		$this->db->where('school_id',$school_id);
		$this->db->group_by('class_id');
		$q=$this->db->get('attendance');
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{
				$data[]=$result;
			}
		}
		return $data;

	}
//------------------------ End function to get attendance per class -----------------------------//
//----------------------- Start function to get record of attendance ---------------------------------//
	function TotalAttendance($school_id,$type,$class_id)
	{
		$this->db->where('school_id',$school_id);
		$this->db->where('attendance_status',$type);
		$this->db->where('class_id',$class_id);
		$q=$this->db->get('attendance');
		return $q->num_rows();
	}
//------------------------- End function to get record of attendance ---------------------------------//
	function ar_subsector_list()
	{
	$data=array();
			$sql = "select 	distinct(sub_sector_id) from ar_apinews";
			
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}

	function insertNewsImage($data)
	{
		$res = $this->db->insert('news_image',$data);
		return $res;
	}

	function checkImageExist($id)
	{
		$this->db->select('*');
		$this->db->from('news_image');
		$this->db->where('news_id',$id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function getAllBingNews()
	{
		$this->db->select('*');
		$this->db->from('ar_apinews');
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}

	function insertNewsLogo($data)
	{
		$res = $this->db->insert('news_image',$data);
		return $res;
	}

	function updateNewsLogo($data)
	{
		$id 		= $data['news_id'];
		$logoUrl 	= $data['logo']; 

		$res['logo']  = $logoUrl;

		$this->db->where('news_id',$id);
		$res = $this->db->update('news_image', $res); 
		return $res;
	}

	function updateNewsImage($data)
	{
		$id 		= $data['news_id'];
		$logoUrl 	= $data['image_url']; 

		$res['image_url']  = $logoUrl;

		$this->db->where('news_id',$id);
		$res = $this->db->update('news_image', $res); 
		return $res;
	}
}
?>