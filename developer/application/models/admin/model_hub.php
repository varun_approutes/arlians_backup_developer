z`<?php
class model_hub extends CI_Model 
{
	
	
	function __construct()
	{
		parent::__construct();
	}

	function saveBing($data)
	{
		foreach($data as $key=>$val)
		{
			$this->db->where('subsector_code',$key);
			$this->db->update('ar_subsector',array('bing_query_str'=>$val));
		}
	}

	function fetchReportedHub()
	{
		$this->db->select('*');
		$this->db->from('hub_reports');
                 // $this->db->select('reports,hub_title,');
                // $this->db->from('hub_reports');
		$this->db->join('ar_hub','hub_reports.hub_id = ar_hub.hid');
		$query = $this->db->get();
               // $res = $query->result_array();
               // return $res;
              //  print_r($query) ;
              //  exit ;
                
                 return $query;
	}
        
        function statusupdt($id){
        // echo "hello stupdt model";
        //  print_r($id) ;
       // exit ;
        $this->db->select('status');
	$this->db->from('ar_hub');
        $this->db->where('hid',$id);
        $query = $this->db->get();
        $res = $query->result_array();		
        
        if($res[0]['status'] == 1)
        {
             $s = '0' ;
              $this->db->set('status', $s);
              $this->db->where('hid', $id);   
              $this->db->update('ar_hub');
            
        }
       if($res[0]['status'] == 0)
        {
            echo "hello stupdt model";
           
            $s = '1';
            //print_r($s);
          //  print_r($id) ;
           //  exit ;
              $this->db->set('status', $s);
              $this->db->where('hid', $id);   
              $this->db->update('ar_hub');
              
        }
        
            
        }
        
        
        function subsectorview(){
            
          //  echo "hello subsectorview";
          // exit;
            
           
            $query = $this->db->get('ar_subsector'); 
             return $query;
            
            
        }
        function subsectoreditview($subsector_code){
            
           $this->db->select('*');
	    $this->db->from('ar_subsector'); 
            $this->db->where('subsector_code',$subsector_code);
            $query = $this->db->get();
            return $query;
        }
        function  subsectorpdt($data,$subsector_code){
          // print_r($data);
         //  print_r($subsector_code);
         // echo "hello subsectorpdt";
         // exit;  
          $this->db->set($data); 
         $this->db->where("subsector_code", $subsector_code); 
         $this->db->update("ar_subsector",$data);
          
          
        }
        
        function bussniss_view(){
           // echo "hello business view";
           // exit ;
            $query = $this->db->get('ar_bussiness');
            return $query;
            
        }
        function bussniss_edit($bussiness_code){
            
           // print_r($bussiness_code) ;
          //  exit ;
           $this->db->select('*');
	    $this->db->from('ar_bussiness'); 
            $this->db->where('bussiness_code',$bussiness_code);
            $query = $this->db->get();
            return $query; 
            
        }
        function bussnisspdt($data,$bussiness_code){
          $this->db->set($data); 
         $this->db->where("bussiness_code", $bussiness_code); 
         $this->db->update("ar_bussiness",$data);  
            
            
        }
        function  industryview(){
           // echo "hello business view";
          //  exit ; 
           $query = $this->db->get('ar_industry');
            return $query; 
            
        }
        function industryedit($industry_code){
            
          $this->db->select('*');
	    $this->db->from('ar_industry'); 
            $this->db->where('industry_code',$industry_code);
            $query = $this->db->get();
            return $query;   
            
        }
        
        function industrypdt($data,$industry_code){
           $this->db->set($data); 
         $this->db->where("industry_code",$industry_code); 
         $this->db->update("ar_industry",$data);    
            
        }
        
}
?>