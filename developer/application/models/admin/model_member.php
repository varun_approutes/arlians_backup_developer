<?php
class model_member extends CI_Model 
{
	
	
	function __construct()
	{
		parent::__construct();
	}
//----------------- Start function to get all principal list -----------------------------//
	function GetAllList()
	{
		$data=array();
		//if($this->session->userdata('admin_user_type')=="Super_Admin")
		//{
			$this->db->select('ar_members.mid,ar_members.user_type,ar_members.update_date,ar_members.fname,ar_members.lname,ar_members.email,ar_members.password,ar_members.status,ar_request_access_page.bussinessname, ar_request_access_page.bussinesstype');
			$this->db->from('ar_members');
			$this->db->from('ar_request_access_page');
			$this->db->where('ar_members.mid = ar_request_access_page.mid');
			$this->db->order_by("ar_members.mid","desc");
			$q=$this->db->get();
		//}
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------ End function to get all principal list ------------------------------//
//------------------ Start function to get school Id ---------------------------------//

	function GetSchoolList()
	{
		$data=array();
		$this->db->where('status','1');
		$q=$this->db->get('school');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------ End function to to get school Id ------------------------------//

//--------------------- Start section to get Student's Section Details ----------------------------//
	function GetSection()
	{
		$data=array();
		$this->db->where('status','1');
		$q=$this->db->get('section');
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}
//----------------------- End section to get Student's Section Details -----------------------------//

//------------------ Start function to insert school details ---------------------------------//
	function insert()
	{
		$error=0;
		if($this->input->post('email')!='')
		{
			$this->db->where('email',$this->input->post('email'));
			$exist_email=$this->db->get('student');
			if($exist_email->num_rows>0)
			{
				$this->session->set_userdata('error_msg','This email is already exist. Please try another.');
				$return=0;
				$error=1;
			}
		}
		if($this->input->post('phone')!='')
		{
			$this->db->where('phone_no',$this->input->post('phone'));
			$exist_phone=$this->db->get('student');
			if($exist_phone->num_rows>0)
			{
				$this->session->set_userdata('error_msg','This phone number is already exist. Please try another.');
				$return=0;
				$error=1;
			}
		}
		if($error==0)
		{
			$image_name='';
			if($this->input->post('school_id')!=0)
			{
				$school_id=$this->input->post('school_id');
			}
			else
			{
				$school_id=$this->input->post('school_name');
			}
			$registration_number=$this->model_student->getRegistrationNumber($this->input->post('school_id'));
			//echo $registration_number.'<br/>';
			$admission_date = date('Y-m-d',strtotime($this->input->post('admission_date')));
			$this->db->where('school_id',$this->input->post('school_id'));
			$this->db->where('class',$this->input->post('class'));
			$this->db->order_by('roll_no','DESC');
			$this->db->limit(1,0);
			$get_roll=$this->db->get('student');
			if($get_roll->num_rows>0)
			{
				foreach($get_roll->result() as $roll)
				{
					$new_roll=$roll->roll_no+1;
				}
			}
			else
			{
				$new_roll=1;
			}
			//echo $new_roll;die;
			if($_FILES['image']['name']!='')
			{
				$image_name="image";
				$config['upload_path'] = 'uploads/student_image/'; 
				$config['allowed_types'] = 'gif|jpg|jpeg|png'; 
				$config['max_size'] = '1000'; 
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $config); 
				if(!$this->upload->do_upload($image_name)) 
				{
				 	//echo $this->upload->display_errors();die; 
				 	$this->session->set_userdata('error_msg',$this->upload->display_errors());
				 	$return=0;
				 	return $return;
				}
				else 
				{ 
					$fInfo = $this->upload->data();
					$image_name=$fInfo['file_name'];
					$config1 = array(
						  'source_image' => 'uploads/student_image/'.$fInfo['file_name'], //get original image
						  'new_image' => 'uploads/student_image/thumb/', //save as new image //need to create thumbs first
						  'maintain_ratio' => true,
						  'width' => 150,
						  'height' => 150
						);
					$this->load->library('image_lib', $config1); //load library
					$this->image_lib->resize(); //generating thumb
					//echo $this->image_lib->display_errors(); die;
				}
			}
			$insert_array=array(
				'school_id' => $school_id,			
				'name' => $this->input->post('name'),
				'class' => $this->input->post('class'),
				'roll_no' => $new_roll,
				'reg_no' => $registration_number,
				'email' => $this->input->post('email'),
				'password' => md5($this->input->post('password')),
				'address' => $this->input->post('address'),
				'phone_no' => $this->input->post('phone'),
				'admission_date' => $admission_date,
				'student_image' => $image_name,
				'status' => $this->input->post('status'),
			);
			$insert=$this->db->insert('student',$insert_array);

			if($insert)
			{

				$this->session->set_userdata('success_msg','Data insert successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to insert data.Please try again.');
				$return=0;
			}
		}
		return $return;
	}
//-------------------- End function to insert school details ---------------------------------//
//------------------- Start function to get auto generated registration number -----------------------------//
	function getRegistrationNumber($school_id)
	{
		$six_digit_random_number = mt_rand(100000, 999999);
		$get_number='SC'.$school_id.'-'.$six_digit_random_number;
		$this->db->where('reg_no',$get_number);
		$get=$this->db->get('student');
		if($get->num_rows>0)
		{
			$this->model_student->getRegistrationNumber($school_id);
		}
		else
		{
			return $get_number;
		}
	}
//---------------------- End function to get auto generated registration number ---------------------------//
//------------------------ Start function to get details of student in edit ---------------------------------//

	function edit($mid)
	{
		$data=array();
		$this->db->select('ar_members.mid,ar_members.fname,ar_members.lname,ar_members.email,ar_members.password,ar_members.status,ar_request_access_page.bussinessname, ar_request_access_page.bussinesstype');
		$this->db->from('ar_members');
		$this->db->from('ar_request_access_page');
		$this->db->where('ar_members.mid',$mid);
		$this->db->where('ar_members.mid = ar_request_access_page.mid');
		$q=$this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{
				$data[]=$result;
			}
		}
		return $data;
	}
//------------------------- End function to get details of student in edit -----------------------------------//

//------------------------ Start function to update Student Details ----------------------------------//
	function update()
	{
		$error=0;
		$userid=$this->input->post('userid');
		if($this->input->post('email')!='')
		{
			$this->db->where('email',$this->input->post('email'));
			$this->db->where('id !=',$userid);
			$exist_email=$this->db->get('student');
			if($exist_email->num_rows>0)
			{
				$this->session->set_userdata('error_msg','This email is already exist. Please try another.');
				$return=0;
				$error=1;
			}
		}
		if($this->input->post('phone')!='')
		{
			$this->db->where('phone_no',$this->input->post('phone'));
			$this->db->where('id !=',$userid);
			$exist_phone=$this->db->get('student');
			if($exist_phone->num_rows>0)
			{
				$this->session->set_userdata('error_msg','This phone number is already exist. Please try another.');
				$return=0;
				$error=1;
			}
		}
		if($error==0)
		{
			$image_name='';
			
			$admission_date = date('Y-m-d',strtotime($this->input->post('admission_date')));
			
			//echo $new_roll;die;
			if($_FILES['image']['name']!='')
			{
				$image_name="image";
				$config['upload_path'] = 'uploads/student_image/'; 
				$config['allowed_types'] = 'gif|jpg|jpeg|png'; 
				$config['max_size'] = '1000'; 
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $config); 
				if(!$this->upload->do_upload($image_name)) 
				{
				 	//echo $this->upload->display_errors();die; 
				 	$this->session->set_userdata('error_msg',$this->upload->display_errors());
				 	$return=0;
				 	return $return;
				}
				else 
				{ 
					$fInfo = $this->upload->data();
					$image_name=$fInfo['file_name'];
					$config1 = array(
						  'source_image' => 'uploads/student_image/'.$fInfo['file_name'], //get original image
						  'new_image' => 'uploads/student_image/thumb/', //save as new image //need to create thumbs first
						  'maintain_ratio' => true,
						  'width' => 150,
						  'height' => 150
						);
					$this->load->library('image_lib', $config1); //load library
					$this->image_lib->resize(); //generating thumb
					//echo $this->image_lib->display_errors(); die;
					$update_image=array('student_image' => $image_name);
					$this->db->where('id',$userid);
					$this->db->update('student',$update_image);
					if($this->input->post('exist_image')!='')
					{
						$image_path='uploads/student_image/'.$this->input->post('exist_image');
						$image_thumb_path='uploads/student_image/thumb/'.$this->input->post('exist_image');
						if(file_exists($image_path))
						{
							unlink($image_path);
						}
						if(file_exists($image_thumb_path))
						{
							unlink($image_thumb_path);
						}
					}
				}
			}
			$update_array=array(
				
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'address' => $this->input->post('address'),
				'phone_no' => $this->input->post('phone'),
				'admission_date' => $admission_date,
				'status' => $this->input->post('status'),
			);
			$this->db->where('id',$userid);
			$update=$this->db->update('student',$update_array);

			if($update)
			{

				$this->session->set_userdata('success_msg','Data update successfully');
				$return=1;
			}
			else
			{
				$this->session->set_userdata('error_msg','Failed to update data.Please try again.');
				$return=0;
			}
		}
		return $return;
	}
//-------------------------- End function to update Student Details ----------------------------------//
//------------------------ Start function to change status in Student Management ------------------------------//
	function update_status()
	{
		if($this->input->post('status')==1)
		{
			$updated_status='0';
		}
		if($this->input->post('status')==0)
		{
			$updated_status='1';
		}
		$status_change=array('status' => $updated_status);
		$this->db->where('mid',$this->input->post('edit_id'));
		$update=$this->db->update('ar_members',$status_change);
		if($update==1)
		{
			$this->session->set_userdata('success_msg','Status update successfully.');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Failed to update status.Please try again.');
			$return=0;
		}
		return $return;
	}
//-------------------------- End function to change status in Student Management ------------------------------//

//---------------------- Start function to delete school details permanently -------------------------------//
	function delete()
	{
		$image_name='';
		$this->db->where('id',$this->input->post('edit_id'));
		$get=$this->db->get('student');
		if($gqt->num_rows>0)
		{
			foreach ($get->result() as $result) 
			{	
				$image_name=$result->student_image;
			}
		}
		if($image_name!='')
		{
			$unlink_main_path='uploads/student_image/'.$image_name;
			$unlink_thumb_path='uploads/student_image/thumb/'.$image_name;
			if(file_exists($unlink_main_path))
			{
				unlink($unlink_main_path);
			}
			if(file_exists($unlink_thumb_path))
			{
				unlink($unlink_thumb_path);
			}
		}
		$this->db->where('id',$this->input->post('edit_id'));
		$delete=$this->db->delete('student');
		if($delete)
		{
			$this->session->set_userdata('success_msg','Data deleted successfully');
			$return=1;
		}
		else
		{
			$this->session->set_userdata('error_msg','Failed to delete data. Please try again.');
			$return=0;
		}
		return $return;
	}
//-------------------------- ENd function to delete school details permanently ----------------------------------//
//------------------------- Start function to get no of years -----------------------------//
	function GetYear($school_id)
	{
		$years='';
		$this->db->where('id',$school_id);
		$q=$this->db->get('school');
		if($q->num_rows>0)
		{
			foreach($q->result() as $result)
			{
				$years=$result->no_of_years;
			}
		}
		return $years;
	}
	function MemberDetails($email){
		$sql = "SELECT * FROM ar_members left join ar_request_access_page on ar_members.mid=ar_request_access_page.mid WHERE ar_members.email='".$email."'";
		$query = $this->db->query($sql);
		$rs = $query->result_array();
		$data=$rs[0];
		$id=$rs[0]['mid'];
		//$data['back_image']=$this->BackImage($id);
		//$data['fornt_image']=$this->ProfileImage($id);
		//$data['topline_name']=$this->toplineofMember($id);
		return $data;
	}
		
//--------------------------- End function to get no of years ----------------------------//

	//function update_type(){
	
		
	//}
	function GetAllRefList()
	{
		$data=array();
		
			
			$this->db->from('ar_referral ref');
			$this->db->join('ar_members mem', 'ref.mid = mem.mid', 'left');
			$this->db->join('ar_request_access_page rap', 'mem.mid = rap.mid', 'left');
			$this->db->group_by('ref.mid'); 
			$this->db->order_by("ref.mid","desc");
			$q=$this->db->get();
		
		if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}
	function refCount($mid){	
			$sql = 'SELECT count(*) as total FROM ar_referral WHERE mid='.$mid;
			$query = $this->db->query($sql);	
			
			foreach($query->result() as $result){
				foreach ($result as $res){
					return $res;
					
				}
				
				
			}
		
	}
	
}
?>
