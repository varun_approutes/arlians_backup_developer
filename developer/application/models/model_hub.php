<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_hub extends CI_model
{
 
	function __construct(){

		parent::__construct();
			 
	}
	
	
	function myConnectionList($mid){
		  $sql = "SELECT * FROM (`connected`) Inner Join `ar_members` on `ar_members`.`mid`=`connected`.`mid`  WHERE (`connected`.`mid` = ".$mid." OR `connected`.`connected_id` = ".$mid.") AND `connected`.`is_accepted` = '1' and `ar_members`.`status`='1'";
		   $query = $this->db->query($sql);
		   $data = array();
			if($query->num_rows>0)
			{
				foreach ($query->result() as $result) 
				{	
					$data[]=$result;
				}
			}
			//print_r($data);die();
		return $data;		
	}
	function myConnectionListidsxxxxx($mid){
		  $sql = "SELECT * FROM (`connected`) Inner Join `ar_members` on `ar_members`.`mid`=`connected`.`mid`  WHERE (`connected`.`mid` = ".$mid." OR `connected`.`connected_id` = ".$mid.") AND `connected`.`is_accepted` = '1' and `ar_members`.`status`='1'";
		   $query = $this->db->query($sql);
		   $data = array();
			if($query->num_rows>0)
			{
				foreach ($query->result() as $result) 
				{	
					$data[]=$result->mid;
				}
			}
			//print_r($data);die();
		return $data;		
	}
	
	function FnUserConnectedtolikeduser($hid,$loggedinuser_id,$ownermid)
	{
		$status=false;
		 $liked_id=$this->Fnwholikedthishub($hid);
		 $ownermid;
		if($liked_id>0)
		{
			if($liked_id==$ownermid)
			{
				return $status=false;
			}
		}
		if($loggedinuser_id==$ownermid)
		{
			return $status=false;
		}
		else
		{
			
			$this->db->select('mid');
			$this->db->where('hub_id',$hid);
			$query=$this->db->get('hub_like');
			$countquery=$query->num_rows();
			if($countquery>0)
			{
				$likeidreultarr=$query->result_array();

				foreach($likeidreultarr as $likearr)
				{
					$likeids[]=$likearr['mid'];
				}
				if(!in_array($loggedinuser_id,$likeids))
				{
					return $status=true;
				}
			}
		}
		
		
		
		return $status;
	}
	
	function Fnwholikedthishub($hid)
	{   $mid=0;
	    $this->db->select('mid');
		$this->db->where('hub_id',$hid);
		$query=$this->db->get('hub_like');
		//echo $this->db->last_query();
		$countquery=$query->num_rows();
		if($countquery==1)
		{
			$res=$query->row_array();
			$mid=$res['mid'];
		}
		return $mid;
	}
	
	function Fnwhocommentedonthishub($hid)
	{   $mid=0;
	    $this->db->select('mid');
		$this->db->where('hub_id',$hid);
		$query=$this->db->get('hub_comments');
		//echo $this->db->last_query();
		$countquery=$query->num_rows();
		if($countquery==1)
		{
			$res=$query->row_array();
			$mid=$res['mid'];
		}
		return $mid;
	}
	
	function FnUserConnectedtoCommentuser($hid,$loggedinuser_id,$ownermid)
	{
		$status=false;
		$commented_id=$this->Fnwhocommentedonthishub($hid);
		 $ownermid;
		if($commented_id>0)
		{
			if($commented_id==$ownermid)
			{
				return $status=false;
			}
		}
		if($loggedinuser_id==$ownermid)
		{
			return $status=false;
		}
		else
		{
			$this->db->select('mid');
			$this->db->where('hub_id',$hid);
			$query=$this->db->get('hub_comments');
			$countquery=$query->num_rows();
			if($countquery>0)
			{
				$likeidreultarr=$query->result_array();
				foreach($likeidreultarr as $likearr)
				{
					$likeids[]=$likearr['mid'];
				}
				if(!in_array($loggedinuser_id,$likeids))
				{
					$status=true;
				}
			}
		}
		
		
		
		return $status;
	}
	
	 function FnCommentnotconnectedperson($hid,$loggedinuser_id,$ownermid)
	{
		$connected_users=$this->myConnectionListidsxxxxx($loggedinuser_id);
		array_push($connected_users,$loggedinuser_id);
		$likedmid=$this->FnCommentids($connected_users,$hid);
		//print_r($likedmid);die();
		$data=array();
		if(count($likedmid)==1)
		{
			$data['commentbusinessname']=getMemberbusinName($likedmid['mid']);
			$comprof_img=getProfileImage($likedmid['mid']);
			if($comprof_img!='')
			{
			  $data['commentbusinesslogo'] =  base_url().'uploads/profile_image/'.$comprof_img;  
			}
			else
			{
				$data['commentbusinesslogo'] =  '';  
			}
			$em = logobussinessname($likedmid['mid']);
			$data['commentprofile_initional'] = $em;
			$data['commentedbyuserid'] = $likedmid['mid'];
		}
		return $data;
		
		
	} 
	
	function FnCommentids($connected_users,$hub_id)
	{
		$likeids=array();
		$this->db->select('mid');
//$this->db->where_in('mid',$connected_users);
		$this->db->where('hub_id',$hub_id);
		$query=$this->db->get('hub_comments');
		$countquery=$query->num_rows();
		if($countquery>0)
		{
			$likeids=$query->row_array();
			/* foreach($likeidreultarr as $likearr)
			{
				$likeids[]=$likearr['mid'];
			} */
			
		}
		return $likeids;
	}
	
	
	
	
	
	
	
	 function FnLikednotconnectedperson($hid,$loggedinuser_id,$ownermid)
	{
		$connected_users=$this->myConnectionListidsxxxxx($loggedinuser_id);
		array_push($connected_users,$loggedinuser_id);
		$likedmid=$this->FnLikedids($connected_users,$hid);
		//print_r($likedmid);die();
		$data=array();
		if(count($likedmid)==1)
		{
			$data['likedbusinessname']=getMemberbusinName($likedmid['mid']);
			$comprof_img=getProfileImage($likedmid['mid']);
			if($comprof_img!='')
			{
			  $data['likedbusinesslogo'] =  base_url().'uploads/profile_image/'.$comprof_img;  
			}
			else
			{
				$data['likedbusinesslogo'] =  '';  
			}
			$em = logobussinessname($likedmid['mid']);
			$data['likeprofile_initional'] = $em;
			$data['likedbyuserid'] = $likedmid['mid'];
		}
		return $data;
		
		
	} 
	
	function FnLikedids($connected_users,$hub_id)
	{
		$likeids=array();
		$this->db->select('mid');
		//$this->db->where_in('mid',$connected_users);
		$this->db->where('hub_id',$hub_id);
		$query=$this->db->get('hub_like');
		$countquery=$query->num_rows();
//echo $this->db->last_query();die();
		if($countquery>0)
		{
			$likeids=$query->row_array();
			/* foreach($likeidreultarr as $likearr)
			{
				$likeids[]=$likearr['mid'];
			} */
			
		}
		return $likeids;
	}
	
	
	
	
	
	function FnUserConnectedtoshareuser($hid,$loggedinuser_id,$ownermid)
	{
		$status=false;
		
			$this->db->select('mid');
			$this->db->where('hub_id_new',$hid);
			$query=$this->db->get('hub_share');
			//echo $this->db->last_query();
			$countquery=$query->num_rows();
			if($countquery>0)
			{
				
				$status=true;
				
			}
		
		
		
		
		return $status;
	}
	
	function hubPostedByMenber($mid){
			$this->db->from('ar_hub');
			$this->db->where('ar_hub.mid',(int)$mid);
			$q=$this->db->get();
			if($q->num_rows>0)
		{
			foreach ($q->result() as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
		
		
		
	}
/*
	function myConnectionList($mid){

		$data = array();
		$this->db->select('connected_id');
		$this->db->from('connected');
		$this->db->where('connected.mid',(int)$mid);
		$this->db->where('connected.is_accepted','1');
		$q=$this->db->get();
		if($q->num_rows>0){

		foreach ($q->result_array() as $result){

			$data[]=$result['connected_id'];
		}
	}
	return $data;
	}
*/
	 function hubPostedByMember($mids,$start,$limit){
//var_dump($start.','.$limit);exit;
	 		$data = array();
	 		$this->db->select('hid');
			$this->db->from('ar_hub');
			$this->db->where_in('ar_hub.mid',$mids);
			$this->db->order_by('ar_hub.posted_on','desc');
			$this->db->limit($limit,$start);
			$q=$this->db->get();
			//echo $this->db->last_query(); die();
			if($q->num_rows>0)
		{
			foreach ($q->result_array() as $result) 
			{	
				$data[]=$result; 
			}
		}
		return $data;	
		
	}
	
	 function hubPostedByMembertestlal($mids,$start,$limit,$allhids,$checking_sector){
		// print_r($checking_sector);die();
            $data = array();
			$dataids=array();
	 	    $this->db->select('hid');
			$this->db->from('ar_hub');
			//$this->db->join('ar_members','ar_members.mid=ar_hub.mid','INNER');
			$this->db->where_in('ar_hub.mid',$mids);
			//$this->db->where('ar_members.status','1');
			$this->db->order_by('ar_hub.hid','desc');
			$this->db->limit($limit,$start);
			$q=$this->db->get(); 
					
			if($q->num_rows>0)
		{   
			foreach ($q->result_array() as $result) 
			{	
				$data[]=$result;
				$dataids[]=$result['hid'];
			}

			

		}
		//echo count($allhids);
		if(count($allhids)>0)
		{
			$allhidsarr=array_merge($dataids,$allhids);
			arsort($allhidsarr);
			$data=array_unique($allhidsarr); 
			//print_r($data);die();
			return $data;
		} 
		return $dataids;
		
		
	}
	

	 function FnFetchhidsbasedonmids($mids,$start,$limit)
	 {
        $data = array();
		$this->db->select('hid');
		$this->db->from('new_temp_tbl');
		$this->db->join('ar_members','ar_members.mid=new_temp_tbl.mid','INNER');
		$this->db->where_in('new_temp_tbl.mid',$mids);
		$this->db->where('ar_members.status','1');
		$this->db->order_by('new_temp_tbl.hid','desc');
		$this->db->limit($limit,$start);
		$q=$this->db->get();  
		
		return $q->result_array();
		
		
	}
	
	
	
	
	
	
	
	
	
	

	function hubPostDetails($data){
		$res = $this->db->insert('hub_top_details',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
		
	}
	
	function del_hub_details($hid){
		
		$this->db->where('hub_id', $hid);
		$res = $this->db->delete('hub_top_details'); 
		return $res;
	}

	function convertingHubIdToTopline($tag){
		$sql = "SELECT top_code FROM ar_top_tag_rel WHERE tag1_id=".$tag." OR tag2_id=".$tag." OR tag2_id=".$tag;
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0){
			$res = $query->result_array();
			
			return $res[0]['top_code'];
			
		}
	}

	function getTopLineFromMid($id){

		$this->db->select('mem_code');
		$this->db->from('ar_member_data_wiz_one');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		$res = $query->result_array();		
		return $res;
	}

	function getTagCodes($mid){

		$this->db->select('tags');
		$this->db->from('ar_request_access_page');
		$this->db->where('mid',$mid);
		$query = $this->db->get();
		$res = $query->result_array();		
		return $res[0]['tags'];
	}

	function getHubIdsFromTopLine($tl_code){

		$this->db->select('hub_id');
		$this->db->from('hub_top_details');
		$this->db->where_in('topline_code',$tl_code);
		//$this->db->limit(10,10);
		$query = $this->db->get();
		//echo $this->db->last_query(); die();
		$res = $query->result_array();		
		return $res;
	}

	function getHubIdsFromTopLineNew($tl_code,$start,$limit){
		////$this->db->cache_on(); 
		$this->db->select('hub_top_details.hub_id');
		$this->db->from('hub_top_details');
		$this->db->join('ar_hub','hub_top_details.hub_id = ar_hub.hid');
		$this->db->where_in('hub_top_details.topline_code',$tl_code);
		$this->db->where('ar_hub.status','1');
		$this->db->order_by('ar_hub.posted_on','DESC');
		$this->db->limit($limit,$start);
		$query = $this->db->get();
		//echo $this->db->last_query(); die();
		$res = $query->result_array();		
		return $res;
	}

	function getHubIdsFromTopLineWithBlockedId($tl_code,$start,$limit,$blockedids)
	{
		////$this->db->cache_on(); 
		$this->db->select('hub_top_details.hub_id');
		$this->db->from('hub_top_details');
		$this->db->join('ar_hub','hub_top_details.hub_id = ar_hub.hid');
		$this->db->where_in('hub_top_details.topline_code',$tl_code);
		$this->db->where('ar_hub.status','1');
		if(count($blockedids)>0)
		{
		$this->db->where_not_in('ar_hub.mid',$blockedids);	
		}
		
		$this->db->order_by('ar_hub.hid','DESC');
		$this->db->limit($limit,$start);
		$query = $this->db->get();
		//echo $this->db->last_query(); die();
		$res = $query->result_array();		
		return $res;
	}

	function getHubById($hid){
		$data = array();
		////$this->db->cache_on();
 		$this->db->select('*');
		$this->db->from('ar_hub');
		$this->db->where('hid',(int)$hid);
		$this->db->where('status','1');
		$this->db->order_by('posted_on','DESC');
		$q=$this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result_array() as $key => $result) 
			{
				$data[$key]=$result;
				//$ext = explode('.', basename($result['link']));
				$ex = pathinfo($result['link']);
				$ext = $ex['extension'];
				$data[$key]['ext'] = $ext;
			}
		}
	return $data;
	}
	
	function getHubByIdforWall($hid){
		$data = array();
 		$this->db->select('*');
		$this->db->from('ar_hub');
		$this->db->where('hid',(int)$hid);
		$this->db->where('status','1');
		$this->db->where('shared_for <> ', 4);
		$q=$this->db->get();
		if($q->num_rows>0)
		{
			foreach ($q->result_array() as $result) 
			{	
				$data[]=$result;
			}
		}

	return $data;
	}

	function checkHubLike($hid,$mid){
		$this->db->select('*');
		$this->db->from('hub_like');
		$this->db->where('hub_id',$hid);
		$this->db->where('mid',$mid);
		$q=$this->db->get();
		if($q->num_rows>0){
			return 1;
		}else{
			return 0;
		}
	}
	
	function checkNewsLike($nid,$mid)
	{
		$this->db->select('*');
		$this->db->from('hub_like');
		$this->db->where('news_id',$nid);
		$this->db->where('mid',$mid);
		$q=$this->db->get();
		if($q->num_rows>0){
			return 1;
		}else{
			return 0;
		}
	}
	
	function getComments($hid){
	//	//$this->db->cache_on();
		$this->db->select('hub_comments.comm_id,hub_comments.mid,hub_comments.total_comm_like,hub_comments.hub_id,hub_comments.mid,hub_comments.comments as comment_view,member_image.pic_id,member_image.image_url as image,ar_request_access_page.bussinessname as buisnesname,hub_comments.comm_date as comment_date');	
		$this->db->join('ar_request_access_page', 'ar_request_access_page.mid = hub_comments.mid', 'left');
		$this->db->join('member_image', 'member_image.mid = hub_comments.mid and member_image.type = "profile_img"', 'left');			
		$this->db->where('hub_comments.hub_id',$hid);
		$this->db->order_by('hub_comments.comm_id',"desc");
		$query = $this->db->get('hub_comments');
		$res = $query->result_array();		
		return $res;	
	}

	function getNewsCommentsDetails($id)
	{	////$this->db->cache_on();
		$this->db->select('hub_comments.comm_id,hub_comments.mid,hub_comments.hub_id,hub_comments.mid,hub_comments.comments as comment_view,member_image.pic_id,member_image.image_url as image,ar_request_access_page.bussinessname as buisnesname,hub_comments.comm_date as comment_date');	
		$this->db->join('ar_request_access_page', 'ar_request_access_page.mid = hub_comments.mid', 'left');
		$this->db->join('member_image', 'member_image.mid = hub_comments.mid and member_image.type = "profile_img"', 'left');			
		$this->db->where('hub_comments.hub_news_id',$id);
		$this->db->order_by('hub_comments.comm_id',"desc");
		$query = $this->db->get('hub_comments');
		$res = $query->result_array();	
//echo $this->db->last_query();		
		return $res;	
	}

	function postComment($data){
		$this->db->insert('hub_comments',$data);
		$res = $this->db->insert_id();
		return $res;
	}

	function deleteComment($data){
		$this->db->where('comm_id', $data['comm_id']);
		$this->db->where('mid', $data['mid']);
		$res = $this->db->delete('hub_comments'); 
		return $res;
	}

	function deletePost($data){
		$this->db->where('hid', $data['hub_id']);
		$this->db->where('mid', $data['mid']);
		$this->db->delete('ar_hub'); 
		return true;
	}

	function getTotalLikes($hid){
		////$this->db->cache_on();
		$this->db->select('*');
		$this->db->from('hub_like');
		$this->db->where('hub_id',$hid);		
		$query = $this->db->get();
		$res = $query->result_array();
		return count($res);	
		
	}
	function getTotalShare($hid){
		////$this->db->cache_on();
		$this->db->select('*');
		$this->db->from('hub_share');
		$this->db->where('hub_id_old',$hid);		
		$query = $this->db->get();
		$res = $query->result_array();
		return count($res);	
		
	}
	function postLike($hid,$mid,$cur_date){
		$data['hub_id'] = $hid;
		$data['mid'] = $mid;
		$data['like_date'] = $cur_date;
		//var_dump($data);die();
		$res = $this->db->insert('hub_like',$data);
		return $res;
	}

	function deleteHub($hid,$mid){
		$this->db->where('mid', $mid);
		$this->db->where('hub_id', $hid);
		$res = $this->db->delete('hub_like');
		//return $this->db->last_query();
		return $res;
	}

	function reportHub($data){
		$res = $this->db->insert('hub_reports',$data);
		return true;
	}
	function post_hub($data){
		$this->db->insert('ar_hub',$data);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}
	function post_hub_edit($id,$data){		
		$this->db->where('hid', $id);
		$this->db->update('ar_hub', $data);
		return 1;
	}

	function post_hubShare($data){
		$this->db->insert('hub_share',$data);
		$insert_id = $this->db->insert_id();

		return $insert_id;
	}
	function shared($data,$hub_ownermid){

		$hub_details=$this->getHubById($data['hub_id']);
		$sharedfor='';
		if($hub_details[0]['shared_for']=='4')
		{
			$sharedfor='publish_share';
		}
		if($hub_details[0]['url_provider']!='')
		{
			$sharedfor='url_share';
		}
		
		
		$cur_time = date('Y-m-d H:i:s');
		$hub=array( 'mid'=> $data['mid'],
							'hub_title'=> $hub_details[0]['hub_title'],
							'content'=> $hub_details[0]['content'],
							'link'=> $hub_details[0]['link'],
							'tag_id'=> $hub_details[0]['tag_id'],
							'shared_for'=> $data['shared_for'],
							'news_link'=> $hub_details[0]['news_link'],
							'posted_on'=> $cur_time,
							'hbcomment' => $data['hbcomment'],
							'shared_bing' => $sharedfor,
							'url_provider' => $data['url_provider'],
							'hub_share_type' => $data['POSTFLAG']
			);
		$hub_id=$this->post_hub($hub);
		$hub_owner_old=$hub_details[0]['mid'];
		if($hub_ownermid!='')
		{
			$hub_owner_old=$hub_ownermid;
		}
		
		$hub_share=array( 'owner_mid'=> $hub_owner_old,
							'mid'=> $data['mid'],
							'hub_id_old'=> $hub_details[0]['hid'],
							'hub_id_new'=> $hub_id,
							'share_date'=> $cur_time
							
			);

		
		$this->post_hubShare($hub_share);
		$tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
                    $code = $tw_digit_code[0]['mem_code'];
                    $code = substr($code,0,5);

                    $dt['hub_id'] = $hub_id;
                    $dt['topline_code'] = $code;

                    $res = $this->model_hub->hubPostDetails($dt);
                    if($hub_details[0]['tag_id'] != ''){
                        $tag_arr = implode(',',$hub_details[0]['tag_id']);
                        foreach($tag_arr as $tg){
                            $top_line_code = $this->model_hub->convertingHubIdToTopline($tg);
                            
                            if($top_line_code != NULL){
                                $d['hub_id'] = $hub_id;
                                $d['topline_code'] = $top_line_code;
                                $res = $this->model_hub->hubPostDetails($d);
								
                            } 
                        } 
                    }
		
		    //return array('response'=>'success','message'=>'The post successfully shared.'); // your code
		    return $hub_id; // your code
		
	}
	
	function getMyPublish($id){
		////$this->db->cache_on();
		$this->db->select('*');
		$this->db->from('ar_hub');
		$this->db->where('mid',$id);
		$this->db->where('status','1');
		$this->db->where('shared_for',4);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}
	
	function hub_delete($id){
		$this->db->delete('ar_hub',array('hid'=>$id));
		$this->db->delete('hub_top_details',array('hub_id'=>$id));
		$this->db->delete('hub_comments',array('hub_id'=>$id));
		$this->db->delete('hub_like',array('hub_id'=>$id));
		$this->db->delete('hub_reports',array('hub_id'=>$id));
		$this->db->delete('hub_share',array('hub_id_new'=>$id));
		

		return 1;
	}
	function getTagNameById($code){
		////$this->db->cache_on();
		$this->db->select('*');
		$this->db->from('ar_tag');
		$this->db->where('tag_code',$code);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}

	//Hub Report
	function addHubReport($data){
		
		$this->db->insert('hub_reports', $data);				
		return 1;   
		
	}

	//Hub Hub Delete By Owner
	function hubDeleteByOwner($hid, $mid)
	{
		$this->db->where('hid', $hid);
		$this->db->where('mid', $mid);
		$res = $this->db->delete('ar_hub');				
		return $res;

	}

	function insertHubNewsComments($data)
	{
		$this->db->insert('hub_comments',$data);
		$res = $this->db->insert_id();
		return $res;
		
	}

	function insertHubNewsLike($data)
	{
		$res = $this->db->insert('hub_like',$data);
		return $res;
	}

	function insertHubRelation($data)
	{
		$res = $this->db->insert('hub_new_relation',$data);
		return $res; 
	}

	function getNewsComments($id)
	{	////$this->db->cache_on();
		$this->db->select('*');
		$this->db->from('hub_comments');
		$this->db->where('hub_news_id',$id);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}

	function getNewsLike($id)
	{	//$this->db->cache_on();
		$this->db->select('count(*) as ct');
		$this->db->from('hub_like');
		$this->db->where('news_id',$id);
		$query = $this->db->get();
		$res = $query->result_array();
		//echo $this->db->last_query();
		return $res[0]['ct'];
	}

	function getNewsShareCount($id)
	{	//$this->db->cache_on();
		$this->db->select('count(*) as ct');
		$this->db->from('hub_new_relation');
		$this->db->where('news_id',$id);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res[0]['ct'];
	}

	function getNewsCommentCount($id)
	{	//$this->db->cache_on();
		$this->db->select('count(*) as ct');
		$this->db->from('hub_comments');
		$this->db->where('hub_news_id',$id);
		$query = $this->db->get();
		$res = $query->result_array();
		return $res[0]['ct'];
	}
	
	function dislikeNews($mid,$newsId)
	{
		$this->db->where('mid', $mid);  
		$this->db->where('news_id', $newsId);
		$res = $this->db->delete('hub_like'); 
		return $res;
	}
	//function whoseShare($mid,$hub_new_id,$cur_date){
	function whoseShare($mid,$hub_new_id){
		////$this->db->cache_on();
		
		$this->db->select('owner_mid');
		$this->db->from('hub_share');
		//$this->db->where('mid',$mid);
		$this->db->where('hub_id_new',$hub_new_id);
		//$this->db->where('share_date',$cur_date);
		$query = $this->db->get();
		$res = $query->result_array();
        
		//var_dump($res);
		if($res[0]['owner_mid']!=''){
			$id=$res[0]['owner_mid'];
		}else{
			$id='';
		}
		//echo $id;
		return $id;
	}

	//----------------------------Admin Model-----------------
		function ar_subsector_list()
	{
	$data=array();
			$sql = "select 	distinct(sub_sector_id) from ar_apinews";
			
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0)
			{foreach ($rs as $result) 
			{	
				$data[]=$result;
			}
		}
		return $data;
	}

	function insertNewsImage($data)
	{
		$res = $this->db->insert('news_image',$data);
		return $res;
	}

	function checkImageExist($id)
	{
		$this->db->select('*');
		$this->db->from('news_image');
		$this->db->where('news_id',$id);
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function getAllBingNews()
	{
		$this->db->select('*');
		$this->db->where('jsondata','');
		$this->db->from('ar_apinews');
		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}

	function insertNewsLogo($data)
	{
		$res = $this->db->insert('news_image',$data);
		return $res;
	}

	function updateNewsLogo($data)
	{
		$id 		= $data['news_id'];
		$logoUrl 	= $data['logo']; 

		$res['logo']  = $logoUrl;

		$this->db->where('news_id',$id);
		$res = $this->db->update('news_image', $res); 
		return $res;
	}

	function updateNewsImage($data)
	{
		$id 		= $data['news_id'];
		$logoUrl 	= $data['image_url']; 

		$res['image_url']  = $logoUrl;

		$this->db->where('news_id',$id);
		$res = $this->db->update('news_image', $res); 
		return $res;
	}
	
	function newsBlockFromUser($mid,$newsId){
		$data['mid']=$mid;
		$data['news_id']=$newsId;
		$data['block_date']=date('Y-m-d H:i:s');
		
		$res = $this->db->insert('news_block_from_user',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
		
	}

	function getUserNameOutsideConnects($hid,$cur_date)
	{	//$this->db->cache_on();
		$this->db->select('*');
		$this->db->from('hub_like');
		$this->db->where('hub_id',$hid);
		$this->db->where('like_date',$cur_date);
		$this->db->order_by("hl_id","desc");
		$this->db->limit(1, 0);

		$query = $this->db->get();
		//echo $this->db->last_query();
		$res = $query->result_array();
		return $res;
	}
	
function getUserNameOutsideConnectsComment($hid,$cur_date)
	{	//$this->db->cache_on();
		$this->db->select('*');
		$this->db->from('hub_comments');
		$this->db->where('hub_id',$hid);
		$this->db->where('comm_date',$cur_date);
		$this->db->order_by("comm_id","desc");
		$this->db->limit(1, 0);

		$query = $this->db->get();
		$res = $query->result_array();
		return $res;
	}

// Blocking of the hub user code start

function FnFindBlockedHubUserById($member_id)
{
        //$this->db->cache_on();
		$this->db->select('blocked_users');
		$this->db->from('block_hub_user');
		$this->db->where('blocked_by',$member_id);
		$query = $this->db->get();
		$res = $query->result_array();
		//echo $this->db->last_query();die();
		//print_r($res);die();
		$blockedarray=array();
		if(count($res)>0)
		{
			foreach($res as $singlearray)
	        {
	          $blockedarray[]=$singlearray['blocked_users'];
	        }
		}
        
		return $blockedarray;
}	

function FnBlockHubUser($client_id,$hub_id)
{
		$data['blocked_by']=$client_id;
		$data['blocked_users']=$hub_id;
		$res = $this->db->insert('block_hub_user',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
		
}

// Blocking of the hub user code end


 function FnFindLikesHidsInConnection($mids,$start,$limit){
//var_dump($start.','.$limit);exit;
		$data = array();
		//$this->db->select('hub.hid,hub.mid,hublike.hub_id,hublike.mid,hublike.like_date,hub.posted_on');
		$this->db->select('hub.hid');
		$this->db->from('ar_hub as hub');
		$this->db->join('hub_like as hublike','hub.hid=hublike.hub_id','INNER');
		$this->db->where_in('hublike.mid',$mids);
		$this->db->order_by('hub.hid','desc');
		$this->db->limit($limit,$start);
		$q=$this->db->get();
		//echo $this->db->last_query(); 
		if($q->num_rows>0)
		{
			foreach ($q->result_array() as $result) 
			{	
				$data[]=$result['hid'];
			}
		}
		
		return $data;	
		
	}
	
	function FnFindCommentsHidsInConnection($mids,$start,$limit){
//var_dump($start.','.$limit);exit;
		$data = array();
		//$this->db->select('hub.hid,hub.mid,comment.hub_id,comment.mid,comment.comm_date,hub.posted_on');
		$this->db->select('hub.hid');
		$this->db->from('ar_hub as hub');
		$this->db->join('hub_comments  as comment','hub.hid=comment.hub_id','INNER');
		$this->db->where_in('comment.mid',$mids);
		$this->db->order_by('hub.hid','desc');
		$this->db->limit($limit,$start);
		$q=$this->db->get();
//echo $this->db->last_query(); 
		if($q->num_rows>0)
		{
			foreach ($q->result_array() as $result) 
			{	
				$data[]=$result['hid'];
			}
		}
		return $data;
		
	}

	function FnFindShareHidsInConnection($mids,$start,$limit){
    //var_dump($start.','.$limit);exit;
		$data = array();
		//$this->db->select('hub.hid,hub.mid,share.hub_id_old,share.mid,share.share_date,hub.posted_on');
		$this->db->select('hub.hid');
		$this->db->from('ar_hub as hub');
		$this->db->join('hub_share as share','hub.hid=share.hub_id_old','INNER');
		$this->db->where_in('share.mid',$mids);
		$this->db->order_by('share.share_date','desc'); 
		$this->db->limit($limit,$start);
		$q=$this->db->get();
		//echo $this->db->last_query(); die();
		if($q->num_rows>0)
		{
			foreach ($q->result_array() as $result) 
			{	
				$data[]=$result['hid'];
			}
		}
		return $data;
		
	}
	
	function FnLikeHubComment($data)
	{
		$res = $this->db->insert('hub_comm_like',$data);
		$insert_id = $this->db->insert_id();
		return  $insert_id;
	}
	
	function FnTotalCommentCount($data)
	{
		$this->db->select('*');
		$this->db->where('hub_comment_id', $data['hub_comment_id']);
		$query=$this->db->get('hub_comm_like');
		return  $query->num_rows();
	}
	
	function Fnuser_comment_like_status($data)
	{
		$this->db->select('*');
		$this->db->where('hub_comment_id', $data['hub_comment_id']);
		$this->db->where('mid', $data['mid']);
		$query=$this->db->get('hub_comm_like');
		$status=0;
		if($query->num_rows()>0)
		{
			$status=1;
		}
		
		return $status;
	}
	
	

	
	function FnDislikeLikeHubComment($data)
	{
		$this->db->where('hub_comment_id', $data['hub_comment_id']);
		$this->db->where('mid', $data['mid']);
		if($data['hub_id']==null)
		{
			$this->db->where('news_id', $data['news_id']);
		}
		else
		{
			$this->db->where('hub_id', $data['hub_id']);
		}
		
		$res = $this->db->delete('hub_comm_like'); 
		//echo $this->db->last_query();
		return $res;
	}
	
	function FnDeleteCommentandCommentLike($comment_id)
	{
		$sql = "DELETE hub_comments, hub_comm_like FROM hub_comments, hub_comm_like WHERE hub_comments.comm_id = hub_comm_like.hub_comment_id and hub_comments.comm_id='".$comment_id."'";
		$query = $this->db->query($sql);
		return 1;
	}
	function FnGetOldSharemid($hub_id)
	{
		$this->db->select('owner_mid');
		$this->db->where('hub_id_new', $hub_id);
		$query=$this->db->get('hub_share');
		
		$mainmid='';
		if($query->num_rows()==1)
		{
		   $resultarr=$query->row_array();
		   $mainmid=$resultarr['owner_mid'];	
		}
		return $mainmid;
	}




 }
