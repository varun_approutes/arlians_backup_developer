<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_memberprofile extends CI_model
{
 
	function __construct(){

			parent::__construct();
			$this->load->model('model_message');
		}
	function getUserDetails(){
		
		$id = $this->session->userdata['logged_in']['id'];		
		$this->db->select('*');
		$this->db->from('ar_members');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rs = $query->result_array();
		
		$data['fname'] = $rs[0]['fname'];
		$data['lname'] = $rs[0]['lname'];
		$data['email'] = $rs[0]['email'];
		$data['sharecode'] = $rs[0]['sharecode'];
		
		$this->db->select('*');
		$this->db->from('ar_request_access_page');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rsb = $query->result_array(); //echo '<pre>'; var_dump($rsb); die();
		
		$data['bussinessname'] = $rsb[0]['bussinessname'];
		
		$data['tag_line'] = $rsb[0]['tag_line'];
		$data['office_name'] = $rsb[0]['office_name'];
		$data['city'] = $rsb[0]['city'];
		$data['country'] = $rsb[0]['country'];
		$data['bussinessdesc'] = $rsb[0]['bussinessdesc'];
		$data['bussinesstype1'] = $rsb[0]['bussinesstype'];
		$data['about_me'] = $rsb[0]['about_me'];

		$this->db->select('ans_set_cat_id');
		$this->db->from('ar_member_data_wiz_one');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rsb1 = $query->result_array();
		
		$data['wiz_one_ans'] = $rsb1;
		
		$this->db->select('*');
		$this->db->from('member_image');
		$this->db->where('mid',$id);
		$this->db->where('type','profile_img');
		$query = $this->db->get();
		
		$rsb2 = $query->result_array();
		
		if($rsb2 == NULL){
			$imgValue = "profile_image.png";
		}else{
			$imgValue = $rsb2[0]['image_url'];
		}
		$data['profile_image'] = $imgValue;
		
		$this->db->select('*');
		$this->db->from('member_image');
		$this->db->where('mid',$id);
		$this->db->where('type','back_img');
		$query = $this->db->get();
		
		$rsb3 = $query->result_array();
		
		if($rsb3 == NULL){
			$imgValue1 = "profile_image.png";
		}else{
			$imgValue1 = $rsb3[0]['image_url'];
		}
		$data['background_image'] = $imgValue1;
		
		$data['questionone'] = $this->getMemberLookingForData($id);
		
		$data['questionTwo'] = $this->getMemberLocalityData($id);
		
		$data['questionThree'] = $this->getMemberServiceData($id);
		
		$data['questionFour'] = $this->getMemberIndustryData($id);
		
		$data['questionFive'] = $this->getMemberSectorData($id);
		
		$data['questionSix'] = $this->getMemberlookingForBussinessData($id);

		$data['connection_list'] = $this->getCOnnectionDetails($id);


		$data['wiz_two_quiz_one_and_six_selected_biz_ids'] = $this->wizTwoQuizOneAndSixSelectedBizIds($id);
		$data['wiz_two_quiz_four_and_five_selected_biz_ids'] = $this->wizTwoQuizFourAndFiveSelectedBizIds($id);
		$data['locality_to_explore'] = $this->wizTwoQuizLocalityToExplore($id);
		$data['customer_type'] = $this->wizTwoQuizCustomerType($id);
		$data['looking_service'] = $this->wizTwoQuizLookingService($id);
		$data['latest_post'] = $this->getUserLatestFivePost($id);
		$data['doc_uploaded'] = $this->getUserDocUploaded($id);
		$data['has_founding_member'] = $this->paymentType($id);
		return $data;
		
	}
	function getUserDetailsApi($id){
		
			
		$this->db->select('*');
		$this->db->from('ar_members');
		$this->db->where('status','1');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rs = $query->result_array();
		
		$data['fname'] = $rs[0]['fname'];
		$data['lname'] = $rs[0]['lname'];
		$data['email'] = $rs[0]['email'];
		$data['sharecode'] = $rs[0]['sharecode'];
		
		$this->db->select('*');
		$this->db->from('ar_request_access_page');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rsb = $query->result_array(); //echo '<pre>'; var_dump($rsb); die();
		$data['mid']=$id;
		$data['bussinessname'] = $rsb[0]['bussinessname'];
		$em = logobussinessname($data['mid']);
      	$data['logobussinessname'] = $em;
		$data['userProfType'] = userProfType($id);
		//$data['logobussinessname'] = strtoupper(substr($rsb[0]['bussinessname'], 0, 1));
		$data['tag_line'] = $rsb[0]['tag_line'];
		$data['office_name'] = $rsb[0]['office_name'];
		$data['city'] = $rsb[0]['city'];
		$data['country'] = $rsb[0]['country'];
		$data['countryName'] = countryName($rsb[0]['country']);
		$data['bussinessdesc'] = $rsb[0]['bussinessdesc'];
		$data['bussinesstype1'] = $rsb[0]['bussinesstype'];
		$data['about_me'] = $rsb[0]['about_me'];

		$rsb2=fetch_my_sector_pro($data['mid']);
		foreach($rsb2 as $key => $business_name){
			//$secname=explode('-',$business_name["ans_set_cat_id"]);
			$sectorname[$key]['name'] = $business_name['subname'];
			//$sector = array("name" => sector_nameApi($secname[1]));
			
		}
		//subname
		
		$data['sectorname']= $sectorname;
		//$data['sectorname']= fetch_my_sector_pro($data['mid']);
		//
		$this->db->select('*');
		$this->db->from('member_image');
		$this->db->where('mid',$id);
		$this->db->where('type','profile_img');
		$query = $this->db->get();
		
		$rsb2 = $query->result_array();
		
		if($rsb2 == NULL){
			$imgValue = "";
		}else{
			$imgValue = base_url().'/uploads/profile_image/'.$rsb2[0]['image_url'];
		}
		
		$data['profile_image'] = $imgValue;
		
		$this->db->select('*');
		$this->db->from('member_image');
		$this->db->where('mid',$id);
		$this->db->where('type','back_img');
		$query = $this->db->get();
		
		$rsb3 = $query->result_array();
		
		if($rsb3 == NULL){
			$imgValue1 = "";
		}else{
			$imgValue1 =  base_url().'/uploads/profile_image/'.$rsb3[0]['image_url'];
		}
		$data['background_image'] = $imgValue1;
		
		//$data['questionone'] = $this->getMemberLookingForData($id);
		
		//$data['questionTwo'] = $this->getMemberLocalityData($id);
		
		//$data['questionThree'] = $this->getMemberServiceData($id);
		
		//$data['questionFour'] = $this->getMemberIndustryData($id);
		
		//$data['questionFive'] = $this->getMemberSectorData($id);
		
		//$data['questionSix'] = $this->getMemberlookingForBussinessData($id);

		$data['connection_list'] = $this->getCOnnectionDetails($id);
		$data['connection_no'] = $this->connectioncount($id);
		$data['post_no'] = $this->postCount($id);
		$data['publish_no'] = $this->publishCount($id);
		$data['opentalks_no'] = $this->talkCount($id);
		$data['latest_msg']= $this->model_message->inbox_editpage($id);
		$data['connection_with']= $this->connectWith($id);
		$data['profileComplition']= $this->profileComplition($id).'%';
		$data['publishList']= $this->publishList($id);

		$data['wiz_two_quiz_one_and_six_selected_biz_ids'] = $this->wizTwoQuizOneAndSixSelectedBizIds($id);
		$data['wiz_two_quiz_four_and_five_selected_biz_ids'] = $this->wizTwoQuizFourAndFiveSelectedBizIds($id);
		$data['locality_to_explore'] = $this->wizTwoQuizLocalityToExplore($id);
		$data['customer_type'] = $this->wizTwoQuizCustomerType($id);
		$data['looking_service'] = $this->wizTwoQuizLookingService($id);
		$data['latest_post'] = $this->getUserLatestFivePost($id);
		$data['doc_uploaded'] = $this->getUserDocUploaded($id);
		$data['has_founding_member'] = $this->paymentType($id);
		$data['my_community'] = mytalk($id);
		return $data;
		
	}
	
	
	
	function getUserDocUploaded($id){
		$this->db->select('*');
		$this->db->from('ar_user_doc');
		$this->db->where('mid',$id);
		$this->db->order_by("doc_id", "desc");		
		$query = $this->db->get();			 
		$result = $query->result_array();
		 $i=0;
		$sug_member=array();
		foreach ($result as $key=>$value){
			foreach ($value as $k=>$val){
				$sug_member[$i][$k]=$val;
				
			}
			$ext = explode('.', basename($sug_member[$i]['file_name']));//explode file name from dot(.) 
			$file_extension = end($ext);
			$sug_member[$i]['ext']= $file_extension;
			$sug_member[$i]['path']=  base_url().$sug_member[$i]['path'];
			// $sug_member[$i]['bussinessdesc']= substr(strip_tags($sug_member[$i]['bussinessdesc']), 0, 250); 
			//
		$i++;	
		}
//var_dump($sug_member);
//die();
	return  $sug_member;	
		//return $result;	
	}
	function paymentType($mid){
    	$this->db->from('ar_transaction');
    	$this->db->where('mid', $mid);
		$this->db->order_by('mid desc'); 
		$this->db->limit(0,1); 
        $query = $this->db->get();
		$res = $query->row();
		if ($query->num_rows() > 0){
			return $res->subscription_type;
				
		}else{
			return '';
		}	
		
	}
	function getUserLatestFivePost($id){
		$this->db->select('*');
		$this->db->from('ar_hub');
		$this->db->where('mid',$id);
		$this->db->order_by("hid", "desc");
		$this->db->limit(6,0);
		$query = $this->db->get();			 
		$result = $query->result_array();
		return $result;	
	}
	function wizTwoQuizLookingService($id){
		$this->db->select('looking_service');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid',$id);
		$query = $this->db->get();	
		$result = $query->row();	
		//print_r($result); exit;	
		$result = trim($result->looking_service,',');
		$result = explode(',', $result);
		$this->db->select('*');
		$this->db->from('ar_looking_services');
		$this->db->where_in('look_service_id',$result);		
		$query = $this->db->get();	
		$data = $query->result();	
		//print_r($data); exit;
		return $data;
	}
	function wizTwoQuizCustomerType($id){
		$this->db->select('is_service');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid',$id);
		$query = $this->db->get();	
		$result = $query->row();		
		$result = trim($result->is_service,',');
		$result = explode(',', $result);
		$this->db->select('*');
		$this->db->from('ar_is_service');
		$this->db->where_in('service_id',$result);		
		$query = $this->db->get();	
		$data = $query->result();	
		//echo $this->db->last_query(); exit;
		return $data;
	}
	function wizTwoQuizLocalityToExplore($id){
		$this->db->select('locality');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid',$id);
		$query = $this->db->get();	
		$result = $query->row();		
		$result = trim($result->locality,',');
		$result = explode(',', $result);
		$this->db->select('*');
		$this->db->from('ar_locality');
		$this->db->where_in('locality_id',$result);		
		$query = $this->db->get();	
		$data = $query->result();	
		//echo $this->db->last_query(); exit;
		return $data;
	}

	function getLocalityToExplore($id){
		$this->db->select('locality');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid',$id);
		$query = $this->db->get();	
		$result = $query->row();		
		//print_r($result); exit;
		$result = trim($result->locality,',');
		$result = explode(',', $result);
		//echo $result; exit;
		$this->db->select('*');
		$this->db->from('ar_locality');
		$this->db->where_in('locality_id',$result);		
		$query = $this->db->get();	
		$data = $query->result();	
		//echo $this->db->last_query(); exit;
		return $data;
	}

	function wizTwoQuizOneAndSixSelectedBizIds($id){
		$this->db->select('looking_for,looking_service');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid',$id);
		$query = $this->db->get();	
		$result = $query->row();

		$interested_looking_for_ids = explode(',', $result->looking_for);
		$interested_services_ids = explode(',', $result->looking_service);
		$this->db->select('look_service_name');
		$this->db->from('ar_looking_services');
		$this->db->where_in('look_service_id', $interested_services_ids);
		$query_interested_services = $this->db->get();	
		$data['interested_services_result'] = $query_interested_services->result_array();

		$this->db->select('looking_for_name');
		$this->db->from('ar_looking_for');
		$this->db->where_in('looking_for_id', $interested_looking_for_ids);
		$query_interested_services = $this->db->get();
		$data['interested_looking_for_result'] = $query_interested_services->result_array();
		//print_r($data); exit;
		return $data;
	}

	function wizTwoQuizFourAndFiveSelectedBizIds($id){
		$this->db->select('look.industry_code,look.target_sector_code,ar_industry.industry_name,ar_industry.profile_image as industry_img,ar_target_sector.tar_sector_name,ar_target_sector.profile_image as sector_img');
		$this->db->from('ar_wiz_two_looking_industry as look');
		$this->db->join('ar_industry','ar_industry.industry_code = look.industry_code');
		$this->db->join('ar_target_sector','ar_target_sector.tar_sector_code = look.target_sector_code');
		$this->db->where('look.mid',$id);
		$query = $this->db->get();	
		$result = $query->result_array();
		return $result;
		//print_r($result); exit;
	}


	function getParticularUserDetails($id){
		
		//$id = $this->session->userdata['logged_in']['id'];
		
		$this->db->select('*');
		$this->db->from('ar_members');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rs = $query->result_array();
		
		$data['name'] = $rs[0]['email'];
		
		$this->db->select('*');
		$this->db->from('ar_request_access_page');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rsb = $query->result_array(); //echo '<pre>'; var_dump($rsb); die();
		$em = str_split($rs[0]['email']);
       // $profile_initional = $em[0]; 
		$data['logobussinessname'] =$em[0];
		$data['bussinessname'] = $rsb[0]['bussinessname'];
		$data['city'] = $rsb[0]['city'];
		$data['country'] = $rsb[0]['country'];
		$data['bussinessdesc'] = $rsb[0]['bussinessdesc'];
		$data['bussinesstype1'] = $rsb[0]['bussinesstype'];
		$data['tag_line'] = $rsb[0]['tag_line'];
		$this->db->select('ans_set_cat_id');
		$this->db->from('ar_member_data_wiz_one');
		$this->db->where('mid',$id);
		$query = $this->db->get();
		
		$rsb1 = $query->result_array();
		
		$data['wiz_one_ans'] = $rsb1;
		
		$this->db->select('*');
		$this->db->from('member_image');
		$this->db->where('mid',$id);
		$this->db->where('type','profile_img');
		$query = $this->db->get();
		
		$rsb2 = $query->result_array();
		
		if($rsb2 == NULL){
			$imgValue = "";
		}else{
			$imgValue = base_url().'/uploads/profile_image/'.$rsb2[0]['image_url'];
		}
		$data['profile_image'] = $imgValue;
		
		$this->db->select('*');
		$this->db->from('member_image');
		$this->db->where('mid',$id);
		$this->db->where('type','back_img');
		$query = $this->db->get();
		
		$rsb3 = $query->result_array();
		
		if($rsb3 == NULL){
			$imgValue1 = "";
		}else{
			$imgValue1 = base_url().'/uploads/profile_image/'.$rsb3[0]['image_url'];
		}
		$data['background_image'] = $imgValue1;
		
		$data['questionone'] = $this->getMemberLookingForData($id);
		
		$data['questionTwo'] = $this->getMemberLocalityData($id);
		
		$data['questionThree'] = $this->getMemberServiceData($id);
		
		$data['questionFour'] = $this->getMemberIndustryData($id);
		
		$data['questionFive'] = $this->getMemberSectorData($id);
		
		$data['questionSix'] = $this->getMemberlookingForBussinessData($id);
		
		return $data;
		
	}

function connectioncount($id){
	$sql = " SELECT * FROM (`connected`)  WHERE `is_accepted` = '1' and (`mid` = ".$id." OR `connected_id` = ".$id." ) ORDER BY `mid` DESC";
	$query = $this->db->query($sql);
	
	
	
	
	/* $this->db->where('connected_id',$id);
	$this->db->or_where('mid',$id);
	$this->db->where('is_accepted','1'); 
	//$this->db->from('connected');
	echo $num_rows = $this->db->count_all_results('connected'); die();*/
	return $query->num_rows();
}
function postCount($id){
	$this->db->where('mid',$id);
	$this->db->where('shared_for !=',4);
	$num_rows = $this->db->count_all_results('ar_hub');
	return $num_rows;	
}
function publishCount($id){
	$this->db->where('mid',$id);
	$this->db->where('shared_for =',4);
	$num_rows = $this->db->count_all_results('ar_hub');
	return $num_rows;	
}
function talkCount($id){
	$this->db->where('mid',$id);
	$this->db->where('status','1');
	$num_rows = $this->db->count_all_results('ar_community');
	return $num_rows;	
}
function publishList($id){
	$this->db->from('ar_hub');
    $this->db->where('ar_hub.mid',$id);
    $this->db->where('ar_hub.shared_for =',4);
    $query = $this->db->get();
//echo $this->db->last_query();
    $res = $query->result_array();	
  $i=0;
  $sug_member=array();
foreach ($res as $key=>$value){
	foreach ($value as $k=>$val){
		$sug_member[$i][$k]=$val;
	}
	/* $sug_member[$i]['content']= substr(strip_tags($sug_member[$i]['content']), 0, 250); 
	  $sug_member[$i]['fname']= substr(strip_tags($sug_member[$i]['content']), 0, 250); 
	 $sug_member[$i]['lname']= substr(strip_tags($sug_member[$i]['content']), 0, 250);  */
	 $sug_member[$i]['bussinessname']= getMemberbusinName($value['mid']); 
	 $sug_member[$i]['logobussinessname']= logobussinessname($value['mid']);
	 $sug_member[$i]['tag_line']= $this->FnGetUserTagLine($value['mid']);
	 $currentsessionimg_u='';
	if($this->memberlogo($value['mid']) != NULL) 
	{
		$in = $this->memberlogo($value['mid']);
		$currentsessionimg_u = base_url().'uploads/profile_image/'.$in['image_url'];
	}
	 $sug_member[$i]['post_share_count']= $this->FnPublishShareCount($value['hid']);
     $sug_member[$i]['post_comment_count']= $this->FnPublishCommentCount($value['hid']);
	 $sug_member[$i]['post_like_count']= $this->FnPublishLikeCount($value['hid']);
	 $sug_member[$i]['logoimage']= $currentsessionimg_u;
	//
$i++;	
}
	return $sug_member;		
}

function FnPublishLikeCount($hub_id)
{
	$this->db->from('hub_like');
    $this->db->where('hub_like.hub_id',$hub_id);
    $query = $this->db->get();
	return $query->num_rows();
}

function FnPublishShareCount($hub_id)
{
	$this->db->from('hub_share');
    $this->db->where('hub_share.hub_id_old',$hub_id);
    $query = $this->db->get();
	return $query->num_rows();
}

function FnPublishCommentCount($hub_id)
{
	$this->db->from('hub_comments');
    $this->db->where('hub_comments.hub_id',$hub_id);
    $query = $this->db->get();
	return $query->num_rows();
}




function FnGetUserTagLine($id)
{
	$this->db->from('ar_request_access_page');
    $this->db->where('mid',$id);
    $query = $this->db->get('');
    $res = $query->row_array();
	$tagline='';
	if(($query->num_rows)==1)
	{
		$tagline=$res['tag_line'];
	}
	return $tagline;
}

function memberlogo($id)
{
	$this->db->select('image_url');
	$this->db->from('member_image');
	$this->db->where('mid',$id);
	$this->db->where('type','sign_img');
	$query = $this->db->get();
	$res = $query->result_array();
	return $res[0];
}

function latestMessage($id){
		$this->db->select('ar_message.msg_id,ar_message.msg_sub,ar_message.msg_body,ar_message.msg_time,ar_message_trans.sender_id,ar_message_trans.sender_id,member_image.image_url');
		$this->db->join('ar_message_trans', 'ar_message_trans.msg_id = ar_message.msg_id', 'left');
		$this->db->join('member_image', 'member_image.mid = ar_message_trans.sender_id and member_image.type = "profile_img"', 'left');
		//$this->db->join('ar_members', 'member_image.mid = ar_members.mid', 'INNER');
		$this->db->from('ar_message');
		$this->db->where('ar_message_trans.receiver_id',$id);
		//$this->db->where('ar_members.status','1');
		$this->db->order_by('ar_message.msg_id', 'desc');
		$this->db->limit(5);
		$query = $this->db->get();
		$res = $query->result_array();	
		if(count($res)>0)
		{
			foreach($res as $key=>$resultarr)
			{
				$result[$key]['msg_id'] = $resultarr['msg_id'];
				$result[$key]['msg_sub'] = $resultarr['msg_sub'];
				$result[$key]['msg_body'] = $resultarr['msg_body'];
				$result[$key]['msg_time'] = $resultarr['msg_time'];
				$result[$key]['sender_id'] = $resultarr['sender_id'];
				$result[$key]['bussinessname']= getMemberbusinName($resultarr['sender_id']);
				$result[$key]['image_url'] = $resultarr['image_url'];
				$em = logobussinessname($resultarr['sender_id']);
				$result[$key]['logobussinessname'] = $em;
				$result[$key]['posttime'] = calculateTimeFromPost( $resultarr['msg_time'], date("Y-m-d H:i:s"), "s");
			}
		}
		return $result;
	
	
}
function connectWith($id){
	$this->db->select('ar_score.suggested_id,ar_score.score,ar_request_access_page.bussinessname,ar_request_access_page.bussinessdesc,member_image.image_url,ar_members.email');
	$this->db->join('ar_request_access_page', 'ar_request_access_page.mid = ar_score.suggested_id', 'left');
	$this->db->join('member_image', 'member_image.mid = ar_score.suggested_id and member_image.type = "profile_img"', 'left');
	$this->db->join('ar_members', 'ar_members.mid = ar_score.mid', 'left');
	$this->db->from('ar_score');
    $this->db->where('ar_score.mid',$id);
	$this->db->where('ar_members.status','1');
    $this->db->where('ar_score.score !=',0);
    $this->db->where('ar_score.suggested_id !=',$id);
    $query = $this->db->get();
    $res = $query->result_array();

  $i=0;
  $sug_member=array();
foreach ($res as $key=>$value){
	foreach ($value as $k=>$val){
		$sug_member[$i][$k]=$val;
		
	}

	$em = logobussinessname($sug_member[$i]['suggested_id']);
    $sug_member[$i]['logobussinessname'] = $em;
    // $sug_member[$i]['bussinessdesc']= substr(strip_tags($sug_member[$i]['bussinessdesc']), 0, 250); 
	//
$i++;	
}
//var_dump($sug_member);
//die();
	return  $sug_member;	
}

function isDescriptionExist($id){
	$this->db->select('bussinessdesc');
	$this->db->from('ar_request_access_page');
	$this->db->where('mid',$id);
	$query = $this->db->get();
		$res = $query->row();
		//var_dump($res->bussinessdesc);
		if($res->bussinessdesc!=''){
			return 50;
		}else{
			return 0;
		}
}
function isProfilePicture($id){
	$this->db->select('image_url');
	$this->db->from('member_image');
	$this->db->where('mid',$id);
	$this->db->where('type','profile_img');
	$query = $this->db->get();
		$res = $query->row();
		if ($query->num_rows() > 0){
			if($res->image_url!=''){
				return 20;
			}else{
				return 0;
			}
		}else{
			return 0;
		}	
}
function isBannerPicture($id){
	$this->db->select('image_url');
	$this->db->from('member_image');
	$this->db->where('mid',$id);
	$this->db->where('type','back_img');
	$query = $this->db->get();
		$res = $query->row();
		if ($query->num_rows() > 0){
			if($res->image_url!=''){
				return 20;
			}else{
				return 0;
			}
		}else{
			return 0;
		}		
}
function profileComplition($id){
	
$description=$this->isDescriptionExist($id);
$profile_image=$this->isProfilePicture($id);
$banner_image=$this->isBannerPicture($id);
$portfolio=0;
$profileComplition=$description+$profile_image+$banner_image+$portfolio;
return $profileComplition;
}
	function getCOnnectionDetails($id){
		$this->db->select('connected.mid,connected.connected_id,ar_request_access_page.bussinessname,ar_request_access_page.bussinessdesc,member_image.image_url');
		$this->db->join('ar_request_access_page', 'ar_request_access_page.mid = connected.mid', 'left');
		$this->db->join('member_image', 'member_image.mid = connected.mid and member_image.type = "profile_img"', 'left');
		$this->db->from('connected');
		$this->db->where('connected.connected_id',$id);
		$this->db->or_where('connected.mid',$id);
		$this->db->where('connected.is_accepted','1');
		$query = $this->db->get();
		$res = $query->result_array();	
		return $res;
	}
	function getselectedLookinfFor($id){	
		$this->db->select('looking_for');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid', $id);
		$query = $this->db->get();
			if ($query->num_rows() > 0){
			$result = $query->result_array();


			$x = $result[0]['looking_for'];
			$arr = explode(',',$x);
			
			$answerOne = array();
			array_pop($arr);
			$this->db->select('looking_for_name,looking_for_id');
			$this->db->from('ar_looking_for');
			$this->db->where_in('looking_for_id',$arr);
			$query1 = $this->db->get();

			$res = $query1->result();
			/*foreach($arr as $row){
				
				if($row == NULL) continue;
				
				$this->db->select('looking_for_name');
				$this->db->from('ar_looking_for');
				$this->db->where('looking_for_id',$row);
				$query1 = $this->db->get();
				$res = $query1->result_array();
				$answerOne[] = $res[0]['looking_for_name'];
			}*/
			return $res;
		} else{
			$arr = array();
			return $arr;
		}	
	}
	function getMemberLookingForData($id){
		
		$this->db->select('looking_for');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid', $id);
		$query = $this->db->get();
			if ($query->num_rows() > 0){
			$result = $query->result_array();


			$x = $result[0]['looking_for'];
			$arr = explode(',',$x);
			
			$answerOne = array();
			array_pop($arr);
			foreach($arr as $row){
				
				if($row == NULL) continue;
				
				$this->db->select('looking_for_name');
				$this->db->from('ar_looking_for');
				$this->db->where('looking_for_id',$row);
				$query1 = $this->db->get();
				$res = $query1->result_array();
				$answerOne[] = $res[0]['looking_for_name'];
			}
			return $answerOne;
		} else{
			$arr = array();
			return $arr;
		}

	}

	function getMemberLocalityData($id){
		
		$this->db->select('looking_for');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
		$result = $query->result_array();
		
		$x = $result[0]['looking_for'];
		$arr = explode(',',$x);
		
			$answerTwo = array();
		
		//echo '<pre>'; var_dump($arr); die();
			foreach($arr as $row){
				
				if($row == NULL || count($row) == 1) continue;
					
				$this->db->select('locality_type');
				$this->db->from('ar_locality');
				$this->db->where('locality_id',$row);
				$query1 = $this->db->get();
				$res = $query1->result_array();
				 
				//echo isset($row);
				$answerTwo[] = $res[0]['locality_type'];
			}
		return $answerTwo;
			} else{
			$arr = array();
			return $arr;
		}
	}

	function getMemberServiceData($id){
		
		$this->db->select('is_service');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid', $id);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
		$result = $query->result_array();
		
		$x = $result[0]['is_service'];
		$arr = explode(',',$x);
		
			$answerThree = array();
			
		foreach($arr as $row){
			
			if($row == NULL || count($row) == 1) continue;
			
			$this->db->select('service_name');
			$this->db->from('ar_is_service');
			$this->db->where('service_id',$row);
			$query1 = $this->db->get();
			$res = $query1->result_array();
			$answerThree[] = $res[0]['service_name'];
		}
		return $answerThree;
			} else{
			$arr = array();
			return $arr;
		}
	}

	function getMemberlookingForBussinessData($id){
		
		$this->db->select('looking_service');
		$this->db->from('ar_member_data_wiz_two');
		$this->db->where('mid', $id);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0){
		$result = $query->result_array();
		
		$x = $result[0]['looking_service'];
		$arr = explode(',',$x);
		
			$answerSix = array();
			
		foreach($arr as $row){
			
			if($row == NULL || count($row) == 1) continue;
			
			$this->db->select('looking_for_name');
			$this->db->from('ar_looking_for');
			$this->db->where('looking_for_id`',$row);
			$query1 = $this->db->get();
			$res = $query1->result_array();
			$answerSix[] = $res[0]['looking_for_name'];
		}
		return $answerSix;
				} else{
			$arr = array();
			return $arr;
		}
	}

	function getMemberIndustryData($id){
		
		$this->db->select('industry_code');
		$this->db->from('ar_wiz_two_looking_industry');
		$this->db->where('mid',$id);
		$query1 = $this->db->get();

		if ($query1->num_rows() > 0){
		$res = $query1->result_array();
		//echo '<pre>'; var_dump($res); die();
		$answerFour = array();

		foreach($res as $row){
			if($row == NULL || count($row) == 1) continue;
			
			$this->db->select('industry_name');
			$this->db->from('ar_industry');
			$this->db->where('industry_code`',$row['industry_code']);
			$query1 = $this->db->get();
			$res = $query1->result_array();
			$answerFour[] = $res[0]['industry_name'];
		}
		return $answerFour;
				} else{
			$arr = array();
			return $arr;
		}
	}

	function getMemberSectorData($id){
		
		$this->db->select('target_sector_code');
		$this->db->from('ar_wiz_two_looking_industry');
		$this->db->where('mid',$id);
		$query1 = $this->db->get();
		if ($query1->num_rows() > 0){
		$res = $query1->result_array();
		//echo '<pre>'; var_dump($res); die();
		$answerFive = array();
		
		foreach($res as $row){
			if($row == NULL || count($row) == 1) continue;
			
			$this->db->select('tar_sector_name');
			$this->db->from('ar_target_sector');
			$this->db->where('tar_sector_code`',$row['target_sector_code']);
			$query1 = $this->db->get();
			$res = $query1->result_array();
			$answerFive[] = $res[0]['tar_sector_name'];
		}
		return $answerFive;
					} else{
			$arr = array();
			return $arr;
		}
	}

	function updateImage($data){
		
		$id = $data['mid'];
		$sql = 'select exists(SELECT * FROM member_image WHERE type="profile_img" and mid='.$id.') as img_exist';
		$query = $this->db->query($sql);
		 $rs = $query->result_array();

		 if($rs[0]['img_exist']==0){
			 $this->db->insert('member_image',$data);
			 return 'success';
		 }else{
		 	//------------------------unlinking the previous image------

				$image_name = getProfileImage($id); 

		 	//----------------------------------------------------------
			$this->db->where('mid',$id); 
			$this->db->where('type','profile_img');
			$this->db->update('member_image',$data);
			return 'success';
		 }
	}
	
	function FnSaveSubscriotnValue($user_email,$subscription_status,$databasefield)
	{
		$sql = "Update ar_members Set `".$databasefield."`='".$subscription_status."' where email='".$user_email."'";
		$query = $this->db->query($sql);
		return "TRUE";
	}
	
	
	function updateSignImage($data){
		
		$id = $data['mid'];
		$sql = 'select exists(SELECT * FROM member_image WHERE type="sign_img" and mid='.$id.') as img_exist';
		$query = $this->db->query($sql);
		 $rs = $query->result_array();

		 if($rs[0]['img_exist']==0){
			 $this->db->insert('member_image',$data);
			 return 'success';
		 }else{
		 	//------------------------unlinking the previous image------

				$image_name = getProfileImage($id); 

		 	//----------------------------------------------------------
			$this->db->where('mid',$id); 
			$this->db->where('type','sign_img');
			$this->db->update('member_image',$data);
			return 'success';
		 }
	}

	function updateTitleForDoc($data){
		$this->db->where('mid',$data['mid']); 
		$this->db->where('doc_id',$data['doc_id']);
		$this->db->update('ar_user_doc',$data);
		return 'success';
	}
	function updateBackImage($data){
		
		$id = $data['mid'];
		$sql = 'select exists(SELECT * FROM member_image WHERE type="back_img" and mid='.$id.') as img_exist';
		$query = $this->db->query($sql);
		
		$rs = $query->result_array();
		
		 if($rs[0]['img_exist']==0){		  
			 $this->db->insert('member_image',$data);
			 return 'success';
		 }else{		 
			$this->db->where('mid',$id); 
			$this->db->where('type','back_img');
			$this->db->update('member_image',$data);		
			return 'success';
		 }
	}

	function editProfileName($data){	
		$id =  $data['mid'];
		$this->db->where('mid',$id);
		$this->db->update('ar_members',$data);
		if ($this->db->affected_rows() >= 0) {
		    return 'success'; // your code
		} else {
		    return false; // your code
		}
	}

	function editProfileEmail($data){	
		$id = $data['mid'];
		$this->db->where('mid',$id);
		$this->db->update('ar_members',$data);
		if ($this->db->affected_rows() >= 0) {
		    return 'success'; // your code
		} else {
		    return false; // your code
		}
	}

	function validateCurrentPassword($data){
		$mid = $data['mid'];
		$pass = md5($data['password']); 
		$sql = "SELECT * FROM ar_members WHERE mid='".$mid."' AND password='".$pass."'";	
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){  
			return "TRUE";
		} else{		
			return "FALSE";
		}
	}

	function resetPassword($data){
		$mid = $data['mid'];
		$data['password'] = md5($data['password']); 
		$this->db->where('mid',$mid);
		$this->db->update('ar_members',$data);
		if ($this->db->affected_rows() >= 0) {
		    return 'success'; // your code
		} else {
		    return false; // your code
		}
	}

	function editBussinessName($data){
		$id = $data['mid'];
		$this->db->where('mid',$id);
		$this->db->update('ar_request_access_page',$data);
		if ($this->db->affected_rows() >= 0) {
		    return 'success'; // your code
		} else {
		    return false; // your code
		}
	}

	function updateBizDescription($data){	
	
		$this->db->where('mid',$data['mid']);
		$this->db->update('ar_request_access_page',$data);
		if ($this->db->affected_rows() >= 0) {
		    return 'success'; // your code
		} else {
		    return false; // your code
		}
	}

	function editTagLine($data){	
		$this->db->where('mid',$data['mid']);
		$this->db->update('ar_request_access_page',$data);
		if ($this->db->affected_rows() >= 0) {
		    return 'success'; // your code
		} else {
		    return false; // your code
		}
	}

	function countryName($id){
	if($id!=''){
		$sql = "select * from countries where id=".$id;
		$query = $this->db->query($sql);
		$rs = $query->result_array();
	    return $rs[0]['country_name'];
	}else{
		return 'Please insert country name';
	}
 }
	function editMemberLocation($data){	

		$id = $data['mid'];
		$countryId=$data['country'];
		$this->db->where('mid',$id);
		$this->db->update('ar_request_access_page',$data);	
		if ($this->db->affected_rows() >= 0) {
			$countryName= $this->countryName($countryId);
			$res = array('response'=>'success','countryName'=>$countryName);
		   
		} else {
		   $res = array('response'=>'fail');
		   
		}
		 return $res; // your code
	}

	function getConnectedMemberListAll($id){
		//$this->db->select('*');
		$data=array();
		$sql = " SELECT * FROM (`connected`)  WHERE `is_accepted` = '1' and (`mid` = ".$id." OR `connected_id` = ".$id." ) ORDER BY `mid` DESC";
		$query = $this->db->query($sql);
		//echo $this->db->last_query();
		//$res = $query->result_array();
		if ($query->num_rows() > 0){
			$rs = $query->result_array();
			//print_r($rs);die();
			foreach($rs as $value){
				if($value['mid']==$id)
				{
					if($this->FnCheckStatus($value['connected_id'])==1)
					{
						$friends[]=$value['connected_id'];
					}
					
				}elseif($value['connected_id']==$id)
				{
					if($this->FnCheckStatus($value['mid'])==1)
					{
						$friends[]=$value['mid'];
					}
				}
			}
			//print_r($friends);die();
			return $friends;
		}

		
	}	
	function FnCheckStatus($mid)
	{
		$this->db->select('mid');
		$this->db->where('status', '1'); 
		$this->db->where('mid', $mid); 
		$query = $this->db->get('ar_members');
		//echo $query->num_rows();die();
		return $query->num_rows();
	}
	function getConnectedMemberListAllre($id){

		$this->db->where('connected_id', $id); 
		$this->db->where('is_accepted','0');
		$query = $this->db->get('connected');		
		if ($query->num_rows() > 0){
			return $rs = $query->result_array();
		}
	}	
	
	function getConnectedMemberList($id){
		
		$this->db->select('connected_id');
		$this->db->where('mid',$id);
		$this->db->where('is_accepted','1');
		$query = $this->db->get('connected');		
		if ($query->num_rows() > 0){
			return $rs = $query->result_array();
		}
	}

	function getImageFromSector($code){
		$this->db->select('profile_image');
		$this->db->where('sector_code',$code);
		$query = $this->db->get('ar_sector');

		if ($query->num_rows() > 0){
			return $rs = $query->result_array();
		}
	}

	function getNetworkSuggestedList($id=''){
		if($id!=''){
			$this->db->select('suggested_ids,searchtype');
			$this->db->from('ar_suggestion');
			//$this->db->where('mid',41);
			$this->db->where('mid',$id);
			$query = $this->db->get();	
			$result = $query->row();
			//print_r($result); exit;
			$suggested_ids = explode(',', $result->suggested_ids);			
			$this->db->select('member.mid,member.fname,member.lname,member.email,ar_request_access_page.bussinessname,ar_request_access_page.bussinesstype,member_image.image_url');
			$this->db->from('ar_members as member');
			$this->db->join('ar_request_access_page', 'ar_request_access_page.mid = member.mid', 'left');
			$this->db->join('member_image', 'member_image.mid = member.mid and member_image.type = "profile_img"', 'left');
			$this->db->where_in('member.mid', $suggested_ids);
			$query = $this->db->get();	
			$data = $query->result_array();
			//print_r($data); exit;
			return $data;
		}else{
			return false;
		}
	}

	function getNetworkConnectionList($id=''){
		if($id!=''){
			$this->db->select('*');
			$this->db->join('ar_request_access_page', 'ar_request_access_page.mid = connected.mid', 'left');
			$this->db->join('member_image', 'member_image.mid = connected.mid and member_image.type = "profile_img"', 'left');
			$this->db->from('connected');
			//$this->db->where('connected.mid',10);
			$this->db->where('connected.mid',$id);
			$query = $this->db->get();
			$res = $query->result_array();		
			return $res;
			//print_r($data); exit;
			return $data;
		}else{
			return false;
		}
	}

	function getNetworkConnectionRecievedList($id=''){
		if($id!=''){
			//$sql = "SELECT * FROM connected WHERE `is_accepted` = '0' AND (`connected_id` =".$id." OR `mid` = ".$id.")";			
			$sql = "SELECT * FROM connected WHERE `is_accepted` = '0' AND `mid` = ".$id;			
			$query = $this->db->query($sql);
			$res = $query->result_array();
			return $res;			
		}else{
			return false;
		}
	}

	function validateEmail($email){
		//$sql = 'SELECT * FROM ar_members WHERE email="'.$email.'"'; 
		$this->db->select('*');
		$this->db->from('ar_members');
		$this->db->where('email',$email['email']);	
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return "TRUE";
		} else{
			return "FALSE";
		}
	}

	function removeUserDetailFromWizTwo($data){
		//print_r($data); exit;
		$this->db->where($data['key'], $data['code']);
		$this->db->where('mid', $data['mid']);
		$query = $this->db->delete('ar_wiz_two_looking_industry'); 
		//echo $this->db->last_query(); exit;
		//$query = $this->db->get();
		
			return true;
		
	}
	function removeUserDetailFromWizOne($data){
		 $sql="delete from ar_member_data_wiz_one where wiz_one_id=".$data['wiz_one_id'];
		 $query = $this->db->query($sql);
		return true;
		
	}
	function updatesLookingFor($data){
		$d=array( 'looking_for'=>$data['looking_for']
			);
		$this->db->where('mid', $data['mid']);
		$this->db->update('ar_member_data_wiz_two', $d); 
		return true;
	}
	function updatesService($data){
		$d=array( 'is_service'=>$data['is_service']
			);
		$this->db->where('mid', $data['mid']);
		$this->db->update('ar_member_data_wiz_two', $d); 
		//echo $this->db->last_query(); exit;
		return true;
	}
	function updatesBuiznessService($data){
		$d=array( 'looking_service'=>$data['looking_service']
			);
		$this->db->where('mid', $data['mid']);
		$this->db->update('ar_member_data_wiz_two', $d); 
		//echo $this->db->last_query(); exit;
		return true;
	}
	function updatesLocalityFor($data){
	$d=array( 'locality'=>$data['locality']
			);
		$this->db->where('mid', $data['mid']);
		$this->db->update('ar_member_data_wiz_two', $d); 
		//echo $this->db->last_query();
		return true;

	}
	function updateTag($data){
		$this->db->where('mid', $data['mid']);
		$this->db->update('ar_request_access_page', $data);
		return true;	
	}
	function getTagsList(){
		$this->db->select('*');
		$this->db->from('ar_tag');
		$query = $this->db->get();
		$rs = $query->result_array();		
		return $rs;
	}
	function getSelectedTags($data){
		$this->db->select('tags');
		$this->db->where('mid', $data['mid']);
		$this->db->from('ar_request_access_page');
		$query = $this->db->get();
		$rs = $query->row();
		$result = trim($rs->tags,',');
		$result = explode(',', $result);
		
		$this->db->select('*');
		$this->db->from('ar_tag');
		$this->db->where_in('tag_code',$result);		
		$query = $this->db->get();	
		$data = $query->result();	
		return $data;
	}
	function deleteDocument($data){
		$this->db->where('doc_id', $data['doc_id']);
		$this->db->delete('ar_user_doc');
		if ($this->db->affected_rows() >= 0) {
		    return array('response'=>'success','message'=>'Document successfully deleted'); // your code
		} else {
		    return array('response'=>'fail','message'=>'Document not deleted'); // your code
		}
		
	}
	function manageMoreAddressSave($input_data){
		if($input_data['loc_id']!=''){
			$this->db->where('loc_id',$input_data['loc_id']); 		
			$this->db->update('ar_location',$input_data);
		}else{
			$this->db->insert('ar_location', $input_data);
		}
		return true;
	}
	function manageMoreAddressDelete($data){
		$this->db->where('loc_id', $data['loc_id']);
		$this->db->where('mid', $data['mid']);
		$this->db->delete('ar_location');
		return true;
	}
	function uploadFile($input_data){
		$this->db->insert('ar_user_doc', $input_data);
		$insert_id = $this->db->insert_id();

   return  $insert_id;
	}
	
	function updateUploadFiles($input_data){
		$data = array(  'cover_image' => $input_data['cover_image']   );
			$this->db->where('doc_id', $input_data['doc_id']);
			$this->db->update('ar_user_doc', $data); 
			if($this->db->affected_rows() >= 0) {
				
				$res = array('response'=>'success');
			   
			} else {
			   $res = array('response'=>'fail');
			   
			}
			 return $res; // your code
	}
	

	function freeUserFileCount($mid){
		$this->db->where('mid',$mid);
		$this->db->from('ar_user_doc');		
		$count= $this->db->count_all_results();		
		return $count;
		/*if($count>=5){
			return '1';			
		}else{
			return '0';
		}*/
	}  
	function getMoreAddress($data){
		$sql="SELECT `ar_location`.*, `countries`.`country_name` FROM ar_location JOIN `countries` ON `countries`.`id` = `ar_location`.`country` WHERE `ar_location`.`mid` = ".$data['mid'];
		$query = $this->db->query($sql);
		//echo $this->db->last_query();
		$res = $query->result_array();
		return $res;
	}
	function deleteAccount($data){		
		$this->db->where('mid',$data['mid']); 		
		$this->db->update('ar_members',$data);
		return true;
	}
	function deactivateAccount($data){
		$this->db->where('mid',$data['mid']); 		
		$this->db->update('ar_members',$data);
		return true;		
	}
	function feedbackReason($data){
		$this->db->where('mid',$data['mid']); 		
		$this->db->update('ar_members',$data);
		return true;
	}
	function memberName($mid){
		$this->db->select('fname');
		$this->db->select('lname');
		$this->db->from('ar_members');
		$this->db->where('mid',$mid);
		$query = $this->db->get();
		$res = $query->result_array();
		$name = $res[0]['fname']." ".$res[0]['lname'];
		return $name;
	}
	function manageHideFromViewMe($data){
		$this->db->where('mid', $data['mid']);
		$query = $this->db->delete('ar_settings'); 
		if($data['hide_me_prof_view'] == true){
			$this->db->insert('ar_settings',$data);
		}
		return true;
	}
	function confirmationAndDowngrade($update_data){
		$update_data['cancel_date'] = date('Y-m-d H:i:s');
		$this->db->insert('ar_cancel_subscription',$update_data);
		return true;
	}
	
	function getConnectedFriends($id,$limit){
		$this->db->where('mid',$id);
		$this->db->or_where('connected_id', $id); 
		$this->db->where('is_accepted','1');
		$this->db->limit($limit);
		$query = $this->db->get('connected');
		$friends=array();
		if ($query->num_rows() > 0){
			$rs = $query->result_array();
			
			foreach($rs as $value){
				if($value['mid']==$id){
					$friends[]=$value['connected_id'];
				}elseif($value['connected_id']==$id){
					$friends[]=$value['mid'];
				}
			}
			
			return $friends;
		}
	}

	function isAlredyDowngradeAccount($mid){
		$this->db->where('mid',$mid);
		$this->db->limit(1);
		$query = $this->db->get('ar_cancel_subscription');
		if ($query->num_rows() > 0){
			return 'TRUE';
		}else{
			return 'FALSE';
		}
	}
	function check_news_by_sub_sector($subSectorId)
	{
		$this->db->select('count(*) as c');
		$this->db->from('ar_apinews');
		$this->db->where('sub_sector_id',$subSectorId);

		$query = $this->db->get();
		//echo $this->db->last_query();die();
		$rs = $query->result_array();

		return $rs;
	}
	function check_news_by_sub_sector_distinctdates($subSectorId)
	{
		//$this->db->distinct(DISTINCT(accessid));
		$this->db->select('DISTINCT(on_date) as on_date');
		$this->db->from('ar_apinews');
		$this->db->where('sub_sector_id',$code);
		$this->db->limit(1, 0);
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		$rs = $query->row_array();

		return $rs;
	}

	function deleterecordsbylastdate($date,$code)
	{
		$this->db->where('on_date', $date);
		$this->db->where('sub_sector_id',$code);
		$query = $this->db->delete('ar_apinews'); 	
		return true;
	}


	

	function check_news_by_sub_sector_old($subSectorId)
	{
		$this->db->select('*');
		$this->db->from('ar_apinews');
		$this->db->where('sub_sector_id',$subSectorId);
		$this->db->order_by('on_date','DESC');
		$query = $this->db->get();
		$rs = $query->result_array();
		//echo $this->db->last_query();die();

		return $rs;
	}

	function check_news_by_sub_sector_lal($subSectorId,$start,$limit,$BlockedNewsIdForUser)
	{
		$this->db->select('*');
		$this->db->from('ar_apinews');
		if(count($BlockedNewsIdForUser)>0)
		{
		  $this->db->where_not_in('id',$BlockedNewsIdForUser); 
		}
		$this->db->where_in('sub_sector_id',$subSectorId);
		$this->db->group_by('url');
		$this->db->order_by('date','DESC');
		$this->db->limit($limit,$start);
		$query = $this->db->get();
		//echo $this->db->last_query(); 
		$rs = $query->result_array();
		//echo $this->db->last_query();die();

		return $rs;
	}
	
	
	function FnGetBlockedNewsIdsForUser($memberid)
	{
		$this->db->select('news_id');
		$this->db->from('news_block_from_user');
		$this->db->where('mid',$memberid);
		$query = $this->db->get();
	//	echo $this->db->last_query();die();
		$rs = $query->result_array();
		//echo $this->db->last_query();die();
		//print_r($rs);die();
		$blockedidarrs=array();
		if(count($rs)>0)
		{
		   foreach($rs as $blockedids)
		   {
			   $blockedidarrs[]=$blockedids['news_id'];
		   }
		}

		return $blockedidarrs;
	}




	function get_lastest_new_id($subSectorId)
	{
		$this->db->select('*');
		$this->db->from('ar_apinews');
		$this->db->where('sub_sector_id',$subSectorId);
		$this->db->order_by("on_date","asc");
		$this->db->limit(1, 0);
		$query = $this->db->get();
		$rs = $query->result_array();

		return $rs;
	}

	function checkNewsAvailable($dt,$code)
	{
		$this->db->select('*');
		$this->db->from('ar_apinews');
		$this->db->where('on_date',$dt);
		$this->db->where('sub_sector_id ',$code);
		$query = $this->db->get();

		if ($query->num_rows() > 0){
			return FALSE;
		}else{
			return TRUE;
		}
	}

	function getBusinessCodeNews($mid)
	{	//select distinct(SUBSTRING(mem_code,1,5)) as ans_set_cat_id from ar_member_data_wiz_one where mid = 22
		
		 $this->db->select('ans_set_cat_id');
		 $this->db->from('ar_member_data_wiz_one');
         $this->db->where('mid',$mid);
         $query = $this->db->get();
         $result = $query->result_array();
         return $result;

	}
	
	function getBusinessCode($mid)
	{	//select distinct(SUBSTRING(mem_code,1,5)) as ans_set_cat_id from ar_member_data_wiz_one where mid = 22
		
		$sql="select distinct(SUBSTRING(mem_code,1,5)) as ans_set_cat_id from ar_member_data_wiz_one where mid = ".$mid;
		$query = $this->db->query($sql);

		$result = $query->result_array();

		return $result;

	}
	
	
	function getTargetSubSector($mid){
		$this->db->select('*');
		$this->db->join('ar_target_sector', 'ar_target_sector.tar_sector_code = ar_wiz_two_looking_industry.target_sector_code', 'left');
		$this->db->from('ar_wiz_two_looking_industry');
		$this->db->where('ar_wiz_two_looking_industry.mid',$mid);
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	function fetchActiveSubSectorCode()
	{
		$this->db->distinct();
		$this->db->select('ans_set_cat_id');
		$this->db->from('ar_member_data_wiz_one');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}
	function FnCheckbusinessElibility($user_id,$suggestedid,$currentmonth)
	{
		$this->db->select('newsletter_check_id');
		$this->db->where('mid',$user_id);
		$this->db->where('suggestionid',$suggestedid);
		$this->db->where('sent_in_month',$currentmonth);
		$this->db->from('newsletter_business_check');
		$query = $this->db->get();
		$result = $query->num_rows();
		//echo $this->db->last_query();
		return $result;
	}
	
	function FnGetBusinessEmail($businessids)
	{
		$this->db->select('members.mid,members.email,det.rap_id,det.bussinessdesc');
		$this->db->join('ar_request_access_page as det','det.rap_id=members.mid','INNER');
		$this->db->where_in('members.mid',$businessids);
		$this->db->where('members.status','1');
		$this->db->from('ar_members as members');
		$query = $this->db->get();
		//echo $this->db->last_query();die();
		return $query->result_array();
	}
	
	function FnInsertEmail($emailsentarra)
	{
		 $this->db->insert('newsletter_business_check',$emailsentarra);
	}
	
	function UpdateUserDetails($mid,$memberdata,$accessdata)
	{
		$this->db->where('mid',$mid); 
		$this->db->update('ar_members',$memberdata);
		//echo $this->db->last_query();
		$this->db->where('mid',$mid); 
		$this->db->update('ar_request_access_page',$accessdata);
		//echo $this->db->last_query();
		return 1;
	}
	
 } //end