<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Model_suggestion extends CI_model
{
 
	function __construct(){

			parent::__construct();
			 
	}
function looking_for(){
	 $sql = "select * from ar_looking_for";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$data[]=$result;
				}
			}
	return	$data;
 }
	function suggestionSearchType($mid){
		$sql = "select * from ar_member_data_wiz_two where mid=".$mid." limit 0,1";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
		   $row = $query->row();
			$looking_for = explode(',',$row->looking_for);
			array_pop($looking_for);
			$searchtype=array();
			 foreach ($looking_for as $looking){
				  $sql="select * from ar_looking_for where looking_for_id=".$looking;
				  $query = $this->db->query($sql);
				  if ($query->num_rows() > 0){
						$row = $query->row();
						$searchtype[]=$row->search_type;
					}
					
			} 
			return $searchtype ;
		}
	 
	}
	
		function suggestionLocationType($mid){
			$sql = "select * from ar_member_data_wiz_two where mid=".$mid." limit 0,1";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
			   $row = $query->row();
				$locality =$row->locality;
				$locality=rtrim($locality, ",");
				return $locality;
			}
			
		}
		function memberCountryList($mid){
			$sql = "select ar_request_access_page.country as country_id from ar_request_access_page where mid=".$mid." limit 0,1";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
			   $country_id = (array)$query->row();
			}
			$sql = "select ar_location.country as country_id from ar_location where mid=".$mid;
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if(count($rs) > 0){
				foreach ($rs as $result){	
					$country_id[]=$result["country_id"];
				}
			}
			$countryId=array();
			foreach($country_id as $country){
				$countryId[]=$country;
			}
			return $countryId;
		}
		
		function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		  $theta = $lon1 - $lon2;
		  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		  $dist = acos($dist);
		  $dist = rad2deg($dist);
		  $miles = $dist * 60 * 1.1515;
		  $unit = strtoupper($unit);

		  if ($unit == "K") {
			return ($miles * 1.609344);
		  } else if ($unit == "N") {
			  return ($miles * 0.8684);
			} else {
				return $miles;
			  }
		}
		
		function latiOfUser($mid){
			
			$sql = "select 	Latitude from ar_request_access_page where mid=".$mid." limit 0,1";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
			   $Latitude = (array)$query->row();
			}
			
			return $Latitude["Latitude"] ;
		}
		function langOfUser($mid){
			
			$sql = "select 	Longitude from ar_request_access_page where mid=".$mid." limit 0,1";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
			   $Longitude = (array)$query->row();
			}
			return $Longitude['Longitude'] ;
		}
		
		
		
		
		
		function locationMatchbyUserId($sug_id,$mid){
			
			
			$myLocationList = $this->suggestionLocationType($mid);
			$userLocationList = $this->suggestionLocationType($sug_id);
			
			$lids = explode(',',$myLocationList);
			$ulids = explode(',',$userLocationList);
			//var_dump($lids);
			//echo '<br />';
			//var_dump($ulids);
			$locationMatch=array();
			foreach($lids as $lid){
				$ch = $lid;
				if($ch != ''){
					foreach($ulids as $ulid){
								if($ch==$ulid){
									if($ch=='4'){
										$locationMatch['4']='M';
									}elseif($ch=='3'){
										$logmember=$this->memberCountryList($mid);
										$usermember=$this->memberCountryList($sug_id);
										$result = array_intersect($logmember, $usermember);
										if(count($result)>0){
											$locationMatch['3']='M';
										}else{
											$locationMatch['3']='X';
										}										
									}elseif($ch=='2'){
										$logmember=$this->memberCountryList($mid);
										$usermember=$this->memberCountryList($sug_id);
										$result = array_intersect($logmember, $usermember);
										if(count($result)>0){
											$locationMatch['2']='M';
										}else{
											$lat1=$this->latiOfUser($mid);
											$lon1=$this->langOfUser($mid);
											$lat2=$this->latiOfUser($sug_id);
											$lon2=$this->langOfUser($sug_id);
											if($lat1=='' || $lon1=='' || $lat2=='' || $lon2==''){
												$distance=$this->distance($lat1, $lon1, $lat2, $lon2, "M");
												if((int)$distance <= 200){
													$locationMatch['2']='PM';
												}else{
													$locationMatch['2']='X';
												}
											}else{
												$locationMatch['2']='X';
											}
										}							
									}elseif($ch=='1'){
										$logmember=$this->memberCountryList($mid);
										$usermember=$this->memberCountryList($sug_id);
										$result = array_intersect($logmember, $usermember);
										if(count($result)>0){
											$lat1=$this->latiOfUser($mid);
											$lon1=$this->langOfUser($mid);
											$lat2=$this->latiOfUser($sug_id);
											if($lat1=='' || $lon1=='' || $lat2=='' || $lon2==''){
												$distance=$this->distance($lat1, $lon1, $lat2, $lon2, "M");
												if((int)$distance <= 50){
													$locationMatch['1']='M';
												}elseif((int)$distance <= 80 && (int)$distance >= 50){
													$locationMatch['1']='PM';
												}else{
													$locationMatch['1']='X';
												}
											}else{
												$locationMatch['1']='X';
											}
										}else{
											$locationMatch['1']='X';
										}							
										
									}
								}
							}						
							
							
					}
					
				}
				$lMatch='X';
				if (in_array("M", $locationMatch, true)) {
						$lMatch='M';
				}elseif(in_array("PM", $locationMatch, true)) {
					$lMatch='PM';
				}elseif(in_array("X", $locationMatch, true)) {
					$lMatch='X';
				}
			
			return $lMatch;
			
		}
	
		function suggestionMemberDetails($mid){
			$sql = "SELECT * FROM ar_members left join ar_request_access_page on ar_members.mid=ar_request_access_page.mid WHERE ar_members.mid='".$mid."'";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			$data=$rs[0];
			return $data;
		}

		function suggestionLocalLocality($mid){

			$member_arr=$this->suggestionMemberDetails($mid);
			$country=$member_arr['country'];
			$sql = "select ar_request_access_page.mid as mid from ar_request_access_page left join ar_members on ar_request_access_page.mid = ar_members.mid where ar_request_access_page.country =".$country." and ar_members.status='1' and ar_request_access_page.mid<>".$mid;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
			$rs =  $query->result_array();
			return $rs[0];
			}
		}
	
		function deleteMidFromSuggestion($mid=''){
			if($mid!=''){
				$this->db->where('mid', $mid);
				$this->db->delete('ar_suggestion'); 
				return true;
			}else{
				return false;
			}
		}
		function deleteMidFromNetwork($mid='',$searchtype){
			if($mid!=''){
				$this->db->where('mid', $mid);
				$this->db->where('searchtype', $searchtype);
				$this->db->delete('ar_network'); 
				return true;
			}else{
				return false;
			}
		}
		function suggestionGlobal($mid){
			$sql = "select ar_request_access_page.mid as mid from ar_request_access_page  left join ar_members on ar_request_access_page.mid = ar_members.mid where ar_members.status='1' and ar_request_access_page.mid<>".$mid;
			$query = $this->db->query($sql);
			$rs =  $query->result_array();
			return $rs;
		}
		
		function suggestionPossibleTwentyDigitCode($code1,$code2){
			
			$sql_first="select * from ar_wizone_wiztwo_rel where wiztwo_code='".$code1.$code2."'";
			$query_first = $this->db->query($sql_first);
			if ($query_first->num_rows() > 0){
				$rs_first =  $query_first->result_array();
				
				foreach($rs_first as $value){
					foreach( $value as $k=>$v){
						if($k=='wizone_code'){
							$top_code[]=$v;
						}
					}
				}
				return $top_code;
			}
			
		}
		
	function allMemberList($country_id=null){
			
		$sql = "select ar_members.mid as mid from ar_members left join ar_request_access_page on ar_request_access_page.mid = ar_members.mid where ar_members.status='1'";

		if($country_id!=''){
		$sql .= " and ar_request_access_page.country =".$country_id;
		}

		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$rs =  $query->result_array();
			return $rs;
		}
			
	}

	function isAlreadyDataInWizTwo($mid){
			$sql = "SELECT * FROM ar_member_data_wiz_two WHERE mid='".$mid."'";
			$query = $this->db->query($sql);
			$rs = $query->result_array();
			if ($query->num_rows() > 0){
					return 'true';
				} else{
				return 'false';
			}
	}

	function gettendigitcodeofmember($mid){
			
			$sql = "select distinct(CONCAT(industry_code, target_sector_code)) as memcodetwo,target_sector_code,industry_code,ar_members.mid as mid from ar_wiz_two_looking_industry left join ar_members on ar_wiz_two_looking_industry.mid = ar_members.mid ";
			
			$sql .= " where ar_wiz_two_looking_industry.mid=".$mid." and ar_members.status='1'";
            
           //echo $sql;
             //die();
			$query = $this->db->query($sql);
			$rs =  $query->result_array();
			return $rs;
			
	}

	 function suggestiontwentydigitmatch($mid,$top_code,$sector_code){
	 	$rs_sec=0;
			$sql="select distinct(mid), mem_code from ar_member_data_wiz_one where mid <>".$mid." and (mem_code like '".$top_code.$sector_code."%' or  mem_code like '".$top_code."%' or  mem_code like '%".$sector_code."%')"; 
			$query_sec = $this->db->query($sql);
			if ($query_sec->num_rows() > 0){
				$rs_sec =  $query_sec->result_array();
			}
			return $rs_sec;
		}

	function insertTypeTwo($id,$arr,$type){					 

		$suggestedIds = implode(',',$arr);
		if($suggestedIds!=''){
			$data = array('mid' => $id, 'suggested_ids' => $suggestedIds,'searchtype'=>$type);
			$chk = $this->isDataPresentinarSuggestion($id,$type);
			if($chk == 'true'){
				$this->db->where('mid',$id);
				$this->db->where('searchtype',$type);
				$this->db->update('ar_suggestion',$data); 
				return $id;
			}else{
				$this->db->insert('ar_suggestion',$data);
				return $this->db->insert_id();
			} 
		}	
	}
	
	function insertTypeTwoNetwork($id,$arr,$type){		//for network			 

		$suggestedIds = implode(',',$arr);
		if($suggestedIds!=''){
			$data = array('mid' => $id, 'suggested_ids' => $suggestedIds,'searchtype'=>$type);
			$chk = $this->isDataPresentinarNetwork($id,$type);
			if($chk == 'true'){
				$this->db->where('mid',$id);
				$this->db->where('searchtype',$type);
				$this->db->update('ar_network',$data); 
				return $id;
			}else{
				$this->db->insert('ar_network',$data);
				return $this->db->insert_id();
			} 
		}	
	}

	function gettwentydigitcodeofmember($mid){
		$sql = "select distinct(mem_code) as mem_code,ans_set_cat_id,ar_members.mid as mid from ar_member_data_wiz_one left join ar_members on ar_member_data_wiz_one.mid = ar_members.mid ";
			
			$sql .= " where ar_member_data_wiz_one.mid=".$mid." and ar_members.status='1'";
			
			$query = $this->db->query($sql);
			$rs =  $query->result_array();
			return $rs;
		
	}
	function isDataPresentinarSuggestion($mid,$type){
		$sql = "SELECT * FROM ar_suggestion WHERE mid=".$mid." and searchtype='".$type."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
				return 'true';
			} else{
			return 'false';
		}		
	}
	function isDataPresentinarNetwork($mid,$type){ //for network
		$sql = "SELECT * FROM ar_network WHERE mid=".$mid." and searchtype='".$type."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
				return 'true';
			} else{
			return 'false';
		}		
	}

 	function suggestiontendigitmatch($mid,$industry_code,$target_sector_code){
 		$rs_sec = 0;
			$sql="SELECT distinct(CONCAT(industry_code,target_sector_code)) as memcodetwo, mid,target_sector_code,industry_code from ar_wiz_two_looking_industry where mid <>".$mid." and (CONCAT(industry_code,target_sector_code) like '".$industry_code.$target_sector_code."' or  industry_code like '".$industry_code."' or  target_sector_code like '".$target_sector_code."')";
			$query_sec = $this->db->query($sql);
			if ($query_sec->num_rows() > 0){
				$rs_sec =  $query_sec->result_array();
				return $rs_sec;
			}
			
	}

	function getnonconsumertype($mid){
		$sql = "SELECT looking_service FROM ar_member_data_wiz_two WHERE mid='".$mid."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$rs = $query->result_array();
			if($rs[0]['looking_service']!=''){
				return 'true';
			} else{
			return 'false';
			}}else{
			return 'false';
		}		
	}

	function getlookingservice($mid){
		$sql = "SELECT looking_service FROM ar_member_data_wiz_two WHERE mid='".$mid."'";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
			$rs = $query->result_array();
				return $rs[0]['looking_service'];
			} 
	}

	function suggestionCountbyCustomer($mid){

		$sql = "SELECT * FROM ar_suggestion WHERE mid=".$mid;
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0){
				$rs = $query->result_array();
				$suggested_id=array();
				foreach($rs as $val){					
				$suggested_id[$val['searchtype']]=$val['suggested_ids']	;					
				}
				return $suggested_id;				
			} 			
	}
	function blockFromSuggestion($data){
		$this->db->insert('ar_blocked', $data);
		return true;
	}
	function suggestionBlocked($mid){
		$sql = "SELECT * FROM ar_blocked WHERE mid=".$mid;
		//echo $sql; exit;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
				$rs = $query->result_array();
				$blocked_id=array();
				foreach($rs as $val){
					
				$blocked_id[]=$val['blocked_mid']	;
					
				}
				return $blocked_id;
				
			} 
		
	}

	function suggestionLookingforName($mid,$search_type){
			$sql = "select * from ar_member_data_wiz_two where mid=".$mid." limit 0,1";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
			   $row = $query->row();
				$looking_for = explode(',',$row->looking_for);
				array_pop($looking_for);
				$searchtype=array();
				 foreach ($looking_for as $looking){
					  $sql="select * from ar_looking_for where looking_for_id=".$looking." and search_type=".$search_type;
					  $query = $this->db->query($sql);
					  if ($query->num_rows() > 0){
							$row = $query->row();
							$searchtype[]=$row->looking_for_name;
						}
						
				} 
			return $searchtype ;
		}
	 
	}

	function searchconnection($lookingfor,$mid){
		$suggested_ids='';
		foreach($lookingfor as $looking){
			$looking=explode('-',$looking);
			
			if($looking[1] == "1" || $looking[1] == "4" || $looking[1] == "2"){
					$searchOptions= "1";
				}
				else{
					$searchOptions= $looking[1];
				}
			
			$sql="select * from ar_suggestion where mid=".$mid." and searchtype='".$searchOptions."'";
			 $query = $this->db->query($sql);
			 if ($query->num_rows() > 0){
							$row = $query->row();
							$suggested_ids .=$row->suggested_ids.',';
			}
			
		
		}
		if($suggested_ids!=''){
			$suggested_ids=trim($suggested_ids,',');
			$suggestedId=array();
			$suggestedId=explode(',',$suggested_ids);
		
			return array_unique($suggestedId);
		}else{
			return '0';
			
		}
	}

	function is_connected($mid,$con_id){
		if($mid != null && $con_id != null){
			$isConnected =0;
			$sql="select * from connected where (mid=".$mid." and connected_id=".$con_id.") or (mid=".$con_id." and connected_id=".$mid.")";
			//var_dump($sql);
			$query = $this->db->query($sql);
			//var_dump($query->num_rows());
			 if ($query->num_rows() != 0){
				 $row = $query->row();
				$is_accepted =$row->is_accepted;
				//var_dump($is_accepted);exit;
				if($is_accepted=='1'){
					$isConnected =1;
					
				}elseif($is_accepted=='0'){
					$isConnected =2;
					
				}				
			}else{
				$isConnected =0;
			}
			return $isConnected;
		}
		return false;
	}
	function get_adsearch_connection($mid,$con_id){		
		if($mid != null && $con_id != null){
			$isConnected ='';
			$sql="select * from connected where (mid=".$mid." and connected_id=".$con_id.") or (mid=".$con_id." and connected_id=".$mid.")";			
			$query = $this->db->query($sql);
			$result= $query->row();
			//print_r($result); exit;
			if ($query->num_rows() > 0){
				//echo $result->is_accepted; exit;
				if($result->is_accepted == 1){
					$isConnected ='true';
				}else{
					$isConnected ='pending';
				}
			}else{
				$isConnected ='false';
			}
			return $isConnected;
		}
		return false;
	}
	function acceptRequestForConnection($mid,$connected_id){
		
		$data['is_accepted'] = '1';
		$data['is_seen'] = '1';
		$data['connected_id'] = $connected_id;
		$data['mid'] = $mid;
		

		//$this->db->insert('connected', $data);
		//==== Deleting the member ids that are connected to this mid from ar_suggestion.
		$sql = "SELECT sug_id,suggested_ids FROM ar_suggestion WHERE mid=".$mid;
		$query = $this->db->query($sql);

		$ar_ids = array();
		if ($query->num_rows() > 0){
			$rs = $query->result_array();
			
			foreach($rs as $result){
				$up_id = $result['sug_id'];
				$st_id = $result['suggested_ids'];
				$ar_ids = explode(',',$st_id);

				if(($key = array_search($connected_id, $ar_ids)) !== false) {
    				unset($ar_ids[$key]);
				}
				$str = implode(',', $ar_ids);

			$d['suggested_ids'] = $str;
			$this->db->where('sug_id',$up_id);
			$this->db->update('ar_suggestion',$d);
			}
		}
		$this->db->insert('connected', $data);
		return "true";
		
	}
	function acceptRequestForConnectionNew($mid,$connected_id){
		
		$data['is_accepted'] = '1';
		$data['is_seen'] = '1';
		$data['connected_id'] = $connected_id;
		$data['mid'] = $mid;
		

		//$this->db->insert('connected', $data);
		//==== Deleting the member ids that are connected to this mid from ar_suggestion.
		$sql = "SELECT * FROM connected WHERE (mid=".$mid." and connected_id=".$connected_id.") OR (mid=".$connected_id." and connected_id=".$mid.")";
		$query = $this->db->query($sql);

		$ar_ids = array();
		if ($query->num_rows() > 0){
			$result = $query->row();
			//print_r($rs); exit;
			//foreach($rs as $result){
			$mid = $result->mid;
			$connected_id = $result->connected_id; 				
			$d['is_accepted'] = '1';
			$this->db->where('mid',$mid);
			$this->db->where('connected_id',$connected_id);
			$this->db->update('connected',$d);
			//}
		}else{
			$this->db->insert('connected', $data);
		}		
		return "true";		
	}

	function deleteRequestForConnection($mid,$con_id){
		$sql="delete from connected where (mid=".$mid." and connected_id=".$con_id.") or (mid=".$con_id." and connected_id=".$mid.")";
		$query = $this->db->query($sql);
		return 'true';
	}

	function getMemberIdsFromConnected($id){

		$this->db->select('connected_id');
		$this->db->where('is_accepted','1');
		$this->db->where('mid',$id);
		$query = $this->db->get('connected');
		$memIds = array();
		
		if ($query->num_rows() > 0){
			$rs = $query->result_array();
			foreach ($rs as $key => $value) {
				$memIds[] = $value['connected_id'];
			}
		}
		return $memIds;
	}
/*	
	function filterSuggestionLookingFor($mids, $searchtypebyuserarray){
		
		//$mids=explode(',',$mids);
		$uniqueMids = array();
		if(!empty($mids)&& count($mids)>0){
			$searchtype=array();
			foreach($mids as $mid){
					$sql="select * from ar_member_data_wiz_two where mid=".$mid;
					$query = $this->db->query($sql);
					 if ($query->num_rows() > 0){
						 $row = $query->row();
							$searchtype[$row->mid]=trim($row->looking_for,',');
					 }
			}
			
			$searchType2D=array();
			foreach($searchtype as $searchK=>$searchV){
				
				$searchType2D[$searchK]=explode(',',$searchV);
				
			}

			//var_dump($searchtypebyuserarray); die();
			
			foreach($mids as $mid){
				
				foreach($searchtypebyuserarray as $searchUserArray){

					$arr = $searchType2D[$mid];
					//var_dump($arr); 
					//echo $searchUserArray;
					//echo '=========';
					$chk = array_search($searchUserArray,$arr);
					if($chk !== FALSE){
						$uniqueMids[] = $mid;
					}
				}
			}
			//die();
			//echo '<pre>'; var_dump($uniqueMids);
			$uniqueMids = array_unique($uniqueMids);
			return $uniqueMids;
		}
	}
*/

 function filterSuggestionLookingFor($searchtypebyuserarray,$type){
	
	
     if(strtolower($type) == 'buisness'){
       $type = 'B';  
     } else{
         $type = 'SEP';
     }
     
   $searchtypetargetarr=array();
   foreach($searchtypebyuserarray as $searchtypebyuserarr){
    $sql="select * from ar_looking_for_rel where  looking_for_id=".$searchtypebyuserarr." and user_own_type='".$type."'";
    //echo '<br/>';
    $query = $this->db->query($sql);
    if ($query->num_rows() > 0){
        $row = $query->result_array();
        foreach($row as $value){
          $searchtypetargetarr[]=$value['target_looking_for_id'];
      }
    }
   }
   
   /* if($type == 'B'){
    foreach($searchtypebyuserarray as $searchtypebyuserarr){
    $sql="select * from ar_looking_for_rel where  looking_for_id=".$searchtypebyuserarr." and user_own_type='B'";
    // echo '<br>';
	 
    $query = $this->db->query($sql);
    if ($query->num_rows() > 0){
        $row = $query->result_array();
        foreach($row as $value){
          $searchtypetargetarr[]=$value['target_looking_for_id'];
      }
    }
   }
   }
   if($type == 'SEP'){
   	//echo 'HI';
    foreach($searchtypebyuserarray as $searchtypebyuserarr){
    $sql="select * from ar_looking_for_rel where  looking_for_id=".$searchtypebyuserarr." and user_own_type='SEP'";
    //echo '<br/>';
    $query = $this->db->query($sql);
    if ($query->num_rows() > 0){
        $row = $query->result_array();
        foreach($row as $value){
          $searchtypetargetarr[]=$value['target_looking_for_id'];
      }
    }
   }
   } */
   
   $searchtypetargetarr=array_unique($searchtypetargetarr);
  
  $uniqueMids=array();
    foreach($searchtypetargetarr as $searchUserStr){
     //echo $searchUserStr;
    //$searchUserArray=implode(',',$searchtypebyuserarray);
    $sql="select * from ar_member_data_wiz_two where looking_for REGEXP ".$searchUserStr;
  	//echo $sql;
    $query = $this->db->query($sql);
       if ($query->num_rows() > 0){
        $row = $query->result_array();
        foreach($row as $value){
       $uniqueMids[]=$value['mid'];
      }
       
     }
    }
  
   $uniqueMids = array_unique($uniqueMids);
    //var_dump($uniqueMids);die();  
  return $uniqueMids;
 }
 
 function userProfType($mid){
	 if($mid!=''){
  $sql='select bussinesstype from ar_request_access_page where mid='.$mid;
  $query = $this->db->query($sql);
  if ($query->num_rows() > 0){
    $row = $query->row();
    return $row->bussinesstype; 
  }
	 }
 }

function suggestiontwentydigitmatchScore($suggested_ids,$full_code,$fstten_code){
	 	$rs_sec=0;
		//$suggested_id=implode(',',$suggested_ids);
		$fstten_code = substr($fstten_code,0,5);
		$matchs='X';
		if($fstten_code!=''){
			$sql="select distinct(mid), mem_code from ar_member_data_wiz_one where mid = ".$suggested_ids." and (mem_code like '".$fstten_code."%')"; 
			
			$query_sec = $this->db->query($sql);
			if ($query_sec->num_rows() > 0){
				$rs_sec =  $query_sec->result_array();
					foreach($rs_sec as $value){
						$matchs ='PM';
					}
			}
		}
			if($full_code!=''){
				$sql="select distinct(mid), mem_code from ar_member_data_wiz_one where  mid = ".$suggested_ids." and (mem_code = '".$full_code."')";
				$query_sec = $this->db->query($sql);
				if ($query_sec->num_rows() > 0){
					$rs_sec =  $query_sec->result_array();
					foreach($rs_sec as $value){
							$matchs ='M';
						}
				}
			}
			return $matchs;
		
		}

function match1Score($match1_own_vs_targer,$match1_location){
	$sql="select * from match1_root_score where match1_own_vs_targer='".$match1_own_vs_targer."' and match1_location='".$match1_location."'";
	$query = $this->db->query($sql);
					 if ($query->num_rows() > 0){
						 $row = $query->row();	
						return $row->match1_score;
					}
					
			
}
function match3Score($match3_own,$match3_location,$match3_target){
	$sql="select * from match3_root_score where match3_own='".$match3_own."' and match3_location='".$match3_location."' and match3_target='".$match3_target."'";
	$query = $this->db->query($sql);
					 if ($query->num_rows() > 0){
						 $row = $query->row();	
						return $row->match3_score;
					}
					
			
}
function match5Score($match5_own,$match5_location,$match5_target,$match5_service){
	$sql="select * from match5_root_score where match5_own='".$match5_own."' and match5_location='".$match5_location."' and match5_target='".$match5_target."' and match5_service='".$match5_service."'";
	$query = $this->db->query($sql);
					 if ($query->num_rows() > 0){
						 $row = $query->row();	
						return $row->match5_score;
					}
					
			
}
	function isDataPresentinWiztwo($mid){
		$sql = "SELECT * FROM ar_member_data_wiz_two WHERE mid=".$mid;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
				return 'true';
			} else{
			return 'false';
		}		
	}
	function match1UserScore($sug_id,$mid,$suggested_id,$match1Score,$match1_location){

		//$match1_location='M';
		$res_score=$this->match1Score($match1Score,$match1_location);
		$data['sug_id'] = $sug_id;
		$data['mid'] = $mid;
		$data['suggested_id'] = $suggested_id;
		$data['score'] = $res_score;
		$data['score_pattern'] = $match1Score.'-'.$match1_location;
		$this->db->insert('ar_score', $data);
		return "true";
		
	}	
	function match1UserScoreNetwork($net_id,$mid,$suggested_id,$match1Score,$match1_location){

		//$match1_location='M';
		$res_score=$this->match1Score($match1Score,$match1_location);
		$data['net_id'] = $net_id;
		$data['mid'] = $mid;
		$data['suggested_id'] = $suggested_id;
		$data['score'] = $res_score;
		$data['score_pattern'] = $match1Score.'-'.$match1_location;
		$this->db->insert('ar_networkscore', $data);
		return "true";
		
	}

	function suggestiontendigitmatchScore($suggested_ids,$industry_code,$target_sector_code){
	 	$rs_sec=0;
		//$suggested_id=implode(',',$suggested_ids);
		$matchs='X';
		
			$sql="SELECT distinct(CONCAT(industry_code,target_sector_code)) as memcodetwo, mid,target_sector_code,industry_code from ar_wiz_two_looking_industry where mid = ".$suggested_ids." and  (industry_code like '".$industry_code."' or  target_sector_code like '".$target_sector_code."')";
			$query_sec = $this->db->query($sql);
			if ($query_sec->num_rows() > 0){
				$rs_sec =  $query_sec->result_array();
					foreach($rs_sec as $value){
						$matchs ='PM';
					}
			}
	
				$sql="SELECT distinct(CONCAT(industry_code,target_sector_code)) as memcodetwo, mid,target_sector_code,industry_code from ar_wiz_two_looking_industry where mid = ".$suggested_ids." and  (CONCAT(industry_code,target_sector_code) ='".$industry_code.$target_sector_code."')";
				$query_sec = $this->db->query($sql);
				if ($query_sec->num_rows() > 0){
					$rs_sec =  $query_sec->result_array();
					foreach($rs_sec as $value){
							$matchs ='M';
						}
				}
			
			return $matchs;
		
		}

		function match3UserScore($sug_id,$mid,$suggested_id,$match1Score){

		$match1Score=explode('-',$match1Score);
		$match3_own=$match1Score[0];
		$match3_location=$match1Score[1];
		$match3_target=$match1Score[2];
		$res_score=$this->match3Score($match3_own,$match3_location,$match3_target);

		$data['sug_id'] = $sug_id;
		$data['mid'] = $mid;
		$data['suggested_id'] = $suggested_id;
		$data['score'] = $res_score;
		$data['score_pattern'] = $match3_own.'-'.$match3_location.'-'.$match3_target;
		
		$this->db->insert('ar_score', $data);


		return "true";
	}

	function getlookingservicemid($looking_ids,$suggested_ids){
		$matchs ='X';
		foreach($looking_ids as $looking_id){
		$sql="SELECT mid FROM ar_member_data_wiz_two where mid = ".$suggested_ids." and  looking_service like  '%".$looking_id."%'";
			$query_sec = $this->db->query($sql);
			if ($query_sec->num_rows() > 0){
				$rs_sec =  $query_sec->result_array();
					foreach($rs_sec as $value){
						$matchs ='M';
					}
			}
		}
		return $matchs;
	}

	function match5UserScore($sug_id,$mid,$suggested_id,$match1Score){

		$match1Score=explode('-',$match1Score);
		$match5_own=$match1Score[0];
		$match5_location=$match1Score[1];
		$match5_target=$match1Score[2];
		$match5_service=$match1Score[3];
		$res_score=$this->match5Score($match5_own,$match5_location,$match5_target,$match5_service);
		$data['sug_id'] = $sug_id;
		$data['mid'] = $mid;
		$data['suggested_id'] = $suggested_id;
		$data['score'] = $res_score;
		$data['score_pattern'] = $match5_own.'-'.$match5_location.'-'.$match5_target.'-'.$match5_service;

		$this->db->insert('ar_score', $data);
		return "true";
	}

	function showSearchResult($sug_id){
		if($sug_id!=''){
		$sql='select * from ar_score where suggested_id!= mid and sug_id='.$sug_id.' and score <> 0 order by score desc';
		$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->result_array();
					
							
			}
			return $row;
		}
	}

function showOnDashboard($mid){
		$dashboard_res=array();
		$sql='select * from ar_suggestion as sugg left join ar_score as sc on sugg.sug_id=sc.sug_id where sugg.mid='.$mid.' and sc.suggested_id REGEXP sugg.suggested_ids and sc.suggested_id!= sc.mid and sc.score <> 0 order by sc.score desc'; 
		echo $sql;
		$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->result_array();			
			}
			return $row;
	}
	function showOnDashboards($mid){
		
		$sql='select * from ar_suggestion as sugg left join ar_score as sc on sugg.sug_id=sc.sug_id where sugg.mid='.$mid.' and sc.suggested_id!= sc.mid and sc.score <> 0 group by sc.suggested_id order by sc.score desc'; 
		$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->result_array();			
			}
			return $row;
	}
	
	function showOnNetworks($mid,$level){
		
		$sql='select * from ar_network as nt left join ar_networkscore as nsc on nt.net_id=nsc.net_id where nt.mid='.$mid.' and nsc.suggested_id!= nsc.mid and nsc.score <> 0 and searchtype="'.$level.'" group by nsc.suggested_id order by nsc.score desc'; 
		//var_dump($sql);
		//echo '<br />';
		$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->result_array();			
			}
			return $row;
	}

function checkToShowUserInSearchResult($looking_for_search_id,$user_own_type){
	//echo $user_own_type;
	//echo '<br/>';
 if(strtolower($user_own_type) == 'buisness'){
       $user_own_type = 'B';  
     } else{
         $user_own_type = 'SEP';
     }
     //echo $user_own_type;
     //echo '<br/>';
	if($looking_for_search_id !='' && $user_own_type!=''){
		$arr = array();
		$sql = "select * from ar_looking_for_rel where looking_for_id=".$looking_for_search_id." and user_own_type ='".$user_own_type."'";
		//echo '<br/>';
		$query = $this->db->query($sql);

			if ($query->num_rows() > 0){
				 $row = $query->result_array();

				foreach($row as $r){
				$arr[] = $r['target_user_type'];
				}		
			}

			$ct = count($arr);

			if($ct == 2){
				return 'BOTH';
			}else if($ct == 0){
				return "No Result";
			} 
			else{
				return $arr[0];
			}
	}
}

function updateSuggestion($str,$mid){

		if($str!='' && $mid!=''){
			$d['suggested_ids'] = $str;
			$this->db->where('mid',(int)$mid);
			$this->db->update('ar_suggestion',$d);

			$this->db->last_query();
		}
		
		
	}

function suggestionPossibleTenDigitCode($code){
			
			$sql_first="select * from ar_wizone_wiztwo_rel where wizone_code='".$code."'";
			$query_first = $this->db->query($sql_first);
			if ($query_first->num_rows() > 0){
				$rs_first =  $query_first->result_array();
				
				foreach($rs_first as $value){
					foreach( $value as $k=>$v){
						if($k=='wiztwo_code'){
							$top_code[]=$v;
						}
					}
				}
				return $top_code;
			}
			
		}

function deletedUnwantedIds(){
	$sql = "delete from ar_score where ar_score.sug_id not in (select ar_suggestion.sug_id from ar_suggestion)";

	$this->db->query($sql);
}
function deletedUnwantednetIds(){// for network
	$sql = "delete from ar_networkscore where ar_networkscore.net_id not in (select ar_network.net_id from ar_network)";

	$this->db->query($sql);
}
function suggestionSearchTypes($mid){
		$sql = "select * from ar_member_data_wiz_two where mid=".$mid." limit 0,1";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0){
		   $row = $query->row();
			$looking_for = explode(',',$row->looking_for);
			array_pop($looking_for);
			$searchtype=array();
			 foreach ($looking_for as $looking){
				  $sql="select * from ar_looking_for where looking_for_id=".$looking;
				  $query = $this->db->query($sql);
				  if ($query->num_rows() > 0){
						$row = $query->row();
						$searchtype[]=$row->looking_for_id.'-'.$row->search_type;
					}
					
			} 
			//print_r($searchtype);die();
			return $searchtype ;
		}
	 
	}

	function getLookingForById($id){

		$sql = "SELECT * FROM ar_looking_for WHERE looking_for_id=".$id;
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result[0]['looking_for_name'];
	}
	function getLookingForByIdtoType($id){

		$sql = "SELECT * FROM ar_looking_for WHERE looking_for_id=".$id;
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result[0]['search_type'];
	}
	function getLookingForByType($id){

		$sql = "SELECT * FROM ar_looking_for WHERE search_type='".$id."'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result[0]['looking_for_name'];
	}

	function getSuggestionByType($id,$stype){

		$this->db->select('*');
		$this->db->from('ar_suggestion');
		$this->db->where('searchtype',$stype);
		$this->db->where('mid',(int)$id);

		$query = $this->db->get();
		//echo $this->db->last_query();
		$res = $query->result_array();
		return $res;
	}
	function isScoreNotZero($id,$mid){
		if($id!=0){
		$sql='select * from ar_suggestion as sugg left join ar_score as sc on sugg.sug_id=sc.sug_id where sugg.mid='.$mid.' and sc.suggested_id = '.$id.' group by sc.suggested_id order by sc.score desc'; 
		$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->row();			
			}
			//var_dump($row->score);
			if($row->score!=0){
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
			//return $row;
			
	}
	
	function matchScore($id,$mid,$lookingForId){
		$lookingtype=$this->getLookingForByIdtoType($lookingForId);
		$sql='select * from ar_suggestion as sugg left join ar_score as sc on sugg.sug_id=sc.sug_id where sugg.mid='.$mid.' and searchtype="'.$lookingtype.'" and sc.suggested_id = '.$id.' group by sc.suggested_id order by sc.score desc'; 
		$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->row();
				return $row->score;
			}
		}
		function matchScorePat($id,$mid,$lookingForId){
			$lookingtype=$this->getLookingForByIdtoType($lookingForId);
		$sql='select * from ar_suggestion as sugg left join ar_score as sc on sugg.sug_id=sc.sug_id where sugg.mid='.$mid.' and searchtype="'.$lookingtype.'"  and sc.suggested_id = '.$id.' group by sc.suggested_id order by sc.score desc'; 
		$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->row();
				 if($row->score_pattern!=''){
					 
					 return $row->score_pattern;
				 }
				
			}
		}
		
		function suggestionTypeFromId($mid,$sid){
			$sql='select * from ar_suggestion as sugg left join ar_score as sc on sugg.sug_id=sc.sug_id where sugg.mid='.$mid.' and sc.suggested_id = '.$sid.' group by sc.suggested_id order by sc.score desc'; 
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->row();
				return $row->searchtype;
			}
			
		}
		function matchScoretextNew($lookingForId,$score,$pattern){
			$pattern=explode('-',$pattern);
			//var_dump($pattern);
			if($lookingForId==1){
			$sql1="select * from match5_root_score where match5_score=".$score." and match5_own = '".$pattern[0]."' and match5_location='".$pattern[1]."' and match5_target='".$pattern[2]."' and match5_service='".$pattern[3]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text;
				}	
			}elseif($lookingForId==2){
			$sql1="select * from match5_root_score where match5_score=".$score." and match5_own = '".$pattern[0]."' and match5_location='".$pattern[1]."' and match5_target='".$pattern[2]."' and match5_service='".$pattern[3]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text;
				}	
			}elseif($lookingForId==3){
			$sql1="select * from match1_root_score where match1_score=".$score." and match1_own_vs_targer = '".$pattern[0]."' and match1_location='".$pattern[1]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text_supplier;
				}	
			}elseif($lookingForId==4){
			$sql1="select * from match1_root_score where match1_score=".$score." and match1_own_vs_targer = '".$pattern[0]."' and match1_location='".$pattern[1]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text_customer ;
				}	
			}elseif($lookingForId==5){
			$sql1="select * from match1_root_score where match1_score=".$score." and match1_own_vs_targer = '".$pattern[0]."' and match1_location='".$pattern[1]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text_investment;
				}	
			}elseif($lookingForId==6){
			$sql1="select * from match1_root_score where match1_score=".$score." and match1_own_vs_targer = '".$pattern[0]."' and match1_location='".$pattern[1]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text_investment;
				}	
			}elseif($lookingForId==7){
			$sql1="select * from match3_root_score where match3_score=".$score." and match3_own ='".$pattern[0]."' and match3_location='".$pattern[1]."' and match3_target='".$pattern[2]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text;
				}	
			}elseif($lookingForId==8){
			$sql1="select * from match3_root_score where match3_score=".$score." and match3_own ='".$pattern[0]."' and match3_location='".$pattern[1]."' and match3_target='".$pattern[2]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text;
				}	
			}elseif($lookingForId==9){
			$sql1="select * from match3_root_score where match3_score=".$score." and match3_own ='".$pattern[0]."' and match3_location='".$pattern[1]."' and match3_target='".$pattern[2]."'";
			$query1 = $this->db->query($sql1);
			if ($query1->num_rows() > 0){
				 $row1 = $query1->row();
					$text=$row1->matching_text;
				}	
			}
			return $text;
		}
		function Feedbackdetails($mid,$sid){
			$sql='select * from ar_feedback_details where mid='.$mid.' and sid = '.$sid; 
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0){
				 $row = $query->row();
				return $row->is_relivance;
			}
			
		}
		function configRelevant($data){
			//var_dump($data);
			$mid=$data['mid'];
			$sid=$data['suggested_id'];
			$rel_type=$data['rel_type'];
			$d=array(
				'mid'=> $mid,
				'sid'=> $sid,
				'is_relivance' => $rel_type
			);
			$is_exist=$this->Feedbackdetails($mid,$sid);
			if($is_exist != ''){
				$this->db->where('mid',$mid);
				$this->db->where('sid',$sid);
				$this->db->update('ar_feedback_details',$d); 
				return true;
			}else{
				$this->db->insert('ar_feedback_details',$d);
				$this->db->insert_id();
				return true;
			} 
			
		}

		function getTwentyDigitCodeFromTenDigit($ten_digit_code){

			$this->db->select('wizone_code');
			$this->db->where('wiztwo_code',$ten_digit_code);
			$this->db->from('ar_wizone_wiztwo_rel');
			$query = $this->db->get();
			$result = $query->result_array();
			return $result;
		}
 } //end
