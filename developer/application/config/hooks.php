<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
*/



/* End of file hooks.php */
/* Location: ./application/config/hooks.php */
$hook['post_controller_constructor'][] = array(
		'class'    => 'Menu_Helper',
		'function' => 'init',
		'filename' => 'menu_helper.php',
		'filepath' => 'libraries',
		'params'   => array()
	);

$hook['post_controller_constructor'][] = array(
		'class'    => 'User_Manager',
		'function' => 'init',
		'filename' => 'user_manager.php',
		'filepath' => 'libraries',
		'params'   => array()
	);
	

$hook['display_override'] = array(
		'class'    => 'Layout_Manager',
		'function' => 'render_layout',
		'filename' => 'layout_manager.php',
		'filepath' => 'libraries',
		'params'   => array()
	);