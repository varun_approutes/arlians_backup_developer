<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Menu_Helper {

    public static $menu_items;
    public static $current_menu_item;
    private static $current_url;
    public static $breadcrumb = null;

    function init() {
        self::$current_url = $_SERVER['REQUEST_URI'];

        $CI = get_instance();

        $CI->load->helper("url");

        $menu_config_dir = FCPATH . APPPATH . "config" . DS . "menu_config";
        if ($handle = opendir($menu_config_dir)) {
            while (false !== ($entry = readdir($handle))) {
                $ext = pathinfo($entry, PATHINFO_EXTENSION);
                $basename = str_replace("." . $ext, "", $entry);
                if ($ext != "xml")
                    continue;
                $root_element = new SimpleXMLElement(file_get_contents($menu_config_dir . DS . $entry));
                self::$menu_items[$basename] = new stdClass();

                $this->fill_items(self::$menu_items[$basename], $root_element);
            }
        }
    }

//end init

    function fill_items(&$parent, &$items) {
        foreach ($items as $item) {

            $menu_item = new stdClass();
            $menu_item->parent_item = $parent;

            foreach ($item->attributes() as $key => $value) {

                $menu_item->$key = $value . "";
            }
            /* 			
              if(strpos(self::$current_url, $menu_item->link)){
              if(self::$current_menu_item == null || strlen(self::$current_menu_item->link)<strlen($menu_item->link))
              self::$current_menu_item = $menu_item;
              } */
            $menu_item->blocked = false;
            $parent->childs[] = $menu_item;

            if ($item->item) {
                $sub_item = $item->item;
                $this->fill_items($menu_item, $sub_item);
            }
        }
    }

//end fill_items

    /**
     * Remove those menus which are not available for current user
     */
    static function filter_menu(&$menu_root, &$user_roles) {
        foreach ($menu_root->childs as $item) {
            $roles = explode(",", $item->role);
            $item->blocked = true;
            foreach ($roles as $role) {
                if (in_array($role, $user_roles)) {
                    $item->blocked = false;
                    break;
                }

                if (!empty($item->childs))
                    self::filter_menu($item, $user_roles);
            }
        }
    }

    static function get_menu_html($name, $display_default = false) {

        if (empty(self::$menu_items[$name]))
            return "";

        $html = array();
        self::get_rec_menu_html(self::$menu_items[$name], $html, $display_default);
        return implode("", $html);
    }

//end get_menu_html

    private static function get_rec_menu_html(&$node, &$html, $display_default) {
        $html[] = "<ul class=\"menu toggle\">";
        foreach ($node->childs as $item) {
            if ($item->blocked)
                continue;
            if ($item->link == "/" && !$display_default)
                continue;
            $css_class = "";
            if (!empty($item->childs))
                $css_class .= "has-menu-child";
            if ($item === self::$current_menu_item)
                $css_class .= " current-menu-item";

            $html[] = "<li class=\"icn_settings " . $css_class . "\">";
            $html[] = "<a href=\"" . base_url() . $item->link . "\">";
            $html[] = $item->title;
            $html[] = "</a>";
            $html[] = "</li>";
        }
        $html[] = "</ul>";
    }

//end get_rec_menu_html

    public static function get_breadcrumb_html($show_home = true) {
        $nodes = array();
        if ($show_home)
            $nodes[] = "<a href='/'>Home</a>";

        if (self::$breadcrumb != null) {
            foreach (self::$breadcrumb as $node) {
                if (!empty($node->link))
                    $nodes[] = "<a href='" . site_url($node->link) . "'>" . $node->title . "</a>";
                else
                    $nodes[] = $node->title;
            }
        }else if (self::$current_menu_item != null) {
            $item_nodes = array();

            $item = self::$current_menu_item->parent_item;
            while ($item != null) {
                if (empty($item->link))
                    break;
                $item_nodes[] = "<a href='" . site_url($item->link) . "'>" . $item->title . "</a>";
                $item = $item->parent_item;
            }
            array_reverse($item_nodes);
            $item_nodes[] = self::$current_menu_item->title;
            $nodes = array_merge($nodes, $item_nodes);
        }

        return implode("&nbsp;&raquo;&nbsp;", $nodes);
    }

//end get_breadcrumb_html

    public static function get_page_title() {
        $nodes = array();

        if (self::$breadcrumb != null) {
            foreach (self::$breadcrumb as $node) {
                $nodes[] = $node->title;
            }
        } else if (self::$current_menu_item != null) {
            $item_nodes = array();

            $item = self::$current_menu_item->parent_item;
            while ($item != null) {
                if (empty($item->link))
                    break;
                $item_nodes[] = $item->title;
                $item = $item->parent_item;
            }
            array_reverse($item_nodes);
            $item_nodes[] = self::$current_menu_item->title;
            $nodes = array_merge($nodes, $item_nodes);
        }

        return implode(" :: ", $nodes);
    }

//end get_page_title

    public static function get_admin_details() {
        $CI = get_instance();
        $sql = "SELECT * FROM `tbl_user_admin`";
        $query = $CI->db->query($sql);
        $result = $query->first_row();
        return $result->user_name;
    }

//end get_page_title
    //for main menu 

    static function get_main_menu_html($name, $display_default = false) {

        if (empty(self::$menu_items[$name]))
            return "";

        $html = array();
        self::get_rec_main_menu_html(self::$menu_items[$name], $html, $display_default);
        return implode("", $html);
    }

//end get_menu_html

    private static function get_rec_main_menu_html(&$node, &$html, $display_default) {
        $html[] = "";

        foreach ($node->childs as $item) {
            //print_r($item); exit;
            if ($item->blocked)
                continue;
            if ($item->link == "/" && !$display_default)
                continue;
            $css_class = "";            
            
            if ($item->title == 'Profile') {
                $iclass = 'fa-user';      
                $link = base_url() . $item->link ;
            }
            if ($item->title == 'The Hub') {
                $iclass = 'fa-th-list';   
                 $link = base_url() . $item->link ;
            }
            if ($item->title == 'Portfolio') {
                $iclass = 'fa-file';  
                 $link = 'javascript:void(0)' ;
            }
            if ($item->title == 'Matching') {
                $iclass = 'fa-users';  
                //$link = 'javascript:void(0)';
				//$link = 'javascript:void(0)';
				
            }
            if ($item->title == 'Community') {
                $iclass = 'fa-comments';    
                //$link = 'javascript:void(0)';				
            }
            if ($item->title == 'Profile Views') {
                $iclass = 'fa-eye';        
                $link = 'javascript:void(0)';
            }
            if ($item->title == 'Settings') {
                $iclass = 'fa-gears';    
                 $link = base_url() . $item->link ;
            }
            if ($item->title == 'Wallet') {
                $iclass = 'fa-credit-card';  
                $link = 'javascript:void(0)';
            }
            //echo $iclass; exit;
            if ($item === self::$current_menu_item)
                $css_class .= "";
            $url = $_SERVER['REQUEST_URI'];
            //echo $item->link; exit;
            $active = strpos($url, $item->link) !== false ? 'active' : '';
            //$html[] = '<li class="treeview '. $active . '"><a href="' . base_url() . $item->link . '">';
            $html[] = '<li class="treeview '. $active . '"><a href="' .$link. '">';
            $html[] = '<i class="fa '.$iclass.'"></i> <span>';                    
            $html[] = $item->title;
            $html[] = '</span></a></li>';
        }
    }

//end get_rec_menu_html
    //emd main menu
}
