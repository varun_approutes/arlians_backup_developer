<?php
class User_Manager
{
	private $session_token_key = "USER_TOKEN";
	private $session_is_facebook = "USER_FROM_FACEBOOK";
	private $return_url_key = "RETURN_URL";
	
	private static $instance;
	private $ci;
	
	public function User_Manager()
	{
		self::$instance = $this;
		self::$instance->ci = get_instance();		
		self::$instance->ci->load->library('user_agent');
		self::$instance->ci->load->library('Session');		
		self::$instance->user_agent = new CI_User_agent();
	}
	
	function init()
	{	
		//validate with user role
		$current_menu = Menu_Helper::$current_menu_item;
		$logeed_in_user = self::$instance->get_looged_in_user();
		/*if($logeed_in_user)
			$user_roles = array($logeed_in_user->Role);
		else*/
			$user_roles = array("guest");
		
		if($current_menu != null){
			$roles = explode(",", $current_menu->role);
			$role_exist = false;
			foreach($roles as $role){
				if(in_array($role, $user_roles)){
					$role_exist = true;
					break;
				}
			}
			
			if(!$role_exist){
				$this->ci->session->set_userdata($this->return_url_key, current_url());
				$this->ci->session->unset_userdata($this->session_token_key);
				redirect("member");
				//show_error("You don't have permission to access this page", 403);
				return;
			}
		}
		
		/*foreach(Menu_Helper::$menu_items as $item){
			Menu_Helper::filter_menu($item, $user_roles);
		}*/
	}//end init
	
	static function get_instance()
	{
		return self::$instance;
		
	}//end get_instance
	function get_current_user_id_while_visting_other_user_profile(){
		$acc_login = $this->ci->session->userdata($this->session_token_key);
		if($acc_login){
			$this->ci->load->model("model_member");
			$staff_model = new Model_Member();
			$user = $staff_model->MemberDetails($acc_login);
			return $user['mid'];
		}
	}
	function get_looged_in_user()
	{		
		$acc_login = $this->ci->session->userdata($this->session_token_key);
		if($acc_login){
			$this->ci->load->model("model_member");
			$staff_model = new Model_Member();
			if(strpos($_SERVER['REQUEST_URI'], "viewprofile") == true || strpos($_SERVER['REQUEST_URI'], "hubdetails") == true){
				$segments = explode('/', $_SERVER['REQUEST_URI']);
				$viewer = $staff_model->getUserById($segments[5]);
				$acc_login = $viewer->email;
			}			
			$user = $staff_model->MemberDetails($acc_login);			
			if(!$user)
				$this->ci->session->unset_userdata($this->session_token_key);
			return $user;
		}
		return null;
	}//end get_looged_in_user

	function isAlreadyDataInWizTwo(){
		$logeed_in_user = self::$instance->get_looged_in_user();		
		$this->ci->load->model("model_member");
		$staff_model = new Model_Member();				
		$user = $staff_model->isAlreadyDataInWizTwo($logeed_in_user['mid']);
		return $user;
	}

	function isAlreadyDataInWizTwox($id){
		//$logeed_in_user = self::$instance->get_looged_in_user();		
		$this->ci->load->model("model_member");
		$staff_model = new Model_Member();				
		$user = $staff_model->isAlreadyDataInWizTwo($id);
		return $user;
	}
	
	function set_user_token($acc_login)
	{
		$this->ci->session->set_userdata($this->session_token_key, $acc_login);		
		
	}////end set_user_token
	
	function remove_user_token()
	{
		$this->ci->session->unset_userdata($this->session_token_key);
		
	}////end set_user_token
	
	function after_login($front='')
	{
		echo $front; 
		exit;

		if($front){
			//$redirect_url = "users/profile";
			//$redirect_url = "home/dashboard";
			$redirect_url = "betting/betslip";
		}else{
			$redirect_url = "member/dashboard";
		}
		redirect($redirect_url);
	}//end after_login
	
	function get_region_list()
	{
		$this->ci->load->model("home_model");
		$home = new Home_Model();
		$region_list = $home->getRegionList();
		return $region_list;
	}
	
	
}
