<?php 
function pr($array=array(),$die=true)
{
  echo '<pre>';
  print_r($array);
  if($die)
    die;
}
class Test extends CI_Controller
{
    function __construct(){
		parent::__construct();
		$this->load->model('model_member');
		$this->load->model('model_suggestion');

		$this->load->library('User_Manager');
		$this->load->helper('common_helper');
		$this->load->helper('text');
		$this->load->model('model_hub');
		$this->load->helper('load_controller');
		$this->load->model('model_othermembersprofile');
		//$this->model_common->SiteSettingsDetails(); // This is mandatory
		$this->user_manager = User_Manager::get_instance();
	}
        
	function getHubView(){

		$myMid = $this->session->userdata['logged_in']['id'];
		$mids = $this->model_hub->myConnectionList($myMid);
		$my_hub = array();
		array_push($mids,$myMid);

		foreach($mids as $id){
			$my_hub[$id] = $this->model_hub->hubPostedByMember($id);
		}
		
		return $my_hub;
	}

	function getHubViewApi($id){

		$myMid = $id;
		$mids = $this->model_hub->myConnectionList($myMid);
		$my_hub = array();
		array_push($mids,$myMid);

		foreach($mids as $id){
			$my_hub[$id] = $this->model_hub->hubPostedByMember($id);
		}
		
		return $my_hub;
	}

	function getHub(){

		$myMid = $this->session->userdata['logged_in']['id'];
		$tw_digit_codes = $this->model_hub->getTopLineFromMid($myMid);

		$top_line_arr = array();
		// topline code from twenty digit code 
		foreach($tw_digit_codes as $code){
			$cd = $code['mem_code'];
			$top_line_arr[] = substr($cd,0,5);
		}


		// topline code from ten digit code 
		$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($myMid);
//echo '<pre>'; var_dump($ten_digit_code); die();
		foreach($ten_digit_code as $code){
			$cd = $code['memcodetwo'];
			$tw_digit_code = $this->model_suggestion->getTwentyDigitCodeFromTenDigit($cd);
			
			foreach($tw_digit_code as $code){ 
				$cd = $code['wizone_code'];
				$top_line_arr[] = substr($cd,0,5);
			}
		}
		
		// topline code from tag code
		$tag_codes = $this->model_hub->getTagCodes($myMid);
		
		if($tag_codes != ''){
			$tag_arr = explode(',',$tag_codes);
			//echo '<pre>'; var_dump($tag_arr);
			foreach($tag_arr as $code){
				$cd = $this->model_hub->convertingHubIdToTopline($code);
				if($cd!=NULL){
					$top_line_arr[] = $cd;
				}
			}
		}
		
		$top_line_arr = array_unique($top_line_arr);

		$related_hub_ids = $this->model_hub->getHubIdsFromTopLine($top_line_arr);
		//echo '<pre>'; var_dump($related_hub_ids); die(); 
		return $related_hub_ids;
	}
	
	function getHubx($id){
		
		$myMid = $id;
		$tw_digit_codes = $this->model_hub->getTopLineFromMid($myMid);

		$top_line_arr = array();
		// topline code from twenty digit code 
		foreach($tw_digit_codes as $code){
			$cd = $code['mem_code'];
			$top_line_arr[] = substr($cd,0,5);
		}


		// topline code from ten digit code 
		$ten_digit_code = $this->model_suggestion->gettendigitcodeofmember($myMid);
//echo '<pre>'; var_dump($ten_digit_code); die();
		foreach($ten_digit_code as $code){
			$cd = $code['memcodetwo'];
			$tw_digit_code = $this->model_suggestion->getTwentyDigitCodeFromTenDigit($cd);
			
			foreach($tw_digit_code as $code){ 
				$cd = $code['wizone_code'];
				$top_line_arr[] = substr($cd,0,5);
			}
		}
		
		// topline code from tag code
		$tag_codes = $this->model_hub->getTagCodes($myMid);
		
		if($tag_codes != ''){
			$tag_arr = explode(',',$tag_codes);
			//echo '<pre>'; var_dump($tag_arr);
			foreach($tag_arr as $code){
				$cd = $this->model_hub->convertingHubIdToTopline($code);
				if($cd!=NULL){
					$top_line_arr[] = $cd;
				}
			}
		}
		
		$top_line_arr = array_unique($top_line_arr);

		$related_hub_ids = $this->model_hub->getHubIdsFromTopLine($top_line_arr);
		//echo '<pre>'; var_dump($related_hub_ids); die(); 
		return $related_hub_ids;
	}
	function moreHub(){
		
		$data['tag'] = $this->model_member->getAllTag();
		$data['user'] = $this->user_manager->get_looged_in_user();		
            $hub_ids_connects = load_controller('hub', 'getHubView');         
            $hub_ids=array();
            foreach($hub_ids_connects as $ids){
                if($ids != ""){
                   foreach($ids as $id){
                        $hub_ids[] = $id['hid'];
                   }
                }
            }
            $hub_ids_matches = load_controller('hub', 'getHub');
            foreach($hub_ids_matches as $id){
                if($id != ""){
                    $hub_ids[] = $id['hub_id'];
                }
            }
            $hub_ids = array_unique($hub_ids);
            $hub_data = array();
            foreach($hub_ids as $hid){
                $hubs = $this->model_hub->getHubById($hid);
                if($hubs != NULL){
                    $key = $hubs[0]['posted_on'];
                    $hub_data[strtotime($key)] = $hubs[0];
                }                
            }
            krsort($hub_data); 
            $mid=$this->session->userdata['logged_in']['id'];                      
            foreach ($hub_data as $key => $value) {
            	$hud_ind_id = $value['hid'];
            	$like = $this->model_hub->checkHubLike($hud_ind_id,$mid);
            	$comments = $this->model_hub->getComments($hud_ind_id);
            	$hub_data[$key]['comments'] = $comments;
            	$data_for_connection['mid'] = $mid;
            	$data_for_connection['connected_id'] = $value['mid'];
            	$is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection);        	//echo '<pre>'; print_r($comments);
            	$hub_data[$key]['is_connected'] = $is_connected;
            	$likes = $this->model_hub->getTotalLikes($hud_ind_id);
            	$hub_data[$key]['likes'] = $likes;            	
            	$hub_data[$key]['like'] = $like;            	   	
            } 
            //echo"<pre>";
            //print_r($hub_data);exit;
           $data['hubs'] = $hub_data;
         
        $this->load->view('member/morehub',$data);
    }

        function moreHubApiTest(){
        //print_r($_POST);die;
		//===========================================================
    $data['user_email'] = $this->input->post('user_emai');
    //$data['user_email'] = 'sims12781+41@gmail.com';
    $result = $this->model_member->getIdByUserEmail($data);

    $user_id = $result->mid; 
    $mid=$user_id;
    $mid = 22;
    //=============================================================  
		
		$data['tag'] = $this->model_member->getAllTag();
		//$data['user'] = $this->user_manager->get_looged_in_user();		
            //$hub_ids_connects = load_controller('hub', 'getHubView'); 
 
            $hub_ids_connects = $this->getHubViewApi($mid);

            $hub_ids=array();
            foreach($hub_ids_connects as $ids){
                if($ids != ""){
                   foreach($ids as $id){
                        $hub_ids[] = $id['hid'];
                   }
                }
            }
            //$hub_ids_matches = load_controller('hub', 'getHub');
            $hub_ids_matches = $this->getHubx($mid);
            foreach($hub_ids_matches as $id){
                if($id != ""){
                    $hub_ids[] = $id['hub_id'];
                }
            }
            $hub_ids = array_unique($hub_ids);

            $hub_data = array();
            foreach($hub_ids as $hid){
                $hubs = $this->model_hub->getHubById($hid);
                if($hubs != NULL){
                    $key = $hubs[0]['posted_on'];
                    $hub_data[strtotime($key)] = $hubs[0];
                    $hub_data[strtotime($key)]['timestamp'] = strtotime($hubs[0]['posted_on']);
                    $hub_title = explode(',',$hubs[0]['tag_id']);
                    $hubTitle = array();
                    foreach($hub_title as $hb){

                    	$title = getTagNameById($hb);
                    	$hubTitle[$hb] = $title[0]['tag_name'];
                    }
                    $hub_data[strtotime($key)]['tag_title'] = $hubTitle; 
                }                
            }
            krsort($hub_data);
                   
            foreach ($hub_data as $key => $value) {

            	$hud_ind_id = $value['hid'];
            	$like = $this->model_hub->checkHubLike($hud_ind_id,$mid);
            	$comments = $this->model_hub->getComments($hud_ind_id);
            	$hub_data[$key]['comments'] = $comments;

            	$data_for_connection['mid'] = $mid;
            	$data_for_connection['connected_id'] = $value['mid'];
            	$is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection);        	//echo '<pre>'; print_r($comments);
            	$hub_data[$key]['is_connected'] = $is_connected;
            	$likes = $this->model_hub->getTotalLikes($hud_ind_id);
            	$hub_data[$key]['likes'] = $likes;            	
            	$hub_data[$key]['like'] = $like;
            	$hub_data[$key]['type'] = 'hub';  
            	$hub_data[$key]['base_url'] = base_url().'uploads/hub_images';           	   	
            } 
            
           $data_api['hubs'] = $hub_data;

           //-----------------Watson ------------
           include_once('memberprofile.php');
           $obj = new memberprofile();
           $xdata = $obj->watsonList_api($mid);
           $data_api['watson_news'] = json_decode($xdata);

           
           //-----------------------------------

           //--------------Bing---------------

           $bing = new memberprofile();
           $bing->bing_api($mid);
           $data_api['bing_news'] = $bing->fetchBingNews($mid);
           //$data_api['bing_news'] = 
           //--------------------------------
           //echo '<pre>'; print_r($data_api['bing_news']); die();
           $i=1;
           $j=0;
           $z=0;

            $api = array();
            if(!empty($data_api['hubs']))
            {
    				foreach($data_api['hubs'] as $hbs)
    		    {
           		if($i%5!=0)
           		{
                array_push($api,$hbs);
           		}
           		else
              {
           			array_push($api,'xx','yy');
              }
              $i++;
    		    }
	        }
          
      	  foreach($api as $key=>$value)
          {
            if($value=="xx")
            {
              if(!empty($data_api['watson_news'][$j]))
                $api[$key]=$data_api['watson_news'][$j++];
            }
            else if($value=="yy")
            {
              if(!empty($data_api['bing_news'][$z]))
                $api[$key]=$data_api['bing_news'][$z++];
            }
          }
          
          for($i=$j;;$i++)
          {
            if(!empty($data_api['watson_news'][$i]))
              array_push($api,$data_api['watson_news'][$i]);
            if(!empty($data_api['bing_news'][$z]))
              array_push($api,$data_api['watson_news'][$z++]);
            if(empty($data_api['watson_news'][$i]) && empty($data_api['bing_news'][$z]))
              break;
          }
	    	
          pr($api);
           //-----------------------------------
         echo json_encode($api);
         exit;
        //$this->load->view('member/morehub',$data);
    }
    private function loadWatsonAndBing($data_api,$api,$i,$j)
    {
      if(!empty($data_api['watson_news'][$j]))
      {
        $api[$i] = $data_api['watson_news'][$j];
        //$api[$i++]="Watson";
      }
      if(!empty($data_api['bing_news'][$j])){
        $api[$i-1] = $data_api['bing_news'][$j];
        //$api[$i+1]="Bing";
      }
      //echo '<pre>'; var_dump($api); 
      return $api;
    }

    function moreHubApi(){
        //print_r($_POST);die;
		//===========================================================
		$data['user_email'] = $this->input->post('user_emai');
		//$data['user_email'] = 'sims12781+41@gmail.com';
		$result = $this->model_member->getIdByUserEmail($data);

		$user_id = $result->mid; 
        $mid=$user_id;
        //=============================================================  
		
		$data['tag'] = $this->model_member->getAllTag();
		//$data['user'] = $this->user_manager->get_looged_in_user();		
            //$hub_ids_connects = load_controller('hub', 'getHubView'); 
 
            $hub_ids_connects = $this->getHubViewApi($mid);

            $hub_ids=array();
            foreach($hub_ids_connects as $ids){
                if($ids != ""){
                   foreach($ids as $id){
                        $hub_ids[] = $id['hid'];
                   }
                }
            }
            //$hub_ids_matches = load_controller('hub', 'getHub');
            $hub_ids_matches = $this->getHubx($mid);
            foreach($hub_ids_matches as $id){
                if($id != ""){
                    $hub_ids[] = $id['hub_id'];
                }
            }
            $hub_ids = array_unique($hub_ids);

            $hub_data = array();
            foreach($hub_ids as $hid){
                $hubs = $this->model_hub->getHubById($hid);
                if($hubs != NULL){
                    $key = $hubs[0]['posted_on'];
                    $hub_data[strtotime($key)] = $hubs[0];
                    $hub_data[strtotime($key)]['timestamp'] = strtotime($hubs[0]['posted_on']);
                    $hub_title = explode(',',$hubs[0]['tag_id']);
                    $hubTitle = array();
                    foreach($hub_title as $hb){

                    	$title = getTagNameById($hb);
                    	$hubTitle[$hb] = $title[0]['tag_name'];
                    }
                    $hub_data[strtotime($key)]['tag_title'] = $hubTitle; 
                }                
            }
            krsort($hub_data);
                   
            foreach ($hub_data as $key => $value) {

            	$hud_ind_id = $value['hid'];
            	$like = $this->model_hub->checkHubLike($hud_ind_id,$mid);
            	$comments = $this->model_hub->getComments($hud_ind_id);
            	$hub_data[$key]['comments'] = $comments;
                $hub_data[$key]['comment_count'] = count($comments);
            	$data_for_connection['mid'] = $mid;
            	$data_for_connection['connected_id'] = $value['mid'];
            	$is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection);        	//echo '<pre>'; print_r($comments);
            	$hub_data[$key]['is_connected'] = $is_connected;
            	$likes = $this->model_hub->getTotalLikes($hud_ind_id);
            	$hub_data[$key]['like_count'] = $likes;            	
            	$hub_data[$key]['like'] = $like;
            	$hub_data[$key]['type'] = 'hub';  
            	$hub_data[$key]['base_url'] = base_url().'uploads/hub_images/'; 
              $hub_data[$key]['profile_image'] = getProfileImage($value['mid']); 
              $hub_data[$key]['bussiness_name'] = getMemberName($value['mid']); 

            } 
            
           $data_api['hubs'] = $hub_data;

           //-----------------Watson ------------
           include_once('memberprofile.php');
           $obj = new memberprofile();
           $xdata = $obj->watsonList_api($mid);
           $data_api['watson_news'] = json_decode($xdata);

           
           //-----------------------------------

           //--------------Bing---------------

           $bing = new memberprofile();
           $bing->bing_api($mid);
           $data_api['bing_news'] = $bing->fetchBingNews($mid);
           //$data_api['bing_news'] = 
           //--------------------------------
           //echo '<pre>'; print_r($data_api['bing_news']); die();
           $i=1;
           $j=0;
           $z=0;

            $api = array();
            if(!empty($data_api['hubs']))
            {
				foreach($data_api['hubs'] as $hbs)
		        {
	           		if($i%5!=0)
	           		{
	           			$api[$i++]=$hbs;
	           			//$api[$i++]="Hub";
	           		}
	           		else
	           			$i+=2;
		        }
	        }
	        //echo '<pre>';print_r($api);die;
	        for($i=1;;$i++)
	        {
	        	if(!empty($data_api['hubs']))
            	{
		        	if($i%5==0 && count($data_api['hubs'])>4)
		        	{
		        		$api=$this->loadWatsonAndBing($data_api,$api,$i,$j);

		           	}
		           	else
		           	{
		           		if((count($data_api['hubs'])+1)>$i)
		           			$i=count($data_api['hubs'])+1;
		           		$api=$this->loadWatsonAndBing($data_api,$api,$i,$j);
		           	}
	            }
	            else
	            {
	            	$api=$this->loadWatsonAndBing($data_api,$api,$i,$j);
	            }
	            if(empty($data_api['watson_news'][$j]) && empty($data_api['bing_news'][$j]))
    				break;
    			$j++;
   			}
	    	
           //echo '<pre>'; print_r($api); die();
           //-----------------------------------
         echo json_encode($api);
         exit;
        //$this->load->view('member/morehub',$data);
    }
    
    public function hubEditText(){
    	$data['content'] = $this->input->post('edited_text');
    	$id = $this->input->post('hid');    	
    	$result = $this->model_hub->post_hub_edit($id,$data);
    	echo $result;
    	exit;
    }
    public function hubImgEdit(){
    	$imagename = $this->input->post('imgFile');
    	if($imagename!=''){
			$content = $imagename;
			//print_r($content); exit;
			if($content<>''){
				$parts=explode(',',$content);
				if(count($parts)>1){					
					$product_image_data = base64_decode(str_replace($parts[0].',',"",$content));
					//print_r($product_image_data); exit;
					$image_name = "member".round(microtime(true) * 1000).".png";
					$target_path = FCPATH ."uploads".DS."hub_images".DS.$image_name;

					file_put_contents($target_path,$product_image_data);						
					$data = array("content" => $image_name);	
					$id = $this->input->post('hid');			
					$result = $this->model_hub->post_hub_edit($id,$data);
					echo $result;
				}else{
					$result = false;
				}
				exit;
			}
		}
    }
    function hubDetails($hubid,$hubpostid){
    	if($_POST){
    		print_r($_POST); exit;
    	}
		$mid=$this->session->userdata['logged_in']['id'];
		$hubid=$this->uri->segment(4);
		$hubpostid=$this->uri->segment(3);
		$data['hubid']=$hubid;
		$data['hubpostid']=$hubpostid;
		if($mid==$hubpostid){
			$data['viewtype']='own';
		}else{
			$data['viewtype']='other';
		}
		$hubs = $this->model_hub->getHubById($hubid);
		$data['hub']=$hubs[0];
		$like = $this->model_hub->checkHubLike($hubid,$mid);
        $comments = $this->model_hub->getComments($hubid);
        $data['hub']['comments'] = $comments;
		$data_for_connection['mid'] = $mid;
        $data_for_connection['connected_id'] = $hubpostid;
        $is_connected =  $this->model_othermembersprofile->getConnectRequestDetail($data_for_connection); 
        $data['hub']['is_connected'] = $is_connected;
        $likes = $this->model_hub->getTotalLikes($hubid);
        $data['hub']['likes'] = $likes;            	
        $data['hub']['like'] = $like;   
		
	$this->load->view('member/hubdetails',$data);
	}

    function shared(){
    	$data['mid'] = $this->session->userdata['logged_in']['id'];
    	$data['hub_id'] = $this->input->post('hub_id');
		
		$posted_id=getHubById($data['hub_id']);
		$data1['whose_profile_viewed'] = $posted_id[0]["mid"];
		$data1['who_viewed'] =$data['mid'];
		$data1['title'] = '';
		$data1['noti_type'] = 'hub_share';
		$this->model_othermembersprofile->inser_view($data1);
		
		
		
    	echo $res = $this->model_hub->shared($data);
		exit;
    }

    function postComment(){
    	$user=$this->user_manager->get_looged_in_user();
    	//print_r($user); exit;
    	$data['hub_id'] = $_POST['hub_id'];
    	$data['comments'] = $_POST['comment'];
		$data['mid'] = $this->session->userdata['logged_in']['id'];
		$res_id = $this->model_hub->postComment($data);
		
		
		$posted_id=getHubById($data['hub_id']);
		$data1['whose_profile_viewed'] = $posted_id[0]["mid"];
		$data1['who_viewed'] =$data['mid'];
		$data1['title'] = '';
		$data1['noti_type'] = 'hub_comment';
		$this->model_othermembersprofile->inser_view($data1);
		
		
		
		
		$res = array('id' =>$res_id,'comment'=>checkURLfromString($data['comments']),'user_image'=>$user['fornt_image']);
		echo json_encode($res);		
		exit;
    }

	function conceptInsights(){		
		$this->load->view('member/conceptInsights');
	}
	function firstInsightSearch(){
		$key_word = rawurlencode($this->input->post('keyword'));
		//$key_word =$_REQUEST['q'];
		$curlSession = curl_init();
		$url='https://concept-insights-demo.mybluemix.net/api/labelSearch?query='.$key_word.'&limit=7&concept_fields={%22abstract%22%3A1}';
		curl_setopt($curlSession, CURLOPT_URL, $url);
		curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

		$jsonData = json_decode(curl_exec($curlSession));
		curl_close($curlSession);
		//echo '<pre>';
		foreach($jsonData as  $key => $jdata){
			if($key=='matches'){
				$matches = $jdata;
			}
		}		
		//print_r($matches);
		$suggested_array=array();
		foreach($matches as $key => $data){
			foreach($data as $keydata => $data_value){
				if($keydata == 'label'){
					$obj = (object) array('lable' => $data_value);
					array_push($suggested_array,$obj);
				}
			}			
		}
		echo json_encode($suggested_array);	
		//print_r ($suggested_array);
		exit;
		
	}
	function finalInsightSearch(){
		$key_word = rawurlencode($this->input->post('keyword'));
		//$key_word =$_REQUEST['q'];
		$curlSession = curl_init();
		$url='https://concept-insights-demo.mybluemix.net/api/conceptualSearch?ids%5B%5D=%2Fgraphs%2Fwikipedia%2Fen-20120601%2Fconcepts%2F'.$key_word.'&limit=10&document_fields=%7B%22user_fields%22%3A1%7D';
		curl_setopt($curlSession, CURLOPT_URL, $url);
		curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);

		$jsonData = json_decode(curl_exec($curlSession));
		$jsonDat = curl_exec($curlSession);
		curl_close($curlSession);
		//$api='curl -u 1213a261-d453-474d-bd8f-51e95d73802c:xPm7tN6BhxsA "https://gateway.watsonplatform.net/concept-insights/api/v2/corpora/public/ibmresearcher/conceptual_search?ids%5B%5D=%2Fgraphs%2Fwikipedia%2Fen-20120601%2Fconcepts%2F'.$key_word.'&limit=10&document_fields=%7B%22user_fields%22%3A1%7D"';
		//echo $api;
		//$jsonDat = exec($api);
		echo $jsonDat;
		//exit;
		foreach($jsonData as  $key => $jdata){
			if($key=='matches'){
				$matches = $jdata;
			}
		}		
		print_r($matches);
		$suggested_array=array();
		foreach($matches as $key => $data){
			foreach($data as $keydata => $data_value){
				if($keydata == 'label'){
					$obj = (object) array('lable' => $data_value);
					array_push($suggested_array,$obj);
				}
			}			
		}
		echo json_encode($suggested_array);	
		//print_r ($suggested_array);
		exit;
		
	}
	function postLike(){
		$hid = $_POST['hub_id'];
		$mid = $this->session->userdata['logged_in']['id'];
		$posted_id=getHubById($hid);
		$data1['whose_profile_viewed'] = $posted_id[0]["mid"];
		$data1['who_viewed'] =$mid;
		$data1['title'] = '';
		$data1['noti_type'] = 'hub_like';
		//var_dump($data1);die();
		$this->model_othermembersprofile->inser_view($data1);
		echo $this->model_hub->postLike($hid,$mid);
		exit;
	}

	function checkLike(){
		$hid = $_POST['hub_id'];
		$mid = $this->session->userdata['logged_in']['id'];		
		echo $res = $this->model_hub->checkHubLike($hid,$mid);
		exit;
	}

	function hubPublish(){
		//My publish post
		$id = $this->session->userdata['logged_in']['id'];
		$data['my_publish'] = $this->model_hub->getMyPublish($id);
		$data['tag'] = $this->model_member->getAllTag();
		$this->load->view('member/hub_publish',$data);
	}

	function hubPublishApi(){
		//My publish post
		$arr['email'] = $this->input->post('user_email');
		$user_id = getIdByUserEmail($arr['email']);

		$id = $user_id->mid;

		$data['my_publish'] = $this->model_hub->getMyPublish($id);
		$data['tag'] = $this->model_member->getAllTag();
												
		echo json_encode($array=array('msg'=>'Success'));	
		//print_r ($suggested_array);
		exit;

	}
	
	function hubPublishEdit(){
		$hid=$this->uri->segment(3);
		$hub_details=$this->model_hub->getHubById($hid);
		
		$data['hid']=$hub_details[0]['hid'];
		$data['mid']=$hub_details[0]['mid'];
		$data['hub_title']=$hub_details[0]['hub_title'];
		$data['content']=$hub_details[0]['content'];
		$data['link']=$hub_details[0]['link'];
		$data['tag_id']=$hub_details[0]['tag_id'];
		
		$id = $this->session->userdata['logged_in']['id'];
		$data['my_publish'] = $this->model_hub->getMyPublish($id);
		$data['tag'] = $this->model_member->getAllTag();
		
		$this->load->view('member/hub_publish_edit',$data);
	}

	function hubPublishEditApi(){
		$hid=$this->uri->segment(3);
		$hub_details=$this->model_hub->getHubById($hid);
		
		$data['hid']=$hub_details[0]['hid'];
		$data['mid']=$hub_details[0]['mid'];
		$data['hub_title']=$hub_details[0]['hub_title'];
		$data['content']=$hub_details[0]['content'];
		$data['link']=$hub_details[0]['link'];
		$data['tag_id']=$hub_details[0]['tag_id'];
            //---------------------

            $arr['email'] = $this->input->post('user_email');
            $user_id = getIdByUserEmail($arr['email']);

            $id = $user_id->mid;

            //---------------------
		$data['my_publish'] = $this->model_hub->getMyPublish($id);
		$data['tag'] = $this->model_member->getAllTag();
		
		$this->load->view('member/hub_publish_edit',$data);
	}
	
	function hub_edit_post(){
		
			$tag_arr = '';
			if($_POST['tag'] != ''){
				$tag_arr = implode(',',$_POST['tag']);
			}
			
			$filename = "";
			//echo '<pre>';print_r($_POST);print_r($_FILES);die;
			if(!empty($_FILES['file_hub_post']['name'])){
				//echo "TEST";die;
				$prev_img_path = FCPATH."/uploads/hub_images/".$_POST['prev_img'];
				unlink($prev_img_path);
				
				$path = $_FILES['file_hub_post']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				
				$filename = uniqid('HubPublish', true).".".$ext;
				
				$tmp_file = $_FILES['file_hub_post']['tmp_name'];
				$uploads_dir = FCPATH.'/uploads/hub_images/'.$filename;

				move_uploaded_file($tmp_file, $uploads_dir);
			}else{
				$filename=$_POST['prev_img'];
			}
            //---------------------

            $arr['email'] = $this->input->post('user_email');
            $user_id = getIdByUserEmail($arr['email']);

            $data['mid'] = $user_id->mid;

            //---------------------
			$data['hub_title'] = $_POST['title'];
			$data['content'] = $_POST['comments'];
			$data['link'] = $filename;
			$data['shared_for'] = 4;
			$data['tag_id'] = $tag_arr;
			$hub_id = $_POST['hid'];
			$this->model_hub->post_hub_edit($hub_id,$data);
			
			$tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
			$code = $tw_digit_code[0]['mem_code'];
			$code = substr($code,0,5);
			
			$dt['hub_id'] = $hub_id;
			$dt['topline_code'] = $code;

			$res = $this->model_hub->hubPostDetails($dt);

			if($tag_arr != ''){
				$tag_arr = $_POST['tag'];
				//var_dump($tag_arr); die();
				$this->model_hub->del_hub_details($hub_id);
				foreach($tag_arr as $tag){
					$top_line_code = $this->model_hub->convertingHubIdToTopline($tag);
					
					if($top_line_code != NULL){
						$d['hub_id'] = $hub_id;
						$d['topline_code'] = $top_line_code;
						$res = $this->model_hub->hubPostDetails($d);
					} 
				} 
			}
			

		redirect('hub/hubPublish');
	}
	
	function hub_post_api(){
			
			$tag_arr = '';
			if($_POST['tag'] != ''){
				$tag_arr = implode(',',$_POST['tag']);
			}			
			

			//---------------------------------------------
			/*
			$filename = "";
			if(isset($_FILES)){
				$path = $_FILES['file_hub_post']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				
				$filename = uniqid('HubPublish', true).".".$ext;
				
				$tmp_file = $_FILES['file_hub_post']['tmp_name'];
				$uploads_dir = FCPATH.'/uploads/hub_images/'.$filename;

				 move_uploaded_file($tmp_file, $uploads_dir);
				 
			}
			*/
			$image_name;
			$imagename = $this->input->post('imgFile');
			if ($imagename != '')
			{
				$content = $imagename;
				if ($content <> '') 
				{
					$parts = explode(',', $content);
	                if (count($parts) > 1) 
	                {
	                    $product_image_data = base64_decode(str_replace($parts[0] . ',', "", $content));
	                    $image_name = "HubPublish" . round(microtime(true) * 1000) . ".png";
	                    $target_path = FCPATH . "uploads" . DS . "hub_images" . DS . $image_name; 
	                    file_put_contents($target_path, $product_image_data);
					}
				}
			}  //die('===');
			//--------------------------------------------------
			//---------------------

            $arr['email'] = $this->input->post('user_email');
            $user_id = getIdByUserEmail($arr['email']);

            $data['mid'] = $user_id->mid;

            //---------------------
			$data['hub_title'] = $_POST['title'];
			$data['content'] = $_POST['comments'];
			$data['link'] = $image_name;

			$data['shared_for'] = 4;
			$data['tag_id'] = $tag_arr;
			
			$hub_id = $this->model_member->post_hub($data);
			
			$tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
			$code = $tw_digit_code[0]['mem_code'];
			$code = substr($code,0,5);

			$dt['hub_id'] = $hub_id;
			$dt['topline_code'] = $code;

			$res = $this->model_hub->hubPostDetails($dt);

			if($tag_arr != ''){
				$tag_arr = $_POST['tag'];
				foreach($tag_arr as $tag){
					$top_line_code = $this->model_hub->convertingHubIdToTopline($tag);
					
					if($top_line_code != NULL){
						$d['hub_id'] = $hub_id;
						$d['topline_code'] = $top_line_code;
						$res = $this->model_hub->hubPostDetails($d);
					} 
				} 
			}
			$res = array('response'=>'success','hub_id'=>$hub_id);
			echo json_encode($res); exit;
		//redirect('hub/hubPublish');
	}

	function hub_post(){
			
			$tag_arr = '';
			if($_POST['tag'] != ''){
				$tag_arr = implode(',',$_POST['tag']);
			}			
			$filename = "";
			if(isset($_FILES)){
				$path = $_FILES['file_hub_post']['name'];
				$ext = pathinfo($path, PATHINFO_EXTENSION);
				
				$filename = uniqid('HubPublish', true).".".$ext;
				
				$tmp_file = $_FILES['file_hub_post']['tmp_name'];
				$uploads_dir = FCPATH.'/uploads/hub_images/'.$filename;

				 move_uploaded_file($tmp_file, $uploads_dir);
			}
			$data['mid'] =  $this->session->userdata['logged_in']['id'];
			$data['hub_title'] = $_POST['title'];
			$data['content'] = $_POST['comments'];
			$data['link'] = $filename;
			$data['shared_for'] = 4;
			$data['tag_id'] = $tag_arr;
			
			$hub_id = $this->model_member->post_hub($data);
			
			$tw_digit_code = $this->model_suggestion->gettwentydigitcodeofmember($data['mid']);
			$code = $tw_digit_code[0]['mem_code'];
			$code = substr($code,0,5);

			$dt['hub_id'] = $hub_id;
			$dt['topline_code'] = $code;

			$res = $this->model_hub->hubPostDetails($dt);

			if($tag_arr != ''){
				$tag_arr = $_POST['tag'];
				foreach($tag_arr as $tag){
					$top_line_code = $this->model_hub->convertingHubIdToTopline($tag);
					
					if($top_line_code != NULL){
						$d['hub_id'] = $hub_id;
						$d['topline_code'] = $top_line_code;
						$res = $this->model_hub->hubPostDetails($d);
					} 
				} 
			}

		redirect('hub/hubPublish');
	}

	function deleteComment(){
    	$data['comm_id']=$this->input->post('comment_id');		
		$data['mid']=$this->session->userdata['logged_in']['id'];
		echo $res = $this->model_hub->deleteComment($data);
		exit;
    }

    function deletePost(){
    	$data['hub_id']=$this->input->post('hub_id');		
		$data['mid']=$this->session->userdata['logged_in']['id'];
		echo $res = $this->model_hub->deletePost($data);
		exit;
    }

	function deleteLike(){
		$hid = $_POST['hub_id'];
		$mid = $this->session->userdata['logged_in']['id'];
		echo $res = $this->model_hub->deleteHub($hid,$mid);
		exit;

	}

	function reportHub(){
		$data['hub_id']=$this->input->post('hub_id');
		$data['reports']=$this->input->post('reason_report');
		$data['mid']=$this->session->userdata['logged_in']['id'];
		echo $res = $this->model_hub->reportHub($data);
		exit;
	}
	function hub_delete(){
		$id = $_POST['id'];
		$res = $this->model_hub->hub_delete($id);
		echo $res;
		exit;
	}

  function urlTest()
  {
     echo '=============';
    $url = 'https://in.news.yahoo.com/month-water-train-railways-sends-031500547.html';
    echo getFavicon($url);

  }
	
}