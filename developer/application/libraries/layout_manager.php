<?php

class Layout_Manager {

    function render_layout() {
        //echo $_SERVER["HTTP_REFERER"];

        global $OUT, $preffered_layout, $GLOBAL_ERROR, $GLOBAL_MESSAGE;
        $layout = "landing";
        $CI = & get_instance();
        $CI->load->library('session');

        if (strpos($_SERVER['REQUEST_URI'], "member") == true) {
            $layout = "default";
        }
        if (strpos($_SERVER['REQUEST_URI'], "retrivepass") == true) {
            $layout = "retrivepass";
        }
		if(strpos($_SERVER['REQUEST_URI'], "newsletterunsubscribe") == true){
			$layout = "retrivepass";
		}
        if (strpos($_SERVER['REQUEST_URI'], "payment") == true || strpos($_SERVER['REQUEST_URI'], "suggestion") == true || strpos($_SERVER['REQUEST_URI'], "hub") == true || strpos($_SERVER['REQUEST_URI'], "viewprofile") == true || strpos($_SERVER['REQUEST_URI'], "message") == true || strpos($_SERVER['REQUEST_URI'], "company") == true || strpos($_SERVER['REQUEST_URI'], "memberprofile") == true || strpos($_SERVER['REQUEST_URI'], "search") == true) {
            $layout = "postlogin";
        }
        if (strpos($_SERVER['REQUEST_URI'], "admin") == true) {
            $layout = "admin";
        }
        if (strpos($_SERVER['REQUEST_URI'], "dashboard") == true || strpos($_SERVER['REQUEST_URI'], "memberprofile") == true|| strpos($_SERVER['REQUEST_URI'], "message") == true|| strpos($_SERVER['REQUEST_URI'], "viewprofile") == true|| strpos($_SERVER['REQUEST_URI'], "connection") == true ) {
            $layout = "arlians";
        }
        $content = $CI->output->get_output();
        if (Menu_Helper::$current_menu_item && Menu_Helper::$current_menu_item->layout) {
            $layout = Menu_Helper::$current_menu_item->layout;
        }

        if ($preffered_layout != '')
            $layout = $preffered_layout;

        if ($layout != "admin") {
            //replace with module_content
            preg_match_all("/(\{(.[^\}]*)\})/", $content, $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $module_args = explode(" ", $match[2]);
                if ($module_args[0] != 'load')
                    continue;
                ob_start();
                $params = array();
                for ($i = 3; $i < count($module_args); $i++) {
                    $params[] = $module_args[$i];
                }
                $this->load_module($module_args[1], $module_args[2], $params);
                $output = ob_get_clean();

                $content = str_replace($match[0], $output, $content);
            }
            //end
        }
        //echo  $layout;	
        $template_path = FCPATH . APPPATH . "layouts" . DS . $layout . ".php";
        ob_start();
        include($template_path);
        $output = ob_get_clean();

        /* if($layout == "facebook"){
          $pattern = "/(" . str_replace("/", "\/", base_url()) . ")((?!assets|services|assetsfonts).[^\/]*)/im";
          $output = preg_replace($pattern, "\\1facebook/\\2", $output);

          preg_match_all($pattern, $output, $matches, PREG_SET_ORDER);
          foreach($matches as $match){
          if(strpos($match[2], "assets") !== false) continue;
          $output = str_replace($match[0], $output, $match[0] . "facebook/" . $match[0]);
          }

          } */

        $OUT->_display($output);
    }

    public function get_modules($module_type) {
        $modules = array();
        if ($module_type == "admin") {
            $path = dirname(__FILE__) . DS . ".." . DS . "admin_module";
            if ($handle = opendir($path)) {
                while (false !== ($entry = readdir($handle))) {
                    if ($entry == "." || $entry == "..")
                        continue;
                    include($path . DS . $entry . DS . $entry . ".php");
                    $class = ucfirst($entry);
                    $module = new $class;
                    $modules[] = $module;
                }
            }
        }

        return $modules;
    }

    public function load_module($controller, $action = "index", $args = array()) {
        $parts = explode("/", $controller);
        $controller_folder = FCPATH . DS . 'application' . DS . 'controllers';
        for ($i = 0; $i < count($parts) - 1; $i++) {
            $controller_folder .= DS . $parts[$i];
        }
        $controller_name = $parts[count($parts) - 1];
        if (!class_exists($controller_name)) {

            require_once($controller_folder . DS . $controller_name . ".php");
        }
        $controller_name = ucfirst($controller_name);
        $controller_instand = new $controller_name;
        $reflectionMethod = new ReflectionMethod($controller_name, $action);
        $reflectionMethod->invokeArgs($controller_instand, $args);
    }

}
