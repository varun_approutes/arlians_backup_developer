<div class="row" id="dashboard_display" style="display:none">
    <div class="col-md-9 col-sm-12" ng-controller="DashboardHub as hub">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="row" id="class_animate">
                    <div class="post_s">
                        <div class="col-md-12" ng-show="show_default_hub">
                            <div class="box post_update">
                                <div class="post-header">
                                    <ul class="post_link">
                                        <li><a href="javascript:void(0)"><i class="fa fa-edit"></i> Status</a></li>
                                        <li> 
                                            <div>
                                                <input type="file" name="file-3" ng-files="getTheFiles($files)" id="file-3" class="inputfile inputfile-3" data-multiple-caption="{count} files selected"/>
                                                <label for="file-3">                                             
                                                    <span><i class="fa fa-photo"></i> <label class="sm_none">Photos</label> / <i class="fa fa-video-camera"></i> <label class="sm_none">Videos</label> </span>
                                                </label>
                                            </div>
                                        </li>
                                        <!-- <li><a href="javascript:void(0)"><i class="fa fa-photo" ng-click="showPublishSection()"></i><span class="sm_none">Photos</span> / <i class="fa fa-video-camera" onclick="alertify.alert('Section is under construction')"></i><span class="sm_none"> Videos</span></a></li> -->
                                        <!--<li><a href="<?php echo base_url() ?>hub/hubPublish"><i class="fa fa-sign-out"></i> Publish</a></li>-->
                                        <li><a href="javascript:void(0);"  ng-click="showPublishSection()"><i class="fa fa-sign-out"></i> Publish</a></li>
                                    </ul>
                                </div>
                                <form ng-submit="sharePost()"> 
                                    <div class="box-body">
                                        <textarea class="form-control post_field" placeholder="What’s on your mind?" ng-blur="postUrl(text_in_mind)" ng-model="text_in_mind"></textarea>                   
                                        <div class="img_pic" id="post_img" style="display:none"><i class="fa fa-close" ng-click="removeDoc($event)"></i><img id='p_img' src="" /></div>                                       

                                        <div class="news_headlineholder" id="postUrlBlank" ng-bind-html="html" style="display:none">

                                            <i class="fa fa-close"></i>
                                        </div> 
                                    </div>
                                    <div class="post_footer clearfix">
                                        <span class="share_list" style="color:#fff" ng-bind="show_selected">Hello world</span>
                                        <!--<ul class="post_footerlink">
                                            <li><a href="#"><i class="fa fa-user-plus"></i></a></li>
                                            <li><a href="#"><i class="fa fa-video-camera"></i></a></li>
                                            <li><a href="#"><i class="fa fa-photo"></i></a></li>
                                        </ul>-->
                                        <div class="post_footebtn pull-right">
                                            <div class="btn-group post_footerdrpbtn">
                                                <!--<i class="fa fa-gears pull-left"></i>-->
                                                <button aria-expanded="false" type="button" ng-click="select()" class="btn btn-sm">Post</button>
                                                <!--                                                <button aria-expanded="false" type="button" data-toggle="dropdown" class="btn  btn-sm dropdown-toggle">Post</button>
                                                                                                <ul role="menu" class="dropdown-menu pull-right">                                        
                                                                                                    <li><a href="javascript:void(0);" ng-click="select(list)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                                    
                                                                                                </ul>-->
                                                <!--                                        <div class="share_btn">
                                                                                            <div class="select-style">
                                                                                                <select ng-options="opt.shareId as opt.name for opt in share_with_list">
                                                                                                </select>
                                                                                            </div>
                                                                                        </div>-->
                                            </div>
                                            <!--<button type="button" class="btn post_btn" ng-click='sharePost()'>Post</button>-->
                                            <!--                                    <a href="javascript:void(0)" class="btn post_btn" ng-click='sharePost()'>Post</a>-->

                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- <div class="box post_update">
                                <div class="post-header">
                                    <div class="btn-group confidentiality pull-right">
                                        <button class="btn  btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Confidentiality  <i class="fa fa-chevron-down"></i></button>
                                        <ul class="dropdown-menu pull-right" role="menu">
                                            <li><a href="javascript:void(0)">Add new event</a></li>
                                            <li><a href="javascript:void(0)">Clear events</a></li>
                                            <li class="divider"></li>
                                            <li><a href="javascript:void(0)">View calendar</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="box-body">
                                    <form>
                                        <div class="user_pic">
                                            <img alt="" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg">
                                        </div>
            
                                        <textarea class="form-control post_field" placeholder="What’s up in your mind?"></textarea>
                                        <div aria-label="..." role="group" class="btn-group btn-group-justified post_btngrp">
                                            <div role="group" class="btn-group">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-edit"></i> Update</button>
                                            </div>
                                            <div role="group" class="btn-group">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-photo"></i> Media</button>
                                            </div>
                                            <div role="group" class="btn-group">
                                                <button class="btn btn-primary" type="button"><i class="fa fa-sign-out"></i> Publish</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>-->
                        </div>
                        <!-- Modal -->

                        <!-- post block -->     
                        <div  ng-show="show_default_hub" >
                            <div class="col-md-12"  ng-repeat="hub in current_post_publish_list">
                                <div class="box latest_talk">
                                    <div class="box-body">
                                        <div class="image default_image">
                                            <span ng-show="hub.front_img_name == ''" ng-bind="hub.profile_initional"></span>
                                            <img class="latest_talkimg" alt="" ng-show="hub.front_img_name != ''" ng-src="{{hub.prof_img}}" />
                                        </div>
                                        <!--<img class="latest_talkimg" alt="" ng-src="{{hub.user_image != '' && '<?php echo base_url() ?>/uploads/profile_image/'+hub.user_image || +'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg" />-->
                                        <div class="latest_content">
                                            <div class="latest_header">                                                
                                                <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}">
                                                    <h6 ng-bind="hub.bussinessname"></h6>&nbsp;                                                    
                                                </a>
                                                <!--<span ng-show="hub.whose_share != '' && hub.whose_share != ' '" ng-class="{class1:hub.whose_share != ''}">  shared </span>
                                                <a ng-show="hub.whose_share != '' && hub.whose_share != ' '" href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}">
                                                    <span ng-bind="hub.whose_share + '\'s'"></span>
                                                </a>
                                                <span ng-show="hub.whose_share != '' && hub.whose_share != ' '"> post</span>
                                                <span class="publish-this" ng-show="hub.shared_for == '4' && hub.whose_share == '' && hub.whose_share == ' '"> publish this</span>-->
                                                <div ng-show="hub.whose_share != '' && hub.whose_share != ' '" class="share">
                                                    <span class="sharedtxt">&nbsp;shared </span>
                                                    <a ng-show="hub.whose_share != '' && hub.whose_share != ' '" href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}">
                                                        <span ng-bind="hub.whose_share + '\'s'"></span>
                                                    </a>
                                                    <span ng-show="hub.whose_share != '' && hub.whose_share != ' '"> post</span>                                                    
                                                </div>
                                                <span class="publish-this" ng-show="hub.shared_for == '4' && hub.whose_share == '' && hub.whose_share == ' '"> publish this</span>
                                                <div class="btn-group confidentiality pull-right">
                                                    <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                    <ul role="menu" class="dropdown-menu pull-right">
                                                        <li ng-show="hub.mid != user_mid"><a href="javascript:void(0)" ng-click="reportPost(hub)">Report Post</a></li>
                                                        <li ng-show="hub.mid == user_mid"><a href="javascript:void(0)" ng-click="deletePost(hub, $event)">Delete Post</a></li> 
                                                        <li ng-show="hub.mid != user_mid"><a href="javascript:void(0)" ng-click="blockUser(hub)">Block User</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <span class="post_time" ng-bind="hub.hub_post_time">9 Min ago</span>
                                            <p ng-bind-html="hub.hbcomment" ng-hide="hub.hbcomment == null"></p>
                                        </div>
                                    </div>
                                    <div class="post_innercont postimg_big clearfix">
                                        <div>
                                            <div class="box-body" ng-show="hub.whose_share != '' && hub.whose_share != ' '">
                                                <div class="image default_image">
                                                    <span ng-show="hub.whose_share_prof_img == ''" ng-bind="hub.whose_share_logoname"></span>
                                                    <img class="latest_talkimg" alt="" ng-show="hub.whose_share_prof_img != ''" ng-src="{{hub.whose_share_prof_img}}" />
                                                </div>
                                                <!--<img class="latest_talkimg" alt="" ng-src="{{hub.user_image != '' && '<?php echo base_url() ?>/uploads/profile_image/'+hub.user_image || +'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg" />-->
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}">
                                                            <h6 ng-bind="hub.whose_share"></h6>                                                   
                                                        </a>                                                       
                                                        <span class="publish-this" ng-show="hub.shared_for == '4' && hub.whose_share == '' && hub.whose_share == ' '"> publish this</span>                                                        
                                                    </div>
                                                    <span class="post_time" ng-bind="hub.hub_post_time">9 Min ago</span>                                                    
                                                </div>
                                            </div>
                                            <div  ng-if="hub.ext != null" class="post_img">
                                                <span ng-if="hub.ext != 'mp4'">
                                                    <span ng-if="hub.news_link != null">
                                                        <a ng-show="hub.type_of_post === '1'" ng-if="hub.news_link != null" href="{{hub.news_link}}" target="_blank">
                                                            <img  ng-class="hub.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{hub.link}}" src
                                                        </a>
                                                    </span>
                                                    <span ng-if="hub.news_link == null">                                                        
                                                        <a href="javascript:void(0)" data-ng-show="hub.shared_for === '4'" ng-click="readmore(hub)">
                                                            <img  ng-class="hub.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{hub.base_url + hub.link}}" src="">
                                                        </a>
                                                        <img  data-ng-show="hub.shared_for !== '4'" class="" alt="" ng-src="{{hub.base_url + hub.link}}" src="">
                                                    </span>
                                                </span>
                                                <div class="video-ply" ng-if="hub.ext == 'mp4'">
                                                    <video controls ng-src="{{hub.base_url + hub.link}}" src=""></video>
                                                </div>
                                            </div>
                                            <div  ng-if="hub.ext == null" class="post_img">
                                                <span ng-show="hub.news_link != null">
                                                    <img ng-show="hub.link != null" ng-class="hub.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{hub.link}}" src="">
                                                </span>
                                            </div>
                                            <div>                                               
                                                <!--<div class="post_innertxt" ng-if="news.author ==''" class = "full-width">-->
                                                <span ng-if="hub.hub_title != null">
                                                    <a ng-show="hub.type_of_post === '1'" ng-if="hub.news_link != null" href="{{hub.news_link}}" target="_blank">
                                                        <i class="fa fa-link link"></i>
                                                    </a>
                                                    <i ng-show="hub.shared_for === '4'" class="fa fa-file-text link"></i>
                                                    <a ng-show="hub.type_of_post === '1'" ng-if="hub.news_link != null" href="{{hub.news_link}}" target="_blank">
                                                        <h5 ng-bind="hub.hub_title" class="hub_title">The Future of Flying Robots For Suistanable Farming</h5>
                                                        <span ng-show="hub.type_of_post === '1'" class="via-icon">
                                                            <span>via </span>
                                                            <img alt="" src="http://www.google.com/s2/favicons?domain={{hub.news_link}}">
                                                        </span>
                                                    </a>

                                                    <h5 ng-show="hub.news_link == null" ng-bind="hub.hub_title">The Future of Flying Robots For Suistanable Farming</h5>
                                                </span>
                                                <!--<p ng-bind-html="hub.content | limitHtml:350"  ng-hide="hub.expand">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>

                                                <p ng-bind-html="hub.content" ng-show="hub.expand">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>-->
                                                <p data-ng-show="hub.shared_for === '4'"><span ng-bind-html="hub.content | limitTo: 200"></span>
                                                    <a href="javascript:void(0)" ng-click="readmore(hub)">read more</a>
                                                </p>
                                                <p data-ng-show="hub.shared_for === '1'" ng-bind-html="hub.content | limitTo: 200"></p>
                                                <a ng-show="hub.type_of_post === '1'" ng-if="hub.news_link != null" href="{{hub.news_link}}" target="_blank">
                                                    read
                                                </a>
                                                <!--                                                <a href="javascript:void(0)" ng-hide="hub.expand" ng-click="showHubDetails(hub, 'hub')" ng-hide="true">...</a>-->
                                                <!-- <a href="javascript:void(0)" ng-show="hub.hub_title != null" ng-click="showHubDetails(hub, 'hub')">...</a>-->
                                            </div>                                            
                                        </div>
                                    </div>
                                    <div class="box-footer post_bottom clearfix">
                                        <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a href="javascript:void(0)" ng-click="shareHub(hub)"><i class="fa fa-share-alt"></i> Share</a></li>
                                                <li><a href="javascript:void(0)" ng-click="open_comment_Animation(hub)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                <li><a href="javascript:void(0)" ng-class="{'like': hub.like != 0}" ng-click="feedback(hub)"><i ng-class="{'fa fa-heart' :hub.like != 0, 'fa fa-heart-o' :hub.like == 0}" class="fa"></i> Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.total_share"> 23 </span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a href="javascript:void(0)" ng-click="commentsAnimation(hub)"><span ng-bind="hub.comment_count">45</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.likes">23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="comment_box" ng-show="hub.open_comment_box">
                                            <div class="comment_default"> 
                                                <span ng-show="hub.front_img_name == ''" ng-bind="user_initial"></span>
                                                <img class="comment_img" ng-show="hub.front_img_name != ''" ng-src="{{hub.path + hub.front_img_name}}" />
                                            </div>
                                            <!--<img class="comment_img" ng-src="{{hub.prof_img != '' && hub.prof_img|| 'http://arlians.com/assets/images/profile-img.png'}}" />-->
                                            <input type="text" ng-model="hub.comment" class="form-control"  placeholder="Write your comment">
                                            <button class="btn btn_publish" ng-click="postHubComments(hub)">Post</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-11 col-md-offset-1" ng-show="hub.show_comments" ng-repeat="comment in hub.comments">
                                    <div class="box latest_talk comment_reply">
                                        <div class="box-body">
                                            <div class="image default_image">
                                                <span ng-show="comment.image == null" ng-bind="comment.com_initial_name"></span>
                                                <img ng-show="comment.image != null" ng-src="{{comment.image_path + comment.image}}" src="<?php echo base_url() ?>assets_phase2/images/post-userimg.jpg" alt="" class="sub_img">
                                            </div>
                                            <div class="latest_content">
                                                <div class="mCustomScrollbar">
                                                    <div class="latest_header clearfix">
                                                        <div class="btn-group confidentiality pull-right">
                                                            <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                            <ul role="menu" class="dropdown-menu pull-right">                                                               
                                                                <li ng-show="comment.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteComment(hub, comment, $event)">Delete Post</a></li>                               
                                                            </ul>
                                                        </div>
                                                        <a href="<?php echo base_url() ?>memberprofile/profile/{{comment.buisnesname + '/' + comment.mid}}">
                                                            <h6 ng-bind="comment.buisnesname">Mike Johns</h6>
                                                        </a>
                                                    </div>
<!--                                                    <span class="post_time" ng-bind="comment.hub_post_time">9 Min ago</span>  -->
                                                    <p ng-bind="comment.comment_view">Thats a really nice job cant wait to see it </p>                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Share Modal -->
                        <div id="myShareModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>  
                                    </div>
                                    <div class="modal-body clearfix">
                                        <textarea ng-model="shareObj.hbComment" class="share_input" placeholder="Say something about this..."></textarea>
                                        <div ng-bind="shareObj.hub_title"></div>
                                        <span ng-if="shareObj.ext != 'mp4'">                                            
                                            <span ng-if="shareObj.news_link != null">
                                                <!--<img  ng-class="shareObj.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{shareObj.link}}" src="">-->
                                                <img class="" alt="" ng-src="{{shareObj.link}}" src="">
                                            </span>
                                            <span ng-if="shareObj.news_link == null">
                                                <img ng-show="shareObj.ext != null" ng-class="shareObj.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{shareObj.base_url + shareObj.link}}" src="<?php echo base_url(); ?>assets_phase2/images/post-img.jpg">
                                            </span>
                                        </span>
                                <!--<img ng-show="shareObj.link != null" ng-model="shareObj.link" src="{{shareObj.base_url + shareObj.link}}"></img>-->
                                        <div ng-bind-html="shareObj.content"></div>
                                    </div>
                                    <div class="modal-footer">
                                        <!--                                        <button type="button" class="btn btn-primary" ng-click="saveShareComments(shareObj)" >Post</button>-->
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Share post With &nbsp;<span class="caret"></span></button>
                                        <ul class="dropdown-menu  pull-right">
                                            <li><a href="javascript:void(0);" ng-click="saveShareComments(list, shareObj)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                       
                                        </ul>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="myShareNewsModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <!--                                        <div class="btn-group">
                                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                        Share post With &nbsp;<span class="caret"></span>
                                                                                    </button>
                                                                                    <ul class="dropdown-menu">
                                                                                        <li><a href="#">Action</a></li>                                          
                                                                                    </ul>
                                                                                </div>-->

                                    </div>
                                    <div class="modal-body">
                                        <textarea ng-model="shareNewsObj.hbComment" class="share_input" placeholder="Say something about this..."></textarea>
                                        <div ng-bind="shareNewsObj.hub_title"></div>
                                        <span ng-if="shareNewsObj.link != null">
                                            <img ng-show="shareNewsObj.link != null" class="post_img" alt="" ng-src="{{shareNewsObj.link}}" src="<?php echo base_url(); ?>assets_phase2/images/post-img.jpg">
                                        </span>                                        
                                <!--<img ng-show="shareObj.link != null" ng-model="shareObj.link" src="{{shareObj.base_url + shareObj.link}}"></img>-->
                                        <div ng-bind-html="shareNewsObj.content"></div>

                                    </div>
                                    <div class="modal-footer">
<!--                                        <button type="button" class="btn btn-primary" ng-click="saveShareNewsComments(shareNewsObj)" >Post<span class="caret"></span></button>-->
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Share post With &nbsp;<span class="caret"></span></button>
                                        <ul class="dropdown-menu  pull-right">
                                            <li><a href="javascript:void(0);" ng-click="saveShareNewsComments(list, shareNewsObj)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                       
                                        </ul>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div> 
                        </div>
                        <div class="col-md-12 share_box" ng-class="{'sharepost_block':hub.whose_share != '' && hub.whose_share != ' ' }" ng-repeat="hub in hub_list" ng-show="show_default_hub">
                            <div  ng-if="hub.type == 'hub'" >
                                <div class="box latest_talk">
                                    <div class="box-body">
                                        <div class="image default_image">
                                            <span ng-show="hub.prof_img == ''" ng-bind="hub.profile_initional"></span>
                                            <img class="latest_talkimg" alt="" ng-show="hub.prof_img != ''" ng-src="{{hub.prof_img}}" />
                                        </div>
                                        <!--<img class="latest_talkimg" alt="" ng-src="{{hub.user_image != '' && '<?php echo base_url() ?>/uploads/profile_image/'+hub.user_image || +'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg" />-->

                                        <div class="latest_content">
                                            <div class="latest_header">
                                                <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}">
                                                    <h6 ng-bind="hub.bussinessname"></h6>                                                  
                                                </a>
                                                <!-- data comments nilanjan for show shared by  -->
                                                <div ng-show="hub.whose_share != '' && hub.whose_share != ' '" class="share">
                                                    <span class="sharedtxt">&nbsp;shared </span>
                                                    <a ng-show="hub.whose_share != '' && hub.whose_share != ' '" href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}">
                                                        <span ng-bind="hub.whose_share + '\'s'"></span>
                                                    </a>
                                                    <span ng-show="hub.whose_share != '' && hub.whose_share != ' '"> post</span>                                                    
                                                </div>
                                                <!-- for news share -->
                                                 <div ng-show="hub.type_of_post == '3'" class="share">
                                                    <span class="sharedtxt">&nbsp;shared </span>
                                                    <a ng-show="hub.whose_share != '' && hub.whose_share != ' '" href="<?php echo base_url() ?>memberprofile/profile/{{hub.whose_share + '/' + hub.whose_share_id}}">
                                                        <span ng-bind="hub.whose_share + '\'s'"></span>
                                                    </a>
                                                    <span ng-show="hub.whose_share != '' && hub.whose_share != ' '"> post</span>                                                    
                                                </div>

                                                <span class="publish-this" ng-show="hub.shared_for == 4"> &nbsp;publish this</span>
                                                <div class="like" ng-show="hub.not_in_network_like != null">
                                                    <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.not_in_network_like.name + '/' + hub.not_in_network_like.liked_mid}}">
                                                        <span class="sharedtxt" ng-bind="hub.not_in_network_like.name + '\'s'"></span>
                                                    </a>
                                                    <span> like this</span>
                                                </div><br>
                                                <div class="comment"  ng-show="hub.not_in_network_comment != null">
                                                    <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.not_in_network_comment.name + '/' + hub.not_in_network_comment.comment_mid}}">
                                                        <span ng-bind="hub.not_in_network_comment.name + '\'s'"></span>
                                                    </a>
                                                    <span class="sharedtxt"> comment on it</span>
                                                </div>

                                            </div>
                                            <div class="btn-group confidentiality pull-right">
                                                <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                <ul role="menu" class="dropdown-menu pull-right">
                                                    <li ng-show="hub.mid != user_mid"><a href="javascript:void(0)" ng-click="reportPost(hub)">Report Post</a></li>
                                                    <li ng-show="hub.mid == user_mid"><a href="javascript:void(0)" ng-click="deletePost(hub, $event)">Delete Post</a></li>                               
                                                    <li ng-show="hub.mid != user_mid"><a href="javascript:void(0)" ng-click="blockUser(hub)">Block User</a></li>
                                                </ul>
                                            </div>
                                            <span class="post_time" ng-show="hub.whose_share == '' && hub.whose_share == ' ' && hub.not_in_network_like == null && hub.not_in_network_comment == null" ng-bind="hub.hub_post_time">9 Min ago</span>
                                            <p class="update_txt" ng-bind-html="hub.hbcomment" ng-hide="hub.hbcomment == null"></p>
                                        </div>
                                    </div>
                                    <div class="post_innercont postimg_big clearfix">
                                        <!-- <div class="share_innerblock" ng-class="{sharepost_block:hub.whose_share != ''}">-->
                                        <div class="clearfix" ng-class="{'share_innerblock':hub.whose_share != '' && hub.whose_share != ' '}">
                                            <div class="box-body" ng-show="hub.whose_share != '' && hub.whose_share != ' '">
                                                <div class="image default_image">
                                                    <span ng-show="hub.whose_share_prof_img == ''" ng-bind="hub.whose_share_logoname"></span>
                                                    <img class="latest_talkimg" alt="" ng-show="hub.whose_share_prof_img != ''" ng-src="{{hub.whose_share_prof_img_path + hub.whose_share_prof_img}}" />
                                                </div>
                                                <!--<img class="latest_talkimg" alt="" ng-src="{{hub.user_image != '' && '<?php echo base_url() ?>/uploads/profile_image/'+hub.user_image || +'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg" />-->

                                                <div class="latest_content">
                                                    <div class="latest_header">
                                                        <a href="<?php echo base_url() ?>memberprofile/profile/{{hub.bussinessname + '/' + hub.mid}}">
                                                            <h6 ng-bind="hub.whose_share"></h6>                                                   
                                                        </a>                                                       
                                                        <span class="publish-this" ng-show="hub.shared_for == '4' && hub.whose_share != '' && hub.whose_share != ' '"> publish this</span>                                                        
                                                    </div>
                                                    <span class="post_time" ng-bind="hub.hub_post_time">9 Min ago</span>                                                    
                                                </div>
                                            </div>
                                            <div  ng-if="hub.ext != null" class="post_img">
                                                <span ng-if="hub.ext != 'mp4'">
                                                    <span ng-show="hub.news_link != null">
                                                        <span ng-show="hub.link != ''">
                                                            <a ng-show="hub.type_of_post === '1'" ng-if="hub.news_link != null" href="{{hub.news_link}}" target="_blank">
                                                                <img  ng-class="hub.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{hub.link}}" src="<?php echo base_url(); ?>assets_phase2/images/post-img.jpg">
                                                            </a>
                                                        </span>
                                                    </span>
                                                    <span ng-show="hub.news_link == null">
                                                        <a href="javascript:void(0)" data-ng-show="hub.shared_for === '4'" ng-click="readmore(hub)">
                                                            <img  ng-class="hub.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{hub.base_url + hub.link}}" src="">
                                                        </a>
                                                        <img  data-ng-show="hub.shared_for !== '4'" class="" alt="" ng-src="{{hub.base_url + hub.link}}" src="">
                                                    </span>
                                                </span>
                                                <div class="video-ply" ng-if="hub.ext == 'mp4'">
                                                    <video controls ng-src="{{hub.base_url + hub.link}}" src=""></video>
                                                </div>
                                            </div>
                                            <div  ng-if="hub.ext == null" class="post_img">
                                                <span ng-show="hub.news_link != null">
                                                    <span ng-show="hub.link != ''">
                                                        <img  ng-class="hub.shared_for == 4 ? 'post_img':''" class="" alt="" ng-src="{{hub.link}}" src="">
                                                    </span>
                                                </span>
                                            </div>
                                            <div>                                                
                                                <!--<div class="post_innertxt" ng-if="news.author ==''" class = "full-width">-->
                                                <span ng-if="hub.hub_title != ''">
                                                    <a ng-show="hub.type_of_post === '1'" ng-if="hub.news_link != null" href="{{hub.news_link}}" target="_blank">
                                                        <i class="fa fa-link link"></i>
                                                    </a>
                                                    <i ng-show="hub.shared_for === '4'" class="fa fa-file-text link"></i>
                                                    <a ng-if="hub.news_link != null" href="{{hub.news_link}}" target="_blank">                                                        
                                                        <h5 ng-bind="hub.hub_title" class="hub_title">The Future of Flying Robots For Suistanable Farming</h5>
                                                        <span ng-show="hub.type_of_post === '1'" class="via-icon">
                                                            <span>via </span>
                                                            <img alt="" src="http://www.google.com/s2/favicons?domain={{hub.news_link}}">
                                                        </span>
                                                    </a>
                                                    <h5 ng-show="hub.news_link == null" ng-bind="hub.hub_title">The Future of Flying Robots For Suistanable Farming</h5>
                                                </span>
                                                <p data-ng-show="hub.shared_for === '4'"><span ng-bind-html="hub.content |limitTo: 200"></span>
                                                    <a href="javascript:void(0)" ng-click="readmore(hub)">read more</a>
                                                </p>
                                                <p data-ng-show="hub.shared_for === '1'" ng-bind-html="hub.content |limitTo: 200"></p>
                                                <a ng-show="hub.type_of_post === '1'" ng-if="hub.news_link != null" href="{{hub.news_link}}" target="_blank">
                                                    read
                                                </a>
<!--                                                <p ng-bind-html="hub.content" ng-show="hub.expand">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>-->
                                                <!--                                                <a href="javascript:void(0)" ng-hide="hub.expand" ng-click="showHubDetails(hub, 'hub')" ng-hide="true">...</a>-->
                                                <!-- <a href="javascript:void(0)" ng-show="hub.hub_title != null" ng-click="showHubDetails(hub, 'hub')">...</a>-->
                                            </div>                                            
                                            <!-- <div ng-class="{'post_innertxt' : hub.image != ''}"> -->
                                        </div>
                                    </div>
                                    <div class="box-footer post_bottom clearfix">
                                        <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a href="javascript:void(0)" ng-click="shareHub(hub)"><i class="fa fa-share-alt"></i> Share</a></li>
                                                <li><a href="javascript:void(0)" ng-click="open_comment_Animation(hub)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                <li><a href="javascript:void(0)" ng-class="{'like': hub.like != 0}" ng-click="feedback(hub)"><i ng-class="{'fa fa-heart' :hub.like != 0, 'fa fa-heart-o' :hub.like == 0}" class="fa"></i> Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.total_share"> 23 </span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a href="javascript:void(0)" ng-click="commentsAnimation(hub)"><span ng-bind="hub.comment_count">45</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.likes">23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>

                                        <div class="comment_box" ng-show="hub.open_comment_box">
                                            <div class="comment_default"> 
                                                <span ng-show="front_img_name == ''" ng-bind="user_initial"></span>
                                                <img class="comment_img" ng-show="front_img_name != ''" ng-src="{{path + front_img_name}}" />
                                            </div>
                                            <!--                                    
                                                    <img class="comment_img" ng-src="{{hub.prof_img != '' && hub.prof_img|| 'http://arlians.com/assets/images/profile-img.png'}}" />-->
                                            <form>  
                                                <input type="text" ng-model="hub.comment" class="form-control"  placeholder="Write your comment">
                                                <button class="btn btn_publish" ng-click="postHubComments(hub)">Post</button>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-11 col-md-offset-1" ng-show="hub.show_comments" ng-repeat="comment in hub.comments">
                                    <div class="box latest_talk comment_reply">
                                        <div class="box-body">
                                            <div class="image default_image">
                                                <span ng-show="comment.image == null" ng-bind="comment.com_initial_name"></span>
                                                <img ng-show="comment.image != null" ng-src="{{comment.image_path + comment.image}}" src="<?php echo base_url() ?>assets_phase2/images/post-userimg.jpg" alt="" class="sub_img">
                                            </div>
                                            <div class="latest_content">
                                                <div class="mCustomScrollbar">
                                                    <div class="latest_header clearfix">
                                                        <div class="btn-group confidentiality pull-right">
                                                            <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                            <ul role="menu" class="dropdown-menu pull-right">                                                               
                                                                <li ng-show="comment.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteComment(hub, comment, $event)">Delete Post</a></li>                               
                                                            </ul>
                                                        </div>
<!--                                                        <h6>Mike Johns <span>verified talker</span></h6>-->
                                                        <a href="<?php echo base_url() ?>memberprofile/profile/{{comment.buisnesname + '/' + comment.mid}}">
                                                            <h6 ng-bind="comment.buisnesname">Mike Johns</h6>
                                                        </a>
                                                    </div>
                                                    <span class="post_time" ng-bind="comment.hub_post_time">9 Min ago</span>  
                                                    <p ng-bind="comment.comment_view">Thats a really nice job cant wait to see it </p>                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box latest_talk subject"  ng-if="hub.type == 'watson'">
                                <div class="box-body">
                                    <!--                            <a href="javascript:void(0)" ng-href="hub.url">
                                                                    <img class="latest_talkimg" alt="" ng-if="hub.image != '' || hub.image != undefined" ng-bind="hub.image" src="images/post-userimg.jpg">
                                                                </a>-->
                                    <div class="latest_content">
                                        <div class="mCustomScrollbar">
                                            <div class="latest_header clearfix">
                                                <h6 ng-if="hub.author != ''"><a href="javascript:void(0)" ng-href="{{hub.url}}"><span ng-bind="hub.author">Mike Johns</span></a></h6>
                                                <!--                                    <div class="btn-group confidentiality pull-right">
                                                                                        <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                                                        <ul role="menu" class="dropdown-menu pull-right">
                                                                                            <li><a href="#">Add new event</a></li>
                                                                                            <li><a href="#">Clear events</a></li>
                                                                                            <li class="divider"></li>
                                                                                            <li><a href="#">View calendar</a></li>
                                                                                        </ul>
                                                                                    </div>-->
                                            </div>
            <!--                                <span class="post_time">9 Min ago</span>  -->
            <!--                                <p ng-bind="hub.">Watch these adorable robots play follow the leader:</p>                  -->
                                        </div>
                                    </div>
                                </div>
                                <div class="post_innercont clearfix">
                                    <a href="javascript:void(0)" target="_blank" ng-href="{{hub.url}}">
                                        <img class="post_img" alt="" ng-show="hub.image != '' || hub.image != undefined" ng-src="{{hub.image}}" src="<?php echo base_url() ?>assets_phase2/images/post-userimg.jpg">
                                    </a>
                                    <div class="post_innertxt">
                                        <h5 ng-bind="hub.title">The Future of Flying Robots For Suistanable Farming</h5>
                                        <p ng-bind-html="hub.text | limitTo: 300">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>
                                        <!--                                        <a href="javascript:void(0)" ng-click="showHubDetails(hub, 'watson')">...</a>-->
                                    </div>
                                </div>
                                <!--                        <div class="box-footer post_bottom clearfix">
                                                            <ul class="latest_footerlink pull-left">
                                                                <li><a href="#"><i class="fa fa-share-alt"></i> Share</a></li>
                                                                <li><a href="#"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                                <li><a href="#"><i class="fa fa-heart-o"></i> Like</a></li>
                                                            </ul>
                                
                                                            <ul class="latest_footerlink_right pull-right">
                                                                <li><a href="#">43 <i class="fa fa-share-alt"></i></a></li>
                                                                <li><a href="#">45 <i class="fa  fa-comment-o"></i> </a></li>
                                                                <li><a href="#">23 <i class="fa  fa-heart-o"></i></a></li>
                                                            </ul>
                                                        </div> -->
                            </div>
                            <div calss="subject" ng-if="hub.type == 'bing'">
                                <div class="box latest_talk"  >
                                    <div class="box-body">
                                        <div class="image news_icon default_image">
                                           <!-- <img class="latest_talkimg" alt="" src="<?php echo base_url(); ?>assets_phase2/images/news-icon.png">-->
                                            <img class="latest_talkimg" alt="" src="http://www.google.com/s2/favicons?domain={{hub.url}}">
                                        </div>
                                     <!-- <img class="latest_talkimg" alt="" src="images/post-userimg.jpg">-->
                                        <div class="latest_content">
                                            <div >
                                                <div class="latest_header">
                                                    <a href="javascript:void(0);" target="_blank" ng-href="{{hub.url}}">
                                                        <h6 ng-bind="hub.source"></h6>
                                                    </a> 

                                                </div>
                                                <div class="btn-group confidentiality pull-right">
                                                    <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                    <ul role="menu" class="dropdown-menu pull-right">
                                                        <li><a href="javascript:void(0)" ng-click="hideNews(hub, $event)">Hide</a></li>
                                                    </ul>
                                                </div>
                                                <span class="post_time" ng-bind="hub.date">9 Min ago</span>
    <!--                                <p>Watch these adorable robots play follow the leader:</p>                  -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="post_innercont clearfix">
                                        <div class="mCustomScrollbar">
                                            <img class="post_img" alt="" ng-show="hub.newsimg != null" ng-src="{{hub.newsimg}}" src="">
                                            <div class="">
                                                <a target="_blank" ng-href="{{hub.url}}"><h5 ng-bind="hub.title">The Future of Flying Robots For Suistanable Farming</h5></a>
                                                <p  ng-hide="hub.expand"><span ng-bind-html="hub.desc"></span><a target="_blank" ng-href="{{hub.url}}">READ MORE</a></p>
        <!--                                        <p ng-bind-html="hub.desc | limitTo: 150" ng-hide="hub.expand">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>-->
                                                <p ng-bind-html="hub.desc" ng-show="hub.expand">At his lab at the University of Pennsylvania, Vijay Kumar and his team have created autonomous aerial robots inspired by honeybees. Their latest breakthrough: Precision Farming,</p>
                                                <!--                                        <a href="javascript:void(0)" ng-hide="hub.expand" ng-click="showHubDetails(hub)">...</a>-->
                                                <a href="javascript:void(0)" ng-show="hub.expand" ng-click="showHubDetails(hub)">
                                                    <i class="fa fa-reply"></i>
                                                </a>
                                                <!--<a href="javascript:void(0)" ng-click="showHubDetails(hub, 'bing')">...</a>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-footer post_bottom clearfix">
                                        <div class="comment_links clearfix">
                                            <ul class="latest_footerlink pull-left">
                                                <li><a href="javascript:void(0)" ng-click="shareNewsHub(hub)"><i class="fa fa-share-alt"></i> Share</a></li>
                                                <li><a href="javascript:void(0)" ng-click="open_comment_Animation(hub)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                <li><a href="javascript:void(0)" ng-class="{'like': hub.like}" ng-click="feedbackNews(hub)"><i ng-class="{'fa fa-heart' :hub.like, 'fa fa-heart-o' :!hub.like}" class="fa"></i> Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right pull-right">
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.share_count"></span> &nbsp;<i class="fa fa-share-alt"></i></a></li>
                                                <li><a href="javascript:void(0)" ng-click="commentsAnimation(hub)"><span ng-bind="hub.comment_count"></span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.like_count"> </span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="comment_box" ng-show="hub.open_comment_box">
                                            <div class="comment_default"> 
                                                <span ng-show="front_img_name == ''" ng-bind="user_initial"></span>
                                                <img class="comment_img" ng-show="front_img_name != ''" ng-src="{{path + front_img_name}}" />
                                            </div>
                                            <form>
                                                <input type="text" ng-model="hub.comment" class="form-control"  placeholder="Write your comment">
                                                <button class="btn btn_publish" ng-click="postNewsComments(hub)">Post</button>
                                            </form>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-11 col-md-offset-1" ng-show="hub.show_comments" ng-repeat="comment in hub.comments">
                                    <div class="box latest_talk comment_reply">
                                        <div class="box-body">
                                            <div class="image default_image">
                                                <span ng-show="comment.image == null" ng-bind="comment.com_initial_name"></span>
                                                <img ng-show="comment.image != null" ng-src="{{comment.image_path + comment.image}}" src="<?php echo base_url() ?>assets_phase2/images/post-userimg.jpg" alt="" class="sub_img">
                                            </div>
                                            <div class="latest_content">

                                                <div class="mCustomScrollbar">
                                                    <div class="latest_header clearfix">
                                                        <div class="btn-group confidentiality pull-right">
                                                            <button data-toggle="dropdown" class="btn  btn-sm dropdown-toggle"> <i class="fa fa-chevron-down"></i></button>
                                                            <ul role="menu" class="dropdown-menu pull-right">                                                               
                                                                <li ng-show="comment.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteComment(hub, comment, $event)">Delete Post</a></li>                               
                                                            </ul>
                                                        </div>
<!--                                                        <h6>Mike Johns <span>verified talker</span></h6>-->
                                                        <h6 ng-bind="comment.buisnesname">Mike Johns</h6>
                                                    </div>
<!--                                                    <span class="post_time">9 Min ago</span>  -->
                                                    <p ng-bind="comment.comment_view">Thats a really nice job cant wait to see it </p>                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <!-- Modal Crop Image-->
                <div class="modal fade" id="publish_details" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Publish</h4>
                            </div>
                            <div class="modal-body">
                                <div class="publish_second">
                                    <br/>  
                                    <div class="publish_toppart">
                                        <img ng-show="hub.link != null" ng-src="{{hub.base_url + hub.link}}"  alt="">
                                        <div class="publish_namecontainer">
                                            <div>
                                                <span ng-show="hub.prof_img == ''" class="publish_avator" ng-bind="hub.profile_initional"></span>                                   
                                                <img ng-show="hub.prof_img != ''" ng-src="{{hub.prof_img}}" class="publish_avator" alt="User Image">
                                            </div>
                <!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                                            <div class="pulish_heading">
                                                <span class="publis_name" ng-bind="hub.bussinessname">Digitalweb</span>
                <!--                                <p class="publish_subtxt">Production of modern website</p>-->
                                            </div>
                                            <div class="publish_adduser" ng-show="connected">
                                                <i class="fa fa-user-plus" ng-click="connectionLogic(hub.hid)"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="publish_actionarea">
                                        <h5 ng-bind="hub.hub_title">Write your headline</h5>

                                        <div class="publishfooter clearfix">
                                            <ul class="latest_footerlink pull-left publishfooterlink">
                                                <li><a href="javascript:void(0)" ng-click="shareHub(hub)"><i class="fa fa-share-alt"></i> Share</a></li>
                                                <li><a href="javascript:void(0)" ng-click="open_comment_Animation(hub)"><i class="fa  fa-comment-o"></i> Comments</a></li>
                                                <li><a href="javascript:void(0)" ng-class="{'like': hub.like != 0}" ng-click="feedback(hub)"><i ng-class="{'fa fa-heart' :hub.like != 0, 'fa fa-heart-o' :hub.like == 0}" class="fa"></i> Like</a></li>
                                            </ul>
                                            <ul class="latest_footerlink_right">
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.total_share"> 23 </span> <i class="fa fa-share-alt"></i> </a></li>
                                                <li><a href="javascript:void(0)" ng-click="commentsAnimation(hub)"><span ng-bind="hub.comment_count">45</span> <i class="fa  fa-comment-o"></i> </a></li>
                                                <li><a href="javascript:void(0)"><span ng-bind="hub.likes">23</span> <i class="fa  fa-heart-o"></i></a></li>
                                            </ul>
                                            <p class="pub_date">Published the <span ng-bind="hub.hub_post_time">23/05/16</span></p>


                                        </div>
                                    </div>
                                    <div class="publish_editor_txt  box-body">
                                        <div  ng-bind-html="hub.content">

                                        </div>
                                        <div class="pub_comment_section" ng-show="hub.open_comment_box">
                                            <textarea rows="2" class="publish_cmntpart" ng-model="hub.comment"></textarea>
                                            <button class="btn btn_publish  pull-right" ng-click="postHubComments(hub)">post</button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer">                                                                           
                                <button class="btn btn_publish pull-right" data-dismiss="modal">Back</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box publish_section clearfix" ng-show="show_publish">
                    <div class="publish_toppart">
                        <!--                        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                          Launch demo modal
                        </button>-->

                        <!-- Modal Crop Image-->
                        <div class="modal fade" id="crop_publish_image" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Upload an Image</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container-crop">
                                            <div class="imageBox">
                                                <div class="thumbBox"></div>
                                                <div class="spinner" style="display: none">Loading...</div>
                                            </div>
                                            <div class="action">

                                            </div>
                                            <div class="cropped"></div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <span class="note">image size 450 x 250  for best result</span>
                                        <input type="file" id="file">                                               
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                        
                                        <button type="button" id="btnZoomIn" class="btn btn-primary">+</button>
                                        <button type="button" id="btnZoomOut" class="btn btn-primary">-</button>
                                        <button type="button" id="btnCrop" class="btn btn-primary">Done</button>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        <input type="file" id="upload_img_for_post" onchange="readURLPostImage(this);"/>-->
                        <i class="fa fa-camera change_photo" id="publish_img_icon" style="cursor:pointer" onclick="open_publish_crop_img_modal()"></i>
                        <p class="pubimg_txt">Add an image to your publication<br><span>Best if 450 x 250pixel</span></p>
<!--                        <input type="file" ng-model="image" ng-change="readURLPostImage(this)"><i class="fa fa-camera change_photos"></i></input>-->

                        <div class="publish_namecontainer" style="display:none">
                            <img ng-src="{{front_img_name != '' && path + front_img_name|| 'http://arlians.com/assets/images/profile-img.png'}}" class="publish_avator" src="<?php echo base_url(); ?>assets_phase2/images/admin-user.jpg"  alt="User Image">
<!--                            <img src="<?php echo base_url(); ?>assets/images/publish_avator.jpg" class="publish_avator" alt="">-->
                            <div class="pulish_heading">
                                <span class="publis_name" ng-bind="bussinessname">Digitalweb</span>
                                <p class="publish_subtxt">Production of modern website</p>
                            </div>
                        </div>
                        <!--                        <div></div>-->
                        <img id="hub_publish_default_img" src=""  alt="">
                        <img id="hub_publish_preview_img" class="hub_publish_preview_img" src="" alt="">
                    </div>



                    <input type="text" ng-model="publish_header" class="write_headline" placeholder="Write your headline" />

                    <!--                    <h5>Write your headline</h5>-->

                    <div class="publish_editor">
                        <div text-angular ng-model="content_about_publish" name="demo-editor" ta-text-editor-class="border-around" ta-html-editor-class="border-around"></div>
<!--                        <textarea id="txtEditor"></textarea> -->
                    </div>
<!--                    <div class="post-header add_tags"><i class="fa fa-tag"></i>Add tags</div>   
                    <div class="bs-example">
                        <input type="text" value="tag name" data-role="tagsinput" />
                    </div>-->
                    <!--<div class="clearfix">
                        <span style="color:#345672" ng-bind="show_post_selected">Hello world</span>
                        <div class="btn-group post_footerdrpbtn">
                            <i class="fa fa-gears pull-left"></i>
                            <button aria-expanded="false" data-toggle="dropdown" class="btn  btn-sm dropdown-toggle">Share with</button>
                            <ul role="menu" class="dropdown-menu pull-right">                                        
                                <li><a href="javascript:void(0);" ng-click="select_post(list)" ng-repeat='list in share_with_list' ng-bind="list.name">Add new event</a></li>                                
                            </ul>
                        </div>
                    </div>-->

                    <div class="editor_btn">
                        <button class="btn btn_publish pull-right" ng-click="showDefault()">Back</button>
                        <button class="btn btn_publish pull-right" ng-click="savePublish()">Publish</button>
                    </div>

                </div>

            </div>
            <div class="col-md-6 col-sm-6 tab-sticky-scroll">
                <div class="tab_s mCustomScrollbar"> 
                   <!-- <img class="tab-img" ng-show="show_default_community"  src="<?php echo base_url(); ?>assets_phase2/images/tab-img.jpg" alt="">-->


                    <!-- <div class="article_section">
                        <div class="article_toppart">
                            <h2>Lorem ipsum doler sit amet dummy text</h2>
                            <span class="article_subheading">By Textwriter</span>                     
                        </div>

                        <div class="article_body box box-body">
                            <div class="article_icon"><i class="fa fa-user"></i></div>
                            <span class="article_quote">"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisi urna, ullamcorper ac ligula vitae"</span>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisi urna, ullamcorper ac ligula vitae, sollicitudin rhoncus eros? Pellentesque a accumsan leo. Nulla facilisi. Nullam odio nisi, feugiat ac magna eget, accumsan dictum urna. Nunc at leo dolor. Etiam eleifend, lacus eu feugiat venenatis, velit nisl malesuada sapien, a efficitur nulla eros ac mauris? Duis non leo vitae est tristique sodales ut sed metus. Nulla facilisi. Curabitur blandit, lacus id scelerisque molestie, purus magna ultricies tortor, in consequat tellus velit non sapien. Praesent justo metus; venenatis et ligula in, viverra aliquet purus. Cras ullamcorper purus in purus rutrum porttitor. Vivamus quis dui ex. Nullam congue arcu ut nisi euismod porttitor. Integer turpis felis, elementum facilisis elit sodales, interdum vestibulum neque. Integer eu nulla tellus.</p>
                        </div>               

                    </div>  -->

                    <div class="details_tab">
                        <div id="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1 clearfix">
                                <li><i class="fa  fa-refresh"></i><br>Latest Talks</li>
                                <li><i class="fa  fa-cloud-download"></i><br>My Talks</li>
                                <li><i class="fa  fa-th"></i><br>Top Talks</li>
                                <li><i class="fa  fa-users"></i><br>Start a Talk</li>
                                <li><i class="fa  fa-list"></i><br>More</li>
                            </ul>
                            <div class="modal fade" id="participateModal" tabindex="-1" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" ng-bind="talk.bussiness_name"></h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>This talk is private.So you need to take permission before participate&hellip;</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="button" class="btn btn-primary" ng-click="sendRequest()">Send Request</button>
                                        </div>
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                            <div class="resp-tabs-container hor_1">
                                <div>

                                    <div ng-repeat="talk in fetch_Latest_Talk" >

                                        <div class="box latest_talk" ng-click="latest_talk_toggle_animation(talk)" ng-show="talk.show_latest_talk_section" >

                                            <div class="box-body" >

                                                <div class="image default_image">
                                                    <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                    <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">

                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6 ng-bind="talk.member_name"></h6>                                           

                                                        <a href="#" class="user_likes" ><i class="fa fa-user" ng-bind="talk.total_comment"></i></a>											

                                                    </div>
                                                    <p ><strong ng-bind="talk.title"></strong></p>
                                                    <p ng-bind="talk.content"></p> 
                                                </div>


                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)" id="participate_button" class="toggle" ng-click="open_comment_Animation_latestTalk(talk)">Participate</a></li>
                                                    <li><a href="javascript:void(0)" ng-class="{'com_like': talk.com_like != 0 }" ng-click="feedback_latest_talk(talk)"><i ng-class="{'fa fa-heart' :talk.com_like != 0, 'fa fa-heart-o' :talk.com_like == 0}" class="fa"></i>Like </a></li>
                                                    <li><a href="javascript:void(0)"  ng-click="open_recommend_Animation_latestTalk(talk)">Recommend</a></li>
                                                </ul>

                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)" ng-click="toggleComment(talk)" ><span ng-bind=""></span><i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)" ><span ng-bind="talk.total_like"></span><i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li>
                                                        <div class="btn-group gear-btn">
                                                            <button type="button" class="dropdown-toggle settings_icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-gear"></i>								
                                                            </button>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li ng-show="talk.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteParticipateTalk(talk, $event)">Delete</a></li>
                                                                <li><a href="#">Hide</a></li>

                                                            </ul>
                                                        </div>
                                                    </li>

                                                </ul>

                                            </div> 


                                        </div> 


                                        <!-- comment on latest talk start -->

                                        <div class="modal fade" id="recommend" tabindex="-1" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Recommend Talk</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form novalidate name="recommend">


                                                            <input type="text" id="demo-input-custom-limits_recommend" ng-model="business_name" ng-keyup="businessname_auto_search(business_name)" class="form-control fld"  placeholder="To: [auto fill by user name or email adress]">
                                                            <div class="loading" style="display:none;">searching...</div>
                                                            <div class="no_result" style="display:none;">no result found...</div>

                                                            <div ><a href="#"  ng-repeat="business in savedBusinessName" >
                                                                    <span class="tag_list" id="close"  ng-bind="business.bussinessname"></span><i class="close_taglist" ng-click="closeBusinessname(business)">x</i></a>
                                                            </div>
                                                            <ul id="business_name_recommend"><li ng-repeat="business in businessname" class="dropdown_list_class" id="dropdown_list_recommend" ng-click="saveBusiness_name(business)" ng-bind="business.bussinessname"></li></ul>
                                                            <textarea name="text" id="txt_msg" class="recomnd_txtarea" ng-model="message" placeholder="write talk"></textarea>
                                                            <!--<ul id="tag"><li  ng-repeat="tag in sugessted_tags track by $index" class="dropdown_list_class" id="dropdown_list" ng-click="saveSuggestedTag(tag)" ng-bind="tag.tag_name"></li></ul>-->

<!-- </div><textarea name="text"  class="recomnd_txtarea" ng-model="msg.talk_text" placeholder="write talk"></textarea>-->
                                                        </form>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="closeModal" ng-click="recommend_talk(message)" >Recommend</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                        <div ng-hide="talk.show_post_button">	
                                            <div class="box latest_talk">
                                                <form > 
                                                    <div class="box-body">
                                                        <div class="image default_image">
                                                            <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                            <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="latest_content comment_content">
                                                            <div class="latest_header m-b10">
                                                                <h6><span>Write an update</span></h6>                                                    
                                                            </div>
                                                            <textarea class="form-control latest_commentarea" ng-model="talk.latest_talkParticipation" placeholder="Say Something..."></textarea>                     
                                                        </div>
                                                    </div>
                                                    <div class="post_footer clearfix">
                                                        <span class="share_list" style="color:#fff" ng-bind="show_selected">Hello world</span>

                                                        <div class="post_footebtn pull-right">
                                                            <div class="btn-group post_footerdrpbtn">

                                                                <button aria-expanded="false" type="button"  class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">Post</button>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div ng-hide="talk.open_comment_box">

                                            <div class="comment_subcmnt " id="comment_subcmnt">
                                                <div ng-repeat="comment in talk.comments">
                                                    <div class="latestcomment_part clearfix" >

                                                        <div class="image default_image">
                                                            <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                            <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="">
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="comment.bussiness_name"><span>talks</span></h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i>{{comment.comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="comment.comment"></p>                     
                                                                </div>
                                                            </div>
                                                            <div class="box-footer clearfix">
                                                                <ul class="latest_footerlink pull-left">
                                                                    <li><a href="javascript:void(0)" ng-click="open_animation_sub_comment(comment)">Comment</a></li>
                                                                    <li><a href="javascript:void(0)" ng-class="{'comment_like_status': comment.comment_like_status != 0 }" ng-click="feedback_latest_talk_comment_subComment(comment)"><i ng-class="{'fa fa-heart' :comment.comment_like_status != 0, 'fa fa-heart-o' :comment.comment_like_status == 0}" class="fa fa-heart-o">Like </i></a></li>                                                
                                                                </ul>

                                                                <ul class="latest_footerlink_right pull-right">
                                                                    <li><a href="javascript:void(0)" ng-click="comment_sub_comment(comment)" >{{comment.subcomment_count}}<i class="fa fa-comments"></i></a></li>
                                                                    <li><a href="javascript:void(0)" >{{comment.comment_like_count}} <i class="fa  fa-thumbs-up"></i> </a></li>
                                                                    <li>
                                                                        <div class="btn-group gear-btn">
                                                                            <button type="button" class="settings_icon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i class="fa fa-gear"></i>								
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li >
                                                                                    <a href="javascript:void(0)" ng-click="deleteCommentLatestTalk(talk, comment, $event)">Delete</a>
                                                                                </li>
                                                                                <li><a href="#">Hide</a></li>

                                                                            </ul>
                                                                        </div>													
                                                                    </li>
                                                                </ul>
                                                                <div class="clearfix"></div>

                                                                <div class="comment_box box-footer tab-footer" ng-hide="comment.show_sub_post_button">
                                                                    <div class="comment_default"> 
                                                                        <span ng-show="front_img_name == ''" ng-bind="user_initial"></span>
                                                                        <img class="comment_img" ng-show="front_img_name != ''" ng-src="{{path + front_img_name}}" />
                                                                    </div>

<!--<img class="comment_img" ng-src="{{hub.prof_img != '' && hub.prof_img|| 'http://arlians.com/assets/images/profile-img.png'}}" />-->
                                                                    <form>	
                                                                        <input type="text" ng-model="comment.latestTalk_sub_comment" class="form-control"  placeholder="Write your comment">
                                                                        <button class="btn btn_publish" ng-click="postLatestTalkSubComments(comment)">Post</button>
                                                                    </form>
                                                                </div>
                                                            </div> 
                                                        </div>



                                                    </div>


                                                    <div ng-repeat="subComment in comment.subcomment" class="latestcomment_part clearfix col-sm-offset-1" ng-hide="comment.show_sub_comment">



                                                        <div class="image default_image">
                                                            <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                            <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">
<!--                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="" >
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="subComment.bussiness_name"> <span>commented</span> </h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i> {{subComment.sub_comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="subComment.sub_comment"></p>                     
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>



                                            </div>   
                                        </div>   



                                        <!-- </a>comment on latest talk End -->

                                    </div>

                                </div> 
                            </div> 


                            <div class="resp-tabs-container hor_1">
                                <div>
                                    <div ng-repeat="talk in myTalk">	
                                        <div class="box latest_talk">
                                            <div class="box-body" >
                                                <div class="image default_image">
                                                    <span ng-show="talk.logomembername_url == null" ng-bind="talk.logomembername"></span>
                                                    <img  ng-show="talk.logomembername_url != null" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle"/>
<!--                                                    <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>-->
                                                </div>
                                                <div class="latest_content"  >
                                                    <!-- <div>
                                                            <h3><span ng-bind="talk.bussiness_name"></span></h3>
                                                    </div> -->
                                                    <div class="latest_header clearfix">
                                                        <h6 ng-bind="talk.member_name"></h6>
                                                        <!--<a href="#" class="user_likes"><i class="fa fa-user"></i> 5K</a>-->
                                                    </div>
                                                    <p ><strong ng-bind="talk.title"></strong></p> 
                                                    <span  ng-bind="talk.content"></span>



                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">


                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)" ng-click="" ><span ng-bind="talk.comments_count"></span><i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)" ><span ng-bind="talk.like_count"></span><i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li>
                                                        <div class="btn-group gear-btn">
                                                            <button type="button" class="dropdown-toggle settings_icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-gear"></i>								
                                                            </button>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li ng-show="talk.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteMyTalk(talk, $event)">Delete</a></li>
                                                                <li><a href="#">Hide</a></li>

                                                            </ul>
                                                        </div>
                                                    </li>

                                                </ul>

                                            </div> 
                                        </div>



                                    </div>
                                    <!--   *********** -->

                                    <!--                                    <div ng-repeat="talk in fetch_Latest_Talk" >
                                                                            <div ng-repeat="comments in talk.my_talk">
                                                                                <div class="box latest_talk" >
                                                                                    <div class="box-body" >
                                                                                       <div class="image default_image">
                                                                                        <span ng-show="talk.logomembername_url==null" ng-bind="talk.logomembername"></span>
                                                                                        <img ng-show="talk.logomembername_url!=null" ng-src="{{talk.image_basepath+talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">
                                                                                        <a class="online-status" href="#"><i class="fa fa-circle text-success"></i></a>
                                                                                       </div>
                                                                                        <div class="latest_content"  >
                                                                                            <div>
                                                                                                <h3><span ng-bind=""></span></h3>
                                                                                            </div>
                                                                                            <div class="latest_header clearfix">
                                                                                                <h6><span ng-bind="comments.bussiness_name"></span> <span ng-bind=""></span></h6>
                                                                                                <a href="#" class="user_likes"><i class="fa fa-user"></i> 5K</a>
                                                                                            </div>
                                    
                                                                                            <p><strong ng-bind="comments.comment"></strong></p> 
                                    
                                                                                        </div>
                                                                                    </div>
                                    
                                    
                                                                                </div>
                                                                            </div>
                                                                        </div>-->
                                    <!--   *********** -->
                                </div>
                            </div>

                            <!-- tab 3 -->
                            <!-- <div class="resp-tabs-container hor_1">
                                <div>

                                    <div ng-repeat="talk in fetch_Latest_Talk" >
                                        
                                        <div class="box latest_talk" ng-click="latest_talk_toggle_animation(talk)" ng-show="talk.show_latest_talk_section" >
                                             
                                            <div class="box-body" >

                                                <div class="image default_image">
                                                    <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                    <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">

                                                </div>
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6 ng-bind="talk.member_name"></h6>                                           

                                                      <a href="#" class="user_likes" ><i class="fa fa-user" ng-bind="talk.total_comment"></i></a>                                           

                                                    </div>
                                                    <p ><strong ng-bind="talk.title"></strong></p>
                                                    <p ng-bind="talk.content"></p> 
                                                </div>


                                            </div>
                                            <div class="box-footer clearfix">
                                                <ul class="latest_footerlink pull-left">
                                                    <li><a href="javascript:void(0)" id="participate_button" class="toggle" ng-click="open_comment_Animation_latestTalk(talk)">Participate</a></li>
                                                    <li><a href="javascript:void(0)" ng-class="{'com_like': talk.com_like != 0 }" ng-click="feedback_latest_talk(talk)"><i ng-class="{'fa fa-heart' :talk.com_like != 0, 'fa fa-heart-o' :talk.com_like == 0}" class="fa"></i>Like </a></li>
                                                    <li><a href="javascript:void(0)"  ng-click="open_recommend_Animation_latestTalk(talk)">Recommend</a></li>
                                                </ul>

                                                <ul class="latest_footerlink_right pull-right">
                                                    <li><a href="javascript:void(0)" ng-click="toggleComment(talk)" ><span ng-bind=""></span><i class="fa fa-comments"></i></a></li>
                                                    <li><a href="javascript:void(0)" ><span ng-bind="talk.total_like"></span><i class="fa  fa-thumbs-up"></i> </a></li>
                                                    <li>
                                                        <div class="btn-group gear-btn">
                                                            <button type="button" class="dropdown-toggle settings_icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-gear"></i>                              
                                                            </button>
                                                            <ul class="dropdown-menu pull-right">
                                                                <li ng-show="talk.mid == user_mid"><a href="javascript:void(0)" ng-click="deleteParticipateTalk(talk, $event)">Delete</a></li>
                                                                <li><a href="#">Hide</a></li>

                                                            </ul>
                                                        </div>
                                                    </li>

                                                </ul>

                                            </div> 
                                         

                                        </div> 
                                       

                                       

                                        <div class="modal fade" id="recommend" tabindex="-1" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Recommend Talk</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <form novalidate name="recommend">


                                                            <input type="text" id="demo-input-custom-limits_recommend" ng-model="business_name" ng-keyup="businessname_auto_search(business_name)" class="form-control fld"  placeholder="To: [auto fill by user name or email adress]">
                                                            <div class="loading" style="display:none;">searching...</div>
                                                            <div class="no_result" style="display:none;">no result found...</div>

                                                            <div ><a href="#"  ng-repeat="business in savedBusinessName" >
                                                                    <span class="tag_list" id="close"  ng-bind="business.bussinessname"></span><i class="close_taglist" ng-click="closeBusinessname(business)">x</i></a>
                                                            </div>
                                                            <ul id="business_name_recommend"><li ng-repeat="business in businessname" class="dropdown_list_class" id="dropdown_list_recommend" ng-click="saveBusiness_name(business)" ng-bind="business.bussinessname"></li></ul>
                                                            <textarea name="text" id="txt_msg" class="recomnd_txtarea" ng-model="message" placeholder="write talk"></textarea>
                                                         
                                                        </form>

                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary" id="closeModal" ng-click="recommend_talk(message)" >Recommend</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div ng-hide="talk.show_post_button">   
                                            <div class="box latest_talk">
                                                <form > 
                                                    <div class="box-body">
                                                        <div class="image default_image">
                                                            <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                            <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">
                                                                                                             </div>

                                                        <div class="latest_content comment_content">
                                                            <div class="latest_header m-b10">
                                                                <h6><span>Write an update</span></h6>                                                    
                                                            </div>
                                                            <textarea class="form-control latest_commentarea" ng-model="talk.latest_talkParticipation" placeholder="Say Something..."></textarea>                     
                                                        </div>
                                                    </div>
                                                    <div class="post_footer clearfix">
                                                        <span class="share_list" style="color:#fff" ng-bind="show_selected">Hello world</span>

                                                        <div class="post_footebtn pull-right">
                                                            <div class="btn-group post_footerdrpbtn">

                                                                <button aria-expanded="false" type="button"  class="btn  btn-sm dropdown-toggle" ng-click="Open_Animation_Comment_box_Latest_Talk(talk)">Post</button>
                                                            </div>


                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <div ng-hide="talk.open_comment_box">

                                            <div class="comment_subcmnt " id="comment_subcmnt">
                                                <div ng-repeat="comment in talk.comments">
                                                    <div class="latestcomment_part clearfix" >

                                                        <div class="image default_image">
                                                            <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                            <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">

                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="">
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="comment.bussiness_name"><span>talks</span></h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i>{{comment.comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="comment.comment"></p>                     
                                                                </div>
                                                            </div>
                                                            <div class="box-footer clearfix">
                                                                <ul class="latest_footerlink pull-left">
                                                                    <li><a href="javascript:void(0)" ng-click="open_animation_sub_comment(comment)">Comment</a></li>
                                                                    <li><a href="javascript:void(0)" ng-class="{'comment_like_status': comment.comment_like_status != 0 }" ng-click="feedback_latest_talk_comment_subComment(comment)"><i ng-class="{'fa fa-heart' :comment.comment_like_status != 0, 'fa fa-heart-o' :comment.comment_like_status == 0}" class="fa fa-heart-o">Like </i></a></li>                                                
                                                                </ul>

                                                                <ul class="latest_footerlink_right pull-right">
                                                                    <li><a href="javascript:void(0)" ng-click="comment_sub_comment(comment)" >{{comment.subcomment_count}}<i class="fa fa-comments"></i></a></li>
                                                                    <li><a href="javascript:void(0)" >{{comment.comment_like_count}} <i class="fa  fa-thumbs-up"></i> </a></li>
                                                                    <li>
                                                                        <div class="btn-group gear-btn">
                                                                            <button type="button" class="settings_icon dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                <i class="fa fa-gear"></i>                              
                                                                            </button>
                                                                            <ul class="dropdown-menu pull-right">
                                                                                <li >
                                                                                    <a href="javascript:void(0)" ng-click="deleteCommentLatestTalk(talk, comment, $event)">Delete</a>
                                                                                </li>
                                                                                <li><a href="#">Hide</a></li>

                                                                            </ul>
                                                                        </div>                                                  
                                                                    </li>
                                                                </ul>
                                                                <div class="clearfix"></div>

                                                                <div class="comment_box box-footer tab-footer" ng-hide="comment.show_sub_post_button">
                                                                    <div class="comment_default"> 
                                                                        <span ng-show="front_img_name == ''" ng-bind="user_initial"></span>
                                                                        <img class="comment_img" ng-show="front_img_name != ''" ng-src="{{path + front_img_name}}" />
                                                                    </div>


                                                                    <form>  
                                                                        <input type="text" ng-model="comment.latestTalk_sub_comment" class="form-control"  placeholder="Write your comment">
                                                                        <button class="btn btn_publish" ng-click="postLatestTalkSubComments(comment)">Post</button>
                                                                    </form>
                                                                </div>
                                                            </div> 
                                                        </div>



                                                    </div>


                                                    <div ng-repeat="subComment in comment.subcomment" class="latestcomment_part clearfix col-sm-offset-1" ng-hide="comment.show_sub_comment">



                                                        <div class="image default_image">
                                                            <span ng-show="talk.logomembername_url == 'NULL'" ng-bind="talk.logomembername"></span>
                                                            <img ng-show="talk.logomembername_url != 'NULL'" ng-src="{{talk.logomembername_url}}" alt="" class="latest_talkimg img-circle">

                                                        </div>

                                                        <div class="box latest_talk" >
                                                            <div class="box-body">

                                                                <div class="" >
                                                                    <div class="latest_header clearfix">
                                                                        <h6 ng-bind="subComment.bussiness_name"> <span>commented</span> </h6>
                                                                        <a href="#" class="user_likes"><i class="fa fa-clock-o"></i> {{subComment.sub_comm_date}}</a>
                                                                    </div>                                                 
                                                                    <p ng-bind="subComment.sub_comment"></p>                     
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                </div>



                                            </div>   
                                        </div>   



                                       

                                    </div>

                                </div> 
                            </div> -->







                            <!-- Tab 4 -->
                            <form novalidate>
                                <div class="resp-tabs-container hor_1">
                                    <div> 

                                        <div class="box latest_talk">
                                            <div class="box-body">
                                                <div class="image default_image">
                                                    <span ng-show="front_img_name == ''" ng-bind="user_initial"></span>
                                                    <img class="latest_talkimg" alt="" ng-show="front_img_name != ''" ng-src="{{path + front_img_name}}" />
                                                </div> 
                                                <!--                                            <div class="image">
                                                    <img class="latest_talkimg img-circle" alt="" src="images/user2-160x160.jpg">
                                                    <a href="#" class="online-status"><i class="fa fa-circle text-success"></i></a>
                                                </div>-->
                                                <div class="latest_content">
                                                    <div class="latest_header clearfix">
                                                        <h6> <span>verified talker</span></h6>                     
                                                    </div>
                                                    <div class="form-group">                        
                                                        <div class="">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-pencil-square"></i></span>
                                                                <input type="text" class="form-control" id="titleBlank" name="title" placeholder="Talk title" id="inputGroupSuccess2" ng-model="title" aria-describedby="inputGroupSuccess2Status">
                                                            </div>                          
                                                        </div>
                                                    </div>

                                                    <div class="form-group">                        
                                                        <div class="">
                                                            <div class="input-group">
                                                                <textarea class="form-control" placeholder="&#xf14b; describe your view" id="contentBlank" name="content" ng-model="content"></textarea>
                                                            </div>                          
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="box-footer clearfix">

                                            </div> 
                                        </div>

                                        <div class="box">
                                            <div class="box-header"><i class="fa fa-dot-circle-o"></i> Assign tags to your talk </div>
                                            <div class="box-body asign_talk">
                                                <!-- <form class="sidebar-form top_search" method="get" action="#">
                                                     <div class="input-group">
                                                         <span class="input-group-btn">
                                                             <button class="btn btn-flat" id="search-btn" name="search" type="submit"><i class="fa fa-search"></i></button>
                                                         </span>
                                                         <input type="text" placeholder="Search..." class="form-control search_asign" name="q">         
                                                     </div>
                                                 </form>
                                                -->


                                                <div class="bs-example_1">
                                                    <div class="searchtag_input">
                                                        <i class="fa fa-search"></i>
                                                        <input type="text" value="tag name" id="tag_input" ng-keyup="talkTags(tagsinput)" ng-model="tagsinput" >
                                                    </div>

                                                    <ul id="tag"><li  ng-repeat="tag in sugessted_tags track by $index" class="dropdown_list_class" id="dropdown_list" ng-click="saveSuggestedTag(tag)" ng-bind="tag.tag_name"></li></ul>
                                                </div>
                                                <small>Tags not reconignizied? Suggest it to Arlians</small>
                                                <div class="clearfix"></div>
                                                <div >
                                                    <a href="#" class="tag_field"  ng-repeat="tag in savedtags track by $index" >
                                                        <i class="fa fa-tag tagicon"></i>
                                                        <span class="tag_list" id="close"  ng-bind="tag.tag_name"></span>
                                                        <i class="close_taglist fa fa-close" ng-click="closeTags(tag)"></i>
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="box-footer clearfix">
                                                <!-- Single button -->
                                                <div class="btn-group pull-right">
                                                    <button aria-expanded="false" type="button" data-toggle="dropdown" class="btn btn-primary  btn-sm dropdown-toggle ">Post</button>
                                                    <ul role="menu" class="dropdown-menu">                                        
                                                        <li ng-repeat='list in share_list'><a href="javascript:void(0);" ng-click="startTalkSubmit(content, title, list)"  ng-bind="list.name">Add new event</a></li>
                                                    </ul>
                                                    <!--<button type="submit" class="btn btn-primary pull-right" ng-click="startTalkSubmit(content,title)" >Post</button>-->
                                                </div>
                                            </div> 
                                        </div>  
                                        <div class="" style="width:100%; height:100px; visibility:hidden;">
                                        </div>

                                        <!--<div class="box">
                                            <div class="box-header"><i class="fa  fa-users"></i> Invite your audience to paritcipate </div>
                                            <div class="box-body asign_talk participate">
                                                <form class="sidebar-form top_search pull-right" method="get" action="#">
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <button class="btn btn-flat" id="search-btn" name="search" type="submit"><i class="fa fa-search"></i></button>
                                                        </span>
                                                        <input type="text" placeholder="Search..." class="form-control search_asign" name="q">             
                                                    </div>
                                                </form>
                                                <p>Promote your talk via your favourite social media to invite peers to discuss </p>
                                                <div class="promote_talk clearfix">
                                                    
                                                    <textarea class="form-control" placeholder="&#xf14b;  Promote your talk." ng-model="talkText"></textarea>
                                                        <button ng-click="startTalkSubmit(talkText)">Post</button>-->
                                                    <!--<a href="#" class="promote_link"><i class="fa  fa-linkedin-square"></i></a> 
                                                    <a href="#" class="promote_link"><i class="fa  fa-twitter-square"></i></a>
                                                    
                                                </div>
    
                                            </div>
    
                                        </div>
    
    
                                        <!--<div class="box">
                                            <div class="box-header">
                                                <i class="fa fa-calendar"></i> Schedule your live talk</div>
    
    
                                            <div class="box-body">
                                                <div class="row">
                                                    <div class="date_clock">
                                                        <div class="col-md-7">
                                                            <div class="multiple"></div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div class="clocksjs">
                                                                <img src="images/clock.png" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
    
    
    
                                            </div>
    
                                        </div>-->


                                    </div>                
                                </div>
                            </form>
                        </div>    
                    </div>
                </div>
            </div>  

        </div>
    </div>
    <div class="col-md-3 col-sm-12 left-block" ng-controller="DashboardSuggestionCtrl as suggestion">
        <div class="row">   
            <div class="match_s mCustomScrollbar">  
                <div class="col-sm-6 col-md-12" ng-show="search_module">
                    <div class="view_result_box fixd_box1">
                        <div class="view_box_top"><h4>See All Matching <!--<small><i class="fa fa-search"></i></small>--></h4></div>
                        <div class="view_box_body">
                            <div class="match_result">
                                <span class="mathc_txt">MATCHING <br> RESULTS</span><br>
                                <span class="mathc_resulttxt" ng-bind="total_suggestion_count">132</span>
                            </div>
                            <div class="result_bottom clearfix">
                                <div id="result-circle" class="owl-carousel">
                                    <div class="item" ng-repeat="list in detail_suggestion_list">
                                        <div class="result_numb">
                                            <div class="result_numbin" ng-bind="list.type_count">69</div>
                                        </div>
                                        <span class="result_name" ><a ng-bind="list.type" href="javascript:void(0)" ng-click="getDetailSuggestion(list)">Customers</a></span>
                                    </div>                                 
                                </div>
                            </div>
                        </div>
                        <div class="view_box_bottom">
                           <!-- <a href="javascript:void(0);" class="view_menu" ng-click="openWizardTwoModal()"><i class="fa fa-list"></i></a>-->
                            <a href="javascript:void(0);" class="view_menu" ><i class="fa fa-list"></i></a>
    <!--                        <a href="javascript:void(0);" class="view_menu" data-toggle="modal" data-target="#myModal"><i class="fa fa-list"></i></a>-->
    <!--                        <p class="match_text">Improve your match</p>-->
                            <!-- Modal -->

                            <!--                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    <h4 class="modal-title" id="myModalLabel">What sort of businesses do you service?</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                    </div> 
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                    <button type="button" class="btn btn-primary">Save changes</button>
                                                                </div>
                                                            </div>
                            
                                                        </div>
                                                    </div>-->
                        </div>                      


                    </div>
                </div>
                <div class="col-sm-6 col-md-12"  ng-show="show_detail" id="detail_suggestion">
                    <div class="view_result_box fixd_box1" ng-repeat="detail in details_suggestion">
                        <div class="view_box_top1 matching clearfix"><h4 ng-bind="search_for_suggestion">Customers</h4>
                            <div class="result_numb1">
                                <div class="result_numbin1" ng-bind="total_count">69</div>
                            </div>
                        </div>
                        <div>
                            <div class="view_box_body1">
                                <div class="matchign_cont">
                                    <a href="javascript:void(0)" class="matchig_logo">
                                        <span  ng-show="detail.image == ''" ng-bind="detail.logobussinessname"></span>
                                        <img ng-show="detail.image != ''" ng-src="{{detail.image!=null?'<?php echo base_url(); ?>uploads/profile_image/'+detail.image:'<?php echo base_url(); ?>assets/images/inbox-img.png'}}" alt="" >                                    
                                    </a>
                                    <h5 ng-bind-html="detail.bussinessname | limitTo:100">Company Name</h5>
                                    <p class="match_detail" ng-bind-html="detail.desc | limitTo:120">Excel at enterprise, mobile and web development with Java, Scala, Groovy and Kotlin, with all the latest modern technologies and frameworks available out of the box. </p>
                                </div>
                                <img src="<?php echo base_url() ?>assets_phase2/images/match_enginebg.jpg" alt="">
                            </div>
                            <div class="view_box_body1 text-center clearfix">
                                <a href="javascript:void(0);" class="result_numb3" ng-click="individual_suggestion_user_feedback(detail, 0)">
                                    <div class="result_numbin3"><i class="fa fa-minus"></i></div>
                                </a>
                                <a href="javascript:void(0);" class="result_numb4" ng-click="individual_suggestion_user_feedback(detail, 1)">
                                    <div class="result_numbin4"><i class="fa fa-plus"></i></div>
                                </a>
                                <div class="clearfix match_score">
                                    <div class="" ng-class="{'result_numb_red'   : detail.score == 'one',
                                    'result_numb_amber' : detail.score == 'two',
                                    'result_numb_green'    : detail.score == 'three'}">
                                        <div class=""ng-class="{'result_numbin_red'   : detail.score == 'one',
                                    'result_numbin_amber' : detail.score == 'two',
                                    'result_numbin_green'    : detail.score == 'three'}" ng-bind="detail.score">69</div>

                                    </div>

                                    <span class="score_txt" ng-bind="detail.matched_text | limitTo:140">Match Score</span>
                                </div>
                            </div>
                        </div>
                        <div class="view_box_bottom">
                            <span class="backto_result" >
                                <a href="javascript:void(0);" ng-click="backToSuggestionList()">Back to matching results</a>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-12">
                    <img class="p-d"  src="<?php echo base_url(); ?>assets_phase2/images/p-d.jpg" alt=""/>
                    <!--                <div class="view_result_box portfolio">
                                        <div class="view_box_top"><h4>Portfolio Documents</h4></div>
                                        <div class="view_box_body clearfix">
                    
                                            <div class="portfolio_box white_box">
                                                <i class="fa fa-pencil"></i>
                                            </div>
                    
                                            <div class="portfolio_box normal_box">
                                                <span class="portbox_txt">Notes</span><br>
                                                <span class="port_inncircle">12</span>  
                                            </div>
                    
                                            <div class="portfolio_box  normal_box ">
                                                <span class="portbox_txt">Documents</span><br>
                                                <span class="port_inncircle">12</span> 
                    
                                            </div>
                    
                                            <div class="portfolio_box white_box ">
                                                <i class="fa  fa-folder"></i>    
                                            </div>
                    
                                            <div class="portfolio_box white_box">
                                                <i class="fa fa-comment"></i>
                                            </div>
                    
                                            <div class="portfolio_box normal_box">
                                                <span class="portbox_txt">Talks</span><br>
                                                <span class="port_inncircle">12</span>  
                                            </div>
                    
                                        </div>
                                        <div class="view_box_bottom">
                                            <a href="javascript:void(0)" class="view_menu"><i class="fa fa-list"></i></a>
                                        </div>
                                    </div>-->
                </div>
            </div>
        </div>
    </div>
</div>





<!-- common-helper JS -->   
<link rel="styleSheet" href="<?php echo base_url(); ?>assets/css/token-input.css" /> 

<script src="<?php echo base_url(); ?>assets/js/jquery.tokeninput.js"></script>   


<script src="<?php echo base_url(); ?>assets_phase2/js/dashboard_page.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.tokeninput.js"></script> 
<script src="<?php echo base_url(); ?>assets_phase2/js/custom-file-input.js"></script>
<!-- 05-04-16 for fade post -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets_phase2/js/waypoint-fade.min.js"></script>





<script>
                                            $(window).resize(function() {
                                    $('.mCustomScrollbar').height($(window).height());
                                    });
                                            $(window).trigger('resize');</script>




<script>
            $(function(){
            $('#hub_publish_default_img').show();
                    $('#hub_publish_preview_img').hide();
//CKEDITOR.replace('txtEditor');
                    $('.left-block, .tab-sticky-scroll')
                    .theiaStickySidebar({
                    additionalMarginTop: 50
                    });
                    $('.mCustomScrollbarN').mCustomScrollbar({
            //setHeight: 250
            //autoExpandScrollbar : false
            });
            });</script>

<script>
            $(document).ready(function() {

    });
            (function($){
            $(window).load(function(){

            $("#content-1").mCustomScrollbar({
            axis:"yx", //set both axis scrollbars
                    advanced:{autoExpandHorizontalScroll:true}, //auto-expand content to accommodate floated elements
                    // change mouse-wheel axis on-the-fly 
                    callbacks:{
                    onOverflowY:function(){
                    var opt = $(this).data("mCS").opt;
                            if (opt.mouseWheel.axis !== "y") opt.mouseWheel.axis = "y";
                    },
                            onOverflowX:function(){
                            var opt = $(this).data("mCS").opt;
                                    if (opt.mouseWheel.axis !== "x") opt.mouseWheel.axis = "x";
                            },
                    }
            });
            });
            })(jQuery);</script>
<script>
            $(function () {
            $("#demo-input-custom-limits").tokenInput("<?php echo base_url() . 'member/autocomplete' ?>", {
            searchDelay: 2000,
                    minChars: 2,
                    propertyToSearch: "bussinessname",
                    tokenValue: "mid",
            });
            });

</script>

