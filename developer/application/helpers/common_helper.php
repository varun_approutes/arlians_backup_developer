<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    function pr($array=array(),$die=true)
	{
		echo '<pre>';
		print_r($array);
		if($die)
			die;
	}
 
    function country_name($id)
    {
		 $CI=& get_instance();
         $CI->load->model('model_category');
         $result['country'] = $CI->model_category->country_name($id);
		return $result;
    }
	function countryName($id)
    {
		 $CI=& get_instance();
         $CI->load->model('model_category');
         $result['country'] = $CI->model_category->country_name($id);
		return $result['country'];
    }
	
	 function bussiness_name($code){

		$CI=& get_instance();

        $CI->load->model('model_category');

		$result['bussiness_name'] = $CI->model_category->bussiness_name($code);

		return $result;

		

	}
	function topline_name($code){

		$CI=& get_instance();

        $CI->load->model('model_category');

		$result['top_name'] = $CI->model_category->topline_name($code);

		return $result;
	

	}	
	function topline_alchemyName($code){

		$CI=& get_instance();
        $CI->load->model('model_category');
		$result['alchemyapiname'] = $CI->model_category->watsonKeyWord($code);
		if(!empty($arr_two_level))
		{
			foreach($result['alchemyapiname'] as $value)
			{
				$result['keyword'][]= rawurlencode($CI->model_category->topline_alchemyName($value));

			}
		}

		//var_dump($result['keyword']);exit;
		return $result['keyword'];
	

	}
	function sector_name($code){

		$CI=& get_instance();

        $CI->load->model('model_category');

		$result['sector_name'] = $CI->model_category->sector_name($code);

		return $result;
	

	}
	
	function sub_sector_name($code){

		$CI=& get_instance();

                $CI->load->model('model_category');

		$result['subsector_name'] = $CI->model_category->sub_sector_name($code);

		return $result;
	

	}
	function target_sector_name($code){

		$CI=& get_instance();

        $CI->load->model('model_category');

		$result['subsector_name'] = $CI->model_category->sub_sector_name($code);

		return $result;
	

	}

	function get_bing_str($code)
	{
		$CI=& get_instance();
		$CI->load->model('model_category');
		$result['bing_query_str'] = $CI->model_category->get_bing_str($code);
		return $result;
	}
	
	function sector_nameApi($code){
		$CI=& get_instance();
        $CI->load->model('model_category');
		$result['sector_name'] = $CI->model_category->sector_name($code);
		return $result['sector_name'];
	}
	
	function getCountryList(){
		
		$CI=& get_instance();
		$CI->load->model('model_member');
		
		$result = $CI->model_member->fetch_country_list();
		
		return $result;
	}

	function getMemberbusinName($mid){
		
		$CI=& get_instance();
		$CI->load->model('model_member');
		
		$result = $CI->model_member->member_bussiness_name_message($mid);
		if($result==''){
			
			$result = getMemberName($mid);
		}
		return $result;
	}
	
	function getProfileImage($mid){
		
		$CI=& get_instance();
		$CI->load->model('model_member');
		
		$result = $CI->model_member->ProfileImage($mid);
		
		return $result;
	}

	function suggestionLookingforName($mid,$search_type){

	 	$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->suggestionLookingforName($mid,$search_type);
		
		return $result;
	}

	function getParticularUserDetails($id){

	 	$CI=& get_instance();
		$CI->load->model('model_memberprofile');
		
		$result = $CI->model_memberprofile->getParticularUserDetails($id);
		//print_r($result); die();
		return $result;
	}

	function is_connected($mid,$con_id){
	 	$CI=& get_instance();
		$CI->load->model('model_suggestion');		
		$result = $CI->model_suggestion->is_connected($mid,$con_id);		
		return $result;
	}
		
	function get_topline_image($code){

	 	$CI=& get_instance();
		$CI->load->model('model_category');
		
		$result = $CI->model_category->get_topline_image($code);
		
		return $result;
	}
	function getImageFromSector($code){

	 	$CI=& get_instance();
		$CI->load->model('model_memberprofile');
		
		$result = $CI->model_memberprofile->getImageFromSector($code);
		
		return $result;
	}
	function showOnDashboard($mid){

	 	$CI=&get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->showOnDashboards($mid);
		//echo '<pre>'; var_dump($result); die();
		return $result;
	}

	function checkToShowUserInSearchResult($looking_for_search_id,$user_own_type){
		
		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->checkToShowUserInSearchResult($looking_for_search_id,$user_own_type);
		
		return $result;
	}

	function userProfType($mid){

		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->userProfType($mid);
		
		return $result;
	}
	function updateSuggestion($str,$mid){

		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->updateSuggestion($str,$mid);
		
		return $result;

	}
 
 	function sentMail($mail,$mailBody,$subject){

		$this->load->library('email');
		$this->email->from('admin@arlians.com', 'Arlians');
		$this->email->to($mail);
		$this->email->set_mailtype('html');

		$this->email->subject($subject);

		$this->email->message($mailBody);

		$this->email->send();
		return true;

	}
	function getMemberName($mid){
		$CI=& get_instance();
		$CI->load->model('model_memberprofile');
		
		$result = $CI->model_memberprofile->memberName($mid);
		
		return $result;	
	}

	function getLookingForById($id){
		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->getLookingForById($id);
		
		return $result;	
	}
	function getLookingForByType($id){
		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->getLookingForByType($id);
		
		return $result;	
	}
	function isScoreNotZero($id,$mid){
		
		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->isScoreNotZero($id,$mid);
		
		return $result;	
		
		
	}
	function suggestionTypeFromId($mid,$sid){
		
		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->suggestionTypeFromId($mid,$sid);
		
		return $result;	
		
		
	}
		function matchScore($id,$mid,$lookingForId){
		
		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		
		$result = $CI->model_suggestion->matchScore($id,$mid,$lookingForId);
		if($result=='1'){
			$result='one';
		}elseif($result=='2'){
			$result='two';
		}elseif($result=='3'){
			$result='three';
			
		}else{
			$result='one';
		}
		
		return $result;	
		
		
	}
	
	function matchScoretext($id,$mid,$lookingForId){
			$CI=& get_instance();
		$CI->load->model('model_suggestion');
		$result = $CI->model_suggestion->matchScore($id,$mid,$lookingForId);
		$result_pattern = $CI->model_suggestion->matchScorePat($id,$mid,$lookingForId);
		$textresult = $CI->model_suggestion->matchScoretextNew($lookingForId,$result,$result_pattern );
		
				
		return $textresult;	
	}
	
	function getConnectRequestDetail($cid){
		$CI=& get_instance();
		$CI->load->model('model_othermembersprofile');
		$data['connected_id'] = $cid;
		$data['mid'] = $CI->session->userdata['logged_in']['id'];
		$result =$CI->model_othermembersprofile->getConnectRequestDetail($data);
		return $result;
	}
	
	function Feedbackdetails($mid,$sid){
		
		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		$result = $CI->model_suggestion->Feedbackdetails($mid,$sid);
		return $result;
	}


	function check_member_payment_status($mid){
		$CI=& get_instance();
		$CI->load->model('model_member');
		$result = $CI->model_member->check_member_payment_status($mid);
		return $result;
	}

	function check_member_type(){		
		$CI=& get_instance();
		$CI->load->model('model_member');
		$result = $CI->model_member->check_member_type();
		if($result[0]['user_type'] == "Free"){
          $status = "Beta";
        } else{
          $status = "Premium";
        }
		return $status;
	}

	function getBusinessNameById($mid){

		$CI=& get_instance();
		$CI->load->model('model_member');
		$result = $CI->model_member->getBusinessNameById($mid);
		return $result;
	}

	function getHubById($hid){

		$CI=& get_instance();
		$CI->load->model('model_hub');
		$result = $CI->model_hub->getHubById($hid);
		return $result;
	}
	

	function checkURLfromString($string){
		$string= preg_replace('@((/https/?://)?([-\w]+\.[-\w\.]+)+\w(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)*)@', '<a href="http://$1" target="blank">$1</a>', $string);
	
		return $string;
	}


	function insertTransaction($data){
		
		$CI=& get_instance();
		$CI->load->model('model_payment');
		$result = $CI->model_payment->insert_transaction($data);
		return $result;
	}
	
	function referralCount($id){
		
		$CI=& get_instance();
		$CI->load->model('admin/model_member');
		$result = $CI->model_member->refCount($id);
		return $result;
	}
	
	 function premiumType($mid){
		$CI=& get_instance();
		$CI->load->model('model_payment');
		$result = $CI->model_payment->paymentType($mid);
		return $result;		
	}

    function updateMemberStatus($id){		
		$CI=& get_instance();
		$CI->load->model('model_member');
		$result = $CI->model_member->updateMemberStatus($id);
		return $result;
    }
	function getUserRegistrationDate($mid){
		$CI=& get_instance();
		$CI->load->model('model_member');
		$result = $CI->model_member->getUserRegistrationDate($mid);
		return $result;		
	}
	
	function getUserOwninducstryCodebyuser($mid){
		$CI=& get_instance();
		$CI->load->model('model_category');
		$result = $CI->model_category->getUserOwninducstryCodebyuser($mid);
		return $result;	
	}
    function custom_echo($x, $length)
    {
      if(strlen($x)<=$length)
      {
        echo $x;
      }
      else
      {
        $y=substr($x,0,$length) . '...';
        echo $y;
      }
    }
	function isFoundingMember($mid){
		$CI=& get_instance();
		$CI->load->model('model_member');
		$result = $CI->model_member->isFoundingMember($mid);
		if($result->subscription_type==1){
			
			return true;
		}else{
			return false;
		}	
	}

    function userUpgrationStatus($mid){
        
    	$CI=& get_instance();
		$CI->load->model('model_member');
		$result = $CI->model_member->userUpgrationStatus($mid);
		return $result;	
    }
	function listOfSuggestionBlockIds($mid){
		
		$CI=& get_instance();
		$CI->load->model('model_suggestion');
		$result = $CI->model_suggestion->suggestionBlocked($mid);
		return $result;			
	}
	function isAlredyDowngradeAccount($mid){
		$CI=& get_instance();
		$CI->load->model('model_memberprofile');
		$result = $CI->model_memberprofile->isAlredyDowngradeAccount($mid);
		return $result;			
	}
	
	function readMore($string,$limit){
		$string = strip_tags($string,'<a><br>');
		if (strlen($string) > $limit) {
			$stringCut = substr($string, 0, $limit);  // truncate string
			$string = substr($stringCut, 0, strrpos($stringCut, ' '));  
		}
		return $string;
	}

	function getTagNameById($code){
		$CI=& get_instance();
		$CI->load->model('model_hub');
		$result = $CI->model_hub->getTagNameById($code);
		return $result;			
	}
	function getEmailById($id)
	{
		$CI=& get_instance();
		$CI->load->model('model_member');
		$result = $CI->model_member->getEmailById($id);
		return $result;	
	}

	function getIdByUserEmail($email)
	{
		$CI=& get_instance();
		$CI->load->model('model_member');
		$data['user_email'] = $email;
		$result = $CI->model_member->getIdByUserEmail($data);
		
		return $result;	
	}
	function fifteenToTwentyConversion($fifcode){
		$CI=& get_instance();
		$CI->load->model('model_category');
		$result = $CI->model_category->fifteenToTwentyConversion($fifcode);
		return $result;	
	}
	function fetch_my_subsector($mid)
	{
		$CI=& get_instance();
		$CI->load->model('model_memberprofile');
		$result = $CI->model_memberprofile->getBusinessCodeNews($mid);
		
		$arr = array();
		foreach($result as $res)
		{
			$code = explode('-',$res['ans_set_cat_id']);
			$arr[] = $code[2];
		}
		return $arr;
	}
	function fetch_my_sector_pro($mid)
	{
		$CI=& get_instance();
		$CI->load->model('model_memberprofile');
		$result = $CI->model_memberprofile->getBusinessCode($mid);
		
		$arr = array();
		$i=0;
		foreach($result as $res)
		{
			//$code = explode('-',$res['ans_set_cat_id']);
			$arr[$i]['subcode'] = $res['ans_set_cat_id'];
			$subsecname=topline_name($res['ans_set_cat_id']);
			$arr[$i]['subname'] = $subsecname['top_name'];
			$i++;
		}
		return $arr;
	}
	function fetch_target_sector($mid)
	{
		$CI=& get_instance();
		$CI->load->model('model_memberprofile');
		$result = $CI->model_memberprofile->getTargetSubSector($mid);
		$arr = array();
		$i=0;
		foreach($result as $res)
		{
			$code = $res['target_sector_code'];
			$arr[$i]['targetsubcode'] = $code;
			$arr[$i]['targetsubname'] = $res['tar_sector_name'];
			$i++;
		}
		return $arr;
		
	}
	
function calculateTimeFromPost($postedDateTime, $systemDateTime, $typeOfTime) {
//	var_dump('postdate:'.$postedDateTime.' sysdate:'.$systemDateTime);
$changePostedTimeDate=strtotime($postedDateTime);

$changeSystemTimeDate=strtotime($systemDateTime);
$timeCalc=$changeSystemTimeDate-$changePostedTimeDate;

//var_dump($changeSystemTimeDate.'-'.$changePostedTimeDate);die();
if ($typeOfTime == "s") {
if ($timeCalc > 0) {
$typeOfTime = "s";
}
if ($timeCalc > 60) {
$typeOfTime = "m";
}
if ($timeCalc > (60*60)) {
$typeOfTime = "h";
}
if ($timeCalc > (60*60*24)) {
$typeOfTime = "d";
}
}
if ($typeOfTime == "s") {
$timeCalc = $timeCalc. " sec ago";
}
if ($typeOfTime == "m") {
$timeCalc = round($timeCalc/60) . " min ago";
}
if ($typeOfTime == "h") {
$timeCalc = round($timeCalc/60/60) . " hours ago";
}
if ($typeOfTime == "d") {
$timeCalc = round($timeCalc/60/60/24) . " days ago";
}

$time=explode(' ',$timeCalc);

	if($time[0] < 1){
		return  $timeCalc = 'Just now';
	}else{
		
		return $timeCalc;
	}


}



function getImageFromUrl($main_url)
{ 
   
    @$str = file_get_contents($main_url);
    $img = array();
    $img2 = array();
   // This Code Block is used to extract title
   if(strlen($str)>0)
   {
     $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
     preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title);
   }
     // This Code block is used to extract description 
   $b =$main_url;
   @$url = parse_url( $b ) ;
   @$tags = get_meta_tags( $url['scheme'].'://'.$url['host'] );
   // This Code Block is used to extract any image 1st image of the webpage
   $dom = new domDocument;
   @$dom->loadHTML($str);
   $images = $dom->getElementsByTagName('img');
   foreach ($images as $image)
   {
     $l1=@parse_url($image->getAttribute('src'));
     if($l1['scheme'])
     {
       $img[]=$image->getAttribute('src');
     }
     else
     {
    
     }
   }
   // This Code Block is used to extract og:image which facebook extracts from webpage it is also considered 
   // the default image of the webpage
   $d = new DomDocument();
   @$d->loadHTML($str);
   $xp = new domxpath($d);
   foreach ($xp->query("//meta[@property='og:image']") as $el)
   {
     $l2=parse_url($el->getAttribute("content"));
     if($l2['scheme'])
     {
       $img2[]=$el->getAttribute("content");
     }
     else
     {
    
     }
   }

   $img_url;
   
   if($img2)
   {
      
      $img_url = $img2[0];
   }
   else
   {
     $img_url = $img[0];
   }
   
   
   return $img_url;
}


function getHtmlFromUrl($main_url)
{ 
   $title;
   $src;
   $arr=array();
    @$str = file_get_contents($main_url);
    $img = array();
    $img2 = array();
   // This Code Block is used to extract title
   if(strlen($str)>0)
   {
     $str = trim(preg_replace('/\s+/', ' ', $str)); // supports line breaks inside <title>
     preg_match("/\<title\>(.*)\<\/title\>/i",$str,$title);
   }
   
   include_once('simple_html_dom.php');
   
  // $description=substr(file_get_html($main_url)->plaintext, 0, 200);
   $html=file_get_html($main_url);
   $headlines = array();
 if($html!=false){
foreach($html->find('p') as $header) {
 $headlines[] = $header->plaintext;
}
$para=array();
foreach($headlines as $word){
	//var_dump($word);
	if(strlen($word)> 50){
		
	$para[]= $word;	
	}
	
}
$description=$para[1];
//print_r($para[1]);
     // This Code block is used to extract description 
   //$b =$main_url;
   //@$url = parse_url( $b ) ;
   //@$tags = get_meta_tags( $url['scheme'].'://'.$url['host'] );
   // This Code Block is used to extract any image 1st image of the webpage
   $dom = new domDocument;
   @$dom->loadHTML($str);
   $images = $dom->getElementsByTagName('img');
   foreach ($images as $image)
   {
     $l1=@parse_url($image->getAttribute('src'));
     if($l1['scheme'])
     {
       $img[]=$image->getAttribute('src');
     }
     else
     {
    
     }
   }
   // This Code Block is used to extract og:image which facebook extracts from webpage it is also considered 
   // the default image of the webpage
   $d = new DomDocument();
   @$d->loadHTML($str);
   $xp = new domxpath($d);
   foreach ($xp->query("//meta[@property='og:image']") as $el)
   {
     $l2=parse_url($el->getAttribute("content"));
     if($l2['scheme'])
     {
       $img2[]=$el->getAttribute("content");
     }
     else
     {
    
     }
   }

   $html = '<a href="'.$main_url.'" target="_blank">';
   $html .= "<p id='title'>".$title[1]."</p><br>";
   if($img2)
   {
      $html .= "<img src='".$img2[0]."'><br>";
      $src = $img2[0];
   }
   else
   {
     $html .= "<img src='".$img[0]."'><br>";
     $src = $img[0];
   }
  // $html .= "<p id='desc'>".$description."</p></a>";

   $arr['html'] 	= $html;
   $arr['title'] 	= $title[1];
   $arr['src'] 		= $src;
   $arr['desc'] 	= $description;
   }
   return $arr;

}

function checkUrl($url)
{
    $regex = "((https?|ftp)\:\/\/)?"; // SCHEME 
    $regex .= "([a-z0-9+!*(),;?&=\$_.-]+(\:[a-z0-9+!*(),;?&=\$_.-]+)?@)?"; // User and Pass 
    $regex .= "([a-z0-9-.]*)\.([a-z]{2,3})"; // Host or IP 
    $regex .= "(\:[0-9]{2,5})?"; // Port 
    $regex .= "(\/([a-z0-9+\$_-]\.?)+)*\/?"; // Path 
    $regex .= "(\?[a-z+&\$_.-][a-z0-9;:@&%=+\/\$_.-]*)?"; // GET Query 
    $regex .= "(#[a-z_.-][a-z0-9+\$_.-]*)?"; // Anchor 

       if(preg_match("/^$regex$/", $url)) 
       { 
        return 1; 
       }
       else
       {
       	return 0;
       }
}
function newsImage($id)
{
	$CI=& get_instance();
	$CI->load->model('model_category');
	$result = $CI->model_category->newsImage($id);
	return $result;	
	
}

function getUrls($string) 
	{ 
	 $regex = '/https?\:\/\/[^\" ]+/i';
	 preg_match_all($regex, $string, $matches);
	 $url = ($matches[0][0]);
	 return $url;
	}
	function checkUrlValidation($url)
{
	if (!filter_var($url, FILTER_VALIDATE_URL) === false) 
	{
    	return 1;
	} 
	else 
	{
    	return 0;
	}
}


function logobussinessname($mid){
$userProfType=userProfType($mid);
	if($userProfType=='Buisness'){
		
		$logo=getMemberbusinName($mid);
		$logo = explode(' ',$logo);
		$fword=str_split($logo[0]);
		$sword='';
		$logolastindex=end($logo);
		if($logo[1]){
			$sword=str_split($logolastindex);
		}
		
		$fword=$fword[0].''.$sword[0];
		
		
      	$logobussinessname =$fword;
	}else{
		
		$logo=getMemberName($mid);
		$logo = explode(' ',$logo);
		$fword=str_split($logo[0]);
		$sword='';
		$logolastindex=end($logo);
		if($logo[1]){
			$sword=str_split($logolastindex);
		}
		
		$fword=$fword[0].''.$sword[0];
		
		
      	$logobussinessname =$fword;
		
	}
	//var_dump($logo);
	return $logobussinessname;
	
}

function getFavicon($url)
{
	require 'FaviconDownloader.class.php';

    $favicon = new FaviconDownloader($url);
     
    if($favicon->icoExists)
    {
        return $favicon->icoUrl."\n";
    } 
    else 
    {
        return NULL;
    }
}

 function logomembername($mid){
 $logo   = getMemberName($mid);
 $logo   = explode(' ',$logo);
 $sword   ='';
 $fword=str_split($logo[0]);
 $logolastindex=end($logo);
 if($logo[1]){
  $sword = str_split($logolastindex);
 }
 $word = $fword[0].''.$sword[0];
 return $word;

}
function get_bussiness_list(){
	$CI=& get_instance();
	$CI->load->model('model_category');
	$result = $CI->model_category->bussiness_type_list();	
	return $result;	
}
function url_to_domain($url)
{
    $host = @parse_url($url, PHP_URL_HOST);
    // If the URL can't be parsed, use the original URL
    // Change to "return false" if you don't want that
    if (!$host)
        $host = $url;
    // The "www." prefix isn't really needed if you're just using
    // this to display the domain to the user
    if (substr($host, 0, 4) == "www.")
        $host = substr($host, 4);
    // You might also want to limit the length if screen space is limited
    if (strlen($host) > 50)
        $host = substr($host, 0, 47) . '...';
    return $host;
}
function mytalk($mid){
	
	$CI=& get_instance();
	$CI->load->model('model_community');
	$result = $CI->model_community->get_mycommunity_detailslimit($mid);	
	return $result;	
	
}

?>