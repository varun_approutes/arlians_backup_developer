/*--------mortgage needs------*/

var sld_1;
var sld_2;
var itemindx = 0;
var sliderItemLen;

var testimonial_ItemLen;
var testiItemIndx = 0;

$(document).ready(function(){
	
	/* var slidervars_1 = {moveItemIdex: 4, displayItem: 4, prnt_cont:'.loveus-section .slider-holder', leftnav: '#loveus-leftarrow', rightnav: '#loveus-rightarrow'}
	sld_1 = new RespFleiSlider(slidervars_1)
	sld_1.init(); */
	
	var slidervars_2 = {moveItemIdex: 4, displayItem: 4, prnt_cont:'.partners-section .slider-holder', leftnav: '#partner-leftArrow', rightnav: '#partner-rightArrow'}
	sld_2 = new RespFleiSlider(slidervars_2)
	sld_2.init();
	
	navSliderInit();
	testimonialSlider();
	
	$('.featute-nav-container li').bind('click', function(){
		var id = $(this).find('a').attr('href');
		goto(id);
		return false;
	});
	
	$('.menu-container>li').hover(function(){
		//var _this = $(this);
		//var timeout = setTimeout(function(){_this.find('>ul').fadeIn(500); clearTimeout(timeout)}, 50);
		$(this).find('>ul').show();
	},function(){
		//$(this).find('>ul').fadeOut(500);
		$(this).find('>ul').hide();
	});
	
	/* $('.banner-container img').load(function(){
		$('.banner-content').css({'margin-top': -$('.banner-container').height()/1.25});
	}) */
	
});

var timeoutId;
window.onresize = function(){
	window.clearTimeout(timeoutId);
	timeoutId = window.setTimeout(resizeWindow, 10);
}

function resizeWindow(){
	
	$('.slider-cont').css({'width': $('.container').width()});
	$('.slider-cont-ul').css({'width': $('.slider-cont').width()*sliderItemLen});
	$('.slider-item').css({'width': $('.container').width()});
	itemindx = 0;
	$('.slider-cont-ul').css({'margin-left': 0});
	$('.bullet-container li').removeClass('bull-active');
	$('.bullet-container li').eq(0).addClass('bull-active');
	
	//sld_1.init();
	sld_2.init();
	
	/* $('.banner-content').css({'margin-top': -$('.banner-container').height()/1.25}); */
}

function testimonialSlider(){
	testimonial_ItemLen = $('.loveus-section .slider-holder li').length;
	
	
	for(i=0; i<testimonial_ItemLen; i++){
		var bull_li = $('<li id="tbull-'+ i +'"></li>');
		$('.testimonial-bullets ul').append(bull_li);
		
		if(i == 0){
			bull_li.addClass('bull-active');
		}
		
		bull_li.bind('click', function(){
			testiItemIndx = $(this).attr('id').split('-')[1];
			$('.loveus-section .slider-holder ul').animate({'margin-left': -$('.loveus-section .slider-holder').width()*testiItemIndx}, 500);
			$('.testimonial-bullets li').removeClass('bull-active');
			$(this).addClass('bull-active');
		});
	}
	
	$('.testimonial-bullets').css({'width': 18*testimonial_ItemLen});
	//$('.loveus-section .slider-holder').css({'width': $('.container').width(), 'overflow': 'hidden'});
	$('.loveus-section .slider-holder ul').css({'width': $('.loveus-section .slider-holder').width()*testimonial_ItemLen});
	$('.loveus-section .slider-holder ul li').css({'width': $('.loveus-section .slider-holder').width()});
	
	$('#loveus-leftarrow').bind('click', function(){		
		moveSlideTestimonial("left");
	});
	
	$('#loveus-rightarrow').bind('click', function(){		
		moveSlideTestimonial("right");
	});
}

function navSliderInit() {
	sliderItemLen = $('.slider-cont .slider-item').length;
	
	for(i=0; i<sliderItemLen; i++){
		var bull_li = $('<li id="bull-'+ i +'"></li>');
		$('.bullet-container').append(bull_li);
		
		if(i == 0){
			bull_li.addClass('bull-active');
		}
		
		bull_li.bind('click', function(){
			itemindx = $(this).attr('id').split('-')[1];
			$('.slider-cont-ul').animate({'margin-left': -$('.container').width()*itemindx}, 500);
			$('.bullet-container li').removeClass('bull-active');
			$(this).addClass('bull-active');
		});
	}
	
	$('.slider-nav').css({'width': (23*sliderItemLen) + 22});
	$('.slider-cont').css({'width': $('.container').width()});
	$('.slider-cont-ul').css({'width': $('.slider-cont').width()*sliderItemLen});
	$('.slider-item').css({'width': $('.container').width()});
		
	$('.slider-nav .right-arrow').bind('click', function(){		
		moveSlide("right");
	});
	
	$('.slider-nav .left-arrow').bind('click', function(){		
		moveSlide("left");
	});
}

function moveSlide(dir){
	
	if(dir == "right"){
		itemindx++;
		if(itemindx > sliderItemLen-1){
			itemindx = sliderItemLen-1;
		}
	} else {
		itemindx--;
		if(itemindx < 0){
			itemindx = 0;
		}
	}
	$('.bullet-container li').removeClass('bull-active');
	$('.bullet-container li').eq(itemindx).addClass('bull-active');
	$('.slider-cont-ul').animate({'margin-left': -$('.container').width()*itemindx}, 500);
}

function moveSlideTestimonial(dir){
	
	if(dir == "right"){
		testiItemIndx++;
		if(testiItemIndx > testimonial_ItemLen-1){
			testiItemIndx = testimonial_ItemLen-1;
		}
	} else {
		testiItemIndx--;
		if(testiItemIndx < 0){
			testiItemIndx = 0;
		}
	}
		
	$('.testimonial-bullets li').removeClass('bull-active');
	$('.testimonial-bullets li').eq(testiItemIndx).addClass('bull-active');
	$('.loveus-section .slider-holder ul').animate({'margin-left': -$('.loveus-section .slider-holder').width()*testiItemIndx}, 500);
}


function RespFleiSlider(slidervars){
	var prnt_cont = slidervars.prnt_cont;
	var clicksliderlen = $(prnt_cont + ' li').length;
	var displayItem = slidervars.displayItem;
	var moveItemIdex = slidervars.moveItemIdex;
	var clickindex = 0;
	var item_wd;
	var container_wd;
	
	this.init = function(){
		if($(window).width() >= 320 && $(window).width() < 480){
			displayItem = 1;
			moveItemIdex = 1;
		} else if ($(window).width() >= 480 && $(window).width() <= 640){
			displayItem = 2;
			moveItemIdex=2;
		} else if ($(window).width() >= 641 && $(window).width() <= 800){
			displayItem = 3;
			moveItemIdex=3;
		} else {
			displayItem = 4;
			moveItemIdex=4;
		}
		clickindex= 0;
		item_wd = $(prnt_cont).width()/displayItem;
		$(slidervars.prnt_cont+' li').css({'width': item_wd});
		container_wd = item_wd*clicksliderlen;	
		$(prnt_cont+' ul').css({'width': container_wd, 'margin-left': 0});
	}
	
	function moveProductSlider(dir) {
		if(dir == "left"){
			moveItemIdex++;
			if(moveItemIdex > clicksliderlen){
				moveItemIdex = clicksliderlen;
			} else {
				clickindex++;
			}
		} else {
			moveItemIdex--;
			if(moveItemIdex < displayItem){
				moveItemIdex = displayItem;
			} else {
				clickindex--;
			}
		}
		
		var moveX = -item_wd*clickindex;
		$(prnt_cont+' ul').animate({'margin-left': moveX}, 500);
	}
	
	$(slidervars.rightnav).bind('click', function(){
		moveProductSlider("left");
	});
	
	$(slidervars.leftnav).bind('click', function(){
		moveProductSlider("right");
	});
	
	return this;
}

function goto(id) {
	var scrTo = $(id).offset().top - 100;
	$('html, body').animate({scrollTop:scrTo},4000);
	
}





