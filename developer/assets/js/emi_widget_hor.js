(function () {
	var url = "http://www.emicalculator.in/widget_hor";
    var scriptName = "embed.js"; //name of this script, used to get reference to own tag
    var jQuery; //noconflict reference to jquery
    //var jqueryPath = "http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"; 
	var jqueryPath = "http://code.jquery.com/jquery-1.11.1.js";
	var jqueryUiPath = "http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.js";
    var jqueryVersion = "1.8.3";
    var scriptTag; //reference to the html script tag
 
    /******** Get reference to self (scriptTag) *********/
    var allScripts = document.getElementsByTagName('script');
    var targetScripts = [];
    for (var i in allScripts) {
        var name = allScripts[i].src
        if(name && name.indexOf(scriptName) > 0)
            targetScripts.push(allScripts[i]);
    }
 
    scriptTag = targetScripts[targetScripts.length - 1];
 
    /******** helper function to load external scripts *********/
    function loadScript(src, onLoad) {
        var script_tag = document.createElement('script');
        script_tag.setAttribute("type", "text/javascript");
        script_tag.setAttribute("src", src);
 
        if (script_tag.readyState) {
            script_tag.onreadystatechange = function () {
                if (this.readyState == 'complete' || this.readyState == 'loaded') {
                    onLoad();
                }
            };
        } else {
            script_tag.onload = onLoad;
        }
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script_tag);
    }
 
    /******** helper function to load external css  *********/
    function loadCss(href) {
        var link_tag = document.createElement('link');
        link_tag.setAttribute("type", "text/css");
        link_tag.setAttribute("rel", "stylesheet");
        link_tag.setAttribute("href", href);
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(link_tag);
    }
	
    /******** load jquery into 'jQuery' variable then call main ********/
    if (window.jQuery === undefined || window.jQuery.fn.jquery !== jqueryVersion) {
        loadScript(jqueryPath, initjQuery);
    } else {
        initjQuery();
    }
	
	//load amcharts.js
	loadScript(url+'/js/amcharts.js', function() { /* loaded */ });

		
    function initjQuery() {
        jQuery = window.jQuery.noConflict(true);
        main();
    }
 
    /******** starting point for your widget ********/
    function main() {
      //your widget code goes here
		
        jQuery(document).ready(function ($) {
			loadCss(url+"/css/style_wiget_hor.css");
			loadCss(url+"/css/bootstrap.min.css");
			loadCss(url+"/css/bootstrap-theme.min.css");
			
			/*var html='<div class="row text-center bk_color_gray"><h3>Loan Calculator</h3></div><div class="row wrapper" style="width: 775px;"><div class="show-grid emicalcalulatordiv"><form id="emicalcalulatorform"><div class="sep lamount row"> <div  class="col-md-5"><label for="loanamount"><strong>Loan Amount</strong></label></div><div  class="col-md-7"><input id="loanamount" name="loanamount" style="width: 80%;" value="'+loanamount+'" type="text"/><span style="width: 20%;">&#8377;</span></div></div><div class="sep lamount row"> <div class="col-md-5"><label for="loaninterest"><strong>Interest Rate</strong> </label></div><div  class="col-md-7"><input id="loaninterest" name="loaninterest" style="width: 80%;" value="'+loaninterest+'" type="text"/><span style="width: 20%;">%</span></div></div><div class="sep lamount row"> <div class="col-md-5"><label for="loanterm"><strong>Loan Tenure</strong> </label></div><div  class="col-md-7"><input id="loanterm" style="width: 100%;" name="loanterm" value="'+loanterm+'" type="text"/></div></div><div id="tenurechoice" class="sep col-md-12 text-center"><div id="lyrs" class="col-md-6"> <input name="loantenure" id="loanyears" value="loanyears" checked="checked" type="radio" /><label id="loanyearslabel" for="loanyears">Years</label></div><div id="lmths" class="col-md-6"> <input name="loantenure" id="loanmonths" value="loanmonths" type="radio" /><label id="loanmonthslabel" for="loanmonths">Months</label></div></div></form></div><div id="emipaymentsummary" class="show-grid"><div id="emiamount" class="text-center"><h5>Monthly Payment (EMI)</h5><p>&#8377;<span>24,959</span></p></div><div id="emitotalinterest" class="text-center"><h5>Total Interest Payable</h5><p>&#8377;<span>34,90,279</span></p></div><div id="emitotalamount" class="text-center"><h5>Total Payment<br/>(Principal + Interest)</h5><p>&#8377;<span>59,90,279</span></p></div></div><div id="emipiechart_hor" class="show-grid" style="min-height:260px; width: 244px; height: 260px;padding-left: 0px;padding-right: 0px;" ></div></div><div class="row text-center bk_color_gray"><h3>Powered by <a href="http://emicalculator.in"> emicalculator.in</a></h3></div>';*/
			var html='<div class="wrapper" style="width: 775px;margin-left:200px;"><div class="show-grid emicalcalulatordiv"><form id="emicalcalulatorform"><div class="sep lamount row"> <div style="width: 116px;float: left;"><label for="loanamount"><strong>Loan Amount</strong></label></div><div  style="width: 162px;float: left;"><input id="loanamount" name="loanamount" style="width: 80%;" value="'+loanamount+'" type="text"/><span style="width: 20%;">&#8377;</span></div></div><div class="sep lamount row"> <div style="width: 116px;float: left;"><label for="loaninterest"><strong>Interest Rate</strong> </label></div><div style="width: 162px;float: left;"><input id="loaninterest" name="loaninterest" style="width: 80%;" value="'+loaninterest+'" type="text"/><span style="width: 20%;">%</span></div></div><div class="sep lamount row"> <div style="width: 116px;float: left;"><label for="loanterm"><strong>Loan Tenure</strong> </label></div><div style="width: 162px;float: left;"><input id="loanterm" style="width: 100%;" name="loanterm" value="'+loanterm+'" type="text"/></div></div><div id="tenurechoice" class="sep row text-center"><div id="lyrs" style="width: 144px;float: left;"> <input name="loantenure" id="loanyears" value="loanyears" checked="checked" type="radio" /><label id="loanyearslabel" for="loanyears">Years</label></div><div id="lmths" style="width: 144px;float: left;"> <input name="loantenure" id="loanmonths" value="loanmonths" type="radio" /><label id="loanmonthslabel" for="loanmonths">Months</label></div></div></form></div><div id="emipaymentsummary" class="show-grid"><div id="emiamount" class="text-center"><h5>Monthly Payment (EMI)</h5><p>&#8377;<span>24,959</span></p></div><div id="emitotalinterest" class="text-center"><h5>Total Interest Payable</h5><p>&#8377;<span>34,90,279</span></p></div><div id="emitotalamount" class="text-center"><h5>Total Payment<br/>(Principal + Interest)</h5><p>&#8377;<span>59,90,279</span></p></div></div><div id="emipiechart_hor" class="show-grid" style="min-height:260px; width: 244px; height: 260px;padding-left: 0px;padding-right: 0px;" ></div></div>';

			var widget = document.getElementById("widget_hor");
			widget.innerHTML = html;
			
			jQuery("#widget_hor #loanamount").blur(function () {
				jQuery("#widget_hor #loanamount").val(Math.round(jQuery("#widget_hor #loanamount").val().replace(/[^\d\.]/g, "")));
			});
			jQuery("#widget_hor #loaninterest").blur(function () {
				jQuery("#widget_hor #loaninterest").val(Math.round(1E3 * jQuery("#widget_hor #loaninterest").val().replace(/[^\d\.]/g, "")) / 1E3);
			});
			jQuery("#widget_hor #loanterm").blur(function () {
				"loanyears" == jQuery("#widget_hor #emicalcalulatorform input[name='loantenure']:checked").val() ? jQuery("#widget_hor #loanterm").val(Math.round(Math.round(12 * jQuery("#widget_hor #loanterm").val().replace(/[^\d\.]/g, "")) / 12 * 100) / 100) : jQuery("#widget_hor #loanterm").val(jQuery("#widget_hor #loanterm").val().replace(/[^\d\.]/g, ""));
			});
			jQuery("#widget_hor #emicalcalulatorform input[name='loantenure']").change(function () {
				"loanyears" == jQuery("#widget_hor #emicalcalulatorform input[name='loantenure']:checked").val() ? jQuery("#widget_hor #loanterm").val(Math.round(jQuery("#widget_hor #loanterm").val().replace(/[^\d\.]/g, "") / 12 * 100) / 100) : jQuery("#widget_hor #loanterm").val(Math.round(12 * jQuery("#widget_hor #loanterm").val().replace(/[^\d\.]/g, "")));
				//L();
			});
			H_hor("Loan Amount", 1E7, 5E4, 25E5, 20, 10.5, 30, 0.5, 240);
			J_hor();
			
			//example load css
			//loadCss("http://example.com/widget.css");
          
			//example script load
			//loadScript("http://example.com/anotherscript.js", function() { /* loaded */ });
        });
		
    }
 
	//funcion calculation
	function formatIndian(x){
		x=x.toString();
		var lastThree = x.substring(x.length-3);
		var otherNumbers = x.substring(0,x.length-3);
		if(otherNumbers != '')
			lastThree = ',' + lastThree;
		var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree;
		return res;
	}
	
	function J_hor() {
		//jQuery("#emicalcalulatorform").mask("Calculating EMI...");
		setTimeout(N_hor, 10);
	}
	
	function O_hor() {
    
		var chart;

		var chartData = [{
			name: "Principal Loan",
			value: c
			},
			{
			name: "Total Interest",
			value: parseInt(g * f - c)
		}];
		// PIE CHART
		chart = new AmCharts.AmPieChart();
		legend = new AmCharts.AmLegend();
		legend.valueText = "";
		legend.align = "center";
		
		chart.dataProvider = chartData;
		chart.titleField = "name";
		chart.valueField = "value";
		chart.outlineColor = "";
		chart.outlineAlpha = 0.8;
		chart.outlineThickness = 2;
		
		chart.labelsEnabled = true;
		chart.labelText = "[[percents]]%";
		chart.colors = ["#97c9fb", "#fbc964"];
		chart.balloonText = "[[title]]: [[percents]]% (?[[value]])";
		// this makes the chart 3D
		chart.depth3D = 20;
		chart.angle = 30;
		// WRITE
		
		chart.labelsEnabled = false;
		chart.marginTop = 0;
		chart.marginBottom = 0;
		
		chart.addLegend(legend);
		chart.write("emipiechart_hor");

		//console.log(chart);
	};

	function N_hor() {
		c = Math.abs(jQuery("#widget_hor #loanamount").val().replace(/[^\d\.]/g, ""));
		d = Math.abs(jQuery("#widget_hor #loaninterest").val() / 12 / 100);
		f = "loanyears" == jQuery("#widget_hor #emicalcalulatorform input[name='loantenure']:checked").val() ? Math.abs(Math.round(12 * jQuery("#widget_hor #loanterm").val())) : Math.abs(jQuery("#widget_hor #loanterm").val());
		0 == d && (jQuery("#widget_hor #loaninterest").val(5), d = 0.004166666666666667, q.slider("value", 5));
		0 == f && ("loanyears" == jQuery("#widget_hor #emicalcalulatorform input[name='loantenure']:checked").val() ? jQuery("#widget_hor #loanterm").val(1) : jQuery("#widget_hor #loanterm").val(12), f = 12);
		l = 0;
		g = Math.pow(1 + d, f) / (Math.pow(1 + d, f) - 1) * d * c;

		jQuery("#widget_hor #emiamount span").text(formatIndian(Math.round(g)));
		jQuery("#widget_hor #emitotalinterest span").text(formatIndian(Math.round(g * f - c), "n", "en-IN"));
		jQuery("#widget_hor #emitotalamount span").text(formatIndian(Math.round(g * f), "n", "en-IN"));
		
		O_hor();
		
		//jQuery("#widget_hor #emicalcalulatorform").unmask()
	}
	
	function H_hor(a, e, z, b, k, I, A, M, K) {
		//sua o day
		jQuery("#widget_hor #loanamount").unbind("change");
		jQuery("#widget_hor #loaninterest").unbind("change");
		jQuery("#widget_hor #loanterm").unbind("change");
		jQuery("label[for=loanamount]").html("<strong>" + a + "</strong>");

		jQuery("#widget_hor #loanamount").change(function () {
			J_hor();
		});

		jQuery("#widget_hor #loaninterest").change(function () {
			J_hor();
		});

		jQuery("#widget_hor #loanterm").change(function () {
			console.log(jQuery(this).val());;
			if("loanyears" == jQuery("#widget_hor #emicalcalulatorform input[name='loantenure']:checked").val()){
				jQuery("#widget_hor #loanterm").val(jQuery(this).val());
			} else {
				jQuery("#widget_hor #loanterm").val(jQuery(this).val());			
			}

			J_hor();
		});
		
	}
})();